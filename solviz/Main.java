import code.JavaCodeFilter;
import code.SourceCodeFormatter;

public class Main {
        
        public static void main(String[] args) {
                String javaCode = "public class HelloWorld {\n" + 
                                "       public static void main(String[] args) {\n" + 
                                "               System.out.println(\"Hello World\");\n" + 
                                "       }\n" + 
                                "}\n" + 
                                "";
                SourceCodeFormatter f = new JavaCodeFilter();
                String result;
                String coding1 = "<pre class=\"java\" style=\"border: 1px solid #b4d0dc; background-color: #ecf8ff;\">";
                String coding3 = "</pre>";
                result = f.filter(javaCode);
                result = coding1 + result + coding3;
                System.out.println(result);
        }
}

