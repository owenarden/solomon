package solomon.translate;

import polyglot.ast.Ext;
import polyglot.ast.Node;
import pyxis.common.Placement;
import pyxis.extension.PlacementExt;

public class PyxUtil {

//	public static Ext ext(Node node) {
//        Ext e = node.ext();
//        if (node instanceof Expr) {
//	        while (e != null && !(e instanceof ExprExt)) {
//	            e = e.ext();
//	        }
//        }
//        else if (node instanceof ClassBody) {
//	        while (e != null && !(e instanceof ClassBodyExt)) {
//	            e = e.ext();
//	        }
//        }
//        else if (node instanceof ProcedureDecl) {
//	        while (e != null && !(e instanceof ProcedureDeclExt)) {
//	            e = e.ext();
//	        }
//        }
//        else if (node instanceof Stmt || node instanceof FieldDecl) {
//	        while (e != null && !(e instanceof PlacementExt)) {
//	            e = e.ext();
//	        }
//        }
//        else {
//        	throw new InternalCompilerError("Unexpected node: " + node);
//        }
//        if (e == null)
//        	System.err.println("null ext for " + node);
//        	
//        return e;
//	}
	   public static PlacementExt placementExt(Node node) {
	        Ext e = node.ext();
	        while (e != null && !(e instanceof PlacementExt)) {
	                e = e.ext();
	        }
	        if (e == null)
	            System.err.println("null ext for " + node);
	            
	        return (PlacementExt) e;
	    }

	    public static String label(Placement p) {
	        if ( p == Placement.AppServer ) {
	            return "A";
	        }
	        else if ( p == Placement.Database ) {
	            return "D";
	        }
	        else {
	            throw new Error ("Unknown placement: " + p);
	        }
	    }

	    public static Placement placement(String label) {
	        if (label.equals("A")) return Placement.AppServer;
	        if (label.equals("D")) return Placement.Database;
	        throw new Error ("Unknown placement: " + label);
	    }

}
