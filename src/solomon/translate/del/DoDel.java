package solomon.translate.del;

import polyglot.ast.Do_c;
import polyglot.util.CodeWriter;
import polyglot.visit.PrettyPrinter;

public class DoDel extends LoopDel {
    private static final long serialVersionUID = 1L;

    @Override
    protected void prettyPrintPil(CodeWriter w, PrettyPrinter tr) {
        Do_c n = (Do_c) node();       
        w.write("do ");
        n.printSubStmt(n.body(), w, tr);
        w.write("while(");
        printCondition(w, tr);
        w.write("); ");
    }

}
