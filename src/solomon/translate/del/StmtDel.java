package solomon.translate.del;

import polyglot.ast.JL;
import polyglot.util.CodeWriter;
import polyglot.util.InternalCompilerError;
import polyglot.visit.Translator;
import pyxis.common.Placement;
import pyxis.extension.PlacementExt;
import solomon.translate.PyxUtil;

public class StmtDel extends PilDel implements JL {
    private static final long serialVersionUID = 1L;

    @Override
    public void translate(CodeWriter w, Translator tr) {
        if (tr instanceof PyxisTranslator) {
            PyxisTranslator ptr = (PyxisTranslator) tr;
            if (ptr.outputPil()) {
                PlacementExt ext = (PlacementExt) PyxUtil.placementExt(node());
                Placement p = ext.placement();
                if (p == null) 
                    throw new InternalCompilerError("No placement for " + node() +":"+ node().position() );
                String lbl = PyxUtil.label(p);
                w.write(":" + lbl + ": ");
            }
        }
        super.translate(w, tr);
    }
}
