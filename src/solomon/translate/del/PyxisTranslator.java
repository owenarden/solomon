package solomon.translate.del;

import polyglot.ast.NodeFactory;
import polyglot.frontend.Job;
import polyglot.frontend.TargetFactory;
import polyglot.types.TypeSystem;
import polyglot.visit.Translator;

public class PyxisTranslator extends Translator {

    protected boolean outputPil;

    public PyxisTranslator(Job job, TypeSystem ts, NodeFactory nf,
            TargetFactory tf, boolean outputPil) {
        super(job, ts, nf, tf);
        this.outputPil = outputPil;
    }

    public boolean outputPil() {
        return outputPil;
    }

}
