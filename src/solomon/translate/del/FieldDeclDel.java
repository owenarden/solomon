package solomon.translate.del;

import polyglot.ast.FieldDecl;
import polyglot.util.CodeWriter;
import polyglot.util.InternalCompilerError;
import polyglot.visit.Translator;
import pyxis.common.Placement;
import pyxis.extension.PlacementExt;
import solomon.translate.PyxUtil;

public class FieldDeclDel extends PilDel {
    private static final long serialVersionUID = 1L;

    @Override
	public void translate(CodeWriter w, Translator tr) {
	    if (tr instanceof PyxisTranslator) {
    	    PyxisTranslator ptr = (PyxisTranslator) tr;
    	    if (ptr.outputPil()) {
        	    FieldDecl fd = (FieldDecl) node();
        	    PlacementExt ext = PyxUtil.placementExt(fd);
        	    Placement p = ext.placement();
        	    if (p == null) { 
        	        if (!fd.flags().isStatic()) {
        	            throw new InternalCompilerError("No placement for " + node() +":"+ node().position() );
        	        }
        	        System.err.println("Warning: outputing static field with no placement: " + fd.name());        	        
        	    } else {
                    String lbl = PyxUtil.label(p);
                    w.write(":" + lbl + ": ");
        	    }
    	    }
	    }
		super.translate(w, tr);
	}
}
