package solomon.translate.del;

import polyglot.ast.While_c;
import polyglot.util.CodeWriter;
import polyglot.visit.PrettyPrinter;

public class WhileDel extends LoopDel {
    private static final long serialVersionUID = 1L;

    @Override
    protected void prettyPrintPil(CodeWriter w, PrettyPrinter tr) {
        While_c n = (While_c) node();
        w.write("while (");
        printCondition(w, tr);
        w.write(")");
        n.printSubStmt(n.body(), w, tr);
    }

}
