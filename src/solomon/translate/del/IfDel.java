package solomon.translate.del;

import polyglot.ast.Block;
import polyglot.ast.If_c;
import polyglot.util.CodeWriter;
import polyglot.util.InternalCompilerError;
import polyglot.visit.PrettyPrinter;
import pyxis.extension.PlacementExt;
import solomon.translate.PyxUtil;

public class IfDel extends PilDel {
    private static final long serialVersionUID = 1L;

    /* Write the statement to an output file. */
    protected void prettyPrintPil(CodeWriter w, PrettyPrinter tr) {
    	PlacementExt ext = (PlacementExt) PyxUtil.placementExt(node());
        pyxis.common.Placement p = ext.placement();
        if (p == null)
            throw new InternalCompilerError("No placement for " + node()
                    + ":" + node().position());

        If_c n = (If_c) node();
        w.write("if (");
        
        String lbl = PyxUtil.label(p);
        w.write(":" + lbl + ": ");

        n.printBlock(n.cond(), w, tr);
        w.write(")");

        n.printSubStmt(n.consequent(), w, tr);

        if (n.alternative() != null) {
            if (n.consequent() instanceof Block) {
                // allow the "} else {" formatting
                w.write(" ");
            } else {
                w.allowBreak(0, " ");
            }

            if (n.alternative() instanceof Block) {
                w.write("else ");
                n.print(n.alternative(), w, tr);
            } else {
                w.begin(4);
                w.write("else");
                n.printSubStmt(n.alternative(), w, tr);
                w.end();
            }
        }
    }
}