package solomon.translate.del;

import polyglot.ast.JL_c;
import polyglot.util.CodeWriter;
import polyglot.visit.PrettyPrinter;
import polyglot.visit.Translator;

public abstract class PilDel extends JL_c {
    private static final long serialVersionUID = 1L;

    public void translate(CodeWriter w, Translator tr) {
        if(tr instanceof PyxisTranslator) {
            PyxisTranslator ptr = (PyxisTranslator) tr;
            if (ptr.outputPil()) {
                prettyPrintPil(w, tr);
                return;
            }
        }
        super.translate(w, tr);
    }

    protected void prettyPrintPil(CodeWriter w, PrettyPrinter pp) {
        prettyPrint(w, pp);
    }
}
