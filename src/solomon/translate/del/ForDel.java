package solomon.translate.del;

import polyglot.util.CodeWriter;
import polyglot.util.InternalCompilerError;
import polyglot.visit.PrettyPrinter;

public class ForDel extends LoopDel {
    private static final long serialVersionUID = 1L;

    @Override
    protected void prettyPrintPil(CodeWriter w, PrettyPrinter tr) {
        throw new InternalCompilerError("Serializing for loops not yet supported");
    }

}
