package solomon.translate.del;

import polyglot.ast.AbstractDelFactory_c;
import polyglot.ast.DelFactory;
import polyglot.ast.JL;

public class PilDelFactory extends AbstractDelFactory_c {

    public PilDelFactory(DelFactory delFactory) {
        super(delFactory);
    }

    public PilDelFactory() {
    }

    @Override
    protected JL delLocalDeclImpl() {
        return null;
    }

    @Override
    protected JL delFieldDeclImpl() {
        return new FieldDeclDel();
    }

    @Override
    protected JL delStmtImpl() {
        return new StmtDel();
    }
    
    @Override
    protected JL delCompoundStmtImpl() {
        return null;
    }
        
    @Override
    protected JL delIfImpl() {
        return new IfDel();
    }

    @Override
    protected JL delDoImpl() {
        return new DoDel();
    }
    @Override
    protected JL delForImpl() {
        return new ForDel();
    }

    @Override
    protected JL delWhileImpl() {
        return new WhileDel();
    }

}
