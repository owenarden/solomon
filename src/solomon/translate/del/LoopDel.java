package solomon.translate.del;

import polyglot.ast.JL;
import polyglot.ast.Loop_c;
import polyglot.util.CodeWriter;
import polyglot.util.InternalCompilerError;
import polyglot.visit.PrettyPrinter;
import pyxis.common.Placement;
import pyxis.extension.PlacementExt;
import solomon.translate.PyxUtil;

public abstract class LoopDel extends PilDel implements JL {
    private static final long serialVersionUID = 1L;
   
    /* Write the statement to an output file. */
    protected void printCondition(CodeWriter w, PrettyPrinter tr) {
        PlacementExt ext = (PlacementExt) PyxUtil.placementExt(node());
        Placement p = ext.placement();
        if (p == null)
            throw new InternalCompilerError("No placement for " + node()
                    + ":" + node().position());

        Loop_c n = (Loop_c) node();
                
        String lbl = PyxUtil.label(p);
        w.write(":" + lbl + ": ");
        n.printBlock(n.cond(), w, tr);
    }
}
