package solomon.translate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import polyglot.ast.ArrayAccess;
import polyglot.ast.ArrayAccessAssign;
import polyglot.ast.Assign;
import polyglot.ast.Block;
import polyglot.ast.Call;
import polyglot.ast.ClassBody;
import polyglot.ast.ClassDecl;
import polyglot.ast.ClassMember;
import polyglot.ast.CodeDecl;
import polyglot.ast.CompoundStmt;
import polyglot.ast.ConstructorCall;
import polyglot.ast.ConstructorDecl;
import polyglot.ast.Eval;
import polyglot.ast.Expr;
import polyglot.ast.Ext;
import polyglot.ast.Field;
import polyglot.ast.FieldAssign;
import polyglot.ast.FieldDecl;
import polyglot.ast.Formal;
import polyglot.ast.Id;
import polyglot.ast.If;
import polyglot.ast.Initializer;
import polyglot.ast.Local;
import polyglot.ast.LocalAssign;
import polyglot.ast.LocalDecl;
import polyglot.ast.Loop;
import polyglot.ast.MethodDecl;
import polyglot.ast.New;
import polyglot.ast.NewArray;
import polyglot.ast.Node;
import polyglot.ast.ProcedureCall;
import polyglot.ast.ProcedureDecl;
import polyglot.ast.Receiver;
import polyglot.ast.Return;
import polyglot.ast.Special;
import polyglot.ast.Stmt;
import polyglot.ast.TypeNode;
import polyglot.frontend.ExtensionInfo;
import polyglot.frontend.Job;
import polyglot.translate.ExtensionRewriter;
import polyglot.translate.ext.ToExt_c;
import polyglot.types.FieldInstance;
import polyglot.types.MethodInstance;
import polyglot.types.ProcedureInstance;
import polyglot.types.SemanticException;
import polyglot.util.InternalCompilerError;
import polyglot.util.Position;
import polyglot.visit.AlphaRenamer;
import polyglot.visit.HaltingVisitor;
import polyglot.visit.NodeVisitor;
import pyxis.common.Placement;
import pyxis.extension.PlacementExt;
import solomon.SolExtensionInfo;
import solomon.analysis.SolStmtRegistrar;
import solomon.analysis.partition.ext.PGExt;
import solomon.analysis.partition.ext.PGUtil;
import solomon.analysis.partition.graph.AbstractPartitionGraph;
import solomon.analysis.partition.graph.ContextFreePG;
import solomon.analysis.partition.graph.PGEdge;
import solomon.analysis.partition.graph.PGLocation;
import solomon.analysis.partition.graph.PGNode;
import solomon.analysis.signatures.SolAnalysisSignatures.DBLocation;
import accrue.analysis.defuse.DefUseAnalysisFactory;
import accrue.analysis.ext.ExtFormal;
import accrue.analysis.ext.ObjanalExt;
import accrue.analysis.ext.ObjanalExt_c;
import accrue.analysis.goals.RegisterProceduresGoal;
import accrue.analysis.interprocanalysis.AbstractLocation;
import accrue.analysis.interprocanalysis.EdgeIdentifier;
import accrue.analysis.interprocanalysis.EdgeIdentifier.HeapUseKey;
import accrue.analysis.interprocanalysis.NodeIdentifier;
import accrue.analysis.interprocanalysis.Registrar;
import accrue.analysis.interprocvarcontext.AnalysisContextPeerMap;
import accrue.analysis.pointer.PointerAnalysisPass;
import accrue.analysis.pointer.PointsToGraph;

public class SolToPyxILRewriter extends ExtensionRewriter {
    protected final ContextFreePG pg;
    protected final SolStmtRegistrar registrar;
    protected final Map<FieldInstance, Node> declarations;
    protected final Registrar procRegistrar;
    protected final boolean unsolved;
    protected boolean in_public_method = false;
    protected boolean no_dependency_results = false;
    protected final AlphaRenamer alpha;

    protected final boolean trackStrings;

    public SolToPyxILRewriter(Job job, SolExtensionInfo from_ext,
            Map<FieldInstance, Node> declarations, ExtensionInfo to_ext) {
        this(job, from_ext, declarations, to_ext, false, false);
    }

    public SolToPyxILRewriter(Job job, SolExtensionInfo from_ext,
            Map<FieldInstance, Node> declarations, ExtensionInfo to_ext,
            boolean unsolved, boolean trackStrings) {
        super(job, from_ext, to_ext);
        this.registrar = (SolStmtRegistrar) PointerAnalysisPass.singleton(
                from_ext).registrar();
        ;
        this.pg = (ContextFreePG) from_ext.partitionGraph();
        this.declarations = declarations;
        this.procRegistrar = RegisterProceduresGoal.registrar(from_ext);
        this.unsolved = unsolved;
        this.alpha = new AlphaRenamer(nf);
        this.trackStrings = trackStrings;
    }

    @Override
    public NodeVisitor enterCall(Node n) throws SemanticException {
        SolToPyxILRewriter rw = (SolToPyxILRewriter) super.enterCall(n);
        if (n instanceof CodeDecl) {
            CodeDecl cd = (CodeDecl) n;
            PointsToGraph ptg = PointerAnalysisPass.singleton(from_ext)
                    .pointsToGraph();
            boolean neverCalled;

            if (cd instanceof Initializer
                    && !cd.codeInstance().flags().isStatic())
                // FIXME: We can't easily tell if an initializer is reachable,
                // so just
                // assume it is.
                neverCalled = false;
            else
                neverCalled = ptg.getContexts(cd.codeInstance()).isEmpty();
            if (neverCalled) {
                rw = (SolToPyxILRewriter) rw.copy();
                rw.no_dependency_results = neverCalled;
            }
            if (cd instanceof ProcedureDecl
                    && ((ProcedureDecl) cd).flags().isPublic()) {
                rw.in_public_method = true;
            }
        }
        return rw;
    }

    @SuppressWarnings("serial")
    @Override
    public Node leaveCall(Node parent, Node old, Node n, NodeVisitor v)
            throws SemanticException {
        Node pyx = super.leaveCall(parent, old, n, v);
        if (ToExt_c.ext(pyx) != null)
            throw new SemanticException("Translated node has a ToExt object:"
                    + pyx);

        if (pyx instanceof ClassBody) {
            ClassBody body = (ClassBody) pyx;
            List<ClassMember> members = new ArrayList<ClassMember>();
            // XXX: HACK: Pyxis can't deal w/ Initializers. Skip them.
            for (ClassMember m : (List<ClassMember>) body.members()) {
                if (!(m instanceof Initializer)) {
                    members.add(m);
                } else {
                    System.err
                            .println("WARNING: stripping instance initializer!");
                }
            }
            return body.members(members);
        }
        if (pyx instanceof CodeDecl) {
            CodeDecl cd = (CodeDecl) pyx;
            if (no_dependency_results && cd.body() != null) {
                return cd
                        .body((Block) qq()
                                .parseStmt(
                                        "{ throw new UnsupportedOperationException(\"not statically reachable\");}"));
            }
        }
        PGExt pgExt = PGUtil.ext(old);
        if (pgExt != null && pgExt.pgNode() != null) {
            PGNode pgnode = pgExt.pgNode();

            PGLocation loc = pgnode.location();
            if (unsolved && loc == PGLocation.ANY) {
                throw new InternalCompilerError("Currently not supported");
                // p = new Placement(pgnode.toVariableName()) {
                // public String toString() {
                // return placement;
                // }
                // };
            }
            if (old instanceof FieldDecl) {
                Placement p = PyxUtil.placement(loc.toPlacement());
                // FieldDecl fd = (FieldDecl) pyx;
                // PyxisFieldInstance pfi = (PyxisFieldInstance)
                // fd.fieldInstance();
                // pfi.placement(p);
                // XXX: HACK: Set placement here also so it can be prettyPrinted
                PlacementExt ext = (PlacementExt) PyxUtil.placementExt(pyx);
                ext.placement(p);
            } else if (old instanceof Stmt
                    && (!(old instanceof CompoundStmt || old instanceof LocalDecl)
                            || old instanceof If || old instanceof Loop)) {
                Placement p = PyxUtil.placement(loc.toPlacement());
                Stmt stmt = (Stmt) pyx;
                PlacementExt ext = (PlacementExt) PyxUtil.placementExt(stmt);
                if (ext == null)
                    throw new InternalCompilerError("No ext for " + stmt);
                ext.placement(p);

                // TODO: enforce invariant: If and loop conditions are local
                // expressions.
                // (never need fetching)
                // XXX: assumes the Expression flattener has run.
                if (!(old instanceof CompoundStmt)) {
                    List<Stmt> stmts = new ArrayList<Stmt>();
                    preStmt(pgnode, (Stmt) old, (Stmt) pyx, stmts);
                    stmts.add(stmt);
                    postStmt(pgnode, (Stmt) old, (Stmt) pyx, stmts);
                    pyx = nf.Block(stmt.position(), stmts);
                }
            }
            if (pyx instanceof LocalDecl) {
                LocalDecl ld = (LocalDecl) pyx;
                if (ld.init() != null) {
                    pyx = ld.init(null);
                }
            } else if (!(pyx instanceof CodeDecl || pyx instanceof ClassDecl
                    || parent instanceof If || pyx instanceof LocalDecl)) {
                // System.err.println("WARNING: Unplaceable node has a pgnode: "
                // + pyx);
            }

        } else if ((old instanceof Stmt && !(old instanceof LocalDecl) && (!(old instanceof CompoundStmt)
                || old instanceof If || old instanceof Loop))
                || old instanceof CodeDecl || old instanceof FieldDecl) {
            if (pgExt == null)
                System.err.println("WARNING: No PGExt for " + old + "("
                        + old.position() + ")");
            else
                System.err.println("WARNING: No PGNode for " + old + "("
                        + old.position() + ")");
        } else if (pyx instanceof Formal) {
            // HACK #1: we need an ObjAnalExt object b/c
            // ObjAnalCatch will try to call setIsCatchFormal
            ExtFormal hackExt = (ExtFormal) ObjanalExt_c.ext(old);
            // HACK #2: pyxis doesn't expect more than one ext() object,
            // so add this one to the end
            // Ext pyxExt = pyx.ext();
            hackExt = (ExtFormal) hackExt.ext((Ext) null);
            pyx = pyx.ext(hackExt);
            // pyx = pyx.ext(pyxExt);
        }

        if (pyx instanceof Block) {
            Block b = (Block) pyx;
            List<Stmt> newStmts = new ArrayList<Stmt>();
            // Collapse blocks
            boolean nestedBlock = false;
            for (Stmt s : (List<Stmt>) b.statements()) {
                if (s instanceof Block)
                    nestedBlock = true;
            }

            if (nestedBlock)
                b = (Block) b.visit(alpha);

            for (Stmt s : (List<Stmt>) b.statements()) {
                if (s instanceof Block) {
                    Block sub = (Block) s;
                    newStmts.addAll(((Block) sub).statements());
                } else
                    newStmts.add(s);
            }

            if (parent instanceof ProcedureDecl) {
                ProcedureDecl pd = (ProcedureDecl) parent;
                if (pd.flags().isPublic()) {
                    for (Formal f : (List<Formal>) pd.formals()) {
                        if (f.type().type().isReference()
                                && (trackStrings || !f.type().type()
                                        .equals(typeSystem().String()))) {
                            Local l = to_nf().Local(
                                    Position.compilerGenerated(), f.id());
                            Stmt stmt = createSendLocal(DBLocation.APPSERVER, l);
                            int index = 0;
                            if (pd instanceof ConstructorDecl)
                                index = 1;
                            newStmts.add(index, stmt);
                        }
                    }
                    if (pd instanceof ConstructorDecl
                            || ((MethodDecl) pd).returnType().equals(
                                    from_ts().Void())) {
                        // only insert if last statement is not a return.
                        // FIXME: this still may result in unreachable code!
                        if (!(newStmts.get(newStmts.size() - 1) instanceof Return)) {
                            // XXX: HACK: ensure public methods ("entry points")
                            // end on the client side
                            Stmt empty = to_nf().Empty(
                                    Position.compilerGenerated());
                            PlacementExt ext = (PlacementExt) PyxUtil
                                    .placementExt(empty);
                            ext.placement(PyxUtil.placement("A"));
                            newStmts.add(empty);
                        }
                    }
                }
            }
            if (!newStmts.isEmpty()) {
                Stmt last = newStmts.get(newStmts.size() - 1);
                if (last instanceof Eval
                        && ((Eval) last).expr() instanceof ProcedureCall) {
                    Stmt empty = to_nf().Empty(Position.compilerGenerated());
                    PlacementExt ext = (PlacementExt) PyxUtil
                            .placementExt(empty);
                    PlacementExt lastExt = (PlacementExt) PyxUtil
                            .placementExt(last);
                    ext.placement(lastExt.placement());
                    newStmts.add(empty);
                } else if (last instanceof Eval
                        && ((Eval) last).expr() instanceof Assign
                        && ((Assign) ((Eval) last).expr()).right() instanceof ProcedureCall) {
                    Stmt empty = to_nf().Empty(Position.compilerGenerated());
                    PlacementExt ext = (PlacementExt) PyxUtil
                            .placementExt(empty);
                    PlacementExt lastExt = (PlacementExt) PyxUtil
                            .placementExt(last);
                    ext.placement(lastExt.placement());
                    newStmts.add(empty);
                }
            }
            pyx = b.statements(newStmts);
        }

        return pyx;
    }

    protected void preStmt(PGNode pgnode, Stmt solStmt, Stmt pyxStmt,
            List<Stmt> stmts) {
        if (no_dependency_results) {
            FetchGenerator fg = new FetchGenerator(this, pgnode.location(),
                    stmts);
            fg.begin();
            solStmt.visit(fg);
            fg.finish();
        }
        if (in_public_method && solStmt instanceof Return) {
            // XXX: HACK: ensure public methods ("entry points")
            // send their return values
            // end on the client side
            Return r = (Return) pyxStmt;
            PlacementExt ext = (PlacementExt) PyxUtil.placementExt(r);
            Placement original_placement = ext.placement();
            Placement server = PyxUtil.placement("D");
            if (context().currentCode() instanceof MethodInstance) {
                MethodInstance mi = (MethodInstance) context().currentCode();
                if (original_placement.equals(server)
                        && mi.returnType().isReference()
                        && (trackStrings || !mi.returnType().equals(
                                typeSystem().String()))) {
                    // Should only need to send local half if remote half is
                    // always
                    // added after update.
                    Receiver val = r.expr();
                    // send local side of reference from original placement of
                    // return
                    Stmt send = createSendLocal(pgnode.location(), val);
                    stmts.add(send);
                }
            }
            ext.placement(PyxUtil.placement("A"));
        }
    }

    private static Receiver modifiedObj(Stmt stmt) {
        Eval eval = (Eval) stmt;
        Assign asn = (Assign) eval.expr();
        if (asn instanceof FieldAssign) {
            Field f = (Field) asn.left();
            return f.target();
        } else if (asn instanceof ArrayAccessAssign) {
            ArrayAccess aa = (ArrayAccess) asn.left();
            return aa.array();
        } else if (asn instanceof LocalAssign) {
            // RHS may be a native procedure call or a NewArray
            if (asn.right() instanceof NewArray) {
                return asn.left();
            } else if (asn.right() instanceof New) {
                return asn.left();
            } else if (asn.right() instanceof Call) {
                // must handle specially..
                return null;
            }
            throw new InternalCompilerError("Unexpected local assignment: "
                    + asn.right().getClass());
        }
        throw new InternalCompilerError("Unexpected assignment type: "
                + asn.getClass());
    }

    protected void postStmt(PGNode pgnode, Stmt solStmt, Stmt pyxStmt,
            List<Stmt> stmts) {
        // Insert sends for any updates of remote data
        // if the node has outgoing data deps and the update edge is remote,
        // insert send
        Stmt send = null;
        for (PGEdge out_edge : pg.outgoing(pgnode)) {
            if (isRemoteUpdate(out_edge)) {
                // this logic assumes that only one object is being updated.
                // this is always true for flattened code, but for more general 
                // programs, x.f = o.m() may require both x and o to be sent.
                if (pyxStmt instanceof Eval
                        && ((Eval) pyxStmt).expr() instanceof Assign) {
                    Receiver solLhs = modifiedObj(solStmt);
                    Receiver pyxLhs = (Receiver) solLhs.visit(this);
                    if (pyxLhs == null) {
                        System.err.println("WARNING: Cannot send : " + pyxStmt
                                + " to update " + out_edge);
                        continue;
                    }
                    send = createSendRemote(pgnode.location(), pyxLhs);
                    break;
                } else if (pyxStmt instanceof Eval
                            && ((Eval) pyxStmt).expr() instanceof Call) {
                    // must be an update in a native call
                    Eval eval = (Eval) solStmt;
                    Call call = (Call) eval.expr();
                    if (isNative(call.procedureInstance())
                            && call.target() instanceof Expr) {
                        send = sendNativeReceiver(call, pgnode,
                                (Expr) call.target(), out_edge);
                        break;
                    }
                    throw new InternalCompilerError(
                            "Don't know how to handle remote update: "
                                    + out_edge);
                } else {
                    throw new InternalCompilerError(
                            "Don't know how to handle remote update: "
                                    + out_edge);
                }
            } else if (isRemoteHeapUse(out_edge)) {
                if (pyxStmt instanceof Eval
                        && ((Eval) pyxStmt).expr() instanceof Assign) {
                    Receiver solLhs = modifiedObj(solStmt);
                    if (solLhs == null) {
                        // must be an update in a native call on the RHS
                        Eval eval = (Eval) solStmt;
                        Assign asn = (Assign) eval.expr();
                        if (asn.right() instanceof Call) {
                            Call call = (Call) asn.right();
                            if (isNative(call.procedureInstance())
                                    && call.target() instanceof Expr) {
                                send = sendNativeReceiver(call, pgnode,
                                        (Expr) call.target(), out_edge);
                                break;
                            }
                        } else if (asn.right() instanceof New) {
                            throw new InternalCompilerError(
                                    "Don't know how to handle remote use: "
                                            + out_edge);
                        } else {
                            throw new InternalCompilerError(
                                    "Don't know how to handle remote use: "
                                            + out_edge);
                        }
                    } else {
                        Receiver pyxLhs = (Receiver) solLhs.visit(this);
                        send = createSendLocal(pgnode.location(), pyxLhs);
                        break;
                    }
                } else {
                    if (pyxStmt instanceof Eval
                            && ((Eval) pyxStmt).expr() instanceof Call) {
                        Eval eval = (Eval) solStmt;
                        Call call = (Call) eval.expr();
                        if (isNative(call.procedureInstance())
                                && call.target() instanceof Expr) {
                            send = sendNativeReceiver(call, pgnode,
                                    (Expr) call.target(), out_edge);
                            break;
                        } else {
                            throw new InternalCompilerError(
                                    "Don't know how to handle remote use: "
                                            + out_edge);
                        }
                    } else {
                        throw new InternalCompilerError(
                                "Don't know how to handle remote use: "
                                        + out_edge);
                    }
                }
            } else if (pyxStmt instanceof ConstructorCall) {
                // send "this" after native constructor calls
                // assumes all super constructor calls are explicit.
                ConstructorCall cc = (ConstructorCall) solStmt;
                if (cc.procedureInstance() == null)
                    throw new InternalCompilerError("null procedure instance");
                if (isNative(cc.procedureInstance())) {
                    Special ths = to_nf().Special(cc.position(), Special.THIS);
                    send = createSendRemote(pgnode.location(), ths);
                    break;
                }
            }
        }
        if (send != null) {
            PlacementExt ext = (PlacementExt) PyxUtil.placementExt(send);
            // assumes only 2 nodes
            Placement p = PyxUtil.placement(pgnode.location().toPlacement());
            ext.placement(p);
            ;
            stmts.add(send);
        }
        // TODO: insert sends for speculative pushing on control transfers
    }

    private Stmt sendNativeReceiver(ProcedureCall call, PGNode pgnode,
            Expr solSend, PGEdge out_edge) {
        System.err.println("Native call updates receiver. Sending: " + solSend
                + " with type " + solSend.type());
        Receiver pyxSend = (Receiver) solSend.visit(this);
        return createSendLocal(pgnode.location(), pyxSend);
    }

    protected static boolean isRemoteUpdate(PGEdge edge) {
        return edge.key() == AbstractPartitionGraph.EDGE_UPDATE && edge.isCut();
    }

    protected static boolean isRemoteHeapUse(PGEdge edge) {
        return (edge.key() instanceof EdgeIdentifier.HeapUseKey)
                && edge.isCut();
    }

    protected PGLocation fieldLocation(FieldInstance fi) {
        FieldDecl fd = (FieldDecl) declarations.get(fi);
        if (fd == null) {
            throw new InternalCompilerError("No declaration for " + fi);
        }
        PGNode decl = pg.lookup(fd);
        return decl.location();
    }

    protected Set<PGLocation> arrayLocations(ArrayAccess aa) {
        ObjanalExt ext = ObjanalExt_c.ext(aa);
        AnalysisContextPeerMap<Set<EdgeIdentifier>> deps = ext
                .getAnalysisResult(DefUseAnalysisFactory.DATA_DEPENDENCIES);
        Set<EdgeIdentifier> edges;
        if (deps == null) {
            throw new InternalCompilerError(
                    "Array access has no Def/Use results : " + aa);
        } else {
            edges = new HashSet<EdgeIdentifier>();
            for (Set<EdgeIdentifier> next : deps.values()) {
                edges.addAll(next);
            }
        }
        Set<PGLocation> locations = new HashSet<PGLocation>();
        for (EdgeIdentifier edge : edges) {
            if (edge.getKey() instanceof HeapUseKey) {
                HeapUseKey key = (HeapUseKey) edge.getKey();
                for (AbstractLocation loc : key.used()) {
                    NodeIdentifier arrID = registrar
                            .getAllocSiteNodeID(loc.context);
                    locations.add(pg.lookup(arrID).location());
                }
            }
        }
        return locations;
    }

    protected Stmt createSendLocal(PGLocation location, Receiver lhs) {

        return createSend("sendLocalHeapObjsOnTransfer", location, lhs);
    }

    protected Stmt createSendRemote(PGLocation location, Receiver lhs) {
        return createSend("sendRemoteHeapCacheObjsOnTransfer", location, lhs);
    }

    private Stmt createSend(String method, PGLocation location, Receiver recv) {
        // if (PyxUtil.placementExt(recv) == null) throw new
        // InternalCompilerError("no ext for "+ recv);
        if (!(recv instanceof Expr))
            throw new InternalCompilerError(
                    "Sending static field updates not yet supported");
        Expr expr = (Expr) recv;
        try {
            TypeNode tn = nf.CanonicalTypeNode(Position.compilerGenerated(),
                    to_ts().typeForName("pyxis.runtime.PyxisAnnotations"));
            Stmt send = qq().parseStmt("%T.%s(%E);", tn, method, expr);
            place(send, location);
            return send;
        } catch (SemanticException e) {
            throw new InternalCompilerError(e);
        }
    }

    private void place(Stmt stmt, PGLocation location) {
        PlacementExt ext = (PlacementExt) PyxUtil.placementExt(stmt);
        Placement p = PyxUtil.placement(location.toPlacement());
        ext.placement(p);
    }

    protected Stmt createFetches(PGLocation location, List<Expr> es) {
        throw new Error("is this even used?");
        // Position pos = Position.compilerGenerated();
        // TypeNode tn = to_nf().CanonicalTypeNode(pos, to_ts().Object());
        // ArrayInit ai = to_nf().ArrayInit(pos, es);
        // NewArray na = to_nf().NewArray(pos, tn, 1, ai);
        // Stmt fetch = qq().parseStmt(
        // "pyxis.runtime.PyxisAnnotations.fetchHeapFromRemote(%E);", na);
        // PlacementExt ext = (PlacementExt) PyxUtil.ext(fetch);
        // Placement p = Placement.getPlacement(location.toPlacement());
        // ext.setPlacement(p);
        // return fetch;
    }

    public static class FetchGenerator extends HaltingVisitor {
        protected final List<Stmt> fetches;
        protected final Set<String> references;
        protected final PGLocation location;
        protected final SolToPyxILRewriter rw;
        protected boolean fetch_this = false;
        protected boolean native_call_args = false;

        public FetchGenerator(SolToPyxILRewriter rw, PGLocation loc,
                List<Stmt> fetches) {
            this.rw = rw;
            this.location = loc;
            this.fetches = fetches;
            this.references = new HashSet<String>();
        }

        @Override
        public NodeVisitor enter(Node n) {
            if (n instanceof Call) {
                Call c = (Call) n;
                FetchGenerator fg = (FetchGenerator) bypass(c.target());
                if (rw.isNative(c.procedureInstance()))
                    fg.native_call_args = true;
                return fg;
            }
            return this;
        }

        @Override
        public void finish() {
            List<Expr> exprs = new ArrayList<Expr>(references.size() + 1);
            if (fetch_this)
                exprs.add(rw.to_nf().This(Position.compilerGenerated()));
            for (String name : references) {
                Local l = createLocal(name);
                exprs.add(l);
            }
            if (!exprs.isEmpty()) {
                Stmt fetch = rw.createFetches(location, exprs);
                fetches.add(fetch);
            }
        }

        @Override
        public Node leave(Node old, Node n, NodeVisitor v) {
            if (n instanceof Field) {
                Field f = (Field) n;
                if (!(f.target() instanceof Expr)) {
                    return n;
                }
                // XXX: for now always fetch arrays
                if (f.target().type().isArray()
                        || rw.no_dependency_results
                        || !location
                                .equals(rw.fieldLocation(f.fieldInstance()))) {
                    if (f.target() instanceof Local) {
                        Local l = (Local) f.target();
                        references.add(l.name());
                    } else if (f.target() instanceof Special
                            && ((Special) f.target()).kind() == Special.THIS) {
                        fetch_this = true;
                    }
                    // unnecessary for lowered code
                    // else if (f.target() instanceof Field) {
                    // Field receiver = (Field) f.target();
                    // // Receiver must be a local field
                    // if(!location.equals(rw.fieldLocation(receiver.fieldInstance())))
                    // {
                    // throw new
                    // InternalCompilerError("Unexpected field access expression: "
                    // + f);
                    // }
                    // references.add(f.varInstance());
                    // }
                    else {
                        throw new InternalCompilerError(
                                "Unexpected field access expression: " + f);
                    }
                }
            } else if (n instanceof ArrayAccess) {
                ArrayAccess aa = (ArrayAccess) n;
                // UGH. Just always fetch arrays for now.

                if (aa.array() instanceof Local) {
                    Local l = (Local) aa.array();
                    if (!l.type().isPrimitive())
                        references.add(l.name());
                } else {
                    throw new InternalCompilerError(
                            "Unexpected array access expression: " + aa);
                }
                // Set<PGLocation> locs = rw.arrayLocations(aa);
                // if (locs.size() > 1 || !locs.contains(location)) {
                // if ( aa.array() instanceof Local ) {
                // fetches.add(createFetch(aa.array()));
                // }
                // else {
                // throw new
                // InternalCompilerError("Unexpected array access expression: "
                // + aa);
                // }
                // }
            } else if (native_call_args && n instanceof Local) {
                Local l = (Local) n;
                if (!l.type().isPrimitive())
                    references.add(l.name());
            }
            return n;
        }

        protected Local createLocal(String name) {
            Id id = rw.to_nf().Id(Position.compilerGenerated(), name);
            Local l = rw.to_nf().Local(id.position(), id);
            return l;
        }

    }

    // private static Expr box(QQ qq, Type t, Expr lhs) {
    // if (t == null)
    // throw new InternalCompilerError("Expr has no type " + lhs);
    // if (t.isBoolean()) {
    // return qq.parseExpr("Boolean.valueOf(%E)", lhs);
    // }
    // else if (t.isByte()) {
    // return qq.parseExpr("Byte.valueOf(%E)", lhs);
    // }
    // else if (t.isChar()) {
    // return qq.parseExpr("Character.valueOf(%E)", lhs);
    // }
    // else if (t.isDouble()) {
    // return qq.parseExpr("Double.valueOf(%E)", lhs);
    // }
    // else if (t.isFloat()) {
    // return qq.parseExpr("Float.valueOf(%E)", lhs);
    // }
    // else if (t.isInt()) {
    // return qq.parseExpr("Integer.valueOf(%E)", lhs);
    // }
    // else if (t.isLong()) {
    // return qq.parseExpr("Long.valueOf(%E)", lhs);
    // }
    // else if (t.isShort()) {
    // return qq.parseExpr("Short.valueOf(%E)", lhs);
    // }
    // throw new InternalCompilerError("Unexpected expression type:" +
    // lhs.type());
    // }

    public boolean isNative(ProcedureInstance pi) {
        ProcedureDecl pd = (ProcedureDecl) procRegistrar.getCode(pi);
        return !(pd != null && pd.body() != null);
    }
}
