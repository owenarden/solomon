package solomon;

import java.util.Collections;
import java.util.List;

import polyglot.frontend.EmptyPass;
import polyglot.frontend.ExtensionInfo;
import polyglot.frontend.Job;
import polyglot.frontend.Pass;
import polyglot.frontend.Scheduler;
import polyglot.frontend.goals.Goal;
import polyglot.frontend.goals.SourceFileGoal;
import polyglot.main.OptFlag.Arg;
import polyglot.main.Options;
import polyglot.main.UnhandledArgument;
import polyglot.main.UsageError;
import polyglot.util.InternalCompilerError;

public class SolOutputExtensionInfo extends SolExtensionInfo implements
        ExtensionInfo {
    protected ExtensionInfo parent;

    public SolOutputExtensionInfo() {
    }

    public Scheduler createScheduler() {
        return new SolOutputScheduler(this);
    }

    @Override
    protected SolOptions createOptions() {
        Options parentOpts = parent.getOptions();
        SolOptions opt = new SolOptions(this) {
            @Override
            protected void validateArgs() throws UsageError {
            }
        };
        
        // filter the parent's options by the ones this extension understands
        List<Arg<?>> arguments = parentOpts.filterArgs(opt.flags());
        try {
            opt.processArguments(arguments, Collections.<String> emptySet());
        }
        catch (UnhandledArgument ua) {
            System.err.println(ua.argument().flag());
            throw new InternalCompilerError(ua);
        }
        catch (UsageError e) {
            throw new InternalCompilerError("Got usage error while configuring output extension",
                                            e);
        }
        return opt;
    }

    static protected class SolOutputScheduler extends SolScheduler {
        public SolOutputScheduler(SolExtensionInfo extInfo) {
            super(extInfo);
        }
        public Goal Parsed(Job job) {
            return internGoal(new SourceFileGoal(job) {
                public Pass createPass(ExtensionInfo extInfo) {
                    return new EmptyPass(this);
                }
            });
        }
    }

    public void setParent(JL5Preprocessor parent) {
        this.parent = parent;
    }

}
