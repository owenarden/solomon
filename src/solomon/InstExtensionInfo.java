package solomon;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import polyglot.ast.ExtFactory;
import polyglot.ast.NodeFactory;
import polyglot.ast.NodeFactory_c;
import polyglot.frontend.CyclicDependencyException;
import polyglot.frontend.ExtensionInfo;
import polyglot.frontend.JLExtensionInfo;
import polyglot.frontend.JLScheduler;
import polyglot.frontend.Job;
import polyglot.frontend.Scheduler;
import polyglot.frontend.goals.Goal;
import polyglot.frontend.goals.Serialized;
import polyglot.frontend.goals.VisitorGoal;
import polyglot.main.Options;
import polyglot.types.TypeSystem;
import polyglot.util.InternalCompilerError;
import solomon.analysis.instrumentor.InstrumentingVisitor;
import solomon.analysis.instrumentor.ext.InstExtFactory_c;

public class InstExtensionInfo extends JLExtensionInfo {

    protected ExtensionInfo parent;

    public InstExtensionInfo(ExtensionInfo parent) {
        this.parent = parent;
    }

    public Options getOptions() {
        return parent.getOptions();
    }
    
    @Override
    protected Scheduler createScheduler() {
        return new InstScheduler(this);
    }

    protected NodeFactory createNodeFactory() {
        ExtFactory extFactory = new InstExtFactory_c();
        return new NodeFactory_c(extFactory);
    }

    public static class InstScheduler extends JLScheduler {

        public InstScheduler(ExtensionInfo extInfo) {
            super(extInfo);
        }

        @Override
        public Goal Serialized(Job job) {
            Goal g = internGoal(new Serialized(job) {
                @Override
                public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
                    List<Goal> l = new ArrayList<Goal>();
                    l.add(((InstScheduler) scheduler).Instrumented(job));
                    l.addAll(super.prerequisiteGoals(scheduler));
                    return l;
                }

            });
            return g;
        }

        /**
         * Instrument an AST.
         * 
         * @param job
         * @return
         */
        public Goal Instrumented(Job job) {
            TypeSystem ts = job.extensionInfo().typeSystem();
            NodeFactory nf = job.extensionInfo().nodeFactory();
            Goal g = internGoal(new VisitorGoal(job, new InstrumentingVisitor(
                    job, ts, nf)));
            try {
                g.addPrerequisiteGoal(this.ReachabilityChecked(job), this);
                g.addPrerequisiteGoal(this.ConstantsChecked(job), this);
                g.addPrerequisiteGoal(this.ExceptionsChecked(job), this);
                g.addPrerequisiteGoal(this.ExitPathsChecked(job), this);
                g.addPrerequisiteGoal(this.InitializationsChecked(job), this);
                g.addPrerequisiteGoal(this.ConstructorCallsChecked(job), this);
                g.addPrerequisiteGoal(this.ForwardReferencesChecked(job), this);
            } catch (CyclicDependencyException e) {
                throw new InternalCompilerError(e);
            }
            return g;
        }

    }
}
