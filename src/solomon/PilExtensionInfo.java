package solomon;

import java.util.Collections;
import java.util.List;

import polyglot.ast.NodeFactory;
import polyglot.frontend.EmptyPass;
import polyglot.frontend.ExtensionInfo;
import polyglot.frontend.Job;
import polyglot.frontend.Pass;
import polyglot.frontend.Scheduler;
import polyglot.frontend.goals.Goal;
import polyglot.frontend.goals.SourceFileGoal;
import polyglot.main.Options;
import polyglot.main.UsageError;
import polyglot.main.OptFlag.Arg;
import polyglot.util.InternalCompilerError;
import pyxis.PyxisOptions;
import pyxis.PyxisScheduler;
import pyxis.ast.PyxisExtFactory_c;
import pyxis.ast.PyxisNodeFactory_c;
import solomon.translate.del.PilDelFactory;

public class PilExtensionInfo extends pyxis.ExtensionInfo {
    protected ExtensionInfo parent;
    public PilExtensionInfo(ExtensionInfo parent) {
        this.parent = parent;
    }
    
    public Scheduler createScheduler() {
        return new OutputScheduler(this);
    }
    
    @Override
    protected PyxisOptions createOptions() {
        
        Options parentOpts = parent.getOptions();
        PyxisOptions popt = new PyxisOptions(this) {
            @Override
            protected void validateArgs() throws UsageError {
            }

        };
        
        // filter the parent's options by the ones this extension understands
        List<Arg<?>> arguments = parentOpts.filterArgs(popt.flags());
        try {
            popt.processArguments(arguments, Collections.<String> emptySet());
        }
        catch (UsageError e) {
            throw new InternalCompilerError("Got usage error while configuring output extension",
                                            e);
        }
        return popt;
    }

    @Override
    protected NodeFactory createNodeFactory() {
        return new PyxisNodeFactory_c(new PyxisExtFactory_c(), new PilDelFactory());
    }

    static protected class OutputScheduler extends PyxisScheduler {
        public OutputScheduler(PilExtensionInfo extInfo) {
            super(extInfo);
        }

        public Goal Parsed(Job job) {
            return internGoal(new SourceFileGoal(job) {
                public Pass createPass(polyglot.frontend.ExtensionInfo extInfo) {
                    return new EmptyPass(this);              
                }
            });
        }
    }

}
