package solomon;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.tools.JavaFileManager;

import polyglot.ast.NodeFactory;
import polyglot.frontend.AbstractPass;
import polyglot.frontend.CyclicDependencyException;
import polyglot.frontend.EmptyPass;
import polyglot.frontend.ExtensionInfo;
import polyglot.frontend.Job;
import polyglot.frontend.OutputPass;
import polyglot.frontend.Pass;
import polyglot.frontend.Scheduler;
import polyglot.frontend.goals.AbstractGoal;
import polyglot.frontend.goals.Barrier;
import polyglot.frontend.goals.CodeGenerated;
import polyglot.frontend.goals.EmptyGoal;
import polyglot.frontend.goals.Goal;
import polyglot.frontend.goals.Serialized;
import polyglot.frontend.goals.SourceFileGoal;
import polyglot.frontend.goals.VisitorGoal;
import polyglot.types.TypeSystem;
import polyglot.util.InternalCompilerError;
import polyglot.visit.LoopNormalizer;
import solomon.analysis.goals.CodeGeneratedForInstrumentor;
import solomon.analysis.instrumentor.InstrumentingVisitor;
import solomon.analysis.instrumentor.ProfileLoader;
import solomon.analysis.partition.BlockSubGraphBuilder;
import solomon.analysis.partition.BuildPartitionGraphGoal;
import solomon.analysis.partition.DependencyOrderRewriter;
import solomon.analysis.partition.RegisterBlocks;
import solomon.analysis.partition.graph.PartitionGraph;
import solomon.analysis.visit.BlockFlattener;
import solomon.analysis.visit.SynchRemover;
import solomon.constraints.Solution;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import solomon.solviz.SolvizGeneratedGoal;
import solomon.translate.SolToPyxILRewriter;
import solomon.translate.del.PyxisTranslator;
import accrue.analysis.AnalysisScheduler;
import accrue.analysis.ctldep.CtlDepGoal;
import accrue.analysis.defuse.DefUseAnalysisUtil;
import accrue.analysis.defuse.DefUseGoal;
import accrue.analysis.domination.PostDominatorGoal;
import accrue.analysis.goals.RegisterPointerStmtsGoal;
import accrue.analysis.goals.RegisterProceduresGoal;
import accrue.analysis.goals.SimplifyExpressionsGoal;

public class SolScheduler extends AnalysisScheduler {

    public SolScheduler(SolExtensionInfo extInfo) {
        super(extInfo);
    }

    public Goal AnalysesDone(Job job) {
        Goal g = super.AnalysesDone(job);
        try {
            g.addPrerequisiteGoal(SolvePartitionProblem(), this);
        } catch (CyclicDependencyException e) {
            throw new InternalCompilerError(e);
        }
        return g;
    }

    @Override
    public Goal Serialized(Job job) {
        Goal g = internGoal(new Serialized(job) {
            @Override
            public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
                List<Goal> l = new ArrayList<Goal>();
                l.add(((SolScheduler) scheduler).AnalysesDone(job));
                l.addAll(super.prerequisiteGoals(scheduler));
                return l;
            }
        });
        return g;
    }

    /**
     * Output an AST for instrumentation.
     * 
     * @param job
     * @return
     */
    public Goal CodeGeneratedForInstrumentor(Job job) {
        SolOptions opt = (SolOptions) extInfo.getOptions();
        JavaFileManager.Location outputDir = opt.normalizedDirectory();
        return CodeGeneratedForInstrumentor.create(extInfo, job, outputDir);
    }

    public Goal RegisterInstrumentation(Job job) {
        TypeSystem ts = job.extensionInfo().typeSystem();
        NodeFactory nf = job.extensionInfo().nodeFactory();
        Goal g = internGoal(new VisitorGoal(job, new InstrumentingVisitor(job,
                ts, nf, true)));

        try {
            g.addPrerequisiteGoal(this.TransformationsDone(job), this);
        } catch (CyclicDependencyException e) {
            throw new InternalCompilerError(e);
        }
        return g;
    }

    public Goal LoadProfileData(Job job) {
        TypeSystem ts = job.extensionInfo().typeSystem();
        NodeFactory nf = job.extensionInfo().nodeFactory();
        SolOptions opt = (SolOptions) extInfo.getOptions();
        Goal g = internGoal(new VisitorGoal(job, new ProfileLoader(job, ts, nf,
                opt.profileDir)));
        try {
            g.addPrerequisiteGoal(this.RegisterInstrumentation(job), this);
        } catch (CyclicDependencyException e) {
            throw new InternalCompilerError(e);
        }
        return g;
    }

    public Goal LoadAllProfileData() {
        return internGoal(new Barrier(this) {
            @Override
            public Goal goalForJob(Job job) {
                return ((SolScheduler) scheduler).LoadProfileData(job);
            }
        });
    }

    public Goal PostDom() {
        return PostDominatorGoal.singleton(extInfo);
    }

    public Goal ControlDeps() {
        return CtlDepGoal.singleton(extInfo);
    }

    public BuildPartitionGraphGoal BuildPartionGraph() {
        return BuildPartitionGraphGoal.singleton((SolExtensionInfo) extInfo);
    }

    public Goal IntraDefUseAnalysis() {
        return DefUseGoal.singleton(extInfo,
                DefUseAnalysisUtil.Mode.INTRA_EDGES);
    }

    public Goal BlockSubgraphsBuilt(Job job) {
        SolExtensionInfo ei = (SolExtensionInfo) extInfo;
        TypeSystem ts = ei.typeSystem();
        NodeFactory nf = ei.nodeFactory();

        Goal g = internGoal(new VisitorGoal(job, new BlockSubGraphBuilder(job,
                ts, nf, ei.partitionGraph())));
        try {
            g.addPrerequisiteGoal(SolvePartitionProblem(), this);
            g.addPrerequisiteGoal(BlocksRegistered(job), this);
            g.addPrerequisiteGoal(ControlDeps(), this);
            g.addPrerequisiteGoal(IntraDefUseAnalysis(), this);
        } catch (CyclicDependencyException e) {
            throw new InternalCompilerError(e);
        }
        return g;
    }

    // public Goal BlocksCollapsed(Job job) {
    // SolExtensionInfo ei = (SolExtensionInfo) extInfo;
    // TypeSystem ts = ei.typeSystem();
    // NodeFactory nf = ei.nodeFactory();
    // Goal g = internGoal(new VisitorGoal(job, new CollapseBlocks(job, ts,
    // nf)));
    // try {
    // g.addPrerequisiteGoal(SolvePartitionProblem(), this);
    // g.addPrerequisiteGoal(IntraDefUseAnalysis(), this);
    // } catch (CyclicDependencyException e) {
    // throw new InternalCompilerError(e);
    // }
    // return g;
    // }
    public Goal BlocksRegistered(Job job) {
        SolExtensionInfo ei = (SolExtensionInfo) extInfo;
        TypeSystem ts = ei.typeSystem();
        NodeFactory nf = ei.nodeFactory();

        Goal g = internGoal(new VisitorGoal(job,
                new RegisterBlocks(job, ts, nf)));
        try {
            g.addPrerequisiteGoal(SolvePartitionProblem(), this);
            g.addPrerequisiteGoal(IntraDefUseAnalysis(), this);
        } catch (CyclicDependencyException e) {
            throw new InternalCompilerError(e);
        }
        return g;
    }

    public Goal RewriteStatementOrder(Job job) {
        TypeSystem ts = job.extensionInfo().typeSystem();
        NodeFactory nf = job.extensionInfo().nodeFactory();
        Goal g = internGoal(new VisitorGoal(job, new DependencyOrderRewriter(
                job, ts, nf)));
        try {
            g.addPrerequisiteGoal(BlockSubgraphsBuilt(job), this);
        } catch (CyclicDependencyException e) {
            throw new InternalCompilerError(e);
        }
        return g;
    }

    public Goal InstantiatePartitionProblem() {
        return internGoal(new AbstractGoal(null) {
            @Override
            public Pass createPass(final ExtensionInfo extInfo) {
                return new AbstractPass(this) {
                    @Override
                    public boolean run() {
                        try {
                            SolExtensionInfo ei = (SolExtensionInfo) extInfo;
                            SolOptions opt = (SolOptions) extInfo.getOptions();
                            PartitionGraph graph = ei.partitionGraph();
                            graph.instantiate(ei.solverUtil(), ei.cpuBudget());
                            if (opt.print_partition_graph)
                                graph.printGraph(opt.outputUnsolvedPil());
                        } catch (SolverException e) {
                            throw new InternalCompilerError(e);
                        }
                        return true;
                    }
                };
            }

            @Override
            public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
                List<Goal> l = new ArrayList<Goal>();
                l.add(((SolScheduler) scheduler).BuildPartionGraph());
                l.addAll(super.prerequisiteGoals(scheduler));
                return l;
            }
        });
    }

    public Goal SolvePartitionProblem() {
        return internGoal(new AbstractGoal(null) {
            @Override
            public Pass createPass(final ExtensionInfo extInfo) {
                return new AbstractPass(this) {
                    @Override
                    public boolean run() {
                        try {
                            SolExtensionInfo ei = (SolExtensionInfo) extInfo;
                            SolOptions opt = (SolOptions) extInfo.getOptions();
                            PartitionGraph graph = ei.partitionGraph();
                            SolverUtil su = ei.solverUtil();
                            Solution soln = graph.problem().solve(su);
                            if (soln.isValid()) {
                                soln.applySolution(su);
                                if (opt.print_partition_graph)
                                    graph.printGraph(opt.outputUnsolvedPil());
                                graph.checkPartition(soln.objective(),
                                        ei.cpuBudget());
                                System.out
                                        .println("Partition has "
                                                + graph.controlTransfers()
                                                + " estimated control transfers for profiled workload");
                            } else {
                                throw new SolverException(
                                        "Could not reach valid solution: "
                                                + soln);
                            }
                        } catch (SolverException e) {
                            throw new InternalCompilerError(e);
                        }
                        return true;
                    }
                };
            }

            @Override
            public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
                List<Goal> l = new ArrayList<Goal>();
                l.add(((SolScheduler) scheduler).BuildPartionGraph());
                l.add(((SolScheduler) scheduler).InstantiatePartitionProblem());
                l.addAll(super.prerequisiteGoals(scheduler));
                return l;
            }
        });
    }

    @Override
    public Goal RegisterPointerStmts(Job job) {
        TypeSystem ts = extInfo.typeSystem();
        NodeFactory nf = extInfo.nodeFactory();
        final SolOptions opt = (SolOptions) extInfo.getOptions();

        Goal g = internGoal(new RegisterPointerStmtsGoal(job, ts, nf) {
            public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
                List<Goal> l = new ArrayList<Goal>();
                l.add(scheduler.ReachabilityChecked(job));
                l.add(scheduler.ExceptionsChecked(job));
                l.add(scheduler.ExitPathsChecked(job));
                l.add(scheduler.InitializationsChecked(job));
                l.add(scheduler.ConstructorCallsChecked(job));
                l.add(scheduler.ForwardReferencesChecked(job));

                if (!opt.load_profile) {
                    l.add(((AnalysisScheduler) scheduler)
                            .MoveInnerClassesGoal(job));
                    l.add(((AnalysisScheduler) scheduler)
                            .MoveLocalClassesGoal(job));
                    l.add(((AnalysisScheduler) scheduler)
                            .SimplifyExpressionsGoal(job));
                    l.add(((AnalysisScheduler) scheduler)
                            .RemoveFinallyBlocksGoal(job));
                    l.add(((AnalysisScheduler) scheduler)
                            .MoveFieldInitializers(job));
                }
                l.add(TransformationsDone(job));
                return l;
            }
        });
        return g;
    }

    /**
     * This implementation is identical to its superclass, but only requires
     * dependencies that require source modification if not loading profile data
     */
    @Override
    public Goal RegisterProcedures(Job job) {
        TypeSystem ts = extInfo.typeSystem();
        NodeFactory nf = extInfo.nodeFactory();

        Goal g = internGoal(new RegisterProceduresGoal(job, ts, nf) {
            @Override
            public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
                List<Goal> l = new ArrayList<Goal>();
                l.add(scheduler.TypeChecked(job));
                l.add(scheduler.ExceptionsChecked(job));
                l.add(scheduler.ExitPathsChecked(job));
                l.add(scheduler.InitializationsChecked(job));
                l.add(scheduler.ConstructorCallsChecked(job));
                l.add(scheduler.ForwardReferencesChecked(job));
                l.add(scheduler.ReachabilityChecked(job));
                final SolOptions opt = (SolOptions) extInfo.getOptions();
                if (!opt.load_profile) {
                    l.add(((AnalysisScheduler) scheduler)
                            .MoveFieldInitializers(job));
                    l.add(((AnalysisScheduler) scheduler)
                            .MoveInnerClassesGoal(job));
                    l.add(((AnalysisScheduler) scheduler)
                            .MoveLocalClassesGoal(job));
                    l.add(((AnalysisScheduler) scheduler)
                            .SimplifyExpressionsGoal(job));
                    l.add(((AnalysisScheduler) scheduler)
                            .RemoveFinallyBlocksGoal(job));
                }
                l.add(TransformationsDone(job));
                return l;
            }
        });
        return g;
    }

    public Goal ExpressionsFlattened(Job job) {
        // return internGoal(new SourceFileGoal(job) {
        // public Pass createPass(ExtensionInfo extInfo) {
        // return new EmptyPass(this);
        // }
        // });

        TypeSystem ts = job.extensionInfo().typeSystem();
        NodeFactory nf = job.extensionInfo().nodeFactory();
        Goal g = internGoal(new VisitorGoal(job, new SolExpressionFlattener(
                job, ts, nf, true)));
        try {
            g.addPrerequisiteGoal(InitializationsChecked(job), this);
            g.addPrerequisiteGoal(TypeChecked(job), this);
            g.addPrerequisiteGoal(LoopsNormalized(job), this);
        } catch (CyclicDependencyException e) {
            throw new InternalCompilerError(e);
        }
        return g;
    }

    @Override
    public Goal TypesInitialized(Job job) {
        Goal g = super.TypesInitialized(job);
        try {
            g.addPrerequisiteGoal(LoopsNormalized(job), this);
        } catch (CyclicDependencyException e) {
            throw new InternalCompilerError(e);
        }
        return g;

    }

    public Goal LoopsNormalized(Job job) {
        TypeSystem ts = job.extensionInfo().typeSystem();
        NodeFactory nf = job.extensionInfo().nodeFactory();
        Goal g = internGoal(new VisitorGoal(job,
                new LoopNormalizer(job, ts, nf)));
        return g;
    }

    public Goal SynchronizedRemoved(Job job) {
        Goal g = internGoal(new VisitorGoal(job, new SynchRemover()));
        return g;
    }

    public Goal BlocksFlattened(Job job) {
        TypeSystem ts = job.extensionInfo().typeSystem();
        NodeFactory nf = job.extensionInfo().nodeFactory();
        Goal g = internGoal(new VisitorGoal(job,
                new BlockFlattener(job, ts, nf)));
        return g;
    }

    /**
     * A goal signifying that all transformations on source are complete.
     * 
     * @param job
     * @return
     */
    public Goal TransformationsDone(Job job) {
        Goal g = new SourceFileGoal(job) {
            @Override
            public Pass createPass(ExtensionInfo extInfo) {
                return new EmptyPass(this);
            }

            public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
                SolScheduler s = (SolScheduler) scheduler;
                List<Goal> l = new ArrayList<Goal>();
                final SolOptions opt = (SolOptions) extInfo.getOptions();
                if (!opt.load_profile) {
                    l.add(s.RemoveFinallyBlocksGoal(job));
                    l.add(s.MakeTargetsExplicitGoal(job));
                    l.add(s.MoveLocalClassesGoal(job));
                    l.add(s.MoveInnerClassesGoal(job));
                    l.add(s.MoveFieldInitializers(job));
                    l.add(s.BlocksFlattened(job));
                    l.add(s.SynchronizedRemoved(job));
                    l.add(s.SimplifyExpressionsGoal(job));
                }
                return l;
            }
        };
        return internGoal(g);
    }

    @Override
    public Goal SimplifyExpressionsGoal(Job job) {
        TypeSystem ts = extInfo.typeSystem();
        NodeFactory nf = extInfo.nodeFactory();
        modifiesAST();
        return internGoal(new SimplifyExpressionsGoal(job, nf, ts) {
            @Override
            public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
                List<Goal> l = new ArrayList<Goal>();
                l.add(((SolScheduler) scheduler).ExpressionsFlattened(job));
                l.addAll(super.prerequisiteGoals(scheduler));
                return l;
            }
        });
    }

    private void modifiesAST() {
        // final SolOptions opt = (SolOptions) extInfo.getOptions();
        // if (opt.load_profile) {
        // throw new InternalCompilerError("Cannot modify AST when loading"
        // + " profile data, it will invalidate line numbers");
        // }
    }

    // XXX: Temporary Hack to make pyxis happy (doesn't support labeled
    // statements)
    public Goal MakeTargetsExplicitGoal(Job job) {
        return internGoal(new EmptyGoal(job));
    }

    @Override
    public Goal RemoveFinallyBlocksGoal(Job job) {
        modifiesAST();
        return super.RemoveFinallyBlocksGoal(job);
    }

    @Override
    public Goal MoveInnerClassesGoal(Job job) {
        modifiesAST();
        return super.MoveInnerClassesGoal(job);
    }

    @Override
    public Goal MoveLocalClassesGoal(Job job) {
        modifiesAST();
        return super.MoveLocalClassesGoal(job);
    }

    @Override
    public Goal MoveFieldInitializers(Job job) {
        modifiesAST();
        return super.MoveFieldInitializers(job);
    }

    public Goal SourceGraphGenerated(Job job) {
        Goal g = SolvizGeneratedGoal.create(this, job);
        try {
            g.addPrerequisiteGoal(SolvePartitionProblem(), this);
        } catch (CyclicDependencyException e) {
            throw new InternalCompilerError(e);
        }
        return g;
    }

    public Goal UnsolvedSolToPyxILRewritten(Job job) {
        SolExtensionInfo extInfo = (SolExtensionInfo) this.extInfo;
        PilExtensionInfo pyxis = extInfo.pilExtensionInfo();
        Goal g = internGoal(new VisitorGoal(job, new SolToPyxILRewriter(job,
                extInfo, extInfo.fieldDeclarations(), pyxis, true, false)));
        try {
            g.addPrerequisiteGoal(InstantiatePartitionProblem(), this);
        } catch (CyclicDependencyException e) {
            throw new InternalCompilerError(e);
        }
        return g;
    }

    public Goal SolToPyxILRewritten(Job job) {
        SolExtensionInfo extInfo = (SolExtensionInfo) this.extInfo;
        PilExtensionInfo pyxis = extInfo.pilExtensionInfo();
        Goal g = internGoal(new VisitorGoal(job, new SolToPyxILRewriter(job,
                extInfo, extInfo.fieldDeclarations(), pyxis)));
        try {
            g.addPrerequisiteGoal(SolvePartitionProblem(), this);
            if (extInfo.getOptions().reorderStmts())
                g.addPrerequisiteGoal(RewriteStatementOrder(job), this);
        } catch (CyclicDependencyException e) {
            throw new InternalCompilerError(e);
        }
        return g;
    }

    public boolean runToCompletion() {
        SolExtensionInfo extInfo = (SolExtensionInfo) this.extInfo;
        boolean complete = super.runToCompletion();
        if (complete && extInfo.getOptions().compilePil()) {
            PilExtensionInfo pyxis = extInfo.pilExtensionInfo();
            // Flush the outputfiles collection
            extInfo.compiler().outputFiles().clear();

            // Create a goal to compile every source file.
            for (Job job : pyxis.scheduler().jobs()) {
                Job newJob = pyxis.scheduler().addJob(job.source(), job.ast());
                pyxis.scheduler().addGoal(pyxis.getCompileGoal(newJob));
            }
            return pyxis.scheduler().runToCompletion();
        } else if (complete && extInfo.getOptions().profile) {
            InstExtensionInfo inst = extInfo.instExtensionInfo();
            // Flush the outputfiles collection
            extInfo.compiler().outputFiles().clear();
            // Create a goal to compile every source file.
            for (Job job : inst.scheduler().jobs()) {
                if (job.ast() != null)
                    throw new InternalCompilerError("non-null ast for " + job);
                Job newJob = inst.scheduler().addJob(job.source(), job.ast());
                inst.scheduler().addGoal(inst.getCompileGoal(newJob));
            }
            return inst.scheduler().runToCompletion();
        }
        return complete;
    }

    public Goal PilGenerated(Job job) {
        Goal g = internGoal(new CodeGenerated(job) {
            public Pass createPass(ExtensionInfo extInfo) {
                SolExtensionInfo solExtInfo = (SolExtensionInfo) extInfo;
                PilExtensionInfo pyxisExtInfo = solExtInfo.pilExtensionInfo();
                TypeSystem ts = pyxisExtInfo.typeSystem();
                NodeFactory nf = pyxisExtInfo.nodeFactory();
                return new OutputPass(this, new PyxisTranslator(job(), ts, nf,
                        solExtInfo.pilTargetFactory(), true));
            }

            @Override
            public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
                List<Goal> l = new ArrayList<Goal>();
                SolOptions opt = (SolOptions) extInfo.getOptions();
                // Do NOT serialize Pil types -- there aren't any yet!
                if (opt.outputUnsolvedPil()) {
                    l.add(((SolScheduler) scheduler)
                            .UnsolvedSolToPyxILRewritten(job));
                } else {
                    l.add(((SolScheduler) scheduler).SolToPyxILRewritten(job));
                }
                return l;
            }
        });
        return g;
    }

}
