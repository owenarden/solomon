package solomon;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import polyglot.ast.Stmt;
import polyglot.ast.Term;
import polyglot.frontend.JobExt;

public class SolJobExt implements JobExt {
    protected final Map<Stmt, Term> blockMap;
    protected final Map<Stmt, List<Stmt>> siblingsMap;
    
    public SolJobExt() {
        this.blockMap = new LinkedHashMap<Stmt, Term>();
        this.siblingsMap =  new LinkedHashMap<Stmt, List<Stmt>>();
    }
    
    public  Map<Stmt, Term> blockMap() {
        return blockMap;
    }
    
    public Map<Stmt, List<Stmt>> siblingsMap() {
        return siblingsMap;
    }    
}
