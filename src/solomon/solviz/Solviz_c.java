package solomon.solviz;

import polyglot.ast.JL_c;
import polyglot.util.CodeWriter;
import polyglot.visit.Translator;
import solomon.analysis.partition.ext.PGExt;
import solomon.analysis.partition.ext.PGUtil;
import solomon.analysis.partition.graph.PGEdge;
import solomon.analysis.partition.graph.PGNode;

public class Solviz_c extends JL_c implements Solviz {
    private static final long serialVersionUID = 1L;

    @Override
    public void translate(CodeWriter w, Translator tr) {
        PGExt pgExt = PGUtil.ext(node());
        if ( pgExt == null || pgExt.pgNode() == null) {
            return;
        }
        SolvizTranslator str = (SolvizTranslator) tr;
        PGNode pgnode = pgExt.pgNode();
        String loc = pgnode.location().toPlacement();
       
        w.write("<div id=\"H"+ pgnode.hashCode() +"\" class=\"place_" + loc + "\">");
//        "style=\"top:"+pgnode.nodeID().getNode().position().line()*1.10+"em;" +
//        "left:" +pgnode.nodeID().getNode().position().column() + "px\"
        w.write("<script>");
        w.newline();
        w.write("var out = []; var i = 0;");
        for (PGEdge edge : str.partitionGraph().outgoing(pgnode)) {
            w.write("out[i++] = '#H" + edge.to().hashCode() + "';");
            w.newline();
        }
        w.write("edgeMap['#H" + pgnode.hashCode() + "'] = out;");
        w.write("</script>"); 
//        w.write("<pre>");
        w.allowBreak(0);
        super.translate(w, tr);
        w.allowBreak(0);
//        w.write("</pre>");
        w.write("</div>");
    }
}
