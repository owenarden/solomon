package solomon.solviz;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.tools.FileObject;

import polyglot.ast.SourceFile;
import polyglot.ast.TopLevelDecl;
import polyglot.frontend.Job;
import polyglot.frontend.TargetFactory;
import polyglot.types.Package;
import polyglot.util.CodeWriter;
import polyglot.util.ErrorInfo;
import polyglot.visit.Translator;
import solomon.SolExtensionInfo;
import solomon.analysis.partition.graph.PartitionGraph;

public class SolvizTranslator extends Translator {
    
    protected final PartitionGraph pg;
    
    public SolvizTranslator(Job job, SolExtensionInfo extInfo) {
        super(job, extInfo.typeSystem(), extInfo.nodeFactory(), extInfo
                .solvizTargetFactory());
        this.pg = extInfo.partitionGraph();
    }
    
    public PartitionGraph partitionGraph() {
        return pg;
    }
    /** Translate a single SourceFile node */
    protected boolean translateSource(SourceFile sfn) {
        TargetFactory tf = this.tf;
        int outputWidth = job.compiler().outputWidth();
        
        // Find the public declarations in the file.  We'll use these to
        // derive the names of the target files.  There will be one
        // target file per public declaration.  If there are no public
        // declarations, we'll use the source file name to derive the
        // target file name.
        List<?> exports = exports(sfn);
        
        try {
            FileObject of;
            CodeWriter w;
            
            String pkg = "";
            
            if (sfn.package_() != null) {
                Package p = sfn.package_().package_();
                pkg = p.fullName();
            }
            
            TopLevelDecl first = null;
            
            if (exports.size() == 0) {
                // Use the source name to derive a default output file name.
                of = tf.outputFileObject(pkg, sfn.source());
            }
            else {
                first = (TopLevelDecl) exports.get(0);
                of = tf.outputFileObject(pkg, first.name(), sfn.source());
            }
            
            w = tf.outputCodeWriter(of, outputWidth);
            
            writeHeader(sfn, w);
            
            for (Iterator<?> i = sfn.decls().iterator(); i.hasNext(); ) {
                TopLevelDecl decl = (TopLevelDecl) i.next();
                
                if (decl.flags().isPublic() && decl != first) {
                    // We hit a new exported declaration, open a new file.
                    // But, first close the old file.
                    writeFooter(sfn, w);
                    w.flush();
                    w.close();
                    
                    of = tf.outputFileObject(pkg, decl.name(), sfn.source());
                    w = tf.outputCodeWriter(of, outputWidth);
                    
                    writeHeader(sfn, w);
                }
                
                translateTopLevelDecl(w, sfn, decl);
                
                if (i.hasNext()) {
                    w.newline(0);
                }
            }
            writeFooter(sfn, w);
            w.flush();
            return true;
        }
        catch (IOException e) {
            job.compiler().errorQueue().enqueue(ErrorInfo.IO_ERROR,
                    "I/O error while translating: " + e.getMessage());
            return false;
        }
    }
    
    /** Write the package and import declarations for a source file. */
    protected void writeHeader(SourceFile sfn, CodeWriter w) {
        w.write(
                "<html>" +
                "<head>" +
                "<style>" +
                ".place_C {" +
                "   border:0.1em dotted #d4e06b; " +
                "   opacity:0.8; " +
                "   filter:alpha(opacity=80); " +
                "   width:80ex;" +
                "   z-index:20; " +
//                "   position:absolute; " +
                "   color:black;" +
                "   font-family:helvetica, sans;" +
                "   padding-top:0.9em; " +
                "   font-size:0.9em;" +
                "   text-align:left;" +
                "  background-color:white;" +
                "}" +
                "</style>" +
                "</head>" + 
                "<body>" +
//                "<script src=\"http://code.jquery.com/jquery-latest.js\"></script>" +
                "<div id=\"source\">"+
                "<script>var edgeMap = [];</script>"); 
        //"<pre>\n");    
    }
    
    private static String draw_graph = 
    "<script>\n" +
    "</script>\n";
    protected void writeFooter(SourceFile sfn, CodeWriter w) {
        w.write("</div>");
        w.newline();
        w.write(draw_graph);
        w.newline();
        w.write("</body></html>");
    }
}
