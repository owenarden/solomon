package solomon.solviz;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import polyglot.frontend.ExtensionInfo;
import polyglot.frontend.Job;
import polyglot.frontend.OutputPass;
import polyglot.frontend.Pass;
import polyglot.frontend.Scheduler;
import polyglot.frontend.goals.Goal;
import polyglot.frontend.goals.SourceFileGoal;
import solomon.SolExtensionInfo;

public class SolvizGeneratedGoal extends SourceFileGoal {
    public static Goal create(Scheduler scheduler, Job job) {
        return scheduler.internGoal(new SolvizGeneratedGoal(job));
    }

    /**
     * @param job The job to compile.
     */
    protected SolvizGeneratedGoal(Job job) {
        super(job);
    }
    
    public Pass createPass(ExtensionInfo extInfo) {
        return new OutputPass(this, new SolvizTranslator(job(),
                (SolExtensionInfo) extInfo));
    }
    
    public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
        List<Goal> l = new ArrayList<Goal>();
        l.add(scheduler.Serialized(job));
        l.addAll(super.prerequisiteGoals(scheduler));
        return l;
    }
}
