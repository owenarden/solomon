package solomon.solviz;

import polyglot.ast.AbstractDelFactory_c;
import polyglot.ast.DelFactory;

public class SolvizDelFactory extends AbstractDelFactory_c {

    public SolvizDelFactory(DelFactory delFactory) {
        super(delFactory);
    }

    public SolvizDelFactory() {   
    }

    @Override
    protected Solviz delCodeDeclImpl() {
        return new Solviz_c();
    }

    @Override
    protected Solviz delFieldDeclImpl() {
        return new Solviz_c();
    }

    @Override
    protected Solviz delStmtImpl() {
        return new Solviz_c();
    }
    
    @Override
    protected Solviz delCompoundStmtImpl() {
        return null;
    }
        
    @Override
    protected Solviz delIfImpl() {
        return delStmtImpl();
    }

    @Override
    protected Solviz delLoopImpl() {
        return delStmtImpl();
    }

}
