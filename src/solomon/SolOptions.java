package solomon;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Collections;
import java.util.Set;

import javax.tools.JavaFileManager.Location;

import polyglot.frontend.ExtensionInfo;
import polyglot.main.OptFlag;
import polyglot.main.OptFlag.Arg;
import polyglot.main.OptFlag.DoubleFlag;
import polyglot.main.OptFlag.Switch;
import polyglot.main.UsageError;
import polyglot.util.InternalCompilerError;
import accrue.ObjAnalOptions;

public class SolOptions extends ObjAnalOptions {   
    public boolean profile;
    public File profileDir;
    protected File normalizedDir;

    public boolean load_profile;

	public boolean print_partition_graph;
    protected boolean outputPil;
    protected boolean outputGraph;
    protected boolean compilePil;
    protected Location normalized_output = null;
    protected double cpuBudget;
    protected double percentOfMaxBudget;

    protected double latency;
    protected double bandwidth;
    protected boolean outputUnsolvedPil;
    protected boolean reorderStmts;
    protected String solver;

    @Override
	public void setDefaultValues() {
		super.setDefaultValues();
		this.merge_strings = true;
		this.profile = false;
		this.load_profile = false;
		this.print_partition_graph = false;
		outputPil = true;
	    outputGraph = false;
	    compilePil = true;
	    output_width = 120;
	    cpuBudget = 10000.0;
	    bandwidth = 1000.0;
	    latency = 1.0;
	    reorderStmts = true;
	    solver = "lpsolve";
	    pointsToEngineClass = "accrue.analysis.pointer.PointsToEngineSingleThread";
//	    emptysetsigs = true;
	}

	public SolOptions(ExtensionInfo extension) {
        super(extension);
    }
	
    @Override
    protected void populateFlags(Set<OptFlag<?>> flags) {
        super.populateFlags(flags);
        flags.add(new Switch(new String[] {"-print-partition-graph", "--print-partition-graph"}, "XXX"));
        flags.add(new Switch(new String[] {"-output-pil", "--output-pil"}, "XXX"));
        flags.add(new Switch(new String[] {"-output-unsolved-pil", "--output-unsolved-pil"}, "XXX"));
        flags.add(new Switch(new String[] {"-no-reorder", "--no-reorder"}, "XXX"));
        
        flags.add(new OptFlag<File>(new String[] {"-profile", "--profile"}, "<dir>", 
                "Generate an instrumented version of the program that outputs profile data to <dir>.  ") {
            //"Relative directories will output data relative to the working directory of the program at runtime."
            @Override
            public Arg<File> handle(String[] args, int index) {
                File f = new File(args[index]);
                if (!f.exists()) f.mkdirs();
                return createArg(index + 1, f);
            }
        });
        
        flags.add(new OptFlag<File>(new String[] {"-load-profile-data", "--load-profile-data"}, "<dir>", 
                "Load profile data for classes from <dir>") {
            @Override
            public Arg<File> handle(String[] args, int index) {
                File f = new File(args[index]);
                if (!f.exists()) f.mkdirs();
                return createArg(index + 1, f);
            }
        });

        flags.add(new OptFlag<File>(new String[] {"-normalized-dir", "--normalized-dir"}, "<dir>", 
                "Output normalized source to <dir>.  Only valid when generating code with -profile.") {
            @Override
            public Arg<File> handle(String[] args, int index) {
                File f = new File(args[index]);
                if (!f.exists()) f.mkdirs();
                return createArg(index + 1, f);
            }
            @Override
            public Arg<File> defaultArg() {
                File f = new File("normalized");
                f.mkdirs();
                return createDefault(f);
            }
        });
        
        flags.add(new OptFlag<String>(new String[] {"-cpu-budget", "--cpu-budget"}, "<instr count>", 
                "Budget constraint for remote server.") {
            @Override
            public Arg<String> handle(String[] args, int index) {
                return createArg(index + 1, args[index]);
            }
        });
        
        flags.add(new DoubleFlag(new String[] {"-latency", "--latency"}, "<ms>", 
                "Average network latency."));
        
        flags.add(new DoubleFlag(new String[] {"-bandwidth", "--bandwidth"}, "<Gbps>", 
                "Average network bandwidth."));
        
        flags.add(new OptFlag<String>(new String[] {"-solver", "--solver"}, "<gurobi | lpsolve>", 
                "Solver to invoke for partition solution.") {
            @Override
            public Arg<String> handle(String[] args, int index)
                    throws UsageError {
                return createArg(index + 1, args[index]);
            }
        });
    }
    
    @Override
    protected void handleArg(Arg<?> arg) throws UsageError {
        if (arg.flag().ids().contains("-print-partition-graph")) {
            this.print_partition_graph = (Boolean) arg.value();
        }
        else if (arg.flag().ids().contains("-output-pil")) {
            this.outputPil = (Boolean) arg.value();
        }
        else if (arg.flag().ids().contains("-output-unsolved-pil")) {
            this.outputUnsolvedPil = (Boolean) arg.value();
        }
        else if (arg.flag().ids().contains("-no-reorder")) {
            this.reorderStmts = !(Boolean) arg.value();
        }
        else if (arg.flag().ids().contains("-profile")) {
            this.profile = true; 
            this.profileDir = (File) arg.value();
        }
        else if (arg.flag().ids().contains("-load-profile-data")) {
            this.load_profile = true;
            this.profileDir = (File) arg.value();
        }        
        else if (arg.flag().ids().contains("-normalized-dir")) {
            this.normalizedDir = (File) arg.value();
        }
        else if (arg.flag().ids().contains("-cpu-budget")) {
            String v = (String) arg.value();
            v = v.trim();
            try {
                this.cpuBudget  = Double.valueOf(v);
            } catch (NumberFormatException e) {
                this.cpuBudget = -1;
                try {
                    this.percentOfMaxBudget = DecimalFormat
                            .getPercentInstance().parse(v)
                            .doubleValue();
                } catch (ParseException pe) {
                    throw new UsageError("Invalid budget expression: "
                            + v);
                }
            }                          
        }
        else if (arg.flag().ids().contains("-latency")) {
            this.latency = (Double) arg.value();
        }
        else if (arg.flag().ids().contains("-bandwidth")) {
            this.bandwidth = (Double) arg.value();
        }
        else if (arg.flag().ids().contains("-solver")) {
            this.solver = (String) arg.value();
        }
        else super.handleArg(arg);
    }
    
    @Override
    protected void postApplyArgs() {
        super.postApplyArgs();

        // inter-option dependencies
        if (profile) {
            outputPil = false;
            compilePil = false;
        }
        if (outputPil) {
            compilePil = false;
            output_source_only = true;
            post_compiler = null;
            /* pyxis doesn't like it when 
             * strings are broken into concats */
            this.output_width = 1000;
        }
        if (outputUnsolvedPil) {
            outputPil = true;
            compilePil = false;
            output_source_only = true;
            post_compiler = null;
        }
        this.merge_strings = true;
        emptysetsigs = true;
    }
    
    public boolean outputPil() {
        return outputPil;
    }

    public boolean outputGraph() {
        return outputGraph;
    }

    public boolean compilePil() {
        return compilePil;
    }
        
    public Location normalizedDirectory() {
        if (normalized_output == null) {
            normalized_output = new Location() {            
                @Override
                public boolean isOutputLocation() {
                    return true;
                }
                
                @Override
                public String getName() {
                    return "normalized";
                }
            };
            Set<File> out = Collections.singleton(normalizedDir);
            try {
                extension.extFileManager().setLocation(normalized_output, out);
            } catch (IOException e) {
                throw new InternalCompilerError(e);
            }
        }
        return normalized_output;
    }

    public double cpuBudget() {
        return cpuBudget;
    }
    
    public double percentOfMaxBudget() {
        return percentOfMaxBudget;
    }

    /**
     * Network bandwidth in gigabytes/s
     * @return
     */
    public double bandwidthGBps() {
        return bandwidth;
    }

    /**
     * Network latency in ms
     * @return
     */
    public double latencyms() {
        return latency;
    }

    public boolean outputUnsolvedPil() {
        return outputUnsolvedPil;
    }

    public boolean reorderStmts() {
        return reorderStmts;
    }
    
    public String solver() {
        return solver;
    }

}
