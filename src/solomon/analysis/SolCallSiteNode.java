package solomon.analysis;

import polyglot.ast.Node;
import polyglot.types.ProcedureInstance;
import polyglot.util.Position;
import accrue.analysis.pointer.CallSiteNode;

public class SolCallSiteNode extends CallSiteNode {
    final protected Node node;
    public SolCallSiteNode(ProcedureInstance callee, String debugString, Position pos, Node node) {
        super(callee, debugString, pos);
        this.node = node;
    }
    
    public Node node() {
        return node;
    }
}
