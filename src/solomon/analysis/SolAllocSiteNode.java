package solomon.analysis;

import polyglot.ast.Node;
import polyglot.types.ClassType;
import polyglot.types.Type;
import polyglot.util.Position;
import accrue.analysis.pointer.AllocSiteNode;

public class SolAllocSiteNode extends AllocSiteNode {
    final protected Node node;

    public SolAllocSiteNode(Type type, ClassType containingClass,
            String debugString, Position pos, Node node) {
        this(type, containingClass, false, debugString, pos, node);
    }

    public SolAllocSiteNode(Type type, ClassType containingClass, boolean b,
            String debugString, Position pos, Node node) {
        super(type, containingClass, b, debugString, pos);
        this.node = node;
    }

    public Node node() {
        return node;
    }
}
