package solomon.analysis.signatures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import polyglot.ast.CodeDecl;
import polyglot.ast.Node;
import polyglot.ast.Term;
import polyglot.types.ClassType;
import polyglot.types.MemberInstance;
import polyglot.types.MethodInstance;
import polyglot.types.ProcedureInstance;
import polyglot.types.SemanticException;
import polyglot.types.Type;
import polyglot.types.TypeSystem;
import polyglot.util.InternalCompilerError;
import polyglot.util.Position;
import solomon.analysis.SolCallSiteNode;
import solomon.analysis.SolStmtRegistrar;
import accrue.analysis.interprocanalysis.AnalysisContext;
import accrue.analysis.interprocanalysis.NodeIdentifier;
import accrue.analysis.interprocanalysis.NodeIdentifier_c;
import accrue.analysis.pointer.AllocSiteNode;
import accrue.analysis.pointer.CContext;
import accrue.analysis.pointer.CallSiteNode;
import accrue.analysis.pointer.ConstructorContext;
import accrue.analysis.pointer.HContext;
import accrue.analysis.pointer.LibraryPointsToSignature;
import accrue.analysis.pointer.LocalNode;
import accrue.analysis.pointer.ObjectField;
import accrue.analysis.pointer.PointsToGraph;
import accrue.analysis.pointer.PointsToGraphNode;
import accrue.analysis.pointer.PointsToLibrarySigs;
import accrue.analysis.pointer.ReferenceVariableReplica;
import accrue.analysis.pointer.PointsToEngine.StmtAndContext;
import accrue.analysis.pointer.StmtProcedureCall.ExceptionHandlerPointsToGraphNode;
import accrue.analysis.pointer.StmtRegistrar;
import accrue.analysis.pointer.StmtVirtualMethodCall;
import accrue.analysis.pointer.analyses.HeapAbstractionFactory;

public class SolPointsToLibrarySigs extends PointsToLibrarySigs {
    protected static Map<String, LocalNode> globals = new HashMap<String, LocalNode>();

    public static Type typeForName(TypeSystem ts, String s) {
        try {
            if (s.endsWith("[]")) {
                Type t = typeForName(ts, s.substring(0, s.length() - 2));
                return ts.arrayOf(t);
            }
            if (s.contains(".")) {
                return ts.typeForName(s);
            } else {
                return ts.primitiveForName(s);
            }
        } catch (SemanticException e) {
            throw new InternalCompilerError(e);
        }
    }

    public static class ReturnsPersisted extends LibraryPointsToSignature {
        final String dbname;
        final String expectedType;
        final boolean singleResult;
        AllocSiteNode resultList;
        AllocSiteNode contents;

        public ReturnsPersisted(String dbname, String expectedType,
                boolean singleResult) {
            this.dbname = dbname;
            this.expectedType = expectedType;
            this.singleResult = singleResult;
        }

        protected AllocSiteNode resultSetNode(TypeSystem ts,
                ClassType container, Position pos) {
            if (this.resultList == null) {
                // create the alloc site
                this.resultList = new AllocSiteNode(
                        SolPointsToLibrarySigs
                                .typeForName(ts, "java.util.List"),
                        container, false, false, // not a precise type
                        "returnsPersisted", pos);
            }
            return this.resultList;
        }

        protected AllocSiteNode contents(TypeSystem ts, ClassType container,
                String typeName, Position pos) {
            if (this.contents == null) {
                // create the alloc site
                this.contents = new AllocSiteNode(
                        SolPointsToLibrarySigs.typeForName(ts, typeName),
                        container, false, false, // not a precise type
                        "returnsPersisted", pos);
            }
            return this.contents;
        }

        @Override
        public boolean process(CodeDecl caller, CContext callerContext,
                CallSiteNode callSiteNode, ProcedureInstance mi,
                List<ReferenceVariableReplica> args, HContext receiver,
                ReferenceVariableReplica result,
                List<ExceptionHandlerPointsToGraphNode> handlers,
                Map<Type, ReferenceVariableReplica> procThrows,
                StmtRegistrar stmtRegistrar, PointsToGraph g, Position origin,
                HeapAbstractionFactory af, StmtAndContext sac)
                throws SemanticException {
            if (result == null)
                return false;

            AnalysisContext aCtx = new AnalysisContext(callerContext,
                    ConstructorContext.NORMAL);
            Node callSite = ((SolCallSiteNode) callSiteNode).node();
            NodeIdentifier nodeID = new NodeIdentifier_c(aCtx, (Term) callSite);

            LocalNode global = globals.get(dbname);
            Type type = SolPointsToLibrarySigs.typeForName(mi.typeSystem(),
                    expectedType);

            if (global == null) {
                global = new LocalNode(dbname + "(global)", true, type,
                        Position.compilerGenerated()); // is a static local node
                globals.put(dbname, global);
            }
            ReferenceVariableReplica node = ReferenceVariableReplica.create(
                    callerContext, global, af);

            AllocSiteNode allocSite = contents(mi.typeSystem(),
                    ((MemberInstance) mi).container().toClass(), expectedType,
                    origin);
            HContext contentsCtx = af.record(callerContext, allocSite);
            ((SolStmtRegistrar) stmtRegistrar).registerAllocationID(
                    contentsCtx, nodeID);

            PointsToGraphNode entities = null;
            if (singleResult) {
                entities = result;
            } else {
                // create new context for list and link w/ result
                AllocSiteNode resultSet = resultSetNode(mi.typeSystem(),
                        ((MemberInstance) mi).container().toClass(), origin);
                HContext listContext = af.record(callerContext, resultSet);
                ((SolStmtRegistrar) stmtRegistrar).registerAllocationID(
                        listContext, nodeID);
                g.addEdge(sac, result, listContext, origin);

                // create pseudo-field for result list contents
                entities = ObjectField.create(listContext,
                        SolAnalysisSignatures.CONTENTS, type);
            }

            // read the node to record a dependency
            Set<HContext> fieldPointsTo = g.pointsToFiltered(node, type);
            boolean changed = false;
            // could return any entity
            for (HContext hc : fieldPointsTo) {
                changed |= g.addEdge(sac, entities, hc, origin);
            }

            // or a newly allocated object (i.e. native queries)
            changed |= g.addEdge(sac, entities, contentsCtx, origin);
            return changed;
        }

    }

    public static class PersistsArg extends LibraryPointsToSignature {

        public PersistsArg(String dbname, String typeName, int argIndex) {
            this.dbname = dbname;
            this.typeName = typeName;
            this.argIndex = argIndex;
        }

        final String dbname;
        final String typeName;
        final int argIndex;

        @Override
        public boolean process(CodeDecl caller, CContext callerContext,
                CallSiteNode callSiteNode, ProcedureInstance mi,
                List<ReferenceVariableReplica> args, HContext receiver,
                ReferenceVariableReplica result,
                List<ExceptionHandlerPointsToGraphNode> handlers,
                Map<Type, ReferenceVariableReplica> procThrows,
                StmtRegistrar stmtRegistrar, PointsToGraph g, Position origin,
                HeapAbstractionFactory af, StmtAndContext sac) throws SemanticException {

            LocalNode global = globals.get(dbname);
            Type fieldType = SolPointsToLibrarySigs.typeForName(
                    mi.typeSystem(), typeName);
            if (global == null) {
                global = new LocalNode(dbname + "(global)", true, fieldType,
                        Position.compilerGenerated()); // is a static local node
                globals.put(dbname, global);
            }
            ReferenceVariableReplica node = ReferenceVariableReplica.create(
                    callerContext, global, af);
            // read the node to record a dependency
            g.pointsTo(sac, node);

            boolean changed = false;
            for (HContext p : g.pointsTo(sac, args.get(argIndex))) {
                changed = g.addEdge(sac, node, p, origin) || changed;
            }
            return changed;
        }

    }

    public static class SolAssignsArgToField extends LibraryPointsToSignature {
        public SolAssignsArgToField(String fieldname, String fieldTypeName,
                int argIndex) {
            this.fieldname = fieldname;
            this.fieldTypeName = fieldTypeName;
            this.argIndex = argIndex;
        }

        final String fieldname;
        final String fieldTypeName;
        final int argIndex;

        @Override
        public boolean process(CodeDecl caller, CContext callerContext,
                CallSiteNode callSiteNode, ProcedureInstance mi,
                List<ReferenceVariableReplica> args, HContext receiver,
                ReferenceVariableReplica result,
                List<ExceptionHandlerPointsToGraphNode> handlers,
                Map<Type, ReferenceVariableReplica> procThrows,
                StmtRegistrar stmtRegistrar, PointsToGraph g, Position origin,
                HeapAbstractionFactory af, StmtAndContext sac) throws SemanticException {

            ObjectField node;
            Type receiverType = receiver.type();
            Type fieldType = SolPointsToLibrarySigs.typeForName(
                    mi.typeSystem(), fieldTypeName);
            if (receiverType.isReference()
                    && receiverType.toReference().fieldNamed(fieldname) != null) {
                node = ObjectField.create(receiver, receiverType.toReference()
                        .fieldNamed(fieldname));
            } else {
                node = ObjectField.create(receiver, fieldname, fieldType);
            }

            // read the node to record a dependency
            g.pointsTo(sac, node);

            boolean changed = false;
            for (HContext p : g.pointsTo(sac, args.get(argIndex))) {
                changed = g.addEdge(sac, node, p, origin) || changed;
            }
            return changed;

        }

    }

    /**
     * Returns a new node, and also the receiver.
     */
    public static class AssignsAndReturnsArg extends SolAssignsArgToField {
        public AssignsAndReturnsArg(String fieldname, String fieldTypeName,
                int argIndex) {
            super(fieldname, fieldTypeName, argIndex);
        }

        @Override
        public boolean process(CodeDecl caller, CContext callerContext,
                CallSiteNode callSiteNode, ProcedureInstance mi,
                List<ReferenceVariableReplica> args, HContext receiver,
                ReferenceVariableReplica result,
                List<ExceptionHandlerPointsToGraphNode> handlers,
                Map<Type, ReferenceVariableReplica> procThrows,
                StmtRegistrar stmtRegistrar, PointsToGraph g, Position origin,
                HeapAbstractionFactory af, StmtAndContext sac) throws SemanticException {
            boolean changed = super.process(caller, callerContext,
                    callSiteNode, mi, args, receiver, result, handlers,
                    procThrows, stmtRegistrar, g, origin, af, sac);
            if (result != null && receiver != null) {
                changed |= g.addEdges(sac, result, g.pointsTo(sac, args.get(argIndex)),
                        origin);
            }
            return changed;
        }

    }

    public static class SolReturnsField extends LibraryPointsToSignature {
        public SolReturnsField(String fieldname, String fieldTypeName) {
            this.fieldname = fieldname;
            this.fieldTypeName = fieldTypeName;
        }

        final String fieldname;
        final String fieldTypeName;

        @Override
        public boolean process(CodeDecl caller, CContext callerContext,
                CallSiteNode callSiteNode, ProcedureInstance mi,
                List<ReferenceVariableReplica> args, HContext receiver,
                ReferenceVariableReplica result,
                List<ExceptionHandlerPointsToGraphNode> handlers,
                Map<Type, ReferenceVariableReplica> procThrows,
                StmtRegistrar stmtRegistrar, PointsToGraph g, Position origin,
                HeapAbstractionFactory af, StmtAndContext sac) throws SemanticException {

            if (result == null)
                return false;

            ObjectField node;
            Type receiverType = receiver.type();
            Type fieldType = typeForName(mi.typeSystem(), fieldTypeName);

            if (receiverType.isReference()
                    && receiverType.toReference().fieldNamed(fieldname) != null) {
                node = ObjectField.create(receiver, receiverType.toReference()
                        .fieldNamed(fieldname));
            } else {
                node = ObjectField.create(receiver, fieldname, fieldType);
            }

            // read the node to record a dependency
            Set<HContext> fieldPointsTo = g.pointsTo(sac, node);
            boolean changed = false;
            for (HContext hc : fieldPointsTo) {
                changed |= g.addEdge(sac, result, hc, origin);
            }
            return changed;
        }
    }

    public static class InvokeMethodOfField extends LibraryPointsToSignature {
        final String fieldName;
        final String fieldTypeName;
        final String methodName;
        final List<String> argTypeNames;
        final List<Integer> argMap;

        InvokeMethodOfField(String fieldName, String fieldTypeName,
                String methodName, List<String> argTypes, List<Integer> argMap) {
            this.fieldName = fieldName;
            this.fieldTypeName = fieldTypeName;
            this.methodName = methodName;
            this.argTypeNames = argTypes;
            this.argMap = argMap;
        }

        @Override
        public boolean process(CodeDecl caller, CContext callerContext,
                CallSiteNode callSiteNode, ProcedureInstance mi,
                List<ReferenceVariableReplica> args, HContext receiver,
                ReferenceVariableReplica result,
                List<ExceptionHandlerPointsToGraphNode> handlers,
                Map<Type, ReferenceVariableReplica> procThrows,
                StmtRegistrar stmtRegistrar, PointsToGraph g, Position origin,
                HeapAbstractionFactory af, StmtAndContext sac) throws SemanticException {

            ObjectField node;
            Type receiverType = receiver.type();
            Type fieldType = typeForName(mi.typeSystem(), fieldTypeName);
            if (receiverType.isReference()
                    && receiverType.toReference().fieldNamed(fieldName) != null) {
                node = ObjectField.create(receiver, receiverType.toReference()
                        .fieldNamed(fieldName));
            } else {
                node = ObjectField.create(receiver, fieldName, fieldType);
            }

            // read the node to record a dependency
            Set<HContext> fieldPointsTo = g.pointsTo(sac, node);
            TypeSystem ts = mi.typeSystem();
            List<Type> argTypes = new ArrayList<Type>(argTypeNames.size());
            for (String typeName : argTypeNames) {
                argTypes.add(typeForName(ts, typeName));
            }
            List<ReferenceVariableReplica> argNodes = new ArrayList<ReferenceVariableReplica>(
                    argTypeNames.size());
            for (Integer index : argMap) {
                argNodes.add(args.get(index));
            }
            boolean changed = false;
            for (HContext hc : fieldPointsTo) {
                Type preciseType = hc.type();
                MethodInstance imi = ts.findMethod((ClassType) preciseType,
                        methodName, argTypes, (ClassType) receiverType);

                changed |= StmtVirtualMethodCall.process(result, args.get(0),
                        callSiteNode, imi, argNodes, handlers, procThrows,
                        caller, callerContext, g, stmtRegistrar, origin, af, sac);
            }
            return changed;
        }

    }

    public static class AssignsFieldOfArgToField extends
            LibraryPointsToSignature {
        final String argFieldName;
        final String fieldname;
        final String fieldTypeName;
        final int argIndex;

        public AssignsFieldOfArgToField(String fieldname, String fieldTypeName,
                int argIndex, String argFieldName) {
            this.fieldname = fieldname;
            this.fieldTypeName = fieldTypeName;
            this.argIndex = argIndex;
            this.argFieldName = argFieldName;
        }

        @Override
        public boolean process(CodeDecl caller, CContext callerContext,
                CallSiteNode callSiteNode, ProcedureInstance mi,
                List<ReferenceVariableReplica> args, HContext receiver,
                ReferenceVariableReplica result,
                List<ExceptionHandlerPointsToGraphNode> handlers,
                Map<Type, ReferenceVariableReplica> procThrows,
                StmtRegistrar stmtRegistrar, PointsToGraph g, Position origin,
                HeapAbstractionFactory af, StmtAndContext sac) throws SemanticException {
            ObjectField node;
            Type receiverType = receiver.type();
            Type fieldType = SolPointsToLibrarySigs.typeForName(
                    mi.typeSystem(), fieldTypeName);
            if (receiverType.isReference()
                    && receiverType.toReference().fieldNamed(fieldname) != null) {
                node = ObjectField.create(receiver, receiverType.toReference()
                        .fieldNamed(fieldname));
            } else {
                node = ObjectField.create(receiver, fieldname, fieldType);
            }

            // read the node to record a dependency
            g.pointsTo(sac, node);

            boolean changed = false;
            for (HContext p : g.pointsTo(sac, args.get(argIndex))) {
                Type argType = p.type();
                ObjectField from;

                if (argType.isReference()
                        && argType.toReference().fieldNamed(argFieldName) != null) {
                    from = ObjectField.create(p, argType.toReference()
                            .fieldNamed(argFieldName));
                } else {
                    // XXX: Shouldn't we pass the expected type of the *field*
                    // here?
                    from = ObjectField.create(p, argFieldName, argType);
                }

                for (HContext argField : g.pointsTo(sac, from)) {
                    changed = g.addEdge(sac, node, argField, origin) || changed;
                }
            }
            return changed;
        }

    }

    public static class AssignsFieldsOfArgToFields extends
            LibraryPointsToSignature {
        final Map<String, String> argFieldToFieldName;
        final int argIndex;

        public AssignsFieldsOfArgToFields(int argIndex,
                Map<String, String> argFieldToFieldName) {
            this.argIndex = argIndex;
            this.argFieldToFieldName = argFieldToFieldName;
        }

        @Override
        public boolean process(CodeDecl caller, CContext callerContext,
                CallSiteNode callSiteNode, ProcedureInstance mi,
                List<ReferenceVariableReplica> args, HContext receiver,
                ReferenceVariableReplica result,
                List<ExceptionHandlerPointsToGraphNode> handlers,
                Map<Type, ReferenceVariableReplica> procThrows,
                StmtRegistrar stmtRegistrar, PointsToGraph g, Position origin,
                HeapAbstractionFactory af, StmtAndContext sac) throws SemanticException {
            ObjectField node;
            Type receiverType = receiver.type();
            boolean changed = false;

            for (Entry<String, String> entries : argFieldToFieldName.entrySet()) {
                String argFieldName = entries.getKey();
                String fieldname = entries.getValue();
                if (receiverType.isReference()
                        && receiverType.toReference().fieldNamed(fieldname) != null) {
                    node = ObjectField.create(receiver, receiverType
                            .toReference().fieldNamed(fieldname));
                } else {
                    node = ObjectField
                            .create(receiver, fieldname, receiverType);
                }

                // read the node to record a dependency
                g.pointsTo(sac, node);

                for (HContext p : g.pointsTo(sac, args.get(argIndex))) {
                    Type argType = p.type();
                    ObjectField from;

                    if (argType.isReference()
                            && argType.toReference().fieldNamed(argFieldName) != null) {
                        from = ObjectField.create(p, argType.toReference()
                                .fieldNamed(argFieldName));
                    } else {
                        // XXX: Shouldn't we pass the expected type of the
                        // *field* here?
                        from = ObjectField.create(p, argFieldName, argType);
                    }

                    for (HContext argField : g.pointsTo(sac, from)) {
                        changed = g.addEdge(sac, node, argField, origin) || changed;
                    }
                }
            }
            return changed;
        }

    }

    public static class AssignsArgsToFields extends LibraryPointsToSignature {

        public AssignsArgsToFields(Map<Integer, String> argToFieldName,
                Map<String, String> fieldTypes) {
            this.argToFieldName = argToFieldName;
            this.fieldTypes = fieldTypes;
        }

        final Map<Integer, String> argToFieldName;
        final Map<String, String> fieldTypes;

        @Override
        public boolean process(CodeDecl caller, CContext callerContext,
                CallSiteNode callSiteNode, ProcedureInstance mi,
                List<ReferenceVariableReplica> args, HContext receiver,
                ReferenceVariableReplica result,
                List<ExceptionHandlerPointsToGraphNode> handlers,
                Map<Type, ReferenceVariableReplica> procThrows,
                StmtRegistrar stmtRegistrar, PointsToGraph g, Position origin,
                HeapAbstractionFactory af, StmtAndContext sac) throws SemanticException {

            boolean changed = false;
            for (Entry<Integer, String> entries : argToFieldName.entrySet()) {
                ObjectField node;
                Integer argIndex = entries.getKey();
                String fieldname = entries.getValue();
                String fieldTypeName = fieldTypes.get(fieldname);
                if (fieldTypeName == null)
                    throw new InternalCompilerError("No type for field "
                            + fieldname);

                Type fieldType = SolPointsToLibrarySigs.typeForName(
                        mi.typeSystem(), fieldTypeName);
                Type receiverType = receiver.type();
                if (receiverType.isReference()
                        && receiverType.toReference().fieldNamed(fieldname) != null) {
                    node = ObjectField.create(receiver, receiverType
                            .toReference().fieldNamed(fieldname));
                } else {
                    node = ObjectField.create(receiver, fieldname, fieldType);
                }

                // read the node to record a dependency
                g.pointsTo(sac, node);

                for (HContext p : g.pointsTo(sac, args.get(argIndex))) {
                    changed = g.addEdge(sac, node, p, origin) || changed;
                }
            }
            return changed;
        }

    }

    /**
     * Returns a new node, and maybe also the receiver. The canonical example of
     * this is java.lang.String.substring(), which may return this, or it may
     * allocate a new String.
     */
    public static class SolAllocatorOrReceiver extends AllocatorOrReceiver {

        /**
         * Global pseudo-locations defined by this signature and referred to by
         * name. For instance, writing to the console might be captured by the
         * "$OUTPUT$" location."
         */
        protected Set<String> staticLocations;
        protected Map<String, String> fieldTypes;

        protected Map<String, AllocSiteNode> extraAllocSiteNodes;
        protected Map<String, String> fieldAssignments;
        protected Set<String> fieldAllocs;

        public SolAllocatorOrReceiver(String allocatedType,
                boolean mayReturnReceiver) {
            this(allocatedType, mayReturnReceiver, Collections
                    .<String> emptySet());
        }

        public SolAllocatorOrReceiver(String allocatedType,
                boolean mayReturnReceiver, Set<String> staticAllocs) {
            this(allocatedType, mayReturnReceiver, staticAllocs, Collections
                    .<String> emptySet(), Collections
                    .<String, String> emptyMap());
        }

        public SolAllocatorOrReceiver(String allocatedType,
                boolean mayReturnReceiver, Set<String> staticAllocs,
                Set<String> fieldAllocs, Map<String, String> fieldTypes) {
            this(allocatedType, mayReturnReceiver, staticAllocs, fieldAllocs,
                    fieldTypes, Collections.<String, String> emptyMap());
        }

        public SolAllocatorOrReceiver(String allocatedType,
                boolean mayReturnReceiver, Set<String> staticAllocs,
                Set<String> fieldAllocs, Map<String, String> fieldTypes,
                Map<String, String> fieldAssignments) {
            super(allocatedType, mayReturnReceiver, false);
            this.staticLocations = staticAllocs;
            this.fieldAllocs = fieldAllocs;
            this.fieldTypes = fieldTypes;
            this.extraAllocSiteNodes = new HashMap<String, AllocSiteNode>(
                    fieldTypes.size());
            this.fieldAssignments = fieldAssignments;
        }

        @Override
        public boolean process(CodeDecl caller, CContext calleeContext,
                CallSiteNode callSiteNode, ProcedureInstance mi,
                List<ReferenceVariableReplica> args, HContext receiver,
                ReferenceVariableReplica result,
                List<ExceptionHandlerPointsToGraphNode> handlers,
                Map<Type, ReferenceVariableReplica> procThrows,
                StmtRegistrar stmtRegistrar, PointsToGraph g, Position origin,
                HeapAbstractionFactory af, StmtAndContext sac)
                throws SemanticException {
            SolStmtRegistrar solRegistrar = (SolStmtRegistrar) stmtRegistrar;
            TypeSystem ts = mi.typeSystem();
            AnalysisContext aCtx = new AnalysisContext(calleeContext,
                    ConstructorContext.NORMAL);
            Node callSite = ((SolCallSiteNode) callSiteNode).node();
            NodeIdentifier nodeID = new NodeIdentifier_c(aCtx, (Term) callSite);

            for (String pseudo : staticLocations) {
                solRegistrar.registerStaticPseudoAllocationID(pseudo, nodeID);
            }

            if (result == null)
                return false;

            AllocSiteNode allocSite = allocSiteNode(mi.typeSystem(),
                    ((MemberInstance) mi).container().toClass(), origin);

            HContext newContext = af.record(calleeContext, allocSite);

            // Associate the nodeID of this allocation site with the HContext
            solRegistrar.registerAllocationID(newContext, nodeID);

            boolean changed = false;
            for (String fieldname : fieldAllocs) {
                String allocType = fieldTypes.get(fieldname);
                if (allocType == null)
                    throw new InternalCompilerError("No type for field "
                            + fieldname);

                ObjectField fieldNode;

                // Assign an extra allocation to a field of the returned object
                Type type = newContext.type();
                Type fieldType = SolPointsToLibrarySigs.typeForName(ts,
                        allocType);

                if (type.isReference()
                        && type.toReference().fieldNamed(fieldname) != null) {
                    fieldNode = ObjectField.create(newContext, type
                            .toReference().fieldNamed(fieldname));
                } else {
                    fieldNode = ObjectField.create(newContext, fieldname,
                            fieldType);
                }
                // read the node to record a dependency
                g.pointsTo(sac, fieldNode);

                // Create the extra allocation and record it.
                AllocSiteNode extraAlloc = extraAllocSiteNode(ts,
                        ((MemberInstance) mi).container().toClass(), allocType,
                        origin);
                HContext extraHCtx = af.record(calleeContext, extraAlloc);

                // Associate the nodeID of this allocation site with the
                // HContext
                solRegistrar.registerAllocationID(extraHCtx, nodeID);
                changed |= g.addEdge(sac, fieldNode, extraHCtx, origin);
            }

            for (Entry<String, String> entry : fieldAssignments.entrySet()) {
                String toField = entry.getKey();
                String fromField = entry.getValue();
                String fromFieldTypeName = fieldTypes.get(fromField);
                if (fromFieldTypeName == null)
                    throw new InternalCompilerError("No type for field "
                            + fromField);
                Type fromFieldType = SolPointsToLibrarySigs.typeForName(ts,
                        fromFieldTypeName);
                String toFieldTypeName = fieldTypes.get(toField);
                if (toFieldTypeName == null)
                    throw new InternalCompilerError("No type for field "
                            + toField);
                Type toFieldType = SolPointsToLibrarySigs.typeForName(ts,
                        toFieldTypeName);

                // assign receiver field to new field
                Type type = receiver.type();

                // From receiver field
                ObjectField fromFieldNode;
                if (type.isReference()
                        && type.toReference().fieldNamed(fromField) != null) {
                    fromFieldNode = ObjectField.create(receiver, type
                            .toReference().fieldNamed(fromField));
                } else {
                    fromFieldNode = ObjectField.create(receiver, fromField,
                            fromFieldType);
                }

                // To allocated field
                ObjectField toFieldNode;
                if (type.isReference()
                        && type.toReference().fieldNamed(toField) != null) {
                    toFieldNode = ObjectField.create(receiver, type
                            .toReference().fieldNamed(toField));
                } else {
                    toFieldNode = ObjectField.create(receiver, toField,
                            toFieldType);
                }

                changed |= g.addEdges(sac, toFieldNode, g.pointsTo(sac, fromFieldNode),
                        origin);
            }

            changed |= g.addEdge(sac, result, newContext, origin);
            if (receiver != null && mayReturnReceiver) {
                changed |= g.addEdge(sac, result, receiver, origin);
            }

            return changed;
        }

        protected AllocSiteNode extraAllocSiteNode(TypeSystem ts,
                ClassType container, String allocTypeName, Position pos) {
            AllocSiteNode asn = extraAllocSiteNodes.get(allocTypeName);
            if (asn == null) {
                asn = new AllocSiteNode(SolPointsToLibrarySigs.typeForName(ts,
                        allocTypeName), container, false, isPreciseType,
                        "allcatorOrReceiver", pos);
                extraAllocSiteNodes.put(allocTypeName, asn);
            }
            return asn;
        }
    }

}
