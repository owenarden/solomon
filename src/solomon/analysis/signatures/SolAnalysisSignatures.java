package solomon.analysis.signatures;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import polyglot.ast.Term;
import polyglot.types.Flags;
import polyglot.types.MethodInstance;
import polyglot.types.ProcedureInstance;
import polyglot.types.Type;
import polyglot.types.TypeSystem;
import polyglot.util.Position;
import polyglot.visit.FlowGraph.PeerKey;
import pyxis.common.Placement;
import solomon.SolExtensionInfo;
import solomon.analysis.partition.PGLibrarySignature;
import solomon.analysis.partition.PGLibrarySignature.AlwaysAt;
import solomon.analysis.partition.PartitionGraphBuilder;
import solomon.analysis.partition.graph.PGEdge;
import solomon.analysis.partition.graph.PGLocation;
import solomon.analysis.partition.graph.PGLocation.SolveableLocation;
import solomon.analysis.partition.graph.PGNode;
import solomon.analysis.partition.graph.PartitionGraph;
import solomon.analysis.signatures.SolPointsToLibrarySigs.AssignsAndReturnsArg;
import solomon.analysis.signatures.SolPointsToLibrarySigs.AssignsArgsToFields;
import solomon.analysis.signatures.SolPointsToLibrarySigs.AssignsFieldOfArgToField;
import solomon.analysis.signatures.SolPointsToLibrarySigs.AssignsFieldsOfArgToFields;
import solomon.analysis.signatures.SolPointsToLibrarySigs.PersistsArg;
import solomon.analysis.signatures.SolPointsToLibrarySigs.ReturnsPersisted;
import solomon.analysis.signatures.SolPointsToLibrarySigs.SolAllocatorOrReceiver;
import solomon.analysis.signatures.SolPointsToLibrarySigs.SolAssignsArgToField;
import solomon.analysis.signatures.SolPointsToLibrarySigs.SolReturnsField;
import solomon.constraints.Constraint;
import solomon.constraints.LinearTerm;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import solomon.constraints.SolverVariable;
import solomon.translate.PyxUtil;
import accrue.analysis.defuse.DefUseAbsVal;
import accrue.analysis.defuse.DefUseAnalysisFactory;
import accrue.analysis.defuse.DefUseLibrarySignature;
import accrue.analysis.defuse.DefUseLibrarySignature.DEF_ARG;
import accrue.analysis.defuse.DefUseLibrarySignature.DEF_HIDDEN;
import accrue.analysis.defuse.DefUseLibrarySignature.Join;
import accrue.analysis.defuse.DefUseLibrarySignature.USE_DEF_HIDDEN;
import accrue.analysis.defuse.DefUseLibrarySignature.USE_DEF_USEARG_HIDDEN;
import accrue.analysis.defuse.DefUseLibrarySignature.USE_HIDDEN;
import accrue.analysis.interprocanalysis.AnalysisContext;
import accrue.analysis.interprocanalysis.NativeNodeIdentifier_c;
import accrue.analysis.interprocvarcontext.LibrarySignature;
import accrue.analysis.interprocvarcontext.VarContext;
import accrue.analysis.notnulldataflow.NotNullAbsVal;
import accrue.analysis.notnulldataflow.NotNullAnalysisFactory;
import accrue.analysis.notnulldataflow.NotNullAnalysisUtil;
import accrue.analysis.pointer.HContext;
import accrue.analysis.pointer.LibraryPointsToSignature;
import accrue.analysis.pointer.PointerAnalysisPass;
import accrue.analysis.pointer.PointsToLibrarySigs;
import accrue.analysis.pointer.PointsToLibrarySigs.AllocatorOrReceiver;
import accrue.analysis.pointer.StmtRegistrar;
import accrue.analysis.signatures.AnalysisSignatures;
//import solomon.analysis.signatures.SolPointsToLibrarySigs.AssignsArgsToFields;
//import solomon.analysis.signatures.SolPointsToLibrarySigs.AssignsFieldOfArgToField;
//import solomon.analysis.signatures.SolPointsToLibrarySigs.AssignsFieldsOfArgToFields;
//import solomon.analysis.signatures.SolPointsToLibrarySigs.SolAllocatorOrReceiver;
//import solomon.analysis.signatures.SolPointsToLibrarySigs.SolAssignsArgToField;
//import solomon.analysis.signatures.SolPointsToLibrarySigs.SolReturnsField;

@SuppressWarnings("unchecked")
public class SolAnalysisSignatures extends AnalysisSignatures {
    public static boolean trackString = false;

    public static LibraryPointsToSignature mayAllocString(boolean retRecv) {
        return trackString ? new SolAllocatorOrReceiver("java.lang.String",
                retRecv) : PointsToLibrarySigs.NOOP;
    }

    /** The state of an (or any) external database */
    public static String DATABASE = "$DATABASE$";
    /** The state of a ResultSet cursor. */
    public static String CURSOR = "$CURSOR$";
    /** The state of PreparedStatement params. */
    public static String SQLPARAM = "$SQLPARAMETERS$";

    public static String CONTENTS = "contents";
    public static String KEYS = "keys_contents";
    public static String VALUES = "values_contents";
    public static final String BUFFER = "buffer";

    public static class DBLocation implements PGLocation {
        public static final DBLocation APPSERVER = new DBLocation(0);
        public static final DBLocation DBSERVER = new DBLocation(1);

        protected int id;

        DBLocation(int id) {
            this.id = id;
            PGLocation.ID_MAP.put(id, this);
        }

        public static PGLocation toLocation(int val) {
            if (val != 0 && val != 1)
                throw new Error("Invalid location id " + val);
            return (val == 0) ? APPSERVER : DBSERVER;
        }

        @Override
        public Constraint locationConstraint(SolverUtil sutil,
                SolverVariable var) throws SolverException {
            LinearTerm[] term = new LinearTerm[] { sutil.linearTerm(1.0, var) };
            return sutil.linearConstraint(term, sutil.eq(), id);
        }

        @Override
        public String toPlacement() {
            return (DBSERVER == this) ? PyxUtil.label(Placement.Database)
                    : PyxUtil.label(Placement.AppServer);
        }

        @Override
        public String toString() {
            return (DBSERVER == this) ? "DATABASE" : "APPSERVER";
        }

        @Override
        public boolean equals(Object o) {
            // XXX: this ends up being a bad design
            if (o instanceof SolveableLocation) {
                SolveableLocation sl = (SolveableLocation) o;
                return equals(sl.location());
            } else if (o instanceof DBLocation) {
                DBLocation loc = (DBLocation) o;
                return id == loc.id;
            }
            return false;
        }
    }

    public static SolveableLocation JDBC = new SolveableLocation("JDBC");

    public static class DBCommunication extends AlwaysAt {
        private int DBCODE_COUNTER = 0;

        public DBCommunication(PGLocation location) {
            super(location);
        }

        @Override
        public PGLocation location(ProcedureInstance pi,
                AnalysisContext calleeContext, HContext receiver) {
            return location;
        }

        public void addMissingDependencies(PartitionGraphBuilder pgb,
                PGEdge incoming) {
            SolExtensionInfo extInfo = pgb.extensionInfo();
            TypeSystem ts = extInfo.typeSystem();
            MethodInstance fakeDBCodeInstance = ts.methodInstance(
                    Position.compilerGenerated(), ts.Object(), Flags.NONE,
                    ts.Void(), "DATABASE_CODE", Collections.<Type> emptyList(),
                    Collections.<Type> emptyList());
            NativeNodeIdentifier_c DATABASE_CODE = new NativeNodeIdentifier_c(
                    extInfo.anyContext(), fakeDBCodeInstance, DBCODE_COUNTER++);

            PartitionGraph pg = pgb.partitionGraph();
            PGNode nativeNode = incoming.to();
            PGNode dbcode = pg.lookup(DATABASE_CODE);
            if (dbcode == null) {
                dbcode = pg.createNode(DATABASE_CODE, DBLocation.DBSERVER);
            }
            dbcode.setWeight(0);
            PGEdge dbCall = pg.createEdge(nativeNode, dbcode, incoming.key());
            dbCall.setWeight(2 * incoming.weight());
            System.err.println("DB RT has weight:" + dbCall.weight()
                    + " for edge " + incoming + " from " + incoming.from());
        }
    }

    public static PeerKey CODE_ENTRY = new PeerKey(
            Collections.<Term> emptyList(), Term.ENTRY);
    public static PeerKey CODE_EXIT = new PeerKey(
            Collections.<Term> emptyList(), Term.EXIT);

    private final static List<String> EMPTY = Collections.<String> emptyList();

    public static final DefUseLibrarySignature DEF_SQL_PARAM = new DefUseLibrarySignature(
            List(SQLPARAM), EMPTY, false, false);

    public static final DefUseLibrarySignature USE_SQL_PARAM = new DefUseLibrarySignature(
            EMPTY, List(SQLPARAM), false, false);

    public static final DefUseLibrarySignature INCREMENT_CURSOR = new DefUseLibrarySignature(
            List(CURSOR), List(CURSOR), false, false);

    public static final DefUseLibrarySignature USE_CURSOR = new DefUseLibrarySignature(
            EMPTY, List(CURSOR), false, false);

    public static final DefUseLibrarySignature DEF_USE_DATABASE = new DefUseLibrarySignature(
            EMPTY, EMPTY, List(DATABASE), List(DATABASE), true, false);

    public static final DefUseLibrarySignature USE_DATABASE = new DefUseLibrarySignature(
            EMPTY, EMPTY, EMPTY, List(DATABASE), false, false);

    public static final DefUseLibrarySignature USE_DEF_DATABASE = new DefUseLibrarySignature(
            EMPTY, EMPTY, List(DATABASE), List(DATABASE), false, false);

    public static final DefUseLibrarySignature THROWS_SQL_EXN = new DefUseLibrarySignature(
            EMPTY, EMPTY, List(DATABASE), List(DATABASE), false, false);

    public static final DefUseLibrarySignature NEW_THROWS_SQL_EXN = new DefUseLibrarySignature(
            EMPTY, EMPTY, List(DATABASE), List(DATABASE), false, true);

    public static final DefUseLibrarySignature NOOP = new DefUseLibrarySignature(
            EMPTY, EMPTY, false, false);

    // public static final DefUseLibrarySignature USE_DEF_HIDDEN = new
    // DefUseLibrarySignature(
    // List(HIDDEN), List(HIDDEN), EMPTY, false, false);
    //
    // public static final DefUseLibrarySignature DEF_HIDDEN = new
    // DefUseLibrarySignature(
    // List(HIDDEN), EMPTY, EMPTY, false, false);
    //
    // public static final DefUseLibrarySignature USES_CLOCK = new
    // DefUseLibrarySignature(
    // EMPTY, EMPTY, List(CLOCK), List(CLOCK), EMPTY, false, false);

    public static final PGLibrarySignature JDBC_NO_RT = new AlwaysAt(JDBC);
    public static final PGLibrarySignature JDBC_WITH_RT = new DBCommunication(
            JDBC);
    public static final PGLibrarySignature AT_SERVER = new AlwaysAt(
            DBLocation.DBSERVER);
    public static final PGLibrarySignature AT_CLIENT = new AlwaysAt(
            DBLocation.APPSERVER);
    public static final PGLibrarySignature AT_ANY = new AlwaysAt(PGLocation.ANY);
    private static final String CURRENT_THREAD = "$CURRENT_THREAD$";
    private static final String UNKNOWN_TYPE = "java.lang.Object";
    private static final String CHILDREN = "CHILDREN";
    private static final String ATTRIBUTES = "ATTRIBUTES";

    protected void addToStringSig(String fullClassName, String methodName,
            List<String> argTypes) {
        addSig(fullClassName, methodName, argTypes, mayAllocString(false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, JDBC_NO_RT);
    }

    protected void addConstructorSig(String fullClassName,
            List<String> argTypes, LibraryPointsToSignature sig1,
            LibrarySignature<VarContext<NotNullAbsVal>, NotNullAbsVal> sig2,
            /* PreciseExLibrarySignature sig3, */
            LibrarySignature<VarContext<DefUseAbsVal>, DefUseAbsVal> sig4,
            PGLibrarySignature sig5) {
        signatureRepository(PointerAnalysisPass.ANALYSIS_NAME, true)
                .addSignatureForConstructor(fullClassName, argTypes, sig1);
        signatureRepository(NotNullAnalysisFactory.ANALYSIS_NAME, true)
                .addSignatureForConstructor(fullClassName, argTypes, sig2);
        // signatureRepository(PreciseExAnalysisFactory.ANALYSIS_NAME, true)
        // .addSignatureForConstructor(fullClassName, argTypes, sig3);
        signatureRepository(DefUseAnalysisFactory.ANALYSIS_NAME, true)
                .addSignatureForConstructor(fullClassName, argTypes, sig4);
        signatureRepository(PartitionGraphBuilder.ANALYSIS_NAME, true)
                .addSignatureForConstructor(fullClassName, argTypes, sig5);
    }

    protected void addSig(String fullClassName, String methodName,
            List<String> argTypes, LibraryPointsToSignature sig1,
            LibrarySignature<VarContext<NotNullAbsVal>, NotNullAbsVal> sig2,
            /* PreciseExLibrarySignature sig3, */
            LibrarySignature<VarContext<DefUseAbsVal>, DefUseAbsVal> sig4,
            PGLibrarySignature sig5) {
        signatureRepository(PointerAnalysisPass.ANALYSIS_NAME, true)
                .addSignature(fullClassName, methodName, argTypes, sig1);
        signatureRepository(NotNullAnalysisFactory.ANALYSIS_NAME, true)
                .addSignature(fullClassName, methodName, argTypes, sig2);
        // signatureRepository(PreciseExAnalysisFactory.ANALYSIS_NAME, true)
        // .addSignature(fullClassName, methodName, argTypes, sig3);
        signatureRepository(DefUseAnalysisFactory.ANALYSIS_NAME, true)
                .addSignature(fullClassName, methodName, argTypes, sig4);
        signatureRepository(PartitionGraphBuilder.ANALYSIS_NAME, true)
                .addSignature(fullClassName, methodName, argTypes, sig5);

    }

    // protected void addEJBCreateSignatures(String[] beans,
    // List<String> argTypes, String pkg, String localHomeSuffix,
    // String localSuffix, String beanSuffix) {
    // for (String bean : beans) {
    // LibraryPointsToSignature pointsToSig = new EJBAllocateBean(pkg
    // + bean + localSuffix, pkg + bean + beanSuffix);
    // String localTypeName = pkg + bean + localSuffix;
    // String beanTypeName = pkg + bean + beanSuffix;
    // addSig(pkg + bean + localHomeSuffix, "create", argTypes,
    // pointsToSig, EJBDispatchAdapter.create(
    // NotNullAnalysisUtil.NOOP_SIG, localTypeName,
    // beanTypeName, "ejbCreate", true, true),
    // EJBDispatchAdapter.create(DefUseLibrarySignature.NOOP,
    // localTypeName, beanTypeName, "ejbCreate", true,
    // true), AT_ANY);
    // }
    // }

    /**
     * Add a signature for the create() method on an EJB LocalHome interface
     */
    // public void addEJBCreateSignature(String localHomeTypeName,
    // List<String> argTypes, String localTypeName, String beanTypeName) {
    // LibraryPointsToSignature pointsToSig = new EJBAllocateBean(
    // localTypeName, beanTypeName);
    // addSig(localHomeTypeName, "create", argTypes, pointsToSig,
    // EJBDispatchAdapter.create(NotNullAnalysisUtil.NOOP_SIG,
    // localTypeName, beanTypeName, "ejbCreate", true, true),
    // EJBDispatchAdapter.create(DEF_USE_DATABASE, localTypeName,
    // beanTypeName, "ejbCreate", true, true), AT_ANY);
    // }
    //
    // public void addDAOSignature(BeanInfo bean, String methodName,
    // List<String> argTypeNames) {
    // LibraryPointsToSignature pointsToSig = new DAOFetchBean(bean.local,
    // bean.bean);
    // addSig(bean.localHome, methodName, argTypeNames, pointsToSig,
    // EJBDispatchAdapter.create(NotNullAnalysisUtil.NOOP_SIG,
    // bean.local, bean.bean,
    // DAOFetchBean.toEJBMethod(methodName), true, false),
    // EJBDispatchAdapter.create(USE_DATABASE, bean.local, bean.bean,
    // DAOFetchBean.toEJBMethod(methodName), false, false),
    // AT_ANY);
    // }

    /**
     * Add a signature for a method on an EJB Local interface
     */
    // public void addEJBLocalSignature(BeanInfo bean, String methodName,
    // List<String> argTypes) {
    // LibraryPointsToSignature pointsToSig = new LocalFetchBean(bean.local,
    // bean.bean);
    // addSig(bean.local, methodName, argTypes, pointsToSig,
    // EJBDispatchAdapter.create(NotNullAnalysisUtil.NOOP_SIG,
    // bean.local, bean.bean, methodName, false, false),
    // EJBDispatchAdapter.create(DEF_USE_DATABASE, bean.local,
    // bean.bean, methodName, false, false), AT_ANY);
    // }
    //
    // private static final
    // EJBDispatchAdapter<VarContext<NotNullAbsVal>,NotNullAbsVal> EJBNotNull =
    // new EJBDispatchAdapter<VarContext<NotNullAbsVal>, NotNullAbsVal>();
    //
    // private static final EJBDispatchAdapter<VarContext<DefUseAbsVal>,
    // DefUseAbsVal> EJBDefUse =
    // new EJBDispatchAdapter<VarContext<DefUseAbsVal>, DefUseAbsVal>();
    /**
     * Add a signature for a method on an EJB Local interface
     */
    // public void addEJBLocalSignature(
    // String localTypeName,
    // String localMethodName,
    // List<String> argTypes) {
    // addSig(localTypeName, localMethodName, argTypes,
    // PointsToLibrarySigs.NOOP, EJBNotNull, EJBDefUse, AT_ANY);
    // }

    protected LibraryPointsToSignature arrayAndContents(String baseType) {
        Map<String, String> array = new HashMap<String, String>(1);
        array.put(StmtRegistrar.ARRAY_CONTENTS, baseType);
        return new SolAllocatorOrReceiver(baseType + "[]", false,
                Collections.<String> emptySet(), array.keySet(), array);
    }

    protected LibraryPointsToSignature mapAndContents(String keyType) {
        Map<String, String> map = new HashMap<String, String>(2);
        map.put(KEYS, "java.lang.String");
        map.put(VALUES, "java.lang.String[]");
        return new SolAllocatorOrReceiver("java.util.Map", false,
                Collections.<String> emptySet(), map.keySet(), map);
    }

    protected LibraryPointsToSignature collectionAndContents(
            String collectionType, String contentsType) {
        Map<String, String> map = new HashMap<String, String>(2);
        map.put(CONTENTS, contentsType);
        return new SolAllocatorOrReceiver(collectionType, false,
                Collections.<String> emptySet(), map.keySet(), map);
    }

    protected LibraryPointsToSignature iterator(String typeName) {
        return allocateAndAssign("java.util.Iterator", UNKNOWN_TYPE, CONTENTS,
                CONTENTS);
    }

    protected LibraryPointsToSignature allocateAndAssign(String collectionType,
            String contentsType, String fromFieldName, String toFieldName) {
        Map<String, String> fieldTypes = new HashMap<String, String>(2);
        fieldTypes.put(toFieldName, contentsType);
        fieldTypes.put(fromFieldName, contentsType);
        Map<String, String> assign = new HashMap<String, String>(1);
        assign.put(toFieldName, fromFieldName);
        return new SolAllocatorOrReceiver(collectionType, false,
                Collections.<String> emptySet(),
                Collections.<String> emptySet(), fieldTypes, assign);
    }

    // protected LibraryPointsToSignature allocateAndAssign(String
    // collectionType,
    // String[] contentsTypes, String[] fromFieldNames, String[] toFieldNames) {
    // Map<String, String> fieldTypes = new HashMap<String,
    // String>(contentsTypes.length * 2);
    // for (int i = 0; i < contentsTypes.length; i++) {
    // String contentsType = contentsTypes[i];
    // String fromFieldName = fromFieldNames[i];
    // String toFieldName = toFieldNames[i];
    //
    // fieldTypes.put(toFieldName, contentsType);
    // fieldTypes.put(fromFieldName, contentsType);
    // Map<String, String> assign = new HashMap<String, String>(1);
    // assign.put(toFieldName, fromFieldName);
    // return new SolAllocatorOrReceiver(collectionType, false,
    // Collections.<String> emptySet(),
    // Collections.<String> emptySet(), fieldTypes, assign);
    // }
    // }

    // A null argument list indicates all overloaded methods
    private static List<String> ALL = null;
    /* **********************************************
     * java.sql package signatures
     */
    {
        // java.sql.DriverManager
        addSig("java.sql.DriverManager", "getConnection", ALL,
                new SolAllocatorOrReceiver("java.sql.Connection", false,
                        Set(DATABASE)),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, THROWS_SQL_EXN,
                JDBC_WITH_RT);

        // java.sql.Connection
        addSig("java.sql.Connection", "createStatement", EMPTY,
                new SolAllocatorOrReceiver("java.sql.Statement", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, THROWS_SQL_EXN,
                JDBC_NO_RT);

        addSig("java.sql.Connection",
                "prepareStatement",
                ALL,
                new SolAllocatorOrReceiver("java.sql.PreparedStatement", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, THROWS_SQL_EXN,
                JDBC_NO_RT);

        addSig("java.sql.Connection", "commit", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_USE_DATABASE,
                JDBC_WITH_RT);

        addSig("java.sql.Connection", "setAutoCommit", List("boolean"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_USE_DATABASE,
                JDBC_NO_RT);

        addSig("java.sql.Connection", "close", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_USE_DATABASE,
                JDBC_NO_RT);

        addSig("java.sql.Connection", "rollback", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_USE_DATABASE,
                JDBC_WITH_RT);

        // java.sql.Statement
        addSig("java.sql.Statement", "executeQuery", List("java.lang.String"),
                new SolAllocatorOrReceiver("java.sql.ResultSet", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, USE_DATABASE,
                JDBC_WITH_RT);

        addSig("java.sql.Statement", "executeUpdate", List("java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_USE_DATABASE,
                JDBC_WITH_RT);

        addSig("java.sql.Statement", "executeBatch", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_USE_DATABASE,
                JDBC_WITH_RT);

        addSig("java.sql.Statement", "clearBatch", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);

        addSig("java.sql.Statement", "setFetchSize", List("int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);

        addSig("java.sql.Statement", "close", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_USE_DATABASE,
                JDBC_NO_RT);

        // java.sql.PreparedStatement
        addSig("java.sql.PreparedStatement", "executeQuery", EMPTY,
                new SolAllocatorOrReceiver("java.sql.ResultSet", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new Join(
                        USE_DATABASE, USE_SQL_PARAM), JDBC_WITH_RT);

        addSig("java.sql.PreparedStatement", "executeUpdate", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new Join(
                        DEF_USE_DATABASE, USE_SQL_PARAM), JDBC_WITH_RT);

        // addSig("java.sql.PreparedStatement", "executeBatch",
        // EMPTY,
        // PointerAnalysisPass.ANALYSIS_NAME, PointsToLibrarySigs.NOOP,
        // NotNullAnalysisFactory.ANALYSIS_NAME,
        // NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
        // PreciseExAnalysisFactory.ANALYSIS_NAME,
        // PreciseExLibrarySignature.ExactExceptions("java.sql.SQLException"));
        // addSig("java.sql.PreparedStatement", "executeBatch",
        // EMPTY,
        // DefUseAnalysisFactory.ANALYSIS_NAME, DEF_USE_DATABASE,
        // PartitionGraphBuilderFactory.ANALYSIS_NAME, WITH_JDBC);

        addSig("java.sql.PreparedStatement", "setBoolean", ALL,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);
        addSig("java.sql.PreparedStatement", "setBytes", ALL,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);
        addSig("java.sql.PreparedStatement", "setNull", ALL,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);
        addSig("java.sql.PreparedStatement", "setInt", List("int", "int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);

        addSig("java.sql.PreparedStatement", "setFloat", List("int", "float"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);
        addSig("java.sql.PreparedStatement", "setDouble",
                List("int", "double"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);
        addSig("java.sql.PreparedStatement", "setLong", List("int", "long"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);

        addSig("java.sql.PreparedStatement", "setString",
                List("int", "java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);
        addSig("java.sql.PreparedStatement", "setDate",
                List("int", "java.sql.Date"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);

        addSig("java.sql.PreparedStatement", "addBatch", EMPTY,
                new SolAllocatorOrReceiver("java.sql.ResultSet", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);

        addSig("java.sql.PreparedStatement", "setTimestamp",
                List("int", "java.sql.Timestamp"), new SolAllocatorOrReceiver(
                        "java.sql.ResultSet", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, DEF_SQL_PARAM,
                JDBC_NO_RT);

        // java.sql.ResultSet
        addSig("java.sql.ResultSet", "next", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, INCREMENT_CURSOR,
                JDBC_WITH_RT);

        addSig("java.sql.ResultSet", "getString", ALL,
                mayAllocString( false),
                NotNullAnalysisUtil.NOOP_MAYBENULL_RESULT_SIG, USE_CURSOR,
                JDBC_NO_RT);
        addSig("java.sql.ResultSet", "getDate", ALL,
                new SolAllocatorOrReceiver("java.sql.Date", false),
                NotNullAnalysisUtil.NOOP_MAYBENULL_RESULT_SIG, USE_CURSOR,
                JDBC_NO_RT);
        addSig("java.sql.ResultSet", "getTimestamp", ALL,
                new SolAllocatorOrReceiver("java.sql.Timestamp", false),
                NotNullAnalysisUtil.NOOP_MAYBENULL_RESULT_SIG, USE_CURSOR,
                JDBC_NO_RT);

        addSig("java.sql.ResultSet", "getBoolean", ALL,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, USE_CURSOR,
                JDBC_NO_RT);
        addSig("java.sql.ResultSet", "getInt", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, USE_CURSOR,
                JDBC_NO_RT);
        addSig("java.sql.ResultSet", "getLong", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, USE_CURSOR,
                JDBC_NO_RT);

        addSig("java.sql.ResultSet", "getFloat", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, USE_CURSOR,
                JDBC_NO_RT);

        addSig("java.sql.ResultSet", "getDouble", ALL,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, USE_CURSOR,
                JDBC_NO_RT);

        addSig("java.sql.ResultSet", "close", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, INCREMENT_CURSOR,
                JDBC_NO_RT);

        // java.sql.SQLException
        addConstructorSig("java.sql.SQLException", EMPTY,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, JDBC_NO_RT);

        // java.sql.Timestamp
        addConstructorSig("java.sql.Timestamp", List("long"),
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, JDBC_NO_RT);
        addToStringSig("java.sql.Timestamp", "toString", EMPTY);

        addSig("java.sql.Timestamp", "after", List("java.sql.Timestamp"),
                mayAllocString(false),
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                JDBC_NO_RT);
        addSig("java.sql.Timestamp", "before", List("java.sql.Timestamp"),
                mayAllocString( false),
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                JDBC_NO_RT);

        addConstructorSig("java.sql.Date", List("long"),
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.lang.NullPointerException
        addConstructorSig("java.lang.NullPointerException", EMPTY,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.lang.IllegalStateException
        addConstructorSig("java.lang.IllegalStateException", ALL,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.io.IOException
        addConstructorSig("java.io.IOException", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                AT_ANY);

        // java.io.FileNotFoundException
        addConstructorSig("java.io.FileNotFoundException", ALL,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.lang.reflect.UndeclaredThrowableException
        addConstructorSig("java.lang.reflect.UndeclaredThrowableException",
                ALL, PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.lang.Throwable
        addSig("java.lang.Throwable", "getMessage", EMPTY,
                mayAllocString( false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.Throwable", "initCause", List("java.lang.Throwable"),
                new SolAllocatorOrReceiver("java.lang.Throwable", true),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.net.URI
        addConstructorSig("java.net.URI", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                AT_ANY);
        addSig("java.net.URI", "getSchemeSpecificPart", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.net.URI", "getHost", EMPTY, new SolAllocatorOrReceiver(
                "java.lang.String", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.net.URI", "getPath", EMPTY, new SolAllocatorOrReceiver(
                "java.lang.String", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.net.URL
        addConstructorSig("java.net.URL", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                AT_ANY);
        addSig("java.net.URL", "getProtocol", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.net.URL", "getFile", EMPTY, new SolAllocatorOrReceiver(
                "java.lang.String", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addToStringSig("java.net.URL", "toString", EMPTY);

        addSig("java.net.InetAddress", "getByAddress", List("byte[]"),
                new SolAllocatorOrReceiver("java.net.InetAddress", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.net.InetAddress", "getByName", List("java.lang.String"),
                new SolAllocatorOrReceiver("java.net.InetAddress", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.net.InetAddress", "equals", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.util.Date
        addConstructorSig("java.util.Date", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, new Join(new DEF_HIDDEN("time"),
                        DefUseLibrarySignature.USES_CLOCK), AT_ANY);
        addConstructorSig("java.util.Date", List("long"),
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                new DEF_HIDDEN("time"), AT_ANY);
        addConstructorSig("java.util.Date", List("java.lang.String"),
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                new DEF_HIDDEN("time"), AT_ANY);
        addSig("java.util.Date", "getTime", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, new USE_HIDDEN("time"), AT_ANY);
        addSig("java.util.Date", "before", List("java.util.Date"),
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Arrays", "asList", null, new SolAllocatorOrReceiver(
                "java.util.List", false), NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.text.SimpleDateFormat
        addConstructorSig("java.text.SimpleDateFormat",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, new DEF_HIDDEN("HERE"), AT_ANY);

        // java.text.DateFormat
        addSig("java.text.DateFormat", "format", List("java.util.Date"),
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        "HERE"), AT_ANY);

        // java.util.Properties
        addConstructorSig("java.util.Properties", ALL,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Properties", "setProperty",
                List("java.lang.String", "java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);
        addSig("java.util.Properties", "getProperty",
                List("java.lang.String", "java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        addConstructor("java.lang.UnsupportedOperationException", ALL,
                PartitionGraphBuilder.ANALYSIS_NAME, AT_ANY);

        // java.lang.String
        addConstructorSig("java.lang.String", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                AT_ANY);

        addSig("java.lang.String", "indexOf", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.String", "isEmpty", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.String", "endsWith", List("java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.String", "matches", List("java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.String", "replaceAll",
                List("java.lang.String", "java.lang.String"),
                mayAllocString(  true),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.String", "valueOf", ALL, new SolAllocatorOrReceiver(
                "java.lang.String", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.String", "trim", EMPTY, new SolAllocatorOrReceiver(
                "java.lang.String", true),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.String", "toLowerCase", EMPTY,
                mayAllocString(  true),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.String", "replace", ALL, new SolAllocatorOrReceiver(
                "java.lang.String", true),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.String", "concat", List("java.lang.String"),
                mayAllocString(  true),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.String", "equalsIgnoreCase",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // XXX: Need something to create objects for contents of string array
        addSig("java.lang.String", "split", ALL,
                arrayAndContents("java.lang.String"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.lang.Double
        addToStringSig("java.lang.Double", "toString", EMPTY);

        addToStringSig("java.lang.Double", "toString", List("double"));

        // java.lang.Thread
        // addSig("java.lang.Thread", "currentThread", EMPTY,
        // new PointsToLibrarySigs.Singleton(CURRENT_THREAD,
        // "java.lang.Thread", false),
        // NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
        // DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.Thread", "getContextClassLoader", EMPTY,
                new PointsToLibrarySigs.Singleton(CURRENT_THREAD,
                        "java.lang.ClassLoader", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.lang.StringBuilder
        addConstructorSig("java.lang.StringBuilder", EMPTY,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                new USE_DEF_HIDDEN(BUFFER), AT_ANY);

        addConstructorSig("java.lang.StringBuilder", List("java.lang.String"),
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                new USE_DEF_HIDDEN(BUFFER), AT_ANY);

        addSig("java.lang.StringBuilder", "append", ALL,
                new SolAllocatorOrReceiver("java.lang.StringBuilder", true),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(BUFFER), AT_ANY);

        addSig("java.lang.StringBuilder", "toString", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        BUFFER), AT_ANY);

        // java.lang.StringBuffer
        addSig("java.lang.StringBuffer", "toString", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(BUFFER), AT_ANY);
        addConstructorSig("java.lang.StringBuffer", ALL,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                new USE_DEF_HIDDEN(BUFFER), AT_ANY);
        addSig("java.lang.StringBuffer", "append", ALL,
                new SolAllocatorOrReceiver("java.lang.StringBuffer", true),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(BUFFER), AT_ANY);

        // java.io.StringWriter
        addSig("java.io.StringWriter", "getBuffer", EMPTY, new SolReturnsField(
                BUFFER, "java.lang.StringBuffer"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(BUFFER), AT_ANY);

        addSig("java.io.StringWriter", "toString", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(BUFFER), AT_ANY);

        addConstructorSig("java.security.SecureRandom", ALL,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addConstructorSig("java.util.Random", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                AT_ANY);
        addSig("java.util.Random", "nextFloat", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN("random"), AT_ANY);
        addSig("java.util.Random", "nextDouble", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN("random"), AT_ANY);

        addSig("java.lang.Math", "random", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, NOOP, AT_ANY);

        addSig("java.lang.Math", "floor", List("double"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, NOOP, AT_ANY);

        addConstructorSig("java.lang.Long", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                AT_ANY);
        addSig("java.lang.Long", "intValue", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, NOOP, AT_ANY);
        addSig("java.lang.Long", "valueOf", ALL, new AllocatorOrReceiver(
                "java.lang.Long", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, NOOP, AT_ANY);
        addToStringSig("java.lang.Long", "toString", EMPTY);

        addSig("java.lang.Long", "longValue", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, NOOP, AT_ANY);

        addConstructorSig("java.lang.Double", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                AT_ANY);

        addSig("java.lang.Double", "intValue", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, NOOP, AT_ANY);
        addSig("java.lang.Double", "valueOf", ALL, new AllocatorOrReceiver(
                "java.lang.Double", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, NOOP, AT_ANY);
        addSig("java.lang.Double", "doubleValue", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, NOOP, AT_ANY);

        addSig("java.util.Collection", "iterator", EMPTY,
                iterator("java.util.Iterator"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);
        addSig("java.util.Collection", "size", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        addSig("java.util.AbstractSequentialList", "iterator", ALL,
                iterator("java.util.Iterator"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        addSig("java.util.AbstractCollection", "isEmpty", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        addConstructorSig("java.util.Vector", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, new DEF_HIDDEN(CONTENTS), AT_ANY);

        addSig("java.util.Vector", "removeAllElements", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);
        addSig("java.util.Vector", "addElement", List("java.lang.Object"),
                new SolAssignsArgToField(CONTENTS, UNKNOWN_TYPE, 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);
        addSig("java.util.Vector", "size", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);
        addSig("java.util.Vector", "elementAt", List("int"),
                new SolReturnsField(CONTENTS, UNKNOWN_TYPE),
                NotNullAnalysisUtil.NOOP_MAYBENULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);
        addSig("java.util.Vector", "contains", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_MAYBENULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);
        addSig("java.util.Vector", "add", List("java.lang.Object"),
                new SolAssignsArgToField(CONTENTS, UNKNOWN_TYPE, 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);
        addSig("java.util.Vector", "get", List("int"), new SolReturnsField(
                CONTENTS, UNKNOWN_TYPE),
                NotNullAnalysisUtil.NOOP_MAYBENULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        addSig("java.util.Stack", "push", List("java.lang.Object"),
                new AssignsAndReturnsArg(CONTENTS, UNKNOWN_TYPE, 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);

        addSig("java.util.Stack", "pop", EMPTY, new SolReturnsField(CONTENTS,
                UNKNOWN_TYPE), NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);

        // java.util.LinkedList
        addConstructorSig("java.util.LinkedList", EMPTY,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                new DEF_HIDDEN(CONTENTS), AT_ANY);
        addSig("java.util.LinkedList", "clear", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);
        addSig("java.util.LinkedList", "add", ALL, new SolAssignsArgToField(
                CONTENTS, UNKNOWN_TYPE, 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);
        addSig("java.util.LinkedList", "size", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);
        addSig("java.util.LinkedList", "get", List("int"), new SolReturnsField(
                CONTENTS, UNKNOWN_TYPE),
                NotNullAnalysisUtil.NOOP_MAYBENULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);
        addSig("java.util.LinkedList", "addAll", List("java.util.Collection"),
                new AssignsFieldOfArgToField(CONTENTS, UNKNOWN_TYPE, 0,
                        CONTENTS), NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_USEARG_HIDDEN(CONTENTS, 1, 0, CONTENTS), AT_ANY);

        addSig("java.util.LinkedList",
                "toArray",
                List("java.lang.Object[]"),
                allocateAndAssign("java.lang.Object[]", UNKNOWN_TYPE, CONTENTS,
                        CONTENTS), NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_HIDDEN(CONTENTS), AT_ANY);

        addSig("java.util.LinkedList", "getFirst", EMPTY, new SolReturnsField(
                CONTENTS, "java.lang.Object"),
                NotNullAnalysisUtil.NOOP_MAYBENULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);
        addSig("java.util.LinkedList", "removeFirst", EMPTY,
                new SolReturnsField(CONTENTS, "java.lang.Object"),
                NotNullAnalysisUtil.NOOP_MAYBENULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);

        addSig("java.util.LinkedList", "removeLast", EMPTY,
                new SolReturnsField(CONTENTS, "java.lang.Object"),
                NotNullAnalysisUtil.NOOP_MAYBENULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);

        addSig("java.util.LinkedList", "addFirst", List("java.lang.Object"),
                new SolAssignsArgToField(CONTENTS, "java.lang.Object", 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);
        addSig("java.util.LinkedList", "addLast", List("java.lang.Object"),
                new SolAssignsArgToField(CONTENTS, "java.lang.Object", 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);

        // java.util.ArrayList
        addConstructorSig("java.util.ArrayList", EMPTY,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                new DEF_HIDDEN(CONTENTS), AT_ANY);
        addSig("java.util.ArrayList", "clear", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);
        addSig("java.util.ArrayList", "add", List("java.lang.Object"),
                new SolAssignsArgToField(CONTENTS, "java.lang.Object", 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);
        addSig("java.util.ArrayList", "size", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);
        addSig("java.util.ArrayList", "get", List("int"), new SolReturnsField(
                CONTENTS, UNKNOWN_TYPE),
                NotNullAnalysisUtil.NOOP_MAYBENULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);
        addSig("java.util.ArrayList", "addAll", List("java.util.Collection"),
                new AssignsFieldOfArgToField(CONTENTS, UNKNOWN_TYPE, 0,
                        CONTENTS), NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_USEARG_HIDDEN(CONTENTS, 1, 0, CONTENTS), AT_ANY);
        addSig("java.util.ArrayList", "contains", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        addSig("java.util.ArrayList",
                "toArray",
                ALL,
                allocateAndAssign("java.lang.Object[]", UNKNOWN_TYPE, CONTENTS,
                        CONTENTS), NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_HIDDEN(CONTENTS), AT_ANY);

        addConstructorSig("java.util.HashSet", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, new DEF_HIDDEN(CONTENTS), AT_ANY);

        addConstructorSig("java.util.LinkedHashSet", EMPTY,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                new DEF_HIDDEN(CONTENTS), AT_ANY);

        addSig("java.util.HashSet", "add", List("java.lang.Object"),
                new SolAssignsArgToField(CONTENTS, UNKNOWN_TYPE, 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);

        addSig("java.util.HashSet", "size", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        addSig("java.util.HashSet", "iterator", EMPTY,
                iterator("java.util.Iterator"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        addSig("java.util.HashSet", "contains", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        // java.util.TreeSet
        addConstructorSig("java.util.TreeSet", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, new DEF_HIDDEN(CONTENTS), AT_ANY);

        addSig("java.util.TreeSet", "add", List("java.lang.Object"),
                new SolAssignsArgToField(CONTENTS, UNKNOWN_TYPE, 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);

        addSig("java.util.TreeSet", "size", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        addSig("java.util.TreeSet", "iterator", EMPTY,
                iterator("java.util.Iterator"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        // java.util.Iterator
        addSig("java.util.Iterator", "hasNext", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        addSig("java.util.Iterator", "next", EMPTY, new SolReturnsField(
                CONTENTS, UNKNOWN_TYPE),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);

        addSig("java.util.Set", "iterator", EMPTY,
                iterator("java.util.Iterator"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Set", "size", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        // java.util.Map
        addSig("java.util.Map",
                "keySet",
                EMPTY,
                allocateAndAssign("java.util.Set", UNKNOWN_TYPE, KEYS, CONTENTS),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        KEYS), AT_ANY);
        addSig("java.util.Hashtable",
                "keySet",
                EMPTY,
                allocateAndAssign("java.util.Set", UNKNOWN_TYPE, KEYS, CONTENTS),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        KEYS), AT_ANY);
        addSig("java.util.Hashtable",
                "keys",
                EMPTY,
                allocateAndAssign("java.util.Enumeration", UNKNOWN_TYPE, KEYS,
                        CONTENTS), NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_HIDDEN(KEYS), AT_ANY);

        // java.util.HashMap
        addConstructorSig("java.util.HashMap", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, new DEF_HIDDEN(KEYS, VALUES),
                AT_ANY);
        addSig("java.util.HashMap",
                "keySet",
                EMPTY,
                allocateAndAssign("java.util.Set", UNKNOWN_TYPE, KEYS, CONTENTS),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        KEYS), AT_ANY);
        addSig("java.util.HashMap",
                "values",
                EMPTY,
                allocateAndAssign("java.util.Collections", UNKNOWN_TYPE,
                        VALUES, CONTENTS),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        VALUES), AT_ANY);
        addSig("java.util.HashMap", "remove", List("java.lang.Object"),
                new SolReturnsField(VALUES, "java.lang.Object"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new DEF_HIDDEN(
                        KEYS, VALUES), AT_ANY);
        addSig("java.util.HashMap", "containsKey", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        KEYS), AT_ANY);

        Map<Integer, String> key_value_fields = new HashMap<Integer, String>();
        key_value_fields.put(0, KEYS);
        key_value_fields.put(1, VALUES);
        Map<String, String> key_value_types = new HashMap<String, String>();
        key_value_types.put(KEYS, UNKNOWN_TYPE);
        key_value_types.put(VALUES, UNKNOWN_TYPE);

        addSig("java.util.HashMap", "put",
                List("java.lang.Object", "java.lang.Object"),
                new AssignsArgsToFields(key_value_fields, key_value_types),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new DEF_HIDDEN(
                        KEYS, VALUES), AT_ANY);

        addSig("java.util.Map", "get", List("java.lang.Object"),
                new SolReturnsField(VALUES, "java.lang.Object"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        KEYS, VALUES), AT_ANY);

        addSig("java.util.HashMap", "get", List("java.lang.Object"),
                new SolReturnsField(VALUES, "java.lang.Object"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        KEYS, VALUES), AT_ANY);

        addSig("java.util.TreeMap", "get", List("java.lang.Object"),
                new SolReturnsField(VALUES, "java.lang.Object"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.TreeMap", "put",
                List("java.lang.Object", "java.lang.Object"),
                new AssignsArgsToFields(key_value_fields, key_value_types),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new DEF_HIDDEN(
                        KEYS, VALUES), AT_ANY);
        addSig("java.util.TreeMap",
                "keySet",
                EMPTY,
                allocateAndAssign("java.util.Set", UNKNOWN_TYPE, KEYS, CONTENTS),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        KEYS), AT_ANY);

        // java.lang.Integer
        addConstructorSig("java.lang.Integer", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                AT_ANY);
        addSig("java.lang.Integer", "intValue", EMPTY,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.Long", "parseLong", List("java.lang.String"),
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.lang.System
        addSig("java.lang.System", "currentTimeMillis", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.USES_CLOCK, AT_ANY);

        addSig("java.lang.System", "getProperty", List("java.lang.String"),
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.System",
                "arraycopy",
                List("java.lang.Object", "int", "java.lang.Object", "int",
                        "int"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.lang.Class
        addSig("java.lang.Class", "getName", EMPTY, new SolAllocatorOrReceiver(
                "java.lang.String", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.Class", "getClassLoader", EMPTY,
                new SolAllocatorOrReceiver("java.lang.ClassLoader", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // addSig("java.lang.Object", "getClass", EMPTY,
        // new SolAllocatorOrReceiver("java.lang.Class", false),
        // NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
        // DefUseLibrarySignature.NOOP, AT_ANY);

        // java.lang.ClassLoader
        addSig("java.lang.ClassLoader", "getResource",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "java.net.URL", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.ClassLoader", "getResources",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "java.util.Enumeration", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.ClassLoader", "getSystemResources",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "java.util.Enumeration", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.util.Enumeration
        addSig("java.util.Enumeration", "hasMoreElements", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);
        addSig("java.util.Enumeration", "nextElement", EMPTY,
                new SolAllocatorOrReceiver(UNKNOWN_TYPE, false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CONTENTS), AT_ANY);

        // TPCW signatures
        addSig("tpcw.servlets.ConnectionPool", "getConnection", ALL,
                new SolAllocatorOrReceiver("java.sql.Connection", false,
                        Set(DATABASE)),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                NEW_THROWS_SQL_EXN, JDBC_WITH_RT);
        addSig("tpcw.servlets.ConnectionPool", "returnConnection",
                List("java.sql.Connection"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, THROWS_SQL_EXN,
                JDBC_NO_RT);

        // MyEnum signatures
        addToStringSig("solomon.MyEnum", "toString", EMPTY);

        // Enum signatures
        addToStringSig("java.lang.Enum", "toString", EMPTY);
        addSig("java.lang.Enum", "equals", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // java.util.Hashtable
        addConstructorSig("java.util.Hashtable", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, new DEF_HIDDEN(KEYS, VALUES),
                AT_ANY);

        addToStringSig("java.util.Hashtable", "toString", EMPTY);

        addSig("java.util.Hashtable", "put",
                List("java.lang.Object", "java.lang.Object"),
                new AssignsArgsToFields(key_value_fields, key_value_types),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new DEF_HIDDEN(
                        KEYS, VALUES), AT_ANY);

        addSig("java.util.Hashtable", "get", List("java.lang.Object"),
                new SolReturnsField(VALUES, UNKNOWN_TYPE),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        KEYS, VALUES), AT_ANY);

        addSig("java.util.Hashtable", "remove", List("java.lang.Object"),
                new SolReturnsField(VALUES, UNKNOWN_TYPE),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new DEF_HIDDEN(
                        KEYS, VALUES), AT_ANY);
        addSig("java.util.Hashtable", "clear", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new DEF_HIDDEN(
                        KEYS, VALUES), AT_ANY);

        Map<String, String> fields2fields = new HashMap<String, String>();
        fields2fields.put(KEYS, KEYS);
        fields2fields.put(VALUES, VALUES);
        addSig("java.util.Hashtable",
                "putAll",
                List("java.util.Map"),
                new AssignsFieldsOfArgToFields(0, fields2fields),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_USEARG_HIDDEN(List(KEYS, VALUES), 1, Collections
                        .singletonList(0), List(KEYS, VALUES)), AT_ANY);

        // java.io.File
        addConstructorSig("java.io.File", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                AT_ANY);
        addSig("java.io.File", "exists", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("java.io.FileInputStream", "read", List("byte[]"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new DEF_ARG(1, 0,
                        StmtRegistrar.ARRAY_CONTENTS), AT_CLIENT);

        addSig("java.io.InputStream", "read", List("byte[]", "int", "int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new DEF_ARG(1, 0,
                        StmtRegistrar.ARRAY_CONTENTS), AT_CLIENT);

        // javax.sql.DataSource
        addSig("javax.sql.DataSource", "getConnection", EMPTY,
                new SolAllocatorOrReceiver("java.sql.Connection", false,
                        Set(DATABASE)),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, JDBC_WITH_RT);

        // javax.naming.InitialContext
        addConstructorSig(
                "javax.naming.InitialContext",
                List("java.util.Hashtable"),
                new AssignsFieldsOfArgToFields(0, fields2fields),
                NotNullAnalysisUtil.NOOP_SIG,
                new USE_DEF_USEARG_HIDDEN(List(KEYS, VALUES), 1, Collections
                        .singletonList(0), List(KEYS, VALUES)), AT_ANY);
        //
        // addSig("javax.rmi.PortableRemoteObject", "narrow",
        // List("java.lang.Object", "java.lang.Class"),
        // PointerAnalysisPass.ANALYSIS_NAME, new DynamicAllocatorOrArgument(1,
        // 0),
        // NotNullAnalysisFactory.ANALYSIS_NAME, NotNullAnalysisUtil.NOOP_SIG,
        // PreciseExAnalysisFactory.ANALYSIS_NAME,
        // PreciseExLibrarySignature.NORM_TERMINATION_ONLY,
        // DefUseAnalysisFactory.ANALYSIS_NAME, DefUseLibrarySignature.NOOP,
        // PartitionGraphBuilder.ANALYSIS_NAME, AT_ANY);

        addSig("javax.xml.parsers.DocumentBuilderFactory", "newInstance",
                EMPTY, new SolAllocatorOrReceiver(
                        "javax.xml.parsers.DocumentBuilderFactory", false),
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                AT_ANY);
        addSig("javax.xml.parsers.DocumentBuilderFactory",
                "newDocumentBuilder", EMPTY, new SolAllocatorOrReceiver(
                        "javax.xml.parsers.DocumentBuilder", false),
                NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP,
                AT_ANY);

        // addSig("javax.ejb.EJBLocalHome", "remove", List("java.lang.Object"),
        // PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
        // DEF_USE_DATABASE, AT_ANY);
        //
        // addSig("javax.ejb.EJBLocalObject", "remove", EMPTY,
        // PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
        // DEF_USE_DATABASE, AT_ANY);
        //
        // addSig("javax.ejb.EJBLocalObject", "getPrimaryKey", EMPTY,
        // new SolAllocatorOrReceiver(UNKNOWN_TYPE, false),
        // NotNullAnalysisUtil.NOOP_SIG, DefUseLibrarySignature.NOOP, AT_ANY);
        //
        // String[] entityBeans = { "Announcement", "Answer", "AnswerSet",
        // "AssignmentFile", "AssignmentItem", "Assignment", "CMSAdmin",
        // "CategoryCol", "CategoryContents", "CategoryFile", "Category",
        // "CategoryRow", "Choice", "CommentFile", "Comment", "Course",
        // "Domain", "Email", "Grade", "GroupAssignedTo", "GroupGrade",
        // "Group", "GroupMember", "Log", "OldAnnouncement",
        // "RegradeRequest", "RequiredFileType", "RequiredSubmission",
        // "Semester", "SiteNotice", "SolutionFile", "Staff", "Student",
        // "SubProblem", "SubmittedFile", "TimeSlot", "User" };
        // String[] sessionBeans = { "Root", "Transactions" };
        //
        // for (String bean : entityBeans) {
        // String local = "edu.cornell.csuglab.cms.base." + bean + "Local";
        // String localHome = local + "Home";
        // String beanBMP = "edu.cornell.csuglab.cms.base." + bean + "BMP";
        // String beanDAO = "edu.cornell.csuglab.cms.base." + bean + "DAOImpl";
        // BeanInfo info = new BeanInfo(localHome, local, beanBMP, beanDAO);
        // beanMap.put(local, info);
        // beanMap.put(localHome, info);
        // }
        // for (String bean : sessionBeans) {
        // String local = "edu.cornell.csuglab.cms.base." + bean + "Local";
        // String localHome = local + "Home";
        // String beanSession = "edu.cornell.csuglab.cms.base." + bean
        // + "Session";
        // String beanDAO = "edu.cornell.csuglab.cms.base." + bean + "DAOImpl";
        // BeanInfo info = new BeanInfo(localHome, local, beanSession, beanDAO);
        // beanMap.put(local, info);
        // beanMap.put(localHome, info);
        // }
        //
        // // Util signatures
        // for (String bean : entityBeans) {
        // String localHome = "edu.cornell.csuglab.cms.base." + bean
        // + "LocalHome";
        // addSig("edu.cornell.csuglab.cms.base." + bean + "Util",
        // "getLocalHome", EMPTY, new EJBSingleton(localHome,
        // localHome),
        // NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
        // DefUseLibrarySignature.NOOP, AT_ANY);
        // }
        // for (String bean : sessionBeans) {
        // String localHome = "edu.cornell.csuglab.cms.base." + bean
        // + "LocalHome";
        // addSig("edu.cornell.csuglab.cms.base." + bean + "Util",
        // "getLocalHome", EMPTY, new EJBSingleton(localHome,
        // localHome),
        // NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
        // DefUseLibrarySignature.NOOP, AT_ANY);
        // }

        // javax.servlet
        addConstructorSig("javax.servlet.http.HttpServlet", ALL,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addConstructorSig("javax.servlet.http.HttpServletRequestWrapper", ALL,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.ServletRequestWrapper", "getParameter",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "java.lang.String", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.http.HttpServletRequestWrapper", "getPathInfo",
                EMPTY, mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.http.HttpServletRequestWrapper",
                "getContextPath", EMPTY, new SolAllocatorOrReceiver(
                        "java.lang.String", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.http.HttpServletRequestWrapper",
                "getServletPath", EMPTY, new SolAllocatorOrReceiver(
                        "java.lang.String", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);
        addSig("javax.servlet.http.HttpServletRequestWrapper", "getServerName",
                EMPTY, mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);
        addSig("javax.servlet.ServletRequestWrapper", "getServerName", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.http.HttpServletRequestWrapper", "getSession",
                List("boolean"), new SolAllocatorOrReceiver(
                        "javax.servlet.http.HttpSession", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.ServletContext", "getRequestDispatcher",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "javax.servlet.RequestDispatcher", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.RequestDispatcher",
                "forward",
                List("javax.servlet.ServletRequest",
                        "javax.servlet.ServletResponse"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.http.HttpServletRequestWrapper", "getRequestURL",
                EMPTY, new SolAllocatorOrReceiver("java.lang.StringBuffer",
                        false), NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addConstructorSig("javax.servlet.http.HttpServletResponseWrapper", ALL,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.ServletResponseWrapper", "setCharacterEncoding",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.ServletResponseWrapper", "sendRedirect",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.http.HttpSession", "getAttribute",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        UNKNOWN_TYPE, false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("org.w3c.dom.Element", "getAttribute", List("java.lang.String"),
                new SolReturnsField(ATTRIBUTES, "java.lang.String"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("org.w3c.dom.Element", "setAttribute",
                List("java.lang.String", "java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.http.HttpSession", "removeAttribute",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        // not currently modeling contents of session attributes
        addSig("javax.servlet.http.HttpSession", "setAttribute",
                List("java.lang.String", "java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.xml.parsers.DocumentBuilder", "newDocument", EMPTY,
                new SolAllocatorOrReceiver("org.w3c.dom.Document", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.Thread", "getName", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // SUSPECT THE SIGNATURES BELOW: they are just NOOPs
        addConstructorSig("java.lang.Boolean", List("boolean"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addConstructorSig("javax.naming.directory.InitialDirContext",
                List("java.util.Hashtable"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Date", "compareTo", List("java.util.Date"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Random", "nextInt", List("int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.servlet.ServletRequestWrapper", "getContentLength",
                EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("java.lang.Throwable", "getStackTrace", EMPTY,
                arrayAndContents("java.lang.StackTraceElement"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.StackTraceElement", "getLineNumber", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.StackTraceElement", "getClassName", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.StackTraceElement", "getFileName", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.StackTraceElement", "getMethodName", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addToStringSig("java.util.AbstractCollection", "toString", EMPTY);

        addSig("javax.servlet.ServletRequestWrapper", "getLocalAddr", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.ServletRequestWrapper", "getRemoteAddr", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.http.HttpServletRequestWrapper", "getHeader",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "java.lang.String", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.http.HttpServletRequestWrapper",
                "getQueryString", EMPTY, new SolAllocatorOrReceiver(
                        "java.lang.String", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.http.HttpServletRequestWrapper", "getRequestURI",
                EMPTY, mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("java.text.DateFormat", "parse", List("java.lang.String"),
                new SolAllocatorOrReceiver("java.util.Date", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.servlet.ServletRequestWrapper", "getParameterNames",
                EMPTY, new SolAllocatorOrReceiver("java.util.Enumeration",
                        false), NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("java.util.AbstractList", "iterator", EMPTY,
                iterator("java.util.Iterator"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.AbstractList", "listIterator", EMPTY,
                iterator("java.util.ListIterator"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        Map<String, String> map = new HashMap<String, String>(2);
        map.put(KEYS, "java.lang.String");
        map.put(VALUES, "java.lang.String[]");
        addSig("javax.servlet.ServletRequestWrapper",
                "getParameterMap",
                EMPTY,
                new SolAllocatorOrReceiver("java.util.Map", false, Collections
                        .<String> emptySet(), map.keySet(), map),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);
        addSig("java.security.MessageDigest", "getInstance",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "java.security.MessageDigest", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.security.MessageDigest", "update", List("byte[]"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.security.MessageDigest", "digest", List("byte[]"),
                new SolAllocatorOrReceiver("byte[]", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.mail.internet.MimeMessage", "getAllRecipients", EMPTY,
                arrayAndContents("javax.mail.Address"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("com.Ostermiller.util.ExcelCSVParser", "getLine", EMPTY,
                arrayAndContents("java.lang.String"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // XXX: does this work?
        addSig("com.Ostermiller.util.ExcelCSVParser", "getAllValues", EMPTY,
                arrayAndContents("java.lang.String[]"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.servlet.ServletRequestWrapper", "getParameterValues",
                List("java.lang.String"), arrayAndContents("java.lang.String"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.naming.directory.InitialDirContext", "getAttributes",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "javax.naming.directory.Attributes", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.naming.directory.Attributes", "get",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "javax.naming.directory.Attribute", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.naming.directory.Attribute", "get", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        // javax.naming.InitialContext
        addConstructorSig("javax.naming.InitialContext", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.naming.directory.Attributes", "getAll", EMPTY,
                new SolAllocatorOrReceiver("javax.naming.NamingEnumeration",
                        false), NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.servlet.GenericServlet", "getServletContext", EMPTY,
                new SolAllocatorOrReceiver("javax.servlet.ServletContext",
                        false), NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.ServletRequestWrapper", "getInputStream", EMPTY,
                new SolAllocatorOrReceiver("javax.servlet.ServletInputStream",
                        false), NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.ServletResponseWrapper", "getOutputStream",
                EMPTY, new SolAllocatorOrReceiver(
                        "javax.servlet.ServletOutputStream", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("javax.servlet.http.HttpServletRequestWrapper", "getCookies",
                EMPTY, arrayAndContents("javax.servlet.http.Cookie"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_CLIENT);

        addSig("java.lang.Integer", "longValue", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.sql.Timestamp", "getTime", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.Character", "isLetter", List("char"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.Character", "toUpperCase", List("char"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.Math", "abs", List("float"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.Boolean", "valueOf", ALL, new SolAllocatorOrReceiver(
                "java.lang.Boolean", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.Boolean", "booleanValue", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.Float", "floatValue", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addToStringSig("java.lang.Boolean", "toString", List("boolean"));

        addToStringSig("java.lang.Float", "toString", List("float"));

        addToStringSig("java.lang.Long", "toString", List("long"));

        // java.util.Calendar
        addSig("java.util.Calendar", "getInstance", EMPTY,
                new SolAllocatorOrReceiver("java.util.Calendar", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.USES_CLOCK, AT_ANY);
        addSig("java.util.Calendar", "setTime", List("java.util.Date"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Calendar", "setTime", List("long"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Calendar", "get", List("int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Calendar", "getTimeInMillis", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Calendar", "set", List("int", "int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.regex.Pattern", "compile", List("java.lang.String"),
                new SolAllocatorOrReceiver("java.util.regex.Pattern", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.mail.Session", "getDefaultInstance",
                List("java.util.Properties", "javax.mail.Authenticator"),
                new SolAllocatorOrReceiver("javax.mail.Session", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.ByteArrayOutputStream", "toByteArray", EMPTY,
                arrayAndContents("byte"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.StringBuffer", "charAt", List("int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.BufferedInputStream", "read",
                List("byte[]", "int", "int"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.StringBuffer", "length", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.ByteArrayOutputStream", "toString", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.ByteArrayOutputStream", "toString",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "java.lang.String", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.StringBuffer", "deleteCharAt", List("int"),
                new SolAllocatorOrReceiver("java.lang.StringBuffer", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.StringBuffer", "replace",
                List("int", "int", "java.lang.String"),
                new SolAllocatorOrReceiver("java.lang.StringBuffer", true),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.BufferedOutputStream", "write",
                List("byte[]", "int", "int"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.ByteArrayOutputStream", "write",
                List("byte[]", "int", "int"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.ByteArrayOutputStream", "write", List("int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.BufferedInputStream", "close", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.io.InputStream", "close", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.ByteArrayOutputStream", "close", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.File", "deleteOnExit", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.FileInputStream", "close", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.FileOutputStream", "close", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.FileOutputStream", "write", List("byte[]"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.FileOutputStream", "write",
                List("byte[]", "int", "int"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.FilterOutputStream", "close", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.OutputStream", "flush", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.sql.Timestamp", "setTime", List("long"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.mail.Message", "addRecipient",
                List("javax.mail.Message.RecipientType", "javax.mail.Address"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.mail.internet.MimeMessage", "setFrom",
                List("javax.mail.Address"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.mail.internet.MimeMessage", "setReplyTo",
                List("javax.mail.Address[]"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.mail.internet.MimeMessage", "setSubject",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.mail.internet.MimeMessage", "setText",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.servlet.ServletResponseWrapper", "setContentType",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.servlet.http.HttpServletResponseWrapper", "sendRedirect",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.servlet.http.HttpServletResponseWrapper", "setHeader",
                List("java.lang.String", "java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("org.w3c.dom.Document", "createElement",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        UNKNOWN_TYPE, false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("org.w3c.dom.Document", "createElementNS",
                List("java.lang.String", "java.lang.String"),
                new SolAllocatorOrReceiver(UNKNOWN_TYPE, false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("org.w3c.dom.Node", "appendChild", List("org.w3c.dom.Node"),
                new AssignsAndReturnsArg(CHILDREN, "org.w3c.dom.Node", 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CHILDREN), AT_ANY);

        addSig("org.w3c.dom.Node", "removeChild", List("org.w3c.dom.Node"),
                new AssignsAndReturnsArg(CHILDREN, "org.w3c.dom.Node", 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_DEF_HIDDEN(CHILDREN), AT_ANY);

        addSig("org.w3c.dom.Node", "getFirstChild", EMPTY, new SolReturnsField(
                CHILDREN, "org.w3c.dom.Node"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CHILDREN), AT_ANY);

        addSig("org.w3c.dom.Document", "importNode",
                List("org.w3c.dom.Node", "boolean"), new AssignsAndReturnsArg(
                        CHILDREN, "org.w3c.dom.Node", 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new DEF_HIDDEN(
                        CHILDREN), AT_ANY);

        addSig("org.w3c.dom.NodeList", "getLength", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        addSig("org.w3c.dom.NodeList", "item", List("int"),
                new SolReturnsField(CONTENTS, "org.w3c.dom.Node"),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CONTENTS), AT_ANY);

        addSig("org.w3c.dom.Node",
                "getChildNodes",
                EMPTY,
                allocateAndAssign("org.w3c.dom.NodeList", "org.w3c.dom.Node",
                        CHILDREN, CONTENTS),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("org.w3c.dom.Document",
                "getElementsByTagName",
                List("java.lang.String"),
                allocateAndAssign("org.w3c.dom.NodeList", "org.w3c.dom.Node",
                        CHILDREN, CONTENTS),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CHILDREN), AT_ANY);
        addSig("org.w3c.dom.Element",
                "getElementsByTagName",
                List("java.lang.String"),
                allocateAndAssign("org.w3c.dom.NodeList", "org.w3c.dom.Node",
                        CHILDREN, CONTENTS),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CHILDREN), AT_ANY);
        addSig("org.w3c.dom.Node",
                "getElementsByTagName",
                List("java.lang.String"),
                allocateAndAssign("org.w3c.dom.NodeList", "org.w3c.dom.Node",
                        CHILDREN, CONTENTS),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG, new USE_HIDDEN(
                        CHILDREN), AT_ANY);

        addSig("org.w3c.dom.Document",
                "getElementsByTagNameNS",
                List("java.lang.String", "java.lang.String"),
                allocateAndAssign("org.w3c.dom.NodeList", "org.w3c.dom.Node",
                        CHILDREN, CONTENTS),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("org.w3c.dom.Element",
                "getElementsByTagNameNS",
                List("java.lang.String", "java.lang.String"),
                allocateAndAssign("org.w3c.dom.NodeList", "org.w3c.dom.Node",
                        CHILDREN, CONTENTS),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("org.w3c.dom.Node",
                "getElementsByTagNameNS",
                List("java.lang.String", "java.lang.String"),
                allocateAndAssign("org.w3c.dom.NodeList", "org.w3c.dom.Node",
                        CHILDREN, CONTENTS),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("org.w3c.dom.Document", "createTextNode",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "org.w3c.dom.Text", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.regex.Pattern", "matcher",
                List("java.lang.CharSequence"), new SolAllocatorOrReceiver(
                        "java.util.regex.Matcher", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.regex.Matcher", "find", List("int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.regex.Matcher", "group", List("int"),
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.regex.Matcher", "end", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.mail.Service", "connect", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("javax.mail.Service", "close", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.mail.Transport", "sendMessage",
                List("javax.mail.Message", "javax.mail.Address[]"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.mail.Session", "getTransport", EMPTY,
                new SolAllocatorOrReceiver("javax.mail.Transport", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.OutputStream", "write", List("byte[]"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Collection", "contains", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Collection",
                "toArray",
                ALL,
                allocateAndAssign("java.lang.Object[]", UNKNOWN_TYPE, CONTENTS,
                        CONTENTS), NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                new USE_HIDDEN(CONTENTS), AT_ANY);

        addSig("java.util.Map", "containsKey", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.List", "size", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.List", "get", List("int"), new SolReturnsField(
                CONTENTS, UNKNOWN_TYPE),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.File", "createNewFile", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.io.File", "delete", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.io.File", "mkdirs", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.Float", "equals", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.Long", "equals", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.AbstractCollection", "addAll",
                List("java.util.Collection"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.AbstractCollection", "containsAll",
                List("java.util.Collection"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.ArrayList", "isEmpty", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.List", "isEmpty", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.ArrayList", "remove", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Date", "after", List("java.util.Date"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.HashMap", "isEmpty", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.HashSet", "remove", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.LinkedList", "remove", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Stack", "empty", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.TreeMap", "containsKey", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.TreeSet", "contains", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Vector", "remove", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.Integer", "floatValue", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.io.InputStream", "read", List("byte[]"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.String", "lastIndexOf", List("int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.String", "lastIndexOf", List("java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Date", "getDate", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Date", "getDay", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Date", "getMonth", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.GregorianCalendar", "getMaximum", List("int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.HashMap", "size", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.zip.ZipFile", "size", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Arrays", "equals", List("byte[]", "byte[]"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.Math", "sqrt", List("double"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.Float", "parseFloat", List("java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.File", "length", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.BufferedOutputStream", "flush", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.security.SecureRandom", "nextBytes", List("byte[]"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.zip.ZipOutputStream", "write",
                List("byte[]", "int", "int"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("com.Ostermiller.util.CSVPrinter", "println",
                List("java.lang.String[]"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("com.Ostermiller.util.CSVPrinter", "writeln",
                List("java.lang.String[]"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("com.Ostermiller.util.ExcelCSVPrinter", "writeln",
                List("java.lang.String[]"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("com.Ostermiller.util.MD5", "update", List("byte[]", "int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.io.ByteArrayInputStream", "close", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.Throwable", "setStackTrace",
                List("java.lang.StackTraceElement[]"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.ArrayList", "add", List("int", "java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.ArrayList", "set", List("int", "java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Calendar", "setLenient", List("boolean"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Calendar", "setTimeInMillis", List("long"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.GregorianCalendar", "add", List("int", "int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.zip.ZipOutputStream", "close", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.zip.ZipOutputStream", "closeEntry", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.zip.ZipOutputStream", "putNextEntry",
                List("java.util.zip.ZipEntry"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.ejb.EJBException", "printStackTrace", EMPTY,
                PointsToLibrarySigs.NOOP, NotNullAnalysisUtil.NOOP_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.naming.InitialContext", "close", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("org.w3c.tidy.Tidy", "setEncloseText", List("boolean"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("org.w3c.tidy.Tidy", "setPrintBodyOnly", List("boolean"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("org.w3c.tidy.Tidy", "setQuiet", List("boolean"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("org.w3c.tidy.Tidy", "setWraplen", List("int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Arrays", "sort", List("float[]"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Arrays", "sort", List("java.lang.Object[]"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Hashtable", "containsKey", List("java.lang.Object"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Vector", "addAll", List("java.util.Collection"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.util.Vector", "removeAll", List("java.util.Collection"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addToStringSig("java.util.Vector", "toString", EMPTY);
        addToStringSig("java.lang.Float", "toString", EMPTY);
        addToStringSig("java.lang.Character", "toString", List("char"));
        addToStringSig("com.Ostermiller.util.MD5", "getHashString", EMPTY);
        addToStringSig("java.io.File", "getName", EMPTY);
        addToStringSig("java.io.File", "getParent", EMPTY);
        addToStringSig("java.io.File", "getPath", EMPTY);
        addToStringSig("java.lang.String", "replaceFirst",
                List("java.lang.String", "java.lang.String"));
        addToStringSig("java.lang.Throwable", "getLocalizedMessage", EMPTY);
        addToStringSig("java.net.URL", "getHost", EMPTY);
        addToStringSig("java.net.URL", "getPath", EMPTY);
        addToStringSig("java.rmi.RemoteException", "getMessage", EMPTY);
        addToStringSig("javax.ejb.EJBException", "getMessage", EMPTY);

        addSig("java.net.URL", "openStream", EMPTY, new SolAllocatorOrReceiver(
                "java.io.InputStream", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Calendar", "getTime", EMPTY,
                new SolAllocatorOrReceiver("java.util.Date", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.io.File", "getParentFile", EMPTY,
                new SolAllocatorOrReceiver("java.io.File", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.zip.ZipFile", "getInputStream",
                List("java.util.zip.ZipEntry"), new SolAllocatorOrReceiver(
                        "java.io.InputStream", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Date", "clone", EMPTY, new SolAllocatorOrReceiver(
                "java.util.Date", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("javax.xml.parsers.DocumentBuilder", "parse",
                List("java.io.File"), new SolAllocatorOrReceiver(
                        "org.w3c.dom.Document", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("javax.xml.parsers.DocumentBuilder", "parse",
                List("java.io.InputStream"), new SolAllocatorOrReceiver(
                        "org.w3c.dom.Document", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("org.w3c.tidy.Tidy", "parse",
                List("java.io.InputStream", "java.io.Writer"),
                new SolAllocatorOrReceiver("org.w3c.tidy.Node", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.Float", "valueOf", ALL, new SolAllocatorOrReceiver(
                "java.lang.Float", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.Integer", "valueOf", ALL, new SolAllocatorOrReceiver(
                "java.lang.Integer", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.sql.Timestamp", "valueOf", ALL,
                new SolAllocatorOrReceiver("java.sql.Timestamp", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("javax.xml.transform.TransformerFactory", "newInstance", EMPTY,
                new SolAllocatorOrReceiver(
                        "javax.xml.transform.TransformerFactory", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.Vector", "clone", EMPTY, new SolAllocatorOrReceiver(
                "java.util.Vector", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("javax.xml.transform.TransformerFactory", "newTransformer",
                EMPTY, new SolAllocatorOrReceiver(
                        "javax.xml.transform.Transformer", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.xml.transform.Transformer", "setOutputProperty",
                List("java.lang.String", "java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("javax.xml.transform.Transformer",
                "transform",
                List("javax.xml.transform.Source", "javax.xml.transform.Result"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        addSig("java.lang.String", "getBytes", List("java.lang.String"),
                new SolAllocatorOrReceiver("byte[]", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("javax.naming.InitialContext", "lookup",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        UNKNOWN_TYPE, false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.io.File", "getAbsolutePath", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.lang.Integer", "toString", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.zip.ZipFile", "entries", EMPTY,
                new SolAllocatorOrReceiver("java.util.Enumeration", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addSig("java.util.zip.ZipEntry", "getName", EMPTY,
                mayAllocString(  false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("com.Ostermiller.util.CSVPrinter",
                List("java.io.OutputStream"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("com.Ostermiller.util.ExcelCSVParser",
                List("java.io.InputStream"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("com.Ostermiller.util.ExcelCSVParser",
                List("java.io.Reader"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("com.Ostermiller.util.ExcelCSVPrinter",
                List("java.io.OutputStream"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("com.Ostermiller.util.MD5", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.io.BufferedInputStream",
                List("java.io.InputStream"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.io.BufferedOutputStream",
                List("java.io.OutputStream"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.io.BufferedOutputStream",
                List("java.io.OutputStream", "int"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.io.ByteArrayInputStream", List("byte[]"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.io.ByteArrayOutputStream", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.io.ByteArrayOutputStream", List("int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.io.FileInputStream", List("java.io.File"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.io.FileInputStream", List("java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.io.FileOutputStream", List("java.io.File"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.io.StringWriter", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.lang.Exception", List("java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.lang.Float", List("float"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.lang.NullPointerException",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.lang.NumberFormatException", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.net.ConnectException",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.rmi.RemoteException", List("java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.security.InvalidParameterException",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.text.ParseException",
                List("java.lang.String", "int"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.util.ArrayList", List("int"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.util.ArrayList", List("java.util.Collection"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.util.GregorianCalendar", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.util.GregorianCalendar",
                List("int", "int", "int"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.util.HashSet", List("java.util.Collection"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.util.LinkedList", List("java.util.Collection"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.util.Stack", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.util.TreeMap", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.util.zip.ZipEntry", List("java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.util.zip.ZipFile", List("java.io.File"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.util.zip.ZipOutputStream",
                List("java.io.OutputStream"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("javax.ejb.CreateException",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("javax.ejb.EJBException",
                List("java.lang.Exception"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("javax.ejb.EJBException", List("java.lang.String"),
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("javax.ejb.FinderException",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("javax.mail.internet.InternetAddress",
                List("java.lang.String"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("javax.mail.internet.MimeMessage",
                List("javax.mail.Session"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("javax.xml.transform.dom.DOMSource",
                List("org.w3c.dom.Node"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("javax.xml.transform.stream.StreamResult",
                List("java.io.Writer"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("org.w3c.tidy.Tidy", EMPTY, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);
        addConstructorSig("java.lang.Error", ALL, PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_ANY);

        /* JPA Signatures */
        addSig("javax.persistence.Persistence", "createEntityManagerFactory",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "javax.persistence.EntityManagerFactory", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);
        addSig("javax.persistence.EntityManagerFactory", "createEntityManager",
                EMPTY, new SolAllocatorOrReceiver(
                        "javax.persistence.EntityManager", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);
        addSig("javax.persistence.EntityManager", "createNamedQuery",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "javax.persistence.Query", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);
        addSig("javax.persistence.EntityManager", "createNativeQuery",
                List("java.lang.String"), new SolAllocatorOrReceiver(
                        "javax.persistence.Query", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);

        addSig("javax.persistence.EntityManager", "persist",
                List("java.lang.Object"), new PersistsArg(DATABASE,
                        "java.lang.Object", 0),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);

        addSig("javax.persistence.EntityManager", "find", ALL,
                new ReturnsPersisted(DATABASE, UNKNOWN_TYPE, true),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);

        addSig("javax.persistence.Query", "getResultList", EMPTY,
                new ReturnsPersisted(DATABASE, UNKNOWN_TYPE, false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);

        addSig("javax.persistence.Query", "getSingleResult", EMPTY,
                new ReturnsPersisted(DATABASE, UNKNOWN_TYPE, true),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);

        addSig("javax.persistence.Query", "executeUpdate", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);
        addSig("javax.persistence.Query", "setParameter",
                List("int", "java.lang.Object"), PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);

        // TODO: track transactions better
        addSig("javax.persistence.EntityManager", "getTransaction", EMPTY,
                new SolAllocatorOrReceiver(
                        "javax.persistence.EntityTransaction", false),
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);
        addSig("javax.persistence.EntityTransaction", "begin", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);
        addSig("javax.persistence.EntityTransaction", "commit", EMPTY,
                PointsToLibrarySigs.NOOP,
                NotNullAnalysisUtil.NOOP_NONNULL_RESULT_SIG,
                DefUseLibrarySignature.NOOP, AT_SERVER);

    }

    private java.util.List<String> List(String... strings) {
        return Arrays.asList(strings);
    }

    private java.util.Set<String> Set(String... strings) {
        return new HashSet<String>(Arrays.asList(strings));
    }

}
