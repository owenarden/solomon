package solomon.analysis.instrumentor;

import java.io.File;
import java.io.IOException;

import polyglot.ast.ClassBody_c;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.frontend.Job;
import polyglot.main.Report;
import polyglot.types.SemanticException;
import polyglot.types.TypeSystem;
import polyglot.visit.ContextVisitor;
import polyglot.visit.NodeVisitor;
import solomon.ProfileUtil;
import solomon.analysis.instrumentor.ext.InstExt;

public class ProfileLoader extends ContextVisitor {
	
	public static final String PROFILE = "profiler";
	protected long[] counters = null;
	protected final File profileDir;
	public ProfileLoader(Job job, TypeSystem ts, NodeFactory nf, File profileDir) {
		super(job, ts, nf);
		this.profileDir = profileDir; 
	}

    @Override
	protected NodeVisitor enterCall(Node parent, Node n)
			throws SemanticException {
		ProfileLoader pl = (ProfileLoader) super.enterCall(parent, n);
		if(n instanceof ClassBody_c) {            
			InstExt ext = InstUtil.instExt(n);
			if(ext != null) {
				pl = ext.loadDataEnter(pl);				
			}
		}
		return pl;
	}

	@Override
	protected Node leaveCall(Node parent, Node old, Node n, NodeVisitor v)
			throws SemanticException {
		InstExt ext = InstUtil.instExt(n);
		if(ext != null) {
			ext.loadData((ProfileLoader) v, parent);
		}
		return n;
	}
	
	public long getCounterValue(int id) {
	    try {
    	    if (counters == null)
    	        return 1;
    		return counters[id];
	    } catch (ArrayIndexOutOfBoundsException e) {
	        String className = context.currentClassScope().fullName();
            System.err.println("Bad counter id in " + className + "(" + counters.length + ")");
            throw e;
	    }
	}
	
    public ProfileLoader loadClassData() {
        String className = context.currentClassScope().fullName();
        try {
            counters = ProfileUtil.loadData(new File(profileDir, className));
            if (Report.should_report(PROFILE, 2)) {
                Report.report(2, "Loaded " + counters.length
                        + " counters for class " + className);
            }
        } catch (IOException e) {
            System.err.println("Failed to load profile data for " + className + " from " + profileDir);
        }
        return this;
    }
}
