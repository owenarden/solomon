package solomon.analysis.instrumentor;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import polyglot.ast.ArrayAccess;
import polyglot.ast.Assign;
import polyglot.ast.Block;
import polyglot.ast.Call;
import polyglot.ast.CanonicalTypeNode;
import polyglot.ast.ClassDecl;
import polyglot.ast.Expr;
import polyglot.ast.Field;
import polyglot.ast.FieldDecl;
import polyglot.ast.Initializer;
import polyglot.ast.IntLit;
import polyglot.ast.MethodDecl;
import polyglot.ast.NewArray;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.Stmt;
import polyglot.ast.StringLit;
import polyglot.ast.Unary;
import polyglot.frontend.Job;
import polyglot.main.Options;
import polyglot.qq.QQ;
import polyglot.types.Flags;
import polyglot.types.SemanticException;
import polyglot.types.TypeSystem;
import polyglot.util.InternalCompilerError;
import polyglot.util.Position;
import polyglot.visit.ContextVisitor;
import polyglot.visit.NodeVisitor;
import solomon.analysis.instrumentor.ext.InstExt;


/**
 * Assumes localdecls and fielddecls have no initialization expressions. 
 */
public class InstrumentingVisitor extends ContextVisitor {
	private final static String COUNTER_ARRAY = "$counters$";
//	private final static String POSITION_MAP = "$position_map$";

	protected final QQ qq;
	protected final Map<String,MethodDecl> trampolines;
	protected final List<Position> counters;
	protected boolean registerOnly = false;

    /** Stack of nested blocks we are currently in. */
    protected final Stack<List<Stmt>> blockStack = new Stack<List<Stmt>>();

	public InstrumentingVisitor(Job job, TypeSystem ts, NodeFactory nf) {
		this(job,ts,nf,false);
	}
	
	public InstrumentingVisitor(Job job, TypeSystem ts, NodeFactory nf, boolean registerOnly) {
		super(job, ts, nf);
		this.qq = new QQ(job.extensionInfo(), Position.compilerGenerated());
		this.trampolines = new HashMap<String,MethodDecl>();
		this.counters = new ArrayList<Position>();
		this.registerOnly  = registerOnly;
		this.rethrowMissingDependencies = true;
	}

    @Override
	protected NodeVisitor enterCall(Node n) throws SemanticException {
        if (n instanceof ClassDecl) {
            ClassDecl cd = (ClassDecl) n;
            if (cd.flags().isInterface()) {
                return bypassChildren(cd);
            }
        }
        if (n instanceof Block && !registerOnly) {
            // push a new statement list for each block
            pushBlock();
        }
        return this;
	}
	
	@Override
	protected Node leaveCall(Node parent, Node old, Node n, NodeVisitor v)
			throws SemanticException {
		InstExt ext = InstUtil.instExt(n);
		if(ext != null) {
			ext.register((InstrumentingVisitor) v, parent);
	        if(ext.shouldInstrument()) {
				n = ext.instrument((InstrumentingVisitor) v, parent);
			}
		}
		if (registerOnly) {
		    // Important -- don't push/pop blocks 
		    // when registering. This could affect 
		    // NodeIdentifier equality between analysis passes
		    return n;
		}
		
        if (n instanceof Block) {
            // replace blocks with list of replacement statements
            Block b = (Block) n;
            n = b.statements(popBlock());
        }

        if (n instanceof Stmt && parent instanceof Block) {
            addStmt((Stmt) n);
        } 
		return n;
	}

	public void addStmt(Stmt s) {
	    blockStack.peek().add(s);
	}
	
	public void addStmt(Stmt s, int idx) {
	    blockStack.peek().add(idx, s);
	}
	
    /** Pushes a new (nested) block onto the stack. */
    protected void pushBlock() {
        blockStack.push(new ArrayList<Stmt>());
    }
    
    /** Pops a block off the stack. */
    protected List<Stmt> popBlock() {
        return blockStack.pop();
    }

	/**
	 * Allocate a new counter usable by instrumentation.
	 * @return the counter id.
	 */
	public int createCounter(Position pos) {
		if (pos.equals(Position.COMPILER_GENERATED)
		        || (Options.global.precise_compiler_generated_positions && pos.toString().contains("(compiler generated)")))
			throw new InternalCompilerError("Found compiler generated position:" + pos);
		counters.add(pos);
		return counters.size() -1;
	}
	
	public int counters() {
		return counters.size();
	}
	
	public FieldDecl declareCounters() {
		if(counters.size() == 0) {
			return null;
		}
		Position pos = Position.compilerGenerated();
		Flags f = Flags.PROTECTED.set(Flags.STATIC);
		CanonicalTypeNode tn1 = nf.CanonicalTypeNode(pos, ts.Long());
		CanonicalTypeNode tn2 = nf.CanonicalTypeNode(pos, ts.Long());
		List<Expr> dim = Collections.<Expr>singletonList(nf.IntLit(pos, IntLit.INT, counters()));
		NewArray init = nf.NewArray(pos, tn1, dim);
		return nf.FieldDecl(pos, f, nf.ArrayTypeNode(pos, tn2), nf.Id(pos,COUNTER_ARRAY), init);		
	}
	
	/**
	 * Generate AST nodes for an expression that increments 
	 * a counter.
	 */
	public Expr incrementCounter(int counterId) {
		Position pos = Position.compilerGenerated();
		return incrementCounter(nf.IntLit(pos, IntLit.INT, counterId));
	}
	
	/**
	 * Generate AST nodes for an expression that increments 
	 * a counter.
	 */
	public Expr incrementCounter(Expr counterId) {
		Position pos = Position.compilerGenerated();
		CanonicalTypeNode scope = nf.CanonicalTypeNode(pos, context
				.currentClassScope());
		Field cnt_array = nf.Field(pos, scope, nf.Id(pos, COUNTER_ARRAY));
		ArrayAccess counter = nf.ArrayAccess(pos, cnt_array, counterId);
		return nf.Unary(pos, counter, Unary.POST_INC);
	}
	
	/**
	 * Generate AST nodes for an expression that adds 
	 * a value to a counter.
	 */
	public Expr addToCounter(int counterId, Expr val) {
		Position pos = Position.compilerGenerated();
		return addToCounter(nf.IntLit(pos, IntLit.INT, counterId), val);
	}
	
	/**
	 * Generate AST nodes for an expression that adds 
	 * a value to a counter.
	 */
	public Expr addToCounter(Expr counterId, Expr val) {
		Position pos = Position.compilerGenerated();
		CanonicalTypeNode scope = nf.CanonicalTypeNode(pos, context
				.currentClassScope());
		Field cnt_array = nf.Field(pos, scope, nf.Id(pos, COUNTER_ARRAY));
		ArrayAccess counter = nf.ArrayAccess(pos, cnt_array, counterId);
		return nf.Assign(pos, counter, Assign.ADD_ASSIGN, val);
	}

	public Collection<MethodDecl> trampolines() {
		return trampolines.values();
	}
	
	public boolean hasTrampoline(String trampId) {
		return trampolines.containsKey(trampId);
	}
	
	public void addTrampoline(MethodDecl tramp) {
		trampolines.put(tramp.name(), tramp);
	}
	
	public Call callTrampoline(String trampId, List<Expr> args) {
		Position pos = Position.compilerGenerated();
		CanonicalTypeNode scope = nf.CanonicalTypeNode(pos, context
				.currentClassScope());
		return nf.Call(pos, scope, nf.Id(pos, trampId), args);
	}
	
	public Initializer writeCountersOnExit() {
		Position pos = Position.compilerGenerated();
		String fmt = "solomon.ProfileUtil.writeDataOnExit(%E,%E);";
		CanonicalTypeNode scope = nf.CanonicalTypeNode(pos, context
				.currentClassScope());
		Field cnt_array = nf.Field(pos, scope, nf.Id(pos, COUNTER_ARRAY));
		StringLit fname = nf.StringLit(pos, context.currentClassScope().fullName());
		return nf.Initializer(pos, Flags.STATIC, nf.Block(pos, qq.parseStmt(fmt, fname,
				cnt_array)));
	}
	
	public QQ qq() {
		return qq;
	}
}
