package solomon.analysis.instrumentor;

import polyglot.ast.Ext;
import polyglot.ast.Node;
import solomon.analysis.instrumentor.ext.InstExt;

public class InstUtil {
	public static InstExt instExt(Node n) {
        Ext e = n.ext();
        while (e != null && !(e instanceof InstExt)) {
            e = e.ext();
        }
        return (InstExt)e;
	}
}
