package solomon.analysis.instrumentor.ext;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import polyglot.ast.Do;
import polyglot.ast.Expr;
import polyglot.ast.For;
import polyglot.ast.IntLit;
import polyglot.ast.Local;
import polyglot.ast.Loop;
import polyglot.ast.MethodDecl;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.Stmt;
import polyglot.ast.While;
import polyglot.types.Type;
import polyglot.types.TypeSystem;
import polyglot.util.InternalCompilerError;
import polyglot.util.Position;
import solomon.analysis.instrumentor.InstrumentingVisitor;

public class InstLoop_c extends InstStmt_c {
    private static final long serialVersionUID = 1L;

    public InstLoop_c(boolean instrument) {
        super(instrument);
    }
    
    @Override
    public Node instrument(InstrumentingVisitor iv) {
        Loop loop = (Loop) node();
        NodeFactory nf = iv.nodeFactory();
        Position pos = Position.compilerGenerated();

        TypeSystem ts = iv.typeSystem();
        Expr cond = loop.cond();
        
        Type t = ts.Boolean();
        String trampId = "count$if";
        List<Expr> args = new ArrayList<Expr>(2);
        args.add(nf.IntLit(pos, IntLit.INT, cntId));

        if (!iv.hasTrampoline(trampId)) {
                // add trampoline 
                String fmt = "public static %T %s(%T cntId, %T val) {" + "%E;"
                        + "return val;" + "}";
                Local cntId_local = nf.Local(pos, nf.Id(pos, "cntId"));
                Expr inc = iv.incrementCounter(cntId_local);
                MethodDecl md = (MethodDecl) iv.qq().parseMember(fmt, t,
                        trampId, ts.Int(), t, inc);
                iv.addTrampoline(md);
        }
        // add condition to arguments
        args.add(cond);        
        Loop newLoop;
        if (loop instanceof Do) {
            Do l = (Do) loop;
            newLoop = l.cond(iv.callTrampoline(trampId, args));    

        } else if (loop instanceof For) {
            For l = (For) loop;
            newLoop = l.cond(iv.callTrampoline(trampId, args));    

        } else if (loop instanceof While) {
            While l = (While) loop;
            newLoop = l.cond(iv.callTrampoline(trampId, args));    
        } else {
            throw new InternalCompilerError("Unknown loop type: " + loop.getClass());
        }      
        
        if (!loop.reachable()) {
            //inserting a trampoline on the condition will screw 
            // up the java reachability analysis, making the end 
            // of the loop reachable.
            List<Expr> arg = Collections.<Expr>singletonList(nf.StringLit(pos, "Instrumentation error: Reached unreachable statement."));
            Expr ex = nf.New(pos, nf.CanonicalTypeNode(pos, ts.Error()),arg);
            Stmt err = nf.Throw(pos, ex);
            iv.addStmt(newLoop);
            return err;
        }
        return newLoop;
    }    
}
