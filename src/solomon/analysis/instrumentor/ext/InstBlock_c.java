package solomon.analysis.instrumentor.ext;


import polyglot.ast.CodeDecl;
import polyglot.ast.Eval;
import polyglot.ast.Expr;
import polyglot.ast.MethodDecl;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.ProcedureDecl;
import polyglot.util.InternalCompilerError;
import polyglot.util.Position;
import solomon.analysis.instrumentor.InstrumentingVisitor;
import solomon.analysis.instrumentor.ProfileLoader;

public class InstBlock_c extends InstStmt_c {
    private static final long serialVersionUID = 1L;

    public InstBlock_c(boolean instrument) {
        super(instrument);
    }

    @Override
    public void registerImpl(InstrumentingVisitor iv, Node parent) {
        if (parent instanceof CodeDecl)
            registerImpl(iv);
    }
    
    @Override
    public void loadDataImpl(ProfileLoader pl, Node parent) {
        if (parent instanceof CodeDecl)
            loadDataImpl(pl);
    }

	@Override
	public Node instrument(InstrumentingVisitor iv) {
		throw new InternalCompilerError(
				"Use instrument(InstrumentingVisitor,Node) for Blocks");
	}
	
	@Override
	public Node instrument(InstrumentingVisitor iv, Node parent) {
		// Count entry into procedure bodies.
		if(parent instanceof ProcedureDecl) {
			NodeFactory nf = iv.nodeFactory();
			Expr cnt = iv.incrementCounter(cntId);		
			
			Position pos = Position.compilerGenerated();	
			Eval inc = nf.Eval(pos, cnt);
			
			if(parent instanceof MethodDecl) {
			    iv.addStmt(inc,0);
			}
			else {
				iv.addStmt(inc,1);
			}
		}
		return node();
	}

}
