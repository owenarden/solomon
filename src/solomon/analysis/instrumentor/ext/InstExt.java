package solomon.analysis.instrumentor.ext;

import polyglot.ast.Ext;
import polyglot.ast.Node;
import solomon.analysis.instrumentor.InstrumentingVisitor;
import solomon.analysis.instrumentor.ProfileLoader;

public interface InstExt extends Ext {
    boolean shouldInstrument();
	Node instrument(InstrumentingVisitor iv);
	Node instrument(InstrumentingVisitor iv, Node parent);
	void register(InstrumentingVisitor iv);
	void register(InstrumentingVisitor iv, Node parent);
	ProfileLoader loadDataEnter(ProfileLoader pl);
    void loadData(ProfileLoader pl, Node parent);
    void checkRegistered();
    void checkLoaded();
}
