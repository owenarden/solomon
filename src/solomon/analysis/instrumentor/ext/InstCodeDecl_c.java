package solomon.analysis.instrumentor.ext;

import polyglot.ast.CodeDecl;
import polyglot.main.Report;
import polyglot.util.InternalCompilerError;
import solomon.analysis.instrumentor.InstUtil;
import solomon.analysis.instrumentor.ProfileLoader;

public class InstCodeDecl_c extends InstExt_c implements InstStmt {
    private static final long serialVersionUID = 1L;

    protected long profiled_cnt = -1;

    public InstCodeDecl_c(boolean instrument) {
        super(instrument);
    }
    @Override
    public void loadDataImpl(ProfileLoader pl) {
        CodeDecl cd = (CodeDecl) node();
        if (cd.body() != null) {
            InstStmt ext = (InstStmt) InstUtil.instExt(cd.body());
            profiled_cnt  = ext.executionCount();
            if (profiled_cnt < 0) 
                throw new InternalCompilerError("Negative counter!" + node());

            if (Report.should_report(ProfileLoader.PROFILE,3)) {
                Report.report(3, "XXX: PROFILE INFO: " + profiled_cnt +":"+ node());
            }
        }
        else if (!pl.context().currentClass().flags().isInterface()
                && !pl.context().currentClass().flags().isAbstract()
                && !pl.context().currentCode().flags().isNative()) {
            throw new InternalCompilerError("No inst node for body of " + node());
        }
    }
    @Override
    public long executionCount() {
        checkLoaded();
        return profiled_cnt;
    }
}
