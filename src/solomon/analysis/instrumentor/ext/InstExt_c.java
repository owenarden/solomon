package solomon.analysis.instrumentor.ext;

import polyglot.ast.Ext_c;
import polyglot.ast.Node;
import polyglot.util.InternalCompilerError;
import solomon.analysis.instrumentor.InstrumentingVisitor;
import solomon.analysis.instrumentor.ProfileLoader;

public abstract class InstExt_c extends Ext_c implements InstExt {
    private static final long serialVersionUID = 1L;
    protected boolean loaded = false;
    protected boolean registered = false;
    protected boolean shouldInstrument;

    public InstExt_c(boolean instrument) {
        this.shouldInstrument = instrument;
    }

    @Override 
    public boolean shouldInstrument() {
        return shouldInstrument;
    }
    
	@Override
    public void checkRegistered() {
	    if (!registered) throw new InternalCompilerError("not registered");
    }

    @Override
    public void checkLoaded() {
        checkRegistered();
        if (!loaded) 
            throw new InternalCompilerError("not loaded");        
    }

    @Override
	public ProfileLoader loadDataEnter(ProfileLoader pl) {	
		return pl;
	}
	
    @Override
    public Node instrument(InstrumentingVisitor iv) {
        return node();
    }

    @Override
    public Node instrument(InstrumentingVisitor iv, Node parent) {
        return instrument(iv);
    }

    @Override
    public final void register(InstrumentingVisitor iv) {
        registerImpl(iv);
        registered = true;
    }

    @Override
    public final void register(InstrumentingVisitor iv, Node parent) {
        registerImpl(iv, parent);
        registered = true;
    }

    protected void registerImpl(InstrumentingVisitor iv) {
    }

    protected void registerImpl(InstrumentingVisitor iv, Node parent) {
        registerImpl(iv);
    }

    @Override
    public final void loadData(ProfileLoader pl, Node parent) {
        loadDataImpl(pl, parent);
        loaded = true;
    }

    protected abstract void loadDataImpl(ProfileLoader pl);

    protected void loadDataImpl(ProfileLoader pl, Node parent) {
        loadDataImpl(pl);
    }
}
