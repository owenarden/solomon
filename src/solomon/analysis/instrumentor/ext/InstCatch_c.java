package solomon.analysis.instrumentor.ext;


import java.util.ArrayList;
import java.util.List;


import polyglot.ast.Block;
import polyglot.ast.Catch;
import polyglot.ast.Eval;
import polyglot.ast.Expr;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.Stmt;
import polyglot.util.Position;
import solomon.analysis.instrumentor.InstrumentingVisitor;

public class InstCatch_c extends InstStmt_c {
    private static final long serialVersionUID = 1L;

    public InstCatch_c(boolean instrument) {
        super(instrument);
    }

	@Override
	public Node instrument(InstrumentingVisitor iv) {
		NodeFactory nf = iv.nodeFactory();
		Catch s = (Catch) node();
		Block b = s.body();
		Expr cnt = iv.incrementCounter(cntId);		
		
		Position pos = Position.compilerGenerated();	
		Eval inc = nf.Eval(pos, cnt);
		List<Stmt> stmts = new ArrayList<Stmt>(b.statements().size() +1); 
		stmts.add(inc);
		stmts.addAll(b.statements());
		return s.body(b.statements(stmts));
	}

}
