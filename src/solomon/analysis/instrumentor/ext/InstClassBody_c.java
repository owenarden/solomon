package solomon.analysis.instrumentor.ext;

import polyglot.ast.ClassBody;
import polyglot.ast.FieldDecl;
import polyglot.ast.MethodDecl;
import polyglot.ast.Node;
import solomon.analysis.instrumentor.InstrumentingVisitor;
import solomon.analysis.instrumentor.ProfileLoader;

public class InstClassBody_c extends InstExt_c {
    private static final long serialVersionUID = 1L;

    public InstClassBody_c(boolean instrument) {
        super(instrument);
    }

    @Override
	public Node instrument(InstrumentingVisitor iv) {
		ClassBody cd = (ClassBody) node();
		for(MethodDecl tramp : iv.trampolines())
			cd = cd.addMember(tramp);
		FieldDecl counters = iv.declareCounters();
		if(counters!=null) {
			cd = cd.addMember(counters);
//			Collection<ClassMember> mems = iv.serializePositionMap();
//			for (ClassMember cm : mems)
//			    cd = cd.addMember(cm);
			cd = cd.addMember(iv.writeCountersOnExit());
		}
		return cd;
	}

	@Override
	public Node instrument(InstrumentingVisitor iv, Node parent) {
		return instrument(iv);
	}

	@Override
	public void registerImpl(InstrumentingVisitor iv) {
	}

	@Override
	public void registerImpl(InstrumentingVisitor iv, Node parent) {
	}

	@Override
	public ProfileLoader loadDataEnter(ProfileLoader pl) {
		pl.loadClassData();
		return pl;
	}

	@Override
	public void loadDataImpl(ProfileLoader pl) {
		
	}
}
