package solomon.analysis.instrumentor.ext;

import polyglot.ast.Eval;
import polyglot.ast.Expr;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.Stmt;
import polyglot.util.Position;
import solomon.analysis.instrumentor.InstrumentingVisitor;

public class InstBranch_c extends InstStmt_c {
    private static final long serialVersionUID = 1L;

    public InstBranch_c(boolean instrument) {
        super(instrument);
    }
	
    @Override
	public Node instrument(InstrumentingVisitor iv) {
		NodeFactory nf = iv.nodeFactory();
		// increment counter before branch
		Expr cnt = iv.incrementCounter(cntId);		
		Position pos = Position.compilerGenerated();	
		Eval inc = nf.Eval(pos, cnt);
        iv.addStmt(inc);
        
		Stmt s = (Stmt) node();
		return s;
	}
}
