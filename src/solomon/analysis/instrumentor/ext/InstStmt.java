package solomon.analysis.instrumentor.ext;

import solomon.analysis.instrumentor.ProfileLoader;

public interface InstStmt extends InstExt {
	void loadDataImpl(ProfileLoader pl);
	long executionCount();
}
