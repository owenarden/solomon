package solomon.analysis.instrumentor.ext;

import java.util.ArrayList;
import java.util.List;

import polyglot.ast.ArrayInit;
import polyglot.ast.Assign;
import polyglot.ast.Expr;
import polyglot.ast.IntLit;
import polyglot.ast.Local;
import polyglot.ast.MethodDecl;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.TypeNode;
import polyglot.main.Report;
import polyglot.types.ArrayType;
import polyglot.types.Type;
import polyglot.types.TypeSystem;
import polyglot.util.Position;
import solomon.analysis.instrumentor.InstrumentingVisitor;
import solomon.analysis.instrumentor.ProfileLoader;

public class InstAssign_c extends InstExt_c implements InstAssign {
    private static final long serialVersionUID = -502729126182385918L;

    protected int cntId = -1;
    protected int sumId = -1;

    protected long profiled_sum = -1;
    protected long profiled_cnt = -1;

    public InstAssign_c(boolean instrument) {
        super(instrument);
    }

    @Override
    public Node instrument(InstrumentingVisitor iv) {
        Assign e = (Assign) node();
        NodeFactory nf = iv.nodeFactory();

        Position pos = Position.compilerGenerated();

        Expr rhs = e.right();
        if (rhs.type().isNull())
            return node();

        Expr lhs = e.left();
        Type t = lhs.type();

        String t_name = t.translate(null);
        t_name = t_name.replace(".", "$");

        if (t.isArray()) {
            t_name = t_name.replace("[]", "$");
            if (rhs instanceof ArrayInit) {
                ArrayType at = (ArrayType) t;
                TypeNode tn = nf.CanonicalTypeNode(pos, at.base());
                rhs = nf.NewArray(pos, tn, at.dims(), (ArrayInit) rhs);
            }
        }
        if (!t.equals(rhs.type()))
            rhs = nf.Cast(pos, nf.CanonicalTypeNode(pos, t), rhs);

        String trampId = "sizeof$assign$" + t_name;

        if (!iv.hasTrampoline(trampId)) {
            TypeSystem ts = iv.typeSystem();
            String fmt = "public static %T %s(%T cntId, %T sumId, %T val) {"
                    + "long sizeof = solomon.ProfileUtil.sizeof(val);"
                    + "%E;%E;" + "return val;" + "}";
            Local cntId_local = nf.Local(pos, nf.Id(pos, "cntId"));
            Expr inc = iv.incrementCounter(cntId_local);
            Local sumId_local = nf.Local(pos, nf.Id(pos, "sumId"));
            Local size_local = nf.Local(pos, nf.Id(pos, "sizeof"));
            Expr sum = iv.addToCounter(sumId_local, size_local);

            MethodDecl md = (MethodDecl) iv.qq().parseMember(fmt, t, trampId,
                    ts.Int(), ts.Int(), t, inc, sum);
            iv.addTrampoline(md);
        }

        List<Expr> args = new ArrayList<Expr>(3);
        args.add(nf.IntLit(pos, IntLit.INT, cntId));
        args.add(nf.IntLit(pos, IntLit.INT, sumId));
        args.add(rhs);

        // set RHS to call to trampoline
        return e.right(iv.callTrampoline(trampId, args));
    }

    @Override
    public Node instrument(InstrumentingVisitor iv, Node parent) {
        return instrument(iv);
    }

    @Override
    public void registerImpl(InstrumentingVisitor iv) {
        cntId = iv.createCounter(node().position());
        sumId = iv.createCounter(node().position());
    }

    @Override
    public void registerImpl(InstrumentingVisitor iv, Node parent) {
        registerImpl(iv);
    }

    @Override
    public double averageSize() {
        if (profiled_cnt > 0)
            return ((double) profiled_sum) / ((double) profiled_cnt);
        else
            return 0;
    }

    @Override
    public void loadDataImpl(ProfileLoader pl) {
        profiled_cnt = pl.getCounterValue(cntId);
        profiled_sum = pl.getCounterValue(sumId);
        if (Report.should_report(ProfileLoader.PROFILE, 3)) {
            Report.report(3, "PROFILE INFO: " + averageSize() + ":" + node());
        }
    }

}
