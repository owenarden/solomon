package solomon.analysis.instrumentor.ext;

import polyglot.ast.Eval;
import polyglot.ast.Expr;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.Stmt;
import polyglot.main.Report;
import polyglot.util.InternalCompilerError;
import polyglot.util.Position;
import solomon.analysis.instrumentor.InstrumentingVisitor;
import solomon.analysis.instrumentor.ProfileLoader;

public class InstStmt_c extends InstExt_c implements InstStmt {	
    private static final long serialVersionUID = 1L;

    protected int cntId = -1;
	protected long profiled_cnt = -1;
	protected boolean instrument = true;

	public InstStmt_c(boolean instrument) {
	    super(instrument);
    }

    @Override
	public Node instrument(InstrumentingVisitor iv) {
        checkRegistered();
		NodeFactory nf = iv.nodeFactory();
		Stmt s = (Stmt) node();
		iv.addStmt(s);
		Expr cnt = iv.incrementCounter(cntId);		
		
		Position pos = Position.compilerGenerated();	
		Eval inc = nf.Eval(pos, cnt);

		return inc;
	}

	@Override
	public Node instrument(InstrumentingVisitor iv, Node parent) {
		return instrument(iv);
	}
	
	@Override
	public void registerImpl(InstrumentingVisitor iv) {
		cntId = iv.createCounter(node().position());
	}
	
	@Override
	public void registerImpl(InstrumentingVisitor iv, Node parent) {
		registerImpl(iv);
	}

	@Override
	public long executionCount() {
	    checkLoaded();
	    if (profiled_cnt < 0) throw new InternalCompilerError("Negative counter!" + node());
		return profiled_cnt;
	}

    @Override
    public void loadDataImpl(ProfileLoader pl, Node parent) {
        loadDataImpl(pl);
    }

	public void loadDataImpl(ProfileLoader pl) {
	    if (cntId < 0) 
	        throw new InternalCompilerError("Unregistered:" + node());
		profiled_cnt  = pl.getCounterValue(cntId);
		if (profiled_cnt < 0) throw new InternalCompilerError("Negative counter!" + node());
		if (Report.should_report(ProfileLoader.PROFILE,3)) {
			Report.report(3, "PROFILE INFO: " + profiled_cnt +":"+ node());
		}
	}
}
