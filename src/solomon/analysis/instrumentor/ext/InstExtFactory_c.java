package solomon.analysis.instrumentor.ext;

import polyglot.ast.AbstractExtFactory_c;
import polyglot.ast.Ext;
import polyglot.ast.ExtFactory;

public class InstExtFactory_c extends AbstractExtFactory_c implements InstExtFactory {
    protected final boolean instrument;
	public InstExtFactory_c(boolean instrument, ExtFactory nextExtFactory) {
        super(nextExtFactory);
        this.instrument = instrument;
    }
	
    public InstExtFactory_c() {
        super();
        this.instrument = true;
    }
    
	@Override
	protected Ext extAssignImpl() {
		return new InstAssign_c(instrument);
	}
	
	@Override
	protected Ext extStmtImpl() {
		return new InstStmt_c(instrument);
	}

	@Override
	protected Ext extClassBodyImpl() {
		return new InstClassBody_c(instrument);
	}

	@Override
	protected Ext extCatchImpl() {
		return new InstCatch_c(instrument);
	}

	@Override
	protected Ext extIfImpl() {
		return new InstIf_c(instrument);
	}

	@Override
	protected Ext extLoopImpl() {
		return new InstLoop_c(instrument);
	}

	@Override
	protected Ext extTryImpl() {
		return new InstTry_c(instrument);
	}

	//nodes not to be instrumented
	@Override
	protected Ext extLocalDeclImpl() {
		return new InstStmt_c(false);
	}
	
	@Override
	protected Ext extConstructorCallImpl() {
        return new InstStmt_c(false);
	}
	
	@Override
	protected Ext extBlockImpl() {
		return new InstBlock_c(instrument);
	}

	@Override
	protected Ext extBranchImpl() {
		return new InstBranch_c(instrument);
	}

	@Override
	protected Ext extReturnImpl() {
		return new InstReturn_c(instrument);
	}

	@Override
	protected Ext extThrowImpl() {
		return new InstThrow_c(instrument);
	}
	
    @Override
    protected Ext extCodeDeclImpl() {
        return new InstCodeDecl_c(instrument);
    }

}
