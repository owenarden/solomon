package solomon.analysis.instrumentor.ext;


import java.util.ArrayList;
import java.util.List;

import polyglot.ast.Expr;
import polyglot.ast.If;
import polyglot.ast.IntLit;
import polyglot.ast.Local;
import polyglot.ast.MethodDecl;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.types.Type;
import polyglot.types.TypeSystem;
import polyglot.util.Position;
import solomon.analysis.instrumentor.InstrumentingVisitor;

public class InstIf_c extends InstStmt_c {
    private static final long serialVersionUID = 1L;

	public InstIf_c(boolean instrument) {
        super(instrument);
    }
	
    @Override
    public Node instrument(InstrumentingVisitor iv) {
        If ifn = (If) node();
        NodeFactory nf = iv.nodeFactory();

        Position pos = Position.compilerGenerated();

        TypeSystem ts = iv.typeSystem();
        Expr cond = ifn.cond();
        
        Type t = ts.Boolean();
        String trampId = "count$if";
        List<Expr> args = new ArrayList<Expr>(2);
        args.add(nf.IntLit(pos, IntLit.INT, cntId));

        if (!iv.hasTrampoline(trampId)) {
                // add trampoline 
                String fmt = "public static %T %s(%T cntId, %T val) {" + "%E;"
                        + "return val;" + "}";
                Local cntId_local = nf.Local(pos, nf.Id(pos, "cntId"));
                Expr inc = iv.incrementCounter(cntId_local);
                MethodDecl md = (MethodDecl) iv.qq().parseMember(fmt, t,
                        trampId, ts.Int(), t, inc);
                iv.addTrampoline(md);
        }
        // add condition to arguments
        args.add(cond);
        return ifn.cond(iv.callTrampoline(trampId, args));    
    }
}
