package solomon.analysis.instrumentor.ext;

import java.util.ArrayList;
import java.util.List;

import polyglot.ast.ArrayInit;
import polyglot.ast.Expr;
import polyglot.ast.IntLit;
import polyglot.ast.Local;
import polyglot.ast.MethodDecl;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.Return;
import polyglot.ast.TypeNode;
import polyglot.types.ArrayType;
import polyglot.types.CodeInstance;
import polyglot.types.ConstructorInstance;
import polyglot.types.MethodInstance;
import polyglot.types.Type;
import polyglot.types.TypeSystem;
import polyglot.util.InternalCompilerError;
import polyglot.util.Position;
import solomon.analysis.instrumentor.InstrumentingVisitor;

public class InstReturn_c extends InstStmt_c {
    private static final long serialVersionUID = 1L;

    public InstReturn_c(boolean instrument) {
        super(instrument);
    }

    @Override
    public Node instrument(InstrumentingVisitor iv) {
        Return ret = (Return) node();
        NodeFactory nf = iv.nodeFactory();

        Position pos = Position.compilerGenerated();

        TypeSystem ts = iv.typeSystem();
        CodeInstance ci = iv.context().currentCode();
        Expr rhs = ret.expr();

        Type t;
        String t_name;
        if (ci instanceof ConstructorInstance) {
            t = ts.Void();
            t_name = ts.Void().translate(null);
        } else if (ci instanceof MethodInstance) {
            MethodInstance mi = (MethodInstance) ci;
            t = mi.returnType();
            t_name = t.translate(null);
            t_name = t_name.replace(".", "$");

            if (t.isArray()) {
                t_name = t_name.replace("[]", "$");
                if (rhs instanceof ArrayInit) {
                    ArrayType at = (ArrayType) t;
                    TypeNode tn = nf.CanonicalTypeNode(pos, at.base());
                    rhs = nf.NewArray(pos, tn, at.dims(), (ArrayInit) rhs);
                }
            }
        } else {
            throw new InternalCompilerError("Unexpectance code instance: " + ci);
        }

        String trampId = "count$return$" + t_name;
        List<Expr> args = new ArrayList<Expr>(3);
        args.add(nf.IntLit(pos, IntLit.INT, cntId));

        if (!t.isVoid()) {
            if (!iv.hasTrampoline(trampId)) {
                // add trampoline 
                String fmt = "public static %T %s(%T cntId, %T val) {" + "%E;"
                        + "return val;" + "}";
                Local cntId_local = nf.Local(pos, nf.Id(pos, "cntId"));
                Expr inc = iv.incrementCounter(cntId_local);
                MethodDecl md = (MethodDecl) iv.qq().parseMember(fmt, t,
                        trampId, ts.Int(), t, inc);
                iv.addTrampoline(md);
            }
            // add return value to arguments
            args.add(rhs);
            return ret.expr(iv.callTrampoline(trampId, args));
        } else {
            // return type is void           
            if (!iv.hasTrampoline(trampId)) {
                // add trampoline 
                String fmt = "public static void %s(%T cntId) {" + "%E;" + "}";
                Local cntId_local = nf.Local(pos, nf.Id(pos, "cntId"));
                Expr inc = iv.incrementCounter(cntId_local);
                MethodDecl md = (MethodDecl) iv.qq().parseMember(fmt, trampId,
                        ts.Int(), inc);
                iv.addTrampoline(md);
            }
            // add call before return statement
            iv.addStmt(nf.Eval(pos, iv.callTrampoline(trampId, args)));
            return ret;
        }
    }
}
