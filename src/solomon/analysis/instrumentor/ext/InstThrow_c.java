package solomon.analysis.instrumentor.ext;

import java.util.ArrayList;
import java.util.List;

import polyglot.ast.ArrayInit;
import polyglot.ast.Expr;
import polyglot.ast.IntLit;
import polyglot.ast.Local;
import polyglot.ast.MethodDecl;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.Throw;
import polyglot.ast.TypeNode;
import polyglot.types.ArrayType;
import polyglot.types.Type;
import polyglot.types.TypeSystem;
import polyglot.util.Position;
import solomon.analysis.instrumentor.InstrumentingVisitor;

public class InstThrow_c extends InstStmt_c {
    private static final long serialVersionUID = 1L;

    public InstThrow_c(boolean instrument) {
        super(instrument);
    }

    @Override
	public Node instrument(InstrumentingVisitor iv) {
		Throw th = (Throw) node();
		NodeFactory nf = iv.nodeFactory();

		Position pos = Position.compilerGenerated();

		Expr rhs = th.expr();
		Type t = rhs.type();

		String t_name = t.translate(null);
		t_name = t_name.replace(".", "$");

		if(t.isArray()) {
			t_name = t_name.replace("[]", "$");
			if(rhs instanceof ArrayInit) {
				ArrayType at = (ArrayType) t;
				TypeNode tn = nf.CanonicalTypeNode(pos, at.base());
				rhs = nf.NewArray(pos, tn, at.dims(), (ArrayInit)rhs);
			}
		}
		String trampId = "count$return$" + t_name;
		
		if(!iv.hasTrampoline(trampId)) {
			TypeSystem ts = iv.typeSystem();
			String fmt = "public static %T %s(%T cntId, %T val) {" +
							"%E;" + 
							"return val;" +
						 "}";
			Local cntId_local = nf.Local(pos, nf.Id(pos, "cntId"));
			Expr inc = iv.incrementCounter(cntId_local);

			MethodDecl md = (MethodDecl) iv.qq().parseMember(fmt, t, trampId,
					ts.Int(), t, inc);
			iv.addTrampoline(md);
		}
				
		List<Expr> args = new ArrayList<Expr>(3);
		args.add(nf.IntLit(pos, IntLit.INT, cntId));
		args.add(rhs);
		
		//set RHS to call to trampoline
		return th.expr(iv.callTrampoline(trampId, args));
	}
}
