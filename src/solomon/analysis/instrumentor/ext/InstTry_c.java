package solomon.analysis.instrumentor.ext;


import java.util.ArrayList;
import java.util.List;

import polyglot.ast.Block;
import polyglot.ast.Eval;
import polyglot.ast.Expr;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.Stmt;
import polyglot.ast.Try;
import polyglot.util.Position;
import solomon.analysis.instrumentor.InstrumentingVisitor;

public class InstTry_c extends InstStmt_c {
    private static final long serialVersionUID = 1L;

    public InstTry_c(boolean instrument) {
        super(instrument);
    }

	@Override
	public Node instrument(InstrumentingVisitor iv) {
		NodeFactory nf = iv.nodeFactory();
		Try s = (Try) node();
		
		Block tb = s.tryBlock();
		Expr try_cnt = iv.incrementCounter(cntId);		
		
		Position pos = Position.compilerGenerated();	
		Eval inc1 = nf.Eval(pos, try_cnt);
		List<Stmt> t_stmts = new ArrayList<Stmt>(tb.statements().size() +1); 
		t_stmts.add(inc1);
		t_stmts.addAll(tb.statements());
		s = s.tryBlock(tb.statements(t_stmts));
		return s;
	}	
}
