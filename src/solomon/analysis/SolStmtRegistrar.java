package solomon.analysis;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import polyglot.ast.Call;
import polyglot.ast.Expr;
import polyglot.ast.Term;
import polyglot.frontend.ExtensionInfo;
import polyglot.types.ArrayType;
import polyglot.types.ClassType;
import polyglot.types.ConstructorInstance;
import polyglot.types.MemberInstance;
import polyglot.types.MethodInstance;
import polyglot.types.ProcedureInstance;
import polyglot.types.ReferenceType;
import polyglot.types.Type;
import polyglot.util.InternalCompilerError;
import polyglot.util.Position;
import accrue.analysis.ext.ExtAlloc;
import accrue.analysis.ext.ExtCall;
import accrue.analysis.ext.ExtExpr;
import accrue.analysis.ext.ExtProcedureCall;
import accrue.analysis.ext.ObjanalExt;
import accrue.analysis.ext.ObjanalExt_c;
import accrue.analysis.interprocanalysis.NodeIdentifier;
import accrue.analysis.pointer.AllocSiteNode;
import accrue.analysis.pointer.CallSiteNode;
import accrue.analysis.pointer.ConstructorContext;
import accrue.analysis.pointer.HContext;
import accrue.analysis.pointer.LiteralNode;
import accrue.analysis.pointer.LocalNode;
import accrue.analysis.pointer.RegisterPointerStmtsVisitor;
import accrue.analysis.pointer.StmtAllocToLocal;
import accrue.analysis.pointer.StmtConstructorCall;
import accrue.analysis.pointer.StmtRegistrar;
import accrue.analysis.pointer.StmtStaticMethodCall;
import accrue.analysis.pointer.StmtVirtualMethodCall;

public class SolStmtRegistrar extends StmtRegistrar {
    public static final String beanField = "$BEAN$";
    public static final String stubsField = "$STUBS$";

	protected Map<HContext, NodeIdentifier> allocNodeIDs;
	protected Map<String, NodeIdentifier> pseudoAllocIDs;
    public SolStmtRegistrar(ExtensionInfo extInfo) {
        super(extInfo);
        this.allocNodeIDs = new HashMap<HContext, NodeIdentifier>();
        this.pseudoAllocIDs = new HashMap<String, NodeIdentifier>();
    }

    
    public void registerArrayClone(Call c, CodeInfo codeInfo, ConstructorContext constructorContext) {
        if (!(c.target().type().isArray() && c.methodInstance().name().equals("clone") &&
                c.methodInstance().formalTypes().isEmpty())) {
            throw new InternalCompilerError("Wrong method...");
        }
        ExtCall extClone = (ExtCall)ObjanalExt_c.ext(c);
        ArrayType type = (ArrayType)c.target().type();

        // "c = new"
        ClassType container = ((MemberInstance)codeInfo.cd.codeInstance()).container().toClass();
        this.addStmt(new StmtAllocToLocal(extClone.getLocalNode(constructorContext), freshAllocNode(type, container, c.toString(), c.position(), extClone), c.position(), codeInfo.cd));

        // add assignments so that the contents of the clone point to the same things as the contents
        // of the original
        ExtExpr extArray = (ExtExpr)ObjanalExt_c.ext(c.target());

        LocalNode temp = freshLocalNode("Clone temp", extClone.getLocalNode(constructorContext).isInStaticContext(), type.base(), c.position());

        // temp = array.contents
        // c.contents = temp
        addAssignment(temp, extArray.getLocalNode(constructorContext), ARRAY_CONTENTS, type.base(), c.position(), codeInfo);
        addAssignmentToField(extClone.getLocalNode(constructorContext), ARRAY_CONTENTS, type.base(), temp, c.position(), codeInfo);
    }

    public void registerLiteral(Expr e, CodeInfo codeInfo, ConstructorContext constructorContext) {
        ClassType container = ((MemberInstance)codeInfo.cd.codeInstance()).container().toClass();
        ExtExpr ext = (ExtExpr)ObjanalExt_c.ext(e);
        LiteralNode sln = freshLitNode(e.type().toClass(), container, e.toString(), e.position());
		this.addStmt(new SolStmtAllocToLocal(ext
				.getLocalNode(constructorContext), sln, e.position(),
				codeInfo.cd, ext, constructorContext));
    }
    
    public LocalNode registerLiteral(ClassType type, String debugString, CodeInfo codeInfo, ConstructorContext constructorContext, Position pos) {
    	throw new UnsupportedOperationException();
    }

    public LocalNode registerLiteral(ClassType type, String debugString, CodeInfo codeInfo, ConstructorContext constructorContext, Position pos, ObjanalExt ext) {
        ClassType container = ((MemberInstance)codeInfo.cd.codeInstance()).container().toClass();
        LiteralNode sln = freshLitNode(type, container, debugString, pos);
        LocalNode ln = freshLocalNode(debugString+"'", false, type, pos);
        this.addStmt(new SolStmtAllocToLocal(ln, sln, null, codeInfo.cd, ext, constructorContext));
        return ln;
    }
    
	public void registerAllocationID(HContext context, NodeIdentifier allocNodeID) {
		allocNodeIDs.put(context, allocNodeID);
	}
	
	public NodeIdentifier getAllocSiteNodeID(HContext context) {
		if(allocNodeIDs.containsKey(context))
			return allocNodeIDs.get(context);
		throw new InternalCompilerError(
				"Heap context "
						+ context
						+ " has unknown allocation site.  This is most likely caused by " +
						"a LibraryPointsToSignature signature that calls " +
						"HeapAbstractionFactory.record without registering a NodeIdentifier" +
						" for the allocation site.");
	}
	
	/**
	 * Utility method for adding exceptions
	 */
	public void addGeneratedExceptionAssignments(
	        Set<ClassType> exceptionTypes, Term cause, RegisterPointerStmtsVisitor v) {
		ObjanalExt causeExt = ObjanalExt_c.ext(cause);
		Position origin = cause.node().position();
		for (ClassType exType : exceptionTypes) {
			LocalNode ln;
			if (USE_SINGLE_ALLOC_NODE_FOR_GENERATED_EXCEPTIONS) {
				Map<ClassType, LocalNode> gen = generatedExceptionNodes
				.get(exType.typeSystem().extensionInfo());
				if (gen == null) {
					gen = new LinkedHashMap<ClassType, LocalNode>();
					generatedExceptionNodes.put(exType.typeSystem()
							.extensionInfo(), gen);
				}
				ln = gen.get(exType);
				if (ln == null) {
					ln = registerStaticAlloc(exType, "generated-"
							+ exType.name(), v.currentCode(), origin, causeExt);
					gen.put(exType, ln);
				}
			}
			else {
				// The exception may be generated, so it is an allocation site
				ln = registerAlloc(exType, origin, "generated-"
						+ exType.name(), v.currentCode(), causeExt, v.constructorContext());
			}
			addExceptionAssignments(exType, ln, origin, v);
		}
	}

	public void registerStaticPseudoAllocationID(String pseudo, NodeIdentifier nodeID) {
		pseudoAllocIDs.put(pseudo, nodeID);		
	}
	
	public boolean isStaticPseudoLocation(String pseudoloc) {
		return pseudoAllocIDs.containsKey(pseudoloc);
	}
	
	public boolean isPseudoLocation(HContext context, String pseudoloc) {
	    return pseudoAllocIDs.containsKey(pseudoloc);
	}

	public NodeIdentifier getPseudoAllocationID(String pseudoloc) {
		if(pseudoAllocIDs.containsKey(pseudoloc))
			return pseudoAllocIDs.get(pseudoloc);
		throw new InternalCompilerError(
				"No allocation node identifier registered for " + pseudoloc);
	}

    // Call site registration
    @Override
    @Deprecated
    protected CallSiteNode freshCallSiteNode(ProcedureInstance callee, String debugString, Position pos) {
        throw new UnsupportedOperationException();
    }
    
    protected CallSiteNode freshCallSiteNode(ProcedureInstance callee, String debugString, Position pos, ObjanalExt ext) {
        return new SolCallSiteNode(callee, debugString, pos, ext.node());
    }

    @Override
    public void addStaticCall(LocalNode result, MethodInstance mi,
            ExtProcedureCall ext, List<LocalNode> actuals,
            List<ExceptionHandlerLocalNode> exceptionHandlers,
            Map<Type, LocalNode> procedureThrows, Position origin,
            CodeInfo codeInfo) {
        CallSiteNode callSiteNode = freshCallSiteNode(mi, "static call", origin, ext);
        ext.setCallSiteNode(callSiteNode);
        this.addStmt(new StmtStaticMethodCall(result, callSiteNode, mi, actuals, exceptionHandlers, procedureThrows, origin, codeInfo.cd));
    }

    @Override
    public void addConstructorCall(LocalNode receiver, ConstructorInstance ci,
            ExtProcedureCall ext, List<LocalNode> actuals,
            List<ExceptionHandlerLocalNode> exceptionHandlers,
            Map<Type, LocalNode> procedureThrows, Position origin,
            CodeInfo codeInfo) {
        CallSiteNode callSiteNode = freshCallSiteNode(ci, "constructor call", origin, ext);
        ext.setCallSiteNode(callSiteNode);
        this.addStmt(new StmtConstructorCall(receiver, callSiteNode,  ci, actuals, exceptionHandlers, procedureThrows, origin, codeInfo.cd));                                         
    }

    @Override
    public void addVirtualCall(LocalNode result, LocalNode receiver,
            MethodInstance pi, ExtProcedureCall ext, List<LocalNode> actuals,
            List<ExceptionHandlerLocalNode> exceptionHandlers,
            Map<Type, LocalNode> procedureThrows, Position origin,
            CodeInfo codeInfo) {
        CallSiteNode callSiteNode = freshCallSiteNode(pi, "virtual call", origin, ext);
        ext.setCallSiteNode(callSiteNode);
//        this.addStmt(new EJBStmtVirtualMethodCall(result, receiver, callSiteNode, pi, actuals, exceptionHandlers, procedureThrows, origin, codeInfo.cd));                                         
      this.addStmt(new StmtVirtualMethodCall(result, receiver, callSiteNode, pi, actuals, exceptionHandlers, procedureThrows, origin, codeInfo.cd));
    }
    
    // Allocation site registration
    @Override
    @Deprecated
    protected AllocSiteNode freshAllocNode(Type type,
            ClassType containingClass, String debugString, Position pos) {
        throw new UnsupportedOperationException();
    }

    protected AllocSiteNode freshAllocNode(Type type,
            ClassType containingClass, String debugString, Position pos, ObjanalExt ext) {
        return new SolAllocSiteNode(type, containingClass, debugString, pos, ext.node());
    }
    
    protected AllocSiteNode freshStaticAllocNode(Type type, ClassType containingClass, String debugString, Position pos, ObjanalExt ext) {
        return new SolAllocSiteNode(type, containingClass, true, debugString, pos, ext.node());
    }

    @Override
    public void registerAlloc(ExtAlloc ext, Type allocatedType, CodeInfo codeInfo, ConstructorContext constructorContext) {
        String name = allocatedType.toString();
        if (allocatedType.isClass()) name = allocatedType.toClass().name();
        registerAlloc(ext, allocatedType, name, codeInfo, constructorContext);
    }
    @Override
    public void registerAlloc(ExtAlloc ext, Type allocatedType, String debugString, CodeInfo codeInfo, ConstructorContext constructorContext) {
        ClassType container = ((MemberInstance)codeInfo.cd.codeInstance()).container().toClass();
        ext.setAllocNode(freshAllocNode(allocatedType, container, debugString, ext.node().position(), ext), constructorContext);
        ExtExpr extExpr = (ExtExpr)ext;
        this.addStmt(new SolStmtAllocToLocal(extExpr
                .getLocalNode(constructorContext), ext
                .getAllocNode(constructorContext), ext.node().position(),
                codeInfo.cd, ext, constructorContext));
    }

    /**
     * This method is deprecated in favor of one that passes additional information about the allocation site.
     */
    @Override
    @Deprecated
    public LocalNode registerAlloc(ReferenceType type, Position origin, String debugString, CodeInfo codeInfo) {
        throw new UnsupportedOperationException();
    }

    public LocalNode registerAlloc(ReferenceType type, Position origin, String debugString, CodeInfo codeInfo, ObjanalExt ext, ConstructorContext constructorContext) {
        ClassType container = ((MemberInstance)codeInfo.cd.codeInstance()).container().toClass();
        AllocSiteNode asn = freshAllocNode(type, container, debugString, origin, ext);
        LocalNode ln = freshLocalNode(debugString+"'", false, type, origin);
        this.addStmt(new SolStmtAllocToLocal(ln, asn, origin, codeInfo.cd, ext, constructorContext));
        return ln;
    }
    
    /**
     * This method is deprecated in favor of one that passes additional information about the allocation site.
     */
    @Override
    @Deprecated
    public LocalNode registerStaticAlloc(Type type, String debugString, CodeInfo codeInfo, Position pos) {
        throw new UnsupportedOperationException();
    }

    /**
     * This method is deprecated in favor of one that passes additional information about the allocation site.
     */
    public LocalNode registerStaticAlloc(Type type, String debugString, CodeInfo codeInfo, Position pos, ObjanalExt ext) {
        ClassType container = ((MemberInstance)codeInfo.cd.codeInstance()).container().toClass();
        AllocSiteNode asn = freshStaticAllocNode(type, container, debugString, pos, ext);
        LocalNode ln = freshLocalNode(debugString+"'", true, type, pos);
        this.addStmt(new SolStmtAllocToLocal(ln, asn, null, codeInfo.cd, ext, ConstructorContext.NORMAL));
        return ln;
    }
}
