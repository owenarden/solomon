package solomon.analysis.partition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import polyglot.ast.Block;
import polyglot.ast.Catch;
import polyglot.ast.CodeDecl;
import polyglot.ast.CompoundStmt;
import polyglot.ast.If;
import polyglot.ast.Loop;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.Stmt;
import polyglot.ast.Term;
import polyglot.ast.Try;
import polyglot.frontend.Job;
import polyglot.types.SemanticException;
import polyglot.types.TypeSystem;
import polyglot.visit.ErrorHandlingVisitor;
import polyglot.visit.NodeVisitor;
import solomon.SolJobExt;

public class RegisterBlocks extends ErrorHandlingVisitor {
    protected final Map<Stmt, Term> blockMap;
    protected final Map<Stmt, List<Stmt>> siblingsMap;

    protected Block currentBlock;

    public RegisterBlocks(Job job, TypeSystem ts, NodeFactory nf) {
        super(job, ts, nf);
        SolJobExt sje = (SolJobExt) job.ext();
        this.blockMap = sje.blockMap();
        this.siblingsMap = sje.siblingsMap();
    }

    @Override
    protected Node leaveCall(Node parent, Node old, Node n, NodeVisitor v)
            throws SemanticException {
        List<Stmt> contents = new ArrayList<Stmt>();
        if (n instanceof CompoundStmt || n instanceof CodeDecl) {
            Term container = (Term) n;
            if (n instanceof If) {
                If i = (If) n;
                // Register stmts
                if (i.consequent() instanceof Block) {
                    List<Stmt> stmts = ((Block) i.consequent()).statements();
                    for (Stmt s : stmts) {
                        siblingsMap.put(s, stmts);
                    }
                    contents.addAll(stmts);
                } else {
                    contents.add(i.consequent());
                    siblingsMap.put(i.consequent(),
                            Collections.<Stmt> singletonList(i.consequent()));
                }
                if (i.alternative() instanceof Block) {
                    List<Stmt> stmts = ((Block) i.alternative()).statements();
                    for (Stmt s : stmts) {
                        siblingsMap.put(s, stmts);
                    }
                    contents.addAll(stmts);
                } else {
                    contents.add(i.alternative());
                    siblingsMap.put(i.alternative(),
                            Collections.<Stmt> singletonList(i.alternative()));
                }
            } else if (n instanceof Loop) {
                Loop l = (Loop) n;
                if (l.body() instanceof Block) {
                    List<Stmt> stmts = ((Block) l.body()).statements();
                    contents.addAll(stmts);
                    for (Stmt s : stmts) {
                        siblingsMap.put(s, stmts);
                    }
                } else {
                    contents.add(l.body());
                    siblingsMap.put(l.body(),
                            Collections.<Stmt> singletonList(l.body()));
                }
            } else if (n instanceof Try) {
                Try t = (Try) n;
                contents.addAll(t.tryBlock().statements());
                for (Stmt s : t.tryBlock().statements()) {
                    siblingsMap.put(s, t.tryBlock().statements());
                }
                if (t.catchBlocks() != null) {
                    for (Catch c : t.catchBlocks()) {
                        contents.addAll(c.body().statements());
                        for (Stmt s : c.body().statements()) {
                            siblingsMap.put(s, c.body().statements());
                        }
                    }
                }
                if (t.finallyBlock() != null) {
                    contents.addAll(t.finallyBlock().statements());
                    for (Stmt s : t.finallyBlock().statements()) {
                        siblingsMap.put(s, t.finallyBlock().statements());
                    }
                }
            } else if (n instanceof CodeDecl) {
                CodeDecl cd = (CodeDecl) n;
                if (cd.body() != null) {
                    contents.addAll(cd.body().statements());
                    for (Stmt s : cd.body().statements()) {
                        siblingsMap.put(s, cd.body().statements());
                    }
                }
            }
            for (Stmt s : contents)
                blockMap.put(s, container);
        }

        if (n instanceof Block) {
            if (!(parent instanceof If || parent instanceof Loop
                    || parent instanceof Try || parent instanceof CodeDecl)) {
                throw new Error("Nested block?");
            }
        }
        return n;
    }
}
