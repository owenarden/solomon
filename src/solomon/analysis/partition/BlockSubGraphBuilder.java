package solomon.analysis.partition;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import polyglot.ast.Block;
import polyglot.ast.ClassDecl;
import polyglot.ast.CodeDecl;
import polyglot.ast.If;
import polyglot.ast.Loop;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.Stmt;
import polyglot.ast.Term;
import polyglot.ast.Try;
import polyglot.frontend.Job;
import polyglot.main.Report;
import polyglot.types.SemanticException;
import polyglot.types.TypeSystem;
import polyglot.util.InternalCompilerError;
import polyglot.visit.ErrorHandlingVisitor;
import polyglot.visit.NodeVisitor;
import solomon.SolExtensionInfo;
import solomon.SolJobExt;
import solomon.analysis.partition.ext.PGExt;
import solomon.analysis.partition.ext.PGUtil;
import solomon.analysis.partition.graph.ContextFreePG;
import solomon.analysis.partition.graph.PGEdge;
import solomon.analysis.partition.graph.PGLocation;
import solomon.analysis.partition.graph.PGNode;
import solomon.analysis.partition.graph.PartitionGraph;
import solomon.analysis.signatures.SolAnalysisSignatures.DBLocation;
import accrue.analysis.interprocanalysis.AnalysisContext;
import accrue.analysis.interprocanalysis.NodeIdentifier;
import accrue.analysis.interprocanalysis.NodeIdentifier_c;

public class BlockSubGraphBuilder extends ErrorHandlingVisitor {
    private static int subcnt = 0;
    public static final String subgraphNode = "$subgraph$";
    protected final PartitionGraph pg;
    protected ContextFreePG curSubGraph;
    protected Set<PGEdge> externalEdges;
    protected final Map<Stmt, Term> blockMap;
    protected final Map<Stmt, List<Stmt>> siblingsMap;
    protected final AnalysisContext ALL_CONTEXTS;

    public BlockSubGraphBuilder(Job job, TypeSystem ts, NodeFactory nf,
            PartitionGraph pg) {
        super(job, ts, nf);
        this.pg = pg;
        SolJobExt sje = (SolJobExt) job.ext();
        this.blockMap = sje.blockMap();
        this.siblingsMap = sje.siblingsMap();
        ALL_CONTEXTS = ((SolExtensionInfo) job.extensionInfo()).anyContext();
    }

    @Override
    protected NodeVisitor enterCall(Node parent, Node n)
            throws SemanticException {
        if (n instanceof ClassDecl && ((ClassDecl) n).flags().isInterface())
            return bypassChildren(n);

        if (n instanceof CodeDecl) {
            BlockSubGraphBuilder v = (BlockSubGraphBuilder) copy();
            v.curSubGraph = new ContextFreePG(ALL_CONTEXTS);
            return v;
        } else if (n instanceof If || n instanceof Loop || n instanceof Try) {
            BlockSubGraphBuilder v = (BlockSubGraphBuilder) copy();
            v.curSubGraph = new ContextFreePG(ALL_CONTEXTS);
            v.externalEdges = new LinkedHashSet<PGEdge>();
            return v;
        } else
            return this;
    }

    @Override
    protected Node leaveCall(Node parent, Node old, Node n, NodeVisitor v)
            throws SemanticException {

        PGExt ext = PGUtil.ext(n);
        if (n instanceof Block)
            return n;

        boolean isContainer = n instanceof If || n instanceof Loop
                || n instanceof Try || n instanceof CodeDecl;

        if (n instanceof Stmt && ext.pgNode() != null) {
            // Find block from parent containing this node.
            PGNode pgNode = ext.pgNode();
            
//            --XXX-- Now use pgnode key to create separate "subgraph" pg nodes.
//                    --- use these for lookups in comparator.
            PGNode subpgNode = curSubGraph.createNode(subgraphNode, pgNode.nodeID(), pgNode.location());

            List<Stmt> siblings = siblingsMap.get(n);

            Term container = blockMap.get(n);
            PGExt cext = PGUtil.ext(container);
            if (cext == null)
                throw new Error("No ext");
                
            int idx = siblings.indexOf(n);

            // next, handle edges from children
            if (isContainer) {
                BlockSubGraphBuilder bsgb = (BlockSubGraphBuilder) v;
                for (Iterator<PGEdge> it = bsgb.externalEdges.iterator(); it
                        .hasNext();) {
                    PGEdge e = it.next();
                    // assert e.from() == node | child of node
                    // is a container of e.to() a sibling?
                    Term s = e.to().nodeID().getNode();
                    while (!(s instanceof CodeDecl) && !siblings.contains(s)) {
                        s = blockMap.get(s);
                    }
                    int sibIdx = siblings.indexOf(s);
                    if (sibIdx < 0) {
                        externalEdges.add(e);
                    } else if (idx >= sibIdx) {
//                        System.err.println("Ignoring edge from " + e.from()
//                                + " to " + e.to());

                        continue;
                    } else {
                        NodeIdentifier nodeID = new NodeIdentifier_c(
                                ALL_CONTEXTS, s);
                        PGNode newTo = curSubGraph.createNode(subgraphNode, nodeID, e.to().location());
//                        System.err.println("External edge from " + e.from()
//                                + " to " + e.to());
//                        System.err.println("\t mapped to edge from " + pgNode
//                                + "[" + idx + "]" + " to " + newTo + "["
//                                + sibIdx + "]");


                        curSubGraph.createEdge(subpgNode, newTo, e.key());
                    }
                }
            }

            // assert idx >= 0
            for (PGEdge edge : pg.outgoing(pgNode)) {
                if (!edge.key().isIntra())
                    continue;

                Term s = edge.to().nodeID().getNode();
                while (!(s instanceof CodeDecl) && !siblings.contains(s)) {
                    s = blockMap.get(s);
                }
                int sibIdx = siblings.indexOf(s);
                if (sibIdx < 0) {
                    externalEdges.add(edge);
                } else if (idx >= sibIdx) {
//                    System.err.println("Ignoring edge from " + edge.from()
//                            + " to " + edge.to());
                    continue;
                } else {
                    // edge connects a node inside this block
                    // and is not a back edge
                    NodeIdentifier nodeID = new NodeIdentifier_c(ALL_CONTEXTS,
                            s);
                    PGNode newTo = curSubGraph.createNode(subgraphNode, nodeID, edge.to()
                            .location());
//                    System.err.println("Descendent edge from " + edge.from()
//                            + " to " + edge.to());
//                    System.err.println("\t mapped to edge from " + pgNode + "["
//                            + idx + "]" + " to " + newTo + "[" + sibIdx + "]");

                    curSubGraph.createEdge(subpgNode, newTo, edge.key());
                }
            }
        }
        if (isContainer) {
            BlockSubGraphBuilder bsgb = (BlockSubGraphBuilder) v;
            if (!bsgb.curSubGraph.nodes().isEmpty()) {
                Set<PGNode> roots = new LinkedHashSet<PGNode>();
                for (PGNode node : bsgb.curSubGraph.nodes()) {
                    if (bsgb.curSubGraph.incoming(node).isEmpty())
                        roots.add(node);
                }
                assignDep(roots, ext.pgNode().location(), bsgb.curSubGraph);                
            }
        }
        return n;
    }

    private void assignDep(Set<PGNode> roots, PGLocation loc,
            PartitionGraph subgraph) {
        Deque<PGNode> clientNodes = new ArrayDeque<PGNode>();
        Deque<PGNode> serverNodes = new ArrayDeque<PGNode>();

        // set initial preference by codedecl
        boolean preferClient = loc.equals(DBLocation.APPSERVER);
        for (PGNode root : roots) {
            if (root.location().equals(DBLocation.APPSERVER)) {
                clientNodes.add(root);
            } else if (root.location().equals(DBLocation.DBSERVER)) {
                serverNodes.add(root);
            } else {
                throw new InternalCompilerError("Unexpected location: " + loc);
            }
        }
        int count = 0;
        Set<PGEdge> seen = new HashSet<PGEdge>();

        Set<PGNode> waiting = new HashSet<PGNode>();
        // Santity check :
        while (!clientNodes.isEmpty() || !serverNodes.isEmpty()) {
            PGNode next;
            if (preferClient) {
                if (clientNodes.isEmpty()) {
                    // assert: serverNodes is non-empty
                    preferClient = false;
                    next = serverNodes.pop();
                } else {
                    next = clientNodes.pop();
                }
            } else {
                if (serverNodes.isEmpty()) {
                    // assert: clientnodes is non-empty
                    preferClient = true;
                    next = clientNodes.pop();
                } else {
                    next = serverNodes.pop();
                }
            }

            if (next.dependencyOrder() >= 0)
                throw new InternalCompilerError("Dependency order for " + next
                        + " already set: " + next.dependencyOrder());
            next.setDependencyOrder(count++);
//            System.err.println("Next: " + next + " " + next.dependencyOrder());

            for (PGEdge edge : subgraph.outgoing(next)) {
                if (!seen.contains(edge)) {
                    seen.add(edge);
                    if (!hasUnseenInEdges(subgraph, seen, edge.to())) {
                        PGLocation toLoc = edge.to().location();
                        waiting.remove(edge.to());
                        if (toLoc.equals(DBLocation.APPSERVER)) {
                            clientNodes.add(edge.to());
                        } else if (toLoc.equals(DBLocation.DBSERVER)) {
                            serverNodes.add(edge.to());
                        } else {
                            throw new InternalCompilerError(
                                    "Unexpected location: " + toLoc);
                        }
                    } else {
                        waiting.add(edge.to());
                    }
                }
            }
        }
        if (Report.should_report("pdgDotFiles", 1)) {
            createDotFile(subgraph);
        }
        if (!waiting.isEmpty()) {
            dumpWaitingNodes(subgraph, waiting, seen);
            throw new InternalCompilerError("Still have remaining nodes");
        }
    }

    private PrintStream printDotHeader() {
        PrintStream out = null;
        String name = null;
        name = job.extensionInfo().scheduler().currentJob().toString()
                + "_sub_" + subcnt++;
        try {
            File f = new File(name + ".dot");
            System.out.println("WRITING DOT FILE : " + f.getAbsolutePath());
            out = new PrintStream(f);
            out.println("digraph InterProceduralCFG {");
            out.println("fontsize=20; center=true; ratio=compress;");
            out.println("node [fontname=\"Courier\"]");
            out.println("node [shape=note]");

        } catch (FileNotFoundException e) {
        }
        return out;
    }

    private void createDotFile(PartitionGraph subgraph) {
        PrintStream out = printDotHeader();
        for (PGNode next : subgraph.nodes()) {
            out.println(next.toDot(false, "" + next.dependencyOrder()));
        }
        for (PGEdge e : subgraph.edges()) {
            out.println(e.toDot(false));
        }
        out.println("}");
        out.close();
    }

    private void dumpWaitingNodes(PartitionGraph subgraph, Set<PGNode> waiting,
            Set<PGEdge> seen) {
        System.out.println("Waiting queue: ");
        for (PGNode w : waiting) {
            System.out.println("\tNode " + w + ":" + w.nodeID().hashCode()
                    + ":" + w.nodeID().getNode().position());
            System.out.println("\t\tUnseen edges: ");
            for (PGEdge e : subgraph.incoming(w)) {
                if (!seen.contains(e)) {
                    System.out.println("\t\t: " + e + " from " + e.from() + ":"
                            + e.from().nodeID().hashCode() + " edge hash: "
                            + e.hashCode());
                }
            }
        }
    }

    private boolean hasUnseenInEdges(PartitionGraph subgraph, Set<PGEdge> seen,
            PGNode to) {
        for (PGEdge e : subgraph.incoming(to)) {
            if (!seen.contains(e)) {
//                System.err.println("Node " + to + " has unseen edge from "
//                        + e.from());
                return true;
            }
        }
        return false;
    }
}
