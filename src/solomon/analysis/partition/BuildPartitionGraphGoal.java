/**
 * 
 */
package solomon.analysis.partition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import polyglot.frontend.Job;
import polyglot.frontend.Scheduler;
import polyglot.frontend.goals.Barrier;
import polyglot.frontend.goals.Goal;
import polyglot.frontend.goals.VisitorGoal;
import solomon.SolExtensionInfo;
import solomon.SolOptions;
import solomon.SolScheduler;

public class BuildPartitionGraphGoal extends Barrier {
    
    public static BuildPartitionGraphGoal singleton(SolExtensionInfo extInfo) {
        Scheduler scheduler = extInfo.scheduler();
        return (BuildPartitionGraphGoal) scheduler
                .internGoal(new BuildPartitionGraphGoal(extInfo));
    }

    protected SolExtensionInfo extInfo;

    BuildPartitionGraphGoal(SolExtensionInfo extInfo) {
        super(extInfo.scheduler());
        this.extInfo = extInfo;
    }

    @Override
    public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
        List<Goal> l = new ArrayList<Goal>();
        l.add(((SolScheduler) scheduler).DefUseAnalysis());
        l.add(((SolScheduler) scheduler).IntraDefUseAnalysis());        
        l.add(((SolScheduler) scheduler).AntiDepAnalysis());
        l.add(((SolScheduler) scheduler).RegisterExpressions());
        l.add(((SolScheduler) scheduler).RegisterDecls());
        l.add(((SolScheduler) scheduler).ControlDeps());
        l.add(((SolScheduler) scheduler).PreciseCallGraph());

        if(((SolOptions)extInfo.getOptions()).load_profile)
            l.add(((SolScheduler) scheduler).LoadAllProfileData());

        l.addAll(super.prerequisiteGoals(scheduler));
        return l;
    }

    @Override
    public Goal goalForJob(Job job) {
        return scheduler.internGoal(new VisitorGoal(job,
                new ContextFreePGBuilder(extInfo)));
    }
}
