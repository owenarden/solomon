package solomon.analysis.partition.graph;

import java.util.HashMap;
import java.util.Map;

import polyglot.util.InternalCompilerError;
import solomon.constraints.AbstractSolveable;
import solomon.constraints.Constraint;
import solomon.constraints.Solveable;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import solomon.constraints.SolverVariable;

public interface PGLocation {
	PGLocation ANY = new PGLocation() {
		public String toString() { return "ANY"; }
		@Override
		public Constraint locationConstraint(SolverUtil sutil, SolverVariable var) { return null; }
		public String toPlacement() { return "?"; }
		public boolean equals(Object o) {
		    return o == this;
		}
	};
	
	@SuppressWarnings("serial")
    Map<Integer, PGLocation> ID_MAP = new HashMap<Integer, PGLocation>() {
		@Override
		public PGLocation put(Integer id, PGLocation loc) {
			if(containsKey(id) && !get(id).equals(loc))
				throw new InternalCompilerError("Tried to insert " + loc
						+ " for location id, but " + get(id)
						+ " already has that id.");
			return super.put(id, loc); 
		}
	};
	
	/**
	 *  Impose a constraint on the solverVariable var based on this location.
	 * @param sutil TODO
	 * @throws SolverException 
	 */
	Constraint locationConstraint(SolverUtil sutil, SolverVariable var) throws SolverException;	
	
	/**
	 *  Return the string representing the PyxIL placement for this location.
	 */
	String toPlacement();
	
    /**
     */
    public static class SolveableLocation extends AbstractSolveable implements Solveable,PGLocation {
        protected final String id;
        protected PGLocation loc;

        public SolveableLocation(String id) {
            this.id = id;
        }
        
        @Override
        public void applySolution(SolverUtil su, double value)
                throws SolverException {
            PGLocation loc = PGLocation.ID_MAP.get((int)value);            
            if(loc == null || (this.loc != null && !this.loc.equals(loc)))
                throw new InternalCompilerError("Invalid location value " + value + " for " + id);
            this.loc = loc;
        }
       
        @Override
        public boolean checkSolution(SolverUtil su) {
            return false;
        }

        @Override
        public String toDot(boolean useVars) {
            return id;
        }

        public String toVariableName() {
            return id;
        }

        @Override
        public Constraint locationConstraint(SolverUtil sutil, SolverVariable var) {
            return null;
        }
        
        @Override
        public String toPlacement() {
            return loc.toPlacement();
        }
        
        @Override
        public boolean equals(Object o) {
            if (o instanceof SolveableLocation) {
                SolveableLocation sl = (SolveableLocation) o;
                if(sl.id.equals(id))
                    return true;
                else if (loc != null) {
                    return loc.equals(sl.loc);
                }
            }
            else if (o instanceof PGLocation) {
                if (loc != null) {
                    return loc.equals(o);
                }
            }
            return false;
        }
        
        public int hashCode() {
            if (loc != null) {
                return loc.hashCode();
            }
            return id.hashCode();
        }
        
        @Override
        public String toString() {
            if (loc != null) 
                return id + "=" + loc.toString();
            else
                return id;
        }

        public PGLocation location() {
            return loc;
        }
    }   
}
