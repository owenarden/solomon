package solomon.analysis.partition.graph;

import solomon.constraints.Solveable;
import accrue.analysis.interprocanalysis.EdgeIdentifier.EdgeType;

public interface PGEdge extends Solveable {
	EdgeType key();

	PGNode from();

	PGNode to();
	
	double weight();
    void setWeight(double weight);

	boolean isCut();
	
	void setCut(boolean b);
}
