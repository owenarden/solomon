package solomon.analysis.partition.graph;

import polyglot.util.StringUtil;
import solomon.constraints.AbstractSolveable;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import accrue.analysis.interprocanalysis.EdgeIdentifier.EdgeType;

public class PGEdge_c extends AbstractSolveable implements PGEdge {
	
	protected final PGNode from;
	protected final PGNode to;
	protected double weight;
	protected final EdgeType edgeType;
	protected boolean cut;
	PartitionGraph graph;
	
    public PGEdge_c(PartitionGraph graph, PGNode from, PGNode to, EdgeType edgeType) {
		if(from == null || to == null || edgeType == null) 
			throw new NullPointerException();
		this.graph = graph;
		this.from = from;
		this.to = to;
		this.edgeType = edgeType;
	}
	
	@Override
	public PGNode from() {
		return graph.node(from);
	}
	
	@Override
	public PGNode to() {
		return graph.node(to);
	}
	
	public boolean equals(Object o) {
		if(o instanceof PGEdge_c) {
			PGEdge_c that = (PGEdge_c) o;
			return edgeType.equals(that.edgeType) 
				&& from().equals(that.from)
				&& to().equals(that.to);
		}
		else
			return false;
	}
	
	public int hashCode() {
		return edgeType.hashCode() ^ from.hashCode()
			^ to.hashCode();
	}
	
	public String toString() {
		return edgeType + (edgeType.isIntra() ? ":I:": "")+ "("+ weight +") [cut? : " + cut + "]";
	}
	
	@Override
	public EdgeType key() {
		return edgeType;
	}
	
	@Override
	public double weight() {
		return weight;
	}
	@Override
	public void setWeight(double w) {
	    this.weight = w;
	}
	
	@Override
	public void applySolution(SolverUtil su, double value) throws SolverException {
		cut = (value == 1.0);
	}

	@Override
	public boolean isCut() {
	    if (weight == 0.0) {
	        // value was potentially unconstrained.
	        return !from.location().equals(to.location());
	    }
		return cut;
	}
	
	@Override 
	public void setCut(boolean cut) {
	    this.cut = cut;
	}

	@Override
	public String toDot(boolean useVars) {
		String color = graph.color(this);
		String style = graph.style(this);
        String label = StringUtil.escape(graph.label(this));
        if (useVars)
            label = toVariableName() + ":" + label; 

        return from().hashCode() + " -> " + to().hashCode() + 
				"[" +
				(color != null ? ("color=\"" + color + "\"") : "") +
				(style != null ? ("style=\"" + style + "\"") : "") +
				(label != null ? ("label=\"" + label + "\"") : "") +
				"]";
	}

	@Override
	public boolean checkSolution(SolverUtil su) {
		return false;
	}
}
