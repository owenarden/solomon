package solomon.analysis.partition.graph;

import solomon.constraints.Solveable;
import accrue.analysis.interprocanalysis.NodeIdentifier;

public interface PGNode extends Solveable {

    /**
     * Set the location this node is assigned to.
     */
	void setLocation(PGLocation location);

    /**
     * The location this node is assigned to.
     */
	PGLocation location();

    /**
     * The weight associated with the node in the cost model.
     */
	double weight();
    void setWeight(double w);

	/**
	 * The program point this partition graph node represents. 
	 */
	NodeIdentifier nodeID();

    /**
     * Set the position of this node in the dependency order.
     */
    void setDependencyOrder(int ord);
    
    /**
     * The position of this node in the dependency order.
     */
    int dependencyOrder();

//  /**
//  * The id of the analysis unit (CodeDecl) of this node.
//  * @return
//  */
// int declId();
//
// /**
//  * Set the id of the analysis unit (CodeDecl) of this node.
//  */
// void setDeclId(int id);
//  /**
//  * The id of the analysis unit (CodeDecl) of this node.
//  * @return
//  */
// int declId();
//
// /**
//  * Set the id of the analysis unit (CodeDecl) of this node.
//  */
// void setDeclId(int id);
//  /**
//  * The id of the analysis unit (CodeDecl) of this node.
//  * @return
//  */
// int declId();
//
// /**
//  * Set the id of the analysis unit (CodeDecl) of this node.
//  */
// void setDeclId(int id);
//    throw new Error("Unset node order");

    String toDot(boolean useVars);
    String toDot(boolean useVars, String lbl);
}
