package solomon.analysis.partition.graph;

import java.util.Iterator;
import java.util.List;

import polyglot.ast.Block;
import polyglot.ast.CodeDecl;
import polyglot.ast.If;
import polyglot.ast.Initializer;
import polyglot.ast.Loop;
import polyglot.types.CodeInstance;
import polyglot.types.ConstructorInstance;
import polyglot.types.MethodInstance;
import polyglot.util.InternalCompilerError;
import polyglot.util.StringUtil;
import solomon.analysis.partition.graph.PGLocation.SolveableLocation;
import solomon.constraints.AbstractSolveable;
import solomon.constraints.Constraint;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import accrue.analysis.interprocanalysis.NodeIdentifier;

public class PGNode_c extends AbstractSolveable implements PGNode {
	protected final NodeIdentifier nodeID;
	protected double weight;
	protected final String label;
	protected int depOrd = -1;

	protected PGLocation loc;
	protected final PartitionGraph graph;
	
	public PGNode_c(PartitionGraph graph ,NodeIdentifier nodeID, PGLocation loc) {
		this.graph = graph;
		this.nodeID = nodeID;
		if(nodeID.isNative()) {
			label = nodeID.toString();//"NATIVE:" + printCodeInstance(((NativeNodeIdentifier) nodeID).getCodeInstance());
		}
		else if(nodeID.getNode() instanceof If) {
			If n = (If) nodeID.getNode();
			label = n.cond().toString();
		}
		else if(nodeID.getNode() instanceof Loop) {
			Loop n = (Loop) nodeID.getNode();
			label = n.cond().toString();
		}
		else if(nodeID.getNode() instanceof CodeDecl) {
			CodeDecl cd = (CodeDecl) nodeID.getNode();
			if (cd instanceof Initializer)
			    label = cd.body().statements().get(0).toString() + "...";
			else 
			    label = printCodeInstance(cd.codeInstance());
		}
		else {
			label = nodeID.getNode().toString();				
			if(nodeID.getNode() instanceof Block) {
				throw new InternalCompilerError("Tried to create PGNode for block:" + nodeID);
			}
		}
		this.loc = loc;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PGNode_c) {
			PGNode_c that = (PGNode_c) obj;
			return nodeID.equals(that.nodeID);
		} else 
			return false;
	}

	@Override
	public NodeIdentifier nodeID() {
		return nodeID;
	}
	
	@Override
	public int hashCode() {
		return nodeID.hashCode();
	}

	@Override
	public String toString() {
		return label;
	}	
	
	private static String printCodeInstance(CodeInstance ci) {
		if(ci instanceof MethodInstance) {
			MethodInstance mi = (MethodInstance) ci;
			return mi.container() + "." + mi.name() + printFormals(mi.formalTypes());
		}
		else if(ci instanceof ConstructorInstance) {
			ConstructorInstance ctor = (ConstructorInstance) ci;
			return ctor.container().toString() + printFormals(ctor.formalTypes());
		}
		else
			return ci.toString();
	}
	private static String printFormals(List<?> l) {
		StringBuilder sb = new StringBuilder("(");
		for(Iterator<?> it = l.iterator(); it.hasNext();) {
			sb.append(it.next());
			if(it.hasNext())
				sb.append(",");
		}
		sb.append(")");
		return sb.toString();
	}
	
	@Override
	public PGLocation location() {
		return loc;
	}

    @Override
	public void setLocation(PGLocation location) {
		this.loc = location;
	}
	    
    @Override
    public void setDependencyOrder(int ord) {
        this.depOrd = ord;
    }
    
    @Override
    public int dependencyOrder() {
        return depOrd;
    }
    
	private void checkLocation(PGLocation loc) {
		if(!this.loc.equals(PGLocation.ANY) && this.loc != null && !this.loc.equals(loc)) {
			System.out.println("Cannot set location of " + nodeID
					+ " to " + loc + ", it is already restricted to "
					+ this.loc);
            for (Constraint c : constraints()) {
                System.out.println(c.toString());
            }
            System.out.println("Node: " + solverVariable());
            throw new Error("Invalid partition");
		}
	}

	@Override
	public void applySolution(SolverUtil su, double value) throws SolverException {
	    if (location() instanceof SolveableLocation) {
	        SolveableLocation sl = (SolveableLocation) location();
	        sl.applySolution(su, value);
	        checkLocation(sl.location());
	        this.loc = sl.location();
	    }
	    else {
    		PGLocation loc = graph.location((int)value);
    		checkLocation(loc);
    		this.loc = loc;
	    }
	}

	@Override
	public double weight() {
		return weight;
	}

	@Override 
	public void setWeight(double w) {
	    this.weight = w;
	}
	    
	@Override
	public String toDot(boolean useVars) {
	    return toDot(useVars, "");
	}
	@Override
	public String toDot(boolean useVars, String lbl) {
		String color = graph.color(this);
		String style = graph.style(this);
        String label = StringUtil.escape(graph.label(this));
        if (useVars)
            label = toVariableName() + ":" + label; 
        if (!lbl.isEmpty())
            label += ":" + lbl;
        
		String shape = graph.shape(this);

		return hashCode() +
				"[" + 
				(color != null ? (" color=\"" + color + "\" ") : "") +
				(style != null ? (" style=\"" + style + "\" ") : "") +
				(label != null ? (" label=\"" + label + "\" ") : "") +
				(shape != null ? (" shape=\"" + shape + "\" ") : "") +
				"]";
	}
	
	@Override
	public boolean checkSolution(SolverUtil su) {
		return false;
	}

}
