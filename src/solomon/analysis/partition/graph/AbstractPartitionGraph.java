package solomon.analysis.partition.graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import polyglot.ast.Block;
import polyglot.ast.ClassDecl;
import polyglot.ast.CodeDecl;
import polyglot.ast.If;
import polyglot.ast.Loop;
import polyglot.ast.Term;
import polyglot.util.InternalCompilerError;
import solomon.analysis.partition.ext.PGExt;
import solomon.analysis.partition.ext.PGUtil;
import solomon.analysis.partition.graph.PGLocation.SolveableLocation;
import solomon.analysis.signatures.SolAnalysisSignatures.DBLocation;
import solomon.constraints.Constraint;
import solomon.constraints.ConstraintType;
import solomon.constraints.LinearTerm;
import solomon.constraints.Problem;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import solomon.constraints.SolverVariable;
import accrue.analysis.interprocanalysis.EdgeIdentifier;
import accrue.analysis.interprocanalysis.EdgeIdentifier.EdgeType;
import accrue.analysis.interprocanalysis.EdgeIdentifier.HeapUseKey;
import accrue.analysis.interprocanalysis.EdgeIdentifier.LocalUseKey;
import accrue.analysis.interprocanalysis.NodeIdentifier;

public abstract class AbstractPartitionGraph implements PartitionGraph {
    protected final Map<NodeIdentifier, PGNode> nodes;
    protected final Map<PGEdge, PGEdge> edges;
    protected final Map<PGNode, Set<PGEdge>> outgoing;
    protected final Map<PGNode, Set<PGEdge>> incoming;
    protected boolean print_constraints = true;
    protected Problem problem;

    public AbstractPartitionGraph() {
        this.nodes = new HashMap<NodeIdentifier, PGNode>();
        this.edges = new HashMap<PGEdge, PGEdge>();

        this.outgoing = new HashMap<PGNode, Set<PGEdge>>();
        this.incoming = new HashMap<PGNode, Set<PGEdge>>();

    }

    @Override
    public PGLocation location(int val) {
        PGLocation loc = PGLocation.ID_MAP.get(val);
        if (loc == null)
            throw new InternalCompilerError("No location for " + val);
        return loc;
    }

    @Override
    public double maximumObjective() {
        double sum = 0;
        for (PGEdge n : edges()) {
            sum += n.weight();
        }
        return sum;
    }

    @Override
    public double controlTransfers() {
        double sum = 0;
        for (PGEdge n : edges()) {
            if (controlEdge(n.key()) && n.isCut())
                sum++;
        }
        return sum;
    }

    @Override
    public double maximumBudget(PGLocation loc) {
        double sum = 0;
        for (PGNode n : nodes()) {
            if (n.location().equals(loc)
                    || n.location() instanceof SolveableLocation
                    || n.location().equals(PGLocation.ANY))
                sum += n.weight();
        }
        return sum;
    }

    @Override
    public double minimumBudget(PGLocation loc) {
        double sum = 0;
        for (PGNode n : nodes()) {
            if (n.location().equals(loc)) {
                double w = n.weight();
                sum += w;
            }
        }
        return sum;
    }

    @Override
    public double actualBudget(PGLocation loc) {
        double sum = 0;
        for (PGNode n : nodes()) {
            if (!n.nodeID().isNative()) {
                if (n.location().equals(loc)) {
                    sum += n.weight();
                }
            }
        }
        return sum;
    }

    // TODO
    // optimizing transformations
    // duplicate defs to reduce cost of multiple uses
    // duplicate data edges to piggy-back post-dominating uses on control
    // transfers
    @Override
    public Collection<PGNode> nodes() {
        return nodes.values();
    }

    @Override
    public Collection<PGEdge> edges() {
        return edges.values();
    }

    @Override
    public PGEdge createEdge(PGNode from, PGNode to, EdgeType edgeType) {
        from = node(from);
        to = node(to);
        PGEdge edge = new PGEdge_c(this, from, to, edgeType);
        if (!edges.containsKey(edge)) {
            edges.put(edge, edge);
            addIncoming(to, edge);
            addOutgoing(from, edge);
        }
        return edge(edge);
    }

    protected void addIncoming(PGNode to, PGEdge edge) {
        Set<PGEdge> in = incoming.get(to);
        if (in == null) {
            in = new HashSet<PGEdge>();
            incoming.put(to, in);
        }
        in.add(edge);
    }

    @Override
    public Set<PGEdge> incoming(PGNode to) {
        Set<PGEdge> in = incoming.get(to);
        if (in != null) {
            return Collections.unmodifiableSet(in);
        } else
            return Collections.<PGEdge> emptySet();
    }

    protected void addOutgoing(PGNode from, PGEdge edge) {
        Set<PGEdge> out = outgoing.get(from);
        if (out == null) {
            out = new HashSet<PGEdge>();
            outgoing.put(from, out);
        }
        out.add(edge);
    }

    @Override
    public Set<PGEdge> outgoing(PGNode from) {
        Set<PGEdge> out = outgoing.get(from);
        if (out != null)
            return Collections.unmodifiableSet(out);
        else
            return Collections.<PGEdge> emptySet();
    }

    @Override
    public final PGNode createNode(NodeIdentifier nodeID, PGLocation loc) {
        return createNode(PGExt.DEFAULT_PGNODE, nodeID, loc);
    }

    @Override
    public PGNode createNode(Object key, NodeIdentifier nodeID, PGLocation loc) {
        if (nodeID.getNode() instanceof CodeDecl) {
            CodeDecl cd = (CodeDecl) nodeID.getNode();
            if (cd.body() == null)
                throw new InternalCompilerError(
                        "Cannot create CodeDecl node w/ null body!");
        }
        PGNode node = new PGNode_c(this, nodeID, loc);
        if (!nodes.containsKey(nodeID)) {
            nodes.put(node.nodeID(), node);
            if (!node.nodeID().isNative()) {
                PGExt pgext = PGUtil.ext(node.nodeID().getNode());
                if (pgext == null)
                    throw new InternalCompilerError(
                            "Tried to create PGNode for node with no ext!"
                                    + ":" + node.nodeID() + "<>"
                                    + node.nodeID().getNode().getClass());
                pgext.setPGNode(key, node);
            }
        }
        return node(node);
    }

    @Override
    public PGNode lookup(NodeIdentifier nodeID) {
        return nodes.get(nodeID);
    }

    @Override
    public PGNode node(PGNode node) {
        PGNode ret = nodes.get(node.nodeID());
        if (ret == null)
            throw new NullPointerException();
        return ret;
    }

    @Override
    public PGEdge edge(PGEdge edge) {
        PGEdge ret = edges.get(edge);
        if (ret == null)
            throw new NullPointerException();
        return ret;
    }

    @Override
    public String color(PGEdge edge) {
        return edge.isCut() ? "red" : "black";
    }

    @Override
    public String color(PGNode node) {
        return getColor(node.location());
    }

    @Override
    public String label(PGEdge edge) {
        if (edge.key() instanceof HeapUseKey) {
            return ((edge.weight() > 1) ? "[" + edge.weight() + "]" : "")
                    + "HEAP";
        }
        return ((edge.weight() > 1) ? "[" + edge.weight() + "]" : "")
                + edge.key().toString();
    }

    @Override
    public String label(PGNode node) {
        Term ast_node = node.nodeID().getNode();
        if (ast_node instanceof If) {
            If ifn = (If) ast_node;
            return ifn.cond().toString();
        } else if (ast_node instanceof Loop) {
            Loop loop = (Loop) ast_node;
            return loop.cond().toString();
        } else if (ast_node instanceof Block) {
            return ast_node.toString();
            // return "{ ... }";
        } else if (ast_node == null) {
            return node.nodeID().toString();
        } else
            return ast_node.toString();
    }

    @Override
    public String style(PGEdge edge) {
        EdgeType k = edge.key();
        if (interDataEdge(k))
            return "dashed";
        else if (controlEdge(k))
            return "solid";
        else if (updateEdge(k))
            return "dotted";
        else if (antiEdge(k))
            return "invis";
        else if (outputEdge(k))
            return "invis";
        else if (intraDataEdge(k))
            return "dashed";
        else
            throw new InternalCompilerError("Unknown edge type: "
                    + edge.key().getClass());
    }

    @Override
    public boolean antiEdge(EdgeType k) {
        return k == EDGE_ANTI;
    }

    @Override
    public boolean outputEdge(EdgeType k) {
        return k == EDGE_OUTPUT;
    }

    @Override
    public boolean intraDataEdge(EdgeType k) {
        return dataEdge(k) && k.isIntra();
    }

    @Override
    public boolean interDataEdge(EdgeType k) {
        return dataEdge(k) && !k.isIntra();
    }

    @Override
    public boolean dataEdge(EdgeType k) {
        return k instanceof HeapUseKey || k instanceof LocalUseKey;
    }

    @Override
    public boolean controlEdge(EdgeType k) {
        return k == EdgeIdentifier.EDGE_CONTROL
                || k instanceof EdgeIdentifier.ExceptionEdge
                || k == EdgeIdentifier.EDGE_TRUE
                || k == EdgeIdentifier.EDGE_FALSE
                || k == EdgeIdentifier.EDGE_CALL
                || k == EdgeIdentifier.EDGE_RETURN
                || k == EdgeIdentifier.EDGE_INIT;
    }

    @Override
    public boolean updateEdge(EdgeType k) {
        return k == EDGE_UPDATE;
    }

    @Override
    public String style(PGNode node) {
        return "solid";
    }

    @Override
    public String shape(PGNode node) {
        Term ast_node = node.nodeID().getNode();
        if (ast_node instanceof If || ast_node instanceof Loop) {
            return "egg";
        }
        return "note";
    }

    public static void printConstraints(List<LinearTerm> objective,
            List<Constraint> constraints) {
        PrintStream out;
        try {
            out = new PrintStream(new File("constraints.txt"));
        } catch (FileNotFoundException e) {
            throw new InternalCompilerError(e);
        }
        out.println("Minimize:");
        StringBuilder sb = new StringBuilder();
        for (LinearTerm lt : objective) {
            sb.append(lt);
            sb.append("+");
        }
        out.println("\t" + sb.subSequence(0, sb.length() - 1));
        out.println("with respect to:");
        for (Constraint c : constraints) {
            out.println("\t" + c.toString());
        }
        out.close();
    }

    @Override
    public void printGraph(boolean useVars) {
        PrintStream out;
        try {
            out = new PrintStream(new File("ICFG.dot"));
            out.println("digraph InterProceduralCFG {");
            out.println("fontsize=20; center=true; ratio=compress;");
            out.println("node [fontname=\"Courier\"]");
            out.println("node [shape=note]");
            for (PGNode node : nodes.values()) {
                out.println(node.toDot(useVars));
                for (PGEdge edge : outgoing(node)) {
                    out.println(edge.toDot(useVars));
                }
            }
            // for(PGEdge edge: edges.keySet()) {
            // if (edge.key() == EdgeIdentifier.KEY_DECL
            // && !print_decl_edges )
            // continue;
            // out.println(edge.toDot(useVars));
            // }
            out.println("}");
            out.close();
        } catch (FileNotFoundException e) {
            throw new InternalCompilerError(e);
        }

    }

    Map<PGLocation, String> colors = new HashMap<PGLocation, String>();
    String[] color_wheel = { "blue", "green", "violet" };
    int color_idx = 0;

    String getColor(PGLocation loc) {
        if (PGLocation.ANY.equals(loc))
            return "black";

        String color = colors.get(loc);
        if (color == null) {
            color = new_color();
            colors.put(loc, color);
        }
        return color;
    }

    private String new_color() {
        return color_wheel[color_idx++];
    }

    @Override
    public void initColors(PGLocation... locations) {
        for (PGLocation loc : locations) {
            getColor(loc);
        }
    }

    public void fixNativeWeights() {
    }

    /**
     * Split each definition into two nodes: one as the target of all incoming
     * edges and one as the source of all outgoing edges. This enables an
     * optimization that reduces multiple data edge cuts when the data
     * dependency can be cached for subsequent uses.
     */
    public void splitDefinitions() {

    }

    @Override
    public boolean checkPartition(double objective, double server_budget) {
        double actualBudget = actualBudget(DBLocation.DBSERVER);
        if (actualBudget > server_budget) {
            throw new Error("Actual budget " + actualBudget
                    + " exceeds specified budget " + server_budget);
        }

        double realObjective = 0;
        for (PGEdge edge : edges()) {
            if (interDataEdge(edge.key()) || controlEdge(edge.key())
                    || updateEdge(edge.key())) {
                double e = edge.isCut() ? 1.0 : 0.0;
                realObjective += edge.weight() * e;

                if (edge.isCut()
                        && edge.from().location().equals(edge.to().location())) {
                    System.out.println("Edge " + edge
                            + " is cut but connects nodes"
                            + " in the same partition!" + edge.from() + " and "
                            + edge.to() + ":" + edge.from().location() + "->"
                            + edge.to().location());
                    for (Constraint c : edge.constraints()) {
                        System.out.println(c.toString());
                    }
                    System.out.println("Edge: " + edge.solverVariable());
                    System.out.println("From: " + edge.from().solverVariable());
                    System.out.println("To: " + edge.to().solverVariable());
                    System.out.println("Edge weight: " + edge.weight());
                    throw new Error("Invalid partition");

                }
                if (!edge.isCut() && edge.weight() >= 1.0
                        && !edge.from().location().equals(edge.to().location())) {
                    // Object from;
                    // if (edge.from().nodeID().isNative())
                    // from = edge.from().nodeID();
                    // else
                    // from = edge.from().nodeID().getNode().position();
                    // Object to;
                    // if (edge.to().nodeID().isNative())
                    // to = edge.to().nodeID();
                    // else
                    // to = edge.to().nodeID().getNode().position();
                    //
                    System.out
                            .println("Edge "
                                    + edge
                                    + " is not cut but connects nodes in different partitions: "
                                    + edge.from() + " and " + edge.to() + ":"
                                    + edge.from().location() + "->"
                                    + edge.to().location());
                    for (Constraint c : edge.constraints()) {
                        System.out.println(c.toString());
                    }
                    System.out.println("Edge: " + edge.solverVariable());
                    System.out.println("From: " + edge.from().solverVariable());
                    System.out.println("To: " + edge.to().solverVariable());
                    System.out.println("Edge weight: " + edge.weight());
                    throw new Error("Invalid partition");
                }
                // (Yes you can, if it is serializeable) Cannot cut callsite of
                // native method and the native method node.
                // if (edge.isCut() &&
                // (!edge.from().nodeID().isNative() &&
                // edge.to().nodeID().isNative())){
                // System.out.println("Edge " + edge
                // + " is cut but connects a call site and a non-native node: "
                // + edge.from() + " and " + edge.to() + ":"
                // + edge.from().location() + "->"
                // + edge.to().location());
                // for (Constraint c : edge.constraints()) {
                // System.out.println(c.toString());
                // }
                // System.out.println("Edge: " + edge.solverVariable());
                // System.out.println("From: " + edge.from().solverVariable());
                // System.out.println("To: " + edge.to().solverVariable());
                // System.out.println("Edge weight: " + edge.weight());
                // throw new Error("Invalid partition");
                // }
            }
        }
        System.out.println("Real objective is: " + realObjective);
        if (objective < realObjective) {
            System.out.println("Solver returned objective " + objective
                    + " but partition graph has " + realObjective);
        }

        return true;
    }

    @Override
    public void instantiate(SolverUtil sutil, double server_budget)
            throws SolverException {
        checkGraph();
        ArrayList<Constraint> constraints = new ArrayList<Constraint>();
        ArrayList<LinearTerm> objective = new ArrayList<LinearTerm>(
                edges.size());
        ArrayList<LinearTerm> budget = new ArrayList<LinearTerm>(nodes.size());

        for (PGNode node : nodes()) {

            if (node.nodeID().getNode() instanceof ClassDecl)
                continue;
            /*
             * SolverVariable indicating assignment of node to server (1) or
             * client (0)
             */
            SolverVariable nv;
            if (node.location() instanceof SolveableLocation) {
                SolveableLocation sl = (SolveableLocation) node.location();
                nv = sutil.addSolveable(sl);
                node.assignVariable(nv);
                SolverVariable nv2 = sutil.addSolveable(node);
                if (nv != nv2)
                    throw new InternalCompilerError(
                            "Expected vars to be equal!");
            } else {
                nv = sutil.addSolveable(node);
                // add location constraints
            }
            Constraint c = node.location().locationConstraint(sutil, nv);
            if (c != null) {
                // System.out.println("Added location constraint for " + node +
                // ":" + c);
                node.addConstraint(c);
                constraints.add(c);
            }
            budget.add(sutil.linearTerm(node.weight(), nv));
        }

        for (PGEdge edge : edges()) {

            if (interDataEdge(edge.key()) || controlEdge(edge.key())
                    || updateEdge(edge.key())) {
                /*
                 * SolverVariable indicating whether a transfer cost is incurred
                 * between two control deps
                 */
                SolverVariable ev = sutil.addSolveable(edge);
                sutil.addSolveable(edge.from());
                sutil.addSolveable(edge.to());

                add_edge_constraints(sutil, edge.from(), edge.to(), edge,
                        constraints);
                if (Double.isNaN(edge.weight()))
                    throw new InternalCompilerError("Edge has NaN weight:"
                            + edge.from() + "--" + edge.key() + "-->"
                            + edge.to());
                LinearTerm lt = sutil.linearTerm(edge.weight(), ev);
                if (Double.isNaN(lt.coeff())) {
                    throw new InternalCompilerError("Edge has NaN weight:"
                            + edge.from() + "--" + edge.key() + "-->"
                            + edge.to());
                }
                objective.add(lt);
            }
        }

        constraints.add(sutil.linearConstraint(
                budget.toArray(new LinearTerm[0]), sutil.leq(), server_budget));
        if (print_constraints) {
            printConstraints(objective, constraints);
        }
        this.problem = sutil.minimizationProblem(
                objective.toArray(new LinearTerm[0]),
                constraints.toArray(new Constraint[0]));
    }

    private void checkGraph() {
        if (edges.keySet().size() != edges.values().size())
            throw new InternalCompilerError("WTF?");

        for (PGEdge edge : edges.values()) {
            boolean in_nodes = nodes.containsKey(edge.to().nodeID())
                    && nodes.containsKey(edge.from().nodeID());
            if (!in_nodes)
                throw new InternalCompilerError("Edge connects an unknown node");

            // addIncoming(edge.to(), edge);
            // addOutgoing(edge.from(), edge);

            if (!outgoing(edge.from()).contains(edge)) {
                System.err.println("outgoing of " + edge.from()
                        + " does not contain " + edge.to());
                System.err.println(outgoing(edge.from()));
                throw new InternalCompilerError(
                        "Incoming edge is not in outgoing");
            }
            if (!incoming(edge.to()).contains(edge)) {
                System.err.println("incoming of " + edge.to()
                        + " does not contain " + edge.from());
                System.err.println(incoming(edge.to()));
                throw new InternalCompilerError(
                        "Outgoing edge is not in incoming");
            }
        }
    }

    @Override
    public Problem problem() {
        if (problem != null)
            return problem;
        throw new InternalCompilerError(
                "Partition problem must be instantiated first!");
    }

    /**
     * Add edge constraints to ensure if x and y are assigned to different
     * hosts, then ev must be 1 x - y - ev <= 0 y - x - ev <= 0
     * 
     * @param from
     *            a solverVariable representing the source node
     * @param to
     *            a solverVariable representing the target node
     * @param edge
     *            a solverVariable representing the edge from x to y
     * @throws SolverException
     */
    private static void add_edge_constraints(SolverUtil sutil, PGNode from,
            PGNode to, PGEdge edge, Collection<Constraint> constraints)
            throws SolverException {
        ConstraintType leq = sutil.leq();
        LinearTerm plus_x = sutil.linearTerm(1.0, from.solverVariable());
        LinearTerm plus_y = sutil.linearTerm(1.0, to.solverVariable());
        LinearTerm minus_x = sutil.linearTerm(-1.0, from.solverVariable());
        LinearTerm minus_y = sutil.linearTerm(-1.0, to.solverVariable());
        LinearTerm minus_ev = sutil.linearTerm(-1.0, edge.solverVariable());

        Constraint c1 = sutil.linearConstraint(new LinearTerm[] { plus_x,
                minus_y, minus_ev }, leq, 0);
        Constraint c2 = sutil.linearConstraint(new LinearTerm[] { plus_y,
                minus_x, minus_ev }, leq, 0);

        edge.addConstraint(c1);
        edge.addConstraint(c2);
        constraints.add(c1);
        constraints.add(c2);
    }

    public static class SimpleLocation implements PGLocation {
        public static final SimpleLocation CLIENT = new SimpleLocation(
                "client", 0);
        public static final SimpleLocation SERVER = new SimpleLocation(
                "server", 1);

        public static PGLocation toLocation(int val) {
            if (val != 0 && val != 1)
                throw new Error("Invalid location id " + val);
            return (val == 0) ? CLIENT : SERVER;
        }

        protected String name;
        protected int id;

        public SimpleLocation(String name, int id) {
            this.name = name;
            this.id = id;
            PGLocation.ID_MAP.put(id, this);
        }

        @Override
        public Constraint locationConstraint(SolverUtil sutil,
                SolverVariable var) throws SolverException {
            LinearTerm[] term = new LinearTerm[] { sutil.linearTerm(1.0, var) };
            return sutil.linearConstraint(term, sutil.eq(), id);
        }

        @Override
        public String toPlacement() {
            return (this == SERVER) ? "S" : "C";
        }

    }

    public static final EdgeType EDGE_OUTPUT = new EdgeType(true) {
        @Override
        public boolean equals(Object o) {
            return this == o;
        }

        @Override
        public int hashCode() {
            return 111222333;
        }

        @Override
        public String toString() {
            return "OUTPUT";
        }
    };

    public static final EdgeType EDGE_ANTI = new EdgeType(true) {
        @Override
        public boolean equals(Object o) {
            return this == o;
        }

        @Override
        public int hashCode() {
            return 1230987;
        }

        @Override
        public String toString() {
            return "ANTI";
        }
    };

    public static final EdgeType EDGE_UPDATE = new EdgeType(false) {
        @Override
        public boolean equals(Object o) {
            return this == o;
        }

        @Override
        public int hashCode() {
            return 410232;
        }

        @Override
        public String toString() {
            return "UPDATE";
        }
    };

}
