package solomon.analysis.partition.graph;

import java.util.HashMap;
import java.util.Map;

import polyglot.ast.Node;
import polyglot.ast.Term;
import accrue.analysis.interprocanalysis.AnalysisContext;
import accrue.analysis.interprocanalysis.NativeNodeIdentifier;
import accrue.analysis.interprocanalysis.NativeNodeIdentifier_c;
import accrue.analysis.interprocanalysis.NodeIdentifier;
import accrue.analysis.interprocanalysis.NodeIdentifier_c;

/**
 * A partition graph in which the nodes are context-free (i.e. the union over
 * AnalysisContexts) statements.
 * 
 * @author owen
 * 
 */
public class ContextFreePG extends AbstractPartitionGraph {
	/** 
	 * A pseudo-context for merging analysis results
	 */
	protected final AnalysisContext ALL_CONTEXTS;
		/**
	 * The set of pseudo-location identifiers
	 * @param norm
	 */
	protected final Map<String, NodeIdentifier> pseudo_locs;
	
	public ContextFreePG(AnalysisContext norm) {
		ALL_CONTEXTS = norm;
		pseudo_locs = new HashMap<String, NodeIdentifier>();
	}

	public AnalysisContext norm() {
        return ALL_CONTEXTS;
    }

	protected NodeIdentifier normalize(NodeIdentifier nodeID) {
		if (nodeID.getContext() == ALL_CONTEXTS) {
			return nodeID;
		} else if (nodeID.isNative()) {
			NativeNodeIdentifier mc = (NativeNodeIdentifier) nodeID;
			return new NativeNodeIdentifier_c(ALL_CONTEXTS, mc
					.getCodeInstance(), mc.getIndex());
		} else {
			Node node = nodeID.getNode();
			return new NodeIdentifier_c(ALL_CONTEXTS, (Term) node);
		}
	}

	@Override
	public PGNode createNode(Object key, NodeIdentifier nodeID, PGLocation loc) {
		NodeIdentifier norm = normalize(nodeID);
		PGNode node = super.createNode(key, norm, loc);	     
		return node;
	}

	public NodeIdentifier contextFreePseudoNodeID(String name) {
		if(pseudo_locs.containsKey(name))
			return pseudo_locs.get(name);
		else {
			NodeIdentifier ni = new PseudoNodeIdentifier(ALL_CONTEXTS, name);
			pseudo_locs.put(name, ni);
			return ni;
		}
	}
    public PGNode lookup(Term node) {
        NodeIdentifier nodeID = new NodeIdentifier_c(ALL_CONTEXTS, node);
        return lookup(nodeID);
    }

    public PGNode lookup(NodeIdentifier nodeID) {
        return super.lookup(normalize(nodeID));
    }

	protected static final class PseudoNodeIdentifier implements NodeIdentifier {
		protected AnalysisContext context;
		protected String name;
		public PseudoNodeIdentifier(AnalysisContext context, String name) {
			this.context = context;
			this.name = name;
		}
		@Override
		public boolean isNative() {
			return true;
		}

		@Override
		public Term getNode() {
			return null;
		}

		@Override
		public AnalysisContext getContext() {
			return context;
		}

		@Override
		public boolean equals(Object o) {
			if(o instanceof PseudoNodeIdentifier) {
				PseudoNodeIdentifier pni = (PseudoNodeIdentifier) o;
				return context.equals(pni.context) && name.equals(pni.name);
			}
			return false;
		}
		
		@Override
		public int hashCode() {
			return context.hashCode() ^ name.hashCode();
		}
	}

}
