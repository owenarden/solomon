package solomon.analysis.partition.graph;

import java.util.Collection;
import java.util.Set;

import solomon.constraints.Problem;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import accrue.analysis.interprocanalysis.EdgeIdentifier.EdgeType;
import accrue.analysis.interprocanalysis.NodeIdentifier;

public interface PartitionGraph {
	PGNode createNode(NodeIdentifier nodeID, PGLocation loc);
	PGNode createNode(Object key, NodeIdentifier nodeID, PGLocation loc);

    PGEdge createEdge(PGNode from, PGNode to, EdgeType key);
	PGLocation location(int val);
	PGNode lookup(NodeIdentifier nodeID);
//    PGEdge lookup(EdgeIdentifier edgeID);
	PGNode node(PGNode node);
	PGEdge edge(PGEdge edge);
	
	void instantiate(SolverUtil su, double d) throws SolverException;
    void printGraph(boolean useVars);

	
	String color(PGEdge edge);
	String style(PGEdge edge);
	String label(PGEdge edge);
	
	String color(PGNode edge);
	String style(PGNode edge);
	String label(PGNode edge);
	String shape(PGNode node);
	void initColors(PGLocation...locations);
	Collection<PGNode> nodes();
	Collection<PGEdge> edges();
    Set<PGEdge> incoming(PGNode to);
    Set<PGEdge> outgoing(PGNode from);
    Problem problem();
    double minimumBudget(PGLocation loc);
    double maximumBudget(PGLocation loc);
    double actualBudget(PGLocation loc);
    double maximumObjective();
    boolean checkPartition(double objective, double server_budget);
    double controlTransfers();
    
    boolean controlEdge(EdgeType k);
    boolean antiEdge(EdgeType k);
    boolean outputEdge(EdgeType k);
    boolean dataEdge(EdgeType k);
    boolean updateEdge(EdgeType k);
    boolean intraDataEdge(EdgeType k);
    boolean interDataEdge(EdgeType k);
}
