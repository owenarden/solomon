package solomon.analysis.partition;

import polyglot.types.ProcedureInstance;
import solomon.analysis.partition.graph.PGEdge;
import solomon.analysis.partition.graph.PGLocation;
import accrue.analysis.interprocanalysis.AnalysisContext;
import accrue.analysis.pointer.HContext;

public abstract class PGLibrarySignature {
	
    protected final PGLocation location;
	
	public PGLibrarySignature(PGLocation location) {
		this.location = location;
	}
	
	public abstract PGLocation location(
			ProcedureInstance pi,
			AnalysisContext calleeContext,
			HContext receiver);
	public abstract void addMissingDependencies(PartitionGraphBuilder pgb, PGEdge incoming);
	
	public static class AlwaysAt extends PGLibrarySignature {

		public AlwaysAt(PGLocation location) {
			super(location);
		}

		@Override
		public PGLocation location(ProcedureInstance pi,
				AnalysisContext calleeContext, HContext receiver) {
			return location;
		}

        @Override
        public void addMissingDependencies(PartitionGraphBuilder pgb,
                PGEdge incoming) { }		
	}

}


