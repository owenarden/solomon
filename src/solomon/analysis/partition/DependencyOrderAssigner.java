package solomon.analysis.partition;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import polyglot.ast.Block;
import polyglot.ast.ClassDecl;
import polyglot.ast.CodeDecl;
import polyglot.ast.CompoundStmt;
import polyglot.ast.Empty;
import polyglot.ast.FieldDecl;
import polyglot.ast.If;
import polyglot.ast.Initializer;
import polyglot.ast.LocalDecl;
import polyglot.ast.Loop;
import polyglot.ast.Node;
import polyglot.ast.ProcedureDecl;
import polyglot.ast.Stmt;
import polyglot.ast.Term;
import polyglot.frontend.ExtensionInfo;
import polyglot.frontend.Job;
import polyglot.main.Report;
import polyglot.types.SemanticException;
import polyglot.util.InternalCompilerError;
import polyglot.visit.CFGBuildError;
import polyglot.visit.CFGBuilder;
import polyglot.visit.DataFlow;
import polyglot.visit.DataFlow.Item;
import polyglot.visit.FlowGraph;
import polyglot.visit.FlowGraph.Edge;
import polyglot.visit.FlowGraph.EdgeKey;
import polyglot.visit.FlowGraph.Peer;
import polyglot.visit.NodeVisitor;
import solomon.analysis.partition.ext.PGExt;
import solomon.analysis.partition.ext.PGUtil;
import solomon.analysis.partition.graph.PGEdge;
import solomon.analysis.partition.graph.PGLocation;
import solomon.analysis.partition.graph.PGNode;
import solomon.analysis.partition.graph.PartitionGraph;
import solomon.analysis.signatures.SolAnalysisSignatures.DBLocation;
import accrue.analysis.callgraph.PreciseCallGraphFactory;
import accrue.analysis.ext.ObjanalExt;
import accrue.analysis.ext.ObjanalExt_c;
import accrue.analysis.interprocanalysis.AnalysisContext;
import accrue.analysis.interprocanalysis.AnalysisContextMap;
import accrue.analysis.interprocanalysis.EdgeIdentifier;
import accrue.analysis.interprocanalysis.NodeIdentifier;
import accrue.analysis.interprocanalysis.NodeIdentifier_c;
import accrue.analysis.interprocanalysis.Registrar;

public class DependencyOrderAssigner extends DataFlow<Item> {
    protected final PartitionGraph pg;
    protected final ExtensionInfo extInfo;
    private int init = 0;
    protected Map<NodeIdentifier, Integer> nodePostordering;
    protected AnalysisContext currentContext;
    protected Map<PGNode, Set<PGNode>> stmtsInBlock;
    protected Map<PGNode, PGNode> blockMap;

    public DependencyOrderAssigner(Job job, ExtensionInfo extInfo,
            PartitionGraph pg) {
        super(job, extInfo.typeSystem(), extInfo.nodeFactory(), true, false,
                true);
        this.pg = pg;
        this.extInfo = extInfo;
    }

    private boolean isCalled(ProcedureDecl cd) {
        if (Registrar.isStartProc(cd.procedureInstance()))
            return true;
        ObjanalExt ext = ObjanalExt_c.ext(cd);
        AnalysisContextMap<Set<EdgeIdentifier>> map = ext
                .getAnalysisResult(PreciseCallGraphFactory.CALL_SOURCES);
        return map != null && map.contexts() != null
                && !map.contexts().isEmpty();
    }

    private int postorderNodes(Peer<Item> p, int count, Set<Peer<Item>> visited) {
        if (visited.contains(p))
            return count;

        // visit p
        visited.add(p);

        // visit all the successors of p
        for (Edge<Item> e : p.succs()) {
            count = postorderNodes(e.getTarget(), count, visited);
        }

        // number p
        nodePostordering.put(nodeID(p), Integer.valueOf(count++));
        return count;
    }

    private NodeIdentifier nodeID(Peer<polyglot.visit.DataFlow.Item> p) {
        return new NodeIdentifier_c(currentContext, p.node());
    }

    @Override
    public Node override(Node parent, Node n) {
        if (n instanceof ClassDecl) {
            ClassDecl cd = (ClassDecl) n;
            if (cd.flags().isInterface())
                return n;
        }      
        if (n instanceof CodeDecl) {
            CodeDecl cd = (CodeDecl) n;
            if (cd instanceof ProcedureDecl && !isCalled((ProcedureDecl) cd)) {
                System.err.println("NEVER CALLED:" + cd);
                return n;
            }
            PGExt ext = PGUtil.ext(cd);
            if (ext.pgNode() == null) {
                // This method wasn't even called...
                System.err.println("No PGNode for " + cd);
                return n;
            }

            createCtlFlowOrder(cd, ext);

            Deque<PGNode> clientNodes = new ArrayDeque<PGNode>();
            Deque<PGNode> serverNodes = new ArrayDeque<PGNode>();

            // set initial preference by codedecl
            PGLocation loc = ext.pgNode().location();
            boolean preferClient;
            if (loc.equals(DBLocation.APPSERVER)) {
                preferClient = true;
                clientNodes.add(ext.pgNode());
            } else if (loc.equals(DBLocation.DBSERVER)) {
                preferClient = false;
                serverNodes.add(ext.pgNode());
            } else {
                throw new InternalCompilerError("Unexpected location: " + loc);
            }

            int count = 0;
            Set<PGEdge> seen = new HashSet<PGEdge>();

            Set<PGNode> waiting = new HashSet<PGNode>();
            // Santity check :
            while (!clientNodes.isEmpty() || !serverNodes.isEmpty()) {
                PGNode next;
                if (preferClient) {
                    if (clientNodes.isEmpty()) {
                        // assert: serverNodes is non-empty
                        preferClient = false;
                        next = serverNodes.pop();
                    } else {
                        next = clientNodes.pop();
                    }
                } else {
                    if (serverNodes.isEmpty()) {
                        // assert: clientnodes is non-empty
                        preferClient = true;
                        next = clientNodes.pop();
                    } else {
                        next = serverNodes.pop();
                    }
                }

                if (next.dependencyOrder() >= 0)
                    throw new InternalCompilerError("Dependency order for "
                            + next + " already set: " + next.dependencyOrder());
                next.setDependencyOrder(count++);
                for (PGEdge edge : pg.outgoing(next)) {
                    if (!seen.contains(edge) && isRelevant(edge)) {
                        seen.add(edge);
                        if (!hasUnseenInEdges(seen, edge.to())) {
                            PGLocation toLoc = edge.to().location();
                            waiting.remove(edge.to());
                            if (toLoc.equals(DBLocation.APPSERVER)) {
                                clientNodes.add(edge.to());
                            } else if (toLoc.equals(DBLocation.DBSERVER)) {
                                serverNodes.add(edge.to());
                            } else {
                                throw new InternalCompilerError(
                                        "Unexpected location: " + toLoc);
                            }
                        } else {
                            waiting.add(edge.to());
                        }
                    }
                }
            }
            if (Report.should_report("pdgDotFiles", 1)) {
                createDotFile(cd);
            }
            if (!waiting.isEmpty()) {
                dumpWaitingNodes(ext.pgNode(), waiting, seen);
                throw new InternalCompilerError("Still have remaining nodes");
            }

            return null;
        }
        return null;
    }

    private void createDotFile(CodeDecl cd) {
        PrintStream out = printDotHeader(cd);
        Set<PGNode> seen = new HashSet<PGNode>();
        Deque<PGNode> s = new ArrayDeque<PGNode>();
        PGExt ext = PGUtil.ext(cd);
        s.offer(ext.pgNode());
        while (!s.isEmpty()) {
            PGNode next = s.poll();
            out.println(next.toDot(false, nodePostordering.get(next.nodeID()).toString()));
            for (PGEdge e : pg.outgoing(next)) {
                if (e.key().isIntra())
                    out.println(e.toDot(false));
                if (e.key().isIntra() && !seen.contains(e.to())) {
                    seen.add(e.to());
                    s.offer(e.to());
                }
            }
        }
        out.println("}");
        out.close();
    }

    private void dumpWaitingNodes(PGNode current, Set<PGNode> waiting,
            Set<PGEdge> seen) {
        System.out.println("Waiting queue: ");
        for (PGNode w : waiting) {
            System.out.println("\tNode " + w + ":" + w.nodeID().hashCode()
                    + ":" + w.nodeID().getNode().position());
            System.out.println("\t\tUnseen edges: ");
            for (PGEdge e : pg.incoming(w)) {
                if (!seen.contains(e) && isRelevant(e)) {
                    System.out.println("\t\t: " + e + " from " + e.from() + ":"
                            + e.from().nodeID().hashCode() + " edge hash: "
                            + e.hashCode());
                }
            }
        }
        findCycles(current);
        try {
            File f = new File("waiting.dot");
            System.out.println("WRITING DOT FILE FOR WAITING QUEUE: "
                    + f.getAbsolutePath());
            PrintStream out = new PrintStream(f);
            out.println("digraph InterProceduralCFG {");
            out.println("fontsize=20; center=true; ratio=compress;");
            out.println("node [fontname=\"Courier\"]");
            out.println("node [shape=note]");
            Set<PGNode> waitingseenNodes = new LinkedHashSet<PGNode>();
            for (PGNode w : waiting) {
                if (!waitingseenNodes.contains(w)) {
                    out.println(w.toDot(false));
                    waitingseenNodes.add(w);
                }
                for (PGEdge e : pg.incoming(w)) {
                    if (!seen.contains(e) && isRelevant(e)) {
                        out.println(e.toDot(false));
                        if (!waitingseenNodes.contains(e.from())) {
                            out.println(e.from().toDot(false));
                            waitingseenNodes.add(e.from());
                        }
                    }
                }
            }
            out.println("}");
            out.close();
        } catch (FileNotFoundException e) {
        }

    }

    private PrintStream printDotHeader(CodeDecl n) {
        PrintStream out = null;
        String name = null;
        if (n instanceof ProcedureDecl)
            name = ((ProcedureDecl) n).name();
        else if (n instanceof Initializer)
            name = extInfo.scheduler().currentJob().toString() + "_init_"
                    + init++;
        try {
            File f = new File(name + ".dot");
            System.out.println("WRITING DOT FILE : " + f.getAbsolutePath());
            out = new PrintStream(f);
            out.println("digraph InterProceduralCFG {");
            out.println("fontsize=20; center=true; ratio=compress;");
            out.println("node [fontname=\"Courier\"]");
            out.println("node [shape=note]");

        } catch (FileNotFoundException e) {
        }
        return out;
    }

    private void createCtlFlowOrder(CodeDecl cd, PGExt ext) {
        FlowGraph<Item> g = initGraph(cd, cd);

        // Build the control flow graph.
        CFGBuilder<Item> v = createCFGBuilder(ts, g);

        try {
            v.visitGraph();
        } catch (CFGBuildError e) {
            throw new InternalCompilerError(e.message(), e.position());
        }

        // construct a postordering of the peers by visiting each peer in a
        // depth first manner
        this.currentContext = ext.pgNode().nodeID().getContext();
        this.nodePostordering = new HashMap<NodeIdentifier, Integer>();
        int count = 0;
        Set<Peer<Item>> visited = new HashSet<Peer<Item>>();
        for (Peer<Item> p : g.startPeers()) {
            count = postorderNodes(p, count, visited);
        }
    }

    private Set<Set<PGNode>> findCycles(PGNode root) {
        int index = 0;
        Map<PGNode, Integer> indexMap = new LinkedHashMap<PGNode, Integer>();
        Map<PGNode, Integer> lowLinkMap = new LinkedHashMap<PGNode, Integer>();

        Stack<PGNode> S = new Stack<PGNode>();
        Set<Set<PGNode>> SCCs = new LinkedHashSet<Set<PGNode>>();
        Set<PGNode> V = methodNodes(root);
        for (PGNode v : V) {
            if (!indexMap.containsKey(indexMap))
                index = strongconnect(v, S, indexMap, lowLinkMap, index, SCCs);
        }
        return SCCs;
    }

    private Set<PGNode> methodNodes(PGNode root) {
        Set<PGNode> nodes = new LinkedHashSet<PGNode>();
        Stack<PGNode> S = new Stack<PGNode>();
        S.push(root);
        while (!S.isEmpty()) {
            PGNode next = S.pop();
            nodes.add(next);
            for (PGEdge edge : pg.outgoing(next))
                if (isRelevant(edge) && !nodes.contains(edge.to()))
                    S.push(edge.to());
        }
        return nodes;
    }

    private int strongconnect(PGNode v, Stack<PGNode> S,
            Map<PGNode, Integer> indexMap, Map<PGNode, Integer> lowLinkMap,
            int index, Set<Set<PGNode>> SCCs) {
        // Set the depth index for v to the smallest unused index
        indexMap.put(v, index);
        lowLinkMap.put(v, index);
        index += index + 1;
        S.push(v);
        // Consider successors of v
        for (PGEdge e : pg.outgoing(v)) {
            if (!isRelevant(e))
                continue;
            PGNode w = e.to();
            if (!indexMap.containsKey(w)) {
                // Successor w has not yet been visited; recurse on it
                index = strongconnect(w, S, indexMap, lowLinkMap, index, SCCs);
                int vlow = lowLinkMap.get(v);
                int wlow = lowLinkMap.get(w);
                lowLinkMap.put(v, Math.min(vlow, wlow));
            } else if (S.contains(w)) {
                // // Successor w is in stack S and hence in the current SCC
                int vlow = lowLinkMap.get(v);
                int widx = indexMap.get(w);
                lowLinkMap.put(v, Math.min(vlow, widx));
            }
        }
        if (lowLinkMap.get(v) == indexMap.get(v)) {
            PGNode w = null;
            Set<PGNode> scc = new LinkedHashSet<PGNode>();
            while (!v.equals(w)) {
                w = S.pop();
                scc.add(w);
            }
            if (scc.size() > 1) {
                System.err.println("----CYCLE---");
                for (PGNode n : scc)
                    System.err.println(n.toDot(false));
                System.err.println("----------");
            }
            SCCs.add(scc);
        }
        return index;
    }

    @Override
    public Node leaveCall(Node old, Node n, NodeVisitor v) {
        PGExt ext = PGUtil.ext(n);
        if (n instanceof ClassDecl || n instanceof FieldDecl) {
            return n;
        }

        if (ext != null && ext.pgNode() != null) {
            PGNode node = ext.pgNode();

            if (n instanceof LocalDecl)
                node.setDependencyOrder(0);

            if (node.dependencyOrder() == -1 && !(n instanceof Empty)) {
                throw new InternalCompilerError("Node " + node
                        + " has no dep order!"
                        + node.nodeID().getNode().position());
            } else if (n instanceof CompoundStmt) {
                node.setDependencyOrder(getMaxDepOrder((CompoundStmt) n));
            }
        }
        return n;
    }

    private int getMaxDepOrder(Stmt n) {
        if (n instanceof If) {
            If i = (If) n;
            int c = getMaxDepOrder(i.consequent());
            if (i.alternative() == null)
                return c;
            else {
                int a = getMaxDepOrder(i.alternative());
                return (c > a) ? c : a;
            }
        } else if (n instanceof Loop) {
            Loop l = (Loop) n;
            return getMaxDepOrder(l.body());
        } else if (n instanceof Block) {
            Block b = (Block) n;
            int max = 0;
            for (Stmt s : (List<Stmt>) b.statements()) {
                int v = getMaxDepOrder(s);
                if (v > max)
                    max = v;
            }
            return max;
        } else if (n instanceof LocalDecl) {
            return 0;
        } else {
            PGExt ext = PGUtil.ext(n);
            PGNode node = ext.pgNode();
            return node.dependencyOrder();
        }
    }

    /**
     * An edge is relevant if it connects two nodes in the same context and is
     * not a backward edge in the control flow ordering.
     */
    private boolean isRelevant(PGEdge edge) {
        if (edge.key().isIntra()) {
            int from = nodePostordering.get(edge.from().nodeID());
            int to = nodePostordering.get(edge.to().nodeID());
            return from > to;
        } else
            return false;
    }

    private boolean hasUnseenInEdges(Set<PGEdge> seen, PGNode to) {
        for (PGEdge e : pg.incoming(to)) {
            if (!seen.contains(e) && isRelevant(e)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected polyglot.visit.DataFlow.Item createInitialItem(
            FlowGraph<polyglot.visit.DataFlow.Item> graph, Term node,
            boolean entry) {
        throw new InternalCompilerError("Not supported");
    }

    @Override
    protected polyglot.visit.DataFlow.Item confluence(
            List<polyglot.visit.DataFlow.Item> items, Term node, boolean entry,
            FlowGraph<polyglot.visit.DataFlow.Item> graph) {
        throw new InternalCompilerError("Not supported");
    }

    @Override
    protected void check(FlowGraph<polyglot.visit.DataFlow.Item> graph, Term n,
            boolean entry, polyglot.visit.DataFlow.Item inItem,
            Map<EdgeKey, polyglot.visit.DataFlow.Item> outItems)
            throws SemanticException {
        throw new InternalCompilerError("Not supported");
    }

}
