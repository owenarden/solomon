package solomon.analysis.partition.ext;

import solomon.analysis.partition.PartitionGraphBuilder;
import accrue.analysis.interprocanalysis.EdgeIdentifier;

@SuppressWarnings("serial")
public class PGProcedureCall_c extends PGExt_c {
	@Override
	public void acceptPGBuilder(PartitionGraphBuilder pgb) {
		super.acceptPGBuilder(pgb);
		for (EdgeIdentifier dfni : pgb.missingTargets(node())) {
			pgb.addOutEdge(dfni);
		}
	}	
}
