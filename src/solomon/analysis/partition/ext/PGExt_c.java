package solomon.analysis.partition.ext;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import polyglot.ast.Ext_c;
import solomon.analysis.partition.PartitionGraphBuilder;
import solomon.analysis.partition.graph.PGNode;
import accrue.analysis.antidep.AntiDepAnalysisFactory;
import accrue.analysis.ctldep.CtlDepAnalysisFactory;
import accrue.analysis.defuse.DefUseAnalysisFactory;
import accrue.analysis.ext.ObjanalExt_c;
import accrue.analysis.interprocanalysis.AbstractLocation;
import accrue.analysis.interprocanalysis.AnalysisContext;
import accrue.analysis.interprocanalysis.AnalysisContextMap;
import accrue.analysis.interprocanalysis.EdgeIdentifier;

@SuppressWarnings("serial")
public class PGExt_c extends Ext_c implements PGExt {    
    //XXX: Hack?
    protected Map<Object, PGNode> pgnodes = new LinkedHashMap<Object, PGNode>(2);
    
    @Override
    public final void setPGNode(PGNode pgnode) {
        setPGNode(PGExt.DEFAULT_PGNODE, pgnode);
    }

    @Override
    public void setPGNode(Object key, PGNode pgnode) {
        pgnodes.put(key, pgnode);
    }
    
    @Override
    public PGNode pgNode() {
        return pgNode(PGExt.DEFAULT_PGNODE);
    }

    @Override
    public PGNode pgNode(Object key) {
        return pgnodes.get(key);
    }
        
	@Override
	public void acceptPGBuilder(PartitionGraphBuilder pgb) {
		for (EdgeIdentifier e : controlDependencies(pgb)) {
			pgb.addInEdge(e);
		}
		for (EdgeIdentifier e : dataDependencies(pgb)) {
			pgb.addInEdge(e);
		}
		for (AbstractLocation e : updateDependencies(pgb)) {
			pgb.addUpdateEdge(e);
		}
		for (EdgeIdentifier e : outputDependencies(pgb)) {
            pgb.addOutputEdge(e);
        }
        for (EdgeIdentifier e : antiDependencies(pgb)) {
            pgb.addAntiEdge(e);
        }
	}

	@Override
	public PartitionGraphBuilder acceptPGBuilderEnter(PartitionGraphBuilder pgb) {
		return pgb;
	}

	/**
	 * Returns the control dependencies of this node.
	 * 
	 * @param pgb
	 * @return
	 */
	@Override
	public Set<EdgeIdentifier> controlDependencies(PartitionGraphBuilder pgb) {
		ObjanalExt_c oaExt = (ObjanalExt_c) ObjanalExt_c.ext(node());

		Set<EdgeIdentifier> control = new HashSet<EdgeIdentifier>();
		AnalysisContextMap<Set<EdgeIdentifier>> branches = 
            oaExt.getAnalysisResult(CtlDepAnalysisFactory.BRANCH_SOURCES);		
		if(branches != null) {
		    for (AnalysisContext context : branches.contexts()) {
		        control.addAll(branches.get(context));
		    }
		}
        return control;
	}

	/**
	 * Returns the data used by this node.
	 * 
	 * @param pgb
	 * @return
	 */
	@Override
	public Set<EdgeIdentifier> dataDependencies(PartitionGraphBuilder pgb) {
		ObjanalExt_c oaExt = (ObjanalExt_c) ObjanalExt_c.ext(node());

		AnalysisContextMap<Set<EdgeIdentifier>> defs = oaExt
                .getAnalysisResult(DefUseAnalysisFactory.DATA_DEPENDENCIES);
		

        Set<EdgeIdentifier> data = new HashSet<EdgeIdentifier>();
        if (defs != null) {
            for (AnalysisContext context : defs.contexts()) {
                data.addAll(defs.get(context));
            }
        }
        
        AnalysisContextMap<Set<EdgeIdentifier>> intraDefs = oaExt
                .getAnalysisResult(DefUseAnalysisFactory.INTRA_DATA_DEPENDENCIES);
        if (intraDefs != null) {
            for (AnalysisContext context : intraDefs.contexts()) {
                data.addAll(intraDefs.get(context));
            }
        }
        return data;
	}
	
    /**
     * Returns the output dependencies (i.e. the assignments that may precede
     * this assignment) of this node.
     * 
     * @param pgb
     * @return
     */
    public Set<EdgeIdentifier> outputDependencies(PartitionGraphBuilder pgb) {
        ObjanalExt_c oaExt = (ObjanalExt_c) ObjanalExt_c.ext(node());
        AnalysisContextMap<Set<EdgeIdentifier>> deps = oaExt
                .getAnalysisResult(DefUseAnalysisFactory.INTRA_OUTPUT_DEPENDENCIES);
        if (deps == null)
            return Collections.emptySet();
        else {
            Set<EdgeIdentifier> output = new HashSet<EdgeIdentifier>();
            for (AnalysisContext context : deps.contexts()) {
                output.addAll(deps.get(context));
            }
            return output;
        }
    }
    
    /**
     * Returns the anti dependencies (i.e. the uses that may precede
     * this assignment) of this node.
     * 
     * @param pgb
     * @return
     */
    public Set<EdgeIdentifier> antiDependencies(PartitionGraphBuilder pgb) {
        ObjanalExt_c oaExt = (ObjanalExt_c) ObjanalExt_c.ext(node());
        AnalysisContextMap<Set<EdgeIdentifier>> deps = oaExt
                .getAnalysisResult(AntiDepAnalysisFactory.INTRA_ANTI_DEPENDENCIES);
        if (deps == null)
            return Collections.emptySet();
        else {
            Set<EdgeIdentifier> anti = new HashSet<EdgeIdentifier>();
            for (AnalysisContext context : deps.contexts()) {
                anti.addAll(deps.get(context));
            }
            return anti;
        }
    }

	/**
	 * Returns the defs made by this node.
	 * 
	 * @param pgb
	 * @return
	 */
	public Set<AbstractLocation> updateDependencies(PartitionGraphBuilder pgb) {
		ObjanalExt_c oaExt = (ObjanalExt_c) ObjanalExt_c.ext(node());
		// Get all the update dependencies
		AnalysisContextMap<Set<AbstractLocation>> deps = oaExt
                .getAnalysisResult(DefUseAnalysisFactory.MODIFIED_LOCATIONS);
        if (deps == null)
            return Collections.emptySet();
        else {
            Set<AbstractLocation> defs = new HashSet<AbstractLocation>();
            for (AnalysisContext context : deps.contexts()) {
                defs.addAll(deps.get(context));
            }
            return defs;
        }
	}

}
