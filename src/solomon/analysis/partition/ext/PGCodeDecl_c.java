package solomon.analysis.partition.ext;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import polyglot.ast.Initializer;
import solomon.analysis.partition.PartitionGraphBuilder;
import accrue.analysis.callgraph.PreciseCallGraphFactory;
import accrue.analysis.ext.ObjanalExt_c;
import accrue.analysis.interprocanalysis.AbstractLocation;
import accrue.analysis.interprocanalysis.AnalysisContext;
import accrue.analysis.interprocanalysis.AnalysisContextMap;
import accrue.analysis.interprocanalysis.EdgeIdentifier;

@SuppressWarnings("serial")
public class PGCodeDecl_c extends PGExt_c {
    @Override
    public Set<EdgeIdentifier> controlDependencies(PartitionGraphBuilder pgb) {
        ObjanalExt_c oaExt = (ObjanalExt_c) ObjanalExt_c.ext(node());
        String resultKey = (node() instanceof Initializer) ? PreciseCallGraphFactory.INIT_SOURCES
                : PreciseCallGraphFactory.CALL_SOURCES;
        AnalysisContextMap<Set<EdgeIdentifier>> calls = 
            oaExt.getAnalysisResult(resultKey);      
        if(calls != null) {
            Set<EdgeIdentifier> control = new HashSet<EdgeIdentifier>();
            for (AnalysisContext ac : calls.contexts()) {
                Set<EdgeIdentifier> next = calls.get(ac);
                control.addAll(next);
            }
            return control;
        }
        else 
            return Collections.emptySet();
    }

    @Override
    public Set<EdgeIdentifier> dataDependencies(PartitionGraphBuilder pgb) {
        // Ignore procedure summaries of data deps
        return Collections.emptySet();
    }

    @Override
    public Set<AbstractLocation> updateDependencies(PartitionGraphBuilder pgb) {
        // Ignore procedure summaries of defs        
        return Collections.emptySet();
    }

}
