package solomon.analysis.partition.ext;

import java.util.Set;

import accrue.analysis.interprocanalysis.EdgeIdentifier;
import polyglot.ast.Ext;
import solomon.analysis.partition.PartitionGraphBuilder;
import solomon.analysis.partition.graph.PGNode;

public interface PGExt extends Ext {
    public static final String DEFAULT_PGNODE = "$default$"; 
	void acceptPGBuilder(PartitionGraphBuilder pgb);
	PartitionGraphBuilder acceptPGBuilderEnter(PartitionGraphBuilder pgb);
    PGNode pgNode();
    PGNode pgNode(Object key);
    void setPGNode(PGNode pgnode);
    void setPGNode(Object key, PGNode pgnode);
    
    Set<EdgeIdentifier> controlDependencies(PartitionGraphBuilder pgb);
    Set<EdgeIdentifier> dataDependencies(PartitionGraphBuilder pgb);
}
