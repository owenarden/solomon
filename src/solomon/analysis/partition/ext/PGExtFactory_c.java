package solomon.analysis.partition.ext;

import polyglot.ast.AbstractExtFactory_c;
import polyglot.ast.Ext;
import polyglot.ast.ExtFactory;

public class PGExtFactory_c extends AbstractExtFactory_c implements PGExtFactory {
    
	public PGExtFactory_c(ExtFactory nextExtFactory) {
        super(nextExtFactory);
    }
	
    public PGExtFactory_c() {
        super();
    }
    
	@Override
	protected Ext extCodeDeclImpl() {
        return new PGCodeDecl_c();
	}
	
	@Override
	protected Ext extClassDeclImpl() {
	    return new PGExt_c();
	}

	@Override
	protected Ext extStmtImpl() {
        return new PGExt_c();
	}
	
	@Override
	protected Ext extCompoundStmtImpl() {
		return null;
	}

	@Override
	protected Ext extExprImpl() {
		return new PGExt_c();
	}

	@Override
	protected Ext extFieldDeclImpl() {
        return new PGExt_c();
	}
		
	@Override
	protected Ext extCatchImpl() {
        return new PGExt_c();
	}

	@Override
	protected Ext extIfImpl() {
		return new PGIf_c();
	}

	@Override
	protected Ext extLoopImpl() {
		return new PGLoop_c();
	}

	@Override
	protected Ext extLabeledImpl() {
        return new PGExt_c();
	}

	@Override
	protected Ext extTryImpl() {
        return new PGExt_c();
	}

	@Override
	protected Ext extConstructorCallImpl() {
        return new PGProcedureCall_c();
	}
	
	@Override
	protected Ext extCallImpl() {
		return new PGProcedureCall_c();
	}

	@Override
	protected Ext extNewImpl() {
        return new PGProcedureCall_c();
	}

	@Override
	protected Ext extLocalImpl() {
        return new PGExt_c();
	}
	
	@Override
	protected Ext extFieldImpl() {
        return new PGExt_c();
	}

	@Override
    protected Ext extLocalAssignImpl() {
        return new PGExt_c();
    }

}
