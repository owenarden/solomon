package solomon.analysis.partition.ext;

import java.util.Set;

import polyglot.ast.If;
import solomon.analysis.partition.PartitionGraphBuilder;
import solomon.analysis.partition.graph.PGNode;
import accrue.analysis.interprocanalysis.EdgeIdentifier;

@SuppressWarnings("serial")
public class PGIf_c extends PGExt_c {
    
    @Override
    public Set<EdgeIdentifier> dataDependencies(PartitionGraphBuilder pgb) {
        If n = (If) node();
        // Get the control dependencies of the loop condition
        PGExt pgExt = PGUtil.ext(n.cond());
        return pgExt.dataDependencies(pgb);
	}

    @Override
    public void setPGNode(Object key, PGNode pgnode) {
        super.setPGNode(key, pgnode);
        // also set PGNode for condition so that CFG traversal 
        // can retrieve If pgnode
        If n = (If) node();
        PGExt condExt = PGUtil.ext(n.cond());
        condExt.setPGNode(key, pgnode);
    }

}
