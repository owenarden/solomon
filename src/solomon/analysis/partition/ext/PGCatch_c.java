package solomon.analysis.partition.ext;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import accrue.analysis.callgraph.PreciseCallGraphFactory;
import accrue.analysis.ext.ObjanalExt_c;
import accrue.analysis.interprocanalysis.EdgeIdentifier;
import accrue.analysis.interprocvarcontext.AnalysisContextPeerMap;

@SuppressWarnings("serial")
public class PGCatch_c extends PGExt_c {
    public Set<EdgeIdentifier> controlDependencies() {
        ObjanalExt_c oaExt = (ObjanalExt_c) ObjanalExt_c.ext(node());
        AnalysisContextPeerMap<Set<EdgeIdentifier>> calls = 
            oaExt.getAnalysisResult(PreciseCallGraphFactory.CATCH_TARGETS);      
        if(calls != null) {
            Set<EdgeIdentifier> control = new HashSet<EdgeIdentifier>();
            for (Set<EdgeIdentifier> next : calls.values()) {
                control.addAll(next);
            }
            return control;
        }
        else 
            return Collections.emptySet();
	}
}
