package solomon.analysis.partition.ext;

import polyglot.ast.Ext;
import polyglot.ast.Node;

public class PGUtil {

	public static PGExt ext(Node n) {
        Ext e = n.ext();
        while (e != null && !(e instanceof PGExt)) {
            e = e.ext();
        }
        return (PGExt)e;
	}
	
}
