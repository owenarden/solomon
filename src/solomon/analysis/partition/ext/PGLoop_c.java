package solomon.analysis.partition.ext;

import java.util.Set;

import polyglot.ast.Loop;
import solomon.analysis.partition.PartitionGraphBuilder;
import accrue.analysis.interprocanalysis.EdgeIdentifier;

public class PGLoop_c extends PGExt_c {
    private static final long serialVersionUID = 1L;

    @Override
    public Set<EdgeIdentifier> controlDependencies(PartitionGraphBuilder pgb) {
		Loop n = (Loop) node();
		// Get the control dependencies of the loop condition
		PGExt pgExt = PGUtil.ext(n.cond());
		return pgExt.controlDependencies(pgb);
	}
	
    @Override
	public Set<EdgeIdentifier> dataDependencies(PartitionGraphBuilder pgb) {
        Loop n = (Loop) node();
        // Get the control dependencies of the loop condition
        PGExt pgExt = PGUtil.ext(n.cond());
        return pgExt.dataDependencies(pgb);
	}

}
