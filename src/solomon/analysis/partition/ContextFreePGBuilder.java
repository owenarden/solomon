package solomon.analysis.partition;

import java.util.Map;

import polyglot.ast.Assign;
import polyglot.ast.Call;
import polyglot.ast.ClassDecl;
import polyglot.ast.CodeDecl;
import polyglot.ast.CompoundStmt;
import polyglot.ast.ConstructorCall;
import polyglot.ast.Eval;
import polyglot.ast.FieldDecl;
import polyglot.ast.Formal;
import polyglot.ast.If;
import polyglot.ast.LocalDecl;
import polyglot.ast.Loop;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.ProcedureCall;
import polyglot.ast.ProcedureDecl;
import polyglot.ast.Stmt;
import polyglot.ast.Term;
import polyglot.frontend.Job;
import polyglot.main.Report;
import polyglot.types.ClassType;
import polyglot.types.FieldInstance;
import polyglot.types.ProcedureInstance;
import polyglot.types.SemanticException;
import polyglot.types.TypeSystem;
import polyglot.util.InternalCompilerError;
import polyglot.visit.ErrorHandlingVisitor;
import polyglot.visit.NodeVisitor;
import solomon.SolExtensionInfo;
import solomon.analysis.instrumentor.InstUtil;
import solomon.analysis.instrumentor.ext.InstExpr;
import solomon.analysis.instrumentor.ext.InstStmt;
import solomon.analysis.partition.ext.PGExt;
import solomon.analysis.partition.ext.PGUtil;
import solomon.analysis.partition.graph.AbstractPartitionGraph;
import solomon.analysis.partition.graph.ContextFreePG;
import solomon.analysis.partition.graph.PGEdge;
import solomon.analysis.partition.graph.PGLocation;
import solomon.analysis.partition.graph.PGNode;
import accrue.analysis.interprocanalysis.AbstractLocation;
import accrue.analysis.interprocanalysis.EdgeIdentifier;
import accrue.analysis.interprocanalysis.EdgeIdentifier_c;
import accrue.analysis.interprocanalysis.NativeNodeIdentifier;
import accrue.analysis.interprocanalysis.NodeIdentifier;
import accrue.analysis.interprocanalysis.NodeIdentifier_c;
import accrue.analysis.pointer.HContext;

/**
 * PGBs generate a graph whose nodes are statements and whose edges are control
 * or data transfers between statements.
 * 
 * PGNodes have "locations" drawn from a set of potential locations and may be
 * replicated among several. Nodes also have a weight representing the intrinsic
 * cost of placing the statement at any location -- i.e. the CPU load incurred.
 * 
 * PGEdges have weights representing the cost of transferring data or control
 * between nodes, and may depend on the location of their source and target.
 * 
 * @author owen
 */
// TODO: refactor declarations and enclosingStmt : move into StmtRegistrar.
// This class shouldn't assume that pg is a ContextFreePG
public class ContextFreePGBuilder extends PartitionGraphBuilder {

    protected Map<Node, Node> enclosingStmt;
	protected Map<FieldInstance, Node> declarations;
    protected final ContextFreePG pg;

	public ContextFreePGBuilder(SolExtensionInfo extInfo) {
		super(extInfo);
		this.enclosingStmt = extInfo.enclosingStmts();
		this.declarations = extInfo.fieldDeclarations();
		this.pg = (ContextFreePG) extInfo.partitionGraph();
	}

    protected ContextFreePGBuilder enterStmtNode(NodeIdentifier nodeID) {
		ContextFreePGBuilder pgb = (ContextFreePGBuilder) copy();		
		pgb.currentNode = pg.lookup(nodeID);
		if (pgb.currentNode == null) {
			pgb.currentNode = pg.createNode(nodeID, locationOf(nodeID));
		}
        if (Report.should_report("partition", 2)) {
            Report.report(2, "Entering stmt node:" + currentNode);
        }
		return pgb;
	}

    public ContextFreePGBuilder enterScope(Node n) {
	    ContextFreePGBuilder pgb = this;
        if (n instanceof ClassDecl) {
            pgb = (ContextFreePGBuilder) copy();
            NodeIdentifier nodeID = new NodeIdentifier_c(extInfo.anyContext(), (Term) n);
            pgb.currentScope = pg.createNode(nodeID, PGLocation.ANY);
            
        } else if (n instanceof CodeDecl) {
            pgb = (ContextFreePGBuilder) copy();
            NodeIdentifier nodeID = new NodeIdentifier_c(extInfo.anyContext(), (Term) n);
            PGNode nextNode = pg.lookup(nodeID);
            if (nextNode == null) {
                nextNode = pg.createNode(nodeID, PGLocation.ANY);
            }
            pgb.currentScope = nextNode;            
        }

        return pgb;
	}

    @Override
	public NodeVisitor enter(Node parent, Node n) {
        // skip abstract code decls
        if (n instanceof ProcedureDecl) {
            ProcedureDecl pd = (ProcedureDecl) n;
            if(pd.flags().isAbstract() || pd.flags().isNative())
                return this;
        }

		PGExt ext = PGUtil.ext(n);
		ContextFreePGBuilder pgb = enterScope(n);
		//TODO: properly handle Try
		if ((n instanceof Stmt 
				&& (!(n instanceof CompoundStmt) || n instanceof If 
				        || n instanceof Loop))
				|| n instanceof CodeDecl || n instanceof FieldDecl) {
			pgb = pgb.enterStmtNode(new NodeIdentifier_c(extInfo.anyContext(), (Term) n));
		}
		
		if (ext != null)
			return ext.acceptPGBuilderEnter(pgb);
		else
			return pgb;
	}

	@Override
    public Node leave(Node parent, Node old, Node n, NodeVisitor v) {
        // skip abstract code decls
        if (n instanceof ProcedureDecl) {
            ProcedureDecl pd = (ProcedureDecl) n;
            if(pd.flags().isAbstract() || pd.flags().isNative())
                return n;
        }

	    if (n instanceof ConstructorCall) {
            NodeIdentifier nodeID = new NodeIdentifier_c(extInfo.anyContext(), (Term) n);
            PGNode nextNode = pg.createNode(nodeID, PGLocation.ANY);
            pg.createEdge(currentScope, nextNode, EdgeIdentifier.EDGE_CONTROL);
        }
        return super.leave(parent, old, n, v);
    }
	
	protected PGNode prepareEdge(EdgeIdentifier edge) {
//	     // XXX:skip exception edges for now
//      if (edge.getKey() instanceof ExceptionKey)
//          return;

        NodeIdentifier targetID = edge.getTarget();
        // Skip native methods that have no location constraint
        if (targetID.isNative() && locationOf(targetID) == PGLocation.ANY) {
            return null;
        }
        NodeIdentifier stmtID = targetID;
        Node targetNode = targetID.getNode();
        if (targetNode != null) {
            Node targetStmt = enclosing(targetNode);
            if (!targetStmt.equals(targetNode)) {
                stmtID = new NodeIdentifier_c(targetID.getContext(),
                        (Term) targetStmt);
            }
        }

        PGNode target = pg.lookup(stmtID);
        if (target == null)
            target = pg.createNode(stmtID, locationOf(stmtID));

        if (Report.should_report("partition", 4)) {
            Report.report(3, "Adding edge from " + target + " to "
                    + currentNode);
        }
        return target;
	}

    public void addInEdge(EdgeIdentifier edge) {
        PGNode source = prepareEdge(edge);
		if (source == null || source.equals(currentNode))
			return;
        pg.createEdge(source, currentNode, edge.getKey());
	}

	@Override
	public void addOutEdge(EdgeIdentifier edge) {
        PGNode target = prepareEdge(edge);
		if (target == null || target.equals(currentNode))
			return;
        pg.createEdge(currentNode, target, edge.getKey());
	}
    
    @Override
    public void addUpdateEdge(AbstractLocation e) {
        if (e.context != null) {
            HContext ctx = e.context;
            if (ctx.type().isArray()) {
                NodeIdentifier nodeID = allocationNodeID(e);
                addOutEdge(new EdgeIdentifier_c(AbstractPartitionGraph.EDGE_UPDATE,
                        nodeID));
            } else if (ctx.type().isClass()) {
                FieldInstance fi = e.fi;
                if (fi == null && e.fieldName != null) {
                    ClassType ct = ctx.type().toClass();
                    fi = ct.fieldNamed(e.fieldName);
                }

                if (fi == null) {
                    //FIXME: fi may be null for 'fake' fields in signatures.
                    // should really have edge to allocation site here.
                    NodeIdentifier nodeID = allocationNodeID(e);
                    addOutEdge(new EdgeIdentifier_c(AbstractPartitionGraph.EDGE_UPDATE,
                            nodeID));
                } else if (declarations.containsKey(fi)) {
                    FieldDecl fd = (FieldDecl) declarations.get(fi);
                    // NodeIdentifier nodeID = ((ContextFreePG) pg)
                    // .contextFreeNodeID(fd);
                    NodeIdentifier nodeID = new NodeIdentifier_c(
                            extInfo.anyContext(), fd);
                    addOutEdge(new EdgeIdentifier_c(AbstractPartitionGraph.EDGE_UPDATE,
                            nodeID));
                } else
                    throw new InternalCompilerError("No declaration for " + fi);
            }
        } else if (e.fieldName != null
                && registrar.isStaticPseudoLocation(e.fieldName)) {
            NodeIdentifier nodeID = registrar
                    .getPseudoAllocationID(e.fieldName);
            addOutEdge(new EdgeIdentifier_c(AbstractPartitionGraph.EDGE_UPDATE, nodeID));
        } else {
            System.err.println("WARNING: Update edge for static allocation not yet implemented");
        }
    }

    @Override
    public void addOutputEdge(EdgeIdentifier edge) {
        PGNode dep = prepareEdge(edge);
        if (dep == null || dep.equals(currentNode))
            return;
        pg.createEdge(dep, currentNode, AbstractPartitionGraph.EDGE_OUTPUT);    
    }

    @Override
    public void addAntiEdge(EdgeIdentifier edge) {
        PGNode dep = prepareEdge(edge);
        if (dep == null || dep.equals(currentNode))
            return;
        pg.createEdge(dep, currentNode, AbstractPartitionGraph.EDGE_ANTI);
    }

	private double executionCount(NodeIdentifier nodeID) {
	    if (nodeID.isNative()) {
	        return 0.0;
	    }
        InstStmt inst = (InstStmt) InstUtil.instExt(nodeID.getNode());
        if (inst == null) {
            if (nodeID.getNode() instanceof Stmt)
                System.err.println("WARNING: No profile data for " + nodeID);
            return 0.0;
        }
        if (inst.executionCount() < 0)
            throw new InternalCompilerError("Negative instruction count for " + nodeID);
        if (inst.executionCount() == 0)
            return 1;
        return inst.executionCount();
    }
	
    private double averageSize(NodeIdentifier nodeID) {
        if (nodeID.getNode() instanceof Eval) {
            Eval eval = (Eval) nodeID.getNode();
            if (eval.expr() instanceof Assign) {
                InstExpr inst = (InstExpr) InstUtil.instExt(eval.expr() );
                //Should we enforce a minimum here?
                if (inst.averageSize() > 1.0)
                    return inst.averageSize();
                else
                    return 1.0;                 
            }
            else if (eval.expr() instanceof Call) {
                //XXX: is this always a native call?
                //TODO: get better estimate based on arguments
                return 1.0;
            }
            else throw new InternalCompilerError("Cannot get average size of non-assignment: " + nodeID.getNode().getClass());
        } 
        else if (nodeID.isNative() || nodeID.getNode() instanceof CodeDecl) {
            //TODO: get better estimate of Formals
            return 1.0;
        }
        else if ( nodeID.getNode() instanceof LocalDecl) {
            //TODO: get better estimate 
            return 1.0;
        }
        else throw new InternalCompilerError("Cannot get average size of non-assignment statement: "+ nodeID.getNode().getClass());
    }
    
    protected double getWeight(PGNode node) {
        NodeIdentifier nodeID = node.nodeID();
        if (nodeID.getNode() instanceof Stmt
                || nodeID.getNode() instanceof CodeDecl) {
            return executionCount(nodeID);
        }
        else if (nodeID.isNative() 
                || nodeID.getNode() instanceof FieldDecl
                || nodeID.getNode() instanceof ClassDecl) {
            return 0.0;
        }
        throw new InternalCompilerError("Cannot get weight of non-Stmt node: " + nodeID);
    }

    protected double getWeight(PGEdge edge) {
        //NOTE: All node weights are expected to be set prior to calling        
        if (pg.controlEdge(edge.key())) {
            double from_cnt = edge.from().weight();
            double to_cnt = edge.to().weight();
            double taken;
            if (edge.to().nodeID().getNode() instanceof ConstructorCall) {
                //FIXME: why is the ctor call 0?
                taken = from_cnt;
            } else if (edge.from().nodeID().isNative()) {
                if (from_cnt > 0.0) {
                    taken = (from_cnt < to_cnt) ? from_cnt : to_cnt;
                } else {
                    //FIXME: why is the return edge from a native node 0?
//                    System.err.println("Setting native edge to " + to_cnt + " for " +edge.from() + "-- " +edge.key()+ " --> " + edge.to() );
                    taken = to_cnt;
                }
            } else {
                taken = (from_cnt < to_cnt) ? from_cnt : to_cnt;
            }
//            if (taken == 0 
//                    && !(edge.from().nodeID().getNode() instanceof LocalDecl)
//                    && !(edge.to().nodeID().getNode() instanceof LocalDecl)) {
//                System.err.println("F:" + from_cnt + " T:" + to_cnt + " :: Zero edge for " + edge.from() + "-- " +edge.key()+ " --> " + edge.to());
//            }
            return taken * LATENCY;            
        } else if (pg.interDataEdge(edge.key())) {
//            if (edge.key() instanceof EffectsSummaryKey)
//                return 0.0;
            double from_cnt = edge.from().weight();
            double to_cnt = edge.to().weight();
            double size = averageSize(edge.from().nodeID());
            if (Double.isNaN(size) || Double.isInfinite(size)) {
                throw new InternalCompilerError("Edge has NaN size:" + size
                        + edge.from() + "--" + edge.key() + "-->"
                        + edge.to());
            }
            
            double min = (from_cnt < to_cnt) ? from_cnt : to_cnt;
            //If we are sending all data, then only the dataLatency matters.
            if (Double.isNaN(min) || Double.isInfinite(min)) {
                throw new InternalCompilerError("Edge has NaN count:" + size
                        + edge.from() + "--" + edge.key() + "-->"
                        + edge.to());
            }
            return min * dataLatency(size);            
        } else if (pg.updateEdge(edge.key())) {
            double from_cnt = edge.from().weight();
            double size = averageSize(edge.from().nodeID());
            double w = from_cnt * dataLatency(size);
//            System.err.println("W: " + w + "F:" + from_cnt + "S: " + size+ " :: update edge for " + edge.from() + "-- " +edge.key()+ " --> " + edge.to());
            return w;
        }
        else {
            return 0.0;
        }
    }

    protected Node enclosing(Node n) {
		if (n instanceof Stmt || n instanceof CodeDecl
				|| n instanceof FieldDecl) {
			return n;
		} else if (n instanceof Formal) {
		    // can't use enclosing for formals reliably.
		    return currentScope.nodeID().getNode();
		} else {
			Node stmt = enclosingStmt.get(n);

			if (stmt == null)
				throw new InternalCompilerError(n + " has no enclosing Stmt!");
			return stmt;
		}
	}

	protected NodeIdentifier enclosing(NodeIdentifier n) {
		if (n.getNode() == null)
			throw new NullPointerException(
					"Can't get enclosing statement of nodeID: " + n);
		Term enc = (Term) enclosing(n.getNode());
		return new NodeIdentifier_c(extInfo.anyContext(), enc);
	}

	protected NodeIdentifier allocationNodeID(AbstractLocation e) {
		if (e == null)
			throw new NullPointerException();

		HContext context = e.context;
		if (context != null) {
			NodeIdentifier nodeID = registrar.getAllocSiteNodeID(context);
			return enclosing(nodeID);
		} else if (e.fi != null) {
			// static field
			throw new InternalCompilerError(
					"NodeID for static allocation not yet implemented");
		} else
			// pseudo field
			return registrar.getPseudoAllocationID(e.fieldName);
	}
	
    /**
     * @param stmt
     * @return
     */
    @Override
    public PGLocation locationOf(NodeIdentifier nodeID) {
        if (!nodeID.isNative()) {
            Term node = nodeID.getNode();
            if (node instanceof If) {
                node = ((If) node).cond();
            } else if (node instanceof Loop) {
                node = ((Loop) node).cond();
            } else if (node instanceof CodeDecl || node instanceof CompoundStmt) {
                if (Report.should_report(TOPICS, 4)) {
                    Report.report(4, nodeID + " has location " + PGLocation.ANY);
                }
                return PGLocation.ANY;
            }
            LocationFinder lf = new LocationFinder(null, ts, nf, PGLocation.ANY, this);
            node.visit(lf);
            if (Report.should_report(TOPICS, 4)) {
                Report.report(4, nodeID + " has location " + lf.location());
            }
            return lf.location();
        } else {
            NativeNodeIdentifier nni = (NativeNodeIdentifier) nodeID;
            PGLocation loc = null;
            if (nni.getCodeInstance() instanceof ProcedureInstance) {
                ProcedureInstance pi = (ProcedureInstance) nni
                        .getCodeInstance();
                PGLibrarySignature sig = getSignature(pi);
                if (sig != null) {
                    loc = sig.location(pi, null, null);
                }
                if (loc == null)
                    loc = PGLocation.ANY;
            } else {
                throw new UnsupportedOperationException(
                        "CodeInstance signatures not yet supported");
            }
            if (Report.should_report(TOPICS, 4)) {
                Report.report(4, nodeID + " has location " + loc);
            }
            return loc;
        }
    }

    protected static class LocationFinder extends ErrorHandlingVisitor {
        protected PGLocation location;
        private final PartitionGraphBuilder pgb;

        public LocationFinder(Job job, TypeSystem ts, NodeFactory nf,
                PGLocation defaultLocation, PartitionGraphBuilder pgb) {
            super(job, ts, nf);
            this.pgb = pgb;
            location = defaultLocation;
        }

        public PGLocation location() {
            return location;
        }

        @Override
        public Node leaveCall(Node old, Node n, NodeVisitor v)
                throws SemanticException {
            if (n instanceof ProcedureCall) {
                ProcedureCall pc = (ProcedureCall) n;               
                PGLibrarySignature sig = pgb.getSignature(pc.procedureInstance());
                if (sig != null) {
                    PGLocation loc = sig.location(pc.procedureInstance(), null, null);
                    if (location != PGLocation.ANY && loc != PGLocation.ANY
                            && !location.equals(loc))
                        throw new SemanticException(
                                "Conflicting location signatures.",
                                n.position());
                    this.location = loc;
                }
            }
            return n;
        }

    }

}
