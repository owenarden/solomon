package solomon.analysis.partition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import polyglot.ast.Block;
import polyglot.ast.Catch;
import polyglot.ast.CodeDecl;
import polyglot.ast.ConstructorCall;
import polyglot.ast.Empty;
import polyglot.ast.If;
import polyglot.ast.LocalDecl;
import polyglot.ast.Loop;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.ProcedureDecl;
import polyglot.ast.Return;
import polyglot.ast.Stmt;
import polyglot.ast.Throw;
import polyglot.ast.Try;
import polyglot.frontend.Job;
import polyglot.types.SemanticException;
import polyglot.types.TypeSystem;
import polyglot.util.InternalCompilerError;
import polyglot.visit.ErrorHandlingVisitor;
import polyglot.visit.NodeVisitor;
import solomon.SolExtensionInfo;
import solomon.analysis.partition.ext.PGExt;
import solomon.analysis.partition.ext.PGUtil;
import solomon.analysis.partition.graph.PGNode;
import accrue.analysis.pointer.PointerAnalysisPass;
import accrue.analysis.pointer.PointsToGraph;

public class DependencyOrderRewriter extends ErrorHandlingVisitor {
    protected final SolExtensionInfo extInfo;

    public DependencyOrderRewriter(Job job, TypeSystem ts, NodeFactory nf) {
        super(job, ts, nf);
        this.extInfo = (SolExtensionInfo) job.extensionInfo();
    }

    private boolean isCalled(ProcedureDecl cd) {
        PointsToGraph ptg = PointerAnalysisPass.singleton(extInfo)
                .pointsToGraph();
        return !ptg.getContexts(cd.codeInstance()).isEmpty();
    }

    @Override
    public Node override(Node parent, Node n) {
        if (n instanceof ProcedureDecl) {
            ProcedureDecl cd = (ProcedureDecl) n;
            /* skip if never called */
            if (!isCalled(cd))
                return n;
        }
        return null;
    }

    @Override
    protected Node leaveCall(Node parent, Node old, Node n, NodeVisitor v)
            throws SemanticException {

        boolean isContainer = n instanceof If || n instanceof Loop
                || n instanceof Try || n instanceof CodeDecl;

        if (isContainer && extInfo.getOptions().reorderStmts()) {
            return rewriteBlocks(n);
        }
        return n;
    }

    private static boolean hasTypePrecedence(Stmt s) {
        return s instanceof ConstructorCall || s instanceof LocalDecl
                || s instanceof Return || s instanceof Throw
                || s instanceof Empty;
    }

    private static int typePrecOrder(Stmt s) {
        // Ctors come before everything
        if (s instanceof ConstructorCall)
            return -2;
        // Decls come before everything except Ctors
        if (s instanceof LocalDecl)
            return -1;
        // Return/throw come after everything
        if (s instanceof Return || s instanceof Throw)
            return 1;
        // everything else is in between
        else
            return 0;
    }

    private static class DepOrderCompare implements Comparator<Stmt> {
        @Override
        public int compare(Stmt s1, Stmt s2) {
            if (hasTypePrecedence(s1) || hasTypePrecedence(s2))
                return typePrecOrder(s1) - typePrecOrder(s2);
            // assert: s1 and s2 are from the same CodeDecl
            PGExt ext1 = PGUtil.ext(s1);
            if (ext1 == null || ext1.pgNode() == null) {
                throw new InternalCompilerError("Statement has no pg node: "
                        + s1 + ":" + ext1);
            }
            PGExt ext2 = PGUtil.ext(s2);
            if (ext2 == null || ext2.pgNode() == null) {
                throw new InternalCompilerError("Statement has no pg node: "
                        + s2 + ":" + ext2);
            }
            PGNode n1 = ext1.pgNode(BlockSubGraphBuilder.subgraphNode);
            PGNode n2 = ext2.pgNode(BlockSubGraphBuilder.subgraphNode);
            if (n1 == null || n2 == null)
                throw new InternalCompilerError("No pg node for "
                        + ((n1 == null) ? n1 : n2));

            if (n1.dependencyOrder() == n2.dependencyOrder())
                throw new InternalCompilerError(
                        "Two nodes cannot have the same dependency order.");

            if (n1.dependencyOrder() < 0 || n2.dependencyOrder() < 0)
                throw new InternalCompilerError(
                        "Dependency order has not been assigned!" + n1 + ":"
                                + n1.nodeID().hashCode());

            return n1.dependencyOrder() - n2.dependencyOrder();
        }
    }

    private Node rewriteBlocks(Node n) {
        DepOrderCompare comp = new DepOrderCompare();

        if (n instanceof If) {
            If i = (If) n;
            Stmt cons = i.consequent(), alt = i.alternative();
            // Register stmts
            if (cons instanceof Block) {
                Block blk = (Block) cons;
                List<Stmt> stmts = new ArrayList<Stmt>(blk.statements());
                Collections.sort(stmts, comp);
                cons = nf.Block(blk.position(), stmts);
            }
            if (alt instanceof Block) {
                Block blk = (Block) alt;
                List<Stmt> stmts = new ArrayList<Stmt>(blk.statements());
                Collections.sort(stmts, comp);
                alt = nf.Block(blk.position(), stmts);
            }
            return i.consequent(cons).alternative(alt);
        } else if (n instanceof Loop) {
            Loop l = (Loop) n;
            if (l.body() instanceof Block) {
                Block blk = (Block) l.body();
                List<Stmt> stmts = new ArrayList<Stmt>(blk.statements());
                Collections.sort(stmts, comp);
                return l.body(nf.Block(blk.position(), stmts));
            }
            return l;
        } else if (n instanceof Try) {
            Try t = (Try) n;
            Block tryblk = (Block) t.tryBlock();
            List<Stmt> trystmts = new ArrayList<Stmt>(tryblk.statements());
            Collections.sort(trystmts, comp);
            tryblk = nf.Block(tryblk.position(), trystmts);
            t = t.tryBlock(tryblk);

            if (t.finallyBlock() != null) {
                Block finblk = (Block) t.finallyBlock();
                List<Stmt> finstmts = new ArrayList<Stmt>(finblk.statements());
                Collections.sort(finstmts, comp);
                finblk = nf.Block(finblk.position(), finstmts);
                t = t.tryBlock(finblk);
            }

            if (t.catchBlocks() != null) {
                List<Catch> newCatches = new ArrayList<Catch>();
                for (Catch c : t.catchBlocks()) {
                    Block catblk = (Block) c.body();
                    List<Stmt> catstmts = new ArrayList<Stmt>(
                            catblk.statements());
                    Collections.sort(catstmts, comp);
                    catblk = nf.Block(catblk.position(), catstmts);
                    newCatches.add(c.body(catblk));
                }
                t = t.catchBlocks(newCatches);
            }
            return t;
        } else if (n instanceof CodeDecl) {
            CodeDecl cd = (CodeDecl) n;
            if (cd.body() != null) {
                Block blk = (Block) cd.body();
                List<Stmt> stmts = new ArrayList<Stmt>(blk.statements());
                Collections.sort(stmts, comp);
                return cd.body(nf.Block(blk.position(), stmts));
            }
            return cd;
        }
        return n;
    }

}
