package solomon.analysis.partition;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import polyglot.ast.ClassDecl;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.main.Report;
import polyglot.types.CodeInstance;
import polyglot.types.ProcedureInstance;
import polyglot.types.TypeSystem;
import polyglot.util.InternalCompilerError;
import polyglot.visit.NodeVisitor;
import solomon.SolExtensionInfo;
import solomon.SolOptions;
import solomon.analysis.SolStmtRegistrar;
import solomon.analysis.partition.ext.PGExt;
import solomon.analysis.partition.ext.PGUtil;
import solomon.analysis.partition.graph.PGEdge;
import solomon.analysis.partition.graph.PGLocation;
import solomon.analysis.partition.graph.PGNode;
import solomon.analysis.partition.graph.PartitionGraph;
import accrue.ObjAnalExtensionInfo;
import accrue.analysis.callgraph.PreciseCallGraphFactory;
import accrue.analysis.ext.ObjanalExt_c;
import accrue.analysis.interprocanalysis.AbstractLocation;
import accrue.analysis.interprocanalysis.AnalysisContext;
import accrue.analysis.interprocanalysis.AnalysisContextMap;
import accrue.analysis.interprocanalysis.EdgeIdentifier;
import accrue.analysis.interprocanalysis.NativeNodeIdentifier;
import accrue.analysis.interprocanalysis.NodeIdentifier;
import accrue.analysis.interprocvarcontext.SignatureRepository;
import accrue.analysis.pointer.PointerAnalysisPass;

public abstract class PartitionGraphBuilder extends NodeVisitor {
    public static final String ANALYSIS_NAME = "solomon.analysis.partitionBuilder";
    protected static final String[] TOPICS = new String[] { "partition" };

    private static final double minimum_latency = 1e-9; //at least a picosecond (in ms)

    protected final double BANDWIDTH;
    protected final double LATENCY;


    protected final SolExtensionInfo extInfo;
    protected final TypeSystem ts;
    protected final NodeFactory nf;
    protected final PartitionGraph pg;
    protected final SolStmtRegistrar registrar;
    protected PGNode currentNode;
    protected PGNode currentScope;
    
    public PartitionGraphBuilder(SolExtensionInfo extInfo) {
        this.extInfo = extInfo;
        this.ts = extInfo.typeSystem();
        this.nf = extInfo.nodeFactory();
        this.pg = extInfo.partitionGraph();
        this.registrar = (SolStmtRegistrar)PointerAnalysisPass.singleton(extInfo).registrar();
        SolOptions opt = extInfo.getOptions();
        this.BANDWIDTH = opt.bandwidthGBps()*1000*1e10;
        this.LATENCY = opt.latencyms();
    }

    public SolExtensionInfo extensionInfo() {
        return extInfo;
    }
    public PartitionGraph partitionGraph() {
        return pg;
    }

    @Override
    public void finish() {
        setWeights();
    }

    @Override
    /**
     * Skip interfaces.  Currently this will also prevent partitioning of 
     * member classes declared in interfaces.
     */
    public Node override(Node parent, Node n) {
        if (n instanceof ClassDecl) {
            ClassDecl cd = (ClassDecl) n;
            if (cd.flags().isInterface())
                return n;
        }
        return null;
    }

    @Override
    public NodeVisitor enter(Node n) {
        PGExt ext = PGUtil.ext(n);
        if (ext != null) {
           return ext.acceptPGBuilderEnter((PartitionGraphBuilder) this);
        }
        return this;
    }

    @Override
    public Node leave(Node parent, Node old, Node n, NodeVisitor v) {
        PGExt ext = PGUtil.ext(n);
        if (ext != null) {
            ext.acceptPGBuilder((PartitionGraphBuilder) v);
        }
        return n;
    }

    // subclasses define how to add edges from currentNode to or from an EI
    public abstract void addInEdge(EdgeIdentifier edge);
    public abstract void addOutEdge(EdgeIdentifier edge);
    public abstract void addUpdateEdge(AbstractLocation e);
    public abstract void addOutputEdge(EdgeIdentifier edge);
    public abstract void addAntiEdge(EdgeIdentifier edge);
    public abstract PGLocation locationOf(NodeIdentifier nodeID);
    protected abstract double getWeight(PGNode node);
    protected abstract double getWeight(PGEdge node);

    public PGLibrarySignature getSignature(ProcedureInstance pi) {
        SignatureRepository<?> r = ((ObjAnalExtensionInfo) extInfo)
                .getAnalysisSignatures().signatureRepository(ANALYSIS_NAME);
        if (r != null) {
            PGLibrarySignature ls = (PGLibrarySignature) r.lookup(pi);
            if (ls != null)
                return ls;
        }
        return null;
    }

    public Set<EdgeIdentifier> missingTargets(Node n) {
        ObjanalExt_c oaExt = (ObjanalExt_c) ObjanalExt_c.ext(n);
        // Get all the control dependencies of this dep
        AnalysisContextMap<Set<EdgeIdentifier>> map = oaExt
                .getAnalysisResult(PreciseCallGraphFactory.CALL_TARGETS);
        Set<EdgeIdentifier> missingTargets = new LinkedHashSet<EdgeIdentifier>();
        if (map != null) {
            // Merge ctl dependencies for all contexts
            for (AnalysisContext ac : map.contexts()) {
                Set<EdgeIdentifier> edgeIds = map.get(ac);
                for (EdgeIdentifier edge : edgeIds) {
                    if (edge.getTarget().isNative()) {                        
                        missingTargets.add(edge);
                    }
                }
            }
            if (Report.should_report("partition", 3)) {
                Report.report(3, "Call " + n + " has missing targets "
                        + missingTargets);
            }
            return missingTargets;
        } else {
            if (Report.should_report("partition", 3)) {
                Report.report(3, "Call " + n + " has no map");
            }
            
            return Collections.emptySet();
        }
    }
    
    public void setWeights() {
        // set node weights from instrumentation
        for (PGNode node : pg.nodes()) {
            NodeIdentifier nodeID = node.nodeID();
            if (!nodeID.isNative()) {
                double d = getWeight(node);
                if (d < 0) throw new InternalCompilerError("Node has negative weight: " + node);
                node.setWeight(d);
            }
        } 
        
        // "push" weights of call sites to native code
        Set<PGEdge> nativeEdges = new HashSet<PGEdge>();
        for (PGEdge edge : pg.edges()) {
            NodeIdentifier fromID = edge.from().nodeID();
            NodeIdentifier toID = edge.to().nodeID();
            if (toID.isNative()) {
                nativeEdges.add(edge);
                // Set execution count from call sites.
                if (pg.controlEdge(edge.key()) && !fromID.isNative()) {                
                    double old = edge.to().weight();
                    edge.to().setWeight(old + edge.from().weight());                
                }
            }
        }
        // now set edge weights from instrumentation
        for (PGEdge edge : pg.edges()) {
            if (pg.controlEdge(edge.key()) || pg.interDataEdge(edge.key()) || pg.updateEdge(edge.key())) {
                double w = getWeight(edge);
                if (w < 0)
                    throw new InternalCompilerError(
                            "Edge has negative weight: " + edge.from() + "--"
                                    + edge.key() + "-->" + edge.to());
                if (Double.isNaN(w)) {
                    throw new InternalCompilerError("Edge has NaN weight:"
                            + edge.from() + "--" + edge.key() + "-->"
                            + edge.to());
                }
                
                edge.setWeight(w);
            }
        }
        
        // now add edges and nodes using signatures.
        for (PGEdge edge : nativeEdges) {
            NativeNodeIdentifier toID = (NativeNodeIdentifier) edge.to().nodeID();
            CodeInstance ci = toID.getCodeInstance();
            if (ci instanceof ProcedureInstance) {
                PGLibrarySignature sig = getSignature((ProcedureInstance)ci);
                if (sig != null) {
                    sig.addMissingDependencies(this, edge);
                }
            }            
        }
    }
    

    protected double dataLatency(double size) {
        //XXX: should we create lower bound to reduce small-weight constraints?
        double l = size / BANDWIDTH;
        return (l > minimum_latency) ? l : minimum_latency;
    }
}
