package solomon.analysis.visit;

import java.util.ArrayList;
import java.util.List;

import polyglot.ast.Block;
import polyglot.ast.Empty;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.Stmt;
import polyglot.frontend.Job;
import polyglot.types.SemanticException;
import polyglot.types.TypeSystem;
import polyglot.visit.AlphaRenamer;
import polyglot.visit.ErrorHandlingVisitor;
//XXX: TODO: FIXME: This is a good idea anyway, but was created to avoid a bug in building the 
// partition graph. The bug occurs because control transfers are not properly handled for nested blocks.
public class BlockFlattener extends ErrorHandlingVisitor {
    protected final AlphaRenamer alpha;

    public BlockFlattener(Job job, TypeSystem ts, NodeFactory nf) {
        super(job, ts, nf);
        this.alpha = new AlphaRenamer(nf);
    }

    @Override
    protected Node leaveCall(Node n) throws SemanticException {
        if (n instanceof Block) {
            // Sort statements by dependency order
            Block b = (Block) n;
            
            //Collapse blocks
            List<Stmt> stmts = new ArrayList<Stmt>(b.statements());
            boolean found_block;
            do {
                found_block = false;
                List<Stmt> newStmts = new ArrayList<Stmt>();    
                for (Stmt s : stmts) {    
                    if ( s instanceof Block ) {
                        found_block = true;
                        Block sub = (Block) s.visit(alpha);
                        newStmts.addAll(sub.statements());
                    }
                    else if (s instanceof Empty) {
                        // do nothing
                    } else
                        newStmts.add(s);
                }
                stmts = newStmts;
            } while (found_block);
            return nf.Block(b.position(), stmts);
        }
        return n;
    }

}
