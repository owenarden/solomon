package solomon.analysis.visit;

import polyglot.ast.Node;
import polyglot.ast.Synchronized;
import polyglot.visit.NodeVisitor;

public class SynchRemover extends NodeVisitor {
    @Override
    public Node leave(Node old, Node n, NodeVisitor v) {
        if (n instanceof Synchronized) {
            Synchronized s = (Synchronized) n;
            System.err.println("WARNING: Ignoring synchronize keyword at " + n.position());
            return s.body();
        }
        return n;
    }
}
