package solomon.analysis.ext;

import polyglot.ast.Ext;
import polyglot.ast.ExtFactory;
import accrue.analysis.ext.AnalysisExtFactory_c;

public class SolAnalysisExtFactory_c extends AnalysisExtFactory_c {

    public SolAnalysisExtFactory_c() {
    }

	public SolAnalysisExtFactory_c(ExtFactory nextFactory) {
	    super(nextFactory);
    }

    @Override
	protected Ext extNewImpl() {
		return new SolExtNew();
	}

	@Override
	protected Ext extNewArrayImpl() {
		return new SolExtNewArray();
	}

	@Override
	protected Ext extLocalDeclImpl() {
		return new SolExtLocalDecl();
	}

	@Override
	protected Ext extFieldDeclImpl() {
		return new SolExtFieldDecl();
	}

	@Override
	protected Ext extMethodDeclImpl() {
		return new SolExtMethodDecl();
	}
}
