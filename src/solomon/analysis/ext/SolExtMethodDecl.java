package solomon.analysis.ext;

import accrue.ObjAnalUtil;
import accrue.analysis.ext.ExtMethodDecl;
import accrue.analysis.pointer.LocalNode;
import accrue.analysis.pointer.RegisterPointerStmtsVisitor;
import accrue.analysis.pointer.StmtRegistrar;
import accrue.analysis.pointer.StmtRegistrar.CodeInfo;
import polyglot.ast.MethodDecl;
import polyglot.types.Type;
import solomon.analysis.SolStmtRegistrar;

public class SolExtMethodDecl extends ExtMethodDecl {
    private static final long serialVersionUID = 1L;
    @Override
	public void registerPointerStmts(StmtRegistrar registrar,
            RegisterPointerStmtsVisitor v) {
    	SolStmtRegistrar solRegistrar = (SolStmtRegistrar) registrar;
        MethodDecl md = (MethodDecl)this.node();
        if (ObjAnalUtil.isMainMethod(md.procedureInstance())) {
            // this is the main method, public static void main(String[])
            // make the argument point to a new array...

            Type stringArray = (Type)md.methodInstance().formalTypes().get(0);
			LocalNode argsArray = solRegistrar.registerStaticAlloc(stringArray,
					"main-args", v.currentCode(), md.position(), this);

            CodeInfo codeInfo = solRegistrar.getCodeInfo(md);
            LocalNode formalLN = codeInfo.formals.get(0);

            solRegistrar.addAssignment(formalLN, argsArray, md.position(), v
                    .currentCode());

            // the contents points to some literals...
            // Treat it as a special kind of alloc site for now.
            LocalNode lits = solRegistrar.registerLiteral(v.typeSystem().String(),
                                                       "main-args", v.currentCode(),
                                                       v.constructorContext(), md.position(), this);

            solRegistrar
                    .addAssignmentToField(argsArray, StmtRegistrar.ARRAY_CONTENTS,
                                          v.typeSystem().String(), lits, md
                                                  .position(), v.currentCode());
        }
        
        //XXX: can't execute the super class method, but parent 
        // executes grandparent's method. Ok to skip since its empty.
        //super.registerPointerStmts(solRegistrar, v);
    }

}
