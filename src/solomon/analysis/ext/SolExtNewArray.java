package solomon.analysis.ext;

import polyglot.ast.NewArray;
import polyglot.types.Type;
import solomon.analysis.SolStmtRegistrar;
import accrue.analysis.ext.ExtNewArray;
import accrue.analysis.pointer.LocalNode;
import accrue.analysis.pointer.RegisterPointerStmtsVisitor;
import accrue.analysis.pointer.StmtRegistrar;

public class SolExtNewArray extends ExtNewArray {
    private static final long serialVersionUID = 1L;
	@Override
	public void registerPointerStmts(StmtRegistrar registrar,
			RegisterPointerStmtsVisitor v) {
		SolStmtRegistrar solRegistrar = (SolStmtRegistrar) registrar;

		NewArray n = (NewArray) this.node();
		solRegistrar.makeFreshLocalNode(n, v.constructorContext());
		if (n.init() == null) {
			// no initializer, need to take care of multiple dimensions.

			LocalNode ln = v.getLocalNode(n);
			solRegistrar.registerAlloc(this, n.type(), v.currentCode(),
					v.constructorContext());
			Type type = n.type().toArray().base();
			for (int i = 1; i < n.dims().size(); i++) {
				// ln points to the contents of the array
				// make a new alloc, and set "ln.contents = new"
				// that is, make two equations "ln' = new; ln.contents = ln'"
				LocalNode lnp = solRegistrar.registerAlloc(type.toReference(),
						n.position(), type.toString(), v.currentCode(), this,
						v.constructorContext());
				solRegistrar.addAssignmentToField(ln,
						StmtRegistrar.ARRAY_CONTENTS, type, lnp, n.position(),
						v.currentCode());

				type = n.type().toArray().base();
				ln = lnp;
			}
		} else {
			// there is an array initializer that has already recorded the
			// allocation site
			solRegistrar.addAssignment(v.getLocalNode(n),
					v.getLocalNode(n.init()), n.position(), v.currentCode());
		}

	}

}
