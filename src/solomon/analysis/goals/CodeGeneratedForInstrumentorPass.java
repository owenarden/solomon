package solomon.analysis.goals;

import java.io.IOException;

import javax.tools.FileObject;

import polyglot.ast.SourceFile;
import polyglot.frontend.FileSource;
import polyglot.frontend.OutputPass;
import polyglot.frontend.TargetFactory;
import polyglot.frontend.goals.Goal;
import polyglot.util.ErrorInfo;
import polyglot.util.ErrorQueue;
import polyglot.util.Position;
import polyglot.visit.Translator;
import solomon.InstExtensionInfo;

/**
 * This pass translates a source file then reparses it.
 */
public class CodeGeneratedForInstrumentorPass extends OutputPass {

    protected InstExtensionInfo extInfo;
    protected TargetFactory targetFactory;

    public CodeGeneratedForInstrumentorPass(Goal goal, InstExtensionInfo extInfo,
            TargetFactory tf, Translator tr) {
        super(goal, tr);
        this.extInfo = extInfo;
        this.targetFactory = tf;
    }

    /**
     * Add each generated source file to the Instrumentor's scheduler
     */
    @Override
    public boolean run() {
        if (super.run()) {
            ErrorQueue eq = extInfo.compiler().errorQueue();
            try {
                SourceFile old_sf = (SourceFile) goal.job().ast();
                String pkg = "";
                if(old_sf.package_() != null)
                    pkg = old_sf.package_().package_().fullName();
                FileObject output = targetFactory.outputFileObject(pkg, old_sf.source());
                FileSource source = extInfo.createFileSource(output, false);
                extInfo.scheduler().addJob(source);
                return true;
            } catch (IOException e) {
                eq.enqueue(ErrorInfo.IO_ERROR, e.getMessage(), new Position(
                        goal.job().source().path(), goal.job().source().name(),
                        1, 1, 1, 1));
                return false;
            }
        }
        return false;
    }
}
