package solomon.analysis.goals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.tools.JavaFileManager.Location;

import polyglot.filemanager.FileManager;
import polyglot.frontend.ExtensionInfo;
import polyglot.frontend.Job;
import polyglot.frontend.Pass;
import polyglot.frontend.Scheduler;
import polyglot.frontend.TargetFactory;
import polyglot.frontend.goals.Goal;
import polyglot.frontend.goals.SourceFileGoal;
import polyglot.util.InternalCompilerError;
import polyglot.visit.Translator;
import solomon.SolExtensionInfo;
import solomon.SolScheduler;

public class CodeGeneratedForInstrumentor extends SourceFileGoal {
    public static Goal create(ExtensionInfo extInfo, Job job) {
        return new CodeGeneratedForInstrumentor(job);
    }
    
    public static Goal create(ExtensionInfo extInfo, Job job, Location outputLocation) {
        return new CodeGeneratedForInstrumentor(job, outputLocation);
    }

    protected final Location outputLocation;
    protected final TargetFactory targetFactory;

    protected CodeGeneratedForInstrumentor(Job job) {
        this(job, createTempLocation(job));
    }
    
	protected CodeGeneratedForInstrumentor(Job job, Location outputLocation) {
		super(job);
		this.outputLocation = outputLocation;
		;
		this.targetFactory = new TargetFactory(job.extensionInfo().extFileManager(),
		        outputLocation, job.extensionInfo().defaultFileExtension(), false);
	}

	@Override
	public Pass createPass(ExtensionInfo extInfo) {
        Translator tr = new Translator(job,
                extInfo.typeSystem(), extInfo.nodeFactory(), targetFactory);
        SolExtensionInfo solExtInfo = (SolExtensionInfo) extInfo;
        return new CodeGeneratedForInstrumentorPass(this, 
                solExtInfo.instExtensionInfo(), targetFactory, tr);
	}
	
    public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
        List<Goal> l = new ArrayList<Goal>();
        l.add(((SolScheduler) scheduler).TransformationsDone(job));
        l.addAll(super.prerequisiteGoals(scheduler));
        return l;
    }

    private static Location createTempLocation(Job job) {
        final File temp;
        try {
            temp = File.createTempFile("temp", Long.toString(System.nanoTime()));
        } catch (IOException e) {
            throw new InternalCompilerError("Could not create temp file");
        }

        if (!(temp.delete())) {
            throw new InternalCompilerError("Could not delete temp file: "
                    + temp.getAbsolutePath());
        }

        if (!(temp.mkdir())) {
            throw new InternalCompilerError("Could not create temp directory: "
                    + temp.getAbsolutePath());
        }
        FileManager fm = job.extensionInfo().extFileManager();

        Location loc = new Location() {            
            @Override
            public boolean isOutputLocation() {
                return true;
            }            
            @Override
            public String getName() {
                return temp.getName();
            }
        };
        
        //fm.addLocation(loc);
        try {
            fm.setLocation(loc, Collections.singleton(temp));
        } catch (IOException e) {
            throw new InternalCompilerError(e);
        }
        return loc;
    }

}
