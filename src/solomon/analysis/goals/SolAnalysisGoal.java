/**
 * 
 */
package solomon.analysis.goals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import polyglot.frontend.EmptyPass;
import polyglot.frontend.ExtensionInfo;
import polyglot.frontend.Pass;
import polyglot.frontend.Scheduler;
import polyglot.frontend.goals.AbstractGoal;
import polyglot.frontend.goals.Goal;
import solomon.SolScheduler;

public class SolAnalysisGoal extends AbstractGoal {
    public static Goal singleton(ExtensionInfo extInfo) {
        Scheduler scheduler = extInfo.scheduler();
        return scheduler.internGoal(new SolAnalysisGoal(extInfo));
    }

    protected SolAnalysisGoal(ExtensionInfo extInfo) {
        super(null, "SolAnalysisGoal");
    }

	public Collection<Goal> prerequisiteGoals(Scheduler scheduler) {
        List<Goal> l = new ArrayList<Goal>();
        l.add(((SolScheduler)scheduler).SolvePartitionProblem());
        l.addAll(super.prerequisiteGoals(scheduler));
        return l;
    }

    public Pass createPass(ExtensionInfo extInfo) {
        return new EmptyPass(this);
    }
}
