package solomon.analysis;

import polyglot.ast.CodeDecl;
import polyglot.ast.Term;
import polyglot.main.Report;
import polyglot.util.Position;
import accrue.analysis.AnalysisTopics;
import accrue.analysis.ext.ObjanalExt;
import accrue.analysis.interprocanalysis.AnalysisContext;
import accrue.analysis.interprocanalysis.NodeIdentifier;
import accrue.analysis.interprocanalysis.NodeIdentifier_c;
import accrue.analysis.pointer.AllocSiteNode;
import accrue.analysis.pointer.CContext;
import accrue.analysis.pointer.ConstructorContext;
import accrue.analysis.pointer.HContext;
import accrue.analysis.pointer.LocalNode;
import accrue.analysis.pointer.PointsToEngine.StmtAndContext;
import accrue.analysis.pointer.PointsToGraph;
import accrue.analysis.pointer.ReferenceVariableReplica;
import accrue.analysis.pointer.StmtAllocToLocal;
import accrue.analysis.pointer.StmtRegistrar;
import accrue.analysis.pointer.analyses.HeapAbstractionFactory;

/**
 * Processes allocations identically to parent class, but associates each new
 * HContext with a NodeIdentifier.
 */
public class SolStmtAllocToLocal extends StmtAllocToLocal {
	protected ConstructorContext constructorContext;
	protected ObjanalExt ext;
	
	public SolStmtAllocToLocal(LocalNode left, AllocSiteNode right,
			Position origin, CodeDecl proc, ObjanalExt ext, ConstructorContext ctorCtx) {
		super(left, right, origin, proc);
		this.ext = ext;
		this.constructorContext = ctorCtx;
	}
	
    @Override
	public boolean process(CContext context, PointsToGraph g, StmtRegistrar registrar, HeapAbstractionFactory af, StmtAndContext sac) {
        SolStmtRegistrar solRegistrar = (SolStmtRegistrar) registrar;

    	if (Report.should_report(AnalysisTopics.POINTSTO, 3))
            Report.report(3, "Processing alloc to local " + this + " in context " + context);

        HContext hCtx = af.record(context, right);
        AnalysisContext aCtx = new AnalysisContext(context, constructorContext);
        NodeIdentifier nodeID = new NodeIdentifier_c(aCtx, (Term)ext.node());

        solRegistrar.registerAllocationID(hCtx, nodeID);

        return g.addEdge(sac, ReferenceVariableReplica.create(context, left, af), 
                   hCtx, origin);
    }
}
