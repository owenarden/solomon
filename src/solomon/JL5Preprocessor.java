package solomon;

import polyglot.ext.jl5.JL5Options;
import polyglot.main.OptFlag.Arg;
import polyglot.main.Options;
import polyglot.main.UnhandledArgument;
import polyglot.main.UsageError;
import accrue.ObjAnalJL5TranslationExtensionInfo;

public class JL5Preprocessor extends ObjAnalJL5TranslationExtensionInfo {
    
    public JL5Preprocessor(SolOutputExtensionInfo outputExtensionInfo) {
        super(outputExtensionInfo);
        outputExtensionInfo.setParent(this);
    }

    @Override
    public String defaultFileExtension() {
        return "sol";
    }
    
    @Override
    public String[] defaultFileExtensions() {
        String ext = defaultFileExtension();
        return new String[] { ext, "java" };
    }
    
    public String compilerName() {
        return "solc";
    }

    @Override
    protected Options createOptions() {
        return new JL5SolOptions(this);
    }
    
    /**
     * Support Solomon options at the command line 
     */
    public static class JL5SolOptions extends JL5Options {
        protected SolOptions solOpt;

        public JL5SolOptions(polyglot.frontend.ExtensionInfo extension) {
            super(extension);
            solOpt = new SolOptions(extension);
            flags().addAll(solOpt.flags());
        }
        
        @Override
        protected void postApplyArgs() {
            super.postApplyArgs();
            this.removeJava5isms = true;
        }
        
        @Override
        protected void handleArg(Arg<?> arg) throws UsageError {
            try {
                super.handleArg(arg);
            } catch (UnhandledArgument ua) {
                // solomon options will be handled after the hand-off
                if (!solOpt.flags().contains(ua.argument().flag())) {
                    throw ua;
                }
            }
        }
    }
}
