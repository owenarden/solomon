package solomon;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.tools.FileObject;
import javax.tools.JavaFileManager.Location;

import polyglot.ast.ExtFactory;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.frontend.Compiler;
import polyglot.frontend.ExtensionInfo;
import polyglot.frontend.FileSource;
import polyglot.frontend.Job;
import polyglot.frontend.JobExt;
import polyglot.frontend.Scheduler;
import polyglot.frontend.TargetFactory;
import polyglot.frontend.goals.Goal;
import polyglot.translate.ext.ToExt;
import polyglot.translate.ext.ToExtFactory_c;
import polyglot.translate.ext.ToExt_c;
import polyglot.types.FieldInstance;
import polyglot.util.InternalCompilerError;
import solomon.analysis.SolStmtRegistrar;
import solomon.analysis.ext.SolAnalysisExtFactory_c;
import solomon.analysis.instrumentor.ext.InstExtFactory_c;
import solomon.analysis.partition.ContextFreePGBuilder;
import solomon.analysis.partition.PartitionGraphBuilder;
import solomon.analysis.partition.ext.PGExtFactory_c;
import solomon.analysis.partition.graph.ContextFreePG;
import solomon.analysis.partition.graph.PartitionGraph;
import solomon.analysis.signatures.SolAnalysisSignatures;
import solomon.analysis.signatures.SolAnalysisSignatures.DBLocation;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import solomon.constraints.gurobi.GurobiSolverUtil;
import solomon.constraints.lpsolve.LPSolverUtil;
import solomon.solviz.SolvizDelFactory;
import accrue.ObjAnalExtensionInfo;
import accrue.analysis.AnalysisTopics;
import accrue.analysis.ast.ObjAnalNodeFactory_c;
import accrue.analysis.goals.RegisterDeclsGoal;
import accrue.analysis.goals.RegisterExpressionsGoal;
import accrue.analysis.interprocanalysis.AnalysisContext;
import accrue.analysis.pointer.CContext;
import accrue.analysis.pointer.ConstructorContext;
import accrue.analysis.pointer.StmtRegistrar;

/**
 * Extension information for Solomon extension.
 */
public class SolExtensionInfo extends ObjAnalExtensionInfo {
    static {
        // force Topics to load
        @SuppressWarnings("unused")
		AnalysisTopics t = new AnalysisTopics();
    }
    protected PartitionGraph pg;
    protected AnalysisContext ANY_CONTEXT;
    protected SolAnalysisSignatures analysisSignatures;
	protected Map<Node, Node> enclosingStmts;
	protected Map<FieldInstance, Node> declarations;
	protected PilExtensionInfo pilExtensionInfo;
	protected InstExtensionInfo instExtensionInfo;
    protected double cpu_budget = -1;
    protected SolverUtil solverUtil;

    @Override
    public String[] defaultFileExtensions() {
        String ext = defaultFileExtension();
        return new String[] { ext, "java" };
    }
    @Override
    public String defaultFileExtension() {
        return "sol";
    }

    public String compilerName() {
        return "solc";
    }

    @Override
    public void initCompiler(Compiler compiler) {
        super.initCompiler(compiler);
        pilExtensionInfo().initCompiler(compiler);
        instExtensionInfo().initCompiler(compiler);
    }
    
    protected NodeFactory createNodeFactory() {
    	boolean instrument = !getOptions().load_profile && getOptions().profile;
        ExtFactory extFactory;
    	if (instrument) {
    	       extFactory = new InstExtFactory_c(instrument, new SolAnalysisExtFactory_c());
    	}
    	extFactory = new PGExtFactory_c(
                new InstExtFactory_c(instrument, new SolAnalysisExtFactory_c(new ToExtFactory_c())));
        if(getOptions().outputGraph()) {
            return new ObjAnalNodeFactory_c(extFactory, new SolvizDelFactory());
        }
        else {
            return new ObjAnalNodeFactory_c(extFactory);
        }
    }
    
    protected Scheduler createScheduler() {
        return new SolScheduler(this);
    }
    
    @Override
    public SolOptions getOptions() {
        return (SolOptions) super.getOptions();
    }
    
    public Goal getCompileGoal(Job job) {
        SolScheduler scheduler = (SolScheduler) this.scheduler;
        Goal g = null;
        if (getOptions().outputPil()) {
            g = scheduler.PilGenerated(job);
        }
        else if (getOptions().profile) {
            g = scheduler.CodeGeneratedForInstrumentor(job);
        }
        else {
            g = scheduler.SolToPyxILRewritten(job);
        }       
        return g;
    }

    public SolAnalysisSignatures getAnalysisSignatures() {
        if (analysisSignatures == null) {
            analysisSignatures = createAnalysisSignatures();
        }
        return analysisSignatures;    
    }

    protected SolAnalysisSignatures createAnalysisSignatures() {
        return new SolAnalysisSignatures();
    }

    @Override
    protected SolOptions createOptions() {
        return new SolOptions(this);
    }

    public AnalysisContext anyContext() {
        if (ANY_CONTEXT == null) {
            CContext empty = heapAbstractionFactory().initialContext();
            ANY_CONTEXT = new AnalysisContext(empty, ConstructorContext.NORMAL) {
                public String toString() {
                    return "ANY_CONTEXT";
                }
            };
        }
        return ANY_CONTEXT;
    }
    public PartitionGraph partitionGraph() {
    	if(pg == null) {
    	    pg = new ContextFreePG(anyContext());
    		pg.initColors(DBLocation.APPSERVER, DBLocation.DBSERVER);
    	}
    	return pg;
    }

    public JobExt jobExt() {
        return new SolJobExt();
    }

    //XXX: This is a hack that should be moved to SolStmtRegistrar
    public Map<Node, Node> enclosingStmts() {
    	if(enclosingStmts == null) 
    		return enclosingStmts = RegisterExpressionsGoal.singleton(this).enclosingStmt();
    	return enclosingStmts;
    }
    
    //XXX: This is a hack that should be moved to SolStmtRegistrar
    public Map<FieldInstance, Node> fieldDeclarations() {
    	if(declarations == null) 
    		return declarations = RegisterDeclsGoal.singleton(this).decls();
    	return declarations;
    }
    
    public PartitionGraphBuilder createPGBuilder() {
    	return new ContextFreePGBuilder(this);
    }
    
    @Override
    public StmtRegistrar createStmtRegistrar() {
    	return new SolStmtRegistrar(this);
    }
    
    /** 
     * Return the ToExt object for rewriting AST nodes.
     * @param to_ext is the ExtensionInfo of the target language
     * @param n the node to rewrite.
     * @return
     */
    public ToExt getToExt(ExtensionInfo to_ext, Node n) {
        return ToExt_c.ext(n);
    }
    
    public PilExtensionInfo pilExtensionInfo() {
        if(pilExtensionInfo == null) {
            pilExtensionInfo = new PilExtensionInfo(this);
        }
        return pilExtensionInfo;
    }
    
    public TargetFactory solvizTargetFactory() {
        if (target_factory == null) {
            target_factory = new TargetFactory(extFileManager(), 
                                                getOptions().outputLocation(),
                                               "html",
                                               getOptions().output_stdout);
        }

        return target_factory;
    }
    
    public TargetFactory pilTargetFactory() {
        if (target_factory == null) {
            target_factory = new TargetFactory(extFileManager(),
                                               getOptions().outputLocation(),
                                               "pil",
                                               getOptions().output_stdout);
        }

        return target_factory;
    }
    public InstExtensionInfo instExtensionInfo() {
        if (instExtensionInfo == null) {
            instExtensionInfo = new InstExtensionInfo(this);
        }
        return instExtensionInfo;
    }
    
    public double cpuBudget() throws SolverException {
        if (cpu_budget == -1) {
            PartitionGraph graph = partitionGraph();
            SolOptions opt = getOptions();
            cpu_budget = opt.cpuBudget();
            double minBudget = graph.minimumBudget(DBLocation.DBSERVER);
            double maxBudget = graph.maximumBudget(DBLocation.DBSERVER);
    
            System.out.println("Minimum budget for graph is " + minBudget);
            System.out.println("Maximum budget for graph is " + maxBudget);
    
            if (cpu_budget == -1) {
                cpu_budget = opt.percentOfMaxBudget() * maxBudget;
            }
            if (minBudget > cpu_budget)
                throw new SolverException("Budget " + cpu_budget + " is too low. Minimum budget is " + minBudget);
            if (maxBudget < cpu_budget)
                System.err.println("INFO: Specified budget is higher" +
                        " than necessary. The maximum budget is " + maxBudget);
            
            System.out.println("Budget is " + cpu_budget);
        }        
        return cpu_budget;
    }
    
    public SolverUtil solverUtil() throws SolverException {
        if (solverUtil == null) {
            SolOptions opt = getOptions();
            if (opt.solver().equals("lpsolve")) {
               solverUtil = new LPSolverUtil();
            }
            else if (opt.solver().equals("gurobi")) {
                solverUtil = new GurobiSolverUtil();
            }
            else {
                throw new InternalCompilerError("Unknown solver: " + opt.solver());
            }
        }
        return solverUtil;
    }
    @Override
    public FileSource createFileSource(FileObject f, boolean user) throws IOException {
        Location outdir = getOptions().outputLocation();
        File indir = new File(f.toUri().getPath()).getAbsoluteFile();
        for (File out : extFileManager().getLocation(outdir)) {     
            // Sanity check to avoid overwriting original files:        
            if (f.getName().endsWith("java") && out.equals(indir)) 
                throw new IOException("Input file " + f + " would be overwritten. Did you forget to specify an output directory?");
        }
        return super.createFileSource(f, user);
    }
    
}
