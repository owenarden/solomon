package solomon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import polyglot.ext.jl5.ExtensionInfo;

public class Main extends polyglot.main.Main {
    public static void main(String[] args) {
        boolean skipJL5 = false;
        Main main = new Main();
        List<String> arglist = new ArrayList<>(Arrays.asList(args));
        for (Iterator<String> it = arglist.iterator(); it.hasNext();) {
            if (it.next().equals("-skip-preprocessor")) {
                skipJL5 = true;
                it.remove();
            }
        }
        try {
            if (!skipJL5) {    
                SolOutputExtensionInfo outInfo = new SolOutputExtensionInfo();
                ExtensionInfo extInfo = new solomon.JL5Preprocessor(outInfo);
                main.start(arglist.toArray(new String[] {}), extInfo);
            } 
            else {
                SolExtensionInfo extInfo = new solomon.SolExtensionInfo();
                main.start(arglist.toArray(new String[] {}), extInfo);
            }
        } catch (TerminationException te) {
            if (te.getMessage() != null) {
                (te.exitCode == 0 ? System.out : System.err).println(te
                        .getMessage());
            }
            System.exit(te.exitCode);
        }
    }
}
