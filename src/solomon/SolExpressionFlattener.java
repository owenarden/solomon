package solomon;

import polyglot.ast.Binary;
import polyglot.ast.Call;
import polyglot.ast.Cast;
import polyglot.ast.Expr;
import polyglot.ast.Field;
import polyglot.ast.Local;
import polyglot.ast.Node;
import polyglot.ast.NodeFactory;
import polyglot.ast.Special;
import polyglot.ast.TypeNode;
import polyglot.frontend.Job;
import polyglot.types.TypeSystem;
import polyglot.visit.ExpressionFlattener;
import polyglot.visit.NodeVisitor;

/**
 * Some modifications to ExpressionFlattener
 * 1. Always flatten string concatenations so that a local is created for them.  
 *      This forces the pointer analysis to create a local node for them which is required if
 *       we want to reason about the pointsTo set of String valued vars.
 * 2. Prevent some simple expressions from being completely flattened.  
 *      In essence, this means treating the following as 'flat' (i.e. like a variable)
 *      -- Simple fields: Type.f, this.f, x.f
 *      -- Simple calls:  this.m(), x.m()
 */
public class SolExpressionFlattener extends ExpressionFlattener {

    protected boolean lessFlattened = false;
    public SolExpressionFlattener(Job job, TypeSystem ts, NodeFactory nf,
            boolean flatten_all_decls) {
        super(job, ts, nf, flatten_all_decls);
    }

    @Override
    public NodeVisitor enter(Node parent, Node n) {
        if (!lessFlattened) 
            return super.enter(parent, n);
            
        if (parent instanceof Binary) {
            // String concatenation expressions require flattening 
            // so that local nodes are created for them.
            Binary b = (Binary) parent;
            if (b.type().equals(ts.String()))
                return super.enter(n);
        }

        if (n instanceof Field) {
                Field f = (Field) n;
                if (isSimpleField(f)) {
                    addDontFlatten(f);
                }
        }
        else if (n instanceof Call) {
            if (isSimpleCall((Call) n)) {
                addDontFlatten((Expr) n);
            }
        } else if (n instanceof Cast) {
            if (isSimpleExpression(parent, (Expr) n)) {
                addDontFlatten((Expr) n);
            }
        }
        return super.enter(n);
    }

    protected boolean isSimpleExpression(Node parent, Expr e) {
        if (e instanceof Field) {
            return isSimpleField((Field) e);
        } else if (e instanceof Call) {
            return isSimpleCall((Call) e);
        }
        return e instanceof Local;
        
    }

    protected boolean isSimpleField(Field f) {
        return (f.target() instanceof TypeNode || f.target() instanceof Local || f.target() instanceof Special);
    }
    
    protected boolean isSimpleCall(Call f) {
        return (f.target() instanceof Local || f.target() instanceof Special);
    }
}
