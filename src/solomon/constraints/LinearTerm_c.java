package solomon.constraints;


public class LinearTerm_c implements LinearTerm {

	protected final double coefficient;
	protected final SolverVariable solverVariable;
	public LinearTerm_c(double coeff, SolverVariable var) {
		this.coefficient = coeff;
		this.solverVariable = var;
	}

	@Override
	public double coeff() {
		return coefficient;
	}

	@Override
	public SolverVariable var() {
		return solverVariable;
	}
	
	@Override
	public String toString() {
	    return coefficient + "*" + solverVariable;
	}

}
