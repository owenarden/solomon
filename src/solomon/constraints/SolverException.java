package solomon.constraints;

@SuppressWarnings("serial")
public class SolverException extends Exception {

	public SolverException(String e) {
		super(e);
	}

	public SolverException(Exception e) {
		super(e);
	}

}
