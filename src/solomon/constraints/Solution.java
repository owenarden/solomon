package solomon.constraints;

import java.util.Collection;
import java.util.Set;

public interface Solution {

	double objective() throws SolverException;

	boolean isValid() throws SolverException;

	int getValue(SolverVariable var) throws SolverException;

	Solveable getSolveable(SolverVariable var);

	Set<SolverVariable> solverVariables();

	void applySolution(SolverUtil su) throws SolverException;

	Collection<Solveable> solveables();

}
