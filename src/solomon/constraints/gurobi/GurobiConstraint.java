package solomon.constraints.gurobi;

import gurobi.GRBLinExpr;
import solomon.constraints.Constraint;
import solomon.constraints.gurobi.GurobiSolverUtil.GurobiConstraintType;

public interface GurobiConstraint extends Constraint {

    GRBLinExpr expr();
    GurobiConstraintType type();

}
