package solomon.constraints.gurobi;

import gurobi.GRB;
import gurobi.GRBException;
import gurobi.GRBModel;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import polyglot.util.InternalCompilerError;

import solomon.constraints.Solution;
import solomon.constraints.Solveable;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import solomon.constraints.SolverVariable;


public class GurobiSolution implements Solution {
    private final GRBModel model;
	private final Map<SolverVariable, Solveable> solveables;
	public GurobiSolution(GRBModel model, Map<SolverVariable, Solveable> solveables) {
	    this.model = model;
	    this.solveables = solveables;
	}
	
	@Override
	public int getValue(SolverVariable var) throws SolverException {
	    GurobiVariable v = (GurobiVariable) var;
	    try {
            double d = v.grbVar().get(GRB.DoubleAttr.X);
            return (int) d;
        } catch (GRBException e) {
            throw new SolverException(e);
        }
	}
	
	@Override
	public Solveable getSolveable(SolverVariable var) {
		return solveables.get(var); 
	}
	
	@Override
	public Set<SolverVariable> solverVariables() {
		return solveables.keySet();
	}
	
	@Override
	public Collection<Solveable> solveables() {
		return solveables.values();
	}


	@Override
	public double objective() throws SolverException {
	    try {
            return model.get(GRB.DoubleAttr.ObjVal);
        } catch (GRBException e) {
            throw new SolverException(e);
        }
	}
	
	@Override
	public boolean isValid() throws SolverException {
	    try {
            return model.get(GRB.IntAttr.Status) == GRB.OPTIMAL;
        } catch (GRBException e) {
            throw new SolverException(e);
        }
	}
	
	@Override
	public void applySolution(SolverUtil su) throws SolverException {
		for(Entry<SolverVariable, Solveable> entry : solveables.entrySet()) {
			Solveable s = entry.getValue();
			SolverVariable sv = s.solverVariable();
			SolverVariable v = entry.getKey();
			if (v != sv) throw new InternalCompilerError("Thought I would get the same vars for" +s);
			s.applySolution(su, getValue(v));
		}
	}

}
