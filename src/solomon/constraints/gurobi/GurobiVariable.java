package solomon.constraints.gurobi;

import gurobi.GRBException;
import gurobi.GRBModel;
import gurobi.GRBVar;
import gurobi.GRB;
import solomon.constraints.SolverVariable;

public class GurobiVariable implements SolverVariable {
	protected final String name;
	protected final GRBVar grbVar;
	public GurobiVariable(String name, GRBVar var) {
		this.name = name;
		this.grbVar = var;
	}
	
	@Override
	public String name() {
		return name;
	}
	
    public GRBVar grbVar() {
        return grbVar;
    }

    public void setObjectiveCoeff(GRBModel model, double weight) throws GRBException {
        grbVar.set(GRB.DoubleAttr.Obj, weight);
        model.update();
    }
    
	public String toString() {
		return name;
	}
	
	public int hashCode() {
		return grbVar.hashCode();
	}
	
	public boolean equals(Object o) {
		if(o instanceof GurobiVariable) {
			return ((GurobiVariable) o).name().equals(name);
		}
		return false;
	}
}
