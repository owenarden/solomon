package solomon.constraints.gurobi;

import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBModel;
import gurobi.GRBVar;

import java.util.HashMap;
import java.util.Map;

import polyglot.main.Report;
import polyglot.util.InternalCompilerError;
import solomon.constraints.Constraint;
import solomon.constraints.ConstraintType;
import solomon.constraints.LinearTerm;
import solomon.constraints.LinearTerm_c;
import solomon.constraints.Problem;
import solomon.constraints.Solveable;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import solomon.constraints.SolverVariable;


public class GurobiSolverUtil implements SolverUtil {
	private static final String[] TOPICS = new String[] {"solver", "gurobi"};
	private int next_id = 1;
	Map<SolverVariable,Solveable> solveables = new HashMap<SolverVariable,Solveable>();
	protected final GRBEnv    env;
	protected final GRBModel  model;

	protected enum GurobiConstraintType implements ConstraintType {
		LE(GRB.LESS_EQUAL), EQ(GRB.EQUAL), GE(GRB.GREATER_EQUAL);
		char id;
		private GurobiConstraintType(char id) {
			this.id = id;
		}
		public char toGurobi() {
			return id;
		}
		public String toString() {
		    if (this.id == GRB.LESS_EQUAL)
		        return "<=";
		    if (this.id == GRB.EQUAL)
		        return "=";
		    if (this.id == GRB.GREATER_EQUAL)
		        return ">=";
		    else 
		        throw new InternalCompilerError("Invalid constraint type id " + id);
		}
	}
	
    public GurobiSolverUtil() throws SolverException {
        try {
            this.env = new GRBEnv("mip1.log");
            env.set(GRB.DoubleParam.MIPGap, 1e-9);
            this.model = new GRBModel(env);
        } catch (GRBException e) {
            throw new SolverException(e);
        }
    }

    @Override
    public GurobiVariable addSolveable(Solveable s) throws SolverException {
        GurobiVariable v = (GurobiVariable) s.solverVariable();
        if (v == null) {
            try {
                if (Report.should_report(TOPICS, 3))
                    Report.report(3, "Creating new solverVariable for " + s
                            + ": " + next_id);
                String name = "X" + next_id++;

                GRBVar var = model.addVar(0.0, 1.0, 1.0, GRB.BINARY, name);
                v = new GurobiVariable(name, var);
                model.update();
                s.assignVariable(v);
            } catch (GRBException e) {
                throw new SolverException(e);
            }
        }
        solveables.put(v, s);
        return v;
    }

	@Override
	public ConstraintType leq() {
		return GurobiConstraintType.LE;
	}
	@Override
	public ConstraintType eq() {
		return GurobiConstraintType.EQ;
	}
	@Override
	public ConstraintType geq() {
		return GurobiConstraintType.GE;
	}
	
	@Override
	public Constraint linearConstraint(LinearTerm[] terms, ConstraintType leq,
			double rhs) {
		return new GurobiConstraint_c(terms, leq, rhs);
	}

    @Override
    public LinearTerm linearTerm(double weight, SolverVariable v) {
        return new LinearTerm_c(weight, v);
    }

    @Override
    public Problem minimizationProblem(LinearTerm[] objective,
            Constraint[] constraints) throws SolverException {
        
        try {
            model.set(GRB.IntAttr.ModelSense, GRB.MINIMIZE);
            model.update();

            // Set objective function coefficients for each variable
            for (LinearTerm term : objective) {
//                System.err.println("Setting coeff for " + term + " to " + term.coeff());
                GurobiVariable var = (GurobiVariable) term.var();
                var.setObjectiveCoeff(model, term.coeff());
                model.update();
            }
            
            //Add/update all variables to model
            model.update();
            int c_id = 0;
            for (Constraint c : constraints) {
                GurobiConstraint gc = (GurobiConstraint) c;
                model.addConstr(gc.expr(), gc.type().toGurobi(), gc.rhs(), "c"+c_id++);
            }
//            GRBLinExpr expr = (GRBLinExpr) model.getObjective();
//            StringBuilder sb = new StringBuilder();
//            try {
//        
//                for (int i = 0; i < expr.size(); i++) {
//                    sb.append(expr.getCoeff(i));
//                    sb.append('*');
//                    sb.append(expr.getVar(i).get(GRB.StringAttr.VarName));
//                    if (i+1 < expr.size())
//                        sb.append('+');
//                }
//            } catch (GRBException e) {
//                e.printStackTrace();
//            }
//            System.out.println("OBJECTIVE:"+ sb.toString());

            return new GurobiProblem(model, solveables);
        } catch (GRBException e) {
            int x =e.getErrorCode() ;
            String msg = e.getMessage();
            throw new SolverException("Gurobi exception:" + x +":"+ msg);
        }
    }

}
