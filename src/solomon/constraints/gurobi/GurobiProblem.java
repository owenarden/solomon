package solomon.constraints.gurobi;

import gurobi.GRBException;
import gurobi.GRBModel;
import gurobi.GRB;
import java.util.Map;

import solomon.constraints.Problem;
import solomon.constraints.Solveable;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import solomon.constraints.SolverVariable;

public class GurobiProblem implements Problem {
//    private static final String[] TOPICS = new String[] {"solver", "gurobi"};
	private final GRBModel model;
	private Map<SolverVariable, Solveable> vars;
	public GurobiProblem(GRBModel model, Map<SolverVariable, Solveable> vars) {
		this.vars = vars;
		this.model = model;
	}
	@Override
	public GurobiSolution solve(SolverUtil su) throws SolverException {
		try {	
		    model.optimize();
		    System.out.println("OBJECTIVE VALUE: " + model.get(GRB.DoubleAttr.ObjVal));
			GurobiSolution soln = new GurobiSolution(model, vars);
			return soln;
		} catch (GRBException e) {
            throw new SolverException(e);
        }
	}
}
