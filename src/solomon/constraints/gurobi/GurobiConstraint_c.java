package solomon.constraints.gurobi;

import gurobi.GRB;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import solomon.constraints.ConstraintType;
import solomon.constraints.LinearTerm;
import solomon.constraints.gurobi.GurobiSolverUtil.GurobiConstraintType;

public class GurobiConstraint_c implements GurobiConstraint {
    protected final GRBLinExpr expr;
    protected final GurobiConstraintType type;
    protected final double rhs;

    public GurobiConstraint_c(LinearTerm[] terms, ConstraintType type, double rhs) {
        this.type = (GurobiConstraintType) type;
        this.rhs = rhs;
        this.expr = new GRBLinExpr();
        for (LinearTerm term : terms) {
            GurobiVariable var = (GurobiVariable) term.var();
            expr.addTerm(term.coeff(), var.grbVar());
        }
    }
    
    @Override
    public GRBLinExpr expr() {
        return expr;
    }

    @Override
    public GurobiConstraintType type() {
        return type;
    }

    @Override
    public double rhs() {
        return rhs;
    }
    public String toString() {
        StringBuilder sb = new StringBuilder();
        try {
    
            for (int i = 0; i < expr.size(); i++) {
                sb.append(expr.getCoeff(i));
                sb.append('*');
                sb.append(expr.getVar(i).get(GRB.StringAttr.VarName));
                if (i+1 < expr.size())
                    sb.append('+');
            }
            sb.append(type.toString());
            sb.append(rhs);
        } catch (GRBException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

}
