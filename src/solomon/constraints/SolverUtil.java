package solomon.constraints;




public interface SolverUtil {

	SolverVariable addSolveable(Solveable s) throws SolverException;

	LinearTerm linearTerm(double weight, SolverVariable ev) throws SolverException;

	ConstraintType leq();
	ConstraintType eq();
	ConstraintType geq();
	
	Constraint linearConstraint(LinearTerm[] terms, ConstraintType leq,	double rhs) throws SolverException;	
	Problem minimizationProblem(LinearTerm[] objective,
			Constraint[] constraints) throws SolverException;
	
}
