package solomon.constraints;

public interface Constraint {
    ConstraintType type();
    double rhs();
}
