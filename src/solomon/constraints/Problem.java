package solomon.constraints;


public interface Problem {
	Solution solve(SolverUtil su) throws SolverException;
}
