package solomon.constraints;

public interface LinearTerm {
	double coeff();
	SolverVariable var();
}
