package solomon.constraints;

import java.util.HashSet;
import java.util.Set;

public abstract class AbstractSolveable implements Solveable {
    protected SolverVariable var;
    protected Set<Constraint> constraints;
    
    public AbstractSolveable() {
        constraints = new HashSet<Constraint>();
    }

    @Override
    public void assignVariable(SolverVariable v) {
        this.var = v;
    }
    
    @Override
    public SolverVariable solverVariable() {
        return this.var;
    }

    @Override
    public String toVariableName() {
        if (var != null)
            return var.toString();
        else
            return "(null)";
    }
    
    @Override
    public void addConstraint(Constraint c) {
        constraints.add(c);
    }
    
    @Override
    public Set<Constraint> constraints() {
        return constraints;
    }

}
