package solomon.constraints;

import java.util.Set;

public interface Solveable {
    /**
     * Associate the solverVariable v with this Sovleable.
     * @param v
     */
    void assignVariable(SolverVariable v);
    SolverVariable solverVariable();

    /**
     * Apply a solution value to the solverVariable associated with this Solveable.
     * @param v
     */
	void applySolution(SolverUtil su, double value) throws SolverException;
	boolean checkSolution(SolverUtil su);
    String toVariableName();	
    String toDot(boolean useVars);
    void addConstraint(Constraint c);
    Set<Constraint> constraints();
}
