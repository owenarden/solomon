package solomon.constraints.lpsolve;

import solomon.constraints.Constraint;
import solomon.constraints.SolverVariable;
import solomon.constraints.lpsolve.LPSolverUtil.LPConstraintType;

public interface LPConstraint extends Constraint {
    double[] coefficients();
    SolverVariable[] solverVariables();
    LPConstraintType type();
}
