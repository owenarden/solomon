package solomon.constraints.lpsolve;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import polyglot.main.Report;
import polyglot.util.InternalCompilerError;

import solomon.constraints.Constraint;
import solomon.constraints.ConstraintType;
import solomon.constraints.LinearTerm;
import solomon.constraints.LinearTerm_c;
import solomon.constraints.Problem;
import solomon.constraints.Solveable;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import solomon.constraints.SolverVariable;

import lpsolve.LpSolve;
import lpsolve.LpSolveException;

public class LPSolverUtil implements SolverUtil {
	private static final String[] TOPICS = new String[] {"solver", "lpsolve"};
	private int next_id = 1;
	//Map<Solveable,GurobiVariable> solverVariables = new HashMap<Solveable, GurobiVariable>(); 
	Map<SolverVariable,Solveable> solveables = new HashMap<SolverVariable,Solveable>();
	
	protected enum LPConstraintType implements ConstraintType {
		LE(LpSolve.LE), EQ(LpSolve.EQ), GE(LpSolve.GE);
		int id;
		private LPConstraintType(int id) {
			this.id = id;
		}
		public int toLP() {
			return id;
		}
		public String toString() {
		    if (this.id == LpSolve.LE)
		        return "<=";
		    if (this.id == LpSolve.EQ)
		        return "=";
		    if (this.id == LpSolve.GE)
		        return ">=";
		    else 
		        throw new InternalCompilerError("Invalid constraint type id " + id);
		}
	}
	
	public LPSolverUtil() {
	}

	@Override
	public LPVariable addSolveable(Solveable s) {
		LPVariable v = (LPVariable) s.solverVariable();
		if(v == null) {
			if(Report.should_report(TOPICS, 3))
				Report.report(3, "Creating new solverVariable for " + s + ": " + next_id);
			v = new LPVariable("X" + next_id, next_id++);
			s.assignVariable(v);
		}
        solveables.put(v, s);
		return v;
	}

	@Override
	public ConstraintType leq() {
		return LPConstraintType.LE;
	}
	@Override
	public ConstraintType eq() {
		return LPConstraintType.EQ;
	}
	@Override
	public ConstraintType geq() {
		return LPConstraintType.GE;
	}
	
	@Override
	public LPConstraint linearConstraint(LinearTerm[] terms, ConstraintType leq,
			double rhs) {
		return new LPConstraint_c(terms, leq, rhs);
	}

	@Override
	public LinearTerm linearTerm(double weight, SolverVariable v) {
		return new LinearTerm_c(weight, v);
	}

	@Override
	public Problem minimizationProblem(LinearTerm[] objective,
			Constraint[] constraints) throws SolverException {

//		if(!solverVariables.values().containsAll(solveables.keySet())
//				|| !solveables.keySet().containsAll(solverVariables.values())
//				|| !solveables.values().containsAll(solverVariables.keySet())
//				|| !solverVariables.keySet().containsAll(solveables.values()))
//				throw new Error("OH NO!");
		LpSolve lp;
		try {
			lp = LpSolve.makeLp(constraints.length, next_id);

			lp.setMinim();
			for(SolverVariable v : solveables.keySet()) {
			    LPVariable lpv = (LPVariable) v;
				lp.setBinary(lpv.id(), true); //XXX this will be false in a multiway partition.
				lp.setColName(lpv.id(), v.name());
			}
			
			double obj[] = new double[next_id];
			for (LinearTerm lt : objective) {
				LPVariable v = (LPVariable) lt.var();
				obj[v.id()] = lt.coeff();
			}
			// - turn row entry mode on
			// - set the objective function
			// - create the constraints
			// - turn row entry mode off
			lp.setAddRowmode(true);
			lp.setObjFn(obj);
			for (Constraint c : constraints) {		
			    LPConstraint lpc = (LPConstraint) c;
				double row[] = new double[next_id];
				SolverVariable[] vs = lpc.solverVariables();
				double cs[] = lpc.coefficients();
				for(int i = 0;i<vs.length;i++) {
					row[((LPVariable) vs[i]).id()] = cs[i];
				}
				int ctype = lpc.type().toLP();
				if(Report.should_report(TOPICS, 4)) {
					Report.report(4, "Constraint withs vars: " + Arrays.toString(vs));
					Report.report(4, "\t and coeffs: " + Arrays.toString(cs));
					Report.report(4, "Has row : " + Arrays.toString(row));
					Report.report(4, "and RHS" + lpc.rhs());
				}
				lp.addConstraint(row, ctype, lpc.rhs());
				//XXX: The below implementation is more efficient!
//				double[] sparserow = c.coefficients();
//				int id = 1;// must start w/ 1
//				int[] columns = new int[sparserow.length+1];
//				for (SolverVariable v : c.solverVariables())
//					columns[id++] = ((GurobiVariable) v).id();
////				int ctype = ((LPConstraintType) c.type()).toLP();
//				lp.addConstraintex(sparserow.length, sparserow, columns, ctype,
//						c.rhs());
			}
			lp.setAddRowmode(false);
			return new LPProblem(lp, solveables);
		} catch (LpSolveException e) {
			throw new SolverException(e);
		}

	}

}
