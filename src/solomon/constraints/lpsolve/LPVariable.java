package solomon.constraints.lpsolve;

import solomon.constraints.SolverVariable;

public class LPVariable implements SolverVariable {
	protected final int id;
	protected final String name;

	public LPVariable(String name, int id) {
		this.name = name;
		this.id = id;
	}
	
	public String name() {
		return name;
	}

	public int id() {
		return id;
	}
	
	public String toString() {
		return name;
	}
	
	public int hashCode() {
		return "GurobiVariable".hashCode() ^ id;
	}
	
	public boolean equals(Object o) {
		if(o instanceof LPVariable) {
			return ((LPVariable) o).id() == id;
		}
		return false;
	}
}	
