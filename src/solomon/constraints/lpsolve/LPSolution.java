package solomon.constraints.lpsolve;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import solomon.constraints.Solution;
import solomon.constraints.Solveable;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import solomon.constraints.SolverVariable;


public class LPSolution implements Solution {
	int status;
	double objective;
	double[] values;
	Map<SolverVariable, Solveable> solveables;
	public LPSolution(int status, double objective, double[] values, Map<SolverVariable, Solveable> vars) {
//		int i=0;
//		for(double d : values) {
//			System.out.println("idx: " + (i+1) +" val:" + values[i++]);
//		}
		this.status = status;
		this.objective = objective;
		this.values = values;
		this.solveables = vars;
	}
	
	@Override
	public int getValue(SolverVariable var) {
	    return (int) values[((LPVariable) var).id()];
		//return values[var.id()-1];
	}
	
	@Override
	public Solveable getSolveable(SolverVariable var) {
		return solveables.get(var); 
	}
	
	@Override
	public Set<SolverVariable> solverVariables() {
		return solveables.keySet();
	}
	
	@Override
	public Collection<Solveable> solveables() {
		return solveables.values();
	}


	@Override
	public double objective() {
		return objective;
	}
	
	@Override
	public boolean isValid() {
		return status == 0;
	}
	
	@Override
	public void applySolution(SolverUtil su) throws SolverException {
		for(Entry<SolverVariable, Solveable> entry : solveables.entrySet()) {
			Solveable s = entry.getValue();
			SolverVariable v = entry.getKey();
			s.applySolution(su, getValue(v));
		}
	}

}
