package solomon.constraints.lpsolve;

import solomon.constraints.ConstraintType;
import solomon.constraints.LinearTerm;
import solomon.constraints.SolverVariable;
import solomon.constraints.lpsolve.LPSolverUtil.LPConstraintType;


public class LPConstraint_c implements LPConstraint {
	protected LPConstraintType type;
	protected double[] coefficients;
	protected SolverVariable[] solverVariables;
	protected double rhs;
	public LPConstraint_c(LinearTerm[] terms, ConstraintType type, double rhs) {
		this.coefficients = new double[terms.length];
		this.solverVariables = new SolverVariable[terms.length];
		int idx=0;
		for(LinearTerm t : terms) {
			coefficients[idx] = t.coeff();
			solverVariables[idx] = t.var();
			idx++;
		}
		this.type = (LPConstraintType) type;
		this.rhs = rhs;
	}

	@Override
	public double[] coefficients() {
		return coefficients;
	}

	@Override
	public double rhs() {
		return rhs;
	}

	@Override
	public LPConstraintType type() {
		return type;
	}

	@Override
	public SolverVariable[] solverVariables() {
		return solverVariables;
	}

	@Override
    public String toString() {
	    StringBuilder sb = new StringBuilder();
	    for (int i = 0; i < coefficients.length; i++) {
	        sb.append(coefficients[i]);
	        sb.append('*');
	        sb.append(solverVariables[i]);
	        if (i+1 < coefficients.length)
	            sb.append('+');
	    }
	    sb.append(type.toString());
	    sb.append(rhs);
	    return sb.toString();
    }
}
