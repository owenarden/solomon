package solomon.constraints.lpsolve;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import polyglot.main.Report;

import solomon.constraints.Problem;
import solomon.constraints.Solveable;
import solomon.constraints.SolverException;
import solomon.constraints.SolverUtil;
import solomon.constraints.SolverVariable;

import lpsolve.LpSolve;
import lpsolve.LpSolveException;

public class LPProblem implements Problem {
	private static final String[] TOPICS = new String[] {"solver", "lpsolve"};
	private final LpSolve lp;
	private Map<SolverVariable, Solveable> vars;
	public LPProblem(LpSolve lp, Map<SolverVariable, Solveable> vars) {
		this.vars = vars;
		this.lp = lp;
	}
	@Override
	public LPSolution solve(SolverUtil su) throws SolverException {
		try {	
		    int fileno=0;
		    while (new File("lpsolve"+fileno+".txt").exists()) {
		        fileno++;
		    }
		    lp.setOutputfile("lpsolve"+fileno+".txt");

		    int ps = LpSolve.PRESOLVE_ROWS
		    | LpSolve.PRESOLVE_COLS 
		    | LpSolve.PRESOLVE_LINDEP 
		    | LpSolve.PRESOLVE_SOS
		    | LpSolve.PRESOLVE_KNAPSACK 
		    | LpSolve.PRESOLVE_PROBEFIX 
		    | LpSolve.PRESOLVE_PROBEREDUCE 
		    | LpSolve.PRESOLVE_ROWDOMINATE
		    | LpSolve.PRESOLVE_COLDOMINATE
		    | LpSolve.PRESOLVE_BOUNDS
		    | LpSolve.PRESOLVE_DUALS
		    | LpSolve.PRESOLVE_SENSDUALS;
		   		    
		    lp.setPresolve(ps, lp.getPresolveloops());
		    int ret = lp.solve();
		    if (ret != LpSolve.OPTIMAL)
	            throw new SolverException("Could not find optimal solution: return value: " + ret);
		    int Norig_columns, Norig_rows, i;           
		    Norig_columns = lp.getNorigColumns();
		    double[] var = new double[lp.getNorigColumns()+1];
		    Norig_rows = lp.getNorigRows();
		    for(i = 1; i <= Norig_columns; i++) {
		      var[i] = lp.getVarPrimalresult(Norig_rows + i);
		    }
		    
			if (Report.should_report(TOPICS, 4)) {
				Report.report(4, "solution vars : " + Arrays.toString(var));
			}
			LPSolution soln = new LPSolution(ret, lp.getObjective(), var, vars);
			// delete the problem and free memory
			lp.deleteLp();
			return soln;
		} catch (LpSolveException e) {
			throw new SolverException(e);
		}
	}
}
