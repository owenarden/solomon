package solomon;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import net.sourceforge.sizeof.SizeOf;

public class ProfileUtil {
	/*
		int           A 32-bit (4-byte) integer value
		
		short         A 16-bit (2-byte) integer value
		
		long          A 64-bit (8-byte) integer value
		
		byte          An 8-bit (1-byte) integer value
		
		float         A 32-bit (4-byte) floating-point value
		
		double        A 64-bit (8-byte) floating-point value
		
		char          A 16-bit character using the Unicode encoding scheme
		
		boolean       A true or false value
	 */
	public static long sizeof(int o) {
		return 4;
	}
	
	public static long sizeof(short o) {
		return 2;
	}
	
	public static long sizeof(long o) {
		return 8;
	}
	
	public static long sizeof(byte o) {
		return 1;
	}
	
	public static long sizeof(float o) {
		return 4;
	}
	
	public static long sizeof(double o) {
		return 8;
	}
	
	public static long sizeof(char o) {
		return 2;
	}
	
	public static long sizeof(boolean o) {
		return 1;
	}

	public static long sizeof(String o) {
		return o.length()*2;
	}
	
	public static long sizeof(Object o) {
		try {
			return SizeOf.deepSizeOf(o);
		} catch (IllegalStateException e) {
			//throw new Error(e);
			return 1000;
		}
	}
	
	public static long sizeof(Connection o) {
		return 313;	
	}
	
	public static long sizeof(PreparedStatement o) {
		return 414;	
	}
	
	public static long sizeof(ResultSet o) {
        if (o == null) return 4;
		try {
			int cur = o.getRow();
			o.last();
			int rows = o.getRow();
            if (cur == rows) return 32;//empty
            long sum = 0;
			for(int i=1; i <= o.getMetaData().getColumnCount();i++) {
                byte[] bytes = o.getBytes(i);
                if (bytes == null) 
                    sum += 4;
                else
				    sum += o.getBytes(i).length;
            }       
			if(cur > 0)
				o.relative(cur-rows);
			else
				o.beforeFirst();
			long size = sum * rows;
			return size;
		} catch (Exception e) {
			throw new Error(e);
		}
	}

	public static long sizeof(StringBuffer sb) {
       return sb.length()*2;
	}

	
	public static void writeDataOnExit(final String filename, final long[] counters) {
		System.err.println("Adding shutdown hook for : " + filename + "(" + counters.length + ")");
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					// Create file
					FileWriter fstream = new FileWriter(filename);
					BufferedWriter out = new BufferedWriter(fstream);
					for(int i = 0; i<counters.length; i++) {
						out.write(i+":"+counters[i]);
						out.newLine();
					}
					out.close();
				} catch (Exception e) {// Catch exception if any
					System.err.println("Error: " + e.getMessage());
				}
			}
		}));
	}
	public static long[] loadData(final File f) throws IOException {
		FileReader fstream = new FileReader(f);
		BufferedReader in = new BufferedReader(fstream);
		String line;
		Map<Integer,Integer> entries = new HashMap<Integer,Integer>();
		int max = 0;
		while((line = in.readLine()) != null) {
			String[] entry = line.split(":");
			Integer id = Integer.valueOf(entry[0]);
			entries.put(id, Integer.valueOf(entry[1]));
			max = (id > max) ? id : max; 
		}
		long[] data = new long[max+1];
		for(Integer id : entries.keySet()) {
			data[id] = entries.get(id);
		}
		in.close();
		return data;	
	}
}
