# Solomon #

Solomon is an automatic program partitioner for Java applications and the Pyxis runtime.

Checkout the [StatusQuo project page](http://db.csail.mit.edu/statusquo/) or contact Owen Arden (owen@cs.cornell.edu) for more details.