/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Home interface for RequiredFileType.
 * @lomboz generated
 */
public interface RequiredFileTypeHome
   extends javax.ejb.EJBHome
{
   public static final String COMP_NAME="java:comp/env/ejb/RequiredFileType";
   public static final String JNDI_NAME="RequiredFileTypeBean";

   public edu.cornell.csuglab.cms.base.RequiredFileType findByPrimaryKey(edu.cornell.csuglab.cms.base.RequiredFileTypePK pk)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findBySubmissionID(long submissionID)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

}
