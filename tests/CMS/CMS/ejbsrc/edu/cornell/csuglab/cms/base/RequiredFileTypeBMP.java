/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * BMP layer for RequiredFileType.
 * @lomboz generated
 */
public class RequiredFileTypeBMP
   extends edu.cornell.csuglab.cms.base.RequiredFileTypeBean
   implements javax.ejb.EntityBean
{

   public long getSubmissionID() 
   {
      return super.getSubmissionID();
   }

   public void setSubmissionID( long submissionID ) 
   {
      super.setSubmissionID(submissionID);
      makeDirty();
   }
   public java.lang.String getFileType() 
   {
      return super.getFileType();
   }

   public void setFileType( java.lang.String fileType ) 
   {
      super.setFileType(fileType);
      makeDirty();
   }

   public boolean isModified()
   {
      return dirty;
   }

   protected void makeDirty()
   {
      dirty = true;
   }

   protected void makeClean()
   {
      dirty = false;
   }

   private boolean dirty = false;

   public edu.cornell.csuglab.cms.base.RequiredFileTypeData getData()
   {
      edu.cornell.csuglab.cms.base.RequiredFileTypeData dataHolder = null;
      try
      {
         dataHolder = new edu.cornell.csuglab.cms.base.RequiredFileTypeData();

         dataHolder.setSubmissionID( getSubmissionID() );
         dataHolder.setFileType( getFileType() );

      }
      catch (RuntimeException e)
      {
         throw new javax.ejb.EJBException(e);
      }

      return dataHolder;
   }

   public edu.cornell.csuglab.cms.base.RequiredFileTypePK ejbCreate(long submissionID,java.lang.String fileType) throws javax.ejb.CreateException
   {
      super.ejbCreate(submissionID,fileType);

      return getDao().create((edu.cornell.csuglab.cms.base.RequiredFileTypeBean) this);
   }

   /**
    * Generated ejbPostCreate for corresponding ejbCreate method.
    *
    * @see #ejbCreate(long submissionID,java.lang.String fileType)
    */
   public void ejbPostCreate(long submissionID,java.lang.String fileType)
   {
   }

   public edu.cornell.csuglab.cms.base.RequiredFileTypePK ejbFindByPrimaryKey(edu.cornell.csuglab.cms.base.RequiredFileTypePK pk) throws javax.ejb.FinderException
   {
      super.ejbFindByPrimaryKey(pk);

      return getDao().findByPrimaryKey(pk);
   }

   public java.util.Collection ejbFindBySubmissionID(long submissionID) throws javax.ejb.FinderException
   {
      super.ejbFindBySubmissionID(submissionID);

      return getDao().findBySubmissionID(submissionID);
   }

   public void ejbLoad() 
   {
      getDao().load((edu.cornell.csuglab.cms.base.RequiredFileTypePK) ctx.getPrimaryKey(), this);
      makeClean();
   }

   public void ejbStore() 
   {
      if (isModified())
      {
         getDao().store((edu.cornell.csuglab.cms.base.RequiredFileTypeBean) this);
         makeClean();
      }
   }

   public void ejbActivate() 
   {
   }

   public void ejbPassivate() 
   {

   }

   private javax.ejb.EntityContext ctx = null;

   public void setEntityContext(javax.ejb.EntityContext ctx) 
   {
      this.ctx = ctx;
   }

   public void unsetEntityContext() 
   {
      this.ctx = null;
   }

   public void ejbRemove() throws javax.ejb.RemoveException
   {
      getDao().remove((edu.cornell.csuglab.cms.base.RequiredFileTypePK) ctx.getPrimaryKey());

   }

      private static edu.cornell.csuglab.cms.base.RequiredFileTypeDAO dao = null;

   protected static synchronized edu.cornell.csuglab.cms.base.RequiredFileTypeDAO getDao()
   {
      if (dao != null) {
         return dao;
      } else {

         dao = (edu.cornell.csuglab.cms.base.RequiredFileTypeDAO) new edu.cornell.csuglab.cms.base.dao.RequiredFileTypeDAOImpl();

         dao.init();
         return dao;
      }
   }

   /* Value Objects BEGIN */

/* Value Objects END */

}
