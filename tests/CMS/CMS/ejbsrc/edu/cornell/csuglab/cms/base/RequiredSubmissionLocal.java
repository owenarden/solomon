/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Local interface for RequiredSubmission.
 * @lomboz generated
 */
public interface RequiredSubmissionLocal
   extends javax.ejb.EJBLocalObject
{

   public long getAssignmentID(  ) ;

   public void setAssignmentID( long assignmentID ) ;

   public long getSubmissionID(  ) ;

   public void setSubmissionID( long submissionID ) ;

   public java.lang.String getSubmissionName(  ) ;

   public void setSubmissionName( java.lang.String submissionName ) ;

   public int getMaxSize(  ) ;

   public void setMaxSize( int maxSize ) ;

   public boolean getHidden(  ) ;

   public void setHidden( boolean hidden ) ;

   public java.util.Collection getRequiredFileTypes(  ) throws javax.ejb.EJBException;

   /**
    * Finds a RequiredFileTypeData file to match the given file type if one exists. Otherwise returns null.
    * @param fileType
    * @return 
    * @throws EJBException
    */
   public edu.cornell.csuglab.cms.base.RequiredFileTypeData matchFileType( java.lang.String fileType ) throws javax.ejb.EJBException;

   public int removeRequiredFileTypes(  ) throws javax.ejb.EJBException;

   public edu.cornell.csuglab.cms.base.RequiredSubmissionData getRequiredSubmissionData(  ) ;

   public edu.cornell.csuglab.cms.base.RequiredSubmissionPK ejbFindByPrimaryKey( edu.cornell.csuglab.cms.base.RequiredSubmissionPK pk ) throws javax.ejb.FinderException;

   public java.util.Collection ejbFindByAssignmentID( long assignmentID ) throws javax.ejb.FinderException;

   public java.util.Collection ejbFindByCourseID( long courseID ) throws javax.ejb.FinderException;

   public java.util.Collection ejbFindHiddenByAssignmentID( long assignmentID ) throws javax.ejb.FinderException;

}
