/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Data object for Semester.
 * @lomboz generated
 */
public class SemesterData
   extends java.lang.Object
   implements java.io.Serializable
{
   private long semesterID;
   private java.lang.String semesterName;
   private java.lang.String termCode;
   private boolean hidden;

  /* begin value object */

  /* end value object */

   public SemesterData()
   {
   }

   public SemesterData( long semesterID,java.lang.String semesterName,java.lang.String termCode,boolean hidden )
   {
      setSemesterID(semesterID);
      setSemesterName(semesterName);
      setTermCode(termCode);
      setHidden(hidden);
   }

   public SemesterData( SemesterData otherData )
   {
      setSemesterID(otherData.getSemesterID());
      setSemesterName(otherData.getSemesterName());
      setTermCode(otherData.getTermCode());
      setHidden(otherData.getHidden());

   }

   public edu.cornell.csuglab.cms.base.SemesterPK getPrimaryKey() {
     edu.cornell.csuglab.cms.base.SemesterPK pk = new edu.cornell.csuglab.cms.base.SemesterPK(this.getSemesterID());
     return pk;
   }

   public long getSemesterID()
   {
      return this.semesterID;
   }
   public void setSemesterID( long semesterID )
   {
      this.semesterID = semesterID;
   }

   public java.lang.String getSemesterName()
   {
      return this.semesterName;
   }
   public void setSemesterName( java.lang.String semesterName )
   {
      this.semesterName = semesterName;
   }

   public java.lang.String getTermCode()
   {
      return this.termCode;
   }
   public void setTermCode( java.lang.String termCode )
   {
      this.termCode = termCode;
   }

   public boolean getHidden()
   {
      return this.hidden;
   }
   public void setHidden( boolean hidden )
   {
      this.hidden = hidden;
   }

   public String toString()
   {
      StringBuffer str = new StringBuffer("{");

      str.append("semesterID=" + getSemesterID() + " " + "semesterName=" + getSemesterName() + " " + "termCode=" + getTermCode() + " " + "hidden=" + getHidden());
      str.append('}');

      return(str.toString());
   }

   public boolean equals( Object pOther )
   {
      if( pOther instanceof SemesterData )
      {
         SemesterData lTest = (SemesterData) pOther;
         boolean lEquals = true;

         lEquals = lEquals && this.semesterID == lTest.semesterID;
         if( this.semesterName == null )
         {
            lEquals = lEquals && ( lTest.semesterName == null );
         }
         else
         {
            lEquals = lEquals && this.semesterName.equals( lTest.semesterName );
         }
         if( this.termCode == null )
         {
            lEquals = lEquals && ( lTest.termCode == null );
         }
         else
         {
            lEquals = lEquals && this.termCode.equals( lTest.termCode );
         }
         lEquals = lEquals && this.hidden == lTest.hidden;

         return lEquals;
      }
      else
      {
         return false;
      }
   }

   public int hashCode()
   {
      int result = 17;

      result = 37*result + (int)(semesterID^(semesterID>>>32));

      result = 37*result + ((this.semesterName != null) ? this.semesterName.hashCode() : 0);

      result = 37*result + ((this.termCode != null) ? this.termCode.hashCode() : 0);

      result = 37*result + (hidden ? 0 : 1);

      return result;
   }

}
