/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Local interface for Comment.
 * @lomboz generated
 */
public interface CommentLocal
   extends javax.ejb.EJBLocalObject
{

   public long getCommentID(  ) ;

   public void setCommentID( long ID ) ;

   public long getGroupID(  ) ;

   public void setGroupID( long groupID ) ;

   public java.lang.String getComment(  ) ;

   public void setComment( java.lang.String comment ) ;

   public java.lang.String getNetID(  ) ;

   public void setNetID( java.lang.String netID ) ;

   public java.sql.Timestamp getDateEntered(  ) ;

   public void setDateEntered( java.sql.Timestamp dateEntered ) ;

   public boolean getHidden(  ) ;

   public void setHidden( boolean hidden ) ;

   public edu.cornell.csuglab.cms.base.CommentData getCommentData(  ) ;

}
