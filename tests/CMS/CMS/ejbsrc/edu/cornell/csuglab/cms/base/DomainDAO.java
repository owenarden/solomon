/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Data Access Object interface for Domain.
 * @lomboz generated
 */
public interface DomainDAO
{
    public void init();

    public void load(edu.cornell.csuglab.cms.base.DomainPK pk, edu.cornell.csuglab.cms.base.DomainBean ejb) throws javax.ejb.EJBException;
    public void store(edu.cornell.csuglab.cms.base.DomainBean ejb) throws javax.ejb.EJBException;
    public void remove(edu.cornell.csuglab.cms.base.DomainPK pk) throws javax.ejb.RemoveException, javax.ejb.EJBException;

    public edu.cornell.csuglab.cms.base.DomainPK create(edu.cornell.csuglab.cms.base.DomainBean ejb) throws javax.ejb.CreateException, javax.ejb.EJBException;

    public edu.cornell.csuglab.cms.base.DomainPK findByPrimaryKey(edu.cornell.csuglab.cms.base.DomainPK key) throws javax.ejb.FinderException;

    public java.util.Collection findAllDomains() throws javax.ejb.FinderException;

}
