/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * BMP layer for CategoryCol.
 * @lomboz generated
 */
public class CategoryColBMP
   extends edu.cornell.csuglab.cms.base.CategoryColBean
   implements javax.ejb.EntityBean
{

   public long getColID() 
   {
      return super.getColID();
   }

   public void setColID( long colID ) 
   {
      super.setColID(colID);
      makeDirty();
   }
   public java.lang.String getColName() 
   {
      return super.getColName();
   }

   public void setColName( java.lang.String colName ) 
   {
      super.setColName(colName);
      makeDirty();
   }
   public java.lang.String getColType() 
   {
      return super.getColType();
   }

   public void setColType( java.lang.String colType ) 
   {
      super.setColType(colType);
      makeDirty();
   }
   public long getCategoryID() 
   {
      return super.getCategoryID();
   }

   public void setCategoryID( long categoryID ) 
   {
      super.setCategoryID(categoryID);
      makeDirty();
   }
   public long getPosition() 
   {
      return super.getPosition();
   }

   public void setPosition( long position ) 
   {
      super.setPosition(position);
      makeDirty();
   }
   public boolean getHidden() 
   {
      return super.getHidden();
   }

   public void setHidden( boolean hidden ) 
   {
      super.setHidden(hidden);
      makeDirty();
   }
   public boolean getRemoved() 
   {
      return super.getRemoved();
   }

   public void setRemoved( boolean removed ) 
   {
      super.setRemoved(removed);
      makeDirty();
   }

   public boolean isModified()
   {
      return dirty;
   }

   protected void makeDirty()
   {
      dirty = true;
   }

   protected void makeClean()
   {
      dirty = false;
   }

   private boolean dirty = false;

   public edu.cornell.csuglab.cms.base.CategoryColData getData()
   {
      edu.cornell.csuglab.cms.base.CategoryColData dataHolder = null;
      try
      {
         dataHolder = new edu.cornell.csuglab.cms.base.CategoryColData();

         dataHolder.setColID( getColID() );
         dataHolder.setColName( getColName() );
         dataHolder.setColType( getColType() );
         dataHolder.setCategoryID( getCategoryID() );
         dataHolder.setPosition( getPosition() );
         dataHolder.setHidden( getHidden() );
         dataHolder.setRemoved( getRemoved() );

      }
      catch (RuntimeException e)
      {
         throw new javax.ejb.EJBException(e);
      }

      return dataHolder;
   }

   public edu.cornell.csuglab.cms.base.CategoryColPK ejbCreate(java.lang.String colName,java.lang.String colType,long categoryID,boolean hidden,boolean removed,long position) throws javax.ejb.CreateException
   {
      super.ejbCreate(colName,colType,categoryID,hidden,removed,position);

      return getDao().create((edu.cornell.csuglab.cms.base.CategoryColBean) this);
   }

   /**
    * Generated ejbPostCreate for corresponding ejbCreate method.
    *
    * @see #ejbCreate(java.lang.String colName,java.lang.String colType,long categoryID,boolean hidden,boolean removed,long position)
    */
   public void ejbPostCreate(java.lang.String colName,java.lang.String colType,long categoryID,boolean hidden,boolean removed,long position)
   {
   }

   public edu.cornell.csuglab.cms.base.CategoryColPK ejbFindByPrimaryKey(edu.cornell.csuglab.cms.base.CategoryColPK pk) throws javax.ejb.FinderException
   {
      super.ejbFindByPrimaryKey(pk);

      return getDao().findByPrimaryKey(pk);
   }

   public java.util.Collection ejbFindByCategoryID(long categoryID,boolean removed,boolean visible) throws javax.ejb.FinderException
   {
      super.ejbFindByCategoryID(categoryID,removed,visible);

      return getDao().findByCategoryID(categoryID,removed,visible);
   }

   public java.util.Collection ejbFindByCourseID(long courseID,edu.cornell.csuglab.cms.author.Principal p) throws javax.ejb.FinderException
   {
      super.ejbFindByCourseID(courseID,p);

      return getDao().findByCourseID(courseID,p);
   }

   public void ejbLoad() 
   {
      getDao().load((edu.cornell.csuglab.cms.base.CategoryColPK) ctx.getPrimaryKey(), this);
      makeClean();
   }

   public void ejbStore() 
   {
      if (isModified())
      {
         getDao().store((edu.cornell.csuglab.cms.base.CategoryColBean) this);
         makeClean();
      }
   }

   public void ejbActivate() 
   {
   }

   public void ejbPassivate() 
   {

   }

   private javax.ejb.EntityContext ctx = null;

   public void setEntityContext(javax.ejb.EntityContext ctx) 
   {
      this.ctx = ctx;
   }

   public void unsetEntityContext() 
   {
      this.ctx = null;
   }

   public void ejbRemove() throws javax.ejb.RemoveException
   {
      getDao().remove((edu.cornell.csuglab.cms.base.CategoryColPK) ctx.getPrimaryKey());

   }

      private static edu.cornell.csuglab.cms.base.CategoryColDAO dao = null;

   protected static synchronized edu.cornell.csuglab.cms.base.CategoryColDAO getDao()
   {
      if (dao != null) {
         return dao;
      } else {

         dao = (edu.cornell.csuglab.cms.base.CategoryColDAO) new edu.cornell.csuglab.cms.base.dao.CategoryColDAOImpl();

         dao.init();
         return dao;
      }
   }

   /* Value Objects BEGIN */

/* Value Objects END */

}
