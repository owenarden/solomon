/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Data object for Group.
 * @lomboz generated
 */
public class GroupData
   extends java.lang.Object
   implements java.io.Serializable
{
   private long assignmentID;
   private java.lang.Long timeSlotID;
   private long groupID;
   private java.sql.Timestamp latestSubmission;
   private java.sql.Timestamp extension;
   private int fileCounter;
   private java.lang.String adjustment;
   private int remainingSubmissions;

  /* begin value object */

  /* end value object */

   public GroupData()
   {
   }

   public GroupData( long assignmentID,java.lang.Long timeSlotID,long groupID,java.sql.Timestamp latestSubmission,java.sql.Timestamp extension,int fileCounter,java.lang.String adjustment,int remainingSubmissions )
   {
      setAssignmentID(assignmentID);
      setTimeSlotID(timeSlotID);
      setGroupID(groupID);
      setLatestSubmission(latestSubmission);
      setExtension(extension);
      setFileCounter(fileCounter);
      setAdjustment(adjustment);
      setRemainingSubmissions(remainingSubmissions);
   }

   public GroupData( GroupData otherData )
   {
      setAssignmentID(otherData.getAssignmentID());
      setTimeSlotID(otherData.getTimeSlotID());
      setGroupID(otherData.getGroupID());
      setLatestSubmission(otherData.getLatestSubmission());
      setExtension(otherData.getExtension());
      setFileCounter(otherData.getFileCounter());
      setAdjustment(otherData.getAdjustment());
      setRemainingSubmissions(otherData.getRemainingSubmissions());

   }

   public edu.cornell.csuglab.cms.base.GroupPK getPrimaryKey() {
     edu.cornell.csuglab.cms.base.GroupPK pk = new edu.cornell.csuglab.cms.base.GroupPK(this.getGroupID());
     return pk;
   }

   public long getAssignmentID()
   {
      return this.assignmentID;
   }
   public void setAssignmentID( long assignmentID )
   {
      this.assignmentID = assignmentID;
   }

   public java.lang.Long getTimeSlotID()
   {
      return this.timeSlotID;
   }
   public void setTimeSlotID( java.lang.Long timeSlotID )
   {
      this.timeSlotID = timeSlotID;
   }

   public long getGroupID()
   {
      return this.groupID;
   }
   public void setGroupID( long groupID )
   {
      this.groupID = groupID;
   }

   public java.sql.Timestamp getLatestSubmission()
   {
      return this.latestSubmission;
   }
   public void setLatestSubmission( java.sql.Timestamp latestSubmission )
   {
      this.latestSubmission = latestSubmission;
   }

   public java.sql.Timestamp getExtension()
   {
      return this.extension;
   }
   public void setExtension( java.sql.Timestamp extension )
   {
      this.extension = extension;
   }

   public int getFileCounter()
   {
      return this.fileCounter;
   }
   public void setFileCounter( int fileCounter )
   {
      this.fileCounter = fileCounter;
   }

   public java.lang.String getAdjustment()
   {
      return this.adjustment;
   }
   public void setAdjustment( java.lang.String adjustment )
   {
      this.adjustment = adjustment;
   }

   public int getRemainingSubmissions()
   {
      return this.remainingSubmissions;
   }
   public void setRemainingSubmissions( int remainingSubmissions )
   {
      this.remainingSubmissions = remainingSubmissions;
   }

   public String toString()
   {
      StringBuffer str = new StringBuffer("{");

      str.append("assignmentID=" + getAssignmentID() + " " + "timeSlotID=" + getTimeSlotID() + " " + "groupID=" + getGroupID() + " " + "latestSubmission=" + getLatestSubmission() + " " + "extension=" + getExtension() + " " + "fileCounter=" + getFileCounter() + " " + "adjustment=" + getAdjustment() + " " + "remainingSubmissions=" + getRemainingSubmissions());
      str.append('}');

      return(str.toString());
   }

   public boolean equals( Object pOther )
   {
      if( pOther instanceof GroupData )
      {
         GroupData lTest = (GroupData) pOther;
         boolean lEquals = true;

         lEquals = lEquals && this.assignmentID == lTest.assignmentID;
         if( this.timeSlotID == null )
         {
            lEquals = lEquals && ( lTest.timeSlotID == null );
         }
         else
         {
            lEquals = lEquals && this.timeSlotID.equals( lTest.timeSlotID );
         }
         lEquals = lEquals && this.groupID == lTest.groupID;
         if( this.latestSubmission == null )
         {
            lEquals = lEquals && ( lTest.latestSubmission == null );
         }
         else
         {
            lEquals = lEquals && this.latestSubmission.equals( lTest.latestSubmission );
         }
         if( this.extension == null )
         {
            lEquals = lEquals && ( lTest.extension == null );
         }
         else
         {
            lEquals = lEquals && this.extension.equals( lTest.extension );
         }
         lEquals = lEquals && this.fileCounter == lTest.fileCounter;
         if( this.adjustment == null )
         {
            lEquals = lEquals && ( lTest.adjustment == null );
         }
         else
         {
            lEquals = lEquals && this.adjustment.equals( lTest.adjustment );
         }
         lEquals = lEquals && this.remainingSubmissions == lTest.remainingSubmissions;

         return lEquals;
      }
      else
      {
         return false;
      }
   }

   public int hashCode()
   {
      int result = 17;

      result = 37*result + (int)(assignmentID^(assignmentID>>>32));

      result = 37*result + ((this.timeSlotID != null) ? this.timeSlotID.hashCode() : 0);

      result = 37*result + (int)(groupID^(groupID>>>32));

      result = 37*result + ((this.latestSubmission != null) ? this.latestSubmission.hashCode() : 0);

      result = 37*result + ((this.extension != null) ? this.extension.hashCode() : 0);

      result = 37*result + (int) fileCounter;

      result = 37*result + ((this.adjustment != null) ? this.adjustment.hashCode() : 0);

      result = 37*result + (int) remainingSubmissions;

      return result;
   }

}
