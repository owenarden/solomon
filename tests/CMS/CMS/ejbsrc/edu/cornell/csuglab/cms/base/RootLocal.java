/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Local interface for Root.
 * @lomboz generated
 */
public interface RootLocal
   extends javax.ejb.EJBLocalObject
{
   /**
    * When the system first starts up, this method gets called to check that SystemProperties settings exist, and also that at least one semester exists. If these things are missing, this method will create default entries.
    */
   public void ensureStartupSettings(  ) throws javax.ejb.FinderException;

   public boolean isDebugMode(  ) ;

   public java.util.Set getLateGroups( long assignmentID ) throws javax.ejb.FinderException;

   public int getMaxFileSize(  ) ;

   public int getCUIDCount(  ) ;

   /**
    * Return the current semester
    */
   public edu.cornell.csuglab.cms.base.SemesterPK findCurrentSemester(  ) throws javax.ejb.FinderException;

   /**
    * Set the current semester
    * @param semesterName The ID of the (existing) semester to switch to
    * @return Whether the operation was completed successfully
    */
   public boolean setCurrentSemester( long semesterID ) ;

   /**
    * Return URL from which to load each semester's server groups
    */
   public java.lang.String getHostGroupsURL(  ) throws javax.ejb.FinderException;

   /**
    * Set the URL from which to load the list of groups of servers for each semester
    * @param url the URL to load from
    * @return Whether the operation was completed successfully
    */
   public boolean setHostGroupsURL( java.lang.String url ) ;

   /**
    * Return JSON string of host, semesterID pairs
    * @param semesterID the semester to fetch
    */
   public java.lang.String getHostGroup( java.lang.Long semesterID ) throws javax.ejb.FinderException;

   /**
    * Return XML containing discovered foreign semesters
    */
   public org.w3c.dom.Document getForeignSemesters(  ) throws javax.ejb.FinderException;

   /**
    * Find a subset of the netids which are not students in the given course
    * @param netids A collection of NetIDs to check
    * @param courseID The CourseID of the course to check
    * @return A collection of NetIDs from the input collection which do not correspond to students in the course
    */
   public java.util.Collection getNonStudentNetIDs( java.util.Collection netids,long courseID ) ;

   /**
    * Find a subset of the netids which are active in groups in this assignment in which they are not the only member
    * @param netids A collection of netids to check
    * @param assignmentID The AssignmentID of the assignment to check
    * @return A collection of NetIDs from the input collection of students who are in a group with other students
    */
   public java.util.Collection getNonSoloGroupMembers( java.util.Collection netids,long assignmentID ) ;

   /**
    * Find that subset of the given netids that have received a grade for this assignment already
    * @param netids A collection of netids to check
    * @param assignmentID The AssignmentID of the assignment to check
    * @return A collection of NetIDs from the input collection of students who have a grade for this assignment
    */
   public java.util.Collection getGradedStudents( java.util.Collection netids,long assignmentID ) ;

   /**
    * Find that subset of the given groups that have received a grade for this assignment already
    * @param groupIDs A collection of group IDs (Longs) to check
    * @param asgnID The AssignmentID of the assignment to check
    * @return A collection of Longs, a subset of the input
    */
   public java.util.Collection getGradedGroups( java.util.Collection groupIDs,long asgnID ) ;

   /**
    * Find a collection of Assignments in which the given NetIDs have received a grade
    * @return A collection of AssignmentIDs (Longs)    */
   public java.util.Collection getGradedAssignments( java.util.Collection netids,long courseID ) ;

   public java.util.Map getAssignmentIDMap( long courseID ) ;

   public java.util.Map getSubmissionNameMap( long assignmentID ) ;

   public java.util.Map getSubmissionNameMapByCourse( long courseID ) ;

   public java.util.Map getSubProblemIDMap( long assignmentID ) ;

   public java.util.Map getCIDCourseCodeMap(  ) ;

   public java.util.Map getAssignmentNameMap(  ) ;

   public java.util.Map getAssignmentNameMap( long courseID ) ;

   public java.util.Map getCategoryIDMap( long courseID ) ;

   public java.util.Map getCourseCodeMap(  ) ;

   public java.util.Map getRemainingSubmissionMap( long assignmentID ) ;

   /**
    * Returns a mapping from GroupID (Long) -> Group (GroupBean) for all active groups in the assignment.
    * @param assignmentID
    */
   public java.util.Map getGroupsMap( long assignmentID ) ;

   /**
    * Returns the GroupIDs of the groups for which at least one problem has been assigned to the grader
    * @param graderNetID The NetID of the grader
    * @param groupIDs The GroupIDs of the groups to check
    * @return A subset of groupIDs which have been assigned
    */
   public java.util.Collection assignedToGroups( long assignID,java.lang.String graderNetID,java.util.Collection groupIDs ) throws javax.ejb.FinderException;

   public java.util.Map getNameMap( long courseID ) ;

   /**
    * Returns a mapping from NetID (String) -> First/Last Name (String[2]) which is valid for all students within the given course
    * @param courseID
    */
   public java.util.Map getFirstLastNameMap( long courseID ) ;

   public java.util.Map getCommentFileMap( long groupID ) ;

   /**
    * Checks the current semester to see if the given user is active in exactly one course (either as a staff or a student). Returns the CourseID if this is the case, null otherwise.
    * @return 
    */
   public java.lang.Long hasSoloCourse( java.lang.String netID ) ;

   /**
    * Checks the given semester to see if the given user is active in exactly one course (either as a staff or a student). Returns the CourseID if this is the case, null otherwise.
    * @param semesterID
    * @return 
    */
   public java.lang.Long hasSoloCourseBySemester( java.lang.String netID,long semesterID ) ;

   /**
    * Finds all Grading-related log details for active members of the given groups in the given course
    * @param courseID
    * @param groupids
    * @return 
    */
   public java.util.Collection findGradeLogDetails( long courseID,java.util.Collection groupids ) ;

   public java.util.Map getLastGradeMap( long assignmentID ) ;

   public java.util.Map getLastGradeMapByCourse( long courseID ) ;

   public java.util.Map getGradeMap( java.lang.String netID,long courseID ) ;

   public java.util.Map getSubProblemGradeMap( java.lang.String netID,long assignmentID ) ;

   public java.util.Map getSubProblemNameMap( long assignmentID ) ;

   public java.util.Map getSubProblemNameMapByCourse( long courseID ) ;

   public java.util.Map getGroupIDMap( long assignmentID ) ;

   public java.util.Map getGroupIDMap( java.lang.String netID ) ;

   public java.util.Map getGroupIDMapByCourse( long courseID ) ;

   public java.util.Map getGroupMemberListMap( long courseID ) ;

   /**
    * Returns a mapping from GroupID (Long) -> NetIDs (ArrayList) for every group in every assignment in the course.
    */
   public java.util.Map getGroupMembersMap( long courseID ) ;

   public java.util.Map getCommentFileRequestIDMap( long assignmentID ) ;

   public java.util.Map getStaffNameMap( long courseID ) ;

   public java.util.Map getStaffFirstLastNameMap( long courseID ) ;

   public java.util.Map getStaffNameMap( java.lang.String netID ) ;

   public java.util.Map getCommentFileGroupIDMap( long assignmentID ) ;

   public java.util.Map getCommentFileRequestIDMapByCourse( long courseID ) ;

   public java.util.Map getCommentFileGroupIDMapByCourse( long courseID ) ;

   /**
    * Creates a new category for specified course
    * @param templ - the metadata for the category and columns associated with it
    * @return 
    * @throws CreateException
    */
   public edu.cornell.csuglab.cms.base.CategoryLocal createCategory( edu.cornell.csuglab.cms.util.category.CategoryTemplate templ ) throws javax.ejb.CreateException;

   /**
    * Creates a new col for specified category
    * @param col- the metadata for the new column
    * @param categoryID - id of specified category
    * @return 
    * @throws CreateException
    */
   public edu.cornell.csuglab.cms.base.CategoryColLocal createCtgColumn( edu.cornell.csuglab.cms.util.category.CtgColInfo col,long categoryID ) throws javax.ejb.CreateException;

   /**
    * Creates a new row for specified category
    * @param row - the metadata associated with new row
    * @param categoryID - id of specified category
    * @return 
    * @throws CreateException
    */
   public edu.cornell.csuglab.cms.base.CategoryRowLocal createCtgRow( edu.cornell.csuglab.cms.util.category.CtgRowInfo row,long categoryID ) throws javax.ejb.CreateException;

   /**
    * Creates a new content bean for specified category. DOES NOT create supporting beans. Files should be created with createNEditCtgFiles() below.
    * @param content
    * @return CategoryContents (non-null)
    * @throws CreateException on ANY problem
    */
   public edu.cornell.csuglab.cms.base.CategoryContentsLocal createCtgContent( edu.cornell.csuglab.cms.util.category.CtgNewContentInfo content ) throws javax.ejb.CreateException;

   /**
    * Creates all the new files associated with the specified (existing, newly changed) content. Does the same thing as createNEditCtgFiles(CtgNewFileContentInfo, long), but with different input types.
    * @param content An info object for an existing cell with type file. This will contain an ordered list of fileInfos and String labels. When the info is non-null, a file will be created; when the info is null and the label is non-null, either a file will be added to the desired cell or the label on an existing file will be changed.
    * @param log The Log to which we should append relevant details
    * @return success
    * @throws CreateException
    */
   public boolean createNEditCtgFiles( edu.cornell.csuglab.cms.util.category.CtgCurFileContentInfo content,edu.cornell.csuglab.cms.base.LogData log ) throws javax.ejb.CreateException;

   /**
    * Creates all the new files associated with the specified (newly created) content. Does the same thing as createNEditCtgFiles(CtgCurFileContentInfo), but with different input types.
    * @param content An info object for a newly created cell with type file. This will contain an ordered list of fileInfos and String labels. When the info is non-null, a file will be created; when the info is null and the label is non-null, either a file will be added to the desired cell or the label on an existing file will be changed.
    * @param contentID The just-generated ID for this cell
    * @param log The Log to which we should append relevant details
    * @return success
    * @throws CreateException
    */
   public boolean createNEditCtgFiles( edu.cornell.csuglab.cms.util.category.CtgNewFileContentInfo content,long contentID,edu.cornell.csuglab.cms.base.LogData log ) throws javax.ejb.CreateException;

   /**
    * Returns true only if the given NetID is assigned to grade ALL the given groups in at least one subproblem
    * @param netID
    * @param groupIDs A Collection of Long objects
    * @return 
    */
   public boolean isAssignedTo( java.lang.String netID,java.util.Collection groupIDs ) ;

   /**
    * Checks to ensure that a collection of GroupIDs all refer to existing groups, and that all groups are within the same assignment. Returns the AssignmentID of that assignment as a Long if all groups exist and are in the same assignment, null otherwise. Returns null on empty collection.
    * @param groupIDs
    * @return 
    */
   public java.lang.Long isValidGroupCollection( java.util.Collection groupIDs ) ;

   /**
    * Returns whether or not a given netID is a valid user
    * @param netID NetID to check
    * @return Whether or not netID is a valid user
    */
   public boolean isUser( java.lang.String netID ) ;

   public edu.cornell.csuglab.cms.base.AnnouncementLocalHome announcementHome(  ) ;

   public edu.cornell.csuglab.cms.base.AnswerLocalHome answerHome(  ) ;

   public edu.cornell.csuglab.cms.base.AnswerSetLocalHome answerSetHome(  ) ;

   public edu.cornell.csuglab.cms.base.AssignmentLocalHome assignmentHome(  ) ;

   public edu.cornell.csuglab.cms.base.AssignmentFileLocalHome assignmentFileHome(  ) ;

   public edu.cornell.csuglab.cms.base.AssignmentItemLocalHome assignmentItemHome(  ) ;

   public edu.cornell.csuglab.cms.base.CategoryLocalHome categoryHome(  ) ;

   public edu.cornell.csuglab.cms.base.CategoryColLocalHome categoryColHome(  ) ;

   public edu.cornell.csuglab.cms.base.CategoryContentsLocalHome categoryContentsHome(  ) ;

   public edu.cornell.csuglab.cms.base.CategoryFileLocalHome categoryFileHome(  ) ;

   public edu.cornell.csuglab.cms.base.CategoryRowLocalHome categoryRowHome(  ) ;

   public edu.cornell.csuglab.cms.base.ChoiceLocalHome choiceHome(  ) ;

   public edu.cornell.csuglab.cms.base.CMSAdminLocalHome cmsAdminHome(  ) ;

   public edu.cornell.csuglab.cms.base.CommentLocalHome commentHome(  ) ;

   public edu.cornell.csuglab.cms.base.CommentFileLocalHome commentFileHome(  ) ;

   public edu.cornell.csuglab.cms.base.CourseLocalHome courseHome(  ) ;

   public edu.cornell.csuglab.cms.base.DomainLocalHome domainHome(  ) ;

   public edu.cornell.csuglab.cms.base.EmailLocalHome emailHome(  ) ;

   public edu.cornell.csuglab.cms.base.GradeLocalHome gradeHome(  ) ;

   public edu.cornell.csuglab.cms.base.GroupAssignedToLocalHome groupAssignedToHome(  ) ;

   public edu.cornell.csuglab.cms.base.GroupLocalHome groupHome(  ) ;

   public edu.cornell.csuglab.cms.base.GroupGradeLocalHome groupGradeHome(  ) ;

   public edu.cornell.csuglab.cms.base.GroupMemberLocalHome groupMemberHome(  ) ;

   public edu.cornell.csuglab.cms.base.LogLocalHome logHome(  ) ;

   public edu.cornell.csuglab.cms.base.OldAnnouncementLocalHome oldAnnouncementHome(  ) ;

   public edu.cornell.csuglab.cms.base.RegradeRequestLocalHome regradeRequestHome(  ) ;

   public edu.cornell.csuglab.cms.base.RequiredFileTypeLocalHome requiredFileTypeHome(  ) ;

   public edu.cornell.csuglab.cms.base.RequiredSubmissionLocalHome requiredSubmissionHome(  ) ;

   public edu.cornell.csuglab.cms.base.SiteNoticeLocalHome siteNoticeHome(  ) ;

   public edu.cornell.csuglab.cms.base.SemesterLocalHome semesterHome(  ) ;

   public edu.cornell.csuglab.cms.base.SolutionFileLocalHome solutionFileHome(  ) ;

   public edu.cornell.csuglab.cms.base.StaffLocalHome staffHome(  ) ;

   public edu.cornell.csuglab.cms.base.StudentLocalHome studentHome(  ) ;

   public edu.cornell.csuglab.cms.base.SubmittedFileLocalHome submittedFileHome(  ) ;

   public edu.cornell.csuglab.cms.base.SubProblemLocalHome subProblemHome(  ) ;

   public edu.cornell.csuglab.cms.base.TimeSlotLocalHome timeSlotHome(  ) ;

   public edu.cornell.csuglab.cms.base.UserLocalHome userHome(  ) ;

   public java.util.Collection getAllUsers(  ) ;

}
