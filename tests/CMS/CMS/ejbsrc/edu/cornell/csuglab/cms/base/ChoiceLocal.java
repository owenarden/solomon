/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Local interface for Choice.
 * @lomboz generated
 */
public interface ChoiceLocal
   extends javax.ejb.EJBLocalObject
{

   public long getChoiceID(  ) ;

   public void setChoiceID( long choiceID ) ;

   public long getSubProblemID(  ) ;

   public void setSubProblemID( long subProblemID ) ;

   public java.lang.String getLetter(  ) ;

   public void setLetter( java.lang.String letter ) ;

   public java.lang.String getText(  ) ;

   public void setText( java.lang.String text ) ;

   public boolean getHidden(  ) ;

   public void setHidden( boolean hidden ) ;

   public edu.cornell.csuglab.cms.base.ChoicePK ejbFindByChoiceID( long choiceID ) throws javax.ejb.FinderException;

   public edu.cornell.csuglab.cms.base.ChoicePK ejbFindByPrimaryKey( edu.cornell.csuglab.cms.base.ChoicePK pk ) throws javax.ejb.FinderException;

   public java.util.Collection ejbFindBySubProblemID( long subProblemID,boolean hidden ) throws javax.ejb.FinderException;

}
