/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Primary key for SubProblem.
 * @lomboz generated
 */
public class SubProblemPK
   extends java.lang.Object
   implements java.io.Serializable
{

   public long subProblemID;

   public SubProblemPK()
   {
   }

   public SubProblemPK( long subProblemID )
   {
      this.subProblemID = subProblemID;
   }

   public long getSubProblemID()
   {
      return subProblemID;
   }

   public void setSubProblemID(long subProblemID)
   {
      this.subProblemID = subProblemID;
   }

   public int hashCode()
   {
      int _hashCode = 0;
         _hashCode += (int)this.subProblemID;

      return _hashCode;
   }

   public boolean equals(Object obj)
   {
      if( !(obj instanceof edu.cornell.csuglab.cms.base.SubProblemPK) )
         return false;

      edu.cornell.csuglab.cms.base.SubProblemPK pk = (edu.cornell.csuglab.cms.base.SubProblemPK)obj;
      boolean eq = true;

      if( obj == null )
      {
         eq = false;
      }
      else
      {
         eq = eq && this.subProblemID == pk.subProblemID;
      }

      return eq;
   }

   /** @return String representation of this pk in the form of [.field1.field2.field3]. */
   public String toString()
   {
      StringBuffer toStringValue = new StringBuffer("[.");
         toStringValue.append(this.subProblemID).append('.');
      toStringValue.append(']');
      return toStringValue.toString();
   }

}
