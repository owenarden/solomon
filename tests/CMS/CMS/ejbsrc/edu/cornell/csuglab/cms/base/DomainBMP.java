/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * BMP layer for Domain.
 * @lomboz generated
 */
public class DomainBMP
   extends edu.cornell.csuglab.cms.base.DomainBean
   implements javax.ejb.EntityBean
{

   public long getDomainID() 
   {
      return super.getDomainID();
   }

   public void setDomainID( long domainID ) 
   {
      super.setDomainID(domainID);
      makeDirty();
   }
   public java.lang.String getDomainName() 
   {
      return super.getDomainName();
   }

   public void setDomainName( java.lang.String domainName ) 
   {
      super.setDomainName(domainName);
      makeDirty();
   }
   public java.lang.String getDomainPrefix() 
   {
      return super.getDomainPrefix();
   }

   public void setDomainPrefix( java.lang.String domainPrefix ) 
   {
      super.setDomainPrefix(domainPrefix);
      makeDirty();
   }

   public boolean isModified()
   {
      return dirty;
   }

   protected void makeDirty()
   {
      dirty = true;
   }

   protected void makeClean()
   {
      dirty = false;
   }

   private boolean dirty = false;

   public edu.cornell.csuglab.cms.base.DomainData getData()
   {
      edu.cornell.csuglab.cms.base.DomainData dataHolder = null;
      try
      {
         dataHolder = new edu.cornell.csuglab.cms.base.DomainData();

         dataHolder.setDomainID( getDomainID() );
         dataHolder.setDomainName( getDomainName() );
         dataHolder.setDomainPrefix( getDomainPrefix() );

      }
      catch (RuntimeException e)
      {
         throw new javax.ejb.EJBException(e);
      }

      return dataHolder;
   }

   public edu.cornell.csuglab.cms.base.DomainPK ejbCreate(java.lang.String domainName) throws javax.ejb.CreateException
   {
      super.ejbCreate(domainName);

      return getDao().create((edu.cornell.csuglab.cms.base.DomainBean) this);
   }

   /**
    * Generated ejbPostCreate for corresponding ejbCreate method.
    *
    * @see #ejbCreate(java.lang.String domainName)
    */
   public void ejbPostCreate(java.lang.String domainName)
   {
   }

   public edu.cornell.csuglab.cms.base.DomainPK ejbFindByPrimaryKey(edu.cornell.csuglab.cms.base.DomainPK key) throws javax.ejb.FinderException
   {
      super.ejbFindByPrimaryKey(key);

      return getDao().findByPrimaryKey(key);
   }

   public java.util.Collection ejbFindAllDomains() throws javax.ejb.FinderException
   {
      super.ejbFindAllDomains();

      return getDao().findAllDomains();
   }

   public void ejbLoad() 
   {
      getDao().load((edu.cornell.csuglab.cms.base.DomainPK) ctx.getPrimaryKey(), this);
      makeClean();
   }

   public void ejbStore() 
   {
      if (isModified())
      {
         getDao().store((edu.cornell.csuglab.cms.base.DomainBean) this);
         makeClean();
      }
   }

   public void ejbActivate() 
   {
   }

   public void ejbPassivate() 
   {

   }

   private javax.ejb.EntityContext ctx = null;

   public void setEntityContext(javax.ejb.EntityContext ctx) 
   {
      this.ctx = ctx;
   }

   public void unsetEntityContext() 
   {
      this.ctx = null;
   }

   public void ejbRemove() throws javax.ejb.RemoveException
   {
      getDao().remove((edu.cornell.csuglab.cms.base.DomainPK) ctx.getPrimaryKey());

   }

      private static edu.cornell.csuglab.cms.base.DomainDAO dao = null;

   protected static synchronized edu.cornell.csuglab.cms.base.DomainDAO getDao()
   {
      if (dao != null) {
         return dao;
      } else {

         dao = (edu.cornell.csuglab.cms.base.DomainDAO) new edu.cornell.csuglab.cms.base.dao.DomainDAOImpl();

         dao.init();
         return dao;
      }
   }

   /* Value Objects BEGIN */

/* Value Objects END */

}
