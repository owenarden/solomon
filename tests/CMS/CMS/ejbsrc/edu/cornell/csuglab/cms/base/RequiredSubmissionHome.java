/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Home interface for RequiredSubmission.
 * @lomboz generated
 */
public interface RequiredSubmissionHome
   extends javax.ejb.EJBHome
{
   public static final String COMP_NAME="java:comp/env/ejb/RequiredSubmission";
   public static final String JNDI_NAME="RequiredSubmissionBean";

   public edu.cornell.csuglab.cms.base.RequiredSubmission findByPrimaryKey(edu.cornell.csuglab.cms.base.RequiredSubmissionPK pk)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findByAssignmentID(long assignmentID)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findByCourseID(long courseID)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findHiddenByAssignmentID(long assignmentID)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findByGroupID(long groupID)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

}
