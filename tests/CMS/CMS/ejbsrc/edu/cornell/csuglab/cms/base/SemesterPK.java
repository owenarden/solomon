/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Primary key for Semester.
 * @lomboz generated
 */
public class SemesterPK
   extends java.lang.Object
   implements java.io.Serializable
{

   public long semesterID;

   public SemesterPK()
   {
   }

   public SemesterPK( long semesterID )
   {
      this.semesterID = semesterID;
   }

   public long getSemesterID()
   {
      return semesterID;
   }

   public void setSemesterID(long semesterID)
   {
      this.semesterID = semesterID;
   }

   public int hashCode()
   {
      int _hashCode = 0;
         _hashCode += (int)this.semesterID;

      return _hashCode;
   }

   public boolean equals(Object obj)
   {
      if( !(obj instanceof edu.cornell.csuglab.cms.base.SemesterPK) )
         return false;

      edu.cornell.csuglab.cms.base.SemesterPK pk = (edu.cornell.csuglab.cms.base.SemesterPK)obj;
      boolean eq = true;

      if( obj == null )
      {
         eq = false;
      }
      else
      {
         eq = eq && this.semesterID == pk.semesterID;
      }

      return eq;
   }

   /** @return String representation of this pk in the form of [.field1.field2.field3]. */
   public String toString()
   {
      StringBuffer toStringValue = new StringBuffer("[.");
         toStringValue.append(this.semesterID).append('.');
      toStringValue.append(']');
      return toStringValue.toString();
   }

}
