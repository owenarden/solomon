/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * BMP layer for Email.
 * @lomboz generated
 */
public class EmailBMP
   extends edu.cornell.csuglab.cms.base.EmailBean
   implements javax.ejb.EntityBean
{

   public long getEmailID() 
   {
      return super.getEmailID();
   }

   public void setEmailID( long emailID ) 
   {
      super.setEmailID(emailID);
      makeDirty();
   }
   public long getCourseID() 
   {
      return super.getCourseID();
   }

   public void setCourseID( long courseID ) 
   {
      super.setCourseID(courseID);
      makeDirty();
   }
   public java.sql.Timestamp getDateSent() 
   {
      return super.getDateSent();
   }

   public void setDateSent( java.sql.Timestamp dateSent ) 
   {
      super.setDateSent(dateSent);
      makeDirty();
   }
   public java.lang.String getMessage() 
   {
      return super.getMessage();
   }

   public void setMessage( java.lang.String message ) 
   {
      super.setMessage(message);
      makeDirty();
   }
   public int getRecipient() 
   {
      return super.getRecipient();
   }

   public void setRecipient( int recipient ) 
   {
      super.setRecipient(recipient);
      makeDirty();
   }
   public java.lang.String getSender() 
   {
      return super.getSender();
   }

   public void setSender( java.lang.String sender ) 
   {
      super.setSender(sender);
      makeDirty();
   }
   public java.lang.String getSubject() 
   {
      return super.getSubject();
   }

   public void setSubject( java.lang.String subject ) 
   {
      super.setSubject(subject);
      makeDirty();
   }

   public boolean isModified()
   {
      return dirty;
   }

   protected void makeDirty()
   {
      dirty = true;
   }

   protected void makeClean()
   {
      dirty = false;
   }

   private boolean dirty = false;

   public edu.cornell.csuglab.cms.base.EmailData getData()
   {
      edu.cornell.csuglab.cms.base.EmailData dataHolder = null;
      try
      {
         dataHolder = new edu.cornell.csuglab.cms.base.EmailData();

         dataHolder.setEmailID( getEmailID() );
         dataHolder.setCourseID( getCourseID() );
         dataHolder.setDateSent( getDateSent() );
         dataHolder.setMessage( getMessage() );
         dataHolder.setRecipient( getRecipient() );
         dataHolder.setSender( getSender() );
         dataHolder.setSubject( getSubject() );

      }
      catch (RuntimeException e)
      {
         throw new javax.ejb.EJBException(e);
      }

      return dataHolder;
   }

   public edu.cornell.csuglab.cms.base.EmailPK ejbCreate(long courseID,java.lang.String sender,java.lang.String subject,java.lang.String message,int recipient) throws javax.ejb.CreateException
   {
      super.ejbCreate(courseID,sender,subject,message,recipient);

      return getDao().create((edu.cornell.csuglab.cms.base.EmailBean) this);
   }

   /**
    * Generated ejbPostCreate for corresponding ejbCreate method.
    *
    * @see #ejbCreate(long courseID,java.lang.String sender,java.lang.String subject,java.lang.String message,int recipient)
    */
   public void ejbPostCreate(long courseID,java.lang.String sender,java.lang.String subject,java.lang.String message,int recipient)
   {
   }

   public edu.cornell.csuglab.cms.base.EmailPK ejbFindByPrimaryKey(edu.cornell.csuglab.cms.base.EmailPK key) throws javax.ejb.FinderException
   {
      super.ejbFindByPrimaryKey(key);

      return getDao().findByPrimaryKey(key);
   }

   public java.util.Collection ejbFindByCourseID(long courseID) throws javax.ejb.FinderException
   {
      super.ejbFindByCourseID(courseID);

      return getDao().findByCourseID(courseID);
   }

   public void ejbLoad() 
   {
      getDao().load((edu.cornell.csuglab.cms.base.EmailPK) ctx.getPrimaryKey(), this);
      makeClean();
   }

   public void ejbStore() 
   {
      if (isModified())
      {
         getDao().store((edu.cornell.csuglab.cms.base.EmailBean) this);
         makeClean();
      }
   }

   public void ejbActivate() 
   {
   }

   public void ejbPassivate() 
   {

   }

   private javax.ejb.EntityContext ctx = null;

   public void setEntityContext(javax.ejb.EntityContext ctx) 
   {
      this.ctx = ctx;
   }

   public void unsetEntityContext() 
   {
      this.ctx = null;
   }

   public void ejbRemove() throws javax.ejb.RemoveException
   {
      getDao().remove((edu.cornell.csuglab.cms.base.EmailPK) ctx.getPrimaryKey());

   }

      private static edu.cornell.csuglab.cms.base.EmailDAO dao = null;

   protected static synchronized edu.cornell.csuglab.cms.base.EmailDAO getDao()
   {
      if (dao != null) {
         return dao;
      } else {

         dao = (edu.cornell.csuglab.cms.base.EmailDAO) new edu.cornell.csuglab.cms.base.dao.EmailDAOImpl();

         dao.init();
         return dao;
      }
   }

   /* Value Objects BEGIN */

/* Value Objects END */

}
