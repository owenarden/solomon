/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Data object for Student.
 * @lomboz generated
 */
public class StudentData
   extends java.lang.Object
   implements java.io.Serializable
{
   private long courseID;
   private java.lang.String userID;
   private java.lang.String finalGrade;
   private java.lang.Float totalScore;
   private java.lang.String lecture;
   private java.lang.String lab;
   private java.lang.String section;
   private java.lang.String department;
   private java.lang.String courseNum;
   private java.lang.String credits;
   private java.lang.String gradeOption;
   private java.lang.String status;
   private boolean emailNewAssignment;
   private boolean emailDueDate;
   private boolean emailGroup;
   private boolean emailNewGrade;
   private boolean emailRegrade;
   private boolean emailFile;
   private boolean emailFinalGrade;
   private boolean emailTimeSlot;

  /* begin value object */

  /* end value object */

   public StudentData()
   {
   }

   public StudentData( long courseID,java.lang.String userID,java.lang.String finalGrade,java.lang.Float totalScore,java.lang.String lecture,java.lang.String lab,java.lang.String section,java.lang.String department,java.lang.String courseNum,java.lang.String credits,java.lang.String gradeOption,java.lang.String status,boolean emailNewAssignment,boolean emailDueDate,boolean emailGroup,boolean emailNewGrade,boolean emailRegrade,boolean emailFile,boolean emailFinalGrade,boolean emailTimeSlot )
   {
      setCourseID(courseID);
      setUserID(userID);
      setFinalGrade(finalGrade);
      setTotalScore(totalScore);
      setLecture(lecture);
      setLab(lab);
      setSection(section);
      setDepartment(department);
      setCourseNum(courseNum);
      setCredits(credits);
      setGradeOption(gradeOption);
      setStatus(status);
      setEmailNewAssignment(emailNewAssignment);
      setEmailDueDate(emailDueDate);
      setEmailGroup(emailGroup);
      setEmailNewGrade(emailNewGrade);
      setEmailRegrade(emailRegrade);
      setEmailFile(emailFile);
      setEmailFinalGrade(emailFinalGrade);
      setEmailTimeSlot(emailTimeSlot);
   }

   public StudentData( StudentData otherData )
   {
      setCourseID(otherData.getCourseID());
      setUserID(otherData.getUserID());
      setFinalGrade(otherData.getFinalGrade());
      setTotalScore(otherData.getTotalScore());
      setLecture(otherData.getLecture());
      setLab(otherData.getLab());
      setSection(otherData.getSection());
      setDepartment(otherData.getDepartment());
      setCourseNum(otherData.getCourseNum());
      setCredits(otherData.getCredits());
      setGradeOption(otherData.getGradeOption());
      setStatus(otherData.getStatus());
      setEmailNewAssignment(otherData.getEmailNewAssignment());
      setEmailDueDate(otherData.getEmailDueDate());
      setEmailGroup(otherData.getEmailGroup());
      setEmailNewGrade(otherData.getEmailNewGrade());
      setEmailRegrade(otherData.getEmailRegrade());
      setEmailFile(otherData.getEmailFile());
      setEmailFinalGrade(otherData.getEmailFinalGrade());
      setEmailTimeSlot(otherData.getEmailTimeSlot());

   }

   public edu.cornell.csuglab.cms.base.StudentPK getPrimaryKey() {
     edu.cornell.csuglab.cms.base.StudentPK pk = new edu.cornell.csuglab.cms.base.StudentPK(this.getCourseID(),this.getUserID());
     return pk;
   }

   public long getCourseID()
   {
      return this.courseID;
   }
   public void setCourseID( long courseID )
   {
      this.courseID = courseID;
   }

   public java.lang.String getUserID()
   {
      return this.userID;
   }
   public void setUserID( java.lang.String userID )
   {
      this.userID = userID;
   }

   public java.lang.String getFinalGrade()
   {
      return this.finalGrade;
   }
   public void setFinalGrade( java.lang.String finalGrade )
   {
      this.finalGrade = finalGrade;
   }

   public java.lang.Float getTotalScore()
   {
      return this.totalScore;
   }
   public void setTotalScore( java.lang.Float totalScore )
   {
      this.totalScore = totalScore;
   }

   public java.lang.String getLecture()
   {
      return this.lecture;
   }
   public void setLecture( java.lang.String lecture )
   {
      this.lecture = lecture;
   }

   public java.lang.String getLab()
   {
      return this.lab;
   }
   public void setLab( java.lang.String lab )
   {
      this.lab = lab;
   }

   public java.lang.String getSection()
   {
      return this.section;
   }
   public void setSection( java.lang.String section )
   {
      this.section = section;
   }

   public java.lang.String getDepartment()
   {
      return this.department;
   }
   public void setDepartment( java.lang.String department )
   {
      this.department = department;
   }

   public java.lang.String getCourseNum()
   {
      return this.courseNum;
   }
   public void setCourseNum( java.lang.String courseNum )
   {
      this.courseNum = courseNum;
   }

   public java.lang.String getCredits()
   {
      return this.credits;
   }
   public void setCredits( java.lang.String credits )
   {
      this.credits = credits;
   }

   public java.lang.String getGradeOption()
   {
      return this.gradeOption;
   }
   public void setGradeOption( java.lang.String gradeOption )
   {
      this.gradeOption = gradeOption;
   }

   public java.lang.String getStatus()
   {
      return this.status;
   }
   public void setStatus( java.lang.String status )
   {
      this.status = status;
   }

   public boolean getEmailNewAssignment()
   {
      return this.emailNewAssignment;
   }
   public void setEmailNewAssignment( boolean emailNewAssignment )
   {
      this.emailNewAssignment = emailNewAssignment;
   }

   public boolean getEmailDueDate()
   {
      return this.emailDueDate;
   }
   public void setEmailDueDate( boolean emailDueDate )
   {
      this.emailDueDate = emailDueDate;
   }

   public boolean getEmailGroup()
   {
      return this.emailGroup;
   }
   public void setEmailGroup( boolean emailGroup )
   {
      this.emailGroup = emailGroup;
   }

   public boolean getEmailNewGrade()
   {
      return this.emailNewGrade;
   }
   public void setEmailNewGrade( boolean emailNewGrade )
   {
      this.emailNewGrade = emailNewGrade;
   }

   public boolean getEmailRegrade()
   {
      return this.emailRegrade;
   }
   public void setEmailRegrade( boolean emailRegrade )
   {
      this.emailRegrade = emailRegrade;
   }

   public boolean getEmailFile()
   {
      return this.emailFile;
   }
   public void setEmailFile( boolean emailFile )
   {
      this.emailFile = emailFile;
   }

   public boolean getEmailFinalGrade()
   {
      return this.emailFinalGrade;
   }
   public void setEmailFinalGrade( boolean emailFinalGrade )
   {
      this.emailFinalGrade = emailFinalGrade;
   }

   public boolean getEmailTimeSlot()
   {
      return this.emailTimeSlot;
   }
   public void setEmailTimeSlot( boolean emailTimeSlot )
   {
      this.emailTimeSlot = emailTimeSlot;
   }

   public String toString()
   {
      StringBuffer str = new StringBuffer("{");

      str.append("courseID=" + getCourseID() + " " + "userID=" + getUserID() + " " + "finalGrade=" + getFinalGrade() + " " + "totalScore=" + getTotalScore() + " " + "lecture=" + getLecture() + " " + "lab=" + getLab() + " " + "section=" + getSection() + " " + "department=" + getDepartment() + " " + "courseNum=" + getCourseNum() + " " + "credits=" + getCredits() + " " + "gradeOption=" + getGradeOption() + " " + "status=" + getStatus() + " " + "emailNewAssignment=" + getEmailNewAssignment() + " " + "emailDueDate=" + getEmailDueDate() + " " + "emailGroup=" + getEmailGroup() + " " + "emailNewGrade=" + getEmailNewGrade() + " " + "emailRegrade=" + getEmailRegrade() + " " + "emailFile=" + getEmailFile() + " " + "emailFinalGrade=" + getEmailFinalGrade() + " " + "emailTimeSlot=" + getEmailTimeSlot());
      str.append('}');

      return(str.toString());
   }

   public boolean equals( Object pOther )
   {
      if( pOther instanceof StudentData )
      {
         StudentData lTest = (StudentData) pOther;
         boolean lEquals = true;

         lEquals = lEquals && this.courseID == lTest.courseID;
         if( this.userID == null )
         {
            lEquals = lEquals && ( lTest.userID == null );
         }
         else
         {
            lEquals = lEquals && this.userID.equals( lTest.userID );
         }
         if( this.finalGrade == null )
         {
            lEquals = lEquals && ( lTest.finalGrade == null );
         }
         else
         {
            lEquals = lEquals && this.finalGrade.equals( lTest.finalGrade );
         }
         if( this.totalScore == null )
         {
            lEquals = lEquals && ( lTest.totalScore == null );
         }
         else
         {
            lEquals = lEquals && this.totalScore.equals( lTest.totalScore );
         }
         if( this.lecture == null )
         {
            lEquals = lEquals && ( lTest.lecture == null );
         }
         else
         {
            lEquals = lEquals && this.lecture.equals( lTest.lecture );
         }
         if( this.lab == null )
         {
            lEquals = lEquals && ( lTest.lab == null );
         }
         else
         {
            lEquals = lEquals && this.lab.equals( lTest.lab );
         }
         if( this.section == null )
         {
            lEquals = lEquals && ( lTest.section == null );
         }
         else
         {
            lEquals = lEquals && this.section.equals( lTest.section );
         }
         if( this.department == null )
         {
            lEquals = lEquals && ( lTest.department == null );
         }
         else
         {
            lEquals = lEquals && this.department.equals( lTest.department );
         }
         if( this.courseNum == null )
         {
            lEquals = lEquals && ( lTest.courseNum == null );
         }
         else
         {
            lEquals = lEquals && this.courseNum.equals( lTest.courseNum );
         }
         if( this.credits == null )
         {
            lEquals = lEquals && ( lTest.credits == null );
         }
         else
         {
            lEquals = lEquals && this.credits.equals( lTest.credits );
         }
         if( this.gradeOption == null )
         {
            lEquals = lEquals && ( lTest.gradeOption == null );
         }
         else
         {
            lEquals = lEquals && this.gradeOption.equals( lTest.gradeOption );
         }
         if( this.status == null )
         {
            lEquals = lEquals && ( lTest.status == null );
         }
         else
         {
            lEquals = lEquals && this.status.equals( lTest.status );
         }
         lEquals = lEquals && this.emailNewAssignment == lTest.emailNewAssignment;
         lEquals = lEquals && this.emailDueDate == lTest.emailDueDate;
         lEquals = lEquals && this.emailGroup == lTest.emailGroup;
         lEquals = lEquals && this.emailNewGrade == lTest.emailNewGrade;
         lEquals = lEquals && this.emailRegrade == lTest.emailRegrade;
         lEquals = lEquals && this.emailFile == lTest.emailFile;
         lEquals = lEquals && this.emailFinalGrade == lTest.emailFinalGrade;
         lEquals = lEquals && this.emailTimeSlot == lTest.emailTimeSlot;

         return lEquals;
      }
      else
      {
         return false;
      }
   }

   public int hashCode()
   {
      int result = 17;

      result = 37*result + (int)(courseID^(courseID>>>32));

      result = 37*result + ((this.userID != null) ? this.userID.hashCode() : 0);

      result = 37*result + ((this.finalGrade != null) ? this.finalGrade.hashCode() : 0);

      result = 37*result + ((this.totalScore != null) ? this.totalScore.hashCode() : 0);

      result = 37*result + ((this.lecture != null) ? this.lecture.hashCode() : 0);

      result = 37*result + ((this.lab != null) ? this.lab.hashCode() : 0);

      result = 37*result + ((this.section != null) ? this.section.hashCode() : 0);

      result = 37*result + ((this.department != null) ? this.department.hashCode() : 0);

      result = 37*result + ((this.courseNum != null) ? this.courseNum.hashCode() : 0);

      result = 37*result + ((this.credits != null) ? this.credits.hashCode() : 0);

      result = 37*result + ((this.gradeOption != null) ? this.gradeOption.hashCode() : 0);

      result = 37*result + ((this.status != null) ? this.status.hashCode() : 0);

      result = 37*result + (emailNewAssignment ? 0 : 1);

      result = 37*result + (emailDueDate ? 0 : 1);

      result = 37*result + (emailGroup ? 0 : 1);

      result = 37*result + (emailNewGrade ? 0 : 1);

      result = 37*result + (emailRegrade ? 0 : 1);

      result = 37*result + (emailFile ? 0 : 1);

      result = 37*result + (emailFinalGrade ? 0 : 1);

      result = 37*result + (emailTimeSlot ? 0 : 1);

      return result;
   }

}
