/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Data object for TimeSlot.
 * @lomboz generated
 */
public class TimeSlotData
   extends java.lang.Object
   implements java.io.Serializable
{
   private long timeSlotID;
   private long assignmentID;
   private long courseID;
   private java.lang.String name;
   private java.lang.String location;
   private java.lang.String staff;
   private java.sql.Timestamp startTime;
   private boolean hidden;
   private int population;

  /* begin value object */

  /* end value object */

   public TimeSlotData()
   {
   }

   public TimeSlotData( long timeSlotID,long assignmentID,long courseID,java.lang.String name,java.lang.String location,java.lang.String staff,java.sql.Timestamp startTime,boolean hidden,int population )
   {
      setTimeSlotID(timeSlotID);
      setAssignmentID(assignmentID);
      setCourseID(courseID);
      setName(name);
      setLocation(location);
      setStaff(staff);
      setStartTime(startTime);
      setHidden(hidden);
      setPopulation(population);
   }

   public TimeSlotData( TimeSlotData otherData )
   {
      setTimeSlotID(otherData.getTimeSlotID());
      setAssignmentID(otherData.getAssignmentID());
      setCourseID(otherData.getCourseID());
      setName(otherData.getName());
      setLocation(otherData.getLocation());
      setStaff(otherData.getStaff());
      setStartTime(otherData.getStartTime());
      setHidden(otherData.getHidden());
      setPopulation(otherData.getPopulation());

   }

   public edu.cornell.csuglab.cms.base.TimeSlotPK getPrimaryKey() {
     edu.cornell.csuglab.cms.base.TimeSlotPK pk = new edu.cornell.csuglab.cms.base.TimeSlotPK(this.getTimeSlotID());
     return pk;
   }

   public long getTimeSlotID()
   {
      return this.timeSlotID;
   }
   public void setTimeSlotID( long timeSlotID )
   {
      this.timeSlotID = timeSlotID;
   }

   public long getAssignmentID()
   {
      return this.assignmentID;
   }
   public void setAssignmentID( long assignmentID )
   {
      this.assignmentID = assignmentID;
   }

   public long getCourseID()
   {
      return this.courseID;
   }
   public void setCourseID( long courseID )
   {
      this.courseID = courseID;
   }

   public java.lang.String getName()
   {
      return this.name;
   }
   public void setName( java.lang.String name )
   {
      this.name = name;
   }

   public java.lang.String getLocation()
   {
      return this.location;
   }
   public void setLocation( java.lang.String location )
   {
      this.location = location;
   }

   public java.lang.String getStaff()
   {
      return this.staff;
   }
   public void setStaff( java.lang.String staff )
   {
      this.staff = staff;
   }

   public java.sql.Timestamp getStartTime()
   {
      return this.startTime;
   }
   public void setStartTime( java.sql.Timestamp startTime )
   {
      this.startTime = startTime;
   }

   public boolean getHidden()
   {
      return this.hidden;
   }
   public void setHidden( boolean hidden )
   {
      this.hidden = hidden;
   }

   public int getPopulation()
   {
      return this.population;
   }
   public void setPopulation( int population )
   {
      this.population = population;
   }

   public String toString()
   {
      StringBuffer str = new StringBuffer("{");

      str.append("timeSlotID=" + getTimeSlotID() + " " + "assignmentID=" + getAssignmentID() + " " + "courseID=" + getCourseID() + " " + "name=" + getName() + " " + "location=" + getLocation() + " " + "staff=" + getStaff() + " " + "startTime=" + getStartTime() + " " + "hidden=" + getHidden() + " " + "population=" + getPopulation());
      str.append('}');

      return(str.toString());
   }

   public boolean equals( Object pOther )
   {
      if( pOther instanceof TimeSlotData )
      {
         TimeSlotData lTest = (TimeSlotData) pOther;
         boolean lEquals = true;

         lEquals = lEquals && this.timeSlotID == lTest.timeSlotID;
         lEquals = lEquals && this.assignmentID == lTest.assignmentID;
         lEquals = lEquals && this.courseID == lTest.courseID;
         if( this.name == null )
         {
            lEquals = lEquals && ( lTest.name == null );
         }
         else
         {
            lEquals = lEquals && this.name.equals( lTest.name );
         }
         if( this.location == null )
         {
            lEquals = lEquals && ( lTest.location == null );
         }
         else
         {
            lEquals = lEquals && this.location.equals( lTest.location );
         }
         if( this.staff == null )
         {
            lEquals = lEquals && ( lTest.staff == null );
         }
         else
         {
            lEquals = lEquals && this.staff.equals( lTest.staff );
         }
         if( this.startTime == null )
         {
            lEquals = lEquals && ( lTest.startTime == null );
         }
         else
         {
            lEquals = lEquals && this.startTime.equals( lTest.startTime );
         }
         lEquals = lEquals && this.hidden == lTest.hidden;
         lEquals = lEquals && this.population == lTest.population;

         return lEquals;
      }
      else
      {
         return false;
      }
   }

   public int hashCode()
   {
      int result = 17;

      result = 37*result + (int)(timeSlotID^(timeSlotID>>>32));

      result = 37*result + (int)(assignmentID^(assignmentID>>>32));

      result = 37*result + (int)(courseID^(courseID>>>32));

      result = 37*result + ((this.name != null) ? this.name.hashCode() : 0);

      result = 37*result + ((this.location != null) ? this.location.hashCode() : 0);

      result = 37*result + ((this.staff != null) ? this.staff.hashCode() : 0);

      result = 37*result + ((this.startTime != null) ? this.startTime.hashCode() : 0);

      result = 37*result + (hidden ? 0 : 1);

      result = 37*result + (int) population;

      return result;
   }

}
