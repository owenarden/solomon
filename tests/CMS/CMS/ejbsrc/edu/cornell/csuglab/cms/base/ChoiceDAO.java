/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Data Access Object interface for Choice.
 * @lomboz generated
 */
public interface ChoiceDAO
{
    public void init();

    public void load(edu.cornell.csuglab.cms.base.ChoicePK pk, edu.cornell.csuglab.cms.base.ChoiceBean ejb) throws javax.ejb.EJBException;
    public void store(edu.cornell.csuglab.cms.base.ChoiceBean ejb) throws javax.ejb.EJBException;
    public void remove(edu.cornell.csuglab.cms.base.ChoicePK pk) throws javax.ejb.RemoveException, javax.ejb.EJBException;

    public edu.cornell.csuglab.cms.base.ChoicePK create(edu.cornell.csuglab.cms.base.ChoiceBean ejb) throws javax.ejb.CreateException, javax.ejb.EJBException;

    public edu.cornell.csuglab.cms.base.ChoicePK findByChoiceID(long choiceID) throws javax.ejb.FinderException;

    public edu.cornell.csuglab.cms.base.ChoicePK findByPrimaryKey(edu.cornell.csuglab.cms.base.ChoicePK pk) throws javax.ejb.FinderException;

    public java.util.Collection findBySubProblemID(long subProblemID,boolean hidden) throws javax.ejb.FinderException;

}
