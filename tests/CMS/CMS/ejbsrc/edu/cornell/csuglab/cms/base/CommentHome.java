/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Home interface for Comment.
 * @lomboz generated
 */
public interface CommentHome
   extends javax.ejb.EJBHome
{
   public static final String COMP_NAME="java:comp/env/ejb/Comment";
   public static final String JNDI_NAME="CommentBean";

   public edu.cornell.csuglab.cms.base.Comment findByPrimaryKey(edu.cornell.csuglab.cms.base.CommentPK key)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findByGroupID(long groupID)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findByGroupIDs(java.util.Collection groupIDs)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

}
