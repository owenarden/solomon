/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Primary key for OldAnnouncement.
 * @lomboz generated
 */
public class OldAnnouncementPK
   extends java.lang.Object
   implements java.io.Serializable
{

   public long oldAnnouncementID;

   public OldAnnouncementPK()
   {
   }

   public OldAnnouncementPK( long oldAnnouncementID )
   {
      this.oldAnnouncementID = oldAnnouncementID;
   }

   public long getOldAnnouncementID()
   {
      return oldAnnouncementID;
   }

   public void setOldAnnouncementID(long oldAnnouncementID)
   {
      this.oldAnnouncementID = oldAnnouncementID;
   }

   public int hashCode()
   {
      int _hashCode = 0;
         _hashCode += (int)this.oldAnnouncementID;

      return _hashCode;
   }

   public boolean equals(Object obj)
   {
      if( !(obj instanceof edu.cornell.csuglab.cms.base.OldAnnouncementPK) )
         return false;

      edu.cornell.csuglab.cms.base.OldAnnouncementPK pk = (edu.cornell.csuglab.cms.base.OldAnnouncementPK)obj;
      boolean eq = true;

      if( obj == null )
      {
         eq = false;
      }
      else
      {
         eq = eq && this.oldAnnouncementID == pk.oldAnnouncementID;
      }

      return eq;
   }

   /** @return String representation of this pk in the form of [.field1.field2.field3]. */
   public String toString()
   {
      StringBuffer toStringValue = new StringBuffer("[.");
         toStringValue.append(this.oldAnnouncementID).append('.');
      toStringValue.append(']');
      return toStringValue.toString();
   }

}
