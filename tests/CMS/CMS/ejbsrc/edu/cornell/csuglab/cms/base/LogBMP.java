/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * BMP layer for Log.
 * @see LogDetail
 * @see LogSearchParams
 * @lomboz generated
 */
public class LogBMP
   extends edu.cornell.csuglab.cms.base.LogBean
   implements javax.ejb.EntityBean
{

   public long getLogID() 
   {
      return super.getLogID();
   }

   public void setLogID( long logID ) 
   {
      super.setLogID(logID);
      makeDirty();
   }
   public java.lang.String getActingNetID() 
   {
      return super.getActingNetID();
   }

   public void setActingNetID( java.lang.String actingNetID ) 
   {
      super.setActingNetID(actingNetID);
      makeDirty();
   }
   public java.lang.String getSimulatedNetID() 
   {
      return super.getSimulatedNetID();
   }

   public void setSimulatedNetID( java.lang.String simulatedNetID ) 
   {
      super.setSimulatedNetID(simulatedNetID);
      makeDirty();
   }
   public java.util.SortedSet getReceivingNetIDs() 
   {
      return super.getReceivingNetIDs();
   }

   public java.net.InetAddress getActingIPAddress() 
   {
      return super.getActingIPAddress();
   }

   public void setActingIPAddress( java.net.InetAddress actingIPAddress ) 
   {
      super.setActingIPAddress(actingIPAddress);
      makeDirty();
   }
   public java.sql.Timestamp getTimestamp() 
   {
      return super.getTimestamp();
   }

   public void setTimestamp( java.sql.Timestamp timestamp ) 
   {
      super.setTimestamp(timestamp);
      makeDirty();
   }
   public java.lang.String getLogName() 
   {
      return super.getLogName();
   }

   public void setLogName( java.lang.String logName ) 
   {
      super.setLogName(logName);
      makeDirty();
   }
   public long getLogType() 
   {
      return super.getLogType();
   }

   public void setLogType( long logType ) 
   {
      super.setLogType(logType);
      makeDirty();
   }
   public java.lang.Long getCourseID() 
   {
      return super.getCourseID();
   }

   public void setCourseID( java.lang.Long courseID ) 
   {
      super.setCourseID(courseID);
      makeDirty();
   }
   public java.util.SortedSet getAssignmentIDs() 
   {
      return super.getAssignmentIDs();
   }

   public java.util.Collection getDetailLogs() 
   {
      return super.getDetailLogs();
   }

   public void setDetailLogs( java.util.Collection detailLogs ) 
   {
      super.setDetailLogs(detailLogs);
      makeDirty();
   }

   public boolean isModified()
   {
      return dirty;
   }

   protected void makeDirty()
   {
      dirty = true;
   }

   protected void makeClean()
   {
      dirty = false;
   }

   private boolean dirty = false;

   public edu.cornell.csuglab.cms.base.LogData getData()
   {
      edu.cornell.csuglab.cms.base.LogData dataHolder = null;
      try
      {
         dataHolder = new edu.cornell.csuglab.cms.base.LogData();

         dataHolder.setLogID( getLogID() );
         dataHolder.setActingNetID( getActingNetID() );
         dataHolder.setSimulatedNetID( getSimulatedNetID() );
         dataHolder.setReceivingNetIDs( getReceivingNetIDs() );
         dataHolder.setActingIPAddress( getActingIPAddress() );
         dataHolder.setTimestamp( getTimestamp() );
         dataHolder.setLogName( getLogName() );
         dataHolder.setLogType( getLogType() );
         dataHolder.setCourseID( getCourseID() );
         dataHolder.setAssignmentIDs( getAssignmentIDs() );
         dataHolder.setDetailLogs( getDetailLogs() );

      }
      catch (RuntimeException e)
      {
         throw new javax.ejb.EJBException(e);
      }

      return dataHolder;
   }

   public edu.cornell.csuglab.cms.base.LogPK ejbCreate(edu.cornell.csuglab.cms.base.LogData log) throws javax.ejb.CreateException
   {
      super.ejbCreate(log);

      return getDao().create((edu.cornell.csuglab.cms.base.LogBean) this);
   }

   /**
    * Generated ejbPostCreate for corresponding ejbCreate method.
    *
    * @see #ejbCreate(edu.cornell.csuglab.cms.base.LogData log)
    */
   public void ejbPostCreate(edu.cornell.csuglab.cms.base.LogData log)
   {
   }

   public edu.cornell.csuglab.cms.base.LogPK ejbFindByPrimaryKey(edu.cornell.csuglab.cms.base.LogPK key) throws javax.ejb.FinderException
   {
      super.ejbFindByPrimaryKey(key);

      return getDao().findByPrimaryKey(key);
   }

   public java.util.Collection ejbFind(edu.cornell.csuglab.cms.log.LogSearchParams params) throws javax.ejb.FinderException
   {
      super.ejbFind(params);

      return getDao().find(params);
   }

   public java.util.Collection ejbFindCommentsNRegrades(java.lang.String studentNetID,long assignmentID) throws javax.ejb.FinderException
   {
      super.ejbFindCommentsNRegrades(studentNetID,assignmentID);

      return getDao().findCommentsNRegrades(studentNetID,assignmentID);
   }

   public void ejbLoad() 
   {
      getDao().load((edu.cornell.csuglab.cms.base.LogPK) ctx.getPrimaryKey(), this);
      makeClean();
   }

   public void ejbStore() 
   {
      if (isModified())
      {
         getDao().store((edu.cornell.csuglab.cms.base.LogBean) this);
         makeClean();
      }
   }

   public void ejbActivate() 
   {
   }

   public void ejbPassivate() 
   {

   }

   private javax.ejb.EntityContext ctx = null;

   public void setEntityContext(javax.ejb.EntityContext ctx) 
   {
      this.ctx = ctx;
   }

   public void unsetEntityContext() 
   {
      this.ctx = null;
   }

   public void ejbRemove() throws javax.ejb.RemoveException
   {
      getDao().remove((edu.cornell.csuglab.cms.base.LogPK) ctx.getPrimaryKey());

   }

      private static edu.cornell.csuglab.cms.base.LogDAO dao = null;

   protected static synchronized edu.cornell.csuglab.cms.base.LogDAO getDao()
   {
      if (dao != null) {
         return dao;
      } else {

         dao = (edu.cornell.csuglab.cms.base.LogDAO) new edu.cornell.csuglab.cms.base.dao.LogDAOImpl();

         dao.init();
         return dao;
      }
   }

   /* Value Objects BEGIN */

/* Value Objects END */

}
