/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Local home interface for SolutionFile.
 * @generated 
 * @lomboz generated
 */
public interface SolutionFileLocalHome
   extends javax.ejb.EJBLocalHome
{
   public static final String COMP_NAME="java:comp/env/ejb/SolutionFileLocal";
   public static final String JNDI_NAME="SolutionFileLocal";

   public edu.cornell.csuglab.cms.base.SolutionFileLocal create(long assignmentID , java.lang.String fileName , boolean hidden , java.lang.String path)
      throws javax.ejb.CreateException;

   public edu.cornell.csuglab.cms.base.SolutionFileLocal findByPrimaryKey(edu.cornell.csuglab.cms.base.SolutionFilePK pk)
      throws javax.ejb.FinderException;

   public edu.cornell.csuglab.cms.base.SolutionFileLocal findByAssignmentID(long assignmentID)
      throws javax.ejb.FinderException;

   public java.util.Collection findHiddenByAssignmentID(long assignmentID)
      throws javax.ejb.FinderException;

}
