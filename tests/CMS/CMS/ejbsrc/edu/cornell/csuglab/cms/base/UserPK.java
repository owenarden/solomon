/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Primary key for User.
 * @lomboz generated
 */
public class UserPK
   extends java.lang.Object
   implements java.io.Serializable
{

   public java.lang.String userID;

   public UserPK()
   {
   }

   public UserPK( java.lang.String userID )
   {
      this.userID = userID;
   }

   public java.lang.String getUserID()
   {
      return userID;
   }

   public void setUserID(java.lang.String userID)
   {
      this.userID = userID;
   }

   public int hashCode()
   {
      int _hashCode = 0;
         if (this.userID != null) _hashCode += this.userID.hashCode();

      return _hashCode;
   }

   public boolean equals(Object obj)
   {
      if( !(obj instanceof edu.cornell.csuglab.cms.base.UserPK) )
         return false;

      edu.cornell.csuglab.cms.base.UserPK pk = (edu.cornell.csuglab.cms.base.UserPK)obj;
      boolean eq = true;

      if( obj == null )
      {
         eq = false;
      }
      else
      {
         if( this.userID != null )
         {
            eq = eq && this.userID.equals( pk.getUserID() );
         }
         else  // this.userID == null
         {
            eq = eq && ( pk.getUserID() == null );
         }
      }

      return eq;
   }

   /** @return String representation of this pk in the form of [.field1.field2.field3]. */
   public String toString()
   {
      StringBuffer toStringValue = new StringBuffer("[.");
         toStringValue.append(this.userID).append('.');
      toStringValue.append(']');
      return toStringValue.toString();
   }

}
