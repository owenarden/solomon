/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * BMP layer for Group.
 * @lomboz generated
 */
public class GroupBMP
   extends edu.cornell.csuglab.cms.base.GroupBean
   implements javax.ejb.EntityBean
{

   public long getAssignmentID() 
   {
      return super.getAssignmentID();
   }

   public void setAssignmentID( long assignmentID ) 
   {
      super.setAssignmentID(assignmentID);
      makeDirty();
   }
   public java.lang.Long getTimeSlotID() 
   {
      return super.getTimeSlotID();
   }

   public void setTimeSlotID( java.lang.Long timeSlotID ) 
   {
      super.setTimeSlotID(timeSlotID);
      makeDirty();
   }
   public long getGroupID() 
   {
      return super.getGroupID();
   }

   public void setGroupID( long groupID ) 
   {
      super.setGroupID(groupID);
      makeDirty();
   }
   public java.sql.Timestamp getLatestSubmission() 
   {
      return super.getLatestSubmission();
   }

   public void setLatestSubmission( java.sql.Timestamp latestSubmission ) 
   {
      super.setLatestSubmission(latestSubmission);
      makeDirty();
   }
   public java.sql.Timestamp getExtension() 
   {
      return super.getExtension();
   }

   public void setExtension( java.sql.Timestamp extension ) 
   {
      super.setExtension(extension);
      makeDirty();
   }
   public int getFileCounter() 
   {
      return super.getFileCounter();
   }

   public void setFileCounter( int fileCounter ) 
   {
      super.setFileCounter(fileCounter);
      makeDirty();
   }
   public java.lang.String getAdjustment() 
   {
      return super.getAdjustment();
   }

   public void setAdjustment( java.lang.String adjustment ) 
   {
      super.setAdjustment(adjustment);
      makeDirty();
   }
   public int getRemainingSubmissions() 
   {
      return super.getRemainingSubmissions();
   }

   public void setRemainingSubmissions( int remainingSubmissions ) 
   {
      super.setRemainingSubmissions(remainingSubmissions);
      makeDirty();
   }

   public boolean isModified()
   {
      return dirty;
   }

   protected void makeDirty()
   {
      dirty = true;
   }

   protected void makeClean()
   {
      dirty = false;
   }

   private boolean dirty = false;

   public edu.cornell.csuglab.cms.base.GroupData getData()
   {
      edu.cornell.csuglab.cms.base.GroupData dataHolder = null;
      try
      {
         dataHolder = new edu.cornell.csuglab.cms.base.GroupData();

         dataHolder.setAssignmentID( getAssignmentID() );
         dataHolder.setTimeSlotID( getTimeSlotID() );
         dataHolder.setGroupID( getGroupID() );
         dataHolder.setLatestSubmission( getLatestSubmission() );
         dataHolder.setExtension( getExtension() );
         dataHolder.setFileCounter( getFileCounter() );
         dataHolder.setAdjustment( getAdjustment() );
         dataHolder.setRemainingSubmissions( getRemainingSubmissions() );

      }
      catch (RuntimeException e)
      {
         throw new javax.ejb.EJBException(e);
      }

      return dataHolder;
   }

   public edu.cornell.csuglab.cms.base.GroupPK ejbCreate(long assignmentid,int remainingSubmissions) throws javax.ejb.CreateException
   {
      super.ejbCreate(assignmentid,remainingSubmissions);

      return getDao().create((edu.cornell.csuglab.cms.base.GroupBean) this);
   }

   /**
    * Generated ejbPostCreate for corresponding ejbCreate method.
    *
    * @see #ejbCreate(long assignmentid,int remainingSubmissions)
    */
   public void ejbPostCreate(long assignmentid,int remainingSubmissions)
   {
   }

   public edu.cornell.csuglab.cms.base.GroupPK ejbFindByPrimaryKey(edu.cornell.csuglab.cms.base.GroupPK key) throws javax.ejb.FinderException
   {
      super.ejbFindByPrimaryKey(key);

      return getDao().findByPrimaryKey(key);
   }

   public java.util.Collection ejbFindByAssignmentID(long assignmentID) throws javax.ejb.FinderException
   {
      super.ejbFindByAssignmentID(assignmentID);

      return getDao().findByAssignmentID(assignmentID);
   }

   public java.util.Collection ejbFindLateByAssignmentID(long assignmentID) throws javax.ejb.FinderException
   {
      super.ejbFindLateByAssignmentID(assignmentID);

      return getDao().findLateByAssignmentID(assignmentID);
   }

   public java.util.Collection ejbFindByTimeSlotID(long timeSlotID) throws javax.ejb.FinderException
   {
      super.ejbFindByTimeSlotID(timeSlotID);

      return getDao().findByTimeSlotID(timeSlotID);
   }

   public edu.cornell.csuglab.cms.base.GroupPK ejbFindByGroupID(long groupID) throws javax.ejb.FinderException
   {
      super.ejbFindByGroupID(groupID);

      return getDao().findByGroupID(groupID);
   }

   public java.util.Collection ejbFindByGroupIDs(java.util.Collection groupIDs) throws javax.ejb.FinderException
   {
      super.ejbFindByGroupIDs(groupIDs);

      return getDao().findByGroupIDs(groupIDs);
   }

   public java.util.Collection ejbFindByNetIDCourseID(java.lang.String netID,long courseID) throws javax.ejb.FinderException
   {
      super.ejbFindByNetIDCourseID(netID,courseID);

      return getDao().findByNetIDCourseID(netID,courseID);
   }

   public edu.cornell.csuglab.cms.base.GroupPK ejbFindByNetIDAssignmentID(java.lang.String netID,long assignmentID) throws javax.ejb.FinderException
   {
      super.ejbFindByNetIDAssignmentID(netID,assignmentID);

      return getDao().findByNetIDAssignmentID(netID,assignmentID);
   }

   public java.util.Collection ejbFindByNetIDsAssignmentID(java.util.Collection netIDs,long assignmentID) throws javax.ejb.FinderException
   {
      super.ejbFindByNetIDsAssignmentID(netIDs,assignmentID);

      return getDao().findByNetIDsAssignmentID(netIDs,assignmentID);
   }

   public java.util.Collection ejbFindInvitedByNetIDAssignmentID(java.lang.String netID,long assignmentID) throws javax.ejb.FinderException
   {
      super.ejbFindInvitedByNetIDAssignmentID(netID,assignmentID);

      return getDao().findInvitedByNetIDAssignmentID(netID,assignmentID);
   }

   public void ejbLoad() 
   {
      getDao().load((edu.cornell.csuglab.cms.base.GroupPK) ctx.getPrimaryKey(), this);
      makeClean();
   }

   public void ejbStore() 
   {
      if (isModified())
      {
         getDao().store((edu.cornell.csuglab.cms.base.GroupBean) this);
         makeClean();
      }
   }

   public void ejbActivate() 
   {
   }

   public void ejbPassivate() 
   {

   }

   private javax.ejb.EntityContext ctx = null;

   public void setEntityContext(javax.ejb.EntityContext ctx) 
   {
      this.ctx = ctx;
   }

   public void unsetEntityContext() 
   {
      this.ctx = null;
   }

   public void ejbRemove() throws javax.ejb.RemoveException
   {
      getDao().remove((edu.cornell.csuglab.cms.base.GroupPK) ctx.getPrimaryKey());

   }

      private static edu.cornell.csuglab.cms.base.GroupDAO dao = null;

   protected static synchronized edu.cornell.csuglab.cms.base.GroupDAO getDao()
   {
      if (dao != null) {
         return dao;
      } else {

         dao = (edu.cornell.csuglab.cms.base.GroupDAO) new edu.cornell.csuglab.cms.base.dao.GroupDAOImpl();

         dao.init();
         return dao;
      }
   }

   /* Value Objects BEGIN */

/* Value Objects END */

}
