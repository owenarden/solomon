/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Local interface for AssignmentItem.
 * @lomboz generated
 */
public interface AssignmentItemLocal
   extends javax.ejb.EJBLocalObject
{

   public long getAssignmentItemID(  ) ;

   public void setAssignmentItemID( long assignmentItemID ) ;

   public long getAssignmentID(  ) ;

   public void setAssignmentID( long assignmentID ) ;

   public java.lang.String getItemName(  ) ;

   public void setItemName( java.lang.String itemName ) ;

   public boolean getHidden(  ) ;

   public void setHidden( boolean hidden ) ;

   public edu.cornell.csuglab.cms.base.AssignmentFileLocal getAssignmentFile(  ) throws javax.ejb.EJBException;

   public java.util.Collection getHiddenAssignmentFiles(  ) throws javax.ejb.EJBException;

   public edu.cornell.csuglab.cms.base.AssignmentItemData getAssignmentItemData(  ) ;

   public edu.cornell.csuglab.cms.base.AssignmentItemPK ejbFindByPrimaryKey( edu.cornell.csuglab.cms.base.AssignmentItemPK pk ) throws javax.ejb.FinderException;

   public java.util.Collection ejbFindByAssignmentID( long assignmentID ) throws javax.ejb.FinderException;

   public java.util.Collection ejbFindHiddenByAssignmentID( long assignmentID ) throws javax.ejb.FinderException;

}
