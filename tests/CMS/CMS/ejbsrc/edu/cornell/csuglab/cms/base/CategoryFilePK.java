/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Primary key for CategoryFile.
 * @lomboz generated
 */
public class CategoryFilePK
   extends java.lang.Object
   implements java.io.Serializable
{

   public long categoryFileID;

   public CategoryFilePK()
   {
   }

   public CategoryFilePK( long categoryFileID )
   {
      this.categoryFileID = categoryFileID;
   }

   public long getCategoryFileID()
   {
      return categoryFileID;
   }

   public void setCategoryFileID(long categoryFileID)
   {
      this.categoryFileID = categoryFileID;
   }

   public int hashCode()
   {
      int _hashCode = 0;
         _hashCode += (int)this.categoryFileID;

      return _hashCode;
   }

   public boolean equals(Object obj)
   {
      if( !(obj instanceof edu.cornell.csuglab.cms.base.CategoryFilePK) )
         return false;

      edu.cornell.csuglab.cms.base.CategoryFilePK pk = (edu.cornell.csuglab.cms.base.CategoryFilePK)obj;
      boolean eq = true;

      if( obj == null )
      {
         eq = false;
      }
      else
      {
         eq = eq && this.categoryFileID == pk.categoryFileID;
      }

      return eq;
   }

   /** @return String representation of this pk in the form of [.field1.field2.field3]. */
   public String toString()
   {
      StringBuffer toStringValue = new StringBuffer("[.");
         toStringValue.append(this.categoryFileID).append('.');
      toStringValue.append(']');
      return toStringValue.toString();
   }

}
