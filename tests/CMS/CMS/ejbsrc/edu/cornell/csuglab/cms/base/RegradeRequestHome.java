/*
 * Generated by XDoclet - Do not edit!
 */
package edu.cornell.csuglab.cms.base;

/**
 * Home interface for RegradeRequest.
 * @generated 
 * @lomboz generated
 */
public interface RegradeRequestHome
   extends javax.ejb.EJBHome
{
   public static final String COMP_NAME="java:comp/env/ejb/RegradeRequest";
   public static final String JNDI_NAME="RegradeRequest";

   public edu.cornell.csuglab.cms.base.RegradeRequest findByPrimaryKey(edu.cornell.csuglab.cms.base.RegradeRequestPK pk)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findByGroupID(long groupID)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findByGroupIDs(java.util.Collection groupids)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findByAssignmentID(long assignmentID)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findPendingByAssignmentID(long assignmentID)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findPendingByCourseID(long courseID)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findByCommentID(long commentID)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

   public java.util.Collection findByCourseID(long courseID)
      throws javax.ejb.FinderException,java.rmi.RemoteException;

}
