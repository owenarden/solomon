package edu.cornell.csuglab.cms.base;
public class ChoiceUtil {
native    public static edu.cornell.csuglab.cms.base.ChoiceHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.ChoiceHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.ChoiceLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
