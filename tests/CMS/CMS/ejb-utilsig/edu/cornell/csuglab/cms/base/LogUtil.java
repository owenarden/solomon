package edu.cornell.csuglab.cms.base;
public class LogUtil {
native    public static edu.cornell.csuglab.cms.base.LogHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.LogHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.LogLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
