package edu.cornell.csuglab.cms.base;
public class SemesterUtil {
native    public static edu.cornell.csuglab.cms.base.SemesterHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.SemesterHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.SemesterLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
