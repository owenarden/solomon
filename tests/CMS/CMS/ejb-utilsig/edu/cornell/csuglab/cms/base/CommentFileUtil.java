package edu.cornell.csuglab.cms.base;
public class CommentFileUtil {
native    public static edu.cornell.csuglab.cms.base.CommentFileHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CommentFileHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CommentFileLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
