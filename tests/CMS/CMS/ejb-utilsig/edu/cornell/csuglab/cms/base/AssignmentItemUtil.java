package edu.cornell.csuglab.cms.base;
public class AssignmentItemUtil {
native    public static edu.cornell.csuglab.cms.base.AssignmentItemHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.AssignmentItemHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.AssignmentItemLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
