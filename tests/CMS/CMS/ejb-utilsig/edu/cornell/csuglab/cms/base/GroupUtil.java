package edu.cornell.csuglab.cms.base;
public class GroupUtil {
native    public static edu.cornell.csuglab.cms.base.GroupHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.GroupHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.GroupLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
