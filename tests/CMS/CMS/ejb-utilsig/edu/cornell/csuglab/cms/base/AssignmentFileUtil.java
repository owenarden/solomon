package edu.cornell.csuglab.cms.base;
public class AssignmentFileUtil {
native    public static edu.cornell.csuglab.cms.base.AssignmentFileHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.AssignmentFileHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.AssignmentFileLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
