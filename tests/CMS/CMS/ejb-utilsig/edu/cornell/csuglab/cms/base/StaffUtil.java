package edu.cornell.csuglab.cms.base;
public class StaffUtil {
native    public static edu.cornell.csuglab.cms.base.StaffHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.StaffHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.StaffLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
