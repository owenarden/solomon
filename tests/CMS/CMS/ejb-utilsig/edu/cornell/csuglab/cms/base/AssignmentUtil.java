package edu.cornell.csuglab.cms.base;
public class AssignmentUtil {
native    public static edu.cornell.csuglab.cms.base.AssignmentHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.AssignmentHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.AssignmentLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
