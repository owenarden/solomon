package edu.cornell.csuglab.cms.base;
public class RequiredFileTypeUtil {
native    public static edu.cornell.csuglab.cms.base.RequiredFileTypeHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.RequiredFileTypeHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.RequiredFileTypeLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
