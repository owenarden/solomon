package edu.cornell.csuglab.cms.base;
public class GroupGradeUtil {
native    public static edu.cornell.csuglab.cms.base.GroupGradeHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.GroupGradeHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.GroupGradeLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
