package edu.cornell.csuglab.cms.base;
public class SolutionFileUtil {
native    public static edu.cornell.csuglab.cms.base.SolutionFileHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.SolutionFileHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.SolutionFileLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
