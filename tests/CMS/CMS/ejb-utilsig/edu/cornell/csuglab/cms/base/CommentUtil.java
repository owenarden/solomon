package edu.cornell.csuglab.cms.base;
public class CommentUtil {
native    public static edu.cornell.csuglab.cms.base.CommentHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CommentHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CommentLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
