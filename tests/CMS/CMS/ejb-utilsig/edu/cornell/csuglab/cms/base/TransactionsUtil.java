package edu.cornell.csuglab.cms.base;
public class TransactionsUtil {
native    public static edu.cornell.csuglab.cms.base.TransactionsHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.TransactionsHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.TransactionsLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
