package edu.cornell.csuglab.cms.base;
public class GroupMemberUtil {
native    public static edu.cornell.csuglab.cms.base.GroupMemberHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.GroupMemberHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.GroupMemberLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
