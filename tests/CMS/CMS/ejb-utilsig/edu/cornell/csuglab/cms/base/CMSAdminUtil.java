package edu.cornell.csuglab.cms.base;
public class CMSAdminUtil {
native    public static edu.cornell.csuglab.cms.base.CMSAdminHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CMSAdminHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CMSAdminLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
