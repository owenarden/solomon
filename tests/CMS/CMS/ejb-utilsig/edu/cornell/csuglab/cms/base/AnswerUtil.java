package edu.cornell.csuglab.cms.base;
public class AnswerUtil {
native    public static edu.cornell.csuglab.cms.base.AnswerHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.AnswerHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.AnswerLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
