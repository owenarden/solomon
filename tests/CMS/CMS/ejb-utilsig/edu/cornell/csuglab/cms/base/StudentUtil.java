package edu.cornell.csuglab.cms.base;
public class StudentUtil {
native    public static edu.cornell.csuglab.cms.base.StudentHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.StudentHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.StudentLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
