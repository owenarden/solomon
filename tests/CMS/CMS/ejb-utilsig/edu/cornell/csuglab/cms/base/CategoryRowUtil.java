package edu.cornell.csuglab.cms.base;
public class CategoryRowUtil {
native    public static edu.cornell.csuglab.cms.base.CategoryRowHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CategoryRowHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CategoryRowLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
