package edu.cornell.csuglab.cms.base;
public class DomainUtil {
native    public static edu.cornell.csuglab.cms.base.DomainHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.DomainHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.DomainLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
