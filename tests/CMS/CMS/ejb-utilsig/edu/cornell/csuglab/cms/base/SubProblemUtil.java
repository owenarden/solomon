package edu.cornell.csuglab.cms.base;
public class SubProblemUtil {
native    public static edu.cornell.csuglab.cms.base.SubProblemHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.SubProblemHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.SubProblemLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
