package edu.cornell.csuglab.cms.base;
public class CategoryFileUtil {
native    public static edu.cornell.csuglab.cms.base.CategoryFileHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CategoryFileHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CategoryFileLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
