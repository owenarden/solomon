package edu.cornell.csuglab.cms.base;
public class GradeUtil {
native    public static edu.cornell.csuglab.cms.base.GradeHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.GradeHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.GradeLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
