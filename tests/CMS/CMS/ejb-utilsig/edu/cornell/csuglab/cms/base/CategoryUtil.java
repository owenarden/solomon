package edu.cornell.csuglab.cms.base;
public class CategoryUtil {
native    public static edu.cornell.csuglab.cms.base.CategoryHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CategoryHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CategoryLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
