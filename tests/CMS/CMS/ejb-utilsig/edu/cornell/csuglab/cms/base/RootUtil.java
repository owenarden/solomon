package edu.cornell.csuglab.cms.base;
public class RootUtil {
native    public static edu.cornell.csuglab.cms.base.RootHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.RootHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.RootLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
