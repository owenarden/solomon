package edu.cornell.csuglab.cms.base;
public class EmailUtil {
native    public static edu.cornell.csuglab.cms.base.EmailHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.EmailHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.EmailLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
