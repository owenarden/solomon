#!/bin/bash
SUF=".java"
dir=$1
shift
for filename in $@
do
	basename=`basename $filename`
	outname=$dir/$basename
	classname=${basename%$SUF}
	echo "package edu.cornell.csuglab.cms.base;" > $outname
	echo "public class $classname {" >> $outname
    oldIFS=$IFS
	IFS=$(echo -en "\n\b")
	for method in `grep public $filename |grep -v class` 
	do
		method=${method%\{}
		echo native $method ";" >> $outname
	done
	echo "}" >> $outname
	IFS=$oldIFS
done
