package edu.cornell.csuglab.cms.base;
public class OldAnnouncementUtil {
native    public static edu.cornell.csuglab.cms.base.OldAnnouncementHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.OldAnnouncementHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.OldAnnouncementLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
