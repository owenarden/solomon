package edu.cornell.csuglab.cms.base;
public class SubmittedFileUtil {
native    public static edu.cornell.csuglab.cms.base.SubmittedFileHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.SubmittedFileHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.SubmittedFileLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
