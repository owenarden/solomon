package edu.cornell.csuglab.cms.base;
public class CategoryColUtil {
native    public static edu.cornell.csuglab.cms.base.CategoryColHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CategoryColHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CategoryColLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
