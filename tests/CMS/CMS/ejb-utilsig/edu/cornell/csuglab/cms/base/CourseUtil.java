package edu.cornell.csuglab.cms.base;
public class CourseUtil {
native    public static edu.cornell.csuglab.cms.base.CourseLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
