package edu.cornell.csuglab.cms.base;
public class GroupAssignedToUtil {
native    public static edu.cornell.csuglab.cms.base.GroupAssignedToHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.GroupAssignedToHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.GroupAssignedToLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
