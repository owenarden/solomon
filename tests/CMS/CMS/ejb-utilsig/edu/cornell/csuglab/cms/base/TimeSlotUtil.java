package edu.cornell.csuglab.cms.base;
public class TimeSlotUtil {
native    public static edu.cornell.csuglab.cms.base.TimeSlotHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.TimeSlotHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.TimeSlotLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
