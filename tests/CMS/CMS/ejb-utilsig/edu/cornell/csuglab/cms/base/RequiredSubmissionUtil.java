package edu.cornell.csuglab.cms.base;
public class RequiredSubmissionUtil {
native    public static edu.cornell.csuglab.cms.base.RequiredSubmissionHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.RequiredSubmissionHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.RequiredSubmissionLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
