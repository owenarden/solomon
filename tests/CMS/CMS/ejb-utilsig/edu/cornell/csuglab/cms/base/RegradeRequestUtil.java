package edu.cornell.csuglab.cms.base;
public class RegradeRequestUtil {
native    public static edu.cornell.csuglab.cms.base.RegradeRequestHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.RegradeRequestHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.RegradeRequestLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
