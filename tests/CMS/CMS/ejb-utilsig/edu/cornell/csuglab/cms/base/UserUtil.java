package edu.cornell.csuglab.cms.base;
public class UserUtil {
native    public static edu.cornell.csuglab.cms.base.UserLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
