package edu.cornell.csuglab.cms.base;
public class AnswerSetUtil {
native    public static edu.cornell.csuglab.cms.base.AnswerSetHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.AnswerSetHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.AnswerSetLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
