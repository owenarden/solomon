package edu.cornell.csuglab.cms.base;
public class AnnouncementUtil {
native    public static edu.cornell.csuglab.cms.base.AnnouncementHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.AnnouncementHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.AnnouncementLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
