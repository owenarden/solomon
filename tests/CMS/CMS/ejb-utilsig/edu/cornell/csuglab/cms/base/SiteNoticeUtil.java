package edu.cornell.csuglab.cms.base;
public class SiteNoticeUtil {
native    public static edu.cornell.csuglab.cms.base.SiteNoticeHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.SiteNoticeHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.SiteNoticeLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
