package edu.cornell.csuglab.cms.base;
public class CategoryContentsUtil {
native    public static edu.cornell.csuglab.cms.base.CategoryContentsHome getHome() throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CategoryContentsHome getHome( java.util.Hashtable environment ) throws javax.naming.NamingException ;
native    public static edu.cornell.csuglab.cms.base.CategoryContentsLocalHome getLocalHome() throws javax.naming.NamingException ;
native    public static final String generateGUID(Object o)  ;
}
