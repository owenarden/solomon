/****** Object:  Database cmsdb_j2ee    Script Date: 11/9/2006 9:37:05 AM ******/
IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'cmsdb_j2ee')
	DROP DATABASE [cmsdb_j2ee]
GO

CREATE DATABASE [cmsdb_j2ee]  ON (NAME = N'cmsdb_j2ee_Data', FILENAME = N'F:\Microsoft SQL Server\MSSQL\data\cmsdb_j2ee_Data.MDF' , SIZE = 6, FILEGROWTH = 10%) LOG ON (NAME = N'cmsdb_j2ee_Log', FILENAME = N'F:\Microsoft SQL Server\MSSQL\data\cmsdb_j2ee_Log.LDF' , SIZE = 28, FILEGROWTH = 10%)
 COLLATE SQL_Latin1_General_CP1_CI_AS
GO

exec sp_dboption N'cmsdb_j2ee', N'autoclose', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'bulkcopy', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'trunc. log', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'torn page detection', N'true'
GO

exec sp_dboption N'cmsdb_j2ee', N'read only', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'dbo use', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'single', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'autoshrink', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'ANSI null default', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'recursive triggers', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'ANSI nulls', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'concat null yields null', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'cursor close on commit', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'default to local cursor', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'quoted identifier', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'ANSI warnings', N'false'
GO

exec sp_dboption N'cmsdb_j2ee', N'auto create statistics', N'true'
GO

exec sp_dboption N'cmsdb_j2ee', N'auto update statistics', N'true'
GO

if( ( (@@microsoftversion / power(2, 24) = 8) and (@@microsoftversion & 0xffff >= 724) ) or ( (@@microsoftversion / power(2, 24) = 7) and (@@microsoftversion & 0xffff >= 1082) ) )
	exec sp_dboption N'cmsdb_j2ee', N'db chaining', N'false'
GO

use [cmsdb_j2ee]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tLogDetails_tLogs]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tLogDetails] DROP CONSTRAINT FK_tLogDetails_tLogs
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tCourse_tSemesters]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tCourse] DROP CONSTRAINT FK_tCourse_tSemesters
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tSystemProperties_tSemesters]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tSystemProperties] DROP CONSTRAINT FK_tSystemProperties_tSemesters
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tCmsAdmin_tUser]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tCmsAdmin] DROP CONSTRAINT FK_tCmsAdmin_tUser
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tEmails_tUser]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tEmails] DROP CONSTRAINT FK_tEmails_tUser
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tGrades_tUser]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tGrades] DROP CONSTRAINT FK_tGrades_tUser
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tGroupMembers_tUser]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tGroupMembers] DROP CONSTRAINT FK_tGroupMembers_tUser
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tStaff_tUser]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tStaff] DROP CONSTRAINT FK_tStaff_tUser
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tStudent_tUser]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tStudent] DROP CONSTRAINT FK_tStudent_tUser
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tTimeSlot_tUser]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tTimeSlot] DROP CONSTRAINT FK_tTimeSlot_tUser
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tAnnouncement_tCourse]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tAnnouncement] DROP CONSTRAINT FK_tAnnouncement_tCourse
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tAssignment_tCourse]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tAssignment] DROP CONSTRAINT FK_tAssignment_tCourse
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tCategori_tCourse]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tCategori] DROP CONSTRAINT FK_tCategori_tCourse
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tEmails_tCourse]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tEmails] DROP CONSTRAINT FK_tEmails_tCourse
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tStaff_tCourse]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tStaff] DROP CONSTRAINT FK_tStaff_tCourse
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tStudent_tCourse]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tStudent] DROP CONSTRAINT FK_tStudent_tCourse
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tOldAnnouncement_tAnnouncement]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tOldAnnouncement] DROP CONSTRAINT FK_tOldAnnouncement_tAnnouncement
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AssignmentItems_tAssignment]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tAssignmentItems] DROP CONSTRAINT FK_AssignmentItems_tAssignment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tGrades_tAssignment]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tGrades] DROP CONSTRAINT FK_tGrades_tAssignment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tGroups_tAssignment]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tGroups] DROP CONSTRAINT FK_tGroups_tAssignment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tRequiredSubmissions_tAssignment]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tRequiredSubmissions] DROP CONSTRAINT FK_tRequiredSubmissions_tAssignment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tSolutionFiles_tAssignment]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tSolutionFiles] DROP CONSTRAINT FK_tSolutionFiles_tAssignment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tSubProblems_tAssignment]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tSubProblems] DROP CONSTRAINT FK_tSubProblems_tAssignment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tTimeSlot_tAssignment]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tTimeSlot] DROP CONSTRAINT FK_tTimeSlot_tAssignment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tCategoryCol_tCategori]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tCategoryCol] DROP CONSTRAINT FK_tCategoryCol_tCategori
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tCategoryRow_tCategori]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tCategoryRow] DROP CONSTRAINT FK_tCategoryRow_tCategori
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tAssignmentFiles_AssignmentItems]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tAssignmentFiles] DROP CONSTRAINT FK_tAssignmentFiles_AssignmentItems
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tCtgContents_tCategoryCol]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tCtgContents] DROP CONSTRAINT FK_tCtgContents_tCategoryCol
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tCtgContents_tCategoryRow]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tCtgContents] DROP CONSTRAINT FK_tCtgContents_tCategoryRow
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tRequiredFileTypes_tRequiredSubmissions]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tRequiredFileTypes] DROP CONSTRAINT FK_tRequiredFileTypes_tRequiredSubmissions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tSubmittedFiles_tRequiredSubmissions]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tSubmittedFiles] DROP CONSTRAINT FK_tSubmittedFiles_tRequiredSubmissions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tGroups_tTimeSlot]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tGroups] DROP CONSTRAINT FK_tGroups_tTimeSlot
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tCategoryFiles_tCtgContents]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tCategoryFiles] DROP CONSTRAINT FK_tCategoryFiles_tCtgContents
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tComments_tGroups]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tComments] DROP CONSTRAINT FK_tComments_tGroups
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tGroupAssignedTo_tGroups]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tGroupAssignedTo] DROP CONSTRAINT FK_tGroupAssignedTo_tGroups
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tGroupGrades_tGroups]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tGroupGrades] DROP CONSTRAINT FK_tGroupGrades_tGroups
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tGroupMembers_tGroups]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tGroupMembers] DROP CONSTRAINT FK_tGroupMembers_tGroups
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tRegradeRequests_tGroups]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tRegradeRequests] DROP CONSTRAINT FK_tRegradeRequests_tGroups
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tSubmittedFiles_tGroups]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tSubmittedFiles] DROP CONSTRAINT FK_tSubmittedFiles_tGroups
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tSubmittedFiles_tGroups1]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tSubmittedFiles] DROP CONSTRAINT FK_tSubmittedFiles_tGroups1
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tCommentFiles_tComments]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tCommentFiles] DROP CONSTRAINT FK_tCommentFiles_tComments
GO

/****** Object:  Table [dbo].[tCommentFiles]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tCommentFiles]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tCommentFiles]
GO

/****** Object:  Table [dbo].[tCategoryFiles]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tCategoryFiles]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tCategoryFiles]
GO

/****** Object:  Table [dbo].[tComments]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tComments]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tComments]
GO

/****** Object:  Table [dbo].[tGroupAssignedTo]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tGroupAssignedTo]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tGroupAssignedTo]
GO

/****** Object:  Table [dbo].[tGroupGrades]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tGroupGrades]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tGroupGrades]
GO

/****** Object:  Table [dbo].[tGroupMembers]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tGroupMembers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tGroupMembers]
GO

/****** Object:  Table [dbo].[tRegradeRequests]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tRegradeRequests]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tRegradeRequests]
GO

/****** Object:  Table [dbo].[tSubmittedFiles]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tSubmittedFiles]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tSubmittedFiles]
GO

/****** Object:  Table [dbo].[tAssignmentFiles]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tAssignmentFiles]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tAssignmentFiles]
GO

/****** Object:  Table [dbo].[tCtgContents]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tCtgContents]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tCtgContents]
GO

/****** Object:  Table [dbo].[tGroups]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tGroups]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tGroups]
GO

/****** Object:  Table [dbo].[tRequiredFileTypes]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tRequiredFileTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tRequiredFileTypes]
GO

/****** Object:  Table [dbo].[tAssignmentItems]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tAssignmentItems]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tAssignmentItems]
GO

/****** Object:  Table [dbo].[tCategoryCol]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tCategoryCol]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tCategoryCol]
GO

/****** Object:  Table [dbo].[tCategoryRow]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tCategoryRow]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tCategoryRow]
GO

/****** Object:  Table [dbo].[tGrades]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tGrades]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tGrades]
GO

/****** Object:  Table [dbo].[tOldAnnouncement]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tOldAnnouncement]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tOldAnnouncement]
GO

/****** Object:  Table [dbo].[tRequiredSubmissions]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tRequiredSubmissions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tRequiredSubmissions]
GO

/****** Object:  Table [dbo].[tSolutionFiles]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tSolutionFiles]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tSolutionFiles]
GO

/****** Object:  Table [dbo].[tSubProblems]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tSubProblems]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tSubProblems]
GO

/****** Object:  Table [dbo].[tTimeSlot]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tTimeSlot]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tTimeSlot]
GO

/****** Object:  Table [dbo].[tAnnouncement]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tAnnouncement]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tAnnouncement]
GO

/****** Object:  Table [dbo].[tAssignment]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tAssignment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tAssignment]
GO

/****** Object:  Table [dbo].[tCategori]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tCategori]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tCategori]
GO

/****** Object:  Table [dbo].[tEmails]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tEmails]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tEmails]
GO

/****** Object:  Table [dbo].[tStaff]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tStaff]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tStaff]
GO

/****** Object:  Table [dbo].[tStudent]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tStudent]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tStudent]
GO

/****** Object:  Table [dbo].[tCmsAdmin]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tCmsAdmin]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tCmsAdmin]
GO

/****** Object:  Table [dbo].[tCourse]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tCourse]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tCourse]
GO

/****** Object:  Table [dbo].[tLogDetails]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tLogDetails]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tLogDetails]
GO

/****** Object:  Table [dbo].[tSystemProperties]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tSystemProperties]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tSystemProperties]
GO

/****** Object:  Table [cmsdb_java].[JMS_MESSAGES]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[cmsdb_java].[JMS_MESSAGES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [cmsdb_java].[JMS_MESSAGES]
GO

/****** Object:  Table [cmsdb_j2ee].[JMS_MESSAGES]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[cmsdb_j2ee].[JMS_MESSAGES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [cmsdb_j2ee].[JMS_MESSAGES]
GO

/****** Object:  Table [cmsdb_java].[JMS_ROLES]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[cmsdb_java].[JMS_ROLES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [cmsdb_java].[JMS_ROLES]
GO

/****** Object:  Table [cmsdb_j2ee].[JMS_ROLES]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[cmsdb_j2ee].[JMS_ROLES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [cmsdb_j2ee].[JMS_ROLES]
GO

/****** Object:  Table [dbo].[JMS_SUBSCRIPTIONS]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[JMS_SUBSCRIPTIONS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[JMS_SUBSCRIPTIONS]
GO

/****** Object:  Table [cmsdb_java].[JMS_SUBSCRIPTIONS]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[cmsdb_java].[JMS_SUBSCRIPTIONS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [cmsdb_java].[JMS_SUBSCRIPTIONS]
GO

/****** Object:  Table [cmsdb_j2ee].[JMS_SUBSCRIPTIONS]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[cmsdb_j2ee].[JMS_SUBSCRIPTIONS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [cmsdb_j2ee].[JMS_SUBSCRIPTIONS]
GO

/****** Object:  Table [cmsdb_java].[JMS_TRANSACTIONS]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[cmsdb_java].[JMS_TRANSACTIONS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [cmsdb_java].[JMS_TRANSACTIONS]
GO

/****** Object:  Table [cmsdb_j2ee].[JMS_TRANSACTIONS]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[cmsdb_j2ee].[JMS_TRANSACTIONS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [cmsdb_j2ee].[JMS_TRANSACTIONS]
GO

/****** Object:  Table [cmsdb_java].[JMS_USERS]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[cmsdb_java].[JMS_USERS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [cmsdb_java].[JMS_USERS]
GO

/****** Object:  Table [cmsdb_j2ee].[JMS_USERS]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[cmsdb_j2ee].[JMS_USERS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [cmsdb_j2ee].[JMS_USERS]
GO

/****** Object:  Table [cmsdb_j2ee].[TIMERS]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[cmsdb_j2ee].[TIMERS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [cmsdb_j2ee].[TIMERS]
GO

/****** Object:  Table [dbo].[tFileTypes]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tFileTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tFileTypes]
GO

/****** Object:  Table [dbo].[tLogs]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tLogs]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tLogs]
GO

/****** Object:  Table [dbo].[tSemesters]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tSemesters]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tSemesters]
GO

/****** Object:  Table [dbo].[tUser]    Script Date: 11/9/2006 9:37:13 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tUser]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tUser]
GO

/****** Object:  Table [cmsdb_java].[JMS_MESSAGES]    Script Date: 11/9/2006 9:37:17 AM ******/
CREATE TABLE [cmsdb_java].[JMS_MESSAGES] (
	[MESSAGEID] [int] NOT NULL ,
	[DESTINATION] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TXID] [int] NULL ,
	[TXOP] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MESSAGEBLOB] [image] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [cmsdb_j2ee].[JMS_MESSAGES]    Script Date: 11/9/2006 9:37:17 AM ******/
CREATE TABLE [cmsdb_j2ee].[JMS_MESSAGES] (
	[MESSAGEID] [int] NOT NULL ,
	[DESTINATION] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TXID] [int] NULL ,
	[TXOP] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MESSAGEBLOB] [image] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [cmsdb_java].[JMS_ROLES]    Script Date: 11/9/2006 9:37:18 AM ******/
CREATE TABLE [cmsdb_java].[JMS_ROLES] (
	[ROLEID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[USERID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [cmsdb_j2ee].[JMS_ROLES]    Script Date: 11/9/2006 9:37:18 AM ******/
CREATE TABLE [cmsdb_j2ee].[JMS_ROLES] (
	[ROLEID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[USERID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[JMS_SUBSCRIPTIONS]    Script Date: 11/9/2006 9:37:18 AM ******/
CREATE TABLE [dbo].[JMS_SUBSCRIPTIONS] (
	[CLIENTID] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SUBNAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TOPIC] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SELECTOR] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [cmsdb_java].[JMS_SUBSCRIPTIONS]    Script Date: 11/9/2006 9:37:19 AM ******/
CREATE TABLE [cmsdb_java].[JMS_SUBSCRIPTIONS] (
	[CLIENTID] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SUBNAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TOPIC] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SELECTOR] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [cmsdb_j2ee].[JMS_SUBSCRIPTIONS]    Script Date: 11/9/2006 9:37:19 AM ******/
CREATE TABLE [cmsdb_j2ee].[JMS_SUBSCRIPTIONS] (
	[CLIENTID] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SUBNAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TOPIC] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SELECTOR] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [cmsdb_java].[JMS_TRANSACTIONS]    Script Date: 11/9/2006 9:37:19 AM ******/
CREATE TABLE [cmsdb_java].[JMS_TRANSACTIONS] (
	[TXID] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [cmsdb_j2ee].[JMS_TRANSACTIONS]    Script Date: 11/9/2006 9:37:20 AM ******/
CREATE TABLE [cmsdb_j2ee].[JMS_TRANSACTIONS] (
	[TXID] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [cmsdb_java].[JMS_USERS]    Script Date: 11/9/2006 9:37:20 AM ******/
CREATE TABLE [cmsdb_java].[JMS_USERS] (
	[USERID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PASSWD] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CLIENTID] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [cmsdb_j2ee].[JMS_USERS]    Script Date: 11/9/2006 9:37:20 AM ******/
CREATE TABLE [cmsdb_j2ee].[JMS_USERS] (
	[USERID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PASSWD] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CLIENTID] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [cmsdb_j2ee].[TIMERS]    Script Date: 11/9/2006 9:37:21 AM ******/
CREATE TABLE [cmsdb_j2ee].[TIMERS] (
	[TIMERID] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TARGETID] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[INITIALDATE] [datetime] NOT NULL ,
	[TIMERINTERVAL] [bigint] NULL ,
	[INSTANCEPK] [image] NULL ,
	[INFO] [image] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tFileTypes]    Script Date: 11/9/2006 9:37:21 AM ******/
CREATE TABLE [dbo].[tFileTypes] (
	[FileType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tLogs]    Script Date: 11/9/2006 9:37:21 AM ******/
CREATE TABLE [dbo].[tLogs] (
	[LogID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[ActingNetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SimulatedNetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ActingIPAddress] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Dte] [datetime] NOT NULL ,
	[LogName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LogType] [int] NOT NULL ,
	[CourseID] [bigint] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tSemesters]    Script Date: 11/9/2006 9:37:22 AM ******/
CREATE TABLE [dbo].[tSemesters] (
	[SemesterID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[SemesterName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Hidden] [bit] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tUser]    Script Date: 11/9/2006 9:37:22 AM ******/
CREATE TABLE [dbo].[tUser] (
	[NetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[FirstName] [varchar] (50) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL ,
	[LastName] [varchar] (50) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL ,
	[CUID] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[College] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tCmsAdmin]    Script Date: 11/9/2006 9:37:22 AM ******/
CREATE TABLE [dbo].[tCmsAdmin] (
	[NetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tCourse]    Script Date: 11/9/2006 9:37:23 AM ******/
CREATE TABLE [dbo].[tCourse] (
	[CourseID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[Code] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DisplayedCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name] [varchar] (100) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL ,
	[Description] [varchar] (8000) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL ,
	[SemesterID] [bigint] NOT NULL ,
	[ShowFinalGrade] [bit] NOT NULL ,
	[ShowTotalScore] [bit] NOT NULL ,
	[ShowAssignWeights] [bit] NOT NULL ,
	[ShowGraderNetID] [bit] NULL ,
	[FreezeCourse] [bit] NOT NULL ,
	[FileCounter] [bigint] NOT NULL ,
	[Hidden] [bit] NOT NULL ,
	[CourseGuestAccess] [bit] NOT NULL ,
	[AssignGuestAccess] [bit] NOT NULL ,
	[AnnounceGuestAccess] [bit] NOT NULL ,
	[SolutionGuestAccess] [bit] NOT NULL ,
	[CourseCCAccess] [bit] NOT NULL ,
	[AssignCCAccess] [bit] NOT NULL ,
	[AnnounceCCAccess] [bit] NOT NULL ,
	[SolutionCCAccess] [bit] NOT NULL ,
	[MaxTotalScore] [float] NULL ,
	[HighTotalScore] [float] NULL ,
	[MeanTotalScore] [float] NULL ,
	[MedianTotalScore] [float] NULL ,
	[StDevTotalScore] [float] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tLogDetails]    Script Date: 11/9/2006 9:37:24 AM ******/
CREATE TABLE [dbo].[tLogDetails] (
	[LogID] [bigint] NOT NULL ,
	[Details] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AssignmentID] [bigint] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tSystemProperties]    Script Date: 11/9/2006 9:37:24 AM ******/
CREATE TABLE [dbo].[tSystemProperties] (
	[ID] [int] NOT NULL ,
	[CurrentSemester] [bigint] NOT NULL ,
	[DebugMode] [bit] NOT NULL ,
	[UploadMaxFileSize] [bigint] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tAnnouncement]    Script Date: 11/9/2006 9:37:24 AM ******/
CREATE TABLE [dbo].[tAnnouncement] (
	[AnnouncementID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[CourseID] [bigint] NOT NULL ,
	[Author] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PostDate] [datetime] NOT NULL ,
	[Text] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Hidden] [bit] NOT NULL ,
	[EditInfo] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tAssignment]    Script Date: 11/9/2006 9:37:25 AM ******/
CREATE TABLE [dbo].[tAssignment] (
	[AssignmentID] [bigint] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[CourseID] [bigint] NOT NULL ,
	[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[NameShort] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Status] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Weight] [numeric](8, 0) NULL ,
	[MaxScore] [numeric](8, 0) NULL ,
	[DueDate] [datetime] NOT NULL ,
	[GracePeriod] [int] NULL ,
	[StudentRegrade] [bit] NULL ,
	[RegradeDeadline] [datetime] NULL ,
	[LateDeadline] [datetime] NULL ,
	[AllowLate] [bit] NULL ,
	[NumAssignedFiles] [int] NULL ,
	[StatMedian] [real] NULL ,
	[StatMax] [real] NULL ,
	[StatMean] [real] NULL ,
	[StatStDev] [real] NULL ,
	[ShowStats] [bit] NOT NULL ,
	[ShowSolution] [bit] NOT NULL ,
	[AssignedGraders] [bit] NOT NULL ,
	[GroupSizeMax] [int] NULL ,
	[GroupSizeMin] [int] NULL ,
	[Description] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AssignedGroups] [bit] NOT NULL ,
	[GroupLimit] [int] NULL ,
	[Duration] [bigint] NULL ,
	[Scheduled] [bit] NOT NULL ,
	[ScheduleLockTime] [datetime] NULL ,
	[Hidden] [bit] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tCategori]    Script Date: 11/9/2006 9:37:26 AM ******/
CREATE TABLE [dbo].[tCategori] (
	[CategoryID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[CourseID] [bigint] NOT NULL ,
	[CategoryName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Hidden] [bit] NOT NULL ,
	[Ascending] [bit] NOT NULL ,
	[SortByColId] [bigint] NOT NULL ,
	[FileCount] [bigint] NOT NULL ,
	[NumShowContents] [bigint] NOT NULL ,
	[Authorzn] [int] NOT NULL ,
	[Positn] [int] NULL ,
	[Special] [bit] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tEmails]    Script Date: 11/9/2006 9:37:26 AM ******/
CREATE TABLE [dbo].[tEmails] (
	[EmailID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[CourseID] [bigint] NOT NULL ,
	[Recipient] [tinyint] NOT NULL ,
	[Subject] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Message] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[DateSent] [datetime] NOT NULL ,
	[Sender] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tStaff]    Script Date: 11/9/2006 9:37:27 AM ******/
CREATE TABLE [dbo].[tStaff] (
	[NetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CourseID] [bigint] NOT NULL ,
	[AdminPriv] [bit] NOT NULL ,
	[GroupsPriv] [bit] NOT NULL ,
	[GradesPriv] [bit] NOT NULL ,
	[AssignmentsPriv] [bit] NOT NULL ,
	[CategoryPriv] [bit] NULL ,
	[Status] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[EmailNewAssign] [bit] NOT NULL ,
	[EmailDueDate] [bit] NOT NULL ,
	[EmailAssignedTo] [bit] NOT NULL ,
	[EmailRequest] [bit] NOT NULL ,
	[EmailFinalGrade] [bit] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tStudent]    Script Date: 11/9/2006 9:37:28 AM ******/
CREATE TABLE [dbo].[tStudent] (
	[NetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CourseID] [bigint] NOT NULL ,
	[Status] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[FinalGrade] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TotalScore] [float] NULL ,
	[Lecture] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Lab] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Section] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[GradeOption] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Credits] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmailNewAssign] [bit] NOT NULL ,
	[EmailDueDate] [bit] NOT NULL ,
	[EmailGroupInvites] [bit] NOT NULL ,
	[EmailNewGrade] [bit] NOT NULL ,
	[EmailRegrade] [bit] NOT NULL ,
	[EmailFileSubmit] [bit] NOT NULL ,
	[EmailFinalGrade] [bit] NOT NULL ,
	[EmailTimeSlot] [bit] NOT NULL ,
	[Department] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CourseNum] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tAssignmentItems]    Script Date: 11/9/2006 9:37:28 AM ******/
CREATE TABLE [dbo].[tAssignmentItems] (
	[AssignmentItemID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[AssignmentID] [bigint] NOT NULL ,
	[ItemName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Hidden] [bit] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tCategoryCol]    Script Date: 11/9/2006 9:37:29 AM ******/
CREATE TABLE [dbo].[tCategoryCol] (
	[ColID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[ColName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ColType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CategoryID] [bigint] NOT NULL ,
	[Hidden] [bit] NOT NULL ,
	[Positn] [bigint] NULL ,
	[Removed] [bit] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tCategoryRow]    Script Date: 11/9/2006 9:37:29 AM ******/
CREATE TABLE [dbo].[tCategoryRow] (
	[RowID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[CategoryID] [bigint] NOT NULL ,
	[Hidden] [bit] NOT NULL ,
	[ReleaseDate] [datetime] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tGrades]    Script Date: 11/9/2006 9:37:30 AM ******/
CREATE TABLE [dbo].[tGrades] (
	[GradeID] [bigint] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[AssignmentID] [bigint] NOT NULL ,
	[SubProblemID] [bigint] NOT NULL ,
	[NetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Score] [float] NOT NULL ,
	[GraderNetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CommentID] [bigint] NULL ,
	[DateEntered] [datetime] NULL ,
	[RequestedBy] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tOldAnnouncement]    Script Date: 11/9/2006 9:37:30 AM ******/
CREATE TABLE [dbo].[tOldAnnouncement] (
	[OldAnnouncementID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[AnnouncementID] [bigint] NOT NULL ,
	[Text] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tRequiredSubmissions]    Script Date: 11/9/2006 9:37:30 AM ******/
CREATE TABLE [dbo].[tRequiredSubmissions] (
	[SubmissionID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[AssignmentID] [bigint] NOT NULL ,
	[SubmissionName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[MaxSize] [int] NOT NULL ,
	[Hidden] [bit] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tSolutionFiles]    Script Date: 11/9/2006 9:37:31 AM ******/
CREATE TABLE [dbo].[tSolutionFiles] (
	[SolutionFileID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[AssignmentID] [bigint] NOT NULL ,
	[FileName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Hidden] [bit] NOT NULL ,
	[Path] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tSubProblems]    Script Date: 11/9/2006 9:37:31 AM ******/
CREATE TABLE [dbo].[tSubProblems] (
	[SubProblemID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[AssignmentID] [bigint] NOT NULL ,
	[SubProblemName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[MaxScore] [float] NOT NULL ,
	[Hidden] [bit] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tTimeSlot]    Script Date: 11/9/2006 9:37:32 AM ******/
CREATE TABLE [dbo].[tTimeSlot] (
	[TimeSlotID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[AssignmentID] [bigint] NOT NULL ,
	[TimeSlotName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Location] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Population] [int] NOT NULL ,
	[StaffNetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[StartTime] [datetime] NOT NULL ,
	[Hidden] [bit] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tAssignmentFiles]    Script Date: 11/9/2006 9:37:32 AM ******/
CREATE TABLE [dbo].[tAssignmentFiles] (
	[AssignmentFileID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[AssignmentItemID] [bigint] NOT NULL ,
	[FileName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Hidden] [bit] NOT NULL ,
	[FileDate] [datetime] NOT NULL ,
	[Path] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tCtgContents]    Script Date: 11/9/2006 9:37:33 AM ******/
CREATE TABLE [dbo].[tCtgContents] (
	[ContentID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[ColID] [bigint] NOT NULL ,
	[ColType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[RowID] [bigint] NOT NULL ,
	[Dte] [datetime] NULL ,
	[Text] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LinkName] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Number] [bigint] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tGroups]    Script Date: 11/9/2006 9:37:33 AM ******/
CREATE TABLE [dbo].[tGroups] (
	[GroupID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[AssignmentID] [bigint] NOT NULL ,
	[LatestSubmission] [datetime] NULL ,
	[Extension] [datetime] NULL ,
	[FileCounter] [int] NOT NULL ,
	[RemainingSubmissions] [int] NOT NULL ,
	[TimeSlotID] [bigint] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tRequiredFileTypes]    Script Date: 11/9/2006 9:37:34 AM ******/
CREATE TABLE [dbo].[tRequiredFileTypes] (
	[FileType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SubmissionID] [bigint] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tCategoryFiles]    Script Date: 11/9/2006 9:37:34 AM ******/
CREATE TABLE [dbo].[tCategoryFiles] (
	[CategoryFileID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[ContentID] [bigint] NOT NULL ,
	[FileName] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Hidden] [bit] NOT NULL ,
	[Path] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LinkName] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tComments]    Script Date: 11/9/2006 9:37:35 AM ******/
CREATE TABLE [dbo].[tComments] (
	[CommentID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[GroupID] [bigint] NOT NULL ,
	[Comment] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[NetID] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[DateEntered] [datetime] NOT NULL ,
	[Hidden] [bit] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tGroupAssignedTo]    Script Date: 11/9/2006 9:37:35 AM ******/
CREATE TABLE [dbo].[tGroupAssignedTo] (
	[GroupID] [bigint] NOT NULL ,
	[SubProblemID] [bigint] NOT NULL ,
	[NetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tGroupGrades]    Script Date: 11/9/2006 9:37:35 AM ******/
CREATE TABLE [dbo].[tGroupGrades] (
	[GroupID] [bigint] NOT NULL ,
	[SubproblemID] [bigint] NOT NULL ,
	[Score] [float] NOT NULL ,
	[IsAveraged] [bit] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tGroupMembers]    Script Date: 11/9/2006 9:37:36 AM ******/
CREATE TABLE [dbo].[tGroupMembers] (
	[GroupID] [bigint] NOT NULL ,
	[NetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Status] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tRegradeRequests]    Script Date: 11/9/2006 9:37:36 AM ******/
CREATE TABLE [dbo].[tRegradeRequests] (
	[RequestID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[GroupID] [bigint] NOT NULL ,
	[NetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Request] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Status] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[DateEntered] [datetime] NOT NULL ,
	[CommentID] [bigint] NULL ,
	[SubProblemID] [bigint] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tSubmittedFiles]    Script Date: 11/9/2006 9:37:37 AM ******/
CREATE TABLE [dbo].[tSubmittedFiles] (
	[SubmittedFileID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[SubmissionID] [bigint] NOT NULL ,
	[OriginalGroupID] [bigint] NOT NULL ,
	[GroupID] [bigint] NOT NULL ,
	[FileName] [varchar] (50) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL ,
	[FileDate] [datetime] NOT NULL ,
	[FileType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[NetID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[FileSize] [int] NOT NULL ,
	[MD5] [char] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LateSubmission] [bit] NOT NULL ,
	[Path] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tCommentFiles]    Script Date: 11/9/2006 9:37:37 AM ******/
CREATE TABLE [dbo].[tCommentFiles] (
	[CommentFileID] [bigint] IDENTITY (1, 1) NOT NULL ,
	[CommentID] [bigint] NOT NULL ,
	[FileName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Path] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [cmsdb_java].[JMS_MESSAGES] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[MESSAGEID],
		[DESTINATION]
	)  ON [PRIMARY] 
GO

ALTER TABLE [cmsdb_j2ee].[JMS_MESSAGES] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[MESSAGEID],
		[DESTINATION]
	)  ON [PRIMARY] 
GO

ALTER TABLE [cmsdb_java].[JMS_ROLES] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[USERID],
		[ROLEID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [cmsdb_j2ee].[JMS_ROLES] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[USERID],
		[ROLEID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[JMS_SUBSCRIPTIONS] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[CLIENTID],
		[SUBNAME]
	)  ON [PRIMARY] 
GO

ALTER TABLE [cmsdb_java].[JMS_SUBSCRIPTIONS] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[CLIENTID],
		[SUBNAME]
	)  ON [PRIMARY] 
GO

ALTER TABLE [cmsdb_j2ee].[JMS_SUBSCRIPTIONS] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[CLIENTID],
		[SUBNAME]
	)  ON [PRIMARY] 
GO

ALTER TABLE [cmsdb_java].[JMS_USERS] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[USERID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [cmsdb_j2ee].[JMS_USERS] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[USERID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [cmsdb_j2ee].[TIMERS] WITH NOCHECK ADD 
	CONSTRAINT [TIMERS_PK] PRIMARY KEY  CLUSTERED 
	(
		[TIMERID],
		[TARGETID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tFileTypes] WITH NOCHECK ADD 
	CONSTRAINT [PK_tFileType] PRIMARY KEY  CLUSTERED 
	(
		[FileType]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tLogs] WITH NOCHECK ADD 
	CONSTRAINT [PK_tLogs] PRIMARY KEY  CLUSTERED 
	(
		[LogID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tSemesters] WITH NOCHECK ADD 
	CONSTRAINT [PK_tSemesters] PRIMARY KEY  CLUSTERED 
	(
		[SemesterID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tUser] WITH NOCHECK ADD 
	CONSTRAINT [PK_Person] PRIMARY KEY  CLUSTERED 
	(
		[NetID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tCmsAdmin] WITH NOCHECK ADD 
	CONSTRAINT [PK_tCmsAdmin] PRIMARY KEY  CLUSTERED 
	(
		[NetID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tCourse] WITH NOCHECK ADD 
	CONSTRAINT [PK_Course] PRIMARY KEY  CLUSTERED 
	(
		[CourseID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tSystemProperties] WITH NOCHECK ADD 
	CONSTRAINT [PK_tSystemProperties] PRIMARY KEY  CLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tAnnouncement] WITH NOCHECK ADD 
	CONSTRAINT [PK_tAnnouncement] PRIMARY KEY  CLUSTERED 
	(
		[AnnouncementID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tAssignment] WITH NOCHECK ADD 
	CONSTRAINT [IX_tAssignment] UNIQUE  CLUSTERED 
	(
		[CourseID],
		[AssignmentID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tCategori] WITH NOCHECK ADD 
	CONSTRAINT [PK_tCategori] PRIMARY KEY  CLUSTERED 
	(
		[CategoryID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tEmails] WITH NOCHECK ADD 
	CONSTRAINT [PK_tEmails] PRIMARY KEY  CLUSTERED 
	(
		[EmailID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tStaff] WITH NOCHECK ADD 
	CONSTRAINT [PK_Staff] PRIMARY KEY  CLUSTERED 
	(
		[NetID],
		[CourseID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tStudent] WITH NOCHECK ADD 
	CONSTRAINT [PK_Student] PRIMARY KEY  CLUSTERED 
	(
		[CourseID],
		[NetID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tAssignmentItems] WITH NOCHECK ADD 
	CONSTRAINT [PK_AssignmentItems] PRIMARY KEY  CLUSTERED 
	(
		[AssignmentItemID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tCategoryCol] WITH NOCHECK ADD 
	CONSTRAINT [PK_tCategoryCol] PRIMARY KEY  CLUSTERED 
	(
		[ColID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tCategoryRow] WITH NOCHECK ADD 
	CONSTRAINT [PK_tCategoryRow] PRIMARY KEY  CLUSTERED 
	(
		[RowID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tGrades] WITH NOCHECK ADD 
	CONSTRAINT [PK_Grades] PRIMARY KEY  CLUSTERED 
	(
		[GradeID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tOldAnnouncement] WITH NOCHECK ADD 
	CONSTRAINT [PK_tOldAnnouncement] PRIMARY KEY  CLUSTERED 
	(
		[OldAnnouncementID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tRequiredSubmissions] WITH NOCHECK ADD 
	CONSTRAINT [PK_tRequiredSubmissions] PRIMARY KEY  CLUSTERED 
	(
		[SubmissionID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tSolutionFiles] WITH NOCHECK ADD 
	CONSTRAINT [PK_tSolutionFiles] PRIMARY KEY  CLUSTERED 
	(
		[SolutionFileID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tSubProblems] WITH NOCHECK ADD 
	CONSTRAINT [PK_SubAssignments] PRIMARY KEY  CLUSTERED 
	(
		[SubProblemID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tTimeSlot] WITH NOCHECK ADD 
	CONSTRAINT [PK_tTimeSlot] PRIMARY KEY  CLUSTERED 
	(
		[TimeSlotID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tAssignmentFiles] WITH NOCHECK ADD 
	CONSTRAINT [PK_tAssignmentSrcFiles] PRIMARY KEY  CLUSTERED 
	(
		[AssignmentFileID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tCtgContents] WITH NOCHECK ADD 
	CONSTRAINT [PK_tCtgContents] PRIMARY KEY  CLUSTERED 
	(
		[ContentID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tGroups] WITH NOCHECK ADD 
	CONSTRAINT [PK_Groups] PRIMARY KEY  CLUSTERED 
	(
		[GroupID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tRequiredFileTypes] WITH NOCHECK ADD 
	CONSTRAINT [PK_tRequiredFileTypes] PRIMARY KEY  CLUSTERED 
	(
		[FileType],
		[SubmissionID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tCategoryFiles] WITH NOCHECK ADD 
	CONSTRAINT [PK_tCategoryFiles] PRIMARY KEY  CLUSTERED 
	(
		[CategoryFileID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tComments] WITH NOCHECK ADD 
	CONSTRAINT [PK_Comments] PRIMARY KEY  CLUSTERED 
	(
		[CommentID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tGroupAssignedTo] WITH NOCHECK ADD 
	CONSTRAINT [PK_tGroupAssignedTo] PRIMARY KEY  CLUSTERED 
	(
		[GroupID],
		[SubProblemID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tGroupGrades] WITH NOCHECK ADD 
	CONSTRAINT [PK_tGroupGrades] PRIMARY KEY  CLUSTERED 
	(
		[GroupID],
		[SubproblemID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tGroupMembers] WITH NOCHECK ADD 
	CONSTRAINT [PK_tGroupMembers] PRIMARY KEY  CLUSTERED 
	(
		[GroupID],
		[NetID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tRegradeRequests] WITH NOCHECK ADD 
	CONSTRAINT [PK_tRegradeRequests] PRIMARY KEY  CLUSTERED 
	(
		[RequestID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tSubmittedFiles] WITH NOCHECK ADD 
	CONSTRAINT [PK_SubmittedFiles] PRIMARY KEY  CLUSTERED 
	(
		[SubmittedFileID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tCommentFiles] WITH NOCHECK ADD 
	CONSTRAINT [PK_tCommentFiles] PRIMARY KEY  CLUSTERED 
	(
		[CommentFileID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tSemesters] ADD 
	CONSTRAINT [DF_tSemesters_Hidden] DEFAULT (0) FOR [Hidden]
GO

ALTER TABLE [dbo].[tCourse] ADD 
	CONSTRAINT [DF_tCourse_Description] DEFAULT (null) FOR [Description],
	CONSTRAINT [DF_tCourse_SemesterID] DEFAULT (1) FOR [SemesterID],
	CONSTRAINT [DF_tCourse_ShowFinalGrade] DEFAULT (0) FOR [ShowFinalGrade],
	CONSTRAINT [DF_tCourse_ShowTotalScore] DEFAULT (0) FOR [ShowTotalScore],
	CONSTRAINT [DF_tCourse_ShowAssignWeights] DEFAULT (0) FOR [ShowAssignWeights],
	CONSTRAINT [DF_tCourse_FreezeCourse] DEFAULT (0) FOR [FreezeCourse],
	CONSTRAINT [DF_tCourse_FileCounter] DEFAULT (1) FOR [FileCounter],
	CONSTRAINT [DF_tCourse_Hidden] DEFAULT (0) FOR [Hidden],
	CONSTRAINT [DF_tCourse_CourseGuestAccess] DEFAULT (0) FOR [CourseGuestAccess],
	CONSTRAINT [DF_tCourse_AssignGuestAccess] DEFAULT (0) FOR [AssignGuestAccess],
	CONSTRAINT [DF_tCourse_AnnounceGuestAccess] DEFAULT (0) FOR [AnnounceGuestAccess],
	CONSTRAINT [DF_tCourse_SolutionGuestAccess] DEFAULT (0) FOR [SolutionGuestAccess],
	CONSTRAINT [DF_tCourse_CourseCCAccess] DEFAULT (0) FOR [CourseCCAccess],
	CONSTRAINT [DF_tCourse_AssignCCAccess] DEFAULT (0) FOR [AssignCCAccess],
	CONSTRAINT [DF_tCourse_AnnounceCCAccess] DEFAULT (0) FOR [AnnounceCCAccess],
	CONSTRAINT [DF_tCourse_SolutionCCAccess] DEFAULT (0) FOR [SolutionCCAccess]
GO

ALTER TABLE [dbo].[tSystemProperties] ADD 
	CONSTRAINT [DF_tSystemProperties_PropertySetName] DEFAULT (1) FOR [ID],
	CONSTRAINT [DF_tSystemProperties_CurrentSemester] DEFAULT (1) FOR [CurrentSemester],
	CONSTRAINT [DF_tSystemProperties_DebugMode] DEFAULT (1) FOR [DebugMode]
GO

ALTER TABLE [dbo].[tAnnouncement] ADD 
	CONSTRAINT [DF_tAnnouncement_Hidden] DEFAULT (0) FOR [Hidden]
GO

ALTER TABLE [dbo].[tAssignment] ADD 
	CONSTRAINT [DF_tAssignment_Status] DEFAULT ('Hidden') FOR [Status],
	CONSTRAINT [DF_tAssignment_AllowLate] DEFAULT (0) FOR [AllowLate],
	CONSTRAINT [DF_tAssignment_ShowStats] DEFAULT (0) FOR [ShowStats],
	CONSTRAINT [DF_tAssignment_ShowSolution] DEFAULT (0) FOR [ShowSolution],
	CONSTRAINT [DF_tAssignment_AssignedGraders] DEFAULT (0) FOR [AssignedGraders],
	CONSTRAINT [DF_tAssignment_AssignedGroups] DEFAULT (0) FOR [AssignedGroups],
	CONSTRAINT [DF_tAssignment_Scheduled] DEFAULT (0) FOR [Scheduled],
	CONSTRAINT [DF_tAssignment_Hidden] DEFAULT (0) FOR [Hidden],
	CONSTRAINT [PK_Assignment] PRIMARY KEY  NONCLUSTERED 
	(
		[AssignmentID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tCategori] ADD 
	CONSTRAINT [DF_tCategori_NumShow] DEFAULT (10) FOR [NumShowContents]
GO

ALTER TABLE [dbo].[tStaff] ADD 
	CONSTRAINT [DF_tStaff_AdminPriv] DEFAULT (0) FOR [AdminPriv],
	CONSTRAINT [DF_tStaff_GroupsPriv] DEFAULT (0) FOR [GroupsPriv],
	CONSTRAINT [DF_tStaff_GradesPriv] DEFAULT (0) FOR [GradesPriv],
	CONSTRAINT [DF_tStaff_AssignmentsPriv] DEFAULT (0) FOR [AssignmentsPriv],
	CONSTRAINT [DF_tStaff_Status] DEFAULT ('Active') FOR [Status],
	CONSTRAINT [DF_tStaff_EmailNewAssign] DEFAULT (0) FOR [EmailNewAssign],
	CONSTRAINT [DF_tStaff_EmailDueDate] DEFAULT (0) FOR [EmailDueDate],
	CONSTRAINT [DF_tStaff_EmailAssignedTo] DEFAULT (0) FOR [EmailAssignedTo],
	CONSTRAINT [DF_tStaff_EmailRequest] DEFAULT (0) FOR [EmailRequest],
	CONSTRAINT [DF_tStaff_EmailFinalGrade] DEFAULT (0) FOR [EmailFinalGrade]
GO

ALTER TABLE [dbo].[tStudent] ADD 
	CONSTRAINT [DF_tStudent_Status] DEFAULT ('Enrolled') FOR [Status],
	CONSTRAINT [DF_tStudent_EmailNewAssign] DEFAULT (0) FOR [EmailNewAssign],
	CONSTRAINT [DF_tStudent_EmailDueDate] DEFAULT (0) FOR [EmailDueDate],
	CONSTRAINT [DF_tStudent_EmailGroupInvites] DEFAULT (0) FOR [EmailGroupInvites],
	CONSTRAINT [DF_tStudent_EmailNewGrade] DEFAULT (0) FOR [EmailNewGrade],
	CONSTRAINT [DF_tStudent_EmailRegrade] DEFAULT (0) FOR [EmailRegrade],
	CONSTRAINT [DF_tStudent_EmailFileSubmit] DEFAULT (0) FOR [EmailFileSubmit],
	CONSTRAINT [DF_tStudent_EmailFinalGrade] DEFAULT (0) FOR [EmailFinalGrade],
	CONSTRAINT [DF_tStudent_EmailTimeSlot] DEFAULT (0) FOR [EmailTimeSlot]
GO

ALTER TABLE [dbo].[tAssignmentItems] ADD 
	CONSTRAINT [DF_AssignmentItems_Hidden] DEFAULT (0) FOR [Hidden]
GO

ALTER TABLE [dbo].[tCategoryCol] ADD 
	CONSTRAINT [DF_tCategoryCol_Removed] DEFAULT (0) FOR [Removed]
GO

ALTER TABLE [dbo].[tCategoryRow] ADD 
	CONSTRAINT [DF_tCategoryRow_Hidden] DEFAULT (0) FOR [Hidden]
GO

ALTER TABLE [dbo].[tRequiredSubmissions] ADD 
	CONSTRAINT [DF_tRequiredSubmissions_Hidden] DEFAULT (0) FOR [Hidden]
GO

ALTER TABLE [dbo].[tSolutionFiles] ADD 
	CONSTRAINT [DF_tSolutionFiles_Hidden] DEFAULT (0) FOR [Hidden]
GO

ALTER TABLE [dbo].[tSubProblems] ADD 
	CONSTRAINT [DF_tSubAssignments_Status] DEFAULT (0) FOR [Hidden]
GO

ALTER TABLE [dbo].[tTimeSlot] ADD 
	CONSTRAINT [DF_tTimeSlot_Population] DEFAULT (0) FOR [Population],
	CONSTRAINT [DF_tTimeSlot_Hidden] DEFAULT (0) FOR [Hidden]
GO

ALTER TABLE [dbo].[tAssignmentFiles] ADD 
	CONSTRAINT [DF_tAssignmentSrcFiles_Hidden] DEFAULT (0) FOR [Hidden]
GO

ALTER TABLE [dbo].[tGroups] ADD 
	CONSTRAINT [DF_tGroups_Extension] DEFAULT (0) FOR [Extension],
	CONSTRAINT [DF_tGroups_FileCounter] DEFAULT (1) FOR [FileCounter]
GO

ALTER TABLE [dbo].[tComments] ADD 
	CONSTRAINT [DF_tComments_Hidden] DEFAULT (0) FOR [Hidden]
GO

ALTER TABLE [dbo].[tGroupMembers] ADD 
	CONSTRAINT [DF_tGroupMembers_Status] DEFAULT ('Active') FOR [Status]
GO

ALTER TABLE [dbo].[tRegradeRequests] ADD 
	CONSTRAINT [DF_tRegradeRequests_Status] DEFAULT ('Pending') FOR [Status]
GO

ALTER TABLE [dbo].[tSubmittedFiles] ADD 
	CONSTRAINT [DF_tSubmittedFiles_LateSubmission] DEFAULT (0) FOR [LateSubmission]
GO

ALTER TABLE [dbo].[tCmsAdmin] ADD 
	CONSTRAINT [FK_tCmsAdmin_tUser] FOREIGN KEY 
	(
		[NetID]
	) REFERENCES [dbo].[tUser] (
		[NetID]
	)
GO

ALTER TABLE [dbo].[tCourse] ADD 
	CONSTRAINT [FK_tCourse_tSemesters] FOREIGN KEY 
	(
		[SemesterID]
	) REFERENCES [dbo].[tSemesters] (
		[SemesterID]
	)
GO

ALTER TABLE [dbo].[tLogDetails] ADD 
	CONSTRAINT [FK_tLogDetails_tLogs] FOREIGN KEY 
	(
		[LogID]
	) REFERENCES [dbo].[tLogs] (
		[LogID]
	)
GO

ALTER TABLE [dbo].[tSystemProperties] ADD 
	CONSTRAINT [FK_tSystemProperties_tSemesters] FOREIGN KEY 
	(
		[CurrentSemester]
	) REFERENCES [dbo].[tSemesters] (
		[SemesterID]
	)
GO

ALTER TABLE [dbo].[tAnnouncement] ADD 
	CONSTRAINT [FK_tAnnouncement_tCourse] FOREIGN KEY 
	(
		[CourseID]
	) REFERENCES [dbo].[tCourse] (
		[CourseID]
	)
GO

ALTER TABLE [dbo].[tAssignment] ADD 
	CONSTRAINT [FK_tAssignment_tCourse] FOREIGN KEY 
	(
		[CourseID]
	) REFERENCES [dbo].[tCourse] (
		[CourseID]
	)
GO

ALTER TABLE [dbo].[tCategori] ADD 
	CONSTRAINT [FK_tCategori_tCourse] FOREIGN KEY 
	(
		[CourseID]
	) REFERENCES [dbo].[tCourse] (
		[CourseID]
	)
GO

ALTER TABLE [dbo].[tEmails] ADD 
	CONSTRAINT [FK_tEmails_tCourse] FOREIGN KEY 
	(
		[CourseID]
	) REFERENCES [dbo].[tCourse] (
		[CourseID]
	),
	CONSTRAINT [FK_tEmails_tUser] FOREIGN KEY 
	(
		[Sender]
	) REFERENCES [dbo].[tUser] (
		[NetID]
	)
GO

ALTER TABLE [dbo].[tStaff] ADD 
	CONSTRAINT [FK_tStaff_tCourse] FOREIGN KEY 
	(
		[CourseID]
	) REFERENCES [dbo].[tCourse] (
		[CourseID]
	),
	CONSTRAINT [FK_tStaff_tUser] FOREIGN KEY 
	(
		[NetID]
	) REFERENCES [dbo].[tUser] (
		[NetID]
	)
GO

ALTER TABLE [dbo].[tStudent] ADD 
	CONSTRAINT [FK_tStudent_tCourse] FOREIGN KEY 
	(
		[CourseID]
	) REFERENCES [dbo].[tCourse] (
		[CourseID]
	),
	CONSTRAINT [FK_tStudent_tUser] FOREIGN KEY 
	(
		[NetID]
	) REFERENCES [dbo].[tUser] (
		[NetID]
	)
GO

ALTER TABLE [dbo].[tAssignmentItems] ADD 
	CONSTRAINT [FK_AssignmentItems_tAssignment] FOREIGN KEY 
	(
		[AssignmentID]
	) REFERENCES [dbo].[tAssignment] (
		[AssignmentID]
	)
GO

ALTER TABLE [dbo].[tCategoryCol] ADD 
	CONSTRAINT [FK_tCategoryCol_tCategori] FOREIGN KEY 
	(
		[CategoryID]
	) REFERENCES [dbo].[tCategori] (
		[CategoryID]
	)
GO

ALTER TABLE [dbo].[tCategoryRow] ADD 
	CONSTRAINT [FK_tCategoryRow_tCategori] FOREIGN KEY 
	(
		[CategoryID]
	) REFERENCES [dbo].[tCategori] (
		[CategoryID]
	)
GO

ALTER TABLE [dbo].[tGrades] ADD 
	CONSTRAINT [FK_tGrades_tAssignment] FOREIGN KEY 
	(
		[AssignmentID]
	) REFERENCES [dbo].[tAssignment] (
		[AssignmentID]
	),
	CONSTRAINT [FK_tGrades_tUser] FOREIGN KEY 
	(
		[NetID]
	) REFERENCES [dbo].[tUser] (
		[NetID]
	)
GO

ALTER TABLE [dbo].[tOldAnnouncement] ADD 
	CONSTRAINT [FK_tOldAnnouncement_tAnnouncement] FOREIGN KEY 
	(
		[AnnouncementID]
	) REFERENCES [dbo].[tAnnouncement] (
		[AnnouncementID]
	)
GO

ALTER TABLE [dbo].[tRequiredSubmissions] ADD 
	CONSTRAINT [FK_tRequiredSubmissions_tAssignment] FOREIGN KEY 
	(
		[AssignmentID]
	) REFERENCES [dbo].[tAssignment] (
		[AssignmentID]
	)
GO

ALTER TABLE [dbo].[tSolutionFiles] ADD 
	CONSTRAINT [FK_tSolutionFiles_tAssignment] FOREIGN KEY 
	(
		[AssignmentID]
	) REFERENCES [dbo].[tAssignment] (
		[AssignmentID]
	)
GO

ALTER TABLE [dbo].[tSubProblems] ADD 
	CONSTRAINT [FK_tSubProblems_tAssignment] FOREIGN KEY 
	(
		[AssignmentID]
	) REFERENCES [dbo].[tAssignment] (
		[AssignmentID]
	)
GO

ALTER TABLE [dbo].[tTimeSlot] ADD 
	CONSTRAINT [FK_tTimeSlot_tAssignment] FOREIGN KEY 
	(
		[AssignmentID]
	) REFERENCES [dbo].[tAssignment] (
		[AssignmentID]
	),
	CONSTRAINT [FK_tTimeSlot_tUser] FOREIGN KEY 
	(
		[StaffNetID]
	) REFERENCES [dbo].[tUser] (
		[NetID]
	)
GO

ALTER TABLE [dbo].[tAssignmentFiles] ADD 
	CONSTRAINT [FK_tAssignmentFiles_AssignmentItems] FOREIGN KEY 
	(
		[AssignmentItemID]
	) REFERENCES [dbo].[tAssignmentItems] (
		[AssignmentItemID]
	)
GO

ALTER TABLE [dbo].[tCtgContents] ADD 
	CONSTRAINT [FK_tCtgContents_tCategoryCol] FOREIGN KEY 
	(
		[ColID]
	) REFERENCES [dbo].[tCategoryCol] (
		[ColID]
	),
	CONSTRAINT [FK_tCtgContents_tCategoryRow] FOREIGN KEY 
	(
		[RowID]
	) REFERENCES [dbo].[tCategoryRow] (
		[RowID]
	)
GO

ALTER TABLE [dbo].[tGroups] ADD 
	CONSTRAINT [FK_tGroups_tAssignment] FOREIGN KEY 
	(
		[AssignmentID]
	) REFERENCES [dbo].[tAssignment] (
		[AssignmentID]
	),
	CONSTRAINT [FK_tGroups_tTimeSlot] FOREIGN KEY 
	(
		[TimeSlotID]
	) REFERENCES [dbo].[tTimeSlot] (
		[TimeSlotID]
	)
GO

ALTER TABLE [dbo].[tRequiredFileTypes] ADD 
	CONSTRAINT [FK_tRequiredFileTypes_tRequiredSubmissions] FOREIGN KEY 
	(
		[SubmissionID]
	) REFERENCES [dbo].[tRequiredSubmissions] (
		[SubmissionID]
	)
GO

ALTER TABLE [dbo].[tCategoryFiles] ADD 
	CONSTRAINT [FK_tCategoryFiles_tCtgContents] FOREIGN KEY 
	(
		[ContentID]
	) REFERENCES [dbo].[tCtgContents] (
		[ContentID]
	)
GO

ALTER TABLE [dbo].[tComments] ADD 
	CONSTRAINT [FK_tComments_tGroups] FOREIGN KEY 
	(
		[GroupID]
	) REFERENCES [dbo].[tGroups] (
		[GroupID]
	)
GO

ALTER TABLE [dbo].[tGroupAssignedTo] ADD 
	CONSTRAINT [FK_tGroupAssignedTo_tGroups] FOREIGN KEY 
	(
		[GroupID]
	) REFERENCES [dbo].[tGroups] (
		[GroupID]
	)
GO

ALTER TABLE [dbo].[tGroupGrades] ADD 
	CONSTRAINT [FK_tGroupGrades_tGroups] FOREIGN KEY 
	(
		[GroupID]
	) REFERENCES [dbo].[tGroups] (
		[GroupID]
	)
GO

ALTER TABLE [dbo].[tGroupMembers] ADD 
	CONSTRAINT [FK_tGroupMembers_tGroups] FOREIGN KEY 
	(
		[GroupID]
	) REFERENCES [dbo].[tGroups] (
		[GroupID]
	),
	CONSTRAINT [FK_tGroupMembers_tUser] FOREIGN KEY 
	(
		[NetID]
	) REFERENCES [dbo].[tUser] (
		[NetID]
	)
GO

ALTER TABLE [dbo].[tRegradeRequests] ADD 
	CONSTRAINT [FK_tRegradeRequests_tGroups] FOREIGN KEY 
	(
		[GroupID]
	) REFERENCES [dbo].[tGroups] (
		[GroupID]
	)
GO

ALTER TABLE [dbo].[tSubmittedFiles] ADD 
	CONSTRAINT [FK_tSubmittedFiles_tGroups] FOREIGN KEY 
	(
		[GroupID]
	) REFERENCES [dbo].[tGroups] (
		[GroupID]
	),
	CONSTRAINT [FK_tSubmittedFiles_tGroups1] FOREIGN KEY 
	(
		[OriginalGroupID]
	) REFERENCES [dbo].[tGroups] (
		[GroupID]
	),
	CONSTRAINT [FK_tSubmittedFiles_tRequiredSubmissions] FOREIGN KEY 
	(
		[SubmissionID]
	) REFERENCES [dbo].[tRequiredSubmissions] (
		[SubmissionID]
	)
GO

ALTER TABLE [dbo].[tCommentFiles] ADD 
	CONSTRAINT [FK_tCommentFiles_tComments] FOREIGN KEY 
	(
		[CommentID]
	) REFERENCES [dbo].[tComments] (
		[CommentID]
	)
GO


exec sp_addextendedproperty N'MS_Description', N'''Date'' seems to be a reserved word', N'user', N'dbo', N'table', N'tLogs', N'column', N'Dte'
GO
exec sp_addextendedproperty N'MS_Description', N'see codes in LogBean', N'user', N'dbo', N'table', N'tLogs', N'column', N'LogType'


GO


exec sp_addextendedproperty N'MS_Description', N'if not null, deadline for *students* to change timeslots', N'user', N'dbo', N'table', N'tAssignment', N'column', N'ScheduleLockTime'
GO
exec sp_addextendedproperty N'MS_Description', N'Hidden | Open | Graded', N'user', N'dbo', N'table', N'tAssignment', N'column', N'Status'


GO


exec sp_addextendedproperty N'MS_Description', N'1 - Staff, 2 - Students, 3 - Both', N'user', N'dbo', N'table', N'tEmails', N'column', N'Recipient'


GO


exec sp_addextendedproperty N'MS_Description', null, N'user', N'dbo', N'table', N'tStaff', N'column', N'EmailRequest'
GO
exec sp_addextendedproperty N'MS_Description', N'Active | Inactive (Inactive means have no permissions)', N'user', N'dbo', N'table', N'tStaff', N'column', N'Status'


GO


exec sp_addextendedproperty N'MS_Description', N'null => use tCourse.Code', N'user', N'dbo', N'table', N'tStudent', N'column', N'CourseNum'
GO
exec sp_addextendedproperty N'MS_Description', N'Enrolled | Dropped', N'user', N'dbo', N'table', N'tStudent', N'column', N'Status'


GO


exec sp_addextendedproperty N'MS_Description', N'hidden columns are visible when editing/viewing layout but not on the course home page', N'user', N'dbo', N'table', N'tCategoryCol', N'column', N'Hidden'
GO
exec sp_addextendedproperty N'MS_Description', N'removed columns are never shown when the category is displayed', N'user', N'dbo', N'table', N'tCategoryCol', N'column', N'Removed'


GO


exec sp_addextendedproperty N'MS_Description', N'0=open; 1=Hidden', N'user', N'dbo', N'table', N'tAssignmentFiles', N'column', N'Hidden'


GO


exec sp_addextendedproperty N'MS_Description', N'text to show for hyperlinks', N'user', N'dbo', N'table', N'tCtgContents', N'column', N'LinkName'
GO
exec sp_addextendedproperty N'MS_Description', N'an index for user-defined sorting of rows; 0 is not a valid value', N'user', N'dbo', N'table', N'tCtgContents', N'column', N'Number'
GO
exec sp_addextendedproperty N'MS_Description', N'target URL for hyperlinks; text for text contents', N'user', N'dbo', N'table', N'tCtgContents', N'column', N'Text'


GO


exec sp_addextendedproperty N'MS_Description', N'0 = no extension; 1 = extension', N'user', N'dbo', N'table', N'tGroups', N'column', N'Extension'
GO
exec sp_addextendedproperty N'MS_Description', N'Total of submissions and comment files', N'user', N'dbo', N'table', N'tGroups', N'column', N'FileCounter'


GO


exec sp_addextendedproperty N'MS_Description', N'file title & extension', N'user', N'dbo', N'table', N'tCategoryFiles', N'column', N'FileName'
GO
exec sp_addextendedproperty N'MS_Description', N'filesystem path w/o title & extension', N'user', N'dbo', N'table', N'tCategoryFiles', N'column', N'Path'


GO


exec sp_addextendedproperty N'MS_Description', N'Active; Dropped; Invited; Rejected', N'user', N'dbo', N'table', N'tGroupMembers', N'column', N'Status'


GO


exec sp_addextendedproperty N'MS_Description', N'file extension', N'user', N'dbo', N'table', N'tSubmittedFiles', N'column', N'FileType'
GO
exec sp_addextendedproperty N'MS_Description', N'The GroupID of the group to which this file belongs. (Not necessarily the original submitting group, due to group splits)', N'user', N'dbo', N'table', N'tSubmittedFiles', N'column', N'GroupID'
GO
exec sp_addextendedproperty N'MS_Description', N'The GroupID of the group which originally submitted the file.  This is the GroupID used in the file''s path in the file system.', N'user', N'dbo', N'table', N'tSubmittedFiles', N'column', N'OriginalGroupID'
GO
exec sp_addextendedproperty N'MS_Description', N'The file''s path on the file system is \Submissions\<CourseID>\<AssignmentID>\<OriginalGroupID>\<FileCounter>\<SubmissionID>[.FileType].  Note that this path will be system specific based on the OS which wrote it.  Not to be used to find the file.', N'user', N'dbo', N'table', N'tSubmittedFiles', N'column', N'Path'
GO
exec sp_addextendedproperty N'MS_Description', null, N'user', N'dbo', N'table', N'tSubmittedFiles', N'column', N'SubmittedFileID'


GO

