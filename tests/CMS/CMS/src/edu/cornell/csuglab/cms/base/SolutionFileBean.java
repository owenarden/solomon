/*
 * Created on Oct 28, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.FinderException;

/**
 *
 * <!-- begin-user-doc --> You can insert your documentation for '<em><b>SolutionFileBean</b></em>'. <!-- end-user-doc --> *
 <!--  begin-lomboz-definition -->
 <?xml version="1.0" encoding="UTF-8"?>
 <lomboz:EJB xmlns:j2ee="http://java.sun.com/xml/ns/j2ee" xmlns:lomboz="http://lomboz.objectlearn.com/xml/lomboz">
 <lomboz:entity>
 <lomboz:entityEjb>
 <j2ee:display-name>SolutionFile</j2ee:display-name>
 <j2ee:ejb-name>SolutionFile</j2ee:ejb-name>
 <j2ee:ejb-class>edu.cornell.csuglab.cms.base.SolutionFileBean</j2ee:ejb-class>
 <j2ee:persistence-type>Bean</j2ee:persistence-type>
 <j2ee:cmp-version>2.x</j2ee:cmp-version>
 <j2ee:abstract-schema-name>mySchema</j2ee:abstract-schema-name>
 </lomboz:entityEjb>
 <lomboz:tableName></lomboz:tableName>
 <lomboz:dataSourceName></lomboz:dataSourceName>
 </lomboz:entity>
 </lomboz:EJB>
 <!--  end-lomboz-definition -->
 *
 * <!-- begin-xdoclet-definition -->



/**
 * A solution file represents the solution to an assignment.  There is at most
 * one un-hidden solution file for a given assignment at any time.  Adding a new
 * assignment file, or restoring an old one, has the side effect of causing all
 * the other solution files to be deleted/hidden.
 * 
 * @ejb.bean name="SolutionFile"
 *	jndi-name="SolutionFile"
 *	type="BMP"
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.SolutionFileDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.SolutionFileDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 * <!-- end-xdoclet-defintion -->
 * @generated
 **/
public abstract class SolutionFileBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long solutionFileID,assignmentID;
	private boolean hidden;
	private String fileName, path;
	
	/**
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getSolutionFileID() {
		return solutionFileID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param solutionFileID
	 */
	public void setSolutionFileID(long solutionFileID) {
		this.solutionFileID = solutionFileID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getAssignmentID() {
		return assignmentID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param assignmentID
	 */
	public void setAssignmentID(long assignmentID) {
		this.assignmentID = assignmentID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getFileName() {
		return fileName;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileName
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public boolean getHidden() {
		return hidden;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param hidden
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getPath() {
		return path;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileCounter
	 */
	public void setPath(String path) {
		this.path = path;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @return
	 * FIXME do we need this? see cms.util.FileUtil
	public String getFilePath() {
		String result = Util.FILES_DIR;
		String SLASH = Util.SLASH;
		result = result + SLASH + "SolutionFiles" + SLASH;
		result = result + getSemesterID() + SLASH + getCourseID() + SLASH;
		result = result + getFileCounter() + SLASH + getFileName();
		return result;
	}*/
	
	/**
	 * @ejb.interface-method view-type="local"
	 */
	public SolutionFileData getSolutionFileData() {
		return new SolutionFileData(getSolutionFileID(), getAssignmentID(), getFileName(), getHidden(),
				getPath());
	}
	
	/**
	 * @param pk
	 * @return
	 */
	public SolutionFilePK ejbFindByPrimaryKey(SolutionFilePK pk) throws FinderException {
		return null;
	}
	
	/**
	 * 
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public SolutionFilePK ejbFindByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * 
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection ejbFindHiddenByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}

	/**
	 *
	 * <!-- begin-user-doc -->
	 * The  ejbCreate method.
	 * <!-- end-user-doc -->
	 *
	 * <!-- begin-xdoclet-definition --> 
	 * @ejb.create-method view-type="local"
	 * <!-- end-xdoclet-definition --> 
	 * @generated
	 */
	public SolutionFilePK ejbCreate(long assignmentID,
			String fileName, boolean hidden, String path) throws javax.ejb.CreateException {
		setAssignmentID(assignmentID);
		setFileName(fileName);
		setHidden(hidden);
		setPath(path);
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * The container invokes this method immediately after it calls ejbCreate.
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void ejbPostCreate() throws javax.ejb.CreateException {
	}

}
