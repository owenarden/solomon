/*
 * Created on Aug 1, 2004
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.AnswerBean;
import edu.cornell.csuglab.cms.base.AnswerDAO;
import edu.cornell.csuglab.cms.base.AnswerPK;

/**
 * @author Yan
 *  
 */
public class AnswerDAOImpl extends DAOMaster implements AnswerDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AnswerDAO#load(edu.cornell.csuglab.cms.base.AnswerPK,
	 *      edu.cornell.csuglab.cms.base.AnswerBean)
	 */
	public void load(AnswerPK pk, AnswerBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tAnswers where AnswerID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getAnswerID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setAnswerID(rs.getLong("AnswerID"));
				ejb.setAnswerSetID(rs.getLong("AnswerSetID"));
				ejb.setSubProblemID(rs.getLong("SubProblemID"));
				ejb.setText(rs.getString("Text"));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
			}
			throw new EJBException("Row id " + pk.getAnswerID()
					+ " failed to load in tAnswers.", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AnswerDAO#store(edu.cornell.csuglab.cms.base.AnswerBean)
	 */
	public void store(AnswerBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tAnswers set AnswerSetID = ?, SubProblemID = ?, Text = ? WHERE AnswerID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, ejb.getAnswerSetID());
			ps.setLong(2, ejb.getSubProblemID());
			ps.setString(3, ejb.getText());
			ps.setLong(4, ejb.getAnswerID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
				f.printStackTrace();
			}
			throw new EJBException("Row id " + ejb.getAnswerID()
					+ ", failed to store properly", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AnswerDAO#remove(edu.cornell.csuglab.cms.base.AnswerPK)
	 */
	public void remove(AnswerPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AnswerDAO#create(edu.cornell.csuglab.cms.base.AnswerBean)
	 */
	public AnswerPK create(AnswerBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AnswerPK pk = null;
		try {
			conn = jdbcFactory.getConnection();
			String updateQuery = "insert into tAnswers "
					+ "(AnswerSetID, SubProblemID, Text) "
					+ "values " + "(?, ?, ?)";
			String findQuery = "select @@identity as AnswerID from tAnswers";
			ps = conn.prepareStatement(updateQuery);
			ps.setLong(1, ejb.getAnswerSetID());
			ps.setLong(2, ejb.getSubProblemID());
			ps.setString(3, ejb.getText());
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findQuery).executeQuery();
			if (count > 0 && rs.next()) {
				long AnswerID = rs.getLong("AnswerID");
				pk = new AnswerPK(AnswerID);
				ejb.setAnswerID(AnswerID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				e.printStackTrace();
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (Exception f) {
				f.printStackTrace();
			}
			throw new CreateException(e.getMessage());
		}
		return pk;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AnswerDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.AnswerPK)
	 */
	public AnswerPK findByPrimaryKey(AnswerPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select AnswerID from tAnswers where AnswerID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getAnswerID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException(
						"Could not find submitted file with AnswerID = "
								+ pk.getAnswerID());
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (Exception f) {
			}

			/* OLD CODE: (triple-X replaced with XXx)
			 * 
			
			try {
				throw ((FinderException) e); //XXx wtf?
			} catch (ClassCastException x) {
				throw new FinderException("Caught Exception: " + e.getMessage());
			}
			
			 * I think this just turns all exceptions into FinderExceptions,
			 * but I think my code does it cleaner
			 * 
			 * Am I correct in my interpretation?
			 * - Alex, Feb 2008
			 */
			
			// we can only explicitly throw FinderExceptions from this catch block
			if (e instanceof FinderException)
				throw (FinderException) e;
			else
				throw new FinderException("Caught Exception: " + e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AnswerDAO#findByAnswer(long)
	 */
	public AnswerPK findByAnswerID(long answerID) throws FinderException {
		return findByPrimaryKey(new AnswerPK(answerID));
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AnswerDAO#findByAnswerSetID(long)
	 */
	public Collection findByAnswerSetID(long answerSetID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT a.AnswerID FROM tAnswers a " + 
				"WHERE a.AnswerSetID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, answerSetID);
			rs = ps.executeQuery();
			while (rs.next()) {
				long answerID = rs.getLong("AnswerID");
				result.add(new AnswerPK(answerID));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AnswerDAO#findByAnswerSetID(long)
	 */
	public Collection findBySubProblemID(long subProblemID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT a.AnswerID FROM tAnswers a WHERE a.SubProblemID = ?";
			/* This used to be:
			 * "SELECT a.AnswerID FROM tAnswers a INNER JOIN tAnswerSets aset ON a.AnswerSetID = aset.AnswerSetID WHERE aset.SubProblemID = ?";
			 * ...but this doesn't make sense */
			ps = conn.prepareStatement(query);
			ps.setLong(1, subProblemID);
			rs = ps.executeQuery();
			while (rs.next()) {
				long answerID = rs.getLong("AnswerID");
				result.add(new AnswerPK(answerID));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}

}
