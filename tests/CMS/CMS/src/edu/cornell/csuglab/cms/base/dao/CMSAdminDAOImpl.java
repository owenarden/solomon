/*
 * Created on Mar 13, 2005
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.CMSAdminBean;
import edu.cornell.csuglab.cms.base.CMSAdminDAO;
import edu.cornell.csuglab.cms.base.CMSAdminPK;

/**
 * @author Evan
 */
public class CMSAdminDAOImpl extends DAOMaster implements CMSAdminDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see CMSAdminDAO#load(CMSAdminPK, CMSAdminBean)
	 */
	public void load(CMSAdminPK pk, CMSAdminBean ejb)
			throws javax.ejb.EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select NetID, DomainID from tCmsAdmin where NetID = ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, pk.getNetID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setNetID(rs.getString("NetID"));
				ejb.setDomainID(rs.getInt("DomainID"));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see CMSAdminDAO#store(CMSAdminBean)
	 */
	public void store(edu.cornell.csuglab.cms.base.CMSAdminBean ejb)
			throws RuntimeException, javax.ejb.EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "update tCmsAdmin set DomainID = ? where NetID = ?";
			ps = conn.prepareStatement(update);
			ps.setInt(1, ejb.getDomainID());
			ps.setString(2, ejb.getNetID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			}
			catch (Exception f) {}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see CMSAdminDAO#remove(String)
	 */
	public void remove(String netID) throws javax.ejb.RemoveException,
			javax.ejb.EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "delete from tCmsAdmin where NetID = ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, netID);
			ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (Exception e) {
			try {
				e.printStackTrace();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
				f.printStackTrace();
			}
			throw new RemoveException(e.getMessage());
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see CMSAdminDAO#remove(CMSAdminPK)
	 */
	public void remove(CMSAdminPK pk) throws javax.ejb.RemoveException,
			javax.ejb.EJBException {
		remove(pk.getNetID());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see CMSAdminDAO#create(CMSAdminBean)
	 */
	public CMSAdminPK create(CMSAdminBean ejb)
			throws javax.ejb.CreateException, javax.ejb.EJBException {
		Connection conn = null;
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null;
		int count;
		CMSAdminPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String query1 = "insert into tCmsAdmin (NetID,DomainID) values (?,?)";
			String query2 = "select NetID from tCmsAdmin where NetID = ?";
			ps = conn.prepareStatement(query1);
			if (ejb.getNetID() == null)
				ps.setNull(1, java.sql.Types.VARCHAR);
			else
				ps.setString(1, ejb.getNetID());
			ps.setInt(2, ejb.getDomainID());
			count = ps.executeUpdate();
			// make sure the entry was created correctly by searching for it
			ps2 = conn.prepareStatement(query2);
			if (ejb.getNetID() == null)
				ps2.setNull(1, java.sql.Types.VARCHAR);
			else
				ps2.setString(1, ejb.getNetID());
			rs = ps2.executeQuery();
			if (rs.next() && count == 1) {
				result = new CMSAdminPK(rs.getString(1));
				ejb.setNetID(result.getNetID());
			} else
				throw new CreateException("Error in creating tCmsAdmin entry");
			rs.close();
			ps.close();
			ps2.close();
			conn.close();
		} catch (Exception e) {
			try {
				e.printStackTrace();
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (ps2 != null)
					ps2.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
				f.printStackTrace();
			}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see CMSAdminDAO#findByPrimaryKey(CMSAdminPK)
	 */
	public CMSAdminPK findByPrimaryKey(CMSAdminPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select NetID from tCmsAdmin where NetID = ? ";
			ps = conn.prepareStatement(query);
			ps.setString(1, pk.getNetID());
			rs = ps.executeQuery();
			if (!rs.next())
				throw new FinderException("Could not find admin netid "
						+ pk.getNetID());
			rs.close();
			ps.close();
			conn.close();
		} catch (Exception e) {
			try {
				// e.printStackTrace();
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
				f.printStackTrace();
			}
			throw new FinderException("NetID " + pk.getNetID()
					+ "not found in database");
		}
		return pk;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see CMSAdminDAO#findAllAdmins()
	 */
	public Collection findAllAdmins() throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select NetID from tCmsAdmin WHERE DomainID = 1";
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			// dump the data in rs into a Collection of CMSAdminPKs
			Collection result = new ArrayList();
			while (rs.next())
				result.add(new CMSAdminPK(rs.getString(1)));
			rs.close();
			ps.close();
			conn.close();
			return result;
		} catch (Exception e) {
			try {
				e.printStackTrace();
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
				f.printStackTrace();
			}
			throw new FinderException("No admins found in tCmsAdmin");
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see CMSAdminDAO#findAllAdmins()
	 */
	public Collection findAllSubAdmins() throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select NetID from tCmsAdmin";
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			// dump the data in rs into a Collection of CMSAdminPKs
			Collection result = new ArrayList();
			while (rs.next())
				result.add(new CMSAdminPK(rs.getString(1)));
			rs.close();
			ps.close();
			conn.close();
			return result;
		} catch (Exception e) {
			try {
				e.printStackTrace();
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
				f.printStackTrace();
			}
			throw new FinderException("No admins found in tCmsAdmin");
		}
	}
}
