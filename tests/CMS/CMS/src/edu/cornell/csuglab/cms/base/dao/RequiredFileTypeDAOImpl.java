/*
 * Created on Nov 4, 2004
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.*;

import java.util.ArrayList;
import java.util.Collection;
import javax.ejb.*;

import edu.cornell.csuglab.cms.base.*;

/**
 * @author Jon
 *
 */
public class RequiredFileTypeDAOImpl extends DAOMaster implements RequiredFileTypeDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RequiredFileTypeDAO#load(edu.cornell.csuglab.cms.base.RequiredFileTypePK, edu.cornell.csuglab.cms.base.RequiredFileTypeBean)
	 */
	public void load(RequiredFileTypePK pk, RequiredFileTypeBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tRequiredFileTypes "
					+ "where SubmissionID = ? and FileType = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getSubmissionID());
			ps.setString(2, pk.getFileType());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setSubmissionID(rs.getLong("SubmissionID"));
				ejb.setFileType(rs.getString("FileType"));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RequiredFileTypeDAO#store(edu.cornell.csuglab.cms.base.RequiredFileTypeBean)
	 */
	public void store(RequiredFileTypeBean ejb) throws EJBException {

	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RequiredFileTypeDAO#remove(edu.cornell.csuglab.cms.base.RequiredFileTypePK)
	 */
	public void remove(RequiredFileTypePK pk) throws RemoveException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String removeQuery = "delete from tRequiredFileTypes where SubmissionID = ? and FileType = ?";
			ps = conn.prepareStatement(removeQuery);
			ps.setLong(1, pk.getSubmissionID());
			ps.setString(2, pk.getFileType());
			int count;
			count = ps.executeUpdate();
			if (count != 1) {
				throw new RemoveException("Failed to properly delete RequiredFile");
			}
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			} catch (Exception f) {}
			throw new RemoveException(e.getMessage());
		}
	}
	
	public int removeBySubmissionID(long submissionID) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		int count;
		try {
			conn = jdbcFactory.getConnection();
			String query = "delete from tRequiredFileTypes where SubmissionID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, submissionID);
			count = ps.executeUpdate();
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			} catch (Exception f) {}
			throw new EJBException(e);
		}
		return count;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RequiredFileTypeDAO#create(edu.cornell.csuglab.cms.base.RequiredFileTypeBean)
	 */
	public RequiredFileTypePK create(RequiredFileTypeBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		RequiredFileTypePK pk = null;
		try {
			conn = jdbcFactory.getConnection();
			String updateString = "insert into tRequiredFileTypes "
					+ "(SubmissionID, FileType) values (?, ?)";
			ps = conn.prepareStatement(updateString);
			ps.setLong(1, ejb.getSubmissionID());
			if(ejb.getFileType() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getFileType());
			int count = ps.executeUpdate();
			if (count == 1) {
				pk = new RequiredFileTypePK(ejb.getSubmissionID(), ejb.getFileType());
			} else {
				throw new CreateException("Failed to create a new required submission");
			}
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				e.printStackTrace();
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			}
			catch (Exception f) {
				f.printStackTrace();
			}
			throw new CreateException(e.getMessage());
		}
		return pk;

	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RequiredFileTypeDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.RequiredFileTypePK)
	 */
	public RequiredFileTypePK findByPrimaryKey(RequiredFileTypePK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select SubmissionID, FileType from tRequiredFileTypes "
					+ "where SubmissionID = ? and FileType = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, pk.getSubmissionID());
			ps.setString(2, pk.getFileType());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find required file type " + pk.getSubmissionID() + "," +
						pk.getFileType());
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {
			}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RequiredFileTypeDAO#findBySubmissionID(long)
	 */
	public Collection findBySubmissionID(long submissionID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select * from tRequiredFileTypes "
					+ "where SubmissionID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, submissionID);
			rs = ps.executeQuery();
			while (rs.next()) {
				RequiredFileTypePK key = new RequiredFileTypePK(rs.getLong("SubmissionID"), rs.getString("FileType"));
				result.add(key);
			}
			conn.close();
			rs.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

}
