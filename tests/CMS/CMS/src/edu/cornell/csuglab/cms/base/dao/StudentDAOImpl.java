/*
 * Created on Mar 4, 2004
 */

package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.GroupMemberBean;
import edu.cornell.csuglab.cms.base.StudentBean;
import edu.cornell.csuglab.cms.base.StudentDAO;
import edu.cornell.csuglab.cms.base.StudentPK;
import edu.cornell.csuglab.cms.www.util.ResultCache;

/**
 * @author Theodore Chao
 */
public class StudentDAOImpl extends DAOMaster implements StudentDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StudentDAO#create(edu.cornell.csuglab.cms.base.StudentBean)
	 */
	public StudentPK create(StudentBean ejb) throws CreateException, EJBException {
		preCreate("Student", ejb);
		Connection conn = null;
		PreparedStatement ps = null;
		StudentPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "insert into tStudent "
					+ "(courseid, netid) values (?, ?)";
			ps = conn.prepareStatement(createString);
			ps.setLong(1, ejb.getCourseID());
			if(ejb.getUserID() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getUserID());
			ps.executeUpdate();
			result = new StudentPK(ejb.getCourseID(), ejb.getUserID());
			conn.close();
			ps.close();
        } catch (Exception e) {
        	try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StudentDAO#load(edu.cornell.csuglab.cms.base.StudentPK,
	 *      edu.cornell.csuglab.cms.base.StudentBean)
	 */
	public void load(StudentPK pk, StudentBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select * from tStudent where NetID = ? and CourseID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, pk.getUserID());
			ps.setLong(2, pk.getCourseID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setUserID(rs.getString("netid"));
				ejb.setCourseID(rs.getLong("courseid"));
				ejb.setStatus(rs.getString("status"));
				ejb.setFinalGrade(rs.getString("finalgrade"));
				float totalscore = rs.getFloat("TotalScore");
				if (rs.wasNull()) {
				    ejb.setTotalScore(null);
				} else {
				    ejb.setTotalScore(new Float(totalscore));
				}
				ejb.setLecture(rs.getString("Lecture"));
				ejb.setLab(rs.getString("Lab"));
				ejb.setSection(rs.getString("Section"));
				ejb.setCredits(rs.getString("Credits"));
				ejb.setGradeOption(rs.getString("GradeOption"));
				ejb.setDepartment(rs.getString("Department"));
				ejb.setCourseNum(rs.getString("CourseNum"));
				ejb.setEmailNewAssignment(rs.getBoolean("emailnewassign"));
				ejb.setEmailDueDate(rs.getBoolean("emailduedate"));
				ejb.setEmailGroup(rs.getBoolean("emailgroupinvites"));
				ejb.setEmailNewGrade(rs.getBoolean("emailnewgrade"));
				ejb.setEmailRegrade(rs.getBoolean("emailregrade"));
				ejb.setEmailFile(rs.getBoolean("emailfilesubmit"));
				ejb.setEmailFinalGrade(rs.getBoolean("emailfinalgrade"));
				ejb.setEmailTimeSlot(rs.getBoolean("emailtimeslot"));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StudentDAO#remove(edu.cornell.csuglab.cms.base.StudentPK)
	 */
	public void remove(StudentPK pk) throws RemoveException, EJBException {
		preRemove("Student");
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			conn = jdbcFactory.getConnection();
			String query = "delete from tStudent where NetID = ? and CourseID = ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, pk.getUserID());
			ps.setLong(2, pk.getCourseID());
			ps.executeUpdate(); 
			ps.close();
			conn.close();
		}
		catch(Exception e){
			try{
				e.printStackTrace();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new RemoveException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StudentDAO#store(edu.cornell.csuglab.cms.base.StudentBean)
	 */
	public void store(StudentBean ejb) throws EJBException {
		preStore("Student", ejb);
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "update tstudent "
					+ "set status = ?, finalgrade = ?, totalscore = ?, "
					+ "emailnewassign = ?, emailduedate = ?, emailgroupinvites = ?, emailtimeslot = ?, "
					+ "emailnewgrade = ?, emailregrade = ?, emailfilesubmit = ?, emailfinalgrade = ?, "
					+ "lecture = ?, lab = ?, section = ?, credits = ?, GradeOption = ?, department = ?, "
					+ "courseNum = ? "
					+ "where netid = ? and courseid = ?";
			ps = conn.prepareStatement(update);
			int INDEX = 1;
			if(ejb.getStatus() == null) ps.setNull(INDEX++, java.sql.Types.VARCHAR);
			else ps.setString(INDEX++, ejb.getStatus());
			if (ejb.getFinalGrade() == null) {
			    ps.setNull(INDEX++, Types.VARCHAR);
			} else {
			    ps.setString(INDEX++, ejb.getFinalGrade());
			}
			if (ejb.getTotalScore() == null) {
			    ps.setNull(INDEX++, Types.FLOAT);
			} else {
			    ps.setFloat(INDEX++, ejb.getTotalScore().floatValue());
			}
			ps.setBoolean(INDEX++, ejb.getEmailNewAssignment());
			ps.setBoolean(INDEX++, ejb.getEmailDueDate());
			ps.setBoolean(INDEX++, ejb.getEmailGroup());
			ps.setBoolean(INDEX++, ejb.getEmailTimeSlot());
			ps.setBoolean(INDEX++, ejb.getEmailNewGrade());
			ps.setBoolean(INDEX++, ejb.getEmailRegrade());
			ps.setBoolean(INDEX++, ejb.getEmailFile());
			ps.setBoolean(INDEX++, ejb.getEmailFinalGrade());
			if(ejb.getLecture() == null) ps.setNull(INDEX++, Types.VARCHAR);
			else ps.setString(INDEX++, ejb.getLecture());
			if(ejb.getLab() == null) ps.setNull(INDEX++, Types.VARCHAR);
			else ps.setString(INDEX++, ejb.getLab());
			if(ejb.getSection() == null) ps.setNull(INDEX++, Types.VARCHAR);
			else ps.setString(INDEX++, ejb.getSection());
			if(ejb.getCredits() == null) ps.setNull(INDEX++, Types.VARCHAR);
			else ps.setString(INDEX++, ejb.getCredits());
			if(ejb.getGradeOption() == null) ps.setNull(INDEX++, Types.VARCHAR);
			else ps.setString(INDEX++, ejb.getGradeOption());
			if(ejb.getDepartment() == null) ps.setNull(INDEX++, Types.VARCHAR);
			else ps.setString(INDEX++, ejb.getDepartment());
			if(ejb.getCourseNum() == null) ps.setNull(INDEX++, Types.VARCHAR);
			else ps.setString(INDEX++, ejb.getCourseNum());
			if(ejb.getUserID() == null) ps.setNull(INDEX++, java.sql.Types.VARCHAR);
			else ps.setString(INDEX++, ejb.getUserID());
			ps.setLong(INDEX++, ejb.getCourseID());
			ps.executeUpdate();
			conn.close();
			ps.close();
        } catch (Exception e) {
        	try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StudentDAO#findAll()
	 */
	public Collection findAll() throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID, CourseID from tStudent";
			ps = conn.prepareStatement(queryString);
			rs = ps.executeQuery();
			while (rs.next()) {
				StudentPK pk = new StudentPK(rs.getLong(2), rs.getString(1).trim());
				result.add(new StudentPK(pk.getCourseID(), pk.getUserID()));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
        return result;
	}

	/**
	 * return all students regardless of status
	 */
	public Collection findAllByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select s.NetID, s.CourseID from tStudent s " +
				"inner join tUser u on s.NetID = u.NetID " +
				"where s.CourseID = ? order by u.LastName, u.FirstName, s.NetID";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) result.add(new StudentPK(rs.getLong(2), rs.getString(1).trim()));
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/**
	 * Return only students with status ENROLLED
	 */
	public Collection findByCourseIDSortByLastName(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select s.NetID, s.CourseID from tStudent s " +
				"inner join tUser u on s.NetID = u.NetID " +
				"where s.CourseID = ? and s.Status = ? " +
				"order by u.LastName, u.FirstName, s.NetID";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setString(2, StudentBean.ENROLLED);
			rs = ps.executeQuery();
			while (rs.next()) result.add(new StudentPK(rs.getLong(2), rs.getString(1).trim()));
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/**
	 * Return only students with status ENROLLED
	 */
	public Collection findByCourseIDSortByNetID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select s.NetID, s.CourseID from tStudent s " +
				"inner join tUser u on s.NetID = u.NetID " +
				"where s.CourseID = ? and s.Status = ? " +
				"order by s.NetID";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setString(2, StudentBean.ENROLLED);
			rs = ps.executeQuery();
			while (rs.next()) result.add(new StudentPK(rs.getLong(2), rs.getString(1).trim()));
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	public Collection findByStaff(String netID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select s.NetID, s.CourseID from tStudent s, tStaff t where s.CourseID = t.CourseID " +
					"and s.Status = ? and t.NetID = ? order by s.CourseID ASC, s.NetID ASC";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, StudentBean.ENROLLED);
			ps.setString(2, netID);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new StudentPK(rs.getLong("CourseID"), rs.getString("NetID").trim()));
			}
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StudentDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.StudentPK)
	 */
	public StudentPK findByPrimaryKey(StudentPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID from tStudent where NetID = ? and CourseID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, pk.userID);
			ps.setLong(2, pk.courseID);
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find student with NetID = " + pk.getUserID() +
						" in course " + pk.getCourseID());
			}
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StudentDAO#findByUserID(java.lang.String)
	 */
	public Collection findByUserID(String userID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID, CourseID from tStudent where NetID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, userID);
			rs = ps.executeQuery();
			while (rs.next()) {
				StudentPK pk = new StudentPK(rs.getLong(2), rs.getString(1)
						.trim());
				result.add(pk);
			}
			rs.close();
			ps.close();
			conn.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StudentDAO#findByUserCourse(java.lang.String,
	 *      long)
	 */
	public StudentPK findByUserCourse(String netid, long courseid)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StudentPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID, CourseID from tStudent where NetID = ? and courseid = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netid);
			ps.setLong(2, courseid);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = new StudentPK(rs.getLong(2), rs.getString(1).trim());
			} else {
				throw new FinderException("Could not find student in course");
			}
			rs.close();
			ps.close();
			conn.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.StudentDAO#findEmptyNameByCourseID(long)
	 */
	public Collection findEmptyNameByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select s.NetID, s.CourseID from tStudent st, tUser u " + 
					"where st.CourseID = ? and st.NetID = u.NetID and (u.FirstName = ? or u.LastName = ?)";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setString(2, "");
			ps.setString(3, "");
			rs = ps.executeQuery();
			while (rs.next()) {
				StudentPK pk = new StudentPK(rs.getLong(2), rs.getString(1)
						.trim());
				result.add(pk);
			}
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	private Collection findEmailOption(long courseID, String emailOption) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID, CourseID from tStudent where CourseID = ? and " +
					"Status = ? and " + emailOption + " = ? order by NetID ASC";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setString(2, StudentBean.ENROLLED);
			ps.setBoolean(3, true);
			rs = ps.executeQuery();
			while (rs.next()) result.add(new StudentPK(rs.getLong(2), rs.getString(1).trim()));
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.StudentDAO#findEmailNewAssigns(long)
     */
    public Collection findEmailNewAssigns(long courseID) throws FinderException {
        return findEmailOption(courseID, "EmailNewAssign");
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.StudentDAO#findEmailDueDate(long)
     */
    public Collection findEmailDueDate(long courseID) throws FinderException {
        return findEmailOption(courseID, "EmailDueDate");
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.StudentDAO#findEmailGrades(long)
     */
    public Collection findEmailGrades(long courseID) throws FinderException {
        return findEmailOption(courseID, "EmailNewGrade");
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.StudentDAO#findEmailFinalGrades(long)
     */
    public Collection findEmailFinalGrades(long courseID) throws FinderException {
        return findEmailOption(courseID, "EmailFinalGrade");
    }

    public Collection findEmailRegrade(Collection groupIDs) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (groupIDs.size() == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT s.NetID, s.CourseID FROM tStudent s INNER JOIN " +
                      "tAssignment a ON s.CourseID = a.CourseID INNER JOIN " +
                      "tGroups g ON a.AssignmentID = g.AssignmentID INNER JOIN " +
                      "tGroupMembers m ON m.GroupID = g.GroupID AND m.NetID = s.NetID " +
                      "WHERE (m.Status = ?) AND (s.Status = ?) AND (s.EmailRegrade = ?) AND (";
			for (int i=0; i < groupIDs.size() - 1; i++) {
			    queryString += "g.GroupID = ? OR ";
			}
			queryString += "g.GroupID = ?)";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, GroupMemberBean.ACTIVE);
			ps.setString(2, StudentBean.ENROLLED);
			ps.setBoolean(3, true);
			Iterator groups = groupIDs.iterator();
			int count = 4;
			while (groups.hasNext()) {
			    Long groupID = (Long) groups.next();
			    ps.setLong(count++, groupID.longValue());
			}
			rs = ps.executeQuery();
			while (rs.next()) result.add(new StudentPK(rs.getLong(2), rs.getString(1).trim()));
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
    }
    
    public Collection findEmailTimeSlots(Collection groupIDs) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (groupIDs.size() == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT s.NetID, s.CourseID FROM tStudent s INNER JOIN " +
                      "tAssignment a ON s.CourseID = a.CourseID INNER JOIN " +
                      "tGroups g ON a.AssignmentID = g.AssignmentID INNER JOIN " +
                      "tGroupMembers m ON m.GroupID = g.GroupID AND m.NetID = s.NetID " +
                      "WHERE (m.Status = ?) AND (s.Status = ?) AND (s.EmailTimeslot = ?) AND (";
			for (int i=0; i < groupIDs.size() - 1; i++) {
			    queryString += "g.GroupID = ? OR ";
			}
			queryString += "g.GroupID = ?)";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, GroupMemberBean.ACTIVE);
			ps.setString(2, StudentBean.ENROLLED);
			ps.setBoolean(3, true);
			Iterator groups = groupIDs.iterator();
			int count = 4;
			while (groups.hasNext()) {
			    Long groupID = (Long) groups.next();
			    ps.setLong(count++, groupID.longValue());
			}
			rs = ps.executeQuery();
			while (rs.next()) result.add(new StudentPK(rs.getLong(2), rs.getString(1).trim()));
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
    }
    
    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.StudentDAO#findByAssignmentIDAssignedTo(long, java.lang.String)
     */
    public Collection findByAssignmentIDAssignedTo(long assignID, String netID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT DISTINCT s.NetID, s.CourseID " +
					"FROM tStudent s INNER JOIN " +
                    "tAssignment a ON s.CourseID = a.CourseID INNER JOIN " +
                    "tGroups g ON a.AssignmentID = g.AssignmentID INNER JOIN " +
                    "tGroupMembers m ON g.GroupID = m.GroupID AND s.NetID = m.NetID INNER JOIN " +
                    "tGroupAssignedTo ga ON ga.GroupID = g.GroupID " +
                    "WHERE (ga.NetID = ?) AND (s.Status = ?) AND (m.Status = ?) AND (a.AssignmentID = ?) " +
                    "ORDER BY s.NetID";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netID);
			ps.setString(2, StudentBean.ENROLLED);
			ps.setString(3, GroupMemberBean.ACTIVE);
			ps.setLong(4, assignID);
			rs = ps.executeQuery();
			while (rs.next()) result.add(new StudentPK(rs.getLong("CourseID"), rs.getString("NetID").trim()));
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.StudentDAO#findEmailFileSubmit(long)
     */
    public Collection findEmailFileSubmit(long groupID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT s.NetID, s.CourseID FROM tStudent s INNER JOIN " +
					"tAssignment a ON s.CourseID = a.CourseID INNER JOIN " +
                    "tGroups g ON a.AssignmentID = g.AssignmentID INNER JOIN " +
                    "tGroupMembers gm ON g.GroupID = gm.GroupID " +
                    "WHERE (s.Status = ?) AND (gm.Status = ?) AND (s.EmailFileSubmit = ?) AND (g.GroupID = ?)";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, StudentBean.ENROLLED);
			ps.setString(2, GroupMemberBean.ACTIVE);
			ps.setBoolean(3, true);
			ps.setLong(4, groupID);
			rs = ps.executeQuery();
			while (rs.next()) result.add(new StudentPK(rs.getLong(2), rs.getString(1).trim()));
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
    }

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.StudentDAO#findByAssignmentIDNetID(long, java.lang.String)
	 */
	public StudentPK findByAssignmentIDNetID(long assignID, String netID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StudentPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT c.CourseID FROM tStudent s INNER JOIN " +
				"tCourse c ON s.CourseID = c.CourseID INNER JOIN " +
				"tAssignment a ON c.CourseID = a.CourseID " +
				"WHERE (a.AssignmentID = ?) AND (s.NetID = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignID);
			ps.setString(2, netID);
			rs = ps.executeQuery();
			if (rs.next()) result = new StudentPK(rs.getLong("CourseID"), netID);
			else throw new FinderException("Could not find student with NetID = " + netID + " in assignment " + assignID);
			rs.close();
			ps.close();
			conn.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			FinderException g = new FinderException(e.getMessage());
			g.setStackTrace(e.getStackTrace());
			throw g;
		}
		return result;
	}
	
	public StudentPK findByGroupIDNetID(long groupID, String netID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StudentPK result = (StudentPK)ResultCache.getCache("StudentByGroupIDNetID-" + 
					String.valueOf(groupID) + "-" + netID);
		if(result == null) {
			try {
				conn = jdbcFactory.getConnection();
				String query = "SELECT c.CourseID FROM tStudent s INNER JOIN " +
					"tCourse c ON s.CourseID = c.CourseID INNER JOIN " +
					"tAssignment a ON c.CourseID = a.CourseID INNER JOIN " +
					"tGroups g ON g.AssignmentID = a.AssignmentID " + 
					"WHERE (g.GroupID = ?) AND (s.NetID = ?)";
				ps = conn.prepareStatement(query);
				ps.setLong(1, groupID);
				ps.setString(2, netID);
				rs = ps.executeQuery();
				if (rs.next()) result = new StudentPK(rs.getLong("CourseID"), netID);
				else throw new FinderException("Could not find student with NetID = " + netID + " in group " + groupID);
				rs.close();
				ps.close();
				conn.close();
				ResultCache.addCacheEntry("StudentByGroupIDNetID-" + 
						String.valueOf(groupID) + "-" + netID, 
						new String[] {"Assignment", "Group", "Student"}, 
						result);
			} catch (Exception e) {
				try {
					if (conn != null) conn.close();
					if (ps != null) ps.close();
					if (rs != null) rs.close();
				} catch (Exception f) {}
				FinderException g = new FinderException(e.getMessage());
				g.setStackTrace(e.getStackTrace());
				throw g;
			}
		}
		return result;
	}
	
}
