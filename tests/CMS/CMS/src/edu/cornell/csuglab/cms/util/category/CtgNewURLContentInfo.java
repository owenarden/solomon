/*
 * Created on Aug 31, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.util.category;

import java.sql.Timestamp;
import java.util.ArrayList;


/**
 * @author evan
 *
 * Holds info for one category content of type URL
 */
public class CtgNewURLContentInfo extends CtgNewContentInfo
{
	private String address, //the URL linked to
		label;               //the name displayed for the link
	
	public CtgNewURLContentInfo(long rowID, long colID, String address, String label)
	{
		super(rowID, colID);
		this.address = address;
		this.label = label;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public String getLabel()
	{
		return label;
	}
	
	public void setAddress(String address)
	{
		this.address = address;
	}
	
	public void setLabel(String label)
	{
		this.label = label;
	}
	
	/*
	 * CtgNewContentInfo functions. A null return value signifies no data.
	 */
	
	public String getColType()
	{
		return CtgUtil.CTNT_URL;
	}
	
	public Timestamp getDate()       //used by contents of type date
	{
		return null;
	}
	
	public String getText()          //used by contents of type text
	{
		return null;
	}
	
	public Long getNumber()          //used by contents of type number
	{
		return null;
	}
	
	public String getURLAddress()    //used by contents of type url
	{
		return getAddress();
	}
	
	public String getURLLabel()      //used by contents of type url
	{
		return getLabel();
	}
	
	public ArrayList getFileInfos()  //used by contents of type file
	{
		return null;
	}
	
	public ArrayList getFileLabels() //used by contents of type file
	{
		return null;
	}
}
