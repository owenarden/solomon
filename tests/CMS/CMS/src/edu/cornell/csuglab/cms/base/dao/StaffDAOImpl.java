/*
 * Created on Apr 7, 2004
 */

package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.StaffBean;
import edu.cornell.csuglab.cms.base.StaffDAO;
import edu.cornell.csuglab.cms.base.StaffPK;

/**
 * @author Theodore Chao
 */
public class StaffDAOImpl extends DAOMaster implements StaffDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StaffDAO#load(edu.cornell.csuglab.cms.base.StaffPK,
	 *      edu.cornell.csuglab.cms.base.StaffBean)
	 */
	public void load(StaffPK pk, StaffBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT * FROM tStaff WHERE NetID = ? AND CourseID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, pk.getNetID());
			ps.setLong(2, pk.getCourseID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setNetID(pk.getNetID());
				ejb.setCourseID(pk.getCourseID());
				ejb.setStatus(rs.getString("Status"));
				ejb.setAdminPriv(rs.getBoolean("AdminPriv"));
				ejb.setGroupsPriv(rs.getBoolean("GroupsPriv"));
				ejb.setGradesPriv(rs.getBoolean("GradesPriv"));
				ejb.setAssignmentsPriv(rs.getBoolean("AssignmentsPriv"));
				ejb.setCategoryPriv(rs.getBoolean("CategoryPriv"));
				ejb.setEmailAssignedTo(rs.getBoolean("EmailAssignedTo"));
				ejb.setEmailAssignSubmit(rs.getBoolean("EmailAssignSubmit"));
				ejb.setEmailDueDate(rs.getBoolean("EmailDueDate"));
				ejb.setEmailFinalGrade(rs.getBoolean("EmailFinalGrade"));
				ejb.setEmailNewAssign(rs.getBoolean("EmailNewAssign"));
				ejb.setEmailRequest(rs.getBoolean("EmailRequest"));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
			throw new EJBException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StaffDAO#store(edu.cornell.csuglab.cms.base.StaffBean)
	 */
	public void store(StaffBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tStaff set status = ?, adminpriv = ?, groupspriv = ?, " +
				"gradespriv = ?, assignmentspriv = ?, categorypriv = ?, " +
				"emailassignedto = ?, emailassignsubmit = ?, emailduedate = ?, emailnewassign = ?, " + 
				"emailfinalgrade = ?, emailrequest = ? where NetID = ? and CourseID = ?";
			ps = conn.prepareStatement(query);
			if(ejb.getStatus() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getStatus());
			ps.setBoolean(2, ejb.getAdminPriv());
			ps.setBoolean(3, ejb.getGroupsPriv());
			ps.setBoolean(4, ejb.getGradesPriv());
			ps.setBoolean(5, ejb.getAssignmentsPriv());
			ps.setBoolean(6, ejb.getCategoryPriv());
			ps.setBoolean(7, ejb.getEmailAssignedTo());
			ps.setBoolean(8, ejb.getEmailAssignSubmit());
			ps.setBoolean(9, ejb.getEmailDueDate());
			ps.setBoolean(10, ejb.getEmailNewAssign());
			ps.setBoolean(11, ejb.getEmailFinalGrade());
			ps.setBoolean(12, ejb.getEmailRequest());
			if(ejb.getNetID() == null) ps.setNull(13, java.sql.Types.VARCHAR);
			else ps.setString(13, ejb.getNetID());
			ps.setLong(14, ejb.getCourseID());
			ps.executeUpdate();
			conn.close();
			ps.close();
        } catch (Exception e) {
        	try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
			throw new EJBException(e.getMessage());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StaffDAO#remove(edu.cornell.csuglab.cms.base.StaffPK)
	 */
	public void remove(StaffPK pk) throws RemoveException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "delete from tStaff where NetID = ? and CourseID = ?";
			ps = conn.prepareStatement(update);
			ps.setString(1, pk.getNetID());
			ps.setLong(2, pk.getCourseID());
			ps.executeUpdate();
			conn.close();
			ps.close();
        } catch (Exception e) {
        	try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StaffDAO#create(edu.cornell.csuglab.cms.base.StaffBean)
	 */
	public StaffPK create(StaffBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		StaffPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "INSERT INTO tStaff (CourseID, NetID, Status) VALUES (?, ?, ?)";
			ps = conn.prepareStatement(createString);
			ps.setLong(1, ejb.getCourseID());
			if(ejb.getNetID() == null || ejb.getNetID().equals("")) throw new CreateException("Cannot create staff member with null or empty NetID");
			ps.setString(2, ejb.getNetID());
			ps.setString(3, StaffBean.ACTIVE);
			int count = ps.executeUpdate();
			if (count == 1) {
				result = new StaffPK(ejb.getCourseID(), ejb.getNetID());
			} else {
				throw new CreateException("Failed to create new staff member");
			}
			ps.close();
			conn.close();
        } catch (Exception e) {
        	try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StaffDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.StaffPK)
	 */
	public StaffPK findByPrimaryKey(StaffPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID from tStaff where NetID = ? and CourseID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, pk.getNetID());
			ps.setLong(2, pk.getCourseID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find staff member with NetID = " + pk.getNetID() +
						" in course " + pk.getCourseID());
			}
			rs.close();
			ps.close();
			conn.close();
			
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

	public StaffPK findByAssignmentIDNetID(long assignID, String netID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StaffPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT c.CourseID FROM tStaff s INNER JOIN " +
				"tCourse c ON s.CourseID = c.CourseID INNER JOIN " +
				"tAssignment a ON a.CourseID = c.CourseID " +
				"WHERE (a.AssignmentID = ?) AND (s.NetID = ?)";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignID);
			ps.setString(2, netID);
			rs = ps.executeQuery();
			if (rs.next()) result = new StaffPK(rs.getLong("CourseID"), netID);
			else throw new FinderException("Cannot find staff member with NetID = " + netID + " for assignment " + assignID);
			rs.close();
			ps.close();
			conn.close();
			
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StaffDAO#findByCourseID(long)
	 */
	public Collection findByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID, CourseID from tStaff where CourseID = ? " +
					"and Status = ? order by NetID asc";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setString(2, StaffBean.ACTIVE);
			rs = ps.executeQuery();
			while (rs.next()) {
				StaffPK pk = new StaffPK(rs.getLong(2), rs.getString(1).trim());
				result.add(new StaffPK(pk.getCourseID(), pk.getNetID()));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findAllByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID, CourseID from tStaff where CourseID = ? " +
					"order by NetID asc";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
				StaffPK pk = new StaffPK(rs.getLong(2), rs.getString(1).trim());
				result.add(new StaffPK(pk.getCourseID(), pk.getNetID()));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	public Collection findGradersByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID, CourseID from tStaff where CourseID = ? " +
					"and (AdminPriv = ? or GradesPriv = ?) and Status = ? order by NetID asc";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setBoolean(2, true);
			ps.setBoolean(3, true);
			ps.setString(4, StaffBean.ACTIVE);
			rs = ps.executeQuery();
			while (rs.next()) {
				StaffPK pk = new StaffPK(rs.getLong(2), rs.getString(1).trim());
				result.add(new StaffPK(pk.getCourseID(), pk.getNetID()));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}	

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StaffDAO#findByUserID(java.lang.String)
	 */
	public Collection findByUserID(String userID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID, CourseID from tStaff where netid = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, userID);
			rs = ps.executeQuery();
			while (rs.next()) {
				StaffPK pk = new StaffPK(rs.getLong(2), rs.getString(1).trim());
				result.add(new StaffPK(pk.getCourseID(), pk.getNetID()));
			}
			conn.close();
			rs.close();
			ps.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.StaffDAO#findByUserCourse(java.lang.String,
	 *      long)
	 */
	public StaffPK findByUserCourse(String netid, long courseid)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StaffPK result = null;
		try {
			if (null == jdbcFactory)
				throw new RuntimeException("no jdbc!!");
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID from tStaff where NetID = ? and CourseID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netid);
			ps.setLong(2, courseid);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = new StaffPK(courseid, netid);
			} else {
				throw new FinderException("Could not find staff member in course");
			}
			conn.close();
			rs.close();
			ps.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.StaffDAO#findEmailNewAssigns(long)
     */
    private Collection findEmail(long courseID, String emailString) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID, CourseID from tStaff where CourseID = ? and " +
					"Status = ? and " + emailString + " = ? order by NetID";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setString(2, StaffBean.ACTIVE);
			ps.setBoolean(3, true);
			rs = ps.executeQuery();
			while (rs.next()) {
				StaffPK pk = new StaffPK(rs.getLong("CourseID"), rs.getString("NetID").trim());
				result.add(pk);
			}
			conn.close();
			rs.close();
			ps.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
			throw new FinderException(e.getMessage());
		}
        return result;
    }
    
    public Collection findEmailNewAssigns(long courseID) throws FinderException {
        return findEmail(courseID, "EmailNewAssign");
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.StaffDAO#findEmailDueDates(long)
     */
    public Collection findEmailDueDates(long courseID) throws FinderException {
        return findEmail(courseID, "EmailDueDate");
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.StaffDAO#findEmailFinalGrades(long)
     */
    public Collection findEmailFinalGrades(long courseID) throws FinderException {
        return findEmail(courseID, "EmailFinalGrade");
    }
}
