/*
 * Created on Mar 8, 2004
 */

package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EntityBean;
import javax.ejb.FinderException;

/**
 * @ejb.bean name="Grade"
 *	jndi-name="GradeBean"
 *	type="BMP" 
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.GradeDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.GradeDAOImpl"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 **/
public abstract class GradeBean implements EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private String netID;
	private long assignmentID;
	private long subproblemID;
	private long gradeID;
	private Float grade;
	private String grader;
	private Timestamp dateEntered;
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The unique grading ID
	 */
	public long getGradeID() {
		return gradeID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param ID The unique grading ID
	 */
	public void setGradeID(long ID) {
		this.gradeID = ID;
	}

	/**
	 * @return The specific student being graded (the netID)
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getNetID() {
		return netID;
	}

	/**
	 * @param The specific student being graded (the netID)
	 * @ejb.interface-method view-type="local"
	 */
	public void setNetID(String netID) {
		this.netID = netID;
	}
	
	/**
	 * @return The assignmentID
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getAssignmentID() {
		return assignmentID;
	}

	/**
	 * @param The assignmentID
	 * @ejb.interface-method view-type="local"
	 */
	public void setAssignmentID(long assignmentID) {
		this.assignmentID = assignmentID;
	}

	/**
	 * @return The subassignmentID
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getSubProblemID() {
		return subproblemID;
	}

	/**
	 * @param The subassignmentID
	 * @ejb.interface-method view-type="local"
	 */
	public void setSubProblemID(long subproblemID) {
		this.subproblemID = subproblemID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The grade value
	 */
	public Float getGrade() {
		return grade;//grade.floatValue();
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param grade The grade value
	 */
	public void setGrade(Float grade) {
		this.grade = grade;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The grader's netid
	 */
	public String getGrader() {
		return grader;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param grader The grader's netid
	 */
	public void setGrader(String grader) {
		this.grader = grader;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The date the grade was entered
	 */
	public Timestamp getDateEntered() {
		return dateEntered;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param dateEntered The date the grade was entered
	 */
	public void setDateEntered(Timestamp dateEntered) {
		this.dateEntered = dateEntered;
	}

	/**
	 * Checks to see if the given primary key can be found in the database
	 * 
	 * @param key
	 *            The primary key being serarched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public GradePK ejbFindByPrimaryKey(GradePK key) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all the grade entries for all active members of a collection of groups
	 * 
	 * @param groupids
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupIDs(Collection groupids) throws FinderException {
		return null;
	}
	
	/**
	 * Finds only the grade entries for all active members of a collection of groups
	 * which have been assigned to a certain grader
	 * 
	 * @param groupids
	 * @param grader
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupIDsAssignedTo(Collection groupids, String grader, int numSubProbs) throws FinderException {
		return null;
	}

	/**
	 * Finds all the latest grade entries for all students in a course
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindMostRecentByCourseID(long courseID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all the latest total grade entries for all students in a course
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindTotalsByCourseID(long courseID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all total grades in the course which are the latest updated entries 
	 * and which are over the maximum score for the assignment
	 */
	public Collection ejbFindOverMaxByCourseID(long courseID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all the grade entries for a given student for a single assignment
	 * 
	 * @param netid
	 *            The student's netid
	 * @param assignmentid
	 *            The assignmentID
	 * @return The collection of grades
	 * @throws FinderException
	 */
	public GradePK ejbFindMostRecentByNetID(String netid, long assignmentid, long subProblemID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all the most recent grade entries for a given student for all
	 * assignments in a course
	 * @param netid
	 * @param courseid
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindMostRecentByNetIDCourseID(String netid, long courseid) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all the most recent grade entries for all Active group members
	 * who are grouped with the given user in the given course.  This only includes
	 * grades for assignments on which they are grouped with the given user, and
	 * always includes all the given user's grades.
	 * @param netid
	 * @param courseid
	 * @param admin True only if this query is being run for a full course admin,
	 *  in which case we want to ignore any Grading assignments and just return all grades
	 * @param grader The NetID of the grader who this query is being run for, only grades
	 *  which this person has been assigned to will be returned if the given assignments have
	 *  assigned grading only.
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindRecentByNetIDCourseID(String netid, long courseid, boolean admin, String grader) throws FinderException {
	    return null;
	}
	
	/**
	 * 
	 * @param netid
	 * @param assignmentid
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindMostRecentByNetAssignmentID(String netid, long assignmentid) throws FinderException {
	    return null;
	}

	/**
	 * Finds all the most recent grades for all students in a course for a given
	 * assignment.  Collection is ordered by NetID then by SubProblemID. 
	 * @param assignmentID
	 * @return
	 */
	public Collection ejbFindMostRecentByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all the most recent grades for all students who are active in groups
	 * for which the given grader has been assigned to at least one subproblem.
	 * Ordered by NetID then SubProblemID.
	 * @param assignmentID
	 * @param grader
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindMostRecentByAssignmentIDGrader(long assignmentID, String grader) throws FinderException {
	    return null;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 **/
	public GradeData getGradeData() {
		return new GradeData(getGradeID(), getNetID(), getAssignmentID(), getSubProblemID(), getGrade(),
				getGrader(), getDateEntered());
	}
	
	/**
	 * Create a new grade entry
	 * @param semesterID
	 * @param courseID
	 * @param assignmentID
	 * @param subProblemID
	 * @param groupID
	 * @param grade
	 * @param netID
	 * @param grader
	 * @throws CreateException
	 * @ejb.create-method view-type="local"
	 */
	public GradePK ejbCreate(long assignmentID, long subProblemID,
			Float grade, String netID, String grader) throws CreateException {
		setAssignmentID(assignmentID);
		setSubProblemID(subProblemID);
		setGrade(grade);
		setNetID(netID);
		setGrader(grader);
		setDateEntered(new Timestamp(System.currentTimeMillis()));
		return null;
	}
}
