/*
 * Created on Mar 30, 2005
 */
package edu.cornell.csuglab.cms.author;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import edu.cornell.csuglab.cms.base.*;
import edu.cornell.csuglab.cms.www.util.StringUtil;

/**
 * @author yc263
 * 
 */
public class Principal {
	public static final String guestid = "guest";

	public static final int AUTHOR_STAFF = 1, // authorization code levels,
												// used to determine which
												// categories are visible to
												// this principal
			AUTHOR_STUDENT = 2, AUTHOR_CORNELL_COMMUNITY = 3, AUTHOR_GUEST = 4;

	private UserWrapper userW; // user associated with this principal

	private UserWrapper appuserW; // apparent user for staff-as-student mode

	// XXX This is not a good design. The apparent user should be
	// XXX part of the request, not an intrinsic part of the principal.
	// XXX That way it would be possible to go back to a previous view
	// XXX without confusing access control.
	// XXX However, in that case whether the principal can act for
	// XXX the apparent user would need to be checked on each request.
	// XXX --ACM

	private RootLocal database; // Session bean

	private String ip; // IP address of this Principal

	public Principal(RootLocal database, String netID, String ip)
			throws UserNotFoundException {
		this.database = database;
		this.ip = ip;
		try {
			UserLocal userBean = null;
			if (!netID.equals(guestid)) {
				try {
					userBean = database.userHome().findByUserID(netID);
				} catch (Exception e) {
				}
			}
			if (userBean == null) {
				if (netID.equals(guestid)) {
					userW = new UserGuest();
				} else {
					userW = new UserCornellMember(netID);
				}
			} else
				userW = new UserAuthenticated(userBean);
			// buildPrivilegeMap();
		} catch (Exception re) {
			if (re instanceof UserNotFoundException) {
				throw (UserNotFoundException) re;
			}
			re.printStackTrace();
		}
	}
	
	/**
	 * This constructor is used when logging in as external user only.
	 * It checks the supplied password to see if it matches, throws exception if it does not.
	 * 
	 * @param database
	 * @param externalloginID
	 * @param externalloginPW
	 * @param ip
	 * @throws Exception
	 */
	public Principal(RootLocal database, String externalloginID, String externalloginPW, String ip)
	throws Exception {
		this.database = database;
		this.ip = ip;
		UserLocal userBean = null;
		if (!externalloginID.equals(guestid)) {
			userBean = database.userHome().findByUserID(externalloginID);
			if(!userBean.checkPassword(externalloginPW))
			{ 
				throw new UserNotFoundException();
			}
			userW = new UserAuthenticated(userBean);
		}
	}

	/**
	 * for debugging
	 */
	public String toString() {

		return "P{realUser=" + userW.getUserID() + ",apparentUser="
				+ appuserW.getUserID() + "}";
	}

	/** ************************ basic identifying info ************************* */

	/**
	 * @return the netid of the user associated with principal
	 */
	public String getPrincipalID() {
		return userW.getUserID();
	}

	/**
	 * Returns the NetID of this Principal; if it is in Staff-as-Student mode,
	 * returns a string of the form "staffid as studentid".
	 * 
	 * @return The NetID of this Principal, or a string of the form "staffid as
	 *         studentid" if in student-as-staff mode.
	 */
	public String getNetID() {
		if (appuserW != null)
			return userW.getUserID() + " as " + appuserW.getUserID();
		else
			return userW.getUserID();
	}

	/**
	 * If something really messed-up happens, it's possible for this function to
	 * return null. However, this should never happen.
	 * 
	 * @return InetAddress
	 */
	public InetAddress getIPAddress() {
		byte[] ipBytes = new byte[4];
		String tempIP = new String(this.ip);
		for (int i = 0; i < 3; i++) {
			ipBytes[i] = (byte) Integer.parseInt(tempIP.substring(0, tempIP
					.indexOf('.')));
			tempIP = tempIP.substring(tempIP.indexOf('.') + 1);
		}
		ipBytes[3] = (byte) Integer.parseInt(tempIP);
		try {
			return InetAddress.getByAddress(ipBytes);
		} catch (UnknownHostException x) // means address is of illegal
											// length--shouldn't happen
		{
			x.printStackTrace();
			return null;
		}
	}

	/**
	 * Returns the apparent NetID of this Principal. If in student or staff
	 * mode, it is the actual NetID, if in staff-as-student it is the students.
	 * 
	 * @return The apparent netID of this Principal.
	 */
	public String getUserID() {
		if (appuserW != null)
			return appuserW.getUserID();
		else
			return userW.getUserID();
	}

	public String getCUID() {
		// if (appuserW != null) return appuserW.getCUID();
		return userW.getCUID();
	}

	public String getFirstName() {
		// if (appuserW != null) return appuserW.getFirstName();
		return userW.getFirstName();
	}

	public String getLastName() {
		// if (appuserW != null) return appuserW.getLastName();
		return userW.getLastName();
	}

	public String getCollege() {
		// if (appuserW != null) return appuserW.getCollege();
		return userW.getCollege();
	}

	public String getDepartment() {
		// if (appuserW != null) return appuserW.getDepartment();
		return userW.getDepartment();
	}
	
	public boolean isPWExpired() {
		return userW.isPWExpired();
	}
	
	public boolean isDeactivated() {
		return userW.isDeactivated();
	}

	public UserData getUserData() {
		if (appuserW != null)
			return appuserW.getUserData();
		return userW.getUserData();
	}

	/** ************************** staff-as-student **************************** */

	/**
	 * Enter staff-as-student mode, returns null on success, error message if
	 * apparent user doesn't exist
	 * 
	 * @param rawStudNetID
	 *            The apparent user in Staff-as-Student mode; the student to
	 *            mimic. The actual user remains the same. This parameter can be
	 *            untrusted.
	 * @return The empty string if successful, or a nonempty string with error
	 *         details
	 */
	public String setStaffAsStudent(String rawStudNetID, long courseID) {
		// validate netID input, use canonical representation
		List netIDSingleton;
		try {
			netIDSingleton = StringUtil.parseNetIDList(rawStudNetID);
		} catch (IllegalArgumentException e) {
			return "Cannot view as student; '" + rawStudNetID
					+ "' does not appear to be a NetID";
		}

		if (netIDSingleton.size() != 1) {
			return "Cannot view as student; expected single NetID, instead got: "
					+ netIDSingleton.toString();
		}

		String appNetID = (String) netIDSingleton.get(0);

		String result = null;
		UserLocal appuser = null;
		try {
			appuser = database.userHome().findByUserID(appNetID);
		} catch (Exception re) {
		}
		try {
			CourseLocal course = database.courseHome().findByPrimaryKey(
					new CoursePK(courseID));
			if (appuser != null)
				if (appuser.isStudent(courseID))
					appuserW = new UserStaffAsStudent(courseID, appuser);
				else
					result = "User with netid=" + appNetID
							+ " is not a student in " + course.getCode();
			else
				result = "User with netid=" + appNetID
						+ " not found, please input valid student netid";
		} catch (Exception e) {
			result = "An error occurred while trying to enter student mode";
		}
		return result;
	}

	/**
	 * Enter staff-as-CornellMember mode
	 * 
	 */
	public void setStaffAsCornellMem(long courseID) {
		appuserW = new UserStaffAsCornellMember(courseID);
	}

	public void setStaffAsGuest(long courseID) {
		appuserW = new UserStaffAsGuest(courseID);
	}

	/**
	 * Returns this Principal to normal staff mode If not in Staff-as-Student
	 * mode, does nothing.
	 */
	public void resetToStaffMode() {
		appuserW = null;
	}

	/**
	 * @return the course ID for which staff is in a different view mode
	 */
	public long getCourseID() {
		return appuserW.getCourseID();
	}

	/**
	 * "Staff-as-blank" means "masquerading as *somebody*", ie not in normal
	 * mode
	 * 
	 * @return
	 */
	public boolean isInStaffAsBlankMode() {
		return appuserW != null;
	}

	/**
	 * Returns whether or not this staff is currently masquerading as a student
	 * in the course Precond: user must be a staff member in the course
	 * 
	 * @return
	 */
	public boolean isInStaffAsStudentMode() {
		return appuserW != null && appuserW instanceof UserStaffAsStudent;
	}

	/**
	 * Returns whether or not this staff is currently masquerading as a generic
	 * Cornell community member Precond: user must be a staff member in the
	 * course
	 * 
	 * @return
	 */
	public boolean isInStaffAsCornellMemMode() {
		return appuserW != null && appuserW instanceof UserStaffAsCornellMember;
	}

	/**
	 * * Returns whether or not this staff is currently masquerading as a
	 * visitor to the Cornell site Precond: user must be a staff member in the
	 * course
	 * 
	 * @return
	 */
	public boolean isInStaffAsGuestMode() {
		return appuserW != null && appuserW instanceof UserStaffAsGuest;
	}

	/**
	 * TODO finish this comment! 1) If in regular mode, returns the authorizn
	 * level associated with the actual user 2) If in staff-as-[] mode, returns
	 * the authorizn level associated with user [] that mode is in
	 * 
	 * @param courseID
	 * @return
	 */
	public int getAuthoriznLevelByCourseID(long courseID) {
		if (appuserW != null)
			return appuserW.getAuthoriznLevelByCourseID(courseID);
		else
			return userW.getAuthoriznLevelByCourseID(courseID);
	}

	/**
	 * Returns whether or not this Principal is a student or staff in a
	 * specified course
	 * 
	 * @param courseID
	 * @return
	 */
	public boolean isUserInCourseByCourseID(long courseID) {
		return this.getAuthoriznLevelByCourseID(courseID) < AUTHOR_CORNELL_COMMUNITY;
	}

	/*
	 * ---------------------------- Student Privilege
	 * Questions---------------------------------
	 */

	/**
	 * Returns whether or not this Principal is a student in a specified course
	 * 
	 * @param courseID
	 *            The ID of the course to check
	 * @return Whether or not this Principal is a student @ If the db connection
	 *         fails
	 */
	public boolean isStudentInCourseByCourseID(long courseID) {
		if (appuserW != null) {
			return appuserW.isStudentInCourseByCourseID(courseID)
					&& userW.isStaffInCourseByCourseID(courseID);
		} else {
			return userW.isStudentInCourseByCourseID(courseID);
		}
	}

	/**
	 * Returns whether or not this Principal is a student in the course with a
	 * specified assignment.
	 * 
	 * @param assignmentID
	 *            The ID of the assignment to check
	 * @return Whether or not this Principal is a student @ If the db connection
	 *         fails
	 */
	public boolean isStudentInCourseByAssignmentID(long assignmentID) {
		try {
			AssignmentLocal a = database.assignmentHome().findByAssignmentID(
					assignmentID);
			return isStudentInCourseByCourseID(a.getCourseID());
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * Returns whether or not this Principal is a student in a course with a
	 * specified group.
	 * 
	 * @param groupID
	 *            The ID of the group to check
	 * @return Whether or not this Principal is a student @ If the db connection
	 *         fails
	 */
	public boolean isStudentInCourseByGroupID(long groupID) {
		try {
			GroupLocal g = database.groupHome().findByGroupID(groupID);
			AssignmentLocal a = database.assignmentHome().findByAssignmentID(
					g.getAssignmentID());
			return isStudentInCourseByCourseID(a.getCourseID());
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * Returns whether or not this Principal is a studnet in a course identified
	 * by a FileID identifying an AssignmentItem
	 * 
	 * @param FileIDT
	 *            The ID of the file to check
	 * @return Whether or not this Principal is a student @ If the db connection
	 *         fails
	 */
	public boolean isStudentInCourseByAssignmentItemID(long itemID) {
		try {
			AssignmentItemLocal aid = database.assignmentItemHome()
					.findByPrimaryKey(new AssignmentItemPK(itemID));
			return isStudentInCourseByAssignmentID(aid.getAssignmentID());
		} catch (Exception e) {
		}
		return false;
	}

	/*--------------------------------Staff Privilege Questions---------------------------------*/

	/**
	 * Returns whether or not this Principal is a staff member in a specified
	 * course
	 * 
	 * @param courseID
	 *            The ID of the course to check
	 * @return Whether or not this Principal is a staff member @ If the db
	 *         connection fails
	 */
	public boolean isStaffInCourseByCourseID(long courseID) {
		return appuserW == null && userW.isStaffInCourseByCourseID(courseID);
	}

	/**
	 * Returns whether or not this Principal is a staff member in an
	 * announcement's course.
	 * 
	 * @param announceID
	 *            The ID of the announcement to check
	 * @return Whether or not this Principal is a staff member @ If the db
	 *         connection fails
	 */
	public boolean isStaffInCourseByAnnouncementID(long announceID) {
		try {
			AnnouncementLocal announce = database.announcementHome()
					.findByPrimaryKey(new AnnouncementPK(announceID));
			return appuserW == null
					&& isStaffInCourseByCourseID(announce.getCourseID());
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * Returns whether or not this Principal has Admin Privilages for the
	 * specified course. False if not a staff memeber.
	 * 
	 * @param courseID
	 *            The ID of the course to check
	 * @return Whether or not this Principal has Admin privilages @ if the db
	 *         connection fails
	 */
	public boolean isAdminPrivByCourseID(long courseID) {
		// return isStaffInCourseByCourseID(courseID) &&
		// userW.isAdminPrivByCourseID(courseID);
		return appuserW == null && userW.isAdminPrivByCourseID(courseID);
	}

	/**
	 * Returns whether or not this Principal has AdminPrivilages for an
	 * assignment's course. False if not a staff member.
	 * 
	 * @param assignID
	 *            The ID of the assignment to check
	 * @return Whether or not this Principal has Admin Privilages @ if the db
	 *         connection fails
	 */
	public boolean isAdminPrivByAssignmentID(long assignID) {
		try {
			AssignmentLocal assign = database.assignmentHome()
					.findByAssignmentID(assignID);
			return isAdminPrivByCourseID(assign.getCourseID());
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * Returns true if this user can view the students page, and add or drop
	 * students from a course
	 */
	public boolean hasStudentsPageAccess(long courseID) {
		try {
			boolean isAdmin = userW.isAdminPrivByCourseID(courseID), isGroups = userW
					.isGroupsPrivByCourseID(courseID), isGrades = userW
					.isGradesPrivByCourseID(courseID);
			return isAdmin || isGroups || isGrades;
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * Returns whether or not this Principal has Assignments Privilages for an
	 * assignment's course. False if not a staff member.
	 * 
	 * @param assignID
	 *            The ID of the assignment to check
	 * @return Whether or not this Principal has Assignments Privilages @ if the
	 *         db connection fails
	 */
	public boolean isAssignPrivByAssignmentID(long assignID) {
		try {
			AssignmentLocal assign = database.assignmentHome()
					.findByAssignmentID(assignID);
			return isAssignPrivByCourseID(assign.getCourseID());
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * Returns whether or not this Principal has Grades Privilages for the
	 * specified course. False if not a staff memeber.
	 * 
	 * @param courseID
	 *            The ID of the course to check
	 * @return Whether or not this Principal has Grades privilages @ if the db
	 *         connection fails
	 */
	public boolean isGradesPrivByCourseID(long courseID) {
		// return isStaffInCourseByCourseID(courseID) &&
		// userW.isGradesPrivByCourseID(courseID);
		return appuserW == null && userW.isGradesPrivByCourseID(courseID);
	}

	/**
	 * Returns whether or not this Principal has Groups Privialges for an
	 * assignment's course. False if not a staff member.
	 * 
	 * @param assignID
	 *            The ID of the assignment to check.
	 * @return Whether or not this Principal has groups privilages @ if the db
	 *         connection fails
	 */
	public boolean isGradesPrivByAssignmentID(long assignID) {
		try {
			AssignmentLocal assign = database.assignmentHome()
					.findByAssignmentID(assignID);
			return isGradesPrivByCourseID(assign.getCourseID());
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * Returns whether or not this Principal has Groups Privilages for the
	 * specified course. False if not a staff memeber.
	 * 
	 * @param courseID
	 *            The ID of the course to check
	 * @return Whether or not this Principal has Groups privilages @ if the db
	 *         connection fails
	 */
	public boolean isGroupsPrivByCourseID(long courseID) {
		// return isStaffInCourseByCourseID(courseID) &&
		// userW.isGroupsPrivByCourseID(courseID);
		return appuserW == null && userW.isGroupsPrivByCourseID(courseID);
	}

	/**
	 * Returns whether or not this Principal has Groups Privilages for an
	 * assignment's course. False if not a staff member.
	 * 
	 * @param assignID
	 *            The ID of the assignment to check
	 * @return Whether or not this Principal has Groups Privilages @ if the db
	 *         connection fails
	 */
	public boolean isGroupsPrivByAssignmentID(long assignID) {
		try {
			AssignmentLocal assign = database.assignmentHome()
					.findByAssignmentID(assignID);
			return isGroupsPrivByCourseID(assign.getCourseID());
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * Returns whether or not this Principal has Assignments Privilages for the
	 * specified course. False if not a staff memeber.
	 * 
	 * @param courseID
	 *            The ID of the course to check
	 * @return Whether or not this Principal has Assignments privilages @ if the
	 *         db connection fails
	 */
	public boolean isAssignPrivByCourseID(long courseID) {
		// return isStaffInCourseByCourseID(courseID) &&
		// userW.isAssignPrivByCourseID(courseID);
		return appuserW == null && userW.isAssignPrivByCourseID(courseID);
	}

	/**
	 * Returns whether or not this Principal has Category Privilages for the
	 * specified course. False if not a staff memeber.
	 * 
	 * @param courseID
	 *            The ID of the course to check
	 * @return Whether or not this Principal has Category privilages @ if the db
	 *         connection fails
	 */
	public boolean isCategoryPrivByCourseID(long courseID) {
		return appuserW == null && userW.isCategoryPrivByCourseID(courseID);
	}

	/**
	 * Returns whether or not this Principal has Category Privilages for the
	 * specified category. False if not a staff memeber.
	 * 
	 * @param courseID
	 *            The ID of the course to check
	 * @return Whether or not this Principal has Category privilages @ if the db
	 *         connection fails
	 */
	public boolean isCategoryPrivByCategoryID(long ctgID) {
		try {
			CategoryLocal ctg = database.categoryHome().findByPrimaryKey(
					new CategoryPK(ctgID));
			return isCategoryPrivByCourseID(ctg.getCourseID());
		} catch (Exception e) {
		}
		return false;
	}

	public boolean canEditAnnouncement(long announceID) {
		return true;
	}

	/*---------------------------------CMSAdmin Privilege Question-------------------------------*/

	/**
	 * Returns whether or not this principal is a CMS administrator. Doesn't
	 * bother looking at the apparent user.
	 * 
	 * @return Whether or not this principal has CMS admin privileges @ if the
	 *         db connection fails
	 */
	public boolean isCMSAdmin() {
		return appuserW == null && userW.isCMSAdmin();
	}
	
	public boolean isCMSSubAdmin() {
		return appuserW == null && userW.isCMSSubAdmin();
	}
	
	public int getCMSSubAdminDomain() {
		if(appuserW != null)
		{
			return -1;
		}
		else
		{
			return userW.getCMSSubAdminDomain();
		}
	}

	/*-------------------------------------------------------------------------------------------*/

	public boolean isGuest() {
		if (appuserW == null) {
			return userW instanceof UserGuest;
		} else {
			return appuserW instanceof UserStaffAsGuest;
		}
	}

	public boolean isCCMember() {
		if (appuserW == null) {
			return userW instanceof UserCornellMember;
		} else {
			return appuserW instanceof UserStaffAsCornellMember;
		}
	}

	public boolean isAuthenticated() {
		if (appuserW == null) {
			return userW instanceof UserCornellMember
					|| userW instanceof UserAuthenticated;
		} else {
			return appuserW instanceof UserStaffAsCornellMember
					|| appuserW instanceof UserStaffAsStudent;
		}
	}

	/**
	 * @param courseID
	 * @return True if this principal has access to view the course home page
	 *         for this course, false otherwise. Returns false on any error.
	 */
	public boolean hasCourseAccess(long courseID) {
		try {
			CourseLocal course = database.courseHome().findByPrimaryKey(
					new CoursePK(courseID));
			if (isStaffInCourseByCourseID(courseID)
					|| isStudentInCourseByCourseID(courseID)) {
				return true;
			} else if (isAuthenticated()) {
				return course.getCourseCCAccess();
			} else {
				return course.getCourseGuestAccess();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @param courseID
	 * @param assignID
	 * @return True if this principal has access to view a student assignment
	 *         page in a course, false otherwise. Returns false on any error.
	 */
	public boolean hasAssignAccess(long courseID, long assignID) {
		try {
			CourseLocal course = database.courseHome().findByPrimaryKey(
					new CoursePK(courseID));
			AssignmentLocal assignment = database.assignmentHome()
					.findByPrimaryKey(new AssignmentPK(assignID));

			// a staff disguised as a student cannot view the surveys
			if (isInStaffAsStudentMode()
					&& assignment.getType() == AssignmentBean.SURVEY)
				return false;
			else if (isStaffInCourseByCourseID(courseID))
				return true;
			else if (assignment.getHidden()
					|| assignment.getStatus().equals(AssignmentBean.HIDDEN))
				return false;
			else if (isStudentInCourseByCourseID(courseID))
				return true;
			else if (isAuthenticated()) {
				return course.getAssignCCAccess();
			} else {
				return course.getAssignGuestAccess();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Returns whether the course allows guest(neither student nor admin of
	 * course) to view student course page
	 */
	public boolean allowGuestAccessByCourseID(long courseID) {
		try {
			CourseLocal course = database.courseHome().findByPrimaryKey(
					new CoursePK(courseID));
			return course.getCourseGuestAccess();
		} catch (Exception e) {
		}
		return false;
	}

	public boolean canSeeAllGroupsInAssignment(long assignID) {
		try {
			return canSeeAllGroupsInAssignment(database.assignmentHome()
					.findByAssignmentID(assignID));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean canSeeAllGroupsInAssignment(AssignmentLocal assign) {
		return canSeeAllGroupsInAssignment(assign, assign.getCourseID());
	}

	public boolean canSeeAllGroupsInAssignment(AssignmentLocal assign,
			long courseID) {
		boolean adminPriv = isAdminPrivByCourseID(courseID), gradePriv = isGradesPrivByCourseID(courseID), groupsPriv = isGroupsPrivByCourseID(courseID), isAssignedGraders = assign
				.getAssignedGraders();
		return adminPriv || groupsPriv || (!isAssignedGraders && gradePriv);
	}

	public boolean canSeeAllGradesInCourse(long courseID) {
		try {
			return isAdminPrivByCourseID(courseID)
					|| (isGroupsPrivByCourseID(courseID) && isGradesPrivByCourseID(courseID));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean canSeeAllGradesInAssignment(long assignID) {
		try {
			return canSeeAllGradesInAssignment(database.assignmentHome()
					.findByAssignmentID(assignID));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean canSeeAllGradesInAssignment(AssignmentLocal assign) {
		long courseID = assign.getCourseID();
		boolean adminPriv = isAdminPrivByCourseID(courseID), gradePriv = isGradesPrivByCourseID(courseID), groupsPriv = isGroupsPrivByCourseID(courseID), isAssignedGraders = assign
				.getAssignedGraders();
		return adminPriv || (gradePriv && (groupsPriv || !isAssignedGraders));
	}
}
