/*
 * Created on Feb 27, 2004
 */

package edu.cornell.csuglab.cms.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EntityBean;
import javax.ejb.FinderException;

import edu.cornell.csuglab.cms.author.Principal;

/**
 * @ejb.bean name="Course" jndi-name="CourseBean" type="BMP" view-type="local"
 * 
 * @jboss.method-attributes pattern="get*" read-only="true"
 * 
 * @ejb.dao class="edu.cornell.csuglab.cms.base.CourseDAO"
 *          impl-class="edu.cornell.csuglab.cms.base.dao.CourseDAOImpl"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS" res-type="javax.sql.Datasource"
 *                   res-auth="Container"
 * 
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS" jndi-name="java:/MSSQLDS"
 * 
 * 
 * @ejb.ejb-ref ejb-name="Assignment" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Assignment" jndi-name="Assignment"
 * 
 * @ejb.ejb-ref ejb-name="Student" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Student" jndi-name="Student"
 * 
 * @ejb.ejb-ref ejb-name="Staff" view-type="remote"
 * @jboss.ejb-ref-jndu ref-name="Staff" jndi-name="Staff"
 * 
 * @ejb.ejb-ref ejb-name="Announcement" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Announcement" jndi-name="Announcement"
 * 
 * @ejb.ejb-ref ejb-name="Category" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Category" jndi-name="Category"
 * 
 * @ejb.ejb-ref ejb-name="CategoryCol" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="CategoryCol" jndi-name="CategoryCol"
 * 
 * @ejb.ejb-ref ejb-name="CategoryRow" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="CategoryRow" jndi-name="CategoryRow"
 * 
 * @ejb.ejb-ref ejb-name="CategoryContents" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="CategoryContents" jndi-name="CategoryContents"
 * 
 * @ejb.ejb-ref ejb-name="CategoryFile" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="CategoryFile" jndi-name="CategoryFile"
 * 
 */
public abstract class CourseBean implements EntityBean {

	/*
	 * Do NOT set these fields to have default values upon instantiation. Since
	 * beans may not be newly created objects when ejbCreate is called we cannot
	 * rely on any of the values entered here.
	 */
	private long CourseID; // table primary key

	private String code; // the official code, e.g. "COM S 211"

	private String displayedCode; // what the professor wants, e.g. "CS 211"

	private String name; // e.g. "Intro to Programming"

	private String description; // e.g. "In this course you will learn about
								// foo"

	private boolean hasSection;

	private long semesterID;

	private boolean showFinalGrade;

	private boolean showTotalScores;

	private boolean showAssignWeights;

	private boolean showGraderNetID;

	private boolean freezeCourse;

	private long fileCounter;

	private boolean courseGuestAccess, assignGuestAccess, announceGuestAccess,
			solutionGuestAccess, courseCCAccess, assignCCAccess,
			announceCCAccess, solutionCCAccess;

	private Float maxTotalScore, highTotalScore, meanTotalScore,
			medianTotalScore, stDevTotalScore;

	private AssignmentLocalHome assignmentHome = null;

	private StudentLocalHome studentHome = null;

	private StaffLocalHome staffHome = null;

	private AnnouncementLocalHome announcementHome = null;

	private CategoryLocalHome categoryHome = null;

	private CategoryColLocalHome ctgColHome = null;

	private CategoryRowLocalHome ctgRowHome = null;

	private CategoryContentsLocalHome contentHome = null;

	private CategoryFileLocalHome ctgFileHome = null;

	/**
	 * @return Returns the code.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            The code to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return Returns the code.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public String getDisplayedCode() {
		return displayedCode;
	}

	/**
	 * @param code
	 *            The code to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setDisplayedCode(String displayedCode) {
		this.displayedCode = displayedCode;
	}

	/**
	 * @return Returns the description.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the ID.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public long getCourseID() {
		return CourseID;
	}

	/**
	 * @param id
	 *            The ID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setCourseID(long id) {
		CourseID = id;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return Returns the whether the course has sections
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true
	 */
	public boolean getHasSection() {
		return hasSection;
	}

	/**
	 * @param whether
	 *            this course has sections
	 * @ejb.interface-method view-type="local"
	 */
	public void setHasSection(boolean hasSection) {
		this.hasSection = hasSection;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return Returns the course's name.
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            The course name to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return Returns the semester.
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public long getSemesterID() {
		return semesterID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param semester
	 *            The semester to set.
	 */
	public void setSemesterID(long semesterID) {
		this.semesterID = semesterID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return Returns whether the final grade should be shown on the student
	 *         side or not.
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getShowFinalGrade() {
		return showFinalGrade;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param showFinalGrade
	 *            Whether the final grade should be shown on the student side or
	 *            not.
	 */
	public void setShowFinalGrade(boolean showFinalGrade) {
		this.showFinalGrade = showFinalGrade;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @return
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getShowTotalScores() {
		return showTotalScores;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param showTotalScores
	 */
	public void setShowTotalScores(boolean showTotalScores) {
		this.showTotalScores = showTotalScores;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @return
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getShowAssignWeights() {
		return showAssignWeights;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param showAssignWeights
	 */
	public void setShowAssignWeights(boolean showAssignWeights) {
		this.showAssignWeights = showAssignWeights;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return Returns whether the grader who gave a group's score should be
	 *         shown on the student side or not.
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getShowGraderNetID() {
		return showGraderNetID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param showGraderNetID
	 *            Whether the grader who gave a group's score should be shown on
	 *            the student side or not.
	 */
	public void setShowGraderNetID(boolean showGraderNetID) {
		this.showGraderNetID = showGraderNetID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return Returns if the course grading is frozen or not.
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getFreezeCourse() {
		return freezeCourse;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param freezeCourse
	 *            Sets whether the course grading should be frozen or not.
	 */
	public void setFreezeCourse(boolean freezeCourse) {
		this.freezeCourse = freezeCourse;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public long getFileCounter() {
		return fileCounter;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param assignmentFileCounter
	 */
	public void setFileCounter(long fileCounter) {
		this.fileCounter = fileCounter;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return Returns the announceGuestAccess.
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getAnnounceGuestAccess() {
		return announceGuestAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param announceGuestAccess
	 *            The announceGuestAccess to set.
	 */
	public void setAnnounceGuestAccess(boolean announceGuestAccess) {
		this.announceGuestAccess = announceGuestAccess;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return Returns the assignGuestAccess.
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getAssignGuestAccess() {
		return assignGuestAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param assignGuestAccess
	 *            The assignGuestAccess to set.
	 */
	public void setAssignGuestAccess(boolean assignGuestAccess) {
		this.assignGuestAccess = assignGuestAccess;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return Returns the courseGuestAccess.
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getCourseGuestAccess() {
		return courseGuestAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param courseGuestAccess
	 *            The courseGuestAccess to set.
	 */
	public void setCourseGuestAccess(boolean courseGuestAccess) {
		this.courseGuestAccess = courseGuestAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @return
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getSolutionGuestAccess() {
		return solutionGuestAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param solutionGuestAccess
	 */
	public void setSolutionGuestAccess(boolean solutionGuestAccess) {
		this.solutionGuestAccess = solutionGuestAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @return True if this course allows access to Cornell community members
	 *         who are authenticated but not students or staff
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getCourseCCAccess() {
		return courseCCAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param courseCCAccess
	 */
	public void setCourseCCAccess(boolean courseCCAccess) {
		this.courseCCAccess = courseCCAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @return True if this course allows access to it's assignments to Cornell
	 *         community members who are authenticated but not students or staff
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getAssignCCAccess() {
		return assignCCAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 */
	public void setAssignCCAccess(boolean assignCCAccess) {
		this.assignCCAccess = assignCCAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @return True if this course allows access to it's announcements to
	 *         Cornell community members who are authenticated but not students
	 *         or staff
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getAnnounceCCAccess() {
		return announceCCAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 */
	public void setAnnounceCCAccess(boolean announceCCAccess) {
		this.announceCCAccess = announceCCAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @return
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getSolutionCCAccess() {
		return solutionCCAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param solutionCCAccess
	 */
	public void setSolutionCCAccess(boolean solutionCCAccess) {
		this.solutionCCAccess = solutionCCAccess;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public Float getMaxTotalScore() {
		return maxTotalScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 */
	public void setMaxTotalScore(Float maxTotalScore) {
		this.maxTotalScore = maxTotalScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public Float getHighTotalScore() {
		return highTotalScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 */
	public void setHighTotalScore(Float highTotalScore) {
		this.highTotalScore = highTotalScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public Float getMeanTotalScore() {
		return meanTotalScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 */
	public void setMeanTotalScore(Float meanTotalScore) {
		this.meanTotalScore = meanTotalScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public Float getMedianTotalScore() {
		return medianTotalScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 */
	public void setMedianTotalScore(Float medianTotalScore) {
		this.medianTotalScore = medianTotalScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public Float getStDevTotalScore() {
		return stDevTotalScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 */
	public void setStDevTotalScore(Float stDevTotalScore) {
		this.stDevTotalScore = stDevTotalScore;
	}

	/**
	 * Checks to see if the given primary key can be found in the database
	 * 
	 * @param pk
	 *            The primary key being serarched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public CoursePK ejbFindByPrimaryKey(CoursePK pk) throws FinderException {
		return null;
	}

	/**
	 * Finds all the courses in the current semester for which a given netid is
	 * enrolled in as a student
	 * 
	 * @param netid
	 *            The student's netid.
	 * @return The collection of all the Course objects
	 * @throws FinderException
	 */
	public Collection ejbFindStudentCourses(String netid)
			throws FinderException {
		return null;
	}

	/**
	 * Finds all the courses in the current semester for which a given netid is
	 * listed as a staff in.
	 * 
	 * @param netid
	 *            The staff member's netid
	 * @return The collection of all the Course objects
	 * @throws FinderException
	 */
	public Collection ejbFindStaffCourses(String netid) throws FinderException {
		return null;
	}

	/**
	 * Finds all the courses in the current semester for which a given netid is
	 * enrolled in as a student
	 * 
	 * @param netid
	 *            The student's netid.
	 * @return The collection of all the Course objects
	 * @throws FinderException
	 */
	public Collection ejbFindStudentCoursesBySemester(String netid,
			long semesterID) throws FinderException {
		return null;
	}

	/**
	 * Finds all the courses in the current semester for which a given netid is
	 * listed as a staff in.
	 * 
	 * @param netid
	 *            The staff member's netid
	 * @return The collection of all the Course objects
	 * @throws FinderException
	 */
	public Collection ejbFindStaffCoursesBySemester(String netid,
			long semesterID) throws FinderException {
		return null;
	}

	/**
	 * Finds all courses which this netid is a staff member with full admin
	 * privileges in
	 * 
	 * @param netid
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindStaffAdminCourses(long semesterID, String netid)
			throws FinderException {
		return null;
	}

	/**
	 * Returns a collection of all courses in the current semester which allow
	 * access to the course page to guests
	 * 
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindGuestAccess() throws FinderException {
		return null;
	}

	/**
	 * Returns a collection of all courses in the given semester which allow
	 * access to the course page to guests
	 * 
	 * @param semesterID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindGuestAccessBySemester(long semesterID)
			throws FinderException {
		return null;
	}

	/**
	 * Returns a collection of all courses in the current semester which allow
	 * access to the course page to authenticated Cornell communtity members
	 * 
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindCCAccess() throws FinderException {
		return null;
	}

	/**
	 * Returns a collection of all courses in the given semester which allow
	 * access to the course page to authenticated Cornell community members
	 * 
	 * @param semesterID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindCCAccessBySemester(long semesterID)
			throws FinderException {
		return null;
	}

	/**
	 * Finds all the courses under the given semester.
	 * 
	 * @param semID
	 *            The ID of the desired semester
	 * @return A collection of CoursePK objects
	 * @throws FinderException
	 */
	public Collection ejbFindBySemesterID(long semID) throws FinderException {
		return null;
	}

	/**
	 * Finds a course given an assignment in the system
	 * 
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 */
	public CoursePK ejbFindByAssignmentID(long assignmentID)
			throws FinderException {
		return null;
	}

	/**
	 * Finds the course with the given ID
	 * 
	 * @throws FinderException
	 *             The course does not exist
	 */
	public CoursePK ejbFindByCourseID(long courseID) throws FinderException {
		return null;
	}

	/**
	 * Finds the course with the given code in the given semester
	 * 
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseCodeAndSemester(String code, long semID)
			throws FinderException {
		return null;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 */
	public CourseData getCourseData() {
		return new CourseData(getCode(), getDisplayedCode(), getDescription(),
				getCourseID(), getHasSection(), getName(), getSemesterID(),
				getShowFinalGrade(), getShowTotalScores(),
				getShowAssignWeights(), getShowGraderNetID(),
				getFreezeCourse(), getFileCounter(), getAnnounceGuestAccess(),
				getAssignGuestAccess(), getCourseGuestAccess(),
				getSolutionGuestAccess(), getCourseCCAccess(),
				getAssignCCAccess(), getAnnounceCCAccess(),
				getSolutionCCAccess(), getMaxTotalScore(), getHighTotalScore(),
				getMeanTotalScore(), getMedianTotalScore(),
				getStDevTotalScore());
	}

	/**
	 * Return all assignment associated with this course
	 * 
	 * @ejb.interface-method view-type="local"
	 * @return Collection of Assignment objects
	 */
	public Collection getAssignments() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = assignmentHome().findByCourseID(getCourseID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((AssignmentLocal) i.next()).getAssignmentData());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Return all students associated with this course
	 * 
	 * @ejb.interface-method view-type="local"
	 * @return Collection of StudentData objects
	 */
	public Collection getStudents() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = studentHome().findByCourseIDSortByLastName(getCourseID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((StudentLocal) i.next()).getStudentData());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Return all students active in this course
	 * 
	 * @ejb.interface-method view-type="local"
	 * @return Collection of StudentData objects
	 */
	public Collection getActiveStudents() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = studentHome().findByCourseIDSortByLastName(getCourseID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				StudentLocal student = (StudentLocal) i.next();
				if (student.getStatus().equals(StudentBean.ENROLLED))
					result.add(student.getStudentData());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Returns all students in the course who are missing at least one of the
	 * FirstName and LastName fields in the tUser table
	 * 
	 * @return Collection of StudentData objects
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getEmptyNameStudents() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = studentHome().findEmptyNameByCourseID(getCourseID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((StudentLocal) i.next()).getStudentData());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Return all staff associated with this course
	 * 
	 * @ejb.interface-method view-type="local"
	 * @return Collection of StaffData object
	 */
	public Collection getStaff() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = staffHome().findByCourseID(getCourseID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((StaffLocal) i.next()).getStaffData());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * @param p
	 *            The principal for whom to get data
	 * @return List of CategoryDatas ordered by position on course page
	 * @ejb.interface-method view-type="local"
	 */
	public List getCategories(Principal p) {
		try {
			Collection c = categoryHome().findByCourseID(getCourseID(), p);
			Iterator i = c.iterator();
			List ctgData = new ArrayList();
			while (i.hasNext()) {
				CategoryData ctg = ((CategoryLocal) i.next()).getCategoryData();
				ctgData.add(ctg);
			}
			return ctgData;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param p
	 *            The principal for whom to get data
	 * @return List of CategoryColDatas ordered by position of parent category,
	 *         then position within parent category
	 * @ejb.interface-method view-type="local"
	 */
	public List getCtgColumns(Principal p) {
		try {
			Collection c = ctgColHome().findByCourseID(getCourseID(), p);
			Iterator i = c.iterator();
			List colData = new ArrayList();
			while (i.hasNext()) {
				CategoryColData col = ((CategoryColLocal) i.next())
						.getCategoryColData();
				colData.add(col);
			}
			return colData;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Note that rows are unlike columns in that there aren't rows that are only
	 * visible to staff
	 * 
	 * @param p
	 *            The principal for whom to get data
	 * @return List of CategoryRowDatas ordered by position of parent category
	 * @ejb.interface-method view-type="local"
	 */
	public List getCtgRows(Principal p) {
		try {
			Collection c = ctgRowHome().findByCourseID(getCourseID());
			Iterator i = c.iterator();
			List rowData = new ArrayList();
			while (i.hasNext()) {
				CategoryRowData row = ((CategoryRowLocal) i.next())
						.getCategoryRowData();
				rowData.add(row);
			}
			return rowData;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param p
	 *            The principal for whom to get data
	 * @return List of CategoryContentsDatas ordered by category ID
	 * @ejb.interface-method view-type="local"
	 */
	public List getCtgContents(Principal p) {
		try {
			Collection c = contentHome().findByCourseID(getCourseID(), p);
			Iterator i = c.iterator();
			List contentData = new ArrayList();
			while (i.hasNext()) {
				CategoryContentsData content = ((CategoryContentsLocal) i
						.next()).getCategoryContentsData();
				contentData.add(content);
			}
			return contentData;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param p
	 *            The principal for whom to get data
	 * @return List of CategoryFileDatas ordered by content ID
	 * @ejb.interface-method view-type="local"
	 */
	public List getCtgFiles(Principal p) {
		try {
			Collection c = ctgFileHome().findByCourseID(getCourseID(), p);
			Iterator i = c.iterator();
			List fileData = new ArrayList();
			while (i.hasNext()) {
				CategoryFileData file = ((CategoryFileLocal) i.next())
						.getCategoryFileData();
				fileData.add(file);
			}
			return fileData;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Return all announcements from the past week associated with this course
	 * 
	 * @ejb.interface-method view-type="local"
	 * @return Collection of Annoucement objects
	 */
	public Collection getAnnouncements() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = announcementHome().findByCourseID(getCourseID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result
						.add(((AnnouncementLocal) i.next())
								.getAnnouncementData());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Gets all assignment source files for all assignments in this courses.
	 * 
	 * @return A collection of FileDatas.
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getAllAssignmentFiles() {
		Collection result = new ArrayList();
		//try {
		//	/*
		//	 * c= assignmentFileHome().findByCourseID(getCourseID()); Iterator
		//	 * i= c.iterator(); while(i.hasNext()) {
		//	 * result.add(((File)i.next()).getFileData()); }
		//	 */
		//} catch (Exception e) {
		//	e.printStackTrace();
		//}
		return result;
	}

	/**
	 * Caches and returns AssignmentHome
	 * 
	 * @return AssignmentHome
	 */
	private AssignmentLocalHome assignmentHome() {
		try {
			if (assignmentHome == null) {
				assignmentHome = AssignmentUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assignmentHome;
	}

	/**
	 * Caches and returns StudentHome
	 * 
	 * @return StudentHome
	 */
	private StudentLocalHome studentHome() {
		try {
			if (studentHome == null) {
				studentHome = StudentUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return studentHome;
	}

	/**
	 * Caches and returns StaffHome
	 * 
	 * @return StaffHome
	 */
	private StaffLocalHome staffHome() {
		try {
			if (staffHome == null) {
				staffHome = StaffUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return staffHome;
	}

	/**
	 * Caches and returns AnnouncementHome
	 * 
	 * @return AnnouncementHome
	 */
	private AnnouncementLocalHome announcementHome() {
		try {
			if (announcementHome == null) {
				announcementHome = AnnouncementUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return announcementHome;
	}

	/**
	 * Caches and returns CategoryHome
	 * 
	 * @return
	 */

	private CategoryLocalHome categoryHome() {
		try {
			if (categoryHome == null) {
				categoryHome = CategoryUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return categoryHome;
	}

	/**
	 * Caches and returns CategoryColHome
	 * 
	 * @return
	 */
	private CategoryColLocalHome ctgColHome() {
		try {
			if (ctgColHome == null) {
				ctgColHome = CategoryColUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ctgColHome;

	}

	/**
	 * Caches and returns CategoryRowHome
	 * 
	 * @return
	 */
	private CategoryRowLocalHome ctgRowHome() {
		try {
			if (ctgRowHome == null)
				ctgRowHome = CategoryRowUtil.getLocalHome();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ctgRowHome;
	}

	/**
	 * Caches and returns CategoryFileHome
	 * 
	 * @return
	 */
	private CategoryFileLocalHome ctgFileHome() {
		try {
			if (ctgFileHome == null)
				ctgFileHome = CategoryFileUtil.getLocalHome();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ctgFileHome;
	}

	private CategoryContentsLocalHome contentHome() {
		try {
			if (contentHome == null) {
				contentHome = CategoryContentsUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contentHome;
	}

	/**
	 * Creates a new course with the given name, description, code, and email.
	 * 
	 * @param name
	 *            The course's name (eg. Introduction to Algorithms)
	 * @param description
	 *            The course's description
	 * @param code
	 *            The course code (eg. CS 211)
	 * @param email
	 *            The course email
	 * @return The course object
	 * @throws javax.ejb.CreateException
	 * 
	 * @ejb.create-method view-type="local"
	 */
	public CoursePK ejbCreate(String name, String description, String code,
			long semesterID) throws javax.ejb.CreateException {
		setName(name);
		setDescription(description);
		setCode(code);
		setDisplayedCode(code);
		setSemesterID(semesterID);
		setAnnounceGuestAccess(false);
		setAssignGuestAccess(false);
		setCourseGuestAccess(false);
		setSolutionGuestAccess(false);
		setFileCounter(1);
		setFreezeCourse(false);
		setShowFinalGrade(false);
		setShowTotalScores(false);
		setShowAssignWeights(false);
		setShowGraderNetID(false);
		setCourseCCAccess(false);
		setAssignCCAccess(false);
		setAnnounceCCAccess(false);
		setSolutionCCAccess(false);
		setMaxTotalScore(null);
		setHasSection(false);
		setHighTotalScore(null);
		setMeanTotalScore(null);
		setMedianTotalScore(null);
		setStDevTotalScore(null);
		return null;
	}
}
