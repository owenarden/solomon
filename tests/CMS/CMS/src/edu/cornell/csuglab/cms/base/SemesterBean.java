/*
 * Created on Sep 6, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */

package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EntityBean;
import javax.ejb.FinderException;

/**
 * @ejb.bean name="Semester"
 *	jndi-name="SemesterBean"
 *	type="BMP" 
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.SemesterDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.SemesterDAOImpl"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 **/
public abstract class SemesterBean implements EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long semesterID;
	private String semesterName;
	private String termCode;
	private boolean hidden;
	
	/**
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getSemesterID() {
		return semesterID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param semesterID
	 */
	public void setSemesterID(long semesterID) {
		this.semesterID = semesterID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getSemesterName() {
		return semesterName;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param semesterName
	 */
	public void setSemesterName(String semesterName) {
		this.semesterName = semesterName;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getTermCode() {
		return termCode;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param semesterName
	 */
	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public boolean getHidden()
	{
		return hidden;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param hidden
	 */
	public void setHidden(boolean hidden)
	{
		this.hidden = hidden;
	}
	
	/**
	 * @param key
	 * @return
	 * @throws FinderException
	 */
	public SemesterPK ejbFindByPrimaryKey(SemesterPK key) throws FinderException {
		return null;
	}
	
	/**
	 * @return A Collection of Semester objects
	 * @throws FinderException
	 */
	public Collection ejbFindAllSemesters() throws FinderException {
		return null;
	}
	
	/**
	 * Finds all semesters during which a given user was active (either as a 
	 * staff or student) in at least one course
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByUser(String netID) throws FinderException {
	    return null;
	}
	
	/**
	 * @return Returns the current semester
	 * @throws FinderException
	 */
	public SemesterPK ejbFindCurrent() throws FinderException {
	    return null;
	}
	
	/** 
	 * @return
	 */
	public SemesterData getSemesterData() {
		return new SemesterData(getSemesterID(), getSemesterName(), getTermCode(), getHidden());
	}
	
	/**
	 * @param semesterName
	 * @return
	 * @throws CreateException
	 * @ejb.create-method view-type="local"
	 */
	public SemesterPK ejbCreate(String semesterName) throws CreateException {
		setSemesterName(semesterName);
		setHidden(false);
		return null;
	}
	
}
