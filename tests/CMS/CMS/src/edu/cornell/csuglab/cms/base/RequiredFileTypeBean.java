/*
 * Created on Nov 4, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

/**
 * Contains information pertaining to files that students will need to 
 * submit for an assignment. Has the assignment's ID, the filename 
 * that the file will be named to in the server's file-system, and 
 * submission limitations.
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.bean name="RequiredFileType"
 *	jndi-name="RequiredFileTypeBean"
 *	type="BMP" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.RequiredFileTypeDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.RequiredFileTypeDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/
public abstract class RequiredFileTypeBean implements javax.ejb.EntityBean {

    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long submissionID;
	private String fileType;
	
	/**
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getSubmissionID() {
		return submissionID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param submissionID
	 */
	public void setSubmissionID(long submissionID) {
		this.submissionID = submissionID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the file type of the file to be submitted
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param type The file type of the file to be submitted
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @return
	 */
	public RequiredFileTypeData getRequiredFileTypeData() {
		return new RequiredFileTypeData(getSubmissionID(), getFileType());
	}
	
	/**
	 * @param pk
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public RequiredFileTypePK ejbFindByPrimaryKey(RequiredFileTypePK pk) throws FinderException {
		return null;
	}
	
	/**
	 * @param submissionID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection ejbFindBySubmissionID(long submissionID) throws FinderException {
		return null;
	}

	/**
	 * @return
	 * @throws CreateException
	 * @ejb.create-method view-type="local"
	 */
	public RequiredFileTypePK ejbCreate(long submissionID, String fileType) throws CreateException {
		setSubmissionID(submissionID);
		setFileType(fileType);
		return null;
	}

}
