/*
 * Created on Mar 17, 2004
 */

package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.GroupBean;
import edu.cornell.csuglab.cms.base.GroupDAO;
import edu.cornell.csuglab.cms.base.GroupMemberBean;
import edu.cornell.csuglab.cms.base.GroupPK;
import edu.cornell.csuglab.cms.base.StudentBean;

/**
 * @author Theodore Chao
 */
public class GroupDAOImpl extends DAOMaster implements GroupDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupDAO#load(edu.cornell.csuglab.cms.base.GroupPK,
	 *      edu.cornell.csuglab.cms.base.GroupBean)
	 */
	public void load(GroupPK pk, GroupBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tgroups where GroupID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getGroupID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setGroupID(rs.getLong("GroupID"));
				ejb.setAssignmentID(rs.getLong("AssignmentID"));
				ejb.setExtension(rs.getTimestamp("Extension"));
				ejb.setFileCounter(rs.getInt("FileCounter"));
				ejb.setRemainingSubmissions(rs.getInt("RemainingSubmissions"));
				ejb.setAdjustment(rs.getString("Adjustment"));
				long tsid = rs.getLong("TimeslotID");
				if (rs.wasNull()) {
				    ejb.setTimeSlotID(null);
				} else {
				    ejb.setTimeSlotID(new Long(tsid));
				}
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupDAO#store(edu.cornell.csuglab.cms.base.GroupBean)
	 */
	public void store(GroupBean ejb) throws EJBException {
		preStore("Group", ejb);
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "update tgroups "
					+ "set latestsubmission = ?, extension = ?, filecounter = ?, remainingsubmissions = ?, timeslotid = ?, adjustment = ? "
					+ "where groupid = ? and assignmentid = ?";
			ps = conn.prepareStatement(update);
			ps.setTimestamp(1, ejb.getLatestSubmission());
			if (ejb.getExtension() == null) {
			    ps.setNull(2, Types.TIMESTAMP);
			} else {
			    ps.setTimestamp(2, ejb.getExtension());
			}
			ps.setInt(3, ejb.getFileCounter());
			ps.setInt(4, ejb.getRemainingSubmissions());
			if (ejb.getTimeSlotID() == null) {
			    ps.setNull(5, Types.BIGINT);
			} else {
			    ps.setLong(5, ejb.getTimeSlotID().longValue());
			}
			if(ejb.getAdjustment() == null) {
				ps.setNull(6, Types.VARCHAR);
			} else {
				ps.setString(6, ejb.getAdjustment());
			}
			ps.setLong(7, ejb.getGroupID());
			ps.setLong(8, ejb.getAssignmentID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupDAO#remove(edu.cornell.csuglab.cms.base.GroupPK)
	 */
	public void remove(GroupPK pk) throws RemoveException, EJBException {
		preRemove("Group");
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "delete from tgroups where GroupID = ?";
			ps = conn.prepareStatement(update);
			ps.setLong(1, pk.getGroupID());
			ps.executeUpdate();
			conn.close();
			ps.close();
        } catch (Exception e) {
        	try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupDAO#create(edu.cornell.csuglab.cms.base.GroupBean)
	 */
	public GroupPK create(GroupBean ejb) throws CreateException, EJBException {
		preCreate("Group", ejb);
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		GroupPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "insert into tGroups "
					+ "(assignmentid, remainingsubmissions) values (?, ?)";
			String findString = "select @@identity as 'GroupID' from tGroups";
			ps = conn.prepareStatement(createString);
			ps.setLong(1, ejb.getAssignmentID());
			ps.setInt(2, ejb.getRemainingSubmissions());
			ps.executeUpdate();
			rs = conn.prepareStatement(findString).executeQuery();
			if (rs.next()) {
				long groupID = rs.getLong(1);
				result = new GroupPK(groupID);
				ejb.setGroupID(result.getGroupID());
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.GroupPK)
	 */
	public GroupPK findByPrimaryKey(GroupPK key) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select GroupID from tgroups where GroupID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, key.getGroupID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find group with ID = " + key.getGroupID());
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return key;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupDAO#findByAssignmentID(long)
	 */
	public Collection findByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT g.GroupID FROM tGroups g INNER JOIN " +
				"tGroupMembers m ON g.GroupID = m.GroupID INNER JOIN " +
				"tAssignment a ON g.AssignmentID = a.AssignmentID INNER JOIN " +
				"tStudent s ON m.NetID = s.NetID AND a.CourseID = s.CourseID INNER JOIN " +
				"tUser u ON m.NetID = u.NetID " +
				"WHERE (s.Status = ?) AND (m.Status = ?) AND (a.AssignmentID = ?) " +
				"ORDER BY u.LastName, u.FirstName, m.NetID";
			ps = conn.prepareStatement(queryString);
		    ps.setString(1, StudentBean.ENROLLED);
			ps.setString(2, GroupMemberBean.ACTIVE);
			ps.setLong(3, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GroupPK(rs.getLong("GroupID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findLateByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT DISTINCT g.GroupID FROM tGroups g INNER JOIN " +
                      "tGroupMembers m ON g.GroupID = m.GroupID INNER JOIN " +
                      "tAssignment a ON g.AssignmentID = a.AssignmentID INNER JOIN " +
                      "tStudent s ON m.NetID = s.NetID AND a.CourseID = s.CourseID INNER JOIN " +
                      "tSubmittedFiles f ON f.GroupID = m.GroupID " +
					  "WHERE (s.Status = ?) AND (m.Status = ?) AND (a.AssignmentID = ?) AND (f.LateSubmission = ?) " +
					  "GROUP BY g.GroupID";
			ps = conn.prepareStatement(queryString);
		    ps.setString(1, StudentBean.ENROLLED);
			ps.setString(2, GroupMemberBean.ACTIVE);
			ps.setLong(3, assignmentID);
			ps.setBoolean(4, true);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GroupPK(rs.getLong("GroupID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupDAO#findByGroupID(long)
	 */
	public GroupPK findByGroupID(long groupID) throws FinderException {
		return findByPrimaryKey(new GroupPK(groupID));
	}
	
	public Collection findByNetIDsAssignmentID(Collection netIDs, long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (netIDs.size() == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select g.GroupID from tGroups g " +
				"inner join tGroupMembers m on g.GroupID = m.GroupID where " +
				"g.AssignmentID = ? AND (";
			for (int i=0; i < netIDs.size() - 1; i++) {
			    queryString += "m.NetID = ? OR ";
			}
			queryString += "m.NetID = ?)";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentID);
			Iterator i = netIDs.iterator();
			int count = 2;
			while (i.hasNext()) {
			    String netID = (String) i.next();
			    ps.setString(count++, netID);
			}
			rs = ps.executeQuery();
			while (rs.next()) {
			    result.add(new GroupPK(rs.getLong("GroupID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findByGroupIDs(Collection groupIDs) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (groupIDs.size() == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select GroupID from tGroups where (";
			for (int i=0; i < groupIDs.size() - 1; i++) {
			    queryString += "GroupID = ? OR ";
			}
			queryString += "GroupID = ?)";
			ps = conn.prepareStatement(queryString);
			Iterator i = groupIDs.iterator();
			int count = 1;
			while (i.hasNext()) {
			    Long groupID = (Long) i.next();
			    ps.setLong(count++, groupID.longValue());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
			    result.add(new GroupPK(rs.getLong("GroupID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupDAO#findByNetIDCourseID(java.lang.String,
	 *      long)
	 */
	public Collection findByNetIDCourseID(String netID, long courseID)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT g.GroupID " +
				"FROM tGroupMembers m INNER JOIN " +
                	"tGroups g ON m.GroupID = g.GroupID INNER JOIN " +
                    "tAssignment a ON g.AssignmentID = a.AssignmentID " +
                "WHERE (a.CourseID = ?) AND (m.NetID = ?) AND (m.Status = ?) AND (a.Hidden = ?) " +
                "ORDER BY a.DueDate ASC";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setString(2, netID);
			ps.setString(3, GroupMemberBean.ACTIVE);
			ps.setBoolean(4, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				GroupPK pk = new GroupPK(rs.getLong(1));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findByTimeSlotID(long timeslotID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT g.GroupID FROM tGroups g INNER JOIN " +
				"tGroupMembers m ON g.GroupID = m.GroupID INNER JOIN " +
				"tTimeslot a ON g.TimeslotID = a.TimeslotID INNER JOIN " +
				"tAssignment q ON a.AssignmentID = q.AssignmentID INNER JOIN " +
				"tStudent s ON m.NetID = s.NetID AND q.CourseID = s.CourseID INNER JOIN " +
				"tUser u ON m.NetID = u.NetID " +
				"WHERE (s.Status = ?) AND (m.Status = ?) AND (a.TimeslotID = ?) " +
				"ORDER BY u.LastName, u.FirstName, m.NetID";
			ps = conn.prepareStatement(queryString);
		    ps.setString(1, StudentBean.ENROLLED);
			ps.setString(2, GroupMemberBean.ACTIVE);
			ps.setLong(3, timeslotID);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GroupPK(rs.getLong("GroupID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupDAO#findByNetIDAssignmentID(java.lang.String,
	 *      long)
	 */
	public GroupPK findByNetIDAssignmentID(String netID, long assignmentID)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		GroupPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select g.GroupID from tgroups g "
					+ "inner join tgroupmembers gm on g.groupid = gm.groupid "
					+ "where gm.netid = ? and g.assignmentID = ? and gm.status = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netID);
			ps.setLong(2, assignmentID);
			ps.setString(3, GroupMemberBean.ACTIVE);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = new GroupPK(rs.getLong(1));
			} else {
				throw new FinderException("Could not find group for NetID = " + netID + 
						", AssignmentID = " + assignmentID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupDAO#findInvitedByNetIDAssignmentID(java.lang.String,
	 *      long)
	 */
	public Collection findInvitedByNetIDAssignmentID(String netID,
			long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select g.GroupID from tgroups g "
					+ "inner join tgroupmembers gm on g.groupid = gm.groupid "
					+ "where gm.netid = ? and g.assignmentID = ? and gm.status = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netID);
			ps.setLong(2, assignmentID);
			ps.setString(3, GroupMemberBean.INVITED);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GroupPK(rs.getLong(1)));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

}
