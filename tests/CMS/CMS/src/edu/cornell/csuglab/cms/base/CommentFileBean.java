/*
 * Created on Mar 15, 2005
 */
package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

 /**
 * @ejb.bean name="CommentFile"
 *	jndi-name="CommentFile"
 *	type="BMP"
 * 
 * @ejb.dao class="edu.cornell.csuglab.cms.base.CommentFileDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.CommentFileDAOImpl"
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 * @generated
 **/
public abstract class CommentFileBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long commentFileID;
	private long commentID;
	private String fileName;
	private String path;
	
	/**
	 * @return
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getCommentFileID() {
		return commentFileID;
	}
	
	/**
	 * @param commentFileID
	 * @ejb.interface-method view-type="local"
	 */
	public void setCommentFileID(long commentFileID) {
		this.commentFileID = commentFileID;
	}
	
	/**
	 * @return Returns the commentID.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getCommentID() {
		return commentID;
	}
	
	/**
	 * @param commentID The commentID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setCommentID(long commentID) {
		this.commentID = commentID;
	}
	
	/**
	 * @return Returns the fileName.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getFileName() {
		return fileName;
	}
	
	/**
	 * @param fileName The fileName to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getPath() {
		return path;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileCounter
	 */
	public void setPath(String path) {
		this.path = path;
	}
	
	/**
	 * @return
	 * @throws CreateException
	 * @ejb.create-method view-type="local"
	 */
	public CommentFilePK ejbCreate(long commentID, String fileName, String path) throws CreateException {
		setCommentID(commentID);
		setFileName(fileName);
		setPath(path);
		return null;
	}
	
	/**
	 * @param pk
	 * @return
	 */
	public CommentFilePK ejbFindByPrimaryKey(CommentFilePK pk) throws FinderException {
		return null;
	}

	/**
	 * @param commentID
	 * @return
	 */
	public Collection ejbFindByCommentID(long commentID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all CommentFiles belonging to non-hidden comments for 
	 * any of the given groups
	 * @param groupids
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupIDs(Collection groupids) throws FinderException {
		return null;
	}
	
	/**
	 * Returns the file system path pointing to the CommentFile which this
	 * bean represents.
	 * @return
	 * @ejb.interface-method view-type="local"
	 * FIXME do we need this? see cms.util.FileUtil
	public String getFilePath() {
		return Util.FILES_DIR + Util.SLASH + "CommentFiles" + Util.SLASH + 
				getCourseID() + Util.SLASH + getAssignmentID() + Util.SLASH + 
				getGroupID() + Util.SLASH + getFileCounter() + Util.SLASH + getFileName();
	}*/
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public CommentFileData getCommentFileData() {
		return new CommentFileData(getCommentFileID(), getCommentID(), getFileName(),
				getPath());
	}
	
   /**
    * <!-- begin-user-doc -->
    * The container invokes this method immediately after it calls ejbCreate.
    * <!-- end-user-doc -->
    * 
    * @generated
    */
   public void ejbPostCreate() throws javax.ejb.CreateException {
     // begin-user-code
     // end-user-code
   }

}
