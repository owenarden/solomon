/*
 * Created on Oct 7, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.FinderException;

import edu.cornell.csuglab.cms.author.Principal;


 /**
 * @ejb.bean name="Category"
 *	jndi-name="CategoryBean"
 *	type="BMP"
 *
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.CategoryDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.CategoryDAOImpl"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 * 
 * @ejb.ejb-ref ejb-name="CategoryCol" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="CategoryCol" jndi-name="CategoryCol"
 * 
 * @ejb.ejb-ref ejb-name="CategoryContents" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="CategoryContents" jndi-name="CategoryContents"
 * 
 * @ejb.ejb-ref ejb-name="CategoryFile" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="CategoryFile" jndi-name="CategoryFile"
 *
 ***/

public abstract class CategoryBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
    private long categoryID;
	private long courseID;
	private String categoryName;
	private boolean hidden;
	private boolean ascending;
	private long sortByColId;
	private long numShowContents;
	private long fileCount;
	private int authorzn;
	private int positn;
	/*
	 * Announcements must be treated differently from other categories;
	 * set this to true if this category holds announcements.
	 * Note course description and assignments aren't currently handled
	 * as categories. - Evan, 10 / 27 / 05
	 */
	private boolean announcements;
	
	private CategoryColLocalHome columnHome = null;
	private CategoryContentsLocalHome contentsHome = null;
	private CategoryFileLocalHome filesHome = null;

	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the category's unique identifying long
	 */
	public long getCategoryID(){
		return this.categoryID;
	}
	
	/**
	 * ejb.interface-method view-type="local"
	 * @param id Sets the category's unique identifying long
	 */
	public void setCategoryID(long id){
		this.categoryID = id;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the course id that this category is for
	 */
	public long getCourseID(){
		return this.courseID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param courseID Sets the courseID that this category is for
	 */
	public void setCourseID(long courseID){
		this.courseID = courseID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the category name
	 */
	public String getCategoryName(){
		return this.categoryName;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param categoryName Sets the name of the category
	 */
	public void setCategoryName(String categoryName){
		this.categoryName = categoryName;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return True if category contents return in order of ascending release date, false otherwise
	 */
	public boolean getAscending(){
		return this.ascending;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param ascending Sets the Ascending boolean of category
	 */
	public void setAscending(boolean ascending){
		this.ascending = ascending;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the unique identifying colID to sort by
	 */
	public long getSortByColId(){
		return this.sortByColId; 
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param colID
	 */
	public void setSortByColId(long colID){
		this.sortByColId = colID;
	}
	
		/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the number of items to display
	 */
	public long getNumShowContents()
	{
		return this.numShowContents;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param numShow
	 */
	public void setNumShowContents(long numShow)
	{
		this.numShowContents = numShow;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return true if the category should be hidden, false otherwise
	 */
	
	public boolean getHidden(){
		return this.hidden;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param hidden
	 */
	public void setHidden(boolean hidden){
		this.hidden = hidden;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getFileCount(){
		return this.fileCount;
	}
	
	/**
	  * @ejb.interface-method view-type="local"
	 * @param fileCount
	 */
	public void setFileCount(long fileCount){
		this.fileCount = fileCount;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public int getAuthorzn(){
		return this.authorzn;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param authorzn
	 */
	public void setAuthorzn(int authorzn){
		this.authorzn = authorzn;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public int getPositn(){
		return this.positn;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param positn
	 */
	public void setPositn(int positn){
		this.positn = positn;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public boolean getIsAnnouncements(){
		return this.announcements;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param special
	 */
	public void setIsAnnouncements(boolean announcements){
		this.announcements = announcements;
	}
	
	/**
	 * Checks to see if the given primary key can be found in the database
	 * @param pk The primary key being searched for
	 * @return The key passed in if it's found
	 * @throws FinderException
	 */
	public CategoryPK ejbFindByPrimaryKey(CategoryPK pk) throws FinderException{
		return null;
	}
	
	/**
	 * Collection of categories for a course(visible)
	 * @param courseID The courseID
	 * @return A collection of all the categories for the course,in increasing order of the 
	 * unique ID
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseID(long courseID, Principal p) throws FinderException{
		return null;
	}
	
	/**
	 * Collection of categories for a course(visible and hidden)
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseIDAll(long courseID) throws FinderException{
		return null;
	}

	
	/**
	 * @ejb.interface-method view-type="local"
	 */
	public CategoryData getCategoryData(){
		return new CategoryData(getCategoryID(), getCourseID(),
				getCategoryName(), getAscending(),getSortByColId(), 
				getNumShowContents(), getHidden(), getFileCount(), 
				getAuthorzn(), getPositn(), getIsAnnouncements());
	}
	
	/**
	 * Returns collection of columns with desired attributes
	 * @ejb.interface-method view-type="local"
	 * @param removed Whether we should return only records for removed columns
	 * or only records for active columns (we'll never return both at once)
	 * @param visible Whether we should only return records for visible columns (applies if removed is false)
	 * @return A Collection of CategoryColDatas
	 */
	public Collection getColumns(boolean removed, boolean visible){
		Collection c, result = new ArrayList();
		Iterator i ;
		try{
			
			c = columnHome().findByCategoryID(getCategoryID(), removed, visible);
			i = c.iterator();
			while(i.hasNext()){
				result.add(((CategoryColLocal)i.next()).getCategoryColData());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param visibleCol Whether we should limit our search to contents whose columns are visible
	 * @param visibleRow Whether we should limit our search to contents whose rows are visible
	 * @return Collection of CategoryContentsDatas for contents associated with this category
	 */
	public Collection getContents(boolean visibleCol, boolean visibleRow){
		Collection result = new ArrayList();
		try
		{
			Collection c = contentsHome().findByCategoryID(this.getCategoryID(), this.sortByColId, this.getAscending(), visibleCol, visibleRow);
			Iterator i = c.iterator();
			while(i.hasNext())
				result.add(((CategoryContentsLocal)i.next()).getCategoryContentsData());
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Return records for all files within this category, in both hidden and visible rows and columns
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getContentFiles(){
		Collection result = new ArrayList();
		try{
			Collection c = filesHome().findByCategoryID(getCategoryID());
			Iterator i = c.iterator();
			while(i.hasNext()){
				result.add(((CategoryFileLocal)i.next()).getCategoryFileData());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	
	private CategoryFileLocalHome filesHome(){
		try{
			if(filesHome == null)
				filesHome = CategoryFileUtil.getLocalHome();
		}catch(Exception e){
			e.printStackTrace();
		}
		return filesHome;
	}
	
	private CategoryColLocalHome columnHome(){
		try{
			if(columnHome == null)
				columnHome = CategoryColUtil.getLocalHome();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return columnHome;
	}
	
	private CategoryContentsLocalHome contentsHome(){
		try{
			if(contentsHome == null)
				contentsHome = CategoryContentsUtil.getLocalHome();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return contentsHome;
	}
	
	/**
	 * 
	 * @return
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local"
	 */
	public CategoryPK ejbCreate(long courseID, String categoryName, boolean ascending, 
			long colID, long numShow, boolean hidden, long fileCount, int authorzn,
			int positn, boolean announcements)throws javax.ejb.CreateException{
		setCategoryName(categoryName);
		setCourseID(courseID);
		setAscending(ascending);
		setSortByColId(colID);
		setNumShowContents(numShow);
		setHidden(hidden);
		setFileCount(fileCount);
		setAuthorzn(authorzn);
		setPositn(positn);
		setIsAnnouncements(announcements);
		return null;
	}
}
