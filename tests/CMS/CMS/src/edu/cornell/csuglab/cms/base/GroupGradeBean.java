/*
 * Created on Mar 16, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

/**
 *
 * <!-- begin-user-doc --> You can insert your documentation for '<em><b>GroupGradeBean</b></em>'. <!-- end-user-doc --> *
 <!--  begin-lomboz-definition -->
 <?xml version="1.0" encoding="UTF-8"?>
 <lomboz:EJB xmlns:j2ee="http://java.sun.com/xml/ns/j2ee" xmlns:lomboz="http://lomboz.objectlearn.com/xml/lomboz">
 <lomboz:entity>
 <lomboz:entityEjb>
 <j2ee:display-name>GroupGrade</j2ee:display-name>
 <j2ee:ejb-name>GroupGrade</j2ee:ejb-name>
 <j2ee:ejb-class>edu.cornell.csuglab.cms.base.GroupGradeBean</j2ee:ejb-class>
 <j2ee:persistence-type>Bean</j2ee:persistence-type>
 <j2ee:cmp-version>2.x</j2ee:cmp-version>
 <j2ee:abstract-schema-name>mySchema</j2ee:abstract-schema-name>
 </lomboz:entityEjb>
 <lomboz:tableName></lomboz:tableName>
 <lomboz:dataSourceName></lomboz:dataSourceName>
 </lomboz:entity>
 </lomboz:EJB>
 <!--  end-lomboz-definition -->
 *
 * <!-- begin-xdoclet-definition -->



 /**
 * @ejb.bean name="GroupGrade"
 *	jndi-name="GroupGrade"
 *	type="BMP"
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.GroupGradeDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.GroupGradeDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 * <!-- end-xdoclet-defintion -->
 * @generated
 **/
public abstract class GroupGradeBean implements javax.ejb.EntityBean {

    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long groupID;
	private long subProblemID;
	private float score;
	private boolean isAveraged;
	
	/**
	 * @return Returns the groupID.
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getGroupID() {
		return groupID;
	}
	
	/**
	 * @param groupID The groupID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}
	
	/**
	 * @return Returns the average score of this group.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public float getScore() {
		return score;
	}
	
	/**
	 * @param score The score to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setScore(float score) {
		this.score = score;
	}
	
	/**
	 * Returns true if this group grade was calculated by taking an
	 * averaged of group member's grades (i.e. there were members who had 
	 * different grades)
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getAveraged() {
		return isAveraged;
	}
	
	/**
	 * @param maxScore
	 * @ejb.interface-method view-type="local"
	 */
	public void setIsAveraged(boolean isAveraged) {
		this.isAveraged = isAveraged;
	}
	
	/**
	 * @return Returns the subProblemID.
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getSubProblemID() {
		return subProblemID;
	}
	
	/**
	 * @param subProblemID The subProblemID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setSubProblemID(long subProblemID) {
		this.subProblemID = subProblemID;
	}
	
	/**
	 * 
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public GroupGradeData getGroupGradeData() {
		return new GroupGradeData(getGroupID(), 
				getScore(), getAveraged(), getSubProblemID());
	}
	
	/**
	 * @param assignmentID
	 * @param groupID
	 * @param score
	 * @param subProblemID
	 * @return
	 * @throws CreateException
	 * @ejb.create-method view-type="local"
	 */
	public GroupGradePK ejbCreate(long groupID,
			float score, boolean isAveraged, long subProblemID) throws CreateException {
		setGroupID(groupID);
		setScore(score);
		setIsAveraged(isAveraged);
		setSubProblemID(subProblemID);
		return null;
	}
	
	/**
	 * 
	 * @param pk
	 * @return
	 * @throws FinderException
	 */
	public GroupGradePK ejbFindByPrimaryKey(GroupGradePK pk) throws FinderException {
		return null;
	}
	
	/**
	 * 
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}

	/**
	 * Finds all GroupGrades for this assignment which are allowed to be viewed
	 * or changed by the given grader.  
	 */
	public Collection ejbFindByGraderAssignmentID(String netID, long assignmentID, boolean adminPriv, int numSubProbs) throws FinderException {
		return null;
	}
	
  /**
   * <!-- begin-user-doc -->
   * The container invokes this method immediately after it calls ejbCreate.
   * <!-- end-user-doc -->
   * 
   * @generated
   */
  public void ejbPostCreate() throws javax.ejb.CreateException {
    // begin-user-code
    // end-user-code
  }

}
