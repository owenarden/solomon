/*
 * Created on Sep 6, 2004
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.SemesterBean;
import edu.cornell.csuglab.cms.base.SemesterDAO;
import edu.cornell.csuglab.cms.base.SemesterPK;
import edu.cornell.csuglab.cms.base.StaffBean;
import edu.cornell.csuglab.cms.base.StudentBean;

/**
 * @author Jon
 *
 */
public class SemesterDAOImpl extends DAOMaster implements SemesterDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SemesterDAO#load(edu.cornell.csuglab.cms.base.SemesterPK, edu.cornell.csuglab.cms.base.SemesterBean)
	 */
	public void load(SemesterPK pk, SemesterBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tSemesters where SemesterID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getSemesterID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setSemesterID(rs.getLong("SemesterID"));
				ejb.setSemesterName(rs.getString("SemesterName"));
				ejb.setTermCode(rs.getString("TermCode"));
				ejb.setHidden(rs.getBoolean("Hidden"));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SemesterDAO#store(edu.cornell.csuglab.cms.base.SemesterBean)
	 */
	public void store(SemesterBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "update tSemesters set SemesterName = ?, Hidden = ? where SemesterID = ?";
			ps = conn.prepareStatement(update);
			if(ejb.getSemesterName() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getSemesterName());
			ps.setBoolean(2, ejb.getHidden());
			ps.setLong(3, ejb.getSemesterID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			}
			catch (Exception f) {}
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SemesterDAO#remove(edu.cornell.csuglab.cms.base.SemesterPK)
	 */
	public void remove(SemesterPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SemesterDAO#create(edu.cornell.csuglab.cms.base.SemesterBean)
	 */
	public SemesterPK create(SemesterBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null, ret = null;
		ResultSet rs = null;
		SemesterPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "insert into tSemesters (SemesterName, Hidden) values (?, ?)";
			String findString = "select @@identity as 'SemesterID' from tSemesters";
			ps = conn.prepareStatement(createString);
			if(ejb.getSemesterName() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getSemesterName());
			ps.setBoolean(2, false);
			ps.executeUpdate();
			ret = conn.prepareStatement(findString);
			rs = ret.executeQuery();
			if (rs.next()) {
				long semesterID = rs.getLong("SemesterID");
				result = new SemesterPK(semesterID);
				ejb.setSemesterID(result.getSemesterID());
			}
			conn.close();
			ps.close();
			ret.close();
			rs.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if(ret != null) ret.close();
				if (rs != null) rs.close();
			}
			catch (Exception ex) {}
			throw new EJBException(e);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SemesterDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.SemesterPK)
	 */
	public SemesterPK findByPrimaryKey(SemesterPK key) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select SemesterID from tSemesters where SemesterID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, key.getSemesterID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Failed to find Semester with SemesterID = " + key.getSemesterID());
			}
			conn.close();
			ps.close();
			rs.close();
		}
		//XXX what specific error is this first catch meant for?
		catch (FinderException e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			throw e;
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();				
			}
			catch (Exception f) {}
			throw new FinderException("Exception caught: " + e.getMessage());
		}
		return key;
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SemesterDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.SemesterPK)
	 */
	public Collection findAllSemesters() throws FinderException
	{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select SemesterID from tSemesters";
			ps = conn.prepareStatement(queryString);
			rs = ps.executeQuery();
			while(rs.next())
			{
				result.add(new SemesterPK(rs.getLong(1)));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();				
			}
			catch (Exception f) {}
			throw new FinderException("Exception caught: " + e.getMessage());
		}
		return result;
	}
	
	public Collection findByUser(String netID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT DISTINCT s.SemesterID " +
					"FROM tSemesters s INNER JOIN " + 
                    "tCourse c ON s.SemesterID = c.SemesterID LEFT OUTER JOIN " +
                    "tStudent su ON c.CourseID = su.CourseID AND su.NetID = ? LEFT OUTER JOIN " +
                    "tStaff sf ON c.CourseID = sf.CourseID AND sf.NetID = ? " +
                    "WHERE ((su.Status = ?) OR (sf.Status = ?)) AND s.Hidden = ? " + 
                    "ORDER BY s.SemesterID ASC";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netID);
			ps.setString(2, netID);
			ps.setString(3, StudentBean.ENROLLED);
			ps.setString(4, StaffBean.ACTIVE);
			ps.setBoolean(5, false);
			rs = ps.executeQuery();
			while(rs.next())
			{
				result.add(new SemesterPK(rs.getLong(1)));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();				
			}
			catch (Exception f) {}
			throw new FinderException("Exception caught: " + e.getMessage());
		}
		return result;
	}
	
	public SemesterPK findCurrent() throws FinderException {
		//FIXME duplicates RootDAO.findCurrentSemester();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		SemesterPK result;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select CurrentSemester from tSystemProperties where ID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setInt(1, 1);
			rs = ps.executeQuery();
			if (rs.next()){
				result = new SemesterPK(rs.getLong("CurrentSemester"));
			} else throw new FinderException("Could not find the current semester");
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();				
			}
			catch (Exception f) {}
			throw new FinderException("Exception caught: " + e.getMessage());
		}
		return result;
	}
}
