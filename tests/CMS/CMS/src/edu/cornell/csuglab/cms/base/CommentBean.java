/*
 * Created on Mar 31, 2004
 */

package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.Collection;

import javax.ejb.EntityBean;
import javax.ejb.FinderException;

/**
 * @ejb.bean name="Comment"
 *	jndi-name="CommentBean"
 *	type="BMP" 
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.CommentDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.CommentDAOImpl"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 **/
public abstract class CommentBean implements EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
    private long commentID;
	private long groupID;
	private String comment;
	private String netID;
	private Timestamp dateEntered;
	private boolean hidden;
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The comment's unique ID
	 */
	public long getCommentID() {
		return commentID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param ID The comment's unique ID to set. 
	 */
	public void setCommentID(long ID) {
		this.commentID = ID;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getGroupID() {
		return groupID;
	}

	/**
	 * @param groupID
	 * @ejb.interface-method view-type="local"
	 */
	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getComment() {
		return comment;
	}
	
	/**
	 * @param comment
	 * @ejb.interface-method view-type="local"
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getNetID() {
		return netID;
	}
	
	/**
	 * @param netID
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 */
	public void setNetID(String netID) {
		this.netID = netID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public Timestamp getDateEntered() {
		return dateEntered;
	}
	
	/**
	 * @param dateEntered
	 * @ejb.interface-method view-type="local"
	 */
	public void setDateEntered(Timestamp dateEntered) {
		this.dateEntered = dateEntered;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getHidden() {
	    return hidden;
	}
	
	/**
	 * @param hidden
	 * @ejb.interface-method view-type="local"
	 */
	public void setHidden(boolean hidden) {
	    this.hidden = hidden;
	}
 	
	/**
	 * Checks to see if the given primary key can be found in the database
	 * 
	 * @param pk
	 *            The primary key being searched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public CommentPK ejbFindByPrimaryKey(CommentPK key) throws FinderException {
		return null;
	}

	/**
	 * Finds all the non-hidden comments for a given group in ascending order
	 * @param groupID
	 *            The student's groupID for this assignment
	 * @param assignmentid
	 *            The assignmentID
	 * @return The collection of all the comments in increasing order of date.
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupID(long groupID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all non-hidden comments
	 * @param groupIDs
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupIDs(Collection groupIDs) throws FinderException {
		return null;
	}
	
	// TODO: will need methods to find all comments based on assignment and
	// 		 Collection of groups for admin side
	/**
	 * @ejb.interface-method view-type="local"
	 **/
	public CommentData getCommentData() {
		return new CommentData(getCommentID(), getGroupID(),
				getComment(), getNetID(), getDateEntered(), getHidden());
	}

	/**
	 * @param comment
	 * @param netID
	 * @param assignmentID
	 * @param groupID
	 * @param gradeID
	 * @param requestID
	 * @return
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local"
	 */
	public CommentPK ejbCreate(String comment, String netID, long groupID) throws javax.ejb.CreateException {
		setComment(comment);
		setNetID(netID);
		setGroupID(groupID);
		setDateEntered(new Timestamp(System.currentTimeMillis()));
		setHidden(false);
		return null;
	}
}
