/*
 * Created on Apr 28, 2005
 *
 */
package edu.cornell.csuglab.cms.util;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import edu.cornell.csuglab.cms.base.CommentFileData;
import edu.cornell.csuglab.cms.base.SubmittedFileData;

/**
 * @author Jon Guarino
 * 
 */
public class GradeCommentInfo {

	// Contains length four object array of [(String) NetID, (Long)
	// SubProblemID, (Float) Score), (Long) GroupID]
	private Vector scores = new Vector();

	/*
	 * Contains [(String) NetID, (Long) SubProblemID, (Float) Score), (Long)
	 * GroupID] Corresponds to incoming scores, but represents the scores which
	 * were the most recent scores at the time the user loaded the page
	 * originally. For conflict resolutions.
	 */
	private Vector oldscores = new Vector();

	// Contains newly submitted files (for groups)
	private Vector submittedFiles = new Vector();

	/*
	 * Contains length 2 object array of [(String) CommentText,
	 * (CommentFileData) CommentFile, (Vector of Longs) RequestIDs] The keys are
	 * the GroupID that the comment should be assigned to (Long)
	 */
	private Hashtable comments = new Hashtable();
	
	private TreeSet groups = new TreeSet();

	// Contains the CommentIDs (Long) of comments which should be set to hidden
	private TreeSet removedcomments = new TreeSet();

	/*
	 * Contains a length 3 object array of [(String) RegradeText, (Vector of
	 * Longs) SubProblemIDs, (String) RequesterNetID] The keys are the GroupID
	 * that this regrade request is for (Long)
	 */
	private Hashtable newregrades = new Hashtable();

	/* Adjustments to scores (Long -> String)8
   	   String ending in % means a percent
	   adjustment; otherwise it is an absolute change. */
	private Hashtable adjustments = new Hashtable();

	public void addScore(String netID, long subProblemID, Float score,
			long groupID) {
		Object[] entry = { netID, new Long(subProblemID), score,
				new Long(groupID) };
		scores.add(entry);
	}

	public void addAdjustment(Long groupID, String adjustment) {
		adjustments.put(groupID, adjustment);
	}
	
	public void setGroupVisited(Long groupID) {
		groups.add(groupID);
	}

	public boolean isGroupVisited(Long groupID) {
		return groups.contains(groupID);
	}
	
	public String getAdjustment(Long groupID) {
		return (String) adjustments.get(groupID);
	}

	public void addOldScore(String netID, long subProblemID, float score,
			long groupID) {
		Object[] entry = { netID, new Long(subProblemID), new Float(score),
				new Long(groupID) };
		oldscores.add(entry);
	}

	public void addCommentText(long groupID, String text) {
		Long g = new Long(groupID);
		Object[] entry = (Object[]) comments.get(g);
		if (entry == null) {
			entry = new Object[3];
			entry[0] = text;
		} else {
			entry[0] = text;
		}
		comments.put(g, entry);
	}

	public void addCommentFile(long groupID, CommentFileData file) {
		Long g = new Long(groupID);
		Object[] entry = (Object[]) comments.get(g);
		if (entry == null) {
			entry = new Object[3];
			entry[1] = file;
		} else {
			entry[1] = file;
		}
		comments.put(g, entry);
	}

	public void addSubmittedFile(long assignID, String fileName,
			SubmittedFileData file) {
		submittedFiles.add(new Object[] { new Long(assignID), fileName, file });
	}

	/**
	 * @return A collection of Object arrays consisting of a the AssignmentID
	 *         (Long) which pertains to this file in the first position, the
	 *         File Name (String) in the second position, and a
	 *         SubmittedFileData with the file information in the third
	 *         position. [<AssignmentID>, <FileName>, <SubmittedFileData>]
	 */
	public Vector getSubmittedFiles() {
		return submittedFiles;
	}

	public void addRegradeResponse(long groupID, long requestID) {
		Long g = new Long(groupID);
		Object[] entry = (Object[]) comments.get(g);
		if (entry == null) {
			entry = new Object[3];
			Vector v = new Vector();
			v.add(new Long(requestID));
			entry[2] = v;
		} else {
			if (entry[2] == null) {
				entry[2] = new Vector();
			}
			((Vector) entry[2]).add(new Long(requestID));
		}
		comments.put(g, entry);
	}

	public void addNewRegrade(long groupID, String request) {
		Long key = new Long(groupID);
		Object[] entry = (Object[]) newregrades.get(key);
		if (entry == null) {
			entry = new Object[] { request, new Vector(), null };
		} else {
			entry[0] = request;
		}
		newregrades.put(key, entry);
	}

	public void addNewRegradeSubProb(long groupID, long subProbID) {
		Long key = new Long(groupID);
		Object[] entry = (Object[]) newregrades.get(key);
		if (entry == null) {
			entry = new Object[3];
			Vector v = new Vector();
			v.add(new Long(subProbID));
			entry[1] = v;
		} else {
			Vector v = (Vector) entry[1];
			v.add(new Long(subProbID));
		}
		newregrades.put(key, entry);
	}

	public void addNewRegradeNetID(long groupID, String netID) {
		Long key = new Long(groupID);
		Object[] entry = (Object[]) newregrades.get(key);
		if (entry == null) {
			entry = new Object[] { null, new Vector(), netID };
		} else {
			entry[2] = netID;
		}
		newregrades.put(key, entry);
	}

	public Hashtable getNewRegrades() {
		return newregrades;
	}

	/**
	 * Returns a vector of length 3 Object arrays containing: [(String) NetID,
	 * (Long) SubProblemID, (Float) Score, (Long) GroupID]
	 * 
	 * @return
	 */
	public Vector getScores() {
		return scores;
	}

	/**
	 * Removes entries from oldscores which don't have corresponding entries in
	 * scores. Ensures that the ordering of entries matches the order in the
	 * scores Vector, assuming that each entry in scores has a matching one in
	 * oldscore.
	 */
	public void organizeOldScores() {
		Vector organized = new Vector();
		String netID;
		Long subProb, groupID;
		Object[] score, oldscore;
		boolean found;
		for (int i = 0; i < scores.size(); i++) {
			score = (Object[]) scores.get(i);
			netID = (String) score[0];
			subProb = (Long) score[1];
			groupID = (Long) score[3];
			// Find the corresponding oldscore
			found = false;
			for (int j = 0; j < oldscores.size(); j++) {
				oldscore = (Object[]) oldscores.get(j);
				if (oldscore[0].equals(netID) && oldscore[1].equals(subProb)
						&& oldscore[3].equals(groupID)) {
					organized.add(oldscore);
					oldscores.remove(j); // remove it so we don't have to
											// check it again
					found = true;
					break;
				}
			}
			if (!found) {
				organized.add(new Object[] { netID, subProb, null, groupID });
			}
		}
		oldscores = organized;
	}

	public Vector getOldScores() {
		return oldscores;
	}

	/**
	 * Returns a Vector of length 4 Object arrays containing: [(Long) GroupID,
	 * (String) CommentText, (CommentFileData) CommentFile, (Vector of Longs)
	 * RegradeRequests]
	 * 
	 * @return
	 */
	public Vector getComments() {
		Vector result = new Vector();
		Enumeration e = comments.keys();
		while (e.hasMoreElements()) {
			Long groupID = (Long) e.nextElement();
			Object[] vals = (Object[]) comments.get(groupID);
			Object[] entry = { groupID, vals[0], vals[1], vals[2] };
			result.add(entry);
		}
		return result;
	}

	public void addRemovedComment(long commentID) {
		removedcomments.add(new Long(commentID));
	}

	public Set getRemovedComments() {
		return removedcomments;
	}

	public static String formatComments(String commentString) {
		return commentString.replaceAll("\n", "<br />");
	}
}
