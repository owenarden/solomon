package edu.cornell.csuglab.cms.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.ejb.CreateException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;

import edu.cornell.csuglab.cms.util.category.*;
import edu.cornell.csuglab.cms.www.util.Profiler;

/**
 * @ejb.bean name="Root"
 *	jndi-name="RootBean"
 *	type="Stateless" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.RootDAO"
 * 	impl-class="edu.cornell.csuglab.cms.base.dao.RootDAOImpl"
 *
 * @ejb.ejb-ref ejb-name="User" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="User" jndi-name="User"
 * 
 * @ejb.ejb-ref ejb-name="CMSAdmin" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="CMSAdmin" jndi-name="CMSAdmin"
 * 
 * @ejb.ejb-ref ejb-name="Student" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Student" jndi-name="Student"
 * 
 * @ejb.ejb-ref ejb-name="Staff" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Staff" jndi-name="Staff"
 * 
 * @ejb.ejb-ref ejb-name="Course" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Course" jndi-name="Course"
 * 
 * @ejb.ejb-ref ejb-name="Announcement" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Announcement" jndi-name="Announcement"
 * 
 * @ejb.ejb-ref ejb-name="Assignment" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Assignment" jndi-name="Assignment"
 * 
 * @ejb.ejb-ref ejb-name="AssignmentFile" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="AssignmentFile" jndi-name="AssignmentFile"
 * 
 * @ejb.ejb-ref ejb-name="Group" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Group" jndi-name="Group"
 * 
 * @ejb.ejb-ref ejb-name="GroupMember" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="GroupMember" jndi-name="GroupMember"
 *  
 * @ejb.ejb-ref ejb-name="Grade" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Grade" jndi-name="Grade" 
 * 
 * @ejb.ejb-ref ejb-name="Comment" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Comment" jndi-name="Comment"
 *  
 * @ejb.ejb-ref ejb-name = "Category" view-type = "remote"
 * @jboss.ejb-ref-jndi ref-name ="Category" jndi-name = "Category"
 *
 *  @ejb.ejb-ref ejb-name = "CategoryCol" view-type = "remote"
 * @jboss.ejb-ref-jndi ref-name ="CategoryCol" jndi-name = "CategoryCol"
 *
 * @ejb.ejb-ref ejb-name = "CategoryRow" view-type = "remote"
 * @jboss.ejb-ref-jndi ref-name ="CategoryRow" jndi-name = "CategoryRow"
 * 
 * @ejb.ejb-ref ejb-name = "CategoryContents" view-type = "remote"
 * @jboss.ejb-ref-jndi ref-name ="CategoryContents" jndi-name = "CategoryContents"
 * 
 * @ejb.ejb-ref ejb-name = "CategoryFile" view-type = "remote"
 * @jboss.ejb-ref-jndi ref-name ="CategoryContents" jndi-name = "CategoryFile"
 * 
 * @ejb.ejb-ref ejb-name="SolutionFile" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="SolutionFile" jndi-name="SolutionFile"
 * 
 * @ejb.ejb-ref ejb-name="OldAnnouncement" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="OldAnnouncement" jndi-name="OldAnnouncement"
 * 
 * @ejb.ejb-ref ejb-name="Answer" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Answer" jndi-name="Answer"
 * 
 * @ejb.ejb-ref ejb-name="AnswerSet" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="AnswerSet" jndi-name="AnswerSet"
 * 
 * @ejb.ejb-ref ejb-name="Choice" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Choice" jndi-name="Choice"
 * 
 * @ejb.ejb-ref ejb-name="Log" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Log" jndi-name="Log"
 **/
public abstract class RootBean implements SessionBean {
	
	private AnnouncementLocalHome announcementLocalHome;
	private AnswerLocalHome answerLocalHome;
	private AnswerSetLocalHome answerSetLocalHome;
	private AssignmentLocalHome assignmentLocalHome;
	private AssignmentFileLocalHome assignmentFileLocalHome;
	private AssignmentItemLocalHome assignmentItemLocalHome;
	private CategoryLocalHome categoryLocalHome;
	private CategoryColLocalHome categoryColLocalHome;
	private CategoryContentsLocalHome categoryContentsLocalHome;
	private CategoryFileLocalHome categoryFileLocalHome;
	private CategoryRowLocalHome categoryRowLocalHome;
	private ChoiceLocalHome choiceLocalHome;
	private CMSAdminLocalHome cmsAdminLocalHome;
	private CommentLocalHome commentLocalHome;
	private CommentFileLocalHome commentFileLocalHome;
	private CourseLocalHome courseLocalHome;
	private DomainLocalHome domainLocalHome;
	private EmailLocalHome emailLocalHome;
	private GradeLocalHome gradeLocalHome;
	private GroupAssignedToLocalHome groupAssignedToLocalHome;
	private GroupLocalHome groupLocalHome;
	private GroupGradeLocalHome groupGradeLocalHome;
	private GroupMemberLocalHome groupMemberLocalHome;
	private LogLocalHome logLocalHome;
	private OldAnnouncementLocalHome oldAnnouncementLocalHome;
	private RegradeRequestLocalHome regradeRequestLocalHome;
	private RequiredFileTypeLocalHome requiredFileTypeLocalHome;
	private RequiredSubmissionLocalHome requiredSubmissionLocalHome;
	private SemesterLocalHome semesterLocalHome;
	private SiteNoticeLocalHome siteNoticeLocalHome;
	private SolutionFileLocalHome solutionFileLocalHome;
	private StaffLocalHome staffLocalHome;
	private StudentLocalHome studentLocalHome;
	private SubProblemLocalHome subProblemLocalHome;
	private SubmittedFileLocalHome submittedFileLocalHome;
	private TimeSlotLocalHome timeSlotLocalHome;
	private UserLocalHome userLocalHome;
	
	public void ejbCreate() throws CreateException {
		try {
			//create entity-bean homes
			announcementLocalHome = AnnouncementUtil.getLocalHome();
			answerLocalHome = AnswerUtil.getLocalHome();
			answerSetLocalHome = AnswerSetUtil.getLocalHome();
			assignmentLocalHome = AssignmentUtil.getLocalHome();
			assignmentFileLocalHome = AssignmentFileUtil.getLocalHome();
			assignmentItemLocalHome = AssignmentItemUtil.getLocalHome();
			categoryLocalHome = CategoryUtil.getLocalHome();
			cmsAdminLocalHome = CMSAdminUtil.getLocalHome();
			commentLocalHome = CommentUtil.getLocalHome();
			commentFileLocalHome = CommentFileUtil.getLocalHome();
			courseLocalHome = CourseUtil.getLocalHome();
			categoryColLocalHome = CategoryColUtil.getLocalHome();
			categoryContentsLocalHome = CategoryContentsUtil.getLocalHome();
			categoryFileLocalHome = CategoryFileUtil.getLocalHome();
			categoryRowLocalHome = CategoryRowUtil.getLocalHome();
			choiceLocalHome = ChoiceUtil.getLocalHome();
			domainLocalHome = DomainUtil.getLocalHome();
			emailLocalHome = EmailUtil.getLocalHome();
			gradeLocalHome = GradeUtil.getLocalHome();
			groupGradeLocalHome = GroupGradeUtil.getLocalHome();
			groupLocalHome = GroupUtil.getLocalHome();
			groupAssignedToLocalHome = GroupAssignedToUtil.getLocalHome();
			groupMemberLocalHome = GroupMemberUtil.getLocalHome();
			logLocalHome = LogUtil.getLocalHome();
			oldAnnouncementLocalHome = OldAnnouncementUtil.getLocalHome();
			regradeRequestLocalHome = RegradeRequestUtil.getLocalHome();
			requiredFileTypeLocalHome = RequiredFileTypeUtil.getLocalHome();
			requiredSubmissionLocalHome = RequiredSubmissionUtil.getLocalHome();
			semesterLocalHome = SemesterUtil.getLocalHome();
			siteNoticeLocalHome = SiteNoticeUtil.getLocalHome();
			solutionFileLocalHome = SolutionFileUtil.getLocalHome();
			staffLocalHome = StaffUtil.getLocalHome();
			studentLocalHome = StudentUtil.getLocalHome();
			submittedFileLocalHome = SubmittedFileUtil.getLocalHome();
			subProblemLocalHome = SubProblemUtil.getLocalHome();	
			timeSlotLocalHome = TimeSlotUtil.getLocalHome();
			userLocalHome = UserUtil.getLocalHome();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * When the system first starts up, this method gets called to check
	 * that SystemProperties settings exist, and also that at least
	 * one semester exists.  If these things are missing, this method will
	 * create default entries.
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="ensureStartupSettings"
	 */
	public void ensureStartupSettings() throws FinderException {}
	
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getFileTypes"
	 * @return
	 */
	// XXX removed April 2, 2010 by aip23 - obsolete
	/*
	public Collection getFileTypes() {
		return null;
	}
	*/
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="isDebugMode"
	 * @return
	 */
	public boolean isDebugMode() {
		return false;
	}
	
	/**
	 * @return Returns a set of the GroupIDs (Longs) 
	 * of groups which have late submissions in the
	 * given assignment
	 * @ejb.interface-method view-type="local"
	 */
	public Set getLateGroups(long assignmentID) throws FinderException {
		Iterator gs = groupHome().findLateByAssignmentID(assignmentID).iterator();
		HashSet result = new HashSet();
		while (gs.hasNext()) {
			GroupLocal g = (GroupLocal) gs.next();
			result.add(new Long(g.getGroupID()));
		}
		return result;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getMaxFileSize"
	 * @return
	 */
	public int getMaxFileSize() {
		return 0;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getCUIDCount"
	 * @return the number of distinct CUIDs found in the user table
	 */
	public int getCUIDCount() {
	    return 0;
	}
	
	/**
	 * Return the current semester
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="findCurrentSemester"
	 */
	public SemesterPK findCurrentSemester() throws FinderException
	{
		return null;
	}
	
	/**
	 * Set the current semester
	 * @param semesterName The ID of the (existing) semester to switch to
	 * @return Whether the operation was completed successfully
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="setCurrentSemester"
	 */
	public boolean setCurrentSemester(long semesterID)
	{
		return false;
	}
	
	/**
	 * Return URL from which to load each semester's server groups
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getHostGroupsURL"
	 */
	public String getHostGroupsURL() throws FinderException
	{
		return null;
	}
	
	/**
	 * Set the URL from which to load the list of groups of servers for each semester
	 * @param url the URL to load from
	 * @return Whether the operation was completed successfully
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="setHostGroupsURL"
	 */
	public boolean setHostGroupsURL(String url)
	{
		return false;
	}
	
	/**
	 * Return JSON string of host, semesterID pairs
	 * @param semesterID the semester to fetch
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getHostGroup"
	 */
	public String getHostGroup(Long semesterID) throws FinderException
	{
		return null;
	}
	
	/**
	 * Return XML containing discovered foreign semesters
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getForeignSemesters"
	 */
	public org.w3c.dom.Document getForeignSemesters() throws FinderException
	{
		return null;
	}
	
	/**
	 * Find a subset of the netids which are not students in
	 * the given course
	 * @param netids A collection of NetIDs to check
	 * @param courseID The CourseID of the course to check
	 * @return A collection of NetIDs from the input collection
	 *  which do not correspond to students in the course
	 * @dao.call name="getNonStudentNetIDs"
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getNonStudentNetIDs(Collection netids, long courseID) {
		return null;
	}
	
	/**
	 * Find a subset of the netids which are active in groups
	 * in this assignment in which they are not the only member
	 * @param netids A collection of netids to check
	 * @param assignmentID The AssignmentID of the assignment to check
	 * @return A collection of NetIDs from the input collection
	 *  of students who are in a group with other students
	 * @dao.call name="getNonSoloGroupMembers"
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getNonSoloGroupMembers(Collection netids, long assignmentID) {
		return null;
	}
	
	/**
	 * Find that subset of the given netids that have received a
	 * grade for this assignment already
	 * @param netids A collection of netids to check
 	 * @param assignmentID The AssignmentID of the assignment to check
	 * @return A collection of NetIDs from the input collection
	 *  of students who have a grade for this assignment
	 * @dao.call name="getGradedStudents"
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getGradedStudents(Collection netids, long assignmentID) {
		return null;
	}
	
	/**
	 * Find that subset of the given groups that have received a
	 * grade for this assignment already
	 * @param groupIDs A collection of group IDs (Longs) to check
 	 * @param asgnID The AssignmentID of the assignment to check
	 * @return A collection of Longs, a subset of the input
	 * @dao.call name="getGradedGroups"
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getGradedGroups(Collection groupIDs, long asgnID) {
		return null;
	}
	
	/**
	 * Find a collection of Assignments in which the given NetIDs have received a grade 
	 * @dao.call name="getGradedAssignments"
	 * @ejb.interface-method view-type="local"
	 * @return A collection of AssignmentIDs (Longs)
	 */
	public Collection getGradedAssignments(Collection netids, long courseID) {
		return null;
	}
	
	/**
	 * @param courseID
	 * @return A mapping from GroupID (Long) -> AssignmentID (Long) which
	 *  is valid within the given course
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getAssignmentIDMap"
	 */
	public Map getAssignmentIDMap(long courseID) {
	    return null;
	}
	
	/**
	 * @param assignmentID The AssignmentID of the assignment for which
	 *  SubmissionNames are needed
	 * @return A mapping from SubmissionID (Long) -> SubmissionName (String)
	 *  Only contains mappings for given assignment.  Within the assignment,
	 *  there is mapping for every RequiredSubmission (including hidden ones)
	 * @dao.call name="getSubmissionNameMap"
	 * @ejb.interface-method view-type="local"
	 */
	public Map getSubmissionNameMap(long assignmentID) {
		return null;
	}
	
	/**
	 * @param courseID
	 * @return A mapping from SubmissionID (Long) -> SubmissionName (String)
	 *  which is valid for all assignments in the given course
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getSubmissionNameMapByCourse"
	 */
	public Map getSubmissionNameMapByCourse(long courseID) {
	    return null;
	}
	
	/**
	 * @param assignmentID The AssignmentID of the assignment
	 * @return A mapping from SubProblemName (String) -> SubProblemID (Long)
	 *  valid within the given assignment.
	 * @dao.call name="getSubProblemIDMap"
	 * @ejb.interface-method view-type="local"
	 */
	public Map getSubProblemIDMap(long assignmentID) {
		return null;
	}
	
	/**
	 * @return Returns a mapping from CourseID (Long) -> CourseCode (String)
	 *  which is valid for all courses in the system
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getCIDCourseCodeMap"
	 */
	public Map getCIDCourseCodeMap() {
	    return null;
	}
	
	/**
	 * @return Returns a mapping from AssignmentID (Long) -> AssignmentName (String)
	 *  which is valid for all assignments in the system
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getAssignmentNameMap"
	 */
	public Map getAssignmentNameMap() {
	    return null;
	}
	
	/**
	 * @param courseID
	 * @return Returns a mapping from AssignmentID (Long) -> AssignmentName (String)
	 *  which is valid within the given course
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getAssignmentNameMap"
	 */
	public Map getAssignmentNameMap(long courseID) {
	    return null;
	}
	
	/**
	 * @param courseID
	 * @return Returns a mapping from ContentID (Long) -> CategoryID (Long)
	 * 	which is valid within the given course
	 * @dao.call name="getCategoryIDMap"
	 * @ejb.interface-method view-type="local"
	 */
	public Map getCategoryIDMap(long courseID) {
	    return null;
	}
	
	/**
	 * @return Returns a mapping from AssigmentID (Long) -> CourseCode (String)
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getCourseCodeMap"
	 */
	public Map getCourseCodeMap() {
	    return null;
	}
	
	/**
	 * @return A mapping from GroupID (Long) -> RemainingSubmissions (Integer)
	 *  This mapping is not retreived from the tGroups table, but rather is
	 *  calculated in a query based on the number of distinct non-hidden
	 *  RequiredSubmissions a group's submitted files matched.
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getRemainingSubmissionMap"
	 */
	public Map getRemainingSubmissionMap(long assignmentID) {
	    return null;
	}
	
	/**
	 * Returns a mapping from GroupID (Long) -> Group (GroupBean) for all
	 * active groups in the assignment.
	 * @param assignmentID
	 * @ejb.interface-method view-type="local"
	 */
	public Map getGroupsMap(long assignmentID) {
		Map result = new HashMap();
		try {
			Iterator groups = groupLocalHome.findByAssignmentID(assignmentID).iterator();
			while (groups.hasNext()) {
				GroupLocal group = (GroupLocal) groups.next();
				result.put(new Long(group.getGroupID()), group);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Returns the GroupIDs of the groups for which at least
	 * one problem has been assigned to the grader
	 * @param graderNetID The NetID of the grader
	 * @param groupIDs The GroupIDs of the groups to check
	 * @return A subset of groupIDs which have been assigned
	 * @ejb.interface-method view-type="local"
	 */
	public Collection assignedToGroups(long assignID, String graderNetID, Collection groupIDs) throws FinderException {
		Iterator i = groupAssignedToHome().findByNetIDAssignmentID(graderNetID, assignID).iterator();
		HashSet assignedGroups = new HashSet();
		Vector result = new Vector();
		while (i.hasNext()) {
			GroupAssignedToLocal a = (GroupAssignedToLocal) i.next();
			assignedGroups.add(new Long(a.getGroupID()));
		}
		i = groupIDs.iterator();
		while (i.hasNext()) {
			Long groupID = (Long) i.next();
			if (assignedGroups.contains(groupID))
				result.add(groupID);
		}
		return result;
	}
	
	/**
	 * @param courseID
	 * @return A mapping from NetID (String) -> FullName (String) which is
	 *  valid for students within the given course
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getNameMap"
	 */
	public Map getNameMap(long courseID) {
	    return null;
	}
	
	/**
	 * Returns a mapping from NetID (String) -> First/Last Name (String[2])
	 *  which is valid for all students within the given course
	 * @param courseID
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getFirstLastNameMap"
	 */
	public Map getFirstLastNameMap(long courseID) {
		return null;
	}
	
	/**
	 * @param groupID
	 * @return A mapping from CommentID (Long) -> CommentFile (CommentFileData)
	 *  which is valid for comments under a given group
	 * @ejb.interface-method view-type="local"
	 */
	public Map getCommentFileMap(long groupID) {
	    Map result = new HashMap();
	    try {
	    	List groupids = new ArrayList();
	    	groupids.add(new Long(groupID));
	        Iterator cFiles = commentFileLocalHome.findByGroupIDs(groupids).iterator();
	        while (cFiles.hasNext()) {
	            CommentFileLocal cFile = (CommentFileLocal) cFiles.next();
	            result.put(new Long(cFile.getCommentID()), cFile.getCommentFileData());
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	        return null;
	    }
	    return result;
	}
	
	/**
	 * Checks the current semester to see if the given user is active in
	 * exactly one course (either as a staff or a student).
	 * Returns the CourseID if this is the case, null otherwise.
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="hasSoloCourse"
	 */
	public Long hasSoloCourse(String netID) {
	    return null;
	}
	
	/**
	 * Checks the given semester to see if the given user is active in
	 * exactly one course (either as a staff or a student).
	 * Returns the CourseID if this is the case, null otherwise.
	 * @param semesterID
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="hasSoloCourseBySemester"
	 */
	public Long hasSoloCourseBySemester(String netID, long semesterID) {
	    return null;
	}
	
	/**
	 * Finds all Grading-related log details for active members of the given groups
	 * in the given course
	 * @param courseID
	 * @param groupids
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="findGradeLogDetails"
	 */
	public Collection findGradeLogDetails(long courseID, Collection groupids) {
	    return null;
	}
	
	/**
	 * @param assignmentID The AssignmentID of the Assignment
	 * @return A mapping from Strings of the form <NetID>_<SubProblemID> to
	 *  Float objects containing the most recent grade for NetID on the given
	 *  SubProblem. (Valid only within given assignment).
	 *  Returns null if there is no applicable grade in the database
	 * @ejb.interface-method view-type="local"
	 */
	public Map getLastGradeMap(long assignmentID) {
		Profiler.enterMethod("RootBean.getLastGradeMap", "AssignmentID: " + assignmentID);
		Map result = new HashMap();
		try {
			Iterator grades = gradeLocalHome.findMostRecentByAssignmentID(assignmentID).iterator();
			while (grades.hasNext()) {
				GradeLocal grade = (GradeLocal) grades.next();
				String netID = grade.getNetID();
				String subProbID = String.valueOf(grade.getSubProblemID());
				result.put(netID + "_" + subProbID, grade.getGrade());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Profiler.exitMethod("RootBean.getLastGradeMap", "AssignmentID: " + assignmentID);
		return result;
	}
	
	/**
	 * @param courseID
	 * @return A mapping from Strings of the form <NetID>_<AssignmentID>_<SubProblemID> to
	 *  Float objects containing the most recent grade for NetID on the given
	 *  SubProblem, or null if there is no applicable grade in the database
	 * @ejb.interface-method view-type="local"
	 */
	public Map getLastGradeMapByCourse(long courseID) {
	    Map result = new HashMap();
	    try {
	        Iterator grades = gradeLocalHome.findMostRecentByCourseID(courseID).iterator();
	        while (grades.hasNext()) {
	            GradeLocal grade = (GradeLocal) grades.next();
	            String netID = grade.getNetID();
	            String assignID = String.valueOf(grade.getAssignmentID());
	            String subProbID = String.valueOf(grade.getSubProblemID());
	            result.put(netID + "_" + assignID + "_" + subProbID, grade.getGrade());
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}
	
	/**
	 * @param netID
	 * @param courseID
	 * @return A mapping from AssignmentID (Long) -> Grade (Float) which
	 *  is valid for the given student in the given course.
	 * @ejb.interface-method view-type="local"
	 */
	public Map getGradeMap(String netID, long courseID) {
	    Map result = new HashMap();
	    try {
	        Iterator grades = gradeLocalHome.findMostRecentByNetIDCourseID(netID, courseID).iterator();
	        while (grades.hasNext()) {
	            GradeLocal grade = (GradeLocal) grades.next();
	            if (grade.getSubProblemID() == 0) {
	                result.put(new Long(grade.getAssignmentID()), grade.getGrade());
	            }
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	        return null;
	    }
	    return result;
	}
	
	/**
	 * @param netID
	 * @param assignmentID
	 * @return A mapping from SubProblemID (Long) -> Grade (Float) which
	 *  is valid for the given student within the given assignment
	 * @ejb.interface-method view-type="local"
	 */
	public Map getSubProblemGradeMap(String netID, long assignmentID) {
	    Map result = new HashMap();
	    try {
	        Iterator grades = gradeLocalHome.findMostRecentByNetAssignmentID(netID, assignmentID).iterator();
	        while (grades.hasNext()) {
	            GradeLocal grade = (GradeLocal) grades.next();
	            result.put(new Long(grade.getSubProblemID()), grade.getGrade());
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	        return null;
	    }
	    return result;
	}

	/**
	 * @param assignmentID The AssignmentID of the assignment
	 * @return A mapping from SubProblemID (Long) -> SubProblemName (String)
	 *  valid within the given assignment.  Includes a mapping of ID 0 to
	 *  the assignment name.
	 * @dao.call name="getSubProblemNameMap"
	 * @ejb.interface-method view-type="local"
	 */
	public Map getSubProblemNameMap(long assignmentID) {
		return null;
	}
	
	/**
	 * @param assignmentID The AssignmentID of the assignment
	 * @return A mapping from SubProblemID (Long) -> SubProblemName (String)
	 *  valid within the given the given course
	 * @dao.call name="getSubProblemNameMapByCourse"
	 * @ejb.interface-method view-type="local"
	 */
	public Map getSubProblemNameMapByCourse(long courseID) {
		return null;
	}
	
	/**
	 * @param assignmentID The AssignmentID of the assignment
	 * @return A mapping from NetID (String) -> GroupID (Long), the 
	 *  group in which this user is Active for the given assignment
	 * @dao.call name="getGroupIDMap"
	 * @ejb.interface-method view-type="local"
	 */
	public Map getGroupIDMap(long assignmentID) {
		return null;
	}
	
	/**
	 * @param netID The student to find groups for
	 * @return A mapping from AssignmentID (Long) -> GroupID (Long),
	 *  the group in which the given user is active for the assignment
	 * @dao.call name="getGroupIDMap"
	 * @ejb.interface-method view-type="local"
	 */
	public Map getGroupIDMap(String netID) {
	    return null;
	}
	
	/**
	 * @param courseID
	 * @return A mapping from <NetID>_<AssignmentID> (String) -> GroupID (Long)
	 *  which is valid for all groups in the given course
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getGroupIDMapByCourse"
	 */
	public Map getGroupIDMapByCourse(long courseID) {
	    return null;
	}
	
	/**
	 * @param courseID
	 * @return A mapping from GroupID (Long) -> ListOfGroupMembers (String)
	 *  which is valid for all groups in the given course
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getGroupMemberListMap"
	 */
	public Map getGroupMemberListMap(long courseID) {
	    return null;
	}
	
	/**
	 * Returns a mapping from GroupID (Long) -> NetIDs (ArrayList) for every
	 * group in every assignment in the course.
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getGroupMembersMap"
	 */
	public Map getGroupMembersMap(long courseID) {
		return null;
	}
	
	/* XXX THIS WAS NOT IMPLEMENTED CORRECTLY - no such column as AssignmentID - Alex, Feb 2008
	/**
	 * @param assignmentID The AssignmentID of the assignment
	 * @return A mapping from GroupID (Long) -> GroupSize (Int) which is
	 *  valid within the assignment.  
	 * 	GroupSize = the number of active group members in the group.
	 *  ** Only contains mappings for Groups which have at least 1 active member.
	 * @dao.call name="getGroupSizeMap"
	 * @ejb.interface-method view-type="local"
	 *
	public Map getGroupSizeMap(long assignmentID) {
		return null;
	}*/
	
	/**
	 * @param assignmentID
	 * @return A mapping from CommentFileID (Long) -> RequestIDs (Collection of Longs) valid
	 *  within thin given assignment.  If the CommentFileID does not belong
	 *  to a comment which is a response to a RegradeRequest, then it is mapped
	 *  to null. 
	 * @dao.call name="getCommentFileRequestIDMap"
	 * @ejb.interface-method view-type="local"
	 */
	public Map getCommentFileRequestIDMap(long assignmentID) {
		return null;
	}
	
	/**
	 * @param courseID
	 * @return A mapping from NetID (String) -> Full Name (String)
	 * 	valid for all (including inactive) staff members in the given course
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getStaffNameMap"
	 */
	public Map getStaffNameMap(long courseID) {
	    return null;
	}

	/**
	 * @return A mapping from NetID (String) -> First/Last Name (String[2])
	 * 	valid for all (including inactive) staff members in the given course
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getStaffFirstLastNameMap"
	 */
	public Map getStaffFirstLastNameMap(long courseID) {
		return null;
	}
	
	/**
	 * @param netID
	 * @return A mapping from NetID (String) -> Full Name (String)
	 *  valid for all staff members in any course in which the given
	 *  NetID is an enrolled student.
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getStaffNameMap"
	 */
	public Map getStaffNameMap(String netID) {
	    return null;
	}
	
	/**
	 * @param assignmentID
	 * @return A mapping from CommentFileID (Long) -> GroupID (Long), the
	 *  group to which the comment of this comment file belongs to, valid
	 *  within the given assignment
	 * @dao.call name="getCommentFileGroupIDMap"
	 * @ejb.interface-method view-type="local"
	 */
	public Map getCommentFileGroupIDMap(long assignmentID) {
	    return null;
	}
	
	/**
	 * @param assignmentID
	 * @return A mapping from CommentFileID (Long) -> RequestIDs (Collection of Longs) valid
	 *  within the given course. If the CommentFileID does not belong
	 *  to a comment which is a response to a RegradeRequest, then it is mapped
	 *  to null. 
	 * @dao.call name="getCommentFileRequestIDMapByCourse"
	 * @ejb.interface-method view-type="local"
	 */
	public Map getCommentFileRequestIDMapByCourse(long courseID) {
		return null;
	}
	
	/**
	 * @param assignmentID
	 * @return A mapping from CommentFileID (Long) -> GroupID (Long), the
	 *  group to which the comment of this comment file belongs to, valid
	 *  within the given assignment
	 * @dao.call name="getCommentFileGroupIDMapByCourse"
	 * @ejb.interface-method view-type="local"
	 */
	public Map getCommentFileGroupIDMapByCourse(long courseID) {
	    return null;
	}
	
	/**
	 * Creates a new category for specified course
	 * @param templ - the metadata for the category and columns associated with it
	 * @return
	 * @throws CreateException
	 * @ejb.interface-method view-type="local"
	 */
	public CategoryLocal createCategory(CategoryTemplate templ) throws CreateException{
		try{
			return categoryLocalHome.create(templ.getCourseID(), templ.getCategoryName(), 
					templ.getAscending(), templ.getSortByColId(), templ.getNumShowItems(), 
					templ.getHidden(), templ.getFileCount(), templ.getAuthorzn(), 
					templ.getPositn(), templ.isSpecial());
		}catch(Exception e){
			throw new CreateException(e.getMessage());
		}
	}
	

	/**
	 * Creates a new col for specified category 
	 * @param col- the metadata for the new column
	 * @param categoryID - id of specified category
	 * @return
	 * @throws CreateException
	 * @ejb.interface-method view-type="local"
	 */
	public CategoryColLocal createCtgColumn(CtgColInfo col, long categoryID) throws CreateException{
		try{
			return categoryColLocalHome.create(col.getColName(), col.getColType(), categoryID, col.getHidden(), 
					col.getRemoved(), col.getPosition());
		}catch(Exception e){
			throw new CreateException(e.getMessage());
		}
	}
	
	/**
	 * Creates a new row for specified category
	 * @param row - the metadata associated with new row
	 * @param categoryID - id of specified category
	 * @return
	 * @throws CreateException
	 * @ejb.interface-method view-type="local"
	 */
	public CategoryRowLocal createCtgRow(CtgRowInfo row, long categoryID) throws CreateException{
		try{
			return categoryRowLocalHome.create(categoryID, row.getHidden(), row.getReleaseDate());
		}catch(Exception e){
			throw new CreateException(e.getMessage());
		}
	}
	
	/**
	 * Creates a new content bean for specified category. DOES NOT create supporting beans.
	 * Files should be created with createNEditCtgFiles() below.
	 * @param content
	 * @return CategoryContents (non-null)
	 * @throws CreateException on ANY problem
	 * @ejb.interface-method view-type="local"
	 */
	public CategoryContentsLocal createCtgContent(CtgNewContentInfo content) throws CreateException{
		try
		{
			CategoryContentsLocal contentBean = 
				categoryContentsLocalHome.create(content.getColID(), content.getColType(), 
								content.getRowID(), content.getNumber(), content.getDate(), 
								//the URL address (for contents of url type) is stored in the text field of the bean
								(content.getColType() == CtgUtil.CTNT_URL) ? content.getURLAddress() : content.getText(), 
								content.getURLLabel());
			return contentBean;
		}catch(Exception e){
			throw new CreateException(e.getMessage());
		}
	}
	
	/**
	 * Creates all the new files associated with the specified (existing, newly changed) content.
	 * Does the same thing as createNEditCtgFiles(CtgNewFileContentInfo, long), but with different input types.
	 * @param content An info object for an existing cell with type file. This will contain an ordered list
	 * of fileInfos and String labels. When the info is non-null, a file will be created; when the info is null
	 * and the label is non-null, either a file will be added to the desired cell or the label on an existing
	 * file will be changed.
	 * @param log The Log to which we should append relevant details
	 * @return success
	 * @throws CreateException
	 * @ejb.interface-method view-type="local"
	 */
	public boolean createNEditCtgFiles(CtgCurFileContentInfo content, LogData log) throws CreateException
	{
		boolean success = true;
		final boolean hidden = false;
		try{
			List l = content.getFileInfoLabelList();
			for(int i = 0; i < l.size(); i++)
			{
				if(!success) break;
			//	System.out.println("size " + l.size() + ", i = " + i);
				FilePlusLabel info = (FilePlusLabel)l.get(i);
				CtgFileInfo fileInfo = info.file; //could be null
				String fileLabel = info.label; //could be null
				if(fileInfo != null && fileInfo.getFileName() != null) //if we actually have a file
				{
					CategoryFileLocal file = categoryFileLocalHome.create(content.getContentID(), fileInfo.getFileName(),
																				hidden, fileInfo.getFilePath(), fileLabel);
					if(file == null) success = false;
					else TransactionsBean.appendDetail(log, "Created file '" + fileInfo.getFileName() + "' with label '" + fileLabel + "'");
				}
				else if(fileLabel != null) //if there's a file label but not a file
				{
					CategoryFileLocal file = categoryFileLocalHome.create(content.getContentID(), null, hidden, null, fileLabel);
					if(file == null) success = false;
					else TransactionsBean.appendDetail(log, "Created empty file with label '" + fileLabel + "'");
				}
			}
		}catch(Exception e){
			System.out.println("RootBean::createNEditCtgFiles(): caught exception " + e.getClass().getName() + ": " + e.getMessage());
			throw new CreateException(e.getMessage());
		}
		return success;
	}
	
	/**
	 * Creates all the new files associated with the specified (newly created) content.
	 * Does the same thing as createNEditCtgFiles(CtgCurFileContentInfo), but with different input types.
	 * @param content An info object for a newly created cell with type file. This will contain an ordered list
	 * of fileInfos and String labels. When the info is non-null, a file will be created; when the info is null
	 * and the label is non-null, either a file will be added to the desired cell or the label on an existing
	 * file will be changed.
	 * @param contentID The just-generated ID for this cell
	 * @param log The Log to which we should append relevant details
	 * @return success
	 * @throws CreateException
	 * @ejb.interface-method view-type="local"
	 */
	public boolean createNEditCtgFiles(CtgNewFileContentInfo content, long contentID, LogData log) throws CreateException
	{
		//transform CtgNewFileContentInfo data to the format for a CurFileInfo, and call our sister function
		ArrayList fileList = new ArrayList(), labelList = new ArrayList();
		for(int i = 0; i < content.getNumFiles(); i++)
		{
			//the file and label lists better be the same length
			fileList.add(content.getFileInfo(i));
			labelList.add(content.getFileLabel(i));
		}
		return createNEditCtgFiles(new CtgCurFileContentInfo(contentID, fileList, labelList), log);
	}

	/**
	 * Returns true only if the given NetID is assigned to grade ALL
	 * the given groups in at least one subproblem
	 * @param netID
	 * @param groupIDs A Collection of Long objects
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="isAssignedTo"
	 */
	public boolean isAssignedTo(String netID, Collection groupIDs) {
	    return false;
	}
	
	/**
	 * Checks to ensure that a collection of GroupIDs all refer to existing
	 * groups, and that all groups are within the same assignment.  
	 * Returns the AssignmentID of that assignment as a Long if all groups
	 * exist and are in the same assignment, null otherwise. 
	 * Returns null on empty collection.
	 * @param groupIDs
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public Long isValidGroupCollection(Collection groupIDs) {
	    try {
	        Long result = null;
	        Collection c = groupHome().findByGroupIDs(groupIDs);
	        if (c.size() != groupIDs.size()) return null;
	        Iterator groups = c.iterator();
	        while (groups.hasNext()) {
	            GroupLocal group = (GroupLocal) groups.next();
	            if (result == null) {
	                result = new Long(group.getAssignmentID());
	            } else {
	                if (result.longValue() != group.getAssignmentID()) {
	                    return null;
	                }
	            }
	        }
	        return result;
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return null;
	}
	
	/**
	 * Returns whether or not a given netID is a valid user
	 * @param netID NetID to check
	 * @return Whether or not netID is a valid user
	 * @ejb.interface-method view-type="local"
	 */
	public boolean isUser(String netID) {
		try {
			UserLocal u= userLocalHome.findByUserID(netID);
			return (u != null);
		} catch (Exception re) {
			System.out.println("Error in RootBean.isUser(): " + re);
			return false;
		}
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public AnnouncementLocalHome announcementHome() {
		return announcementLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public AnswerLocalHome answerHome() {
		return answerLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public AnswerSetLocalHome answerSetHome() {
		return answerSetLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public AssignmentLocalHome assignmentHome() {
		return assignmentLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public AssignmentFileLocalHome assignmentFileHome() {
		return assignmentFileLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public AssignmentItemLocalHome assignmentItemHome() {
		return assignmentItemLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public CategoryLocalHome categoryHome() {
		return categoryLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public CategoryColLocalHome categoryColHome() {
		return categoryColLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public CategoryContentsLocalHome categoryContentsHome() {
		return categoryContentsLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public CategoryFileLocalHome categoryFileHome() {
		return categoryFileLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public CategoryRowLocalHome categoryRowHome() {
	    return categoryRowLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public ChoiceLocalHome choiceHome() {
		return choiceLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public CMSAdminLocalHome cmsAdminHome() {
		return cmsAdminLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public CommentLocalHome commentHome() {
		return commentLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public CommentFileLocalHome commentFileHome() {
		return commentFileLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public CourseLocalHome courseHome() {
		return courseLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public DomainLocalHome domainHome() {
		return domainLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public EmailLocalHome emailHome() {
	    return emailLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public GradeLocalHome gradeHome() {
		return gradeLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public GroupAssignedToLocalHome groupAssignedToHome() {
		return groupAssignedToLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public GroupLocalHome groupHome() {
		return groupLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public GroupGradeLocalHome groupGradeHome() {
		return groupGradeLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public GroupMemberLocalHome groupMemberHome() {
		return groupMemberLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public LogLocalHome logHome() {
		return logLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public OldAnnouncementLocalHome oldAnnouncementHome() {
		return oldAnnouncementLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public RegradeRequestLocalHome regradeRequestHome() {
		return regradeRequestLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public RequiredFileTypeLocalHome requiredFileTypeHome() {
		return requiredFileTypeLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public RequiredSubmissionLocalHome requiredSubmissionHome() {
		return requiredSubmissionLocalHome;
	}

	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public SiteNoticeLocalHome siteNoticeHome() {
		return siteNoticeLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public SemesterLocalHome semesterHome() {
		return semesterLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public SolutionFileLocalHome solutionFileHome() {
		return solutionFileLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public StaffLocalHome staffHome() {
		return staffLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public StudentLocalHome studentHome() {
		return studentLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public SubmittedFileLocalHome submittedFileHome() {
		return submittedFileLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public SubProblemLocalHome subProblemHome() {
		return subProblemLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public TimeSlotLocalHome timeSlotHome() {
		return timeSlotLocalHome;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public UserLocalHome userHome() {
		return userLocalHome;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @dao.call name="getAllUsers"
	 * @return
	 */
	public Collection getAllUsers() {
		return null;
	}
	
}
