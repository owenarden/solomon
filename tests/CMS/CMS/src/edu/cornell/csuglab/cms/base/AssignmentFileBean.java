/*
 * Created on Apr 15, 2004
 */
package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.EntityBean;
import javax.ejb.FinderException;

import edu.cornell.csuglab.cms.www.util.FileUtil;
/**
 * This contains information for source files associated with 
 * an assignment. An AssignmentFile has a corresponding AssignmentItem
 * that holds metadata.
 * 
 * @ejb.bean name="AssignmentFile"
 *	jndi-name="AssignmentFileBean"
 *	type="BMP" 
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.AssignmentFileDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.AssignmentFileDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/
public abstract class AssignmentFileBean implements EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long assignmentFileID;
	private String path;
	private String fileName;
	private Timestamp fileDate;
	private long assignmentItemID;
	private boolean hidden;

	private AssignmentItemLocalHome assignmentItemHome = null;
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the fileID for the source file.
	 */
	public long getAssignmentFileID() {
		return assignmentFileID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileID The fileID to set.
	 */
	public void setAssignmentFileID(long assignmentFileID) {
		this.assignmentFileID = assignmentFileID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getAssignmentItemID() {
		return assignmentItemID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param name
	 */
	public void setAssignmentItemID(long assignmentItemID) {
		this.assignmentItemID = assignmentItemID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the fileName.
	 */
	public String getFileName() {
		return fileName;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileName The fileName to set.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public Timestamp getFileDate() {
		return fileDate;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileDate
	 */
	public void setFileDate(Timestamp fileDate) {
		this.fileDate = fileDate;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns whether the source files are visible on the student side or not.
	 */
	public boolean getHidden() {
		return hidden;
	}
	/**
	 * @ejb.interface-method view-type="local"
	 * @param hidden To set whether or not the source files should be visible 
	 * 				 on the student side or not.
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getPath() {
		return path;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileCounter
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Checks to see if the given primary key can be found in the database
	 * @param pk The primary key being serarched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public AssignmentFilePK ejbFindByPrimaryKey (AssignmentFilePK key) throws FinderException {
		return null;
	}
	/*
	 * Finds all the source files for all assignments in a given course
	 * @param courseid The courseID
	 * @return The collection of all the source files 
	 * TODO: order the result in increasing order of assignmentID->fileID
	 * @throws FinderException
	 *
	public Collection ejbFindByCourseID (long courseid) throws FinderException {
		return null;
	}*/
	
	/**
	 * Finds all the non-hidden files for a given assignment
	 * @param assignmentid The assignmentID
	 * @return The collection of all the source files.
	 * @throws FinderException
	 */
	public Collection ejbFindByAssignmentID (long assignmentid) throws FinderException {
		return null;
	}
	
	/**
	 * @param assignmentItemID
	 * @return
	 * @throws FinderException
	 */
	public AssignmentFilePK ejbFindByAssignmentItemID(long assignmentItemID) throws FinderException {
		return null;
	}
	
	/**
	 * @param assignmentItemID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindHiddenByAssignmentItemID(long assignmentItemID) throws FinderException {
		return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @return
	 * @throws EJBException
	 */
	public String getItemName() throws EJBException {
		try {
			AssignmentItemLocal item = assignmentItemHome().findByPrimaryKey(new AssignmentItemPK(getAssignmentItemID()));
			String itemName = item.getItemName();
			String[] filetype = FileUtil.splitFileNameType(getFileName());
			itemName += (filetype[1].equals("") ? "" : "." + filetype[1]);
			return itemName;
		} catch (Exception e) {
			throw new EJBException(e);
		}
	}
	
	/**
	 * @return
	 */
	private AssignmentItemLocalHome assignmentItemHome() {
		try {
			if (assignmentItemHome == null) {
				assignmentItemHome = AssignmentItemUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assignmentItemHome;
	}
	
	// FIXME
	public String getFilePath() {
	    return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @return
	 * @throws EJBException
	 */
	public java.io.File getFile() throws EJBException {
		java.io.File result;
		try {
			result = new java.io.File(getFilePath());
		}
		catch (Exception e) {
			throw new EJBException(e);
		}
		return result;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 **/
	public AssignmentFileData getAssignmentFileData() {
		return new AssignmentFileData(getAssignmentFileID(),
				getAssignmentItemID(), getFileName(), getFileDate(), getHidden(), getPath()); 
	}
	
	/**
	 * 
	 * @param assignmentID
	 * @param fileName
	 * @param isHidden
	 * @ejb.create-method view-type="local"
	 */
	public AssignmentFilePK ejbCreate(long assignmentItemID, String fileName, boolean isHidden, String path) throws CreateException {
		setAssignmentItemID(assignmentItemID);
		setFileName(fileName);
		setFileDate(new Timestamp(System.currentTimeMillis()));
		setHidden(isHidden);
		setPath(path);
		return null;
	}
}
