/*
 * Created on Nov 10, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.Collection;

import javax.ejb.FinderException;
 /**
 * @ejb.bean name="CategoryRow"
 *	jndi-name="CategoryRowBean"
 *	type="BMP"
 *
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class = "edu.cornell.csuglab.cms.base.CategoryRowDAO"
 * impl-class = "edu.cornell.csuglab.cms.base.dao.CategoryRowDAOImpl"
 * 
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * */

public abstract class CategoryRowBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
    private long rowID;
	private long categoryID;
	private boolean hidden;
	private Timestamp releaseDate;
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the unique id identifying the row
	 */
	public long getRowID(){
		return this.rowID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param rowID: the unique id identifying the row
	 */
	public void setRowID(long rowID){
		this.rowID = rowID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the id of the category this row is associated with
	 */
	public long getCategoryID(){
		return this.categoryID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param categoryID: the id of the category this row is associated with
	 */
	public void setCategoryID(long categoryID){
		this.categoryID = categoryID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return whether this row should be hidden
	 */
	public boolean getHidden(){
		return this.hidden;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param hidden: whether this row should be hidden
	 */
	public void setHidden(boolean hidden){
		this.hidden = hidden;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the date this row should be released
	 */
	public Timestamp getReleaseDate(){
		return this.releaseDate;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param releaseDate: the date this row should be released
	 */
	public void setReleaseDate(Timestamp releaseDate){
		this.releaseDate = releaseDate;
	}
	
	/**
	 * @ejb.create-method view-type="local"
	 * @return
	 * @throws javax.ejb.CreateException
	 */
  public CategoryRowPK ejbCreate(long categoryID, boolean hidden, Timestamp releaseDate) throws javax.ejb.CreateException {
    this.setCategoryID(categoryID);
  	this.setHidden(hidden);
  	this.setReleaseDate(releaseDate);
    return null;
  }

  /**
   * 
   * @param pk
   * @return
   * @throws FinderException
   */
  public CategoryRowPK ejbFindByPrimaryKey(CategoryRowPK pk) throws FinderException{
	return null;
}
  /**
   * 
   * @param courseID
   * @return
   * @throws FinderException
   */
  public Collection ejbFindByCourseID(long courseID) throws FinderException{
  	return null;
  }
  
  
  /**
   *  @ejb.interface-method view-type="local"
   * @return
   */
	public CategoryRowData getCategoryRowData(){
		return new CategoryRowData(this.getRowID(), this.getCategoryID(), this.getHidden(), this.getReleaseDate());
	}

}
