/*
 * Created on Aug 1, 2004
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.AnswerSetBean;
import edu.cornell.csuglab.cms.base.AnswerSetDAO;
import edu.cornell.csuglab.cms.base.AnswerSetPK;

/**
 * @author Yan
 *
 */
public class AnswerSetDAOImpl extends DAOMaster implements AnswerSetDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AnswerSetDAO#load(edu.cornell.csuglab.cms.base.AnswerSetPK, edu.cornell.csuglab.cms.base.AnswerSetBean)
	 */
	public void load(AnswerSetPK pk, AnswerSetBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tAnswerSets where AnswerSetID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getAnswerSetID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setAnswerSetID(rs.getLong("AnswerSetID"));
				ejb.setAssignmentID(rs.getLong("AssignmentID"));
				ejb.setNetID(rs.getString("NetID"));
				ejb.setGroupID(rs.getLong("GroupID"));
				ejb.setOriginalGroupID(rs.getLong("OriginalGroupID"));
				ejb.setSubmissionDate(rs.getTimestamp("SubmissionDate"));
				ejb.setLateSubmission(rs.getInt("LateSubmission"));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {
			}
			throw new EJBException("Row id " + pk.getAnswerSetID()
					+ " failed to load in tAnswerSets.", e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AnswerSetDAO#store(edu.cornell.csuglab.cms.base.AnswerSetBean)
	 */
	public void store(AnswerSetBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tAnswerSets set AssignmentID = ?, NetID = ?, " +
					"GroupID = ?, OriginalGroupID = ?, SubmissionDate = ?, LateSubmission = ? where AnswerSetID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, ejb.getAssignmentID());
			ps.setString(2, ejb.getNetID());
			ps.setLong(3, ejb.getGroupID());
			ps.setLong(4, ejb.getOriginalGroupID());
			ps.setTimestamp(5, ejb.getSubmissionDate());
			ps.setInt(6, ejb.getLateSubmission());
			
			ps.setLong(7, ejb.getAnswerSetID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {
				f.printStackTrace();
			}
			throw new EJBException("Row id " + ejb.getAnswerSetID() + ", failed to store properly", e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AnswerSetDAO#remove(edu.cornell.csuglab.cms.base.AnswerSetPK)
	 */
	public void remove(AnswerSetPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AnswerSetDAO#create(edu.cornell.csuglab.cms.base.AnswerSetBean)
	 */
	public AnswerSetPK create(AnswerSetBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AnswerSetPK pk = null;
		try {
			conn = jdbcFactory.getConnection();
			String updateQuery = "insert into tAnswerSets " +
					"(AssignmentID, NetID, GroupID, OriginalGroupID, SubmissionDate, " +
					"LateSubmission) values " +
					"(?, ?, ?, ?, ?, ?)";
			String findQuery = "select @@identity as AnswerSetID from tAnswerSets";
			ps = conn.prepareStatement(updateQuery);
			ps.setLong(1, ejb.getAssignmentID());
			ps.setString(2, ejb.getNetID());
			ps.setLong(3, ejb.getGroupID());
			ps.setLong(4, ejb.getOriginalGroupID());
			ps.setTimestamp(5, ejb.getSubmissionDate());
			ps.setInt(6, ejb.getLateSubmission());
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findQuery).executeQuery();
			if (count > 0 && rs.next()) {
				long answerSetID = rs.getLong("AnswerSetID"); 
				pk = new AnswerSetPK(answerSetID);
				ejb.setAnswerSetID(answerSetID);
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				e.printStackTrace();
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {
				f.printStackTrace();
			}
			throw new CreateException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AnswerSetDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.AnswerSetPK)
	 */
	public AnswerSetPK findByPrimaryKey(AnswerSetPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select AnswerSetID from tAnswerSets where AnswerSetID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getAnswerSetID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find submitted file with AnswerSetID = " + pk.getAnswerSetID());
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			try {
				throw ((FinderException) e);  //XXX wtf?
			}
			catch (ClassCastException x) {
				throw new FinderException("Caught Exception: " + e.getMessage());
			}
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AnswerSetDAO#findByAssignmentID(long)
	 */
	public Collection findByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT a.NetID, max(a.AnswerSetID) as AnswerSetID " +
				"FROM tAnswerSets a " + 
				"WHERE a.AssignmentID = ? " +
				"GROUP BY a.NetID " + 
				"ORDER BY max(SubmissionDate)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				long answerSetID = rs.getLong("AnswerSetID");
				result.add(new AnswerSetPK(answerSetID));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GradeDAO#findMostRecentByNetAssignmentID(java.lang.String,
	 *      long)
	 */
	public AnswerSetPK findMostRecentByGroupAssignmentID(long groupid,
			long assignmentid) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AnswerSetPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT a.AnswerSetID "
					+ "FROM (SELECT max(submissiondate) as currentdate "
					+ "FROM tanswersets WHERE assignmentid = ? AND groupid = ? "
					+ "GROUP BY assignmentid ) AS t, tanswersets AS a "
					+ "WHERE a.assignmentid = ? AND groupid = ? "
					+ "AND submissiondate = currentdate";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentid);
			ps.setLong(2, groupid);
			ps.setLong(3, assignmentid);
			ps.setLong(4, groupid);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = new AnswerSetPK(rs.getLong(1));
			}
			else{
				throw new FinderException("No answer set submitted for group " + groupid + " assignment " + assignmentid);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findByGroupIDs(Collection groupids) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (groupids.size() == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT DISTINCT a.answersetid, a.submissiondate FROM tAnswerSets a INNER JOIN " +
					"tGroupMembers m ON a.NetID = m.NetID INNER JOIN " + 
                    "tGroups gr ON m.GroupID = gr.GroupID AND a.AssignmentID = gr.AssignmentID WHERE (";
			for (int i=0; i < groupids.size() - 1; i++) {
				queryString += "gr.GroupID = ? or ";
			}
			queryString += "gr.GroupID = ?) order by a.submissiondate DESC";
			ps = conn.prepareStatement(queryString);
			int c=1;
			for (Iterator i=groupids.iterator(); i.hasNext(); ) {
				ps.setLong(c++, ((Long)i.next()).longValue());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				AnswerSetPK pk = new AnswerSetPK(rs.getLong(1));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findByGroupIDsAssignedTo(Collection groupids, String grader, int numSubProbs) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (groupids.size() == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT DISTINCT ans.AnswerSetID, ans.SubmissionDate " +
			    	"FROM tAnswerSets ans INNER JOIN " +
			    	"tGroupMembers m ON ans.NetID = m.NetID INNER JOIN " + 
			        "tGroups gr ON m.GroupID = gr.GroupID AND ans.AssignmentID = gr.AssignmentID INNER JOIN " +
			        "tGroupAssignedTo ga ON gr.GroupID = ga.GroupID INNER JOIN " +
			        "tAnswers a ON ans.AnswerSetID = a.AnswerSetID LEFT OUTER JOIN " +
			        "(SELECT x.GroupID, COUNT(DISTINCT x.SubProblemID) AS Assigned " +
                       "FROM tGroupAssignedTo x INNER JOIN tSubProblems y ON x.SubProblemID = y.SubProblemID " +
                       "WHERE (x.NetID = ?) AND (y.Hidden = ?) GROUP BY x.GroupID) t ON t.GroupID = gr.GroupID " +
			        "WHERE (ga.NetID = ?) AND (Assigned = ? OR ga.SubProblemID = a.SubProblemID) AND (";
			for (int i=0; i < groupids.size() - 1; i++) {
				queryString += "gr.GroupID = ? or ";
			}
			queryString += "gr.GroupID = ?) order by ans.SubmissionDate DESC";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, grader);
			ps.setBoolean(2, false);
			ps.setString(3, grader);
			ps.setInt(4, numSubProbs);
			int c=5;
			for (Iterator i=groupids.iterator(); i.hasNext(); ) {
				ps.setLong(c++, ((Long)i.next()).longValue());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				AnswerSetPK pk = new AnswerSetPK(rs.getLong(1));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;
	}
	

	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AnswerSetDAO#findByAnswerSetID(long)
	 */
	public AnswerSetPK findByAnswerSetID(long answerSetID) throws FinderException {
		return findByPrimaryKey(new AnswerSetPK(answerSetID));
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AnswerSetDAO#findHiddenByAssignmentID(long)
	 */
	public Collection findHiddenByAssignmentID(long assignmentID) throws FinderException {
		// TODO Auto-generated method stub
		return null;
	}

}
