/*
 * Created on Apr 22, 2005
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.OldAnnouncementBean;
import edu.cornell.csuglab.cms.base.OldAnnouncementDAO;
import edu.cornell.csuglab.cms.base.OldAnnouncementPK;

/**
 * @author yc263
 *
 */
public class OldAnnouncementDAOImpl extends DAOMaster implements OldAnnouncementDAO{

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.OldAnnouncementDAO#load(edu.cornell.csuglab.cms.base.OldAnnouncementPK, edu.cornell.csuglab.cms.base.OldAnnouncementBean)
	 */
	public void load(OldAnnouncementPK pk, OldAnnouncementBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tOldAnnouncement where oldAnnouncementid = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getOldAnnouncementID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setOldAnnouncementID(rs.getLong("OldAnnouncementID"));
				ejb.setAnnouncementID(rs.getLong("AnnouncementID"));
				try {
					ejb.setText(rs.getString("Text"));
				} catch (SQLException e) {
					ejb.setText("");
				}
			}
			rs.close();
			ps.close();
			conn.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.OldAnnouncementDAO#store(edu.cornell.csuglab.cms.base.OldAnnouncementBean)
	 */
	public void store(OldAnnouncementBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "update tOldAnnouncement "
					+ "set announcementID = ?, text = ? "
					+ "where oldAnnouncementid = ?";
			ps = conn.prepareStatement(update);
			ps.setLong(1, ejb.getAnnouncementID());
			if(ejb.getText() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getText());
			ps.setLong(3, ejb.getOldAnnouncementID());
			ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (Exception e) {
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.OldAnnouncementDAO#remove(edu.cornell.csuglab.cms.base.OldAnnouncementPK)
	 */
	public void remove(OldAnnouncementPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.OldAnnouncementDAO#create(edu.cornell.csuglab.cms.base.OldAnnouncementBean)
	 */
	public OldAnnouncementPK create(OldAnnouncementBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		OldAnnouncementPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "insert into tOldAnnouncement "
					+ "(announcementid, text) values "
					+ "(?,?)";
			String findString = "select @@identity as 'oldAnnouncementid' from tOldAnnouncement";
			ps = conn.prepareStatement(createString);
			ps.setLong(1, ejb.getAnnouncementID());
			if(ejb.getText() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getText());
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findString).executeQuery();
			if ((count == 1) && rs.next()) {
				result = new OldAnnouncementPK(rs.getLong(1));
				ejb.setOldAnnouncementID(result.getOldAnnouncementID());
			} else {
				throw new CreateException("Failed to create new row in the database");
			}
			rs.close();
			ps.close();
			conn.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.OldAnnouncementDAO#findByAnnouncementID(long)
	 */
	public Collection findByAnnouncementID(long announcementID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT oldAnnouncementID FROM tOldAnnouncement "
								+ "WHERE announcementID = ? "
								+"ORDER BY oldAnnouncementID DESC";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, announcementID);
			rs = ps.executeQuery();
			while (rs.next()) {
				OldAnnouncementPK pk = new OldAnnouncementPK(rs.getLong(1));
				result.add(pk);
			}
			rs.close();
			ps.close();
			conn.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.OldAnnouncementDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.OldAnnouncementPK)
	 */
	public OldAnnouncementPK findByPrimaryKey(OldAnnouncementPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT oldAnnouncementID from tOldAnnouncement "
								+ "WHERE oldAnnouncementID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, pk.getOldAnnouncementID());
			rs = ps.executeQuery();
			if(!rs.next()) 
				throw new FinderException("Can't find oldAnnouncement with id = "+pk.getOldAnnouncementID());
			rs.close();
			ps.close();
			conn.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

}
