/*
 * Created on Feb 20, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.Collection;

import javax.ejb.FinderException;

/**
 * @ejb.bean name="TimeSlot"
 *	jndi-name="TimeSlotBean"
 *	type="BMP" 
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.TimeSlotDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.TimeSlotDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/

public abstract class TimeSlotBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long timeslotID;
	private long assignmentID;
	private long courseID;
	private int population;
	private String staff;
	private String name, location;
	private Timestamp startTime;
	private boolean hidden;
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the timeslotID
	 */
	public long getTimeSlotID() {
		return timeslotID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param timeslotID The timeslot ID to set this to
	 */
	public void setTimeSlotID(long ID) {
		this.timeslotID = ID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the assignmentID
	 */
	public long getAssignmentID() {
		return assignmentID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param assignmentID The assignment ID to set this to
	 */
	public void setAssignmentID(long ID) {
		this.assignmentID = ID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the courseID
	 */
	public long getCourseID() {
		return courseID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param courseID The course ID to set this assignment to
	 */
	public void setCourseID(long courseID) {
		this.courseID = courseID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the full name of the assignment 
	 */
	public String getName() {
		return name;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param name The name to call this assignment
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getLocation() {
		return location;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param location
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the staff overseeing this timeslot
	 */
	public String getStaff() {
		return staff;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param staff The staff overseeing this timeslot
	 */
	public void setStaff(String staff) {
		this.staff = staff;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The start time of the assignment
	 */
	public Timestamp getStartTime() {
		return startTime;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param startTime The start time to set
	 */
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public boolean getHidden() {
	    return hidden;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param hidden
	 */
	public void setHidden(boolean hidden) {
	    this.hidden = hidden;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public int getPopulation() {
	    return population;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param population
	 */
	public void setPopulation(int population) {
	    this.population = population;
	}
	
	/**
	 * Checks to see if the given primary key can be found in the database
	 * @param pk The primary key being searched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public TimeSlotPK ejbFindByPrimaryKey(TimeSlotPK pk)
			throws FinderException {
		return null;
	}
	
	/**
	 * Finds the information for the given timeslotID
	 * @param timeslotID The timeslot's unique ID
	 * @return The TimeSlot object for the given timeslotID
	 * @throws FinderException
	 */
	public TimeSlotPK ejbFindByTimeSlotID(long timeslotID)
			throws FinderException {
		return null;
	}
	
	/**
	 * Finds all time slots in an assignment for which a given staff
	 * member is assigned to overlapping time slots
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindConflictingByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds the information for the given assignmentID
	 * @param assignmentID The timeslot's assignmentID
	 * @return The collection of TimeSlot objects for the given assignmentID
	 * @throws FinderException
	 */
	public Collection ejbFindByAssignmentID(long assignmentID)
			throws FinderException {
		return null;
	}

	/**
	 * Finds all timeslots for an assignment, including the hidden ones.
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindAllByAssignmentID(long cassignmentID) throws FinderException {
	    return null;
	}

	/**
	 * Finds all hidden timeslots in a course
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindHiddenByAssignmentID(long assignmentID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all active timeslots for a course admin
	 * @param courseID, netID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseAdmin(long courseID, String netID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds the timeslot which is associated with the given group
	 * @param groupID
	 * @return
	 */
	public TimeSlotPK ejbFindByGroupID(long groupID) throws FinderException {
	    return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 **/
	public TimeSlotData getTimeSlotData() {
		return new TimeSlotData(getTimeSlotID(), getAssignmentID(), getCourseID(), getName(),
				 getLocation(), getStaff(), getStartTime(), getHidden(), getPopulation());
	}

	/**
	 * Creates a new timeslot for the given course with a specified 
	 * name and due date. The unique timeslotID is assigned automatically, 
	 * and all the other fields are empty.
	 * @param courseid The courseID
	 * @param assignmentid The assignmentID
	 * @return The new timeslot object
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local"
	 */
	public TimeSlotPK ejbCreate (TimeSlotData newTimeSlot)
			throws javax.ejb.CreateException {
		setCourseID(newTimeSlot.getCourseID());
		setName(newTimeSlot.getName());
		setAssignmentID(newTimeSlot.getAssignmentID());
		setStartTime(newTimeSlot.getStartTime());
		setStaff(newTimeSlot.getStaff());
		setName(newTimeSlot.getName());
		setLocation(newTimeSlot.getLocation());
		setPopulation(newTimeSlot.getPopulation());
		setHidden(false);
		return null;
	}
}
