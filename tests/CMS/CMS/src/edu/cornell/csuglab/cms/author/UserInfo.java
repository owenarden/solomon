/*
 * Created on Apr 10, 2005
 */
package edu.cornell.csuglab.cms.author;

import java.util.Hashtable;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.rmi.PortableRemoteObject;

import edu.cornell.csuglab.cms.base.*;

/**
 * @author yc263
 *
 */
public class UserInfo {
	 private RootLocal database; // Session bean
	   private RootLocalHome dbHome;
	   private String NetID;

	   public UserInfo () {
		  try {
	        dbHome = getHome();
	        database = dbHome.create();
		  }  catch (Exception e) {
	        System.out.println("Error in UserInfo(): " + e);
		  }
	   }

	   /**
	    * @return The Session Bean home interface
	    * @throws NamingException If the RootHome cannot be found
	    */
	   private RootLocalHome getHome() throws NamingException {
//		  Object result = getContext().lookup(RootLocalHome.JNDI_NAME);
//		  return ((RootLocalHome) PortableRemoteObject.narrow(result, RootLocalHome.class));
	       return RootUtil.getLocalHome();
	   }

	   /*
	    * @return The InitalContext
	    * @throws NamingException If naming exception is encountered
	    */
	   private InitialContext getContext() throws NamingException {
	    
		   Hashtable props = new Hashtable();
	       props.put(InitialContext.INITIAL_CONTEXT_FACTORY,
	                  "org.jnp.interfaces.NamingContextFactory");
	       props.put(InitialContext.PROVIDER_URL, "jnp://localhost:1099");
	                  InitialContext ic = new InitialContext(props);
	       return ic;
	   }

	   /*
	    * @query the "User" table to get name for the given NetID
	    * @return the username 
	    */
	   public String getUsername (String NetID) {
	    
		  this.NetID = NetID;
		  String username = null;       
		  try {
	           UserLocal u = database.userHome().findByUserID(NetID);
	           username = u.getFirstName() + " " + u.getLastName();            
		  } catch (Exception re) {
	           System.out.println("Error in findUserByNetID() " + re);
	           re.printStackTrace();
		  }   
		  return username;
	   }

	   /* @query LDAP directory to get name of the authenticated user
	    * @return an 2 element array containing the FirstName of the NetID in the
	    *  first location, and the LastName of the NetID in the second
	    *  if either first or last name is empty, replace empty name(s) with initials
	    */
	   public static String[] getLDAPName (String NetID) {
			String[] name = new String[2];
			String firstName = null, lastName = null;
			//initalize default names to first and last initials (capitalization corrected later)
			name[0] = String.valueOf(NetID.charAt(0)) + ".";
			if (Character.isLetter(NetID.charAt(2)))
				name[1] = String.valueOf(NetID.charAt(2)) + ".";
			else
				name[1] = String.valueOf(NetID.charAt(1)) + ".";
			Properties env = new Properties();
			env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
			env.put("java.naming.provider.url", "ldap://directory.cornell.edu");
			try {
				DirContext ctx = new InitialDirContext(env);
			    Attributes attrs = ctx.getAttributes("uid=" + NetID + ", ou=People, " + 
					"o=Cornell University, c=US");
				if (attrs.getAll() != null) {
				    try {
				        firstName = (String)attrs.get("givenName").get();
				    } catch (NullPointerException e) {}
				    try {
				        lastName = (String)attrs.get("sn").get();
				    } catch (NullPointerException e) {}
				}
			} catch (Exception e) {
			    e.printStackTrace();
			}
			if (firstName != null) name[0] = firstName;
			if (lastName != null) name[1] = lastName;
			// Correct capitalization:
			for (int j= 0; j < 2; j++) {
			    int length= name[j].length();
			    if (length > 0) {
			        name[j]= Character.toUpperCase(name[j].charAt(0)) + 
			        	name[j].substring(1, length).toLowerCase();
			    }
			}	      
			return name;
	   }

}
