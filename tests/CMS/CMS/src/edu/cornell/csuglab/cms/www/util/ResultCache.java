package edu.cornell.csuglab.cms.www.util;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import edu.cornell.csuglab.cms.www.util.Profiler;

/**
 * Caches results from queries and provides efficient operations to invalidate
 * specific cache entries based on table updates.
 * 
 * @author lrw48
 *
 */
public class ResultCache {
	private static Map/*String, Object*/ cacheEntries = new HashMap();
	private static Map/*String, List*/ entriesByTable = new HashMap();
	
	/**
	 * Gets a cache entry keyed by name.
	 * @param name key name of cache entry
	 * @return Cache entry matching the (name, context) key or null if no
	 * entry exists.
	 */
	public static Object getCache(String name) {
		Profiler.enterMethod("ResultCache.getCache", name);
		Profiler.exitMethod("ResultCache.getCache", name);
		return cacheEntries.get(name);
	}
	
	/**
	 * Adds a cache entry. Cache entry is keyed by the (name, context) pair.
	 * @param name Key name of cache entry
	 * @param tables Any tables the entry relies on
	 * @param result Result to cache
	 */
	public static void addCacheEntry(String name, String[] tables, 
			Object result) {
		Profiler.enterMethod("ResultCache.addCacheEntry", name);
		cacheEntries.put(name, result);
		
		//Add reference of cache to tables map
		for(int i = 0; i < tables.length; i++) {
			List entry = (List)entriesByTable.get(tables[i]);
			if(entry == null) {
				entry = new ArrayList();
				entriesByTable.put(tables[i], entry);
			}
			entry.add(name);
		}

		Profiler.exitMethod("ResultCache.addCacheEntry", name);
	}
	
	/**
	 * Invalidates a cache entry by key name
	 * @param name Key name of cache entry to invalidate
	 */
	public static void invalidate(String name) {
		Profiler.enterMethod("ResultCache.invalidate", name);
		cacheEntries.remove(name);
		
		for(Iterator i = entriesByTable.values().iterator(); i.hasNext();) {
			List entry = (List)i.next();
			for(Iterator li = entry.iterator(); li.hasNext();) 
				((List)li.next()).remove(name);
		}
		
		Profiler.exitMethod("ResultCache.invalidate", name);
	}
	
	/**
	 * Invalidates all cache entries that rely on the given table
	 * @param name Name of table by which to invalidate reliant cache entries
	 */
	public static void invalidateByTable(String name) {
		Profiler.enterMethod("ResultCache.invalidateByTable", name);
		List entry = (List)entriesByTable.get(name);
		if(entry != null) {
			for(Iterator i = entry.iterator(); i.hasNext();) {
				cacheEntries.remove(i.next());
			}
			entriesByTable.remove(name);
		}
		
		Profiler.exitMethod("ResultCache.invalidateByTable", name);
	}
}
