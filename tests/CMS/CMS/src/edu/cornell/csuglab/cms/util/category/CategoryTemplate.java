/*
 * Created on Nov 18, 2004
 */
package edu.cornell.csuglab.cms.util.category;

import java.util.Collection;
import java.util.HashMap;


/**
 * @author yc263
 *
 * Represents options from the general category properties page for a single category:
 * options per current category; new categories to add; sorting & hiding options
 */
public class CategoryTemplate {
	public static String 
		ASCENDING = "asc",
		DESCENDING = "desc",
		ANNOUNCEMENTS = "Announcements",
		DATE = "Date",
		ANNOUNCEMENT = "Announcement";
	public static long 
		SHOWALL = Integer.MAX_VALUE;
	public static int
		DEFAULT_POSITN = 100,
		FIRST = 1,
		SECOND = 2, 
		DEFAULT_NUMTOSHOW = 5;
	
	private long courseID;
	private long categoryID;
	private String ctgName;
	private boolean ascending;
	private long sortByColID;
	private long numShowItems;
	private int authorzn;
	private int positn;
	private boolean hidden;
	private long fileCount;
	private boolean special; //whether the category holds announcements (they get special treatment)
	
	private HashMap newColumnMap; //map Longs (col IDs) to CtgColInfos
	private HashMap oldColumnMap; //map Longs (col IDs) to CtgColInfos
	private boolean sortColExist;
	
	public CategoryTemplate(){
		newColumnMap = new HashMap();
		oldColumnMap = new HashMap();
		this.hidden = false;
		this.sortColExist = false;
		this.fileCount = 0;
	}
	
	public CategoryTemplate(long courseID, String ctgName, long numShowItems, int authorzn ){
		this.courseID = courseID;
		this.ctgName = ctgName;
		this.numShowItems = numShowItems;
		this.authorzn = authorzn;
		this.special = true;
	}

	public void setCategoryID(long categoryID){
		this.categoryID = categoryID;
	}
	
	public void setCourseID(long courseID){
		this.courseID = courseID;
	}
	
	public void setCategoryName(String ctgName){
		this.ctgName = ctgName;
	}
	
	public void setAscending(boolean asc){
		this.ascending = asc;
	}
	
	public void setSortByColID(long sortByColID){
		this.sortByColID = sortByColID;
	}
	
	public void setNumShowItems(long numShowItems){
		this.numShowItems = numShowItems;
	}
	
	public void setAuthorzn(int authorzn){
		this.authorzn = authorzn;
	}
	
	public void setPositn(int positn){
		this.positn = positn;
	}
	
	public boolean getSortColExist(){
		return this.sortColExist;
	}
	
	public long getCategoryID(){
		return this.categoryID;
	}
	
	public String getCategoryName(){
		return this.ctgName;
	}
	
	public boolean getAscending(){
		return this.ascending;
	}
	
	public long getCourseID(){
		return this.courseID;
	}
	
	public boolean getHidden(){
		return this.hidden;
	}
	
	public long getSortByColId(){
		return this.sortByColID;
	}
	
	public long getNumShowItems(){
		return this.numShowItems;
	}
	
	public int getAuthorzn(){
		return this.authorzn;
	}
	
	public int getPositn(){
		return this.positn;
	}
	
	public long getFileCount(){
		return this.fileCount;
	}
	
	public boolean isSpecial(){
		return this.special;
	}

	public long getNumOfOldCol(){
		return this.oldColumnMap.size();
	}
	
	public long getNumOfNewCol()
	{
		return this.newColumnMap.size();
	}
	
	public long getTotalNumOfCol(){
		return getNumOfOldCol() + getNumOfNewCol();
	}
	
	private CtgColInfo getNewCol(long id){
		CtgColInfo col = (CtgColInfo)newColumnMap.get(new Long(id));
		if(col == null){
			col = new CtgColInfo();
			col.setPosition(0); //default value; should be overwritten
			newColumnMap.put(new Long(id), col);
		}
		return col;
	}
	
	public void addNewSortByColID(long id){
		this.sortColExist = true;
		CtgColInfo col = getNewCol(id);
			col.setSortByThis();
	}
	
	public void addNewColumnName(String colName, long id){
		CtgColInfo col= getNewCol(id);
		col.setColumnName(colName);
	}
	
	public void addNewColumnType(String colType, long id){
		CtgColInfo col= getNewCol(id);
		col.setColumnType(colType);
	}
	
	public void addNewColumnHidden(boolean hidden, long id)
	{
		CtgColInfo col = getNewCol(id);
		col.setHidden(hidden);
	}
	
	public void addNewColumnPosition(long pos, long id)
	{
		CtgColInfo col = getNewCol(id);
		col.setPosition(pos);
	}
	
	private CtgColInfo getOldCol(long id){
		CtgColInfo col = (CtgColInfo)oldColumnMap.get(new Long(id));
		if(col == null){
			col = new CtgColInfo(id);
			oldColumnMap.put(new Long(id), col);
		}
		return col;
	}
	
	public void addOldSortByColID(long id){
		this.sortColExist = true;
		CtgColInfo col = getOldCol(id);
		col.setSortByThis();
	}
	
	public void addOldColumnName(String colName, long id){
		CtgColInfo col= getOldCol(id);
		col.setColumnName(colName);
	}
	
	public void addOldColumnType(String colType, long id){
		CtgColInfo col= getOldCol(id);
		col.setColumnType(colType);
	}
	
	public void addOldColumnHidden(boolean hidden, long id)
	{
		CtgColInfo col = getOldCol(id);
		col.setHidden(hidden);
	}
	
	public void addOldColumnPosition(long position, long id){
		CtgColInfo col = getOldCol(id);
		col.setPosition(position);
	}
	
	public void updateHidden(long id, boolean hidden){
		CtgColInfo col= getOldCol(id);
		col.setHidden(hidden);
	}
	
	public void updateRemoved(long id, boolean removed)
	{
		CtgColInfo col = getOldCol(id);
		col.setRemoved(removed);
	}
	
	public void resetColumnMaps(){
		this.newColumnMap = new HashMap();
		this.oldColumnMap = new HashMap();
	}
	
	public Collection getNewColumnList(){
		return newColumnMap.values();
	}
	
	public Collection getOldColumnList(){
		return this.oldColumnMap.values();
	}
}
