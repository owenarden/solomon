package edu.cornell.csuglab.cms.util.category;

import java.sql.Timestamp;

/**
 * @author evan
 * 
 * created 9 / 1 / 05
 *
 * Holds info representing an existing content of type date, to be edited
 */
public class CtgCurDateContentInfo extends CtgCurContentInfo
{
	private Timestamp date;
	
	public CtgCurDateContentInfo(long contentID, Timestamp date)
	{
		super(contentID);
		this.date = date;
	}
	
	public Timestamp getDate()
	{
		return this.date;
	}
	
	public void setDate(Timestamp date)
	{
		this.date = date;
	}
	
	public String toString()
	{
		return "CurDateContentInfo{date: " + getDate() + "}";
	}
}
