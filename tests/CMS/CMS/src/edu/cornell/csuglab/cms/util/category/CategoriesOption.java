/*
 * Created on Mar 12, 2005
 */
package edu.cornell.csuglab.cms.util.category;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;


/**
 * @author yc263
 *
 * Holds info on changes to the order of categories for a course, and removing and restoring categories
 */
public class CategoriesOption {
	HashMap map;
	ArrayList restoreList;
	
	public CategoriesOption(){
		this.map = new HashMap();
		this.restoreList = new ArrayList();
	}

	public CategoryInfo addCtg(long categoryID){
		CategoryInfo ctg = (CategoryInfo)map.get(new Long(categoryID));
		if(ctg == null){
			ctg = new CategoryInfo(categoryID);
			map.put(new Long(categoryID),ctg);
		}
		return ctg;
	}
	
	public void setCtgPositn(long categoryID, int positn){
		CategoryInfo ctg = addCtg(categoryID);
		ctg.setCtgPositn(positn);
	}
	
	public void setCtgRemove(long categoryID){
		CategoryInfo ctg = addCtg(categoryID);
		ctg.setHidden(true);
	}
	
	public Collection getCtgCollection(){
		return this.map.values();
	}
	
	public Collection getRestoreCollection(){
		return this.restoreList;
	}
	
	public void setCtgRestore(long categoryID){
		CategoryInfo ctg = new CategoryInfo(categoryID);
		ctg.setHidden(false);
		restoreList.add(ctg);
	}
	

	
	
}
