/*
 * Created on Jan 27, 2005
 */
package edu.cornell.csuglab.cms.util.category;

/**
 * @author yc263
 *
 * Interface for holders for specific types of info that can be stored in
 * categories: see classes Ctg{Cur,New}ContentInfo
 */
public interface CtgContentInfo
{
}
