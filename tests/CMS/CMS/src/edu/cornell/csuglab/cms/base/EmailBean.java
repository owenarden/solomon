/*
 * Created on Mar 31, 2004
 */

package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.Collection;

import javax.ejb.EntityBean;
import javax.ejb.FinderException;

/**
 * @ejb.bean name="Email"
 *	jndi-name="EmailBean"
 *	type="BMP" 
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.EmailDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.EmailDAOImpl"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 **/
public abstract class EmailBean implements EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
    private long emailID, courseID;
    private String subject, message, sender;
    private int recipient;
    private Timestamp dateSent;
    
    public static final int STAFF = 1, STUDENTS = 2, ALL = 3, CUSTOM = 4;
	
    /**
     * @return Returns the emailID.
     * @ejb.interface-method view-type="local"
     * @ejb.persistence
     * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
     */
    public long getEmailID() {
        return emailID;
    }
    
    /**
     * @param emailID The emailID to set.
     * @ejb.interface-method view-type="local"
     */
    public void setEmailID(long emailID) {
        this.emailID = emailID;
    }

    /**
     * @return Returns the courseID.
     * @ejb.interface-method view-type="local"
     * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
     */
    public long getCourseID() {
        return courseID;
    }
    
    /**
     * @param courseID The courseID to set.
     * @ejb.interface-method view-type="local"
     */
    public void setCourseID(long courseID) {
        this.courseID = courseID;
    }
    
    /**
     * @return Returns the dateSent.
     * @ejb.interface-method view-type="local"
     * @ejb.persistence     
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
     */
    public Timestamp getDateSent() {
        return dateSent;
    }
    
    /**
     * @param dateSent The dateSent to set.
     * @ejb.interface-method view-type="local"
     */
    public void setDateSent(Timestamp dateSent) {
        this.dateSent = dateSent;
    }

    /**
     * @return Returns the message.
     * @ejb.interface-method view-type="local"
     * @ejb.persistence     
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
     */
    public String getMessage() {
        return message;
    }
    
    /**
     * @param message The message to set.
     * @ejb.interface-method view-type="local"
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    /**
     * @return Returns the recipient.
     * @ejb.interface-method view-type="local"
     * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
    public int getRecipient() {
        return recipient;
    }
    
    /**
     * @param recipient The recipient to set.
     * @ejb.interface-method view-type="local"
     */
    public void setRecipient(int recipient) {
        this.recipient = recipient;
    }
    
    /**
     * @return Returns the sender.
     * @ejb.interface-method view-type="local"
     * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
     */
    public String getSender() {
        return sender;
    }
    
    /**
     * @param sender The sender to set.
     * @ejb.interface-method view-type="local"
     */
    public void setSender(String sender) {
        this.sender = sender;
    }
    
    /**
     * @return Returns the subject.
     * @ejb.interface-method view-type="local"
     * @ejb.persistence
 	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
    */
    public String getSubject() {
        return subject;
    }
    
    /**
     * @param subject The subject to set.
     * @ejb.interface-method view-type="local"
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }
    
	/**
	 * Checks to see if the given primary key can be found in the database
	 * 
	 * @param pk
	 *            The primary key being searched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public EmailPK ejbFindByPrimaryKey(EmailPK key) throws FinderException {
		return null;
	}

	/**
	 * Finds all the emails sent in a given course ordered by
	 * descending date
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseID(long courseID) throws FinderException {
		return null;
	}
	

	
	// TODO: will need methods to find all comments based on assignment and
	// 		 Collection of groups for admin side
	/**
	 * @ejb.interface-method view-type="local"
	 **/
	public EmailData getEmailData() {
		return new EmailData(getEmailID(), getCourseID(),
				getDateSent(), getMessage(), getRecipient(),
				getSender(), getSubject());
	}

	/**
	 * @param comment
	 * @param netID
	 * @param assignmentID
	 * @param groupID
	 * @param gradeID
	 * @param requestID
	 * @return
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local"
	 */
	public EmailPK ejbCreate(long courseID, String sender, String subject, String message, int recipient) throws javax.ejb.CreateException {
		setCourseID(courseID);
		setSender(sender);
		setSubject(subject);
		setMessage(message);
		setRecipient(recipient);
		setDateSent(new Timestamp(System.currentTimeMillis()));
		return null;
	}
}
