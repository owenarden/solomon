package edu.cornell.csuglab.cms.base.dao;

import java.sql.*;

public class DAOUtils {
	
	/*
	 * Gets a string from a result set, and sets it to the empty string if it is null.
	 * This method will hide inconsistencies between representation of empty string
	 * in Oracle and MSSQL if used to load string from the result set.
	 */
	public static String getResultSetString(ResultSet rs, String ss) throws SQLException {
		String s = rs.getString(ss);
		if (s == null) s = "";
		return s;
	}
}
