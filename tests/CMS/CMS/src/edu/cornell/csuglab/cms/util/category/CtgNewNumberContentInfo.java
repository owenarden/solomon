package edu.cornell.csuglab.cms.util.category;

import java.sql.Timestamp;
import java.util.ArrayList;


/**
 * @author evan
 * 11 / 4 / 05
 *
 * Holds info for one category content of type number
 */
public class CtgNewNumberContentInfo extends CtgNewContentInfo
{
	private Long number;
	
	public CtgNewNumberContentInfo(long rowID, long colID, Long number)
	{
		super(rowID, colID);
		this.number = number;
	}
	
	/*
	 * CtgNewContentInfo functions. A null return value signifies no data.
	 */
	
	public String getColType()
	{
		return CtgUtil.CTNT_NUMBER;
	}
	
	public Timestamp getDate()       //used by contents of type date
	{
		return null;
	}
	
	public String getText()          //used by contents of type text
	{
		return null;
	}
	
	public Long getNumber()          //used by contents of type number
	{
		return number;
	}
	
	public String getURLAddress()    //used by contents of type url
	{
		return null;
	}
	
	public String getURLLabel()      //used by contents of type url
	{
		return null;
	}
	
	public ArrayList getFileInfos()  //used by contents of type file
	{
		return null;
	}
	
	public ArrayList getFileLabels() //used by contents of type file
	{
		return null;
	}
}
