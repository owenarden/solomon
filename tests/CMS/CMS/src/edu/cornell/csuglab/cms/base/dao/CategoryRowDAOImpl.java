/*
 * Created on Nov 10, 2004
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.CategoryRowBean;
import edu.cornell.csuglab.cms.base.CategoryRowDAO;
import edu.cornell.csuglab.cms.base.CategoryRowPK;

/**
 * @author yc263
 * 
 */
public class CategoryRowDAOImpl extends DAOMaster implements CategoryRowDAO {

    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CategoryRowDAO#load(edu.cornell.csuglab.cms.base.CategoryRowPK,
     *      edu.cornell.csuglab.cms.base.CategoryRowBean)
     */
    public void load(CategoryRowPK pk, CategoryRowBean ejb) throws EJBException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = jdbcFactory.getConnection();
            String query = "SELECT * FROM tCategoryRow WHERE rowID = ?";
            ps = conn.prepareStatement(query);
            ps.setLong(1, pk.rowID);
            rs = ps.executeQuery();
            if (rs.next()) {
                ejb.setRowID(rs.getLong("rowID"));
                ejb.setCategoryID(rs.getLong("categoryID"));
                ejb.setHidden(rs.getBoolean("hidden"));
                ejb.setReleaseDate(rs.getTimestamp("ReleaseDate"));
            } else
                throw new EJBException("Error in CategoryRowDAOImpl.load");
            rs.close();
            ps.close();
            conn.close();
        } catch (Exception e) {
            try {
                e.printStackTrace();
                if (rs != null)
                    rs.close();
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (Exception f) {
                f.printStackTrace();
            }
            throw new EJBException("Row id " + pk.rowID
                    + "not found in tCategoryRow", e);
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CategoryRowDAO#store(edu.cornell.csuglab.cms.base.CategoryRowBean)
     */
    public void store(CategoryRowBean ejb) throws EJBException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = jdbcFactory.getConnection();
            String query = "UPDATE tCategoryRow "
                    + "SET categoryID = ?, hidden = ?, releaseDate = ? "
                    + "WHERE rowID = ? ";
            ps = conn.prepareStatement(query);
            ps.setLong(1, ejb.getCategoryID());
            ps.setBoolean(2, ejb.getHidden());
            ps.setTimestamp(3, ejb.getReleaseDate());
            ps.setLong(4, ejb.getRowID());
            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (Exception e) {
            try {
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (Exception f) {
            }
            throw new EJBException(e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CategoryRowDAO#remove(edu.cornell.csuglab.cms.base.CategoryRowPK)
     */
    public void remove(CategoryRowPK pk) throws RemoveException, EJBException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CategoryRowDAO#create(edu.cornell.csuglab.cms.base.CategoryRowBean)
     */
    public CategoryRowPK create(CategoryRowBean ejb) throws CreateException,
            EJBException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int count;
        CategoryRowPK result = null;
        try {
            conn = jdbcFactory.getConnection();
            String query1 = "INSERT INTO tCategoryRow "
                    + "(categoryID, hidden, releaseDate) VALUES " + "(?, ?, ?)";
            String query2 = "SELECT @@identity AS 'rowID' FROM tCategoryRow";
            ps = conn.prepareStatement(query1);
            ps.setLong(1, ejb.getCategoryID());
            ps.setBoolean(2, ejb.getHidden());
            ps.setTimestamp(3, ejb.getReleaseDate());
            count = ps.executeUpdate();
            if (count > 0) {
                rs = (conn.prepareStatement(query2)).executeQuery();
                if (rs.next()) {
                    result = new CategoryRowPK(rs.getLong("rowID"));
                    ejb.setRowID(rs.getLong("rowID"));
                } else
                    throw new CreateException("Error in creating new row");
            } else
                throw new CreateException("Error in creating new row");
            if (rs != null)
                rs.close();
            ps.close();
            conn.close();
        } catch (Exception e) {
            try {
                e.printStackTrace();
                if (rs != null)
                    rs.close();
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (Exception f) {
                f.printStackTrace();
            }
            throw new CreateException(e.getMessage());
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CategoryRowDAO#findByCourseID(long)
     */
    public Collection findByCourseID(long courseID) throws FinderException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try {
            conn = jdbcFactory.getConnection();
            String query = "SELECT ctgRow.rowID "
                    + "FROM tCategoryRow AS ctgRow, tCategori AS ctg "
                    + "WHERE ctgRow.categoryID = ctg.categoryID AND ctg.courseID = ? "
                    + "AND ctgRow.hidden = 0 AND ctg.hidden = 0 "
                    + "ORDER BY ctg.positn, ctgRow.categoryID ";
            ps = conn.prepareStatement(query);
            ps.setLong(1, courseID);
            rs = ps.executeQuery();
            while (rs.next()) {
                long rowID = rs.getLong(1);
                result.add(new CategoryRowPK(rowID));
            }
            rs.close();
            ps.close();
            conn.close();
        } catch (Exception e) {
            try {
                e.printStackTrace();
                if (rs != null)
                    rs.close();
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (Exception f) {
                f.printStackTrace();
            }
            throw new FinderException(e.getMessage());
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CategoryRowDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.CategoryRowPK)
     */
    public CategoryRowPK findByPrimaryKey(CategoryRowPK pk)
            throws FinderException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = jdbcFactory.getConnection();
            String query = "SELECT rowID FROM tCategoryRow WHERE rowID = ? ";
            ps = conn.prepareStatement(query);
            ps.setLong(1, pk.rowID);
            rs = ps.executeQuery();
            if (!rs.next())
                throw new FinderException(
                        "Couldn't find category row with id: " + pk.rowID
                                + " in tCategoryRow");
            rs.close();
            ps.close();
            conn.close();
        } catch (Exception e) {
            try {
                if (conn != null)
                    conn.close();
                if (ps != null)
                    ps.close();
                if (rs != null)
                    rs.close();
            } catch (Exception f) {
            }
            throw new FinderException(e.getMessage());
        }
        return pk;
    }

}
