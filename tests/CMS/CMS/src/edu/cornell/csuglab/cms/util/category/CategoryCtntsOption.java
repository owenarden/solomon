/*
 * Created on Feb 1, 2005
 */
package edu.cornell.csuglab.cms.util.category;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author yc263, evan
 *
 * Holds records of the rows and specific contents in a category; these are the data 
 * that can be added, removed and edited. A CategoryCtntsOption represents one HTML 
 * page's worth of editing (it doesn't last between HTTP requests).
 * 
 * To add new rows of content, just call createNewRow() before using the returned ID 
 * in calls to the addContent()-type functions.
 */
public class CategoryCtntsOption {
	private long categoryID;
	
	/*
	 * Used to edit contents
	 * 
	 * We keep old (being edited) and new (being added) contents in separate lists because
	 * they get row IDs from different sources, and the sets of old and new row IDs might
	 * well overlap. The old row IDs come from the database, whereas the new row IDs come
	 * from the calling JSP, which wasn't given a list of IDs in use in the database.
	 */
	private ArrayList oldContentsList; //CtgCurContentInfo objects to update existing contents
	/*
	 * each ID in each of the following lists gets hidden/unhidden once per instance of it in the list
	 * (but note no ID should occur more than once, just due to system structure)
	 */
	private ArrayList updateRowList;   //row IDs (Longs), want to toggle the hidden bit of row
	private ArrayList updateFileList;  //file IDs (Longs), want to toggle the hidden bit of file
	/*
	 * edit the labels of existing files
	 */
	private HashMap newFileLabels;     //map Longs (file IDs) to Strings (new labels)
	
	/*
	 * Used to add contents
	 */
	private ArrayList newContentsList;	//CtgNewContentInfo objects to be created
	private HashMap newRowMap;          //CtgRowInfo objects for adding new rows, indexed by row ID (Long)
	
	public CategoryCtntsOption(long categoryID){
		this.categoryID = categoryID;
		this.oldContentsList = new ArrayList();
		this.newContentsList = new ArrayList(); 
		this.updateRowList = new ArrayList();
		this.updateFileList = new ArrayList();
		this.newRowMap = new HashMap();
		this.newFileLabels = new HashMap();
	}
	
	public long getCategoryID(){
		return this.categoryID;
	}
	
	public Collection getNewRows(){
		return this.newRowMap.values();
	}
	
	public Collection getNewContentsList(){
		return this.newContentsList;
	}
	
	public Collection getOldContentsList(){
		return this.oldContentsList;
	}
	
	public Collection getUpdateRowList(){
		return this.updateRowList;
	}
	
	/**
	 * @return A Collection of CurFileInfos mixed with NewFileInfos
	 * (note that these two don't have a common ancestor other than Object)
	 */
	public Collection getUpdateFileList(){
		return this.updateFileList;
	}
	
	/**
	 * @return A Map from file IDs (Longs) to Strings to use as their new labels
	 */
	public Map getEditFileLabelsMap()
	{
		return this.newFileLabels;
	}
	
	/**
	 * Toggle the hidden bit for the row
	 * @param rowID
	 */
	public void removeRow(long rowID){
		updateRowList.add(new Long(rowID));
	}
	
	/**
	 * Toggle the hidden bit for the row
	 * @param rowID
	 */
	public void restoreRow(long rowID){
		updateRowList.add(new Long(rowID));
	}
	
	/**
	 * Toggle the hidden bit for the file
	 * @param fileID
	 */
	public void removeFile(long fileID)
	{
		updateFileList.add(new Long(fileID));
	}
	
	/**
	 * Toggle the hidden bit for the file
	 * @param fileID
	 */
	public void restoreFile(long fileID){
		updateFileList.add(new Long(fileID));
	}
	
	/**
	 * Return the content (existing, to be edited) in our records with the given ID, if it
	 * exists in our records; else return null. This function may return any subtype of
	 * CtgCurContentInfo; cast the return value to make it useful.
	 *   Thus function doesn't look for new content that's being added because new
	 * content records use row and column IDs instead of content IDs.
	 * @param contentID
	 * @return A CtgCurContentInfo with the given ID, if it exists; else null
	 */
	public CtgCurContentInfo getCurInfoByContentID(long contentID)
	{
		Iterator i = oldContentsList.iterator();
		while(i.hasNext())
		{
			CtgCurContentInfo info = (CtgCurContentInfo)i.next();
			if(info.getContentID() == contentID)
				return info;
		}
		return null;
	}
	
	/**
	 * Return the content (to be added) in our records for the given row and column, if it's
	 * in our records; else return null. This function may return any subtype of
	 * CtgNewContentInfo; cast the return value to make it useful.
	 *   This function doesn't look for old content that's being edited because old
	 * content records use content IDs instead of row and column IDs.
	 * @param rowID
	 * @param colID
	 * @return A CtgNewContentInfo with the given row and column, if it exists; else null
	 */
	public CtgNewContentInfo findNewContentByRowCol(long rowID, long colID)
	{
		Iterator i = newContentsList.iterator();
		while(i.hasNext())
		{
			CtgNewContentInfo info = (CtgNewContentInfo)i.next();
			if(info.getRowID() == rowID && info.getColID() == colID)
				return info;
		}
		return null;
	}
	
	/**
	 * @param rowID The ID to use for the new row (only needs to be unique within this HTTP request)
	 * @return The row ID of the new row (to be used for adding contents)
	 */
	public long createNewRow(long rowID)
	{
		CtgRowInfo row = new CtgRowInfo(rowID);
		newRowMap.put(new Long(rowID), row);
		return rowID;
	}
	
	/**
	 * Update the date of an existing content
	 * @param contentID
	 * @param date
	 */ 
	public void changeCtntDate(long contentID, Timestamp date){
		CtgCurDateContentInfo info = (CtgCurDateContentInfo)getCurInfoByContentID(contentID);
		if(info == null) //we aren't already editing this content
		{
			info = new CtgCurDateContentInfo(contentID, date);
			oldContentsList.add(info);
		}
		else
		{
			info.setDate(date);
		}
	}
	
	/**
	 * Updates the text of an existing content
	 * @param contentID
	 * @param text
	 */
	public void changeCtntText(long contentID, String text){
		CtgCurTextContentInfo info = (CtgCurTextContentInfo)getCurInfoByContentID(contentID);
		if(info == null) //we aren't already editing this content
		{
			info = new CtgCurTextContentInfo(contentID, text);
			oldContentsList.add(info);
		}
		else
		{
			info.setText(text);
		}
	}
	
	/**
	 * Updates the integral value in an existing content
	 * @param contentID
	 * @param number
	 */
	public void changeCtntNumber(long contentID, Long number)
	{
		CtgCurNumberContentInfo info = (CtgCurNumberContentInfo)getCurInfoByContentID(contentID);
		if(info == null) //we aren't already editing this content
		{
			info = new CtgCurNumberContentInfo(contentID, number);
			oldContentsList.add(info);
		}
		else
		{
			info.setNumber(number);
		}
	}
	
	/**
	 * Add a file to an existing file content (that stores a list of files in a single cell)
	 * @param contentID The ID of the existing content bean
	 * @param fileNum The index of the targeted file in the list of new files for its cell
	 * @param fileInfo The new info for the file
	 */
	public void addCtntFile(long contentID, long fileNum, CtgFileInfo fileInfo)
	{
		System.out.println("CCO addCtntFile: ctnt " + contentID + ", filenum " + fileNum);
		CtgCurFileContentInfo info = (CtgCurFileContentInfo)getCurInfoByContentID(contentID);
		if(info == null) //we aren't already updating this content
		{
			info = new CtgCurFileContentInfo(contentID, fileInfo, "", (int)fileNum);
			oldContentsList.add(info);
		}
		else //the content is already in our update list
			info.addFile(fileInfo, (int)fileNum);
	}
	
	/**
	 * Add a file label to an existing file content (that stores a list of files in a single cell)
	 * @param contentID The ID of the existing content bean
	 * @param fileNum The index of the targeted file in the list of new files for its cell
	 * @param fileLabel The new displayed name for the file
	 */
	public void addCtntFileLabel(long contentID, long fileNum, String fileLabel)
	{
		CtgCurFileContentInfo info = (CtgCurFileContentInfo)getCurInfoByContentID(contentID);
		if(info == null) //we aren't already updating this content
		{
			info = new CtgCurFileContentInfo(contentID, null, fileLabel, (int)fileNum);
			oldContentsList.add(info);
		}
		else //the content is already in our update list
			info.addFileLabel(fileLabel, (int)fileNum);
	}
	
	/**
	 * Change info for an existing URL content
	 * @param contentID The ID of the existing content bean
	 * @param urlAddr The new address for the link
	 */
	public void changeCtntURL(long contentID, String urlAddr)
	{
		CtgCurURLContentInfo info = (CtgCurURLContentInfo)getCurInfoByContentID(contentID);
		if(info == null) //we aren't already updating this content
		{
			info = new CtgCurURLContentInfo(contentID, urlAddr, null);
			oldContentsList.add(info);
		}
		else //the content is already in our update list
		{
			info.setAddress(urlAddr);
		}
	}
	
	/**
	 * Change info for an existing URL content
	 * @param contentID The ID of the existing content bean
	 * @param urlLabel The new displayed name for the link
	 */
	public void changeCtntURLLabel(long contentID, String urlLabel)
	{
		CtgCurURLContentInfo info = (CtgCurURLContentInfo)getCurInfoByContentID(contentID);
		if(info == null) //we aren't already updating this content
		{
			info = new CtgCurURLContentInfo(contentID, null, urlLabel);
			oldContentsList.add(info);
		}
		else //the content is already in our update list
		{
			info.setLabel(urlLabel);
		}
	}
	
	/**
	 * Change info for an existing file (which must be in an existing content)
	 * @param fileID The ID of the existing ctg file bean
	 * @param fileLabel The new displayed name for the file
	 */
	public void changeCtntFileLabel(long fileID, String fileLabel)
	{
		newFileLabels.put(new Long(fileID), fileLabel);
	}
	
	/**
	 * Creates a new date content for a row that may or may not already exist in the db
	 * @param colID
	 * @param rowID
	 * @param date
	 */
	public void addNewCtntDate(long colID, long rowID, Timestamp date){
		newContentsList.add(new CtgNewDateContentInfo(rowID, colID, date));
	}
	
	/**
	 * Create a new text content for a row that may or may not already exist in the db
	 * @param colID
	 * @param rowID
	 * @param text
	 */
	public void addNewCtntText(long colID, long rowID, String text){
		newContentsList.add(new CtgNewTextContentInfo(rowID, colID, text));
	}
	
	/**
	 * Create a new number content for a row that may or may not already exist in the db
	 * @param colID
	 * @param rowID
	 * @param number
	 */
	public void addNewCtntNumber(long colID, long rowID, Long number)
	{
		newContentsList.add(new CtgNewNumberContentInfo(rowID, colID, number));
	}
	
	/**
	 * Add a URL address, but not a name, to the list of new contents
	 * @param colID
	 * @param rowID
	 * @param urlAddr The URL to be linked to
	 */
	public void addNewCtntURL(long colID, long rowID, String urlAddr)
	{
		CtgNewURLContentInfo content = (CtgNewURLContentInfo)findNewContentByRowCol(rowID, colID);
		if(content == null) //this row/col not in our records
		{
			content = new CtgNewURLContentInfo(rowID, colID, urlAddr, null);
			newContentsList.add(content);
		}
		else //we already have a name for this URL; set the address
		{
			content.setAddress(urlAddr);
		}
	}
	
	/**
	 * Add a URL name, but not an address, to the list of new contents
	 * @param colID
	 * @param rowID
	 * @param urlLabel The name to be displayed for the link
	 */
	public void addNewCtntURLLabel(long colID, long rowID, String urlLabel)
	{
		CtgNewURLContentInfo content = (CtgNewURLContentInfo)findNewContentByRowCol(rowID, colID);
		if(content == null) //this row/col not in our records
		{
			content = new CtgNewURLContentInfo(rowID, colID, null, urlLabel);
			newContentsList.add(content);
		}
		else //we already have an address for this URL; set the name
		{
			content.setLabel(urlLabel);
		}
	}
	
	/**
	 * Set the file to be linked to at the given location in a category
	 * @param colID
	 * @param rowID
	 * @param fileNum
	 * @param fileInfo
	 */
	public void addNewCtntFile(long colID, long rowID, long fileNum, CtgFileInfo fileInfo)
	{
		CtgNewFileContentInfo info = (CtgNewFileContentInfo)findNewContentByRowCol(rowID, colID);
		if(info == null) //this row/col isn't in our records; add it first
		{
			info = new CtgNewFileContentInfo(rowID, colID);
			this.newContentsList.add(info);
		}
		info.addFile(fileInfo, (int)fileNum);
	}
	
	/**
	 * Set the label to be displayed on the Web for the file at the given location in a category
	 * @param colID
	 * @param rowID
	 * @param fileNum
	 * @param label
	 */
	public void addNewCtntFileLabel(long colID, long rowID, long fileNum, String label)
	{
		CtgNewFileContentInfo info = (CtgNewFileContentInfo)findNewContentByRowCol(rowID, colID);
		if(info == null) //this row/col isn't in our records; add it first
		{
			info = new CtgNewFileContentInfo(rowID, colID);
			this.newContentsList.add(info);
		}
		info.addFileLabel(label, (int)fileNum);
	}
	
	/**
	 * Remove any new or current file entries that have null or empty label and no actual file
	 * from our file lists (in order not to clutter the database with empty content records)
	 */
	public void removeEmptyFiles()
	{
		removeEmptyFilesFromOneList(this.oldContentsList.iterator());
		removeEmptyFilesFromOneList(this.newContentsList.iterator());
	}
	
	/**
	 * Auxiliary to removeEmptyFiles()
	 * @param i
	 */
	private void removeEmptyFilesFromOneList(Iterator i)
	{
		while(i.hasNext())
		{
			CtgContentInfo info = (CtgContentInfo)i.next();
			if(info instanceof CtgFileContentInfo)
			{
				CtgFileContentInfo finfo = (CtgFileContentInfo)info;
				for(int j = 0; j < finfo.getNumFiles(); j++)
				{
					CtgFileInfo file = finfo.getFileInfo(j);
					String label = finfo.getFileLabel(j);
					if((file == null || !file.fileExists()) && (label == null || label.length() == 0))
					{
						finfo.removeByIndex(j--);
					}
				}
			}
		}
	}
}
