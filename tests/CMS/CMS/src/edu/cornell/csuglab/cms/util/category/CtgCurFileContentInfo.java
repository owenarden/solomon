/*
 * Created on Sep 1, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.util.category;

import java.util.ArrayList;
import java.util.List;

import edu.cornell.csuglab.cms.base.CategoryFileData;

/**
 * @author evan
 *
 * Holds info representing an existing info of type file, to be edited
 */
public class CtgCurFileContentInfo extends CtgCurContentInfo implements CtgFileContentInfo
{
	private ArrayList fileInfos; //holds objects of type CtgFileInfo, in order as they are to appear
	private ArrayList fileLabels; //holds objects of type String, in order as they are to appear
	
	/**
	 * Constructor to add an arbitrary number of initial files, assuming their indices
	 * within this cell are their indices within the lists passed as arguments
	 * @param contentID
	 * @param curFiles An ordered list of all the CtgFileInfos currently in this cell
	 * @param curLabels An ordered list of all the labels (Strings) currently in this cell
	 */
	public CtgCurFileContentInfo(long contentID, List curFiles, List curLabels)
	{
		super(contentID);
		this.fileInfos = new ArrayList();
		this.fileLabels = new ArrayList();
		this.fileInfos.addAll(curFiles);
		this.fileLabels.addAll(curLabels);
	}
	
	/**
	 * Convenience constructor to add just one file
	 * @param contentID
	 * @param fileInfo A file info to add to this cell
	 * @param fileLabel The label for fileInfo
	 * @param fileIndex The index of this file within its cell
	 */
	public CtgCurFileContentInfo(long contentID, CtgFileInfo fileInfo, String fileLabel, int fileIndex)
	{
		super(contentID);
		this.fileInfos = new ArrayList();
		this.fileLabels = new ArrayList();
		addFile(fileInfo, fileIndex);
		addFileLabel(fileLabel, fileIndex);
	}
	
	public long getNumFiles()
	{
		//assume fileInfos and fileLabels have the same size at this point; else doesn't make sense to ask for size
		return fileInfos.size();
	}
	
	/**
	 * Return the file info in our records corresponding to the given index, or null if there isn't
	 * one at index. Note that a null file may also occur at an index where there is a file label.
	 * @param index
	 * @return
	 */
	public CtgFileInfo getFileInfo(int index)
	{
		if(index >= fileInfos.size()) return null;
		return (CtgFileInfo)fileInfos.get(index);
	}
	
	/**
	 * Return an ordered list of file infos in our records
	 * @return ArrayList of CtgFileInfos
	 */
	public ArrayList getFileInfoList()
	{
		return fileInfos;
	}
	
	/**
	 * Return the file label in our records corresponding to the given index, or null if there isn't
	 * one at index. So null should be interpreted as the lack of a label rather than an empty label.
	 * @param index
	 * @return String
	 */
	public String getFileLabel(int index)
	{
		if(index >= fileLabels.size()) return null;
		return (String)fileLabels.get(index);
	}
	
	/**
	 * Return an ordered list of displayed file labels in our records
	 * @return ArrayList of Strings
	 */
	public ArrayList getFileLabelList()
	{
		return fileLabels;
	}
	
	/**
	 * Should not return null.
	 * @return A List of FilePlusLabel objects, each of which holds a file info and a String.
	 * The Infos give file path info, and the Strings are file labels to be displayed in the browser.
	 */
	public List getFileInfoLabelList()
	{
		/*
		 * use a list so we can be sure the order they'll come out of it in is the order we put them in in,
		 * and so the same order in which they'll appear in the database and on the relevant JSPs
		 */
		System.out.println("list lengths: " + fileInfos.size() + ", " + fileLabels.size());
		System.out.println("infos:");
		for(int i = 0; i < fileInfos.size(); i++)
			if(fileInfos.get(i) == null) System.out.print("null,");
			else System.out.print("(" + fileInfos.get(i).getClass().getName() + ")" + fileInfos.get(i) + ",");
		System.out.println("\nlabels:");
		for(int i = 0; i < fileLabels.size(); i++)
			if(fileLabels.get(i) == null) System.out.print("null,");
			else System.out.print("(" + fileLabels.get(i).getClass().getName() + ")" + fileLabels.get(i) + ",");
		System.out.println();
		ArrayList list = new ArrayList();
		for(int i = 0; i < fileLabels.size(); i++)
			list.add(new FilePlusLabel((CtgFileInfo)fileInfos.get(i), (String)fileLabels.get(i)));
		return list;
	}
	
	/**
	 * Add a file info at the given index in the list, extending the list if necessary
	 * @param file
	 * @param index
	 */
	public void addFile(CtgFileInfo file, int index)
	{
		while(index > fileInfos.size()) fileInfos.add(null); //pad the list with empty data
		if(index == fileInfos.size()) fileInfos.add(file);
		else
		{
			fileInfos.remove(index);
			fileInfos.add(index, file);
		}
	}
	
	/**
	 * Add a file label at the given index in the list, extending the list if necessary
	 * @param fileLabel
	 * @param index
	 */
	public void addFileLabel(String fileLabel, int index)
	{
		while(index > fileLabels.size()) fileLabels.add(null); //pad the list with empty data
		if(index == fileLabels.size()) fileLabels.add(fileLabel);
		else
		{
			fileLabels.remove(index);
			fileLabels.add(index, fileLabel);
		}
	}
	
	/**
	 * Remove the file and label at the given index in our lists
	 * @param index
	 */
	public void removeByIndex(int index)
	{
		fileInfos.remove(index);
		fileLabels.remove(index);
	}
	
	/**
	 * Used to remove files that haven't changed from their database values from our list
	 * prior to our list being used to create or modify file records
	 * @param data
	 */
	public void removeFileIfInList(CategoryFileData data)
	{
		for(int i = 0; i < fileInfos.size(); i++)
		{
			CtgFileInfo file = (CtgFileInfo)fileInfos.get(i);
			String label = (String)fileLabels.get(i);
			boolean sameFile = false;
			if(data.getContentID() == this.contentID 
				//note label should never be null, since it's added from a JSP form field
				&& ((data.getLinkName() == null && (/*label == null ||*/ label.equals(""))) || data.getLinkName().equals(label)))
				if(file == null) //maybe label, but no file
				{
					if(data.getFileName() == null && data.getPath() == null)
						sameFile = true;
				}
				else //file exists
				{
					if(file.getFileName().equals(data.getFileName()) && file.getFilePath().equals(data.getPath()))
						sameFile = true;
				}
			if(sameFile)
			{
				fileInfos.remove(i);
				fileLabels.remove(i);
			}
		}
	}
	
	public String toString()
	{
		String s = "CurFileContentInfo\n{";
		for(int i = 0; i < fileInfos.size(); i++)
			s += "\n\t" + fileInfos.get(i) + ", label " + fileLabels.get(i);
		return s + "\n}";
	}
}
