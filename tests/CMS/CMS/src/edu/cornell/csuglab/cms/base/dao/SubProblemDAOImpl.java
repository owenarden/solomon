/*
 * Created on Jan 31, 2005
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.SubProblemBean;
import edu.cornell.csuglab.cms.base.SubProblemDAO;
import edu.cornell.csuglab.cms.base.SubProblemPK;

/**
 * @author Jon
 *
 */
public class SubProblemDAOImpl extends DAOMaster implements SubProblemDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubProblemDAO#load(edu.cornell.csuglab.cms.base.SubProblemPK, edu.cornell.csuglab.cms.base.SubProblemBean)
	 */
	public void load(SubProblemPK pk, SubProblemBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tSubProblems where SubProblemID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getSubProblemID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setSubProblemID(rs.getLong("SubProblemID"));
				ejb.setAssignmentID(rs.getLong("AssignmentID"));
				ejb.setMaxScore(rs.getFloat("MaxScore"));
				ejb.setSubProblemName(rs.getString("SubProblemName"));
				ejb.setHidden(rs.getBoolean("Hidden"));
				ejb.setType(rs.getInt("Type"));
				ejb.setOrder(rs.getInt("ProblemOrder"));
				ejb.setAnswer(rs.getInt("ChoiceID"));
			} else {
				throw new EJBException("Result set empty");
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException("Row id " + pk.getSubProblemID()
					+ " failed to load in tSubProblems.", e);
		}
		

	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubProblemDAO#store(edu.cornell.csuglab.cms.base.SubProblemBean)
	 */
	public void store(SubProblemBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tSubProblems set AssignmentID = ?, "
					+ "SubProblemName = ?, MaxScore = ?, Hidden = ?, "
					+ "Type = ?, ProblemOrder = ?, ChoiceID = ? "
					+ "where SubProblemID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, ejb.getAssignmentID());
			if(ejb.getSubProblemName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getSubProblemName());
			ps.setFloat(3, ejb.getMaxScore());
			ps.setBoolean(4, ejb.getHidden());
			ps.setInt(5, ejb.getType());
			ps.setInt(6, ejb.getOrder());
			ps.setInt(7, ejb.getAnswer());
			ps.setLong(8, ejb.getSubProblemID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {
				f.printStackTrace();
			}
			throw new EJBException("Row id " + ejb.getSubProblemID() + ", failed to store properly", e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubProblemDAO#remove(edu.cornell.csuglab.cms.base.SubProblemPK)
	 */
	public void remove(SubProblemPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubProblemDAO#create(edu.cornell.csuglab.cms.base.SubProblemBean)
	 */
	public SubProblemPK create(SubProblemBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		SubProblemPK pk = null;
		try {
			conn = jdbcFactory.getConnection();
			String updateQuery = "insert into tSubProblems "
					+ "(AssignmentID, SubProblemName," +
					"MaxScore, Type, ProblemOrder, ChoiceID) values (?, ?, ?, ?, ?,  ?)";
			String findQuery = "select @@identity as SubProblemID from tSubProblems";
			ps = conn.prepareStatement(updateQuery);
			ps.setLong(1, ejb.getAssignmentID());
			if(ejb.getSubProblemName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getSubProblemName());
			ps.setFloat(3, ejb.getMaxScore());
			ps.setInt(4, ejb.getType());
			ps.setInt(5, ejb.getOrder());
			ps.setInt(6, ejb.getAnswer());
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findQuery).executeQuery();
			if (count > 0 && rs.next()) {
				long subProblemID = rs.getLong("SubProblemID"); 
				pk = new SubProblemPK(subProblemID);
				ejb.setSubProblemID(subProblemID);
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				e.printStackTrace();
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {
				f.printStackTrace();
			}
			throw new CreateException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubProblemDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.SubProblemPK)
	 */
	public SubProblemPK findByPrimaryKey(SubProblemPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select SubProblemID from tSubProblems where SubProblemID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getSubProblemID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find subproblem with SubProblemID = " + pk.getSubProblemID());
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			try {
				throw ((FinderException) e); //XXX wtf?
			}
			catch (ClassCastException x) {
				throw new FinderException("Caught Exception: " + e.getMessage());
			}
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubProblemDAO#findByAssignmentID(long)
	 */
	private Collection findByAssignmentID(long[] assignmentIDs, boolean hidden) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (assignmentIDs.length == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select SubProblemID from tSubProblems " +
					"where (";
			for (int i=0; i < assignmentIDs.length - 1; i++) {
			    query += "AssignmentID = ? or ";
			}
			query += "AssignmentID = ?) and Hidden = ? order by ProblemOrder ASC";
			ps = conn.prepareStatement(query);
			for (int i=0; i < assignmentIDs.length; i++) {
			    ps.setLong(1 + i, assignmentIDs[i]);
			}
			ps.setBoolean(assignmentIDs.length + 1, hidden);
			rs = ps.executeQuery();
			while (rs.next()) {
				long subProblemID = rs.getLong("SubProblemID");
				result.add(new SubProblemPK(subProblemID));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}

	/*
	 *  (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubProblemDAO#findByAssignmentID(long)
	 */
	public Collection findByAssignmentID(long assignmentID) throws FinderException {
		return findByAssignmentID(new long[] {assignmentID}, false);
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubProblemDAO#findHiddenByAssignmentID(long)
	 */
	public Collection findHiddenByAssignmentID(long assignmentID) throws FinderException {
		return findByAssignmentID(new long[] {assignmentID}, true);
	}
	
	public Collection findByAssignmentIDs(long[] assignmentIDs) throws FinderException {
	    return findByAssignmentID(assignmentIDs, false);
	}
	
}
