/*
 * Created on Mar 15, 2005
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.GroupAssignedToBean;
import edu.cornell.csuglab.cms.base.GroupAssignedToDAO;
import edu.cornell.csuglab.cms.base.GroupAssignedToPK;

/**
 * @author Jon
 *
 */
public class GroupAssignedToDAOImpl extends DAOMaster implements GroupAssignedToDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupAssignedToDAO#load(edu.cornell.csuglab.cms.base.GroupAssignedToPK, edu.cornell.csuglab.cms.base.GroupAssignedToBean)
	 */
	public void load(GroupAssignedToPK pk, GroupAssignedToBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tGroupAssignedTo where GroupID = ? and SubProblemID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getGroupID());
			ps.setLong(2, pk.getSubProblemID());
			rs = ps.executeQuery();
			ejb.setGroupID(pk.getGroupID());
			ejb.setSubProblemID(pk.getSubProblemID());
			if (rs.next()) {
				ejb.setNetID(rs.getString("NetID"));
			} else throw new EJBException("Failed to load GroupAssignedToBean");
			conn.close();
			rs.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new EJBException(e);
		}	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupAssignedToDAO#store(edu.cornell.csuglab.cms.base.GroupAssignedToBean)
	 */
	public void store(GroupAssignedToBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tGroupAssignedTo set NetID = ? "
					+ "where GroupID = ? and SubProblemID = ?";
			ps = conn.prepareStatement(query);
			if (ejb.getNetID() == null) {
				ps.setNull(1, Types.VARCHAR);
			} else {
				ps.setString(1, ejb.getNetID());
			}
			ps.setLong(2, ejb.getGroupID());
			ps.setLong(3, ejb.getSubProblemID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			} catch (Exception f) {}
			throw new EJBException(e);
		}	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupAssignedToDAO#remove(edu.cornell.csuglab.cms.base.GroupAssignedToPK)
	 */
	public void remove(GroupAssignedToPK pk) throws RemoveException, EJBException {
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupAssignedToDAO#create(edu.cornell.csuglab.cms.base.GroupAssignedToBean)
	 */
	public GroupAssignedToPK create(GroupAssignedToBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		GroupAssignedToPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String insert = "insert into tGroupAssignedTo "
				+ "(GroupID, SubProblemID, NetID) "
				+ "values (?,?,?)";
			ps = conn.prepareStatement(insert);
			ps.setLong(1, ejb.getGroupID());
			ps.setLong(2, ejb.getSubProblemID());
			if(ejb.getNetID() == null) ps.setNull(3, java.sql.Types.VARCHAR);
			else ps.setString(3, ejb.getNetID());
			ps.executeUpdate();
			result = new GroupAssignedToPK(ejb.getGroupID(), ejb.getSubProblemID());
			conn.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			} catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return result;	
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupAssignedToDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.GroupAssignedToPK)
	 */
	public GroupAssignedToPK findByPrimaryKey(GroupAssignedToPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select GroupID, SubProblemID from tGroupAssignedTo where GroupID = ? and SubProblemID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getGroupID());
			ps.setLong(2, pk.getSubProblemID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find a GroupAssignedTo matching primary key");
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupAssignedToDAO#findByNetIDAssignmentID(java.lang.String, long)
	 */
	public Collection findByNetIDAssignmentID(String netid, long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select a.GroupID, a.SubProblemID from tGroupAssignedTo a INNER JOIN " +
					"tGroups g ON a.GroupID = g.GroupID where a.NetID = ? and g.AssignmentID = ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, netid);
			ps.setLong(2, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				long groupid = rs.getLong("GroupID");
				long subproblemid = rs.getLong("SubProblemID");
				result.add(new GroupAssignedToPK(groupid, subproblemid));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;	
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupAssignedToDAO#findByAssignmentID(long)
	 */
	public Collection findByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select a.GroupID, a.SubProblemID from tGroupAssignedTo a inner join tGroups g on a.GroupID = g.GroupID where g.AssignmentID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				long groupid = rs.getLong("GroupID");
				long subproblemid = rs.getLong("SubProblemID");
				result.add(new GroupAssignedToPK(groupid, subproblemid));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;	
	}

	public Collection findByAssignIDSubProbID(long assignID, long subProbID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select a.GroupID, a.SubProblemID from tGroupAssignedTo a inner join tGroups g on a.GroupID = g.GroupID where g.AssignmentID = ? and a.SubProblemID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignID);
			ps.setLong(2, subProbID);
			rs = ps.executeQuery();
			while (rs.next()) {
				long groupid = rs.getLong("GroupID");
				long subproblemid = rs.getLong("SubProblemID");
				result.add(new GroupAssignedToPK(groupid, subproblemid));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;	
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupAssignedToDAO#findByGroupID(long)
	 */
	public Collection findByGroupID(long groupID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select GroupID, SubProblemID from tGroupAssignedTo where GroupID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, groupID);
			rs = ps.executeQuery();
			while (rs.next()) {
				long groupid = rs.getLong("GroupID");
				long subproblemid = rs.getLong("SubProblemID");
				result.add(new GroupAssignedToPK(groupid, subproblemid));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;	
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupAssignedToDAO#findBySubProblemID(long)
	 */
	public Collection findBySubProblemID(long subProblemID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select GroupID, SubProblemID from tGroupAssignedTo where SubProblemID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, subProblemID);
			rs = ps.executeQuery();
			while (rs.next()) {
				long groupid = rs.getLong("GroupID");
				long subproblemid = rs.getLong("SubProblemID");
				result.add(new GroupAssignedToPK(groupid, subproblemid));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;	
	}
	
	public Collection findByGroupIDsSubID(Collection groups, long subProblemID, Collection nonExisting) throws FinderException{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			if (groups.size() == 0) return result;
			conn = jdbcFactory.getConnection();
			String query = "select GroupID,SubProblemID from tGroupAssignedTo where " +
					"SubProblemID = ? and (";
			for (int i=0; i < groups.size() - 1; i++) {
				query += "GroupID = ? or ";
			}
			query += "GroupID = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, subProblemID);
			Iterator i = groups.iterator();
			int count = 0;
			while (i.hasNext()) {
				ps.setLong(2+(count++), ((Long) i.next()).longValue());
			}
			rs = ps.executeQuery();
			Collection c = new ArrayList(groups);
			while (rs.next()) {
				long groupID = rs.getLong("GroupID");
				long subID = rs.getLong("SubProblemID");
				c.remove(new Long(groupID));
				result.add(new GroupAssignedToPK(groupID, subID));
			}
			nonExisting.addAll(c);
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	
}
