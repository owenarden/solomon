/*
 * Created on Oct 7, 2004
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.author.Principal;
import edu.cornell.csuglab.cms.base.CategoryContentsBean;
import edu.cornell.csuglab.cms.base.CategoryContentsDAO;
import edu.cornell.csuglab.cms.base.CategoryContentsPK;

/**
 * @author yc263
 *
 */
public class CategoryContentsDAOImpl extends DAOMaster implements CategoryContentsDAO{

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.base.CategoryContentsDAO#load(edu.cornell.csuglab.cms.base.CategoryContentsPK, edu.cornell.csuglab.cms.base.CategoryContentsBean)
	 */
	public void load(CategoryContentsPK pk, CategoryContentsBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = jdbcFactory.getConnection();
			String query = "select * from tCtgContents where ContentID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1,pk.contentID);
			rs = ps.executeQuery();
			if(rs.next()){
				ejb.setContentID(rs.getLong("ContentID"));
				ejb.setColumnID(rs.getLong("ColID"));
				ejb.setColumnType(rs.getString("ColType"));
				ejb.setRowID(rs.getLong("RowID"));
				ejb.setDate(rs.getTimestamp("Dte"));
				try {
					ejb.setText(rs.getString("Text"));
				} catch (SQLException e) {
					ejb.setText("");
				}
				ejb.setLinkName(rs.getString("LinkName"));
				long number = rs.getLong("Number"); 
				ejb.setNumber(rs.wasNull() ? null : new Long(number));
			}
			else throw new EJBException("Error in CategoryContentsDAOImpl.load()");
			rs.close();
			ps.close();
			conn.close();
		}
		catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new EJBException("Row id " + pk.contentID + "not found in tCtgContents", e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.base.CategoryContentsDAO#store(edu.cornell.csuglab.cms.base.CategoryContentsBean)
	 */
	public void store(CategoryContentsBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try
		{
			conn = jdbcFactory.getConnection();
			String query = "update tCtgContents set ColID = ?, "+
				"ColType = ?, RowID = ?, Number = ? ";
			if(ejb.getDate() != null) query += ", Dte = ? ";
			if(ejb.getText() != null) query += ", Text = ? ";
			if(ejb.getLinkName() != null) query += ", LinkName = ? ";
			query += "where ContentID = ? ";
			ps = conn.prepareStatement(query);
			ps.setLong(1, ejb.getColumnID());
			if(ejb.getColumnType() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getColumnType());
			ps.setLong(3, ejb.getRowID());
			if(ejb.getNumber() == null) ps.setNull(4, java.sql.Types.BIGINT);
			else ps.setLong(4, ejb.getNumber().longValue());
			int paramCount = 5;
			if(ejb.getDate() != null) ps.setTimestamp(paramCount++, ejb.getDate());
			if(ejb.getText() != null && ejb.getText() != "") ps.setString(paramCount++, ejb.getText());
			if(ejb.getLinkName() != null && ejb.getLinkName() != "") ps.setString(paramCount++, ejb.getLinkName());
			ps.setLong(paramCount, ejb.getContentID());
			ps.executeUpdate();
			ps.close();
			conn.close();
		}
		catch(Exception e)
		{
			try
			{
				e.printStackTrace();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}
			catch(Exception f){}
			throw new EJBException("Content id " + ejb.getContentID() + " not found in tCtgContents", e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.base.CategoryContentsDAO#remove(edu.cornell.csuglab.cms.base.CategoryContentsPK)
	 */
	public void remove(CategoryContentsPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.base.CategoryContentsDAO#create(edu.cornell.csuglab.cms.base.CategoryContentsBean)
	 */
	public CategoryContentsPK create(CategoryContentsBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		CategoryContentsPK pk = null;
		try{
			conn = jdbcFactory.getConnection();
			String query = "INSERT into tCtgContents "+
							"(ColID, ColType, RowID, Number, Dte, Text, LinkName) VALUES "+
							"(?, ?, ?, ?, ?, ?, ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, ejb.getColumnID() );
			if(ejb.getColumnType() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getColumnType());
			ps.setLong(3, ejb.getRowID());
			if(ejb.getNumber() == null) ps.setNull(4, java.sql.Types.BIGINT);
			else ps.setLong(4, ejb.getNumber().longValue());
			ps.setTimestamp(5, ejb.getDate());
			if(ejb.getText() == null) ps.setNull(6, java.sql.Types.VARCHAR);
			else ps.setString(6, ejb.getText());
			if(ejb.getLinkName() == null) ps.setNull(7, java.sql.Types.VARCHAR);
			else ps.setString(7, ejb.getLinkName());
			int count = ps.executeUpdate();
			if(count >0 ){
				query = "SELECT @@identity AS 'contentID' FROM tCtgContents";
				ps = conn.prepareStatement(query);
				rs = ps.executeQuery();
				if(rs.next()){
					pk = new CategoryContentsPK(rs.getLong("ContentID"));
					ejb.setContentID(pk.getContentID());
				}
				else throw new CreateException("Error in creating new ctg content");
			}
			else throw new CreateException("Error in creating new ctg content");
			if(rs != null) rs.close();
			ps.close();
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
			try{
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null )conn.close();
			}
			catch(Exception f){f.printStackTrace();}
			throw new CreateException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.base.CategoryContentsDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.CategoryContentsPK)
	 */
	public CategoryContentsPK findByPrimaryKey(CategoryContentsPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = jdbcFactory.getConnection();
			String query = "SELECT ContentID FROM tCtgContents "+
					"WHERE ContentID = ? ";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.contentID);
			rs = ps.executeQuery();
			if(!rs.next())
				throw new FinderException("Couldn't find content with id: "
						+pk.contentID+" in tCtgContents");
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
				
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

	public Collection queryByCategoryID(String query, long categoryID, long sortByColID) throws FinderException{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection coll = new ArrayList();
		try{
			conn = jdbcFactory.getConnection();
			ps = conn.prepareStatement(query);
			ps.setLong(1, categoryID);
			ps.setLong(2, categoryID);
			ps.setLong(3, sortByColID);
			rs = ps.executeQuery();
			while(rs.next()){
				long contentID = rs.getLong(1);
				coll.add(new CategoryContentsPK(contentID));
			}
			rs.close();
			ps.close();
			conn.close();
		}catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}
			catch(Exception f){
				f.printStackTrace();
			}
			throw new FinderException("Exception throw "+e.getMessage());
		}
		return coll;
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.base.CategoryContentsDAO#findByCategoryID(long)
	 */
	public Collection findByCategoryID(long categoryID, long sortByColID, boolean ascending, boolean visibleCol, boolean visibleRow) throws FinderException {
		String query = "SELECT C1.ContentID"
						+ " FROM tCtgContents C1"
						+ " INNER JOIN tCategoryRow Row"
						+ "  ON" + (visibleRow ? " Row.Hidden = 0 AND" : "") + " Row.CategoryID = ?"
						+ " INNER JOIN tCategoryCol Col"
						+ "  ON" + (visibleCol ? " Col.Removed = 0 AND Col.Hidden = 0 AND" : "") + " Col.CategoryID = ?"
						+ "   AND C1.ColID = Col.ColID AND C1.RowID = Row.RowID"
						+ " LEFT OUTER JOIN tCtgContents C2"
						+ "  ON C2.ColID = ? AND C1.RowID = C2.RowID"
						+ (ascending ? " ORDER BY C2.Text, C2.Dte, C2.Number, Row.RowID, Col.Positn"
										 : " ORDER BY C2.Text DESC, C2.Dte DESC, C2.Number DESC, Row.RowID, Col.Positn");
		return this.queryByCategoryID(query, categoryID, sortByColID);
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryContentsDAO#findByCourseID(long)
	 */
	public Collection findByCourseID(long courseID, Principal p) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection coll = new ArrayList();
		try{
			conn = jdbcFactory.getConnection();
			String query =  "SELECT C1.ContentID "+
							"FROM tCtgContents C1 INNER JOIN " +
								"tCategori Ctg INNER JOIN "+
			                    "tCategoryRow Row ON Ctg.CategoryID = Row.CategoryID INNER JOIN "+
			                    "tCategoryCol Col ON Ctg.CategoryID = Col.CategoryID ON C1.ColID = Col.ColID AND C1.RowID = Row.RowID LEFT OUTER JOIN "+
			                    "tCtgContents C2 ON Ctg.SortByColId = C2.ColID AND C1.RowID = C2.RowID AND Ctg.Ascending = 1 LEFT OUTER JOIN "+
			                    "tCtgContents C3 ON Ctg.SortByColId = C3.ColID AND C1.RowID = C3.RowID AND Ctg.Ascending = 0 "+
							"WHERE (Ctg.CourseID = ?) AND (Ctg.Authorzn >= ?) AND (Ctg.Hidden = 0) AND (Col.Removed = 0) AND (Col.Hidden = 0) AND (Row.Hidden = 0) "+
							"ORDER BY Ctg.CategoryID, C2.Text, C2.Dte, C2.Number, C3.Text DESC, C3.Dte DESC, C3.Number DESC, Row.RowID, Col.Positn ";					
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			ps.setInt(2, p.getAuthoriznLevelByCourseID(courseID));
			rs = ps.executeQuery();
			while(rs.next()){
				long contentID = rs.getLong(1);
				coll.add(new CategoryContentsPK(contentID));
			}
			rs.close();
			ps.close();
			conn.close();
		}catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}
			catch(Exception f){
				f.printStackTrace();
			}
			throw new FinderException("Exception throw "+e.getMessage());
		}
		return coll;
	}
}
