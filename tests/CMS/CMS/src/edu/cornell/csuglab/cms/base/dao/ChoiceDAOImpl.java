/*
 * Created on Aug 1, 2004
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.ChoiceBean;
import edu.cornell.csuglab.cms.base.ChoiceDAO;
import edu.cornell.csuglab.cms.base.ChoicePK;

/**
 * @author Yan
 *  
 */
public class ChoiceDAOImpl extends DAOMaster implements ChoiceDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.ChoiceDAO#load(edu.cornell.csuglab.cms.base.ChoicePK,
	 *      edu.cornell.csuglab.cms.base.ChoiceBean)
	 */
	public void load(ChoicePK pk, ChoiceBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tChoices where ChoiceID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getChoiceID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setChoiceID(rs.getLong("ChoiceID"));
				ejb.setSubProblemID(rs.getLong("SubProblemID"));
				ejb.setLetter(rs.getString("Letter"));
				ejb.setText(rs.getString("Text"));
				ejb.setHidden(rs.getBoolean("Hidden"));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
			}
			throw new EJBException("Row id " + pk.getChoiceID()
					+ " failed to load in tChoices.", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.ChoiceDAO#store(edu.cornell.csuglab.cms.base.ChoiceBean)
	 */
	public void store(ChoiceBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tChoices set SubProblemID = ?, Letter = ?, Text = ?, Hidden = ? WHERE ChoiceID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, ejb.getSubProblemID());
			ps.setString(2, ejb.getLetter());
			ps.setString(3, ejb.getText());
			ps.setBoolean(4, ejb.getHidden());
			
			ps.setLong(5, ejb.getChoiceID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
				f.printStackTrace();
			}
			throw new EJBException("Row id " + ejb.getChoiceID()
					+ ", failed to store properly", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.ChoiceDAO#remove(edu.cornell.csuglab.cms.base.ChoicePK)
	 */
	public void remove(ChoicePK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.ChoiceDAO#create(edu.cornell.csuglab.cms.base.ChoiceBean)
	 */
	public ChoicePK create(ChoiceBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ChoicePK pk = null;
		try {
			conn = jdbcFactory.getConnection();
			String updateQuery = "insert into tChoices "
					+ "(SubProblemID, Letter, Text, Hidden) "
					+ "values " + "(?, ?, ?, ?)";
			String findQuery = "select @@identity as ChoiceID from tChoices";
			ps = conn.prepareStatement(updateQuery);
			ps.setLong(1, ejb.getSubProblemID());
			ps.setString(2, ejb.getLetter());
			ps.setString(3, ejb.getText());
			ps.setBoolean(4, ejb.getHidden());
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findQuery).executeQuery();
			if (count > 0 && rs.next()) {
				long ChoiceID = rs.getLong("ChoiceID");
				pk = new ChoicePK(ChoiceID);
				ejb.setChoiceID(ChoiceID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				e.printStackTrace();
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (Exception f) {
				f.printStackTrace();
			}
			throw new CreateException(e.getMessage());
		}
		return pk;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.ChoiceDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.ChoicePK)
	 */
	public ChoicePK findByPrimaryKey(ChoicePK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select ChoiceID from tChoices where ChoiceID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getChoiceID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException(
						"Could not find submitted file with ChoiceID = "
								+ pk.getChoiceID());
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (Exception f) {
			}
			try {
				throw ((FinderException) e); //XXX wtf?
			} catch (ClassCastException x) {
				throw new FinderException("Caught Exception: " + e.getMessage());
			}
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.ChoiceDAO#findByChoice(long)
	 */
	public ChoicePK findByChoiceID(long choiceID) throws FinderException {
		return findByPrimaryKey(new ChoicePK(choiceID));
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.ChoiceDAO#findByChoiceSetID(long)
	 */
	public Collection findBySubProblemID(long subProblemID, boolean hidden) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT c.ChoiceID FROM tChoices c " +
				"WHERE c.SubProblemID = ? AND Hidden = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, subProblemID);
			ps.setBoolean(2, hidden);
			rs = ps.executeQuery();
			while (rs.next()) {
				long choiceID = rs.getLong("ChoiceID");
				result.add(new ChoicePK(choiceID));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}

}
