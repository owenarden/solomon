/*
 * Created on Dec 7, 2004
 */
package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.FinderException;

import edu.cornell.csuglab.cms.author.Principal;


/**
 * @ejb.bean name="CategoryFile"
 *	jndi-name="CategoryFile"
 *	type="BMP"
 *
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class = "edu.cornell.csuglab.cms.base.CategoryFileDAO"
 * impl-class = "edu.cornell.csuglab.cms.base.dao.CategoryFileDAOImpl"
 *  
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/

public abstract class CategoryFileBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
    private long categoryFileID;
	private long contentID;
	private String fileName;
	private boolean hidden;
	private String path;
	private String linkName;
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getCategoryFileID(){
		return this.categoryFileID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileName
	 */
	public void setCategoryFileID(long categoryFileID){
		this.categoryFileID = categoryFileID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getContentID(){
		return this.contentID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param contentID
	 */
	public void setContentID(long contentID){
		this.contentID = contentID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getFileName(){
		return this.fileName;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileName
	 */
	public void setFileName(String fileName){
		this.fileName = fileName;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public boolean getHidden(){
		return this.hidden;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param hidden
	 */
	public void setHidden(boolean hidden){
		this.hidden = hidden;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getPath(){
		return this.path;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param path
	 */
	public void setPath(String path){
		this.path = path;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getLinkName(){
		return this.linkName;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param linkName
	 */
	public void setLinkName(String linkName){
		this.linkName = linkName;
	}
	
	/**
	 * 
	 * @param contentID A content to which this file belongs (note one file on disk could belong to multiple cells)
	 * @param fileName The file title
	 * @param hidden
	 * @param filePath The URL to the actual storage, less the file title
	 * @param linkName The name to be displayed online in place of the URL
	 * @return
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local"
	 */
	public CategoryFilePK ejbCreate(long contentID,	String fileName, boolean hidden, String path, String linkName) throws javax.ejb.CreateException{
		this.setContentID(contentID);
		this.setFileName(fileName);
		this.setHidden(hidden);
		this.setPath(path);
		this.setLinkName(linkName);
		return null;
	}
	

	/**
	 * 
	 * @param pk
	 * @return
	 * @throws FinderException
	 */
	public CategoryFilePK ejbFindByPrimaryKey(CategoryFilePK pk) throws FinderException{
		return null;
	}
	
	/**
	 * 
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseID(long courseID, Principal p) throws FinderException{
		return null;
	}
	
	/**
	 * Returns a collection of all content files for a specific category 
	 * @param categoryID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByCategoryID(long categoryID) throws FinderException{
		return null;
	}
	
	/**
	 * Returns a collection of all the content files for a specific category,
	 * ie including those files where associated row and file itself is hidden
	 * @param categoryID
	 * @return
	 * @throws FinderException
	 *
	public Collection ejbFindByCategoryIDAll(long categoryID) throws FinderException{
		return null;
	}
	*/
	
	/**
	 * @ejb.interface-method view-type="local"
	 */
	public CategoryFileData getCategoryFileData(){
		return new CategoryFileData(categoryFileID, contentID, fileName, hidden, path, linkName);
	}
}
