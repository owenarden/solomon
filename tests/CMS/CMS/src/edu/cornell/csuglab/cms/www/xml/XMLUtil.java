package edu.cornell.csuglab.cms.www.xml;

import java.io.StringWriter;
import java.util.LinkedList;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Evan
 */
public class XMLUtil
{
	/**
	 * Return the children of the given node that have the given tag 
	 * @return A CMSNodeList of Elements. CMSNodeList extends Vector (making it writable)
	 * and implements NodeList (to make it interchangeable with Node.getChildNodes() ).
	 */
	public static CMSNodeList getChildrenByTagName(Element element, String tag)
	{
		NodeList children = element.getChildNodes();
		CMSNodeList result = new CMSNodeList();
		for(int i = 0; i < children.getLength(); i++)
			if(((Element)children.item(i)).getTagName().equals(tag))
				result.add(children.item(i));
		return result;
	}
	
	/**
	 * 
	 * @param element
	 * @param tag
	 * @return The first child Element if there is one; else null
	 */
	public static Element getFirstChildByTagName(Element element, String tag)
	{
		NodeList n= getChildrenByTagName(element, tag);
		if (n.getLength() == 0)
		    return null;
		else 
		    return (Element)n.item(0);
	}
	
	/**
	 * Return a list of children of the given node that have the given value for the given attribute
	 * @param element The Element whose children to check
	 * @param attribute
	 * @param value
	 * @return A CMSNodeList of Elements. CMSNodeList extends Vector (making it writable)
	 * and implements NodeList (to make it interchangeable with Node.getChildNodes() ).
	 */
	public static CMSNodeList getChildrenByAttributeValue(Element element, String attribute, String value)
	{
		NodeList children = element.getChildNodes();
		CMSNodeList result = new CMSNodeList();
		for(int i = 0; i < children.getLength(); i++)
			if(((Element)children.item(i)).getAttribute(attribute).equals(value))
				result.add(children.item(i));
		return result;
	}
	
	public static CMSNodeList getChildrenByTagNameAndAttributeValue(Element element, String tag, String attribute, String value)
	{
		NodeList children = element.getChildNodes();
		CMSNodeList result = new CMSNodeList();
		for(int i = 0; i < children.getLength(); i++)
			if(((Element)children.item(i)).getTagName().equals(tag)
				&& ((Element)children.item(i)).getAttribute(attribute).equals(value))
				result.add(children.item(i));
		return result;
	}
	
	public static CMSNodeList getChildrenByTagNameAndAttributeValueR(Element element, String tag, String attribute, String value)
	{
		NodeList children = element.getElementsByTagName(tag);
		CMSNodeList result = new CMSNodeList();
		for(int i = 0; i < children.getLength(); i++)
			if(((Element)children.item(i)).getAttribute(attribute).equals(value))
				result.add(children.item(i));
		return result;
	}
	
	/**
	 * Turn XmlDocument object into a pretty-printed string for output to file.
	 * @param doc The document Element to convert
	 * @return A pretty-printed string for output 
	 */
	public static String xmlToString(Document doc) 
	{
        try 
        {
            Source source = new DOMSource(doc);
            StringWriter stringWriter = new StringWriter();
            Result result = new StreamResult(stringWriter);
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(source, result);
            return stringWriter.getBuffer().toString();
        } 
        catch (TransformerConfigurationException e) 
        {
            e.printStackTrace();
        } 
        catch (TransformerException e) 
        {
            e.printStackTrace();
        }
        return null;
    }
	
	/**
	 * Eliminates whitespaces from nodes.
	 * For use in parsing a pretty-printed xml doc.
	 * @param node n 
	 */
	public static void deleteWhitespaceChildren(Node n) {
		if(n==null) {
			return;
		}
		
		NodeList children = n.getChildNodes();
		if(children==null || children.getLength()==0) {
			return;
		}
		
		for(int i=0; i<children.getLength(); i++) {
			Node cn = children.item(i);
			LinkedList ctoremove = new LinkedList();
			if (cn != null && cn.getNodeValue() != null && cn.getNodeValue().trim().length()==0) {
				ctoremove.add(cn);
			}
			for(int j=0; j<ctoremove.size(); j++) {
				n.removeChild((Node)ctoremove.get(j));
			}
			ctoremove.clear();
		}
		
		for(int i=0; i<children.getLength(); i++) {
			deleteWhitespaceChildren(children.item(i));
		}
	}
}
