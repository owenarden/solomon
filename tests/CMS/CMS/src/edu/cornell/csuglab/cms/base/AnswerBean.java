/*
 * Created on Nov 5, 2004
 *
 */
package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

/**
 * This contains information for source files associated with 
 * an survey answer. An Answer holds the answer to one Subproblem
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.bean name="Answer"
 *	jndi-name="AnswerBean"
 *	type="BMP" 
 * 
 * @ejb.dao class="edu.cornell.csuglab.cms.base.AnswerDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.AnswerDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/
public abstract class AnswerBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long answerID, answerSetID, subProblemID;
	private String text;
	
	private AnswerSetLocalHome answerSetHome = null;
	
	/**
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getAnswerID() {
		return answerID;
	}
	
	/**
	 * @param answerItemID
	 * @ejb.interface-method view-type="local"
	 */
	public void setAnswerID(long answerID) {
		this.answerID = answerID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getAnswerSetID() {
		return answerSetID;
	}
	
	/**
	 * @param answerID
	 * @ejb.interface-method view-type="local"
	 */
	public void setAnswerSetID(long answerSetID) {
		this.answerSetID = answerSetID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getSubProblemID() {
		return subProblemID;
	}
	
	/**
	 * @param answerID
	 * @ejb.interface-method view-type="local"
	 */
	public void setSubProblemID(long subProblemID) {
		this.subProblemID = subProblemID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * @param itemName
	 * @ejb.interface-method view-type="local"
	 */
	public void setText(String text) {
		this.text = text;
	}
		
	/**
	 * @return
	 * @throws EJBException
	 * @ejb.interface-method view-type="local"
	 */
	public AnswerSetLocal getAnswerSet() throws EJBException {
		try {
			return answerSetHome.findByAnswerSetID(getAnswerSetID());
		}
		catch (Exception e) {
			throw new EJBException(e);
		}
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public AnswerData getAnswerData() {
		return new AnswerData(getAnswerID(), getAnswerSetID(), getSubProblemID(), getText());
	}
	
	/**
	 * @param answerID
	 * @param itemName
	 * @return
	 * @ejb.create-method view-type="local"
	 */
	public AnswerPK ejbCreate(long answerSetID, long subproblemID, String text) throws CreateException {
		setAnswerSetID(answerSetID);
		setSubProblemID(subproblemID);
		setText(text);
		return null;
	}

	/**
	 * @param answerID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public AnswerPK ejbFindByPrimaryKey(AnswerPK pk) throws FinderException {
		return null;
	}
	
	/**
	 * @param answerID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public AnswerPK ejbFindByAnswerID(long answerID) throws FinderException {
		return null;
	}
	
	/**
	 * @param answerSetID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection ejbFindByAnswerSetID(long answerSetID) throws FinderException {
		return null;
	}
	
	/**
	 * @param subProblemID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection ejbFindBySubProblemID(long subproblemID) throws FinderException {
		return null;
	}
}
