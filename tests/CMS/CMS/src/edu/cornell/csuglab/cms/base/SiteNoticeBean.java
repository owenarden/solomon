/*
 * Created on Feb 9, 2007
 */
package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.Collection;

import javax.ejb.EntityBean;
import javax.ejb.FinderException;


/**
 * @ejb.bean name="SiteNotice"
 *	jndi-name="SiteNoticeBean"
 *	type="BMP" 
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.SiteNoticeDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.SiteNoticeDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/
public abstract class SiteNoticeBean implements EntityBean {
    
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long noticeID;
	private String author;
	private Timestamp postedDate;
	private Timestamp expireDate;
	private String text;
	private boolean hidden;
	private boolean deleted;
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the site notice's unique identifying long
	 */
	public long getNoticeID() {
		return noticeID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param id Sets the site notice's unique identifying long
	 */
	public void setNoticeID(long id) {
		this.noticeID = id;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The author of the site notice (as a name)
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param author The author of the site notice (as a name); 
	 * 				 beginning and ending white-spaces get trimmed
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The date the site notice was posted
	 */
	public Timestamp getPostedDate() {
		return postedDate;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param postedDate The date the site notice was posted
	 */
	public void setPostedDate(Timestamp postedDate) {
		this.postedDate = postedDate;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The date the site notice automatically (will) hid(e)
	 */
	public Timestamp getExpireDate() {
		return expireDate;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param postedDate The new date the site notice should auto-hide
	 */
	public void setExpireDate(Timestamp expireDate) {
		this.expireDate = expireDate;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The text-body of the notice
	 */
	public String getText() {
		return text;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param text The text-body of the notice
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Whether the notice should be hidden from the main page to everyone but the admin
	 */
	public boolean getHidden() {
	    return hidden;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param hidden Whether or not the annoucement is viewable by all
	 */
	public void setHidden(boolean hidden) {
	    this.hidden = hidden;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Whether the notice is marked as deleted
	 */
	public boolean getDeleted() {
	    return deleted;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param deleted Whether or not the annoucement should be marked as deleted
	 */
	public void setDeleted(boolean deleted) {
	    this.deleted = deleted;
	}
	
	/**
	 * Checks to see if the given primary key can be found in the database
	 * @param pk The primary key being searched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public SiteNoticePK ejbFindByPrimaryKey(SiteNoticePK pk)
			throws FinderException {
		return null;
	}
	
	/**
	 * Returns the keys of notices which should be shown on the overview
	 * @return The keys
	 * @throws FinderException
	 */
	public Collection ejbFindCurrentShowing()
			throws FinderException {
		return null;
	}

	/**
	 * Returns all non-deleted keys, including hidden and expired
	 * @return The keys
	 * @throws FinderException
	 */
	public Collection ejbFindAllLiving()
			throws FinderException {
		return null;
	}
	
	/**
	 * Returns all deleted keys
	 * @return The keys
	 * @throws FinderException
	 */
	public Collection ejbFindDeleted()
			throws FinderException {
		return null;
	}
	/**
	 * Creates a site notice with the given author's name, text, expiration date & time, and hidden status.
	 * The current time is used for the post date. 
	 * @param author The author's name
	 * @param text The main text of the notice
	 * @param exp Expiration time for the notice (may be null)
	 * @param hidden Whether or not the annoucement is viewable by all
	 * @return The primary key for the notice
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local"
	 **/
	public SiteNoticePK ejbCreate(String author, String text, Timestamp exp, boolean hidden) throws javax.ejb.CreateException {
		setAuthor(author);
		setText(text);
		setPostedDate(new Timestamp(System.currentTimeMillis()));
		setExpireDate(exp);
		setHidden(hidden);
		setDeleted(false);
		return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 **/
	public SiteNoticeData getSiteNoticeData() {
		return new SiteNoticeData(getNoticeID(), getAuthor(),
				getPostedDate(), getExpireDate(), getText(), getHidden(), getDeleted());
	}
}
