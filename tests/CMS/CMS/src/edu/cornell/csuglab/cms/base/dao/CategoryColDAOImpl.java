/*
 * Created on Nov 2, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.author.Principal;
import edu.cornell.csuglab.cms.base.CategoryColBean;
import edu.cornell.csuglab.cms.base.CategoryColDAO;
import edu.cornell.csuglab.cms.base.CategoryColPK;

/**
 * @author yc263
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CategoryColDAOImpl extends DAOMaster implements CategoryColDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryColDAO#load(edu.cornell.csuglab.cms.base.CategoryColPK, edu.cornell.csuglab.cms.base.CategoryColBean)
	 * load from database
	 */
	public void load(CategoryColPK pk, CategoryColBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = jdbcFactory.getConnection();
			String query = "SELECT * FROM tCategoryCol WHERE colID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.colID);
			rs = ps.executeQuery();
			if(rs.next()){
				ejb.setColID(rs.getLong("colID"));
				ejb.setColName(rs.getString("colName"));
				ejb.setColType(rs.getString("colType"));
				ejb.setCategoryID(rs.getLong("categoryID"));
				ejb.setHidden(rs.getBoolean("hidden"));
				ejb.setRemoved(rs.getBoolean("removed"));
				ejb.setPosition(rs.getLong("positn"));
			}
			else throw new EJBException("Error in CategoryColDAOImpl.load");
			rs.close();
			ps.close();
			conn.close();	
		}catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new EJBException("Row id " + pk.colID + "not found in tCategoryCol", e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryColDAO#store(edu.cornell.csuglab.cms.base.CategoryColBean)
	 */
	public void store(CategoryColBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			conn = jdbcFactory.getConnection();
			String query = "UPDATE tCategoryCol " +
							"SET colName = ?, colType = ?, categoryID = ?, hidden = ?, removed = ?, positn = ? " +
							"WHERE colID = ? ";
			ps = conn.prepareStatement(query);
			if(ejb.getColName() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getColName());
			if(ejb.getColType() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getColType());
			ps.setLong(3, ejb.getCategoryID());
			ps.setBoolean(4, ejb.getHidden());
			ps.setBoolean(5, ejb.getRemoved());
			ps.setLong(6, ejb.getPosition());
			ps.setLong(7, ejb.getColID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		}
		catch(SQLException e)
		{
			try
			{
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}
			catch(Exception f) {}
			throw new EJBException("CategoryCol id " + ejb.getColID()+ " failed to update.", e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryColDAO#remove(edu.cornell.csuglab.cms.base.CategoryColPK)
	 */
	public void remove(CategoryColPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryColDAO#create(edu.cornell.csuglab.cms.base.CategoryColBean)
	 */
	public CategoryColPK create(CategoryColBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int count;
		CategoryColPK result = null;
		try{
			conn = jdbcFactory.getConnection();
			String query1 = "INSERT INTO tCategoryCol " + 
							"(colName, colType, categoryID, hidden, removed, positn) VALUES " + 
							"(?, ?, ?, ?, ?, ?)";
			String query2 = "SELECT @@identity AS 'colID' FROM tCategoryCol";
			ps = conn.prepareStatement(query1);
			if(ejb.getColName() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getColName());
			if(ejb.getColType() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getColType());
			ps.setLong(3, ejb.getCategoryID());
			ps.setBoolean(4, ejb.getHidden());
			ps.setBoolean(5, ejb.getRemoved());
			ps.setLong(6, ejb.getPosition());
			count = ps.executeUpdate();
			if(count == 0 )
				throw new CreateException("Error in creating new category column");
			else{
				rs = (conn.prepareStatement(query2)).executeQuery();
				if(rs.next()){
					result = new CategoryColPK(rs.getLong("colID"));
					ejb.setColID(rs.getLong("colID"));
				}
				else
					throw new CreateException("Error in creating new category column");
			}
			if(rs != null) rs.close();
			ps.close();
			conn.close();
		}
		catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new CreateException(e.getMessage());
		}
		return result;
			
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryColDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.CategoryColPK)
	 */
	public CategoryColPK findByPrimaryKey(CategoryColPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = jdbcFactory.getConnection();
			String query = "select colID from tCategoryCol where colID = ? ";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.colID);
			rs = ps.executeQuery();
			if(!rs.next()) throw new FinderException("Could not find the category Col with id:" + pk.colID);
			rs.close();
			ps.close();
			conn.close();	
		}catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new FinderException("Row id "+ pk.colID + "not found in tCategori");
		}
		return pk;
	}
	
	private Collection queryByCategoryID(String query, long categoryID) throws FinderException{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try{
			conn = jdbcFactory.getConnection();
			ps = conn.prepareStatement(query);
			ps.setLong(1, categoryID);
			rs = ps.executeQuery();
			while(rs.next()){
				long colID = rs.getLong(1);
				result.add(new CategoryColPK(colID));
			}
			rs.close();
			ps.close();
			conn.close();
		}catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryColDAO#findByCategoryID(long)
	 */
	public Collection findByCategoryID(long categoryID, boolean removed, boolean visible) throws FinderException {
		String query = "SELECT colID" 
						+ " FROM tCategoryCol"
						+ " WHERE categoryID = ?";
		if(!removed)
		{
			if(visible) query += " AND removed = 0 AND hidden = 0";
			else query += " AND removed = 0"; //no requirement on hidden
		}
		else query += " AND removed = 1"; //no requirement on hidden
		query += " ORDER BY positn";
		return queryByCategoryID(query, categoryID);
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryColDAO#findByCourseID(long)
	 */
	public Collection findByCourseID(long courseID, Principal p) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try{
			conn = jdbcFactory.getConnection();
			String query = 	"SELECT ctgCol.colID " +
							"FROM tCategoryCol AS ctgCol, tCategori AS ctg "+
							"WHERE ctgCol.categoryID = ctg.categoryID AND ctg.courseID = ? "+
							"AND ctgCol.removed = 0 AND ctgCol.hidden = 0 AND ctg.hidden = 0 AND (ctg.Authorzn >= ?) " +
							"ORDER BY ctg.Positn, ctgCol.categoryID , ctgCol.positn ";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			ps.setInt(2, p.getAuthoriznLevelByCourseID(courseID));
			rs = ps.executeQuery();
			while(rs.next()){
				long colID = rs.getLong(1);
				result.add(new CategoryColPK(colID));
			}
			rs.close();
			ps.close();
			conn.close();
		}catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
}
