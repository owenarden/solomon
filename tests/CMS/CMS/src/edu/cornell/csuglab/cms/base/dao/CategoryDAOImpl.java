/*
 * Created on Oct 7, 2004
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.author.Principal;
import edu.cornell.csuglab.cms.base.CategoryBean;
import edu.cornell.csuglab.cms.base.CategoryDAO;
import edu.cornell.csuglab.cms.base.CategoryPK;

/**
 * @author yc263
 */
public class CategoryDAOImpl extends DAOMaster implements CategoryDAO{
	
	/*
	 *  (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryDAO#load(edu.cornell.csuglab.cms.base.CategoryPK, edu.cornell.csuglab.cms.base.CategoryBean)
	 */
	public void load(CategoryPK pk, CategoryBean ejb) throws EJBException{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = jdbcFactory.getConnection();
			String query = "select * from tCategori where CategoryID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getCategoryID());
			rs = ps.executeQuery();
			if(rs.next()){
				ejb.setCategoryID(rs.getLong("CategoryID"));
				ejb.setCourseID(rs.getLong("CourseID"));
				ejb.setCategoryName(rs.getString("CategoryName"));
				ejb.setAscending(rs.getBoolean("Ascending"));
				ejb.setSortByColId(rs.getLong("SortByColID"));
				ejb.setHidden(rs.getBoolean("Hidden"));
				ejb.setFileCount(rs.getLong("FileCount"));
				ejb.setNumShowContents(rs.getLong("NumShowContents"));
				ejb.setAuthorzn(rs.getInt("Authorzn"));
				ejb.setPositn(rs.getInt("Positn"));
				ejb.setIsAnnouncements(rs.getBoolean("Special"));
			}
			else throw new EJBException("Error in CategoryDAOImpl.load");
			rs.close();
			ps.close();
			conn.close();	
		}catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new EJBException("Category id " + pk.categoryID + "not found in tCategory", e);
		}
		}
	
	/*
	 *  (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryDAO#store(edu.cornell.csuglab.cms.base.CategoryBean)
	 */
	public void store(CategoryBean ejb) throws EJBException{
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			conn = jdbcFactory.getConnection();
			String update = "update tCategori "
							+ "set CategoryName = ?, Ascending = ?, SortByColId = ?, Hidden = ?, "
							+		"FileCount = ?, NumShowContents = ?, Authorzn = ?, Positn = ?, Special = ? "
							+ "where CategoryID = ? ";
			ps = conn.prepareStatement(update);
			if(ejb.getCategoryName() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getCategoryName());
			ps.setBoolean(2, ejb.getAscending());
			ps.setLong(3, ejb.getSortByColId());
			ps.setBoolean(4, ejb.getHidden());
			ps.setLong(5, ejb.getFileCount());
			ps.setLong(6, ejb.getNumShowContents());
			ps.setInt(7, ejb.getAuthorzn());
			ps.setInt(8, ejb.getPositn());
			ps.setBoolean(9, ejb.getIsAnnouncements());
			ps.setLong(10, ejb.getCategoryID());
			ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (Exception e) {
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}	

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryDAO#remove(edu.cornell.csuglab.cms.base.CategoryPK)
	 */
	public void remove(CategoryPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryDAO#create(edu.cornell.csuglab.cms.base.CategoryBean)
	 */
	public CategoryPK create(CategoryBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int count;
		CategoryPK result = null;
		try{
			conn = jdbcFactory.getConnection();
			String query1 = "insert into tCategori "
				+ "(CourseID, CategoryName, Ascending, SortByColID, Hidden, FileCount, NumShowContents, Authorzn, Positn, Special) values "
				+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			String query2 = "select @@identity as 'CategoryID' from tCategori";
			ps = conn.prepareStatement(query1);
			ps.setLong(1, ejb.getCourseID());
			if(ejb.getCategoryName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getCategoryName());
			ps.setBoolean(3, ejb.getAscending());
			ps.setLong(4, ejb.getSortByColId());
			ps.setBoolean(5, ejb.getHidden());
			ps.setLong(6, ejb.getFileCount());
			ps.setLong(7, ejb.getNumShowContents());
			ps.setInt(8, ejb.getAuthorzn());
			ps.setInt(9, ejb.getPositn());
			ps.setBoolean(10, ejb.getIsAnnouncements());
			count = ps.executeUpdate();
			rs = (conn.prepareStatement(query2)).executeQuery();
			if(rs.next() && count>0){
				result = new CategoryPK(rs.getLong("CategoryID"));
				ejb.setCategoryID(result.categoryID);
			}	
			else throw new CreateException("Error in creating new category");
			rs.close();
			ps.close();
			conn.close();
		}
		catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.CategoryPK)
	 */
	public CategoryPK findByPrimaryKey(CategoryPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = jdbcFactory.getConnection();
			String query = "select CategoryID from tCategori where CategoryID = ? ";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.categoryID);
			rs = ps.executeQuery();
			if(!rs.next()) throw new FinderException("Could not find the category with id:" + pk.categoryID);
			rs.close();
			ps.close();
			conn.close();	
		}catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new FinderException("Row id "+ pk.categoryID + "not found in tCategori");
		}
		return pk;

	}
	
	private Collection queryByCourseID(String query, long courseID, Principal p) throws FinderException{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try{
			conn = jdbcFactory.getConnection();
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			if(p != null)ps.setInt(2, p.getAuthoriznLevelByCourseID(courseID));
			rs =ps.executeQuery();
			while(rs.next()){
				long catID = rs.getLong(1);
				result.add(new CategoryPK(catID));
			}
			rs.close();
			ps.close();
			conn.close();
		}catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryDAO#findByCourseID(long)
	 */
	public Collection findByCourseID(long courseID, Principal p) throws FinderException {
		String query = "SELECT categoryID " +
						"FROM tCategori " +
						"WHERE CourseID = ? AND hidden = 0 AND (authorzn >= ?) " +
						"ORDER BY positn, categoryID";
		return queryByCourseID(query, courseID, p);
	}
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryDAO#findByCourseIDAll(long)
	 */
	public Collection findByCourseIDAll(long courseID) throws FinderException {
		String query = "SELECT categoryID " +
						"FROM tCategori " +
						"WHERE CourseID = ? " +
						"ORDER BY positn, categoryID";
		return queryByCourseID(query, courseID, null);
	}
}
