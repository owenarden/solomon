/*
 * Created on Mar 15, 2005
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.CommentFileBean;
import edu.cornell.csuglab.cms.base.CommentFileDAO;
import edu.cornell.csuglab.cms.base.CommentFilePK;
import edu.cornell.csuglab.cms.www.util.FileUtil;

/**
 * @author Jon
 *
 */
public class CommentFileDAOImpl extends DAOMaster implements CommentFileDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CommentFileDAO#load(edu.cornell.csuglab.cms.base.CommentFilePK, edu.cornell.csuglab.cms.base.CommentFileBean)
	 */
	public void load(CommentFilePK pk, CommentFileBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tCommentFiles where CommentFileID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getCommentFileID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setCommentFileID(rs.getLong("CommentFileID"));
				ejb.setCommentID(rs.getLong("CommentID"));
				ejb.setFileName(rs.getString("FileName"));
				ejb.setPath(FileUtil.translateDBPath(rs.getString("Path")));
			} else throw new EJBException("Error loading CommentFileBean");
			conn.close();
			rs.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CommentFileDAO#store(edu.cornell.csuglab.cms.base.CommentFileBean)
	 */
	public void store(CommentFileBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tCommentFiles set CommentID = ?, FileName = ?, Path = ? "
					+ "where CommentFileID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, ejb.getCommentID());
			if(ejb.getFileName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getFileName());
			if(ejb.getPath() == null) ps.setNull(3, java.sql.Types.VARCHAR);
			else ps.setString(3, FileUtil.translateSysPath(ejb.getPath()));
			ps.setLong(4, ejb.getCommentFileID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			} catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CommentFileDAO#remove(edu.cornell.csuglab.cms.base.CommentFilePK)
	 */
	public void remove(CommentFilePK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CommentFileDAO#create(edu.cornell.csuglab.cms.base.CommentFileBean)
	 */
	public CommentFilePK create(CommentFileBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		CommentFilePK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String insert = "insert into tCommentFiles "
				+ "(CommentID, FileName, Path) values (?,?,?)";
			String getKey = "select @@identity as 'CommentFileID' from tCommentFiles"; 
			ps = conn.prepareStatement(insert);
			ps.setLong(1, ejb.getCommentID());
			if(ejb.getFileName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getFileName());
			if(ejb.getPath() == null) ps.setNull(3, java.sql.Types.VARCHAR);
			else ps.setString(3, FileUtil.translateSysPath(ejb.getPath()));
			ps.executeUpdate();
			rs = conn.prepareStatement(getKey).executeQuery();
			if (rs.next()) {
				long commentFileID = rs.getLong("CommentFileID");
				result = new CommentFilePK(commentFileID);
				ejb.setCommentFileID(commentFileID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CommentFileDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.CommentFilePK)
	 */
	public CommentFilePK findByPrimaryKey(CommentFilePK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		CommentFilePK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select CommentFileID from tCommentFiles where CommentFileID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getCommentFileID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find a CommentFile matching primary key");
			}
			result = pk;
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CommentFileDAO#findByCommentID(long)
	 */
	public Collection findByCommentID(long commentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select CommentFileID from tCommentFiles where CommentID = ? "
					+ "order by CommentFileID ASC";
			ps = conn.prepareStatement(query);
			ps.setLong(1, commentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new CommentFilePK(rs.getLong("CommentFileID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CommentFileDAO#findByGroupIDs(long[])
	 */
	public Collection findByGroupIDs(Collection groupids) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select CommentFileID from tCommentFiles f, tComments c where " +
					"c.CommentID = f.CommentID and c.Hidden = ? and (";
			for (int i=0; i < groupids.size() - 1; i++) {
				query += "c.GroupID = ? or ";
			}
			query += "c.GroupID = ?) order by c.DateEntered DESC";
			ps = conn.prepareStatement(query);
			ps.setBoolean(1, false);
			int c = 2;
			for (Iterator i=groupids.iterator(); i.hasNext(); ) {
				ps.setLong(c++, ((Long)i.next()).longValue());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new CommentFilePK(rs.getLong("CommentFileID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

}
