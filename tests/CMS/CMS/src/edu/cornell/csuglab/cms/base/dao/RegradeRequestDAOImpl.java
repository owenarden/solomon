/*
 * Created on Mar 15, 2005
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.GroupMemberBean;
import edu.cornell.csuglab.cms.base.RegradeRequestBean;
import edu.cornell.csuglab.cms.base.RegradeRequestDAO;
import edu.cornell.csuglab.cms.base.RegradeRequestPK;

/**
 * @author Jon
 *
 */
public class RegradeRequestDAOImpl extends DAOMaster implements RegradeRequestDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RegradeRequestDAO#load(edu.cornell.csuglab.cms.base.RegradeRequestPK, edu.cornell.csuglab.cms.base.RegradeRequestBean)
	 */
	public void load(RegradeRequestPK pk, RegradeRequestBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tRegradeRequests where RequestID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getRequestID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setRequestID(rs.getLong("RequestID"));
				ejb.setGroupID(rs.getLong("GroupID"));
				ejb.setNetID(rs.getString("NetID"));
				try {
					ejb.setRequest(rs.getString("Request"));
				} catch (SQLException e) {
					ejb.setRequest("");
				}
				ejb.setStatus(rs.getString("Status"));
				ejb.setDateEntered(rs.getTimestamp("DateEntered"));
				long commentID = rs.getLong("CommentID");
				if (rs.wasNull()) {
				    ejb.setCommentID(null);
				} else {
				    ejb.setCommentID(new Long(commentID));
				}
				ejb.setSubProblemID(rs.getLong("SubProblemID"));
			} else throw new EJBException("Error loading RegradeRequestBean");
			conn.close();
			rs.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RegradeRequestDAO#store(edu.cornell.csuglab.cms.base.RegradeRequestBean)
	 */
	public void store(RegradeRequestBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tRegradeRequests set NetID = ?, Request = ?, Status = ?," +
					" SubProblemID = ?, CommentID = ? where RequestID = ?";
			ps = conn.prepareStatement(query);
			if (ejb.getNetID() == null)  {
			    ps.setNull(1, Types.VARCHAR);
			} else {
			    ps.setString(1, ejb.getNetID());
			}
			if(ejb.getRequest() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getRequest());
			if(ejb.getStatus() == null) ps.setNull(3, java.sql.Types.VARCHAR);
			else ps.setString(3, ejb.getStatus());
			ps.setLong(4, ejb.getSubProblemID());
			if (ejb.getCommentID() == null) {
				ps.setNull(5, Types.BIGINT);
			} else {
				ps.setLong(5, ejb.getCommentID().longValue());
			}
			ps.setLong(6, ejb.getRequestID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			} catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RegradeRequestDAO#remove(edu.cornell.csuglab.cms.base.RegradeRequestPK)
	 */
	public void remove(RegradeRequestPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RegradeRequestDAO#create(edu.cornell.csuglab.cms.base.RegradeRequestBean)
	 */
	public RegradeRequestPK create(RegradeRequestBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		RegradeRequestPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String insert = "insert into tRegradeRequests "
				+ "(DateEntered, SubProblemID, GroupID, NetID, Request) values (?,?,?,?,?)";
			String getKey = "select @@identity as 'RequestID' from tRegradeRequests"; 
			ps = conn.prepareStatement(insert);
			ps.setTimestamp(1, ejb.getDateEntered());
			ps.setLong(2, ejb.getSubProblemID());
			ps.setLong(3, ejb.getGroupID());
			if (ejb.getNetID() == null) {
			    ps.setNull(4, Types.VARCHAR);
			} else {
			    ps.setString(4, ejb.getNetID());
			}
			if(ejb.getRequest() == null) ps.setNull(5, java.sql.Types.VARCHAR);
			else ps.setString(5, ejb.getRequest());
			ps.executeUpdate();
			rs = conn.prepareStatement(getKey).executeQuery();
			if (rs.next()) {
				long requestID = rs.getLong("RequestID");
				result = new RegradeRequestPK(requestID);
				ejb.setRequestID(requestID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			CreateException f = new CreateException("Error during insert query. " + e.getClass() + ": "+ e.getMessage());
			f.setStackTrace(e.getStackTrace());
			throw f;
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RegradeRequestDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.RegradeRequestPK)
	 */
	public RegradeRequestPK findByPrimaryKey(RegradeRequestPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select RequestID from tRegradeRequests where RequestID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getRequestID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find a RegradeRequest matching primary key");
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RegradeRequestDAO#findByAssignmentID(long)
	 */
	public Collection findByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select r.RequestID, r.DateEntered from tRegradeRequests r, tGroups g " +
					"where r.GroupID = g.GroupID and g.AssignmentID = ? order by Status, DateEntered ASC";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new RegradeRequestPK(rs.getLong("RequestID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RegradeRequestDAO#findPendingByAssignmentID(long)
	 */
	public Collection findPendingByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select r.RequestID, r.DateEntered from tRegradeRequests r, tGroups g " +
					"where r.GroupID = g.GroupID and g.AssignmentID = ? and r.Status = ? order by DateEntered ASC";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			ps.setString(2, RegradeRequestBean.PENDING);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new RegradeRequestPK(rs.getLong("RequestID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RegradeRequestDAO#findPendingByCourseID(long)
	 */
	public Collection findPendingByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select r.RequestID, r.DateEntered from tRegradeRequests r, tGroups g, tAssignment a where "
					+ "r.GroupID = g.GroupID and g.AssignmentID = a.AssignmentID and a.CourseID = ? and r.Status = ? "
					+ "order by r.DateEntered ASC";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			ps.setString(2, RegradeRequestBean.PENDING);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new RegradeRequestPK(rs.getLong("RequestID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT DISTINCT r.RequestID " +
				"FROM tRegradeRequests r INNER JOIN " +
	            "tGroupMembers m ON r.GroupID = m.GroupID INNER JOIN " +
	            "tGroups g ON m.GroupID = g.GroupID INNER JOIN " +
	            "tAssignment a ON g.AssignmentID = a.AssignmentID " +
	            "WHERE (a.CourseID = ?) AND (m.Status = ?) AND (a.Hidden = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			ps.setString(2, GroupMemberBean.ACTIVE);
			ps.setBoolean(3, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new RegradeRequestPK(rs.getLong("RequestID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RegradeRequestDAO#findByGroupIDs(long[])
	 */
	public Collection findByGroupIDs(Collection groupids) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (groupids.size() == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select RequestID from tRegradeRequests where (";
			for (int i=0; i < groupids.size() - 1; i++) {
				query += "GroupID = ? or ";
			}
			query += "GroupID = ?) order by DateEntered DESC";
			ps = conn.prepareStatement(query);
			int c = 0;
			for (Iterator i = groupids.iterator(); i.hasNext(); ) {
				ps.setLong(++c, ((Long)i.next()).longValue());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new RegradeRequestPK(rs.getLong("RequestID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RegradeRequestDAO#findByGroupID(long)
	 */
	public Collection findByGroupID(long groupID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select RequestID from tRegradeRequests where GroupID = ? "+
							"order by DateEntered ASC";
			ps = conn.prepareStatement(query);
			ps.setLong(1, groupID);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new RegradeRequestPK(rs.getLong("RequestID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.RegradeRequestDAO#findByCommentID(long)
     */
    public Collection findByCommentID(long commentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select RequestID from tRegradeRequests where CommentID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, commentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new RegradeRequestPK(rs.getLong("RequestID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
    }

}
