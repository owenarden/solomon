/*
 * Created on Sep 1, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.util.category;

import java.sql.Timestamp;
import java.util.ArrayList;


/**
 * @author evan
 *
 * Holds info representing a content to be added to a category
 * (base class for CtgNewContent<TYPE>Info classes)
 */
public abstract class CtgNewContentInfo implements CtgContentInfo
{
	protected long rowID, //a unique row identifier within the space of the calling JSP
		colID; //the database's column ID for the enclosing column
	
	public CtgNewContentInfo(long rowID, long colID)
	{
		this.rowID = rowID;
		this.colID = colID;
	}
	
	public void setColID(long colID){
		this.colID = colID;
	}
	
	public void setRowID(long rowID){
		this.rowID = rowID;
	}
	
	/*
	 * Functions used by RootBean::createCtgContent(). A null return value signifies no data.
	 */
	public long getRowID() {return this.rowID;}
	public long getColID() {return this.colID;}
	public abstract String getColType();
	public abstract Timestamp getDate();       //used by contents of type date
	public abstract String getText();          //used by contents of type text
	public abstract Long getNumber();          //used by contents of type number
	public abstract String getURLAddress();    //used by contents of type url
	public abstract String getURLLabel();      //used by contents of type url
	public abstract ArrayList getFileInfos();  //used by contents of type file
	public abstract ArrayList getFileLabels(); //used by contents of type file
}
