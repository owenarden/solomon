/*
 * Created on Nov 5, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.AssignmentItemBean;
import edu.cornell.csuglab.cms.base.AssignmentItemDAO;
import edu.cornell.csuglab.cms.base.AssignmentItemPK;

/**
 * @author Jon
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AssignmentItemDAOImpl extends DAOMaster implements AssignmentItemDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentItemDAO#load(edu.cornell.csuglab.cms.base.AssignmentItemPK, edu.cornell.csuglab.cms.base.AssignmentItemBean)
	 */
	public void load(AssignmentItemPK pk, AssignmentItemBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tAssignmentItems "
					+ "where AssignmentItemID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getAssignmentItemID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setAssignmentID(rs.getLong("AssignmentID"));
				ejb.setAssignmentItemID(rs.getLong("AssignmentItemID"));
				ejb.setItemName(rs.getString("ItemName"));
				ejb.setHidden(rs.getBoolean("Hidden"));
			}
			conn.close();
			rs.close();
			ps.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentItemDAO#store(edu.cornell.csuglab.cms.base.AssignmentItemBean)
	 */
	public void store(AssignmentItemBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tAssignmentItems set ItemName = ?, Hidden = ? " + 
				"where AssignmentItemID = ? and AssignmentID = ?";
			ps = conn.prepareStatement(query);
			if(ejb.getItemName() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getItemName());
			ps.setBoolean(2, ejb.getHidden());
			ps.setLong(3, ejb.getAssignmentItemID());
			ps.setLong(4, ejb.getAssignmentID());
			ps.executeUpdate(); //TODO check for error?
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentItemDAO#remove(edu.cornell.csuglab.cms.base.AssignmentItemPK)
	 */
	public void remove(AssignmentItemPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentItemDAO#create(edu.cornell.csuglab.cms.base.AssignmentItemBean)
	 */
	public AssignmentItemPK create(AssignmentItemBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AssignmentItemPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createQuery = "insert into tAssignmentItems (AssignmentID, ItemName) values (?, ?)";
			String findQuery = "select @@identity as 'AssignmentItemID' from tAssignmentItems";
			ps = conn.prepareStatement(createQuery);
			ps.setLong(1, ejb.getAssignmentID());
			if(ejb.getItemName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getItemName());
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findQuery).executeQuery();
			if (count > 0 && rs.next()) {
				long assignmentItemID = rs.getLong("AssignmentItemID");
				ejb.setAssignmentItemID(assignmentItemID);
				result = new AssignmentItemPK(assignmentItemID);
			}
			else {
				throw new CreateException("Failed to create new AssignmentFile");
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentItemDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.AssignmentItemPK)
	 */
	public AssignmentItemPK findByPrimaryKey(AssignmentItemPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select AssignmentItemID from tAssignmentItems "
					+ "where AssignmentItemID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, pk.getAssignmentItemID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find assignment file with " +
						"AssignmentItemID = " + pk.getAssignmentItemID());
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}
	
	private Collection findByAssignmentID(long assignmentID, boolean hidden) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select AssignmentItemID from tAssignmentItems "
					+ "where assignmentid = ? and hidden = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentID);
			ps.setBoolean(2, hidden);
			rs = ps.executeQuery();
			while (rs.next()) {
				AssignmentItemPK pk = new AssignmentItemPK(rs.getLong(1));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentItemDAO#findByAssignmentID(long)
	 */
	public Collection findByAssignmentID(long assignmentID) throws FinderException {
		return findByAssignmentID(assignmentID, false);
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentItemDAO#findHiddenByAssignmentID(long)
	 */
	public Collection findHiddenByAssignmentID(long assignmentID) throws FinderException {
		return findByAssignmentID(assignmentID, true);
	}
	

}
