/*
 * Created on Apr 7, 2005
 */
package edu.cornell.csuglab.cms.author;


/**
 * @author yc263
 *
 * UserStaffAsCornellMember represents the apparent general Cornell Member user who doesn't belong in that course as student/staff
 * for a staff who has decided to view it's course page as that apparent user; 
 */
public class UserStaffAsCornellMember extends UserWrapper{
	public UserStaffAsCornellMember(long courseID){
		super(courseID);
	}
	
	public int getUserType(){
		return UserWrapper.UT_STAFF_AS_CORNELLMEM;
	}
	
	public int getAuthoriznLevelByCourseID(long courseID) {
		return Principal.AUTHOR_CORNELL_COMMUNITY;
	}
	
	public String getUserID() {
		return "Cornell Member";
	}
}
