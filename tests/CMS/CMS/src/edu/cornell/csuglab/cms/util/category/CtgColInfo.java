/*
 * Created on Nov 21, 2004
 */
package edu.cornell.csuglab.cms.util.category;

/**
 * @author yc263, evan
 */
public class CtgColInfo {
	private long colID;
	private String columnName;
	private String colType;
	private long position;
	private boolean hidden;
	private boolean removed;
	private boolean sortByThisCol;

	public CtgColInfo(){
		this.hidden = false;
		this.removed = false;
		this.sortByThisCol = false;
	}
	
	public CtgColInfo(long colID){
		this.hidden = false;
		this.removed = false;
		this.sortByThisCol = false;
		this.colID = colID;
	}
	
	public CtgColInfo(String columnName, String colType, long position){
		this.columnName = columnName;
		this.colType = colType;
		this.position = position;
		this.hidden = false;
		this.removed = false;
		this.sortByThisCol = false;
	}

	public void setColumnID(long columnID){
		this.colID = columnID;
	}
	
	public void setColumnName(String columnName){
		this.columnName = columnName;
	}
	
	public void setColumnType(String colType){
		this.colType = colType;
	}
	
	public void setPosition(long position){
		this.position = position;
	}
	
	public void setSortByThis(){
		this.sortByThisCol = true;
	}
	
	public void setHidden(boolean hidden){
		this.hidden = hidden;
	}
	
	public void setRemoved(boolean removed)
	{
		this.removed = removed;
	}
	
	public long getColumnID(){
		return this.colID;
	}
	public String getColName(){
		return this.columnName;
	}
	
	public String getColType(){
		return this.colType;
	}
	
	public long getPosition(){
		return this.position;
	}
	
	public boolean getHidden(){
		return this.hidden;
	}
	
	public boolean getRemoved()
	{
		return this.removed;
	}
	
	public boolean getSortByThis(){
		return this.sortByThisCol;
	}

}
