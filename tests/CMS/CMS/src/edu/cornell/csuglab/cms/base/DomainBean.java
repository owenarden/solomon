/*
 * Created on Mar 29, 2010
 *
 */

package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EntityBean;
import javax.ejb.FinderException;

/**
 * @ejb.bean name="Domain"
 *	jndi-name="DomainBean"
 *	type="BMP" 
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.DomainDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.DomainDAOImpl"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 **/
public abstract class DomainBean implements EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long domainID;
	private String domainName;
	private String domainPrefix;
	
	/**
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getDomainID() {
		return domainID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param domainID
	 */
	public void setDomainID(long domainID) {
		this.domainID = domainID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getDomainName() {
		return domainName;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param domainName
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getDomainPrefix() {
		return domainPrefix;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param domainPrefix
	 */
	public void setDomainPrefix(String domainPrefix) {
		this.domainPrefix = domainPrefix;
	}
	
	/**
	 * @param key
	 * @return
	 * @throws FinderException
	 */
	public DomainPK ejbFindByPrimaryKey(DomainPK key) throws FinderException {
		return null;
	}
	
	/**
	 * @return A Collection of Domain objects
	 * @throws FinderException
	 */
	public Collection ejbFindAllDomains() throws FinderException {
		return null;
	}
	
	/** 
	 * @return
	 */
	public DomainData getDomainData() {
		return new DomainData(getDomainID(), getDomainName(), getDomainPrefix());
	}
	
	/**
	 * @param domainName
	 * @return
	 * @throws CreateException
	 * @ejb.create-method view-type="local"
	 */
	public DomainPK ejbCreate(String domainName) throws CreateException {
		setDomainName(domainName);
		return null;
	}
	
}
