/*
 * Created on Nov 5, 2004
 *
 */
package edu.cornell.csuglab.cms.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

/**
 * This contains information for source files associated with 
 * an assignment. An AssignmentItem holds information on one file
 * and is associated with one AssignmentFile, but the associated File
 * may change at any time.
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.bean name="AssignmentItem"
 *	jndi-name="AssignmentItemBean"
 *	type="BMP" 
 * 
 * @ejb.dao class="edu.cornell.csuglab.cms.base.AssignmentItemDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.AssignmentItemDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/
public abstract class AssignmentItemBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long assignmentItemID, assignmentID;
	private String itemName;
	private boolean hidden;
	
	private AssignmentFileLocalHome assignmentFileHome = null;
	
	/**
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getAssignmentItemID() {
		return assignmentItemID;
	}
	
	/**
	 * @param assignmentItemID
	 * @ejb.interface-method view-type="local"
	 */
	public void setAssignmentItemID(long assignmentItemID) {
		this.assignmentItemID = assignmentItemID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getAssignmentID() {
		return assignmentID;
	}
	
	/**
	 * @param assignmentID
	 * @ejb.interface-method view-type="local"
	 */
	public void setAssignmentID(long assignmentID) {
		this.assignmentID = assignmentID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getItemName() {
		return itemName;
	}
	
	/**
	 * @param itemName
	 * @ejb.interface-method view-type="local"
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean getHidden() {
		return hidden;
	}
	
	/**
	 * @param hidden
	 * @ejb.interface-method view-type="local"
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	
	/**
	 * @return
	 * @throws EJBException
	 * @ejb.interface-method view-type="local"
	 */
	public AssignmentFileLocal getAssignmentFile() throws EJBException {
		try {
			return assignmentFileHome().findByAssignmentItemID(getAssignmentItemID());
		}
		catch (Exception e) {
			throw new EJBException(e);
		}
	}
	
	/**
	 * @return
	 * @throws EJBException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getHiddenAssignmentFiles() throws EJBException {
		Collection c, result = new ArrayList();
		try {
			c = assignmentFileHome().findHiddenByAssignmentItemID(getAssignmentItemID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((AssignmentFileLocal) i.next()).getAssignmentFileData());
			}
		}
		catch (Exception e) {
			throw new EJBException(e); 
		}
		return result;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public AssignmentItemData getAssignmentItemData() {
		return new AssignmentItemData(getAssignmentItemID(), getAssignmentID(),
				getItemName(), getHidden());
	}
	
	/**
	 * @param assignmentID
	 * @param itemName
	 * @return
	 * @ejb.create-method view-type="local"
	 */
	public AssignmentItemPK ejbCreate(long assignmentID, String itemName) throws CreateException {
		setAssignmentID(assignmentID);
		setItemName(itemName);
		setHidden(false);
		return null;
	}

	/**
	 * @param assignmentItemID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public AssignmentItemPK ejbFindByPrimaryKey(AssignmentItemPK pk) throws FinderException {
		return null;
	}
	
	/**
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection ejbFindByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}

	/**
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection ejbFindHiddenByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	private AssignmentFileLocalHome assignmentFileHome() {
		try {
			if (assignmentFileHome == null) {
				assignmentFileHome = AssignmentFileUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assignmentFileHome;
	}
}
