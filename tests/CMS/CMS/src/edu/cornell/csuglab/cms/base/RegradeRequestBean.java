/*
 * Created on Mar 15, 2005
 */
package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

/**
 * @ejb.bean name="RegradeRequest"
 *	jndi-name="RegradeRequest"
 *	type="BMP"
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.RegradeRequestDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.RegradeRequestDAOImpl"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"

 * @generated
 **/
public abstract class RegradeRequestBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long requestID;
	private long groupID;
	private long subProblemID;
	private Long commentID;
	private String netID;
	private String request;
	private String status;
	private Timestamp dateEntered;
	
	/* Status strings */
	public static final String
		PENDING= "Pending",
		REGRADED= "Regraded";
	
	/**
	 * @return Returns the dateEntered.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public Timestamp getDateEntered() {
		return dateEntered;
	}
	
	/**
	 * @param dateEntered The dateEntered to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setDateEntered(Timestamp dateEntered) {
		this.dateEntered = dateEntered;
	}
	
	/**
	 * @return Returns the gradeID.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getSubProblemID() {
		return subProblemID;
	}
	
	/**
	 * @param gradeID The gradeID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setSubProblemID(long subProblemID) {
		this.subProblemID = subProblemID;
	}
	
	/**
	 * @return Returns the groupID.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getGroupID() {
		return groupID;
	}
	
	/**
	 * @param groupID The groupID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}
	
	/**
	 * @return Returns the netID.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getNetID() {
		return netID;
	}
	
	/**
	 * @param netID The netID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setNetID(String netID) {
		this.netID = netID;
	}
	
	/**
	 * @return Returns the request.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getRequest() {
		return request;
	}
	
	/**
	 * @param request The request to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setRequest(String request) {
		this.request = request;
	}
	
	/**
	 * @return Returns the requestID.
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getRequestID() {
		return requestID;
	}
	
	/**
	 * @param requestID The requestID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setRequestID(long requestID) {
		this.requestID = requestID;
	}
	
	/**
	 * @return Returns the status.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * @param status The status to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public Long getCommentID() {
		return commentID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param commentID
	 */
	public void setCommentID(Long commentID) {
		this.commentID = commentID;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public RegradeRequestData getRegradeRequestData() {
		return new RegradeRequestData(getDateEntered(),
				getSubProblemID(), getGroupID(), getNetID(), getRequest(), 
				getRequestID(), getStatus(), getCommentID());
	}
	
	/**
	 * 
	 * @param pk
	 * @return
	 */
	public RegradeRequestPK ejbFindByPrimaryKey(RegradeRequestPK pk) throws FinderException {
		return null;
	}
	
	/**
	 * Returns collection of regradeRequest in ascending order of date entered
	 * @param groupID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupID(long groupID) throws FinderException{
		return null;
	}
	
	/**
	 * @param groupids
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupIDs(Collection groupids) throws FinderException {
		return null;
	}
	
	/**
	 * Returns all the regrade requests for the given assignment
	 * @param assignmentID The AssignmentID of the assignment
	 * @return
	 */
	public Collection ejbFindByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * Returns all the Pending regrade requests for this assignment
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindPendingByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * Returns all the pending regrade requests for a course
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindPendingByCourseID(long courseID) throws FinderException {
		return null;
	}
	
	/**
	 * Returns all RegradeRequests which have been responded to by a given
	 * comment
	 * @param commentID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByCommentID(long commentID) throws FinderException {
	    return null;
	}
	
	public Collection ejbFindByCourseID(long courseID) throws FinderException {
		return null;
	}
	
	/**
	 * @param assignmentID
	 * @param dateEntered
	 * @param subProblemID
	 * @param groupID
	 * @param netID
	 * @param request
	 * @return
	 * @ejb.create-method view-type="local"
	 */
	public RegradeRequestPK ejbCreate(long subProblemID, long groupID, String netID, String request) throws CreateException {
		setDateEntered(new Timestamp(System.currentTimeMillis()));
		setSubProblemID(subProblemID);
		setGroupID(groupID);
		setNetID(netID);
		setRequest(request);
		setStatus(PENDING);
		setCommentID(null);
		return null;
	}

  /**
   * <!-- begin-user-doc -->
   * The container invokes this method immediately after it calls ejbCreate.
   * <!-- end-user-doc -->
   * 
   * @generated
   */
  public void ejbPostCreate() throws javax.ejb.CreateException {
    // begin-user-code
    // end-user-code
  }


}
