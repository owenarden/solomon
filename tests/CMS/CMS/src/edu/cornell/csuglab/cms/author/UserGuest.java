/*
 * Created on Apr 7, 2005
 */
package edu.cornell.csuglab.cms.author;

/**
 * @author yc263
 *
 * UserGuest represents a guest user of CMS who hasn't been authenticated.
 * They have limited access.
 */
public class UserGuest extends UserWrapper {
	public UserGuest(){
		super();
	}
	public UserGuest(long courseID){
		super(courseID);
	}
	public int getUserType(){
		return UserWrapper.UT_GUEST;
	}
	
	public int getAuthoriznLevelByCourseID(long courseID) {
		return Principal.AUTHOR_GUEST;
	}
	
	public String getUserID() {
		return "guest";
	}
}
