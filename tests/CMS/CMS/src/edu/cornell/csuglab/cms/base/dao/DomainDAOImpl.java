/*
 * Created on Sep 6, 2004
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.DomainBean;
import edu.cornell.csuglab.cms.base.DomainDAO;
import edu.cornell.csuglab.cms.base.DomainPK;
import edu.cornell.csuglab.cms.base.StaffBean;
import edu.cornell.csuglab.cms.base.StudentBean;

/**
 * @author Jon
 *
 */
public class DomainDAOImpl extends DAOMaster implements DomainDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SemesterDAO#load(edu.cornell.csuglab.cms.base.SemesterPK, edu.cornell.csuglab.cms.base.SemesterBean)
	 */
	public void load(DomainPK pk, DomainBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select DomainID,DomainName,DomainPrefix from tDomains where DomainID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getDomainID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setDomainID(rs.getInt("DomainID"));
				ejb.setDomainName(rs.getString("DomainName"));
				ejb.setDomainPrefix(rs.getString("DomainPrefix"));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.DomainDAO#store(edu.cornell.csuglab.cms.base.DomainBean)
	 */
	public void store(DomainBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "update tDomains set DomainName = ?, DomainPrefix = ? where DomainID = ?";
			ps = conn.prepareStatement(update);
			if(ejb.getDomainName() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getDomainName());
			if(ejb.getDomainPrefix() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getDomainPrefix());
			ps.setLong(3, ejb.getDomainID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			}
			catch (Exception f) {}
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.DomainDAO#remove(edu.cornell.csuglab.cms.base.DomainPK)
	 */
	public void remove(DomainPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.DomainDAO#create(edu.cornell.csuglab.cms.base.DomainBean)
	 */
	public DomainPK create(DomainBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null, ret = null;
		ResultSet rs = null;
		DomainPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "insert into tDomains (DomainName, DomainPrefix) values (?, ?)";
			String findString = "select @@identity as 'DomainID' from tDomains";
			ps = conn.prepareStatement(createString);
			if(ejb.getDomainName() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getDomainName());
			if(ejb.getDomainPrefix() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getDomainPrefix());
			ps.executeUpdate();
			ret = conn.prepareStatement(findString);
			rs = ret.executeQuery();
			if (rs.next()) {
				long semesterID = rs.getLong("DomainID");
				result = new DomainPK(semesterID);
				ejb.setDomainID(result.getDomainID());
			}
			conn.close();
			ps.close();
			ret.close();
			rs.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if(ret != null) ret.close();
				if (rs != null) rs.close();
			}
			catch (Exception ex) {}
			throw new EJBException(e);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.DomainDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.DomainPK)
	 */
	public DomainPK findByPrimaryKey(DomainPK key) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select DomainID from tDomains where DomainID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, key.getDomainID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Failed to find Domain with DomainID = " + key.getDomainID());
			}
			conn.close();
			ps.close();
			rs.close();
		}
		//XXX what specific error is this first catch meant for?
		catch (FinderException e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			throw e;
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();				
			}
			catch (Exception f) {}
			throw new FinderException("Exception caught: " + e.getMessage());
		}
		return key;
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.DomainDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.DomainPK)
	 */
	public Collection findAllDomains() throws FinderException
	{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select DomainID from tDomains";
			ps = conn.prepareStatement(queryString);
			rs = ps.executeQuery();
			while(rs.next())
			{
				result.add(new DomainPK(rs.getLong(1)));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();				
			}
			catch (Exception f) {}
			throw new FinderException("Exception caught: " + e.getMessage());
		}
		return result;
	}
	
	public Collection findByUser(String netID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT DISTINCT s.DomainID " +
					"FROM tDomains s INNER JOIN " + 
                    "tCourse c ON s.DomainID = c.DomainID LEFT OUTER JOIN " +
                    "tStudent su ON c.CourseID = su.CourseID AND su.NetID = ? LEFT OUTER JOIN " +
                    "tStaff sf ON c.CourseID = sf.CourseID AND sf.NetID = ? " +
                    "WHERE ((su.Status = ?) OR (sf.Status = ?)) AND s.Hidden = ? " + 
                    "ORDER BY s.DomainID ASC";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netID);
			ps.setString(2, netID);
			ps.setString(3, StudentBean.ENROLLED);
			ps.setString(4, StaffBean.ACTIVE);
			ps.setBoolean(5, false);
			rs = ps.executeQuery();
			while(rs.next())
			{
				result.add(new DomainPK(rs.getLong(1)));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();				
			}
			catch (Exception f) {}
			throw new FinderException("Exception caught: " + e.getMessage());
		}
		return result;
	}

}
