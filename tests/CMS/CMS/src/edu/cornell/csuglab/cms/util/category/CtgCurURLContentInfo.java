/*
 * Created on Sep 1, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.util.category;


/**
 * @author evan
 *
 * Holds info representing an existing content of type URL, to be edited
 */
public class CtgCurURLContentInfo extends CtgCurContentInfo
{
	private String address, //the URL linked to
	label;               //the name displayed for the link

	public CtgCurURLContentInfo(long contentID, String address, String label)
	{
		super(contentID);
		this.address = address;
		this.label = label;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public String getLabel()
	{
		return label;
	}
	
	public void setAddress(String address)
	{
		this.address = address;
	}
	
	public void setLabel(String label)
	{
		this.label = label;
	}
	
	public String toString()
	{
		return "CurURLContentInfo{url: " + getAddress() + ", label " + getLabel() + "}";
	}
}
