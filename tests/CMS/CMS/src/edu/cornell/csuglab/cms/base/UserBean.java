/*
 * Created on Feb 27, 2004 To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */

package edu.cornell.csuglab.cms.base;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Arrays;

import javax.ejb.EntityBean;
import javax.ejb.FinderException;

/**
 * @ejb.bean name="User" jndi-name="UserBean" type="BMP" view-type="local"
 * 
 * @jboss.method-attributes pattern="get*" read-only="true"
 * 
 * @ejb.dao class="edu.cornell.csuglab.cms.base.UserDAO"
 *          impl-class="edu.cornell.csuglab.cms.base.dao.UserDAOImpl"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS" res-type="javax.sql.Datasource"
 *                   res-auth="Container"
 * 
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS" jndi-name="java:/MSSQLDS"
 * 
 * @ejb.ejb-ref ejb-name="Course" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Course" jndi-name="Course"
 * 
 * @ejb.ejb-ref ejb-name="Assignment" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Assignment" jndi-name="Assignment"
 * 
 * @ejb.ejb-ref ejb-name="Announcement" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Announcement" jndi-name="Announcement"
 * 
 * @ejb.ejb-ref ejb-name="Group" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Group" jndi-name="Group"
 * 
 * @ejb.ejb-ref ejb-name="Staff" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Staff" jndi-name="Staff"
 * 
 * @ejb.ejb-ref ejb-name="Student" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Student" jndi-name="Student"
 * 
 * @ejb.ejb-ref ejb-name = "CategoryContents" view-type = "remote"
 * @jboss.ejb-ref-jndi ref-name ="CategoryContents" jndi-name =
 *                     "CategoryContents"
 * 
 * @ejb.ejb-ref ejb-name = "Grade" view-type = "remote"
 * @jboss.ejb-ref-jndi ref-name ="Grade" jndi-name = "Grade"
 * 
 * @ejb.ejb-ref ejb-name = "Comment" view-type = "remote"
 * @jboss.ejb-ref-jndi ref-name ="Comment" jndi-name = "Comment"
 * 
 * @ejb.ejb-ref ejb-name = "CommentFile" view-type = "remote"
 * @jboss.ejb-ref-jndi ref-name ="CommentFile" jndi-name = "CommentFile"
 * 
 * @ejb.ejb-ref ejb-name = "RegradeRequest" view-type = "remote"
 * @jboss.ejb-ref-jndi ref-name ="RegradeRequest" jndi-name = "RegradeRequest"
 */
public abstract class UserBean implements EntityBean {

	/*
	 * Do NOT set these fields to have default values upon instantiation. Since
	 * beans may not be newly created objects when ejbCreate is called we cannot
	 * rely on any of the values entered here.
	 */
	private String userID; // NetID

	private String firstName;

	private String lastName;

	private String CUID;

	private String college;
	
	private int domainID;
	
	private boolean pwExpired;
	
	private String email;
	
	private boolean isDeactivated;
	
	private byte[] pwHash;
	
	private byte[] pwSalt;

	private StudentLocalHome studentHome = null;

	private StaffLocalHome staffHome = null;

	private CMSAdminLocalHome cmsAdminHome = null;

	private CourseLocalHome courseHome = null;

	/**
	 * @return Returns the cUID.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public String getCUID() {
		return CUID;
	}

	/**
	 * @param cuid
	 *            The cUID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setCUID(String cuid) {
		CUID = cuid;
	}

	/**
	 * @return Returns the firstName.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the lastName.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the userID.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 * @param userID
	 *            The userID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setUserID(String userID) {
		this.userID = userID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return Returns the college.
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public String getCollege() {
		return college;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param college
	 *            The college to set.
	 */
	public void setCollege(String college) {
		this.college = college;
	}
	
	/**
	 * @return Returns the domainID.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public int getDomainID() {
		return domainID;
	}

	/**
	 * @param domainID
	 *            The domainID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setDomainID(int domainID) {
		this.domainID = domainID;
	}
	
	/**
	 * @return Returns the PWHash.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public byte[] getPWHash() {
		return pwHash;
	}

	/**
	 * @param PWHash
	 *            The PWHash to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setPWHash(byte[] PWHash) {
		this.pwHash = PWHash;
	}
	
	/**
	 * @return Returns the PWSalt.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public byte[] getPWSalt() {
		return pwSalt;
	}

	/**
	 * @param PWHash
	 *            The PWHash to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setPWSalt(byte[] PWSalt) {
		this.pwSalt = PWSalt;
	}

	/**
	 * @return Returns whether PW is Expired.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean isPWExpired() {
		return this.pwExpired;
	}

	/**
	 * @param PWExpired
	 *            Whether PW is Expired.
	 * @ejb.interface-method view-type="local"
	 */
	public void setPWExpired(boolean PWExpired) {
		this.pwExpired = PWExpired;
	}
	
	/**
	 * @return Returns the Email.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public String getEmail() {
		if(this.domainID != 1)
		{
			return this.email;
		}
		else
		{
			return this.userID + "@cornell.edu";
		}
	}

	/**
	 * @param Email
	 *            The Email to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @return Returns Whether User has been deactivated
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports"
	 * @jboss.method-attributes read-only="true"
	 */
	public boolean isDeactivated() {
		return isDeactivated;
	}

	/**
	 * @param isDeactivated
	 *            Whether User has been deactivated
	 * @ejb.interface-method view-type="local"
	 */
	public void setIsDeactivated(boolean isDeactivated) {
		this.isDeactivated = isDeactivated;
	}
	
	/**
	 * Returns the courses in which this user is a Student member
	 * 
	 * @return Collection of Course objects
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getStudentCourses() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = courseHome().findStudentCourses(getUserID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((CourseLocal) i.next()).getCourseData());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Returns the courses in which this user is a Staff member
	 * 
	 * @return Collection of CourseData objects
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getStaffCourses() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = courseHome().findStaffCourses(getUserID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((CourseLocal) i.next()).getCourseData());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Whether or not this user is a student in a course
	 * 
	 * @param courseID
	 *            ID of the course to check
	 * @return Whether or not user is a student
	 * @ejb.interface-method view-type="local"
	 */
	public boolean isStudent(long courseID) {
		try {
			StudentLocal s = studentHome().findByPrimaryKey(
					new StudentPK(courseID, getUserID()));
			return s.getStatus().equals(StudentBean.ENROLLED);
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * Whether or not this user is a staff member of a course
	 * 
	 * @param courseID
	 *            ID of the course to chekc
	 * @return Whether or not the user is a staff memebr
	 * @ejb.interface-method view-type="local"
	 */
	public boolean isStaff(long courseID) {
		try {
			StaffLocal s = staffHome().findByPrimaryKey(
					new StaffPK(courseID, getUserID()));
			return s.getStatus().equals(StaffBean.ACTIVE);
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * Whether or not this user is a CMS Admin (of entire system, i.e. domainID == 1)
	 * 
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public boolean isCMSAdmin() {
		try {
			CMSAdminLocal a = cmsAdminHome().findByPrimaryKey(new CMSAdminPK(getUserID()));
			return (a.getDomainID() == 1);
		} catch (Exception e) // admin not found or exception occurred
		{
			return false;
		}
	}
	
	/**
	 * Whether or not this user is a CMS Sub-Admin of any domain
	 * 
	 * @return 
	 * @ejb.interface-method view-type="local"
	 */
	public boolean isCMSSubAdmin() {
		try {
			cmsAdminHome().findByPrimaryKey(new CMSAdminPK(getUserID()));
			return true;
		} catch (Exception e) // admin not found or exception occurred
		{
			return false;
		}
	}
	
	/**
	 * Returns the DomainID that the user is a subAdmin for
	 * 
	 * @return -1 if user is not a subAdmin of any domain
	 * @ejb.interface-method view-type="local"
	 */
	public int getCMSSubAdminDomain() {
		try {
			CMSAdminLocal a = cmsAdminHome().findByPrimaryKey(new CMSAdminPK(getUserID()));
			return a.getDomainID();
		} catch (Exception e) // admin not found or exception occurred
		{
			return -1;
		}
	}

	/**
	 * Whether or not this user has assign priv
	 * 
	 * @param courseID
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public boolean isAssignPrivByCourseID(long courseID) {
		try {
			StaffLocal s = staffHome().findByUserCourse(getUserID(), courseID);
			return s.getStatus().equals(StaffBean.ACTIVE)
					&& (s.getAdminPriv() || s.getAssignmentsPriv());
		} catch (Exception e) {

		}
		return false;
	}

	/**
	 * Whether or not this user has group priv
	 * 
	 * @param courseID
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public boolean isGroupsPrivByCourseID(long courseID) {
		try {
			StaffLocal s = staffHome().findByUserCourse(getUserID(), courseID);
			return s.getStatus().equals(StaffBean.ACTIVE)
					&& (s.getAdminPriv() || s.getGroupsPriv());
		} catch (Exception e) {

		}
		return false;
	}

	/**
	 * Whether or not this user has grades priv
	 * 
	 * @param courseID
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public boolean isGradesPrivByCourseID(long courseID) {
		try {
			StaffLocal s = staffHome().findByUserCourse(getUserID(), courseID);
			return s.getStatus().equals(StaffBean.ACTIVE)
					&& (s.getAdminPriv() || s.getGradesPriv());
		} catch (Exception e) {

		}
		return false;
	}

	/**
	 * Whether or not this user has admin priv
	 * 
	 * @param courseID
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public boolean isAdminPrivByCourseID(long courseID) {
		try {
			StaffLocal s = staffHome().findByUserCourse(getUserID(), courseID);
			return s.getStatus().equals(StaffBean.ACTIVE) && s.getAdminPriv();
		} catch (Exception e) {

		}
		return false;
	}

	/**
	 * Whether or not this user has category priv
	 * 
	 * @param courseID
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public boolean isCategoryPrivByCourseID(long courseID) {
		try {
			StaffLocal s = staffHome().findByUserCourse(getUserID(), courseID);
			return s.getStatus().equals(StaffBean.ACTIVE)
					&& (s.getAdminPriv() || s.getCategoryPriv());
		} catch (Exception e) {

		}
		return false;
	}
	
	/**
	 * Check password
	 * 
	 * @param password
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public boolean checkPassword(String password) {
		try {
			if(this.domainID == 1)
			{
				return false;
			}
			byte[] inputPWHash = UserBean.hash(password, getPWSalt());
			return Arrays.equals(inputPWHash, getPWHash());
		} catch (Exception e) {

		}
		return false;
	}
	
	/**
	 * Resets password + salt
	 * 
	 * @return String
	 * @ejb.interface-method view-type="local"
	 */
	public String resetPassword()
	{
		String newPW = getRandomPW();
		changePassword(newPW);
		
		//flag the pw to be changed the next time the user logs in.
		setPWExpired(true); 
		
		return newPW;
	}
	
	/**
	 * Resets password + salt
	 * 
	 * @return String
	 * @ejb.interface-method view-type="local"
	 */
	public void changePassword(String newPW)
	{
		byte[] newSalt = getRandomSalt();
		byte[] newPWHash = hash(newPW,newSalt);
		
		setPWHash(newPWHash);
		setPWSalt(newSalt);
		
		//assume user changed password, and it is no longer expired.
		setPWExpired(false);
	}
	
	/*********************** Password Generation Helper Routines ***************************************/
	
	/**
	 * Gets a randomly generated PW
	 * @return
	 */
	private static String getRandomPW()
	{
		String charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~@#$%^*()_+-={}|][";
		String newPW = "";
		SecureRandom ranGen = new SecureRandom();
		
		while(newPW.length() < 11)
		{
			int randomIndex = ranGen.nextInt(charset.length());
			newPW += charset.charAt(randomIndex);
		}
		
		return newPW;
	}
	
	/**
	 * Gets a randomly generated Salt for PW.
	 * @return
	 */
	private static byte[] getRandomSalt()
	{
		SecureRandom ranGen = new SecureRandom();
		byte[] salt = new byte[8];
		ranGen.nextBytes(salt);
		return salt;
	}
	
	/**
	 * Hashes the given string with the given salt using SHA-256
	 */
	private static byte[] hash(String str, byte[] salt) {
		byte[] hash = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(salt);
			hash = md.digest(str.getBytes());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hash;
	}
	
	/*****************************************************************************/
	

	private StudentLocalHome studentHome() {
		try {
			if (studentHome == null) {
				studentHome = StudentUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return studentHome;
	}

	/**
	 * Finds the proper StaffHome and caches it for use
	 * 
	 * @return StaffHome
	 */
	private StaffLocalHome staffHome() {
		try {
			if (staffHome == null) {
				staffHome = StaffUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return staffHome;
	}

	private CMSAdminLocalHome cmsAdminHome() {
		try {
			if (cmsAdminHome == null) {
				cmsAdminHome = CMSAdminUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cmsAdminHome;
	}

	private CourseLocalHome courseHome() {
		try {
			if (courseHome == null) {
				courseHome = CourseUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return courseHome;
	}

	public UserPK ejbFindByPrimaryKey(UserPK pk) throws FinderException {
		return null;
	}

	public UserPK ejbFindByUserID(String userID) throws FinderException {
		return null;
	}

	public UserPK ejbFindByCUID(String cuid) throws FinderException {
		return null;
	}

	/**
	 * Find all students (but not staff) in the course and sort by NetID
	 */
	public Collection ejbFindByCourseID(long courseID) throws FinderException {
		return null;
	}

	/**
	 * Finds all active staff members of a course
	 * 
	 * @throws FinderException
	 */
	public Collection ejbFindStaffByCourseID(long courseID)
			throws FinderException {
		return null;
	}

	public Collection ejbFindActiveStudentsWithoutCUID() throws FinderException {
		return null;
	}

	/**
	 * Finds a collection of users who are active members in a set of groups
	 * 
	 * @param groupids
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupIDs(long[] groupids) throws FinderException {
		return null;
	}

	/**
	 * Finds the users corresponding to the members of the given group with and
	 * with the given status
	 * 
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupIDStatus(long groupID, String status)
			throws FinderException {
		return null;
	}

	public Collection ejbFindAll() throws FinderException {
		return null;
	}

	/**
	 * Finds all users who have blank first or last names
	 * 
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindMissingNames() throws FinderException {
		return null;
	}

	/**
	 * Returns a collection of users based on the given array of netids
	 * 
	 * @param netids
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByNetIDs(String[] netids) throws FinderException {
		return null;
	}
	
	/**
	 * Returns a collection of users in the given domain
	 * 
	 * @param domainID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByDomain(int domainID) throws FinderException {
		return null;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 */
	public UserData getUserData() {
		return new UserData(getCUID(), getFirstName(), getLastName(),
				getUserID(), getCollege(), getDomainID(), getPWHash(), getPWSalt(), isPWExpired(), getEmail(), isDeactivated());
	}

	/**
	 * 
	 * @ejb.create-method view-type="local"
	 * 
	 */
	public UserPK ejbCreate(String netid, String firstName, String lastName, int domainID, String email, byte[] PWHash, byte[] PWSalt)
			throws javax.ejb.CreateException {
		setUserID(netid);
		setFirstName(firstName);
		setLastName(lastName);
		setCollege(null);
		setCUID(null);
		setDomainID(domainID);
		setEmail(email);
		setPWExpired(false);
		setIsDeactivated(false);
		setPWHash(PWHash);
		setPWSalt(PWSalt);
		return null;
	}
}
