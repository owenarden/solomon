/*
 * Created on Aug 30, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.util;

import java.util.Vector;

/**
 * @author jfg32
 * 
 * A GradingException is thrown if there are conflicting changes to grades made
 * by different users.
 */
public class GradingException extends Exception {
	static final long serialVersionUID = 1L;

	private Vector conflicts;

	public GradingException(Vector conflicts) {
		this.conflicts = conflicts;
	}

	public Vector getConflicts() {
		return conflicts;
	}

}
