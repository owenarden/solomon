/*
 * Created on Mar 13, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.EntityBean;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

/**
 * @author Evan
 * 
 * @ejb.bean name="CMSAdmin"
 *	jndi-name="CMSAdminBean"
 *	type="BMP"
 *
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.CMSAdminDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.CMSAdminDAOImpl"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 ***/
public abstract class CMSAdminBean implements EntityBean {
	
    /* Do NOT set this fields to have a default value upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
    private String netID;
    private int domainID;
	
  /**
   * @param netID
	 * @return
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local"
   */
  public CMSAdminPK ejbCreate(String netID) throws javax.ejb.CreateException
	{
  	setNetID(netID);
  	setDomainID(1);
    return null;
  }
  
  /**
   * @param netID
   * @param domainID
	 * @return
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local"
   */
  public CMSAdminPK ejbCreate(String netID, int domainID) throws javax.ejb.CreateException
	{
  	setNetID(netID);
  	setDomainID(domainID);
    return null;
  }
  
  /**
	 * @ejb.interface-method view-type="local"
	 * Removes this bean's admin entry from the database
	 */
	public void ejbRemove() throws RemoveException
	{
	}
  
  /**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the admin's NetID
	 */
	public String getNetID()
	{
		return this.netID;
	}
	
	/**
	 * ejb.interface-method view-type="local"
	 * @param id Sets the admin's NetID
	 * @throws IllegalArgumentException if netID is null (since an admin's id must exist)
	 */
	public void setNetID(String netID) throws IllegalArgumentException
	{
		if(netID == null) throw new IllegalArgumentException("CMSAdminBean.setNetID(): passed null");
		this.netID = netID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the admin's domainID
	 */
	public int getDomainID()
	{
		return this.domainID;
	}
	
	/**
	 * ejb.interface-method view-type="local"
	 * @param id Sets the admin's domainID
	 */
	public void setDomainID(int domainID)
	{
		this.domainID = domainID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * Checks whether the given primary key can be found in the database
	 * @param pk The primary key being searched for
	 * @return The key passed in if it's found
	 * @throws FinderException
	 */
	public CMSAdminPK ejbFindByPrimaryKey(CMSAdminPK pk) throws FinderException
	{
		return null;
	}
	
	
	/**
	 * @ejb.interface-method view-type="local"
	 * Returns a Collection of all Full CMSAdmins in the database
	 * @throws FinderException
	 */
	public Collection ejbFindAllAdmins() throws FinderException
	{
		return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * Returns a Collection of all CMSSubAdmins in the database
	 * @throws FinderException
	 */
	public Collection ejbFindAllSubAdmins() throws FinderException
	{
		return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * Returns a data structure with this admin's NetID
	 */
	public CMSAdminData getCMSAdminData()
	{
		return new CMSAdminData(netID,domainID);
	}
}
