package edu.cornell.csuglab.cms.util.category;

/**
 * @author evan
 * 9 / 1 / 05
 *
 * Holds info representing an existing info of type text, to be edited
 */
public class CtgCurTextContentInfo extends CtgCurContentInfo
{
	private String text;
	
	public CtgCurTextContentInfo(long contentID, String text)
	{
		super(contentID);
		this.text = text;
	}
	
	public String getText()
	{
		return this.text;
	}
	
	public void setText(String text)
	{
		this.text = text;
	}
	
	public String toString()
	{
		return "CurTextContentInfo{text: " + getText() + "}";
	}
}
