/*
 * Created on Mar 18, 2004
 */

package edu.cornell.csuglab.cms.base;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.ejb.EJBException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.FinderException;

/**
 * @ejb.bean name="GroupMember"
 *	jndi-name="GroupMemberBean"
 *	type="BMP" 
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.GroupMemberDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.GroupMemberDAOImpl"
 * 
 * @ejb.ejb-ref ejb-name="Grade" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Grade" jndi-name="Grade"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 **/
public abstract class GroupMemberBean implements EntityBean {
	public static final String
		INVITED= "Invited",
		REJECTED= "Rejected",
		ACTIVE= "Active";
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long groupID;
	private String netID;
	private String status;
	
	protected EntityContext ctx;

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getGroupID() {
		return groupID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param groupID
	 */
	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getNetID() {
		return netID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param netID
	 */
	public void setNetID(String netID) {
		this.netID = netID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The status of the group member ('active','invited','rejected')
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param status The status of the group member ('active','invited','rejected')
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * Checks to see if the given primary key can be found in the database
	 * 
	 * @param pk
	 *            The primary key being serarched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public GroupMemberPK ejbFindByPrimaryKey(GroupMemberPK key)
			throws FinderException {
		return null;
	}


	/**
	 * Finds all the group members in a group
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupID(long groupID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all the group members in a group with the given status
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupIDStatus(long groupID, String status) throws FinderException {
		return null;
	}

	/**
	 * Finds all the active or invited group members in an assignment
	 * @param assignID
	 * @throws FinderException
	 */
	public Collection ejbFindNonRejectedByAssignmentID(long assignID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds only the active members in a group
	 * @param groupID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindActiveByGroupID(long groupID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds only the invited members of a group
	 * (Ones which have yet to accept the invitation)
	 * @param groupID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindInvitesByGroupID(long groupID) throws FinderException {
		return null;
	}
	
	/**
	 * Find the group member info for a given student (ie. what group he/she is
	 * in)
	 * 
	 * @param netID
	 *            The student's netID
	 * @return The collection of group members info
	 * @throws FinderException
	 */
	public Collection ejbFindByNetID(String netID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all the GroupMember entries for a user in a given assignment.
	 * @param netID The NetID of the user
	 * @param assignmentID The AssignmentID of the assignment
	 * @return Returns a Collection of GroupMembers
	 * @throws FinderException
	 */
	public Collection ejbFindByNetIDAssignmentID(String netID, long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all GroupMembers in a collection of groups which have been assigned
	 * to a given staff member in an assignment
	 * @param grader The NetID of the staff member whose permissions we're checking
	 * @param assignmentID The AssignmentID of the assignment
	 * @param groupids An array containing the groups to check
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupIDsAssignedTo(String grader, long assignmentID, Collection groupids) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all the Active GroupMembers in with the given NetIDs in
	 * the given assignment
	 * @param netids
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindActiveByNetIDsAssignmentID(Collection netids, long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all the active members of groups which the given user has been
	 * invited to join
	 * @param invitedNetID The student whose invitations we're looking for
	 * @param assignmentID The assignment we care about
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindInvitersByNetIDAssignmentID(String invitedNetID, long assignmentID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds the group in which this user is currently active for a given assignment.
	 * @param netID
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 */
	public GroupMemberPK ejbFindActiveByNetIDAssignmentID(String netID, long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all Active group member entries in the given course for group
	 * in which the given student is active.  This includes group member
	 * entries for the given NetID.
	 * @param netID
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByNetIDCourseID(String netID, long courseID) throws FinderException {
	    return null;
	}

	/**
	 * Finds all group members which have been invited to join
	 * the groups in which the students in netids are members
	 * in the given assignment
	 * @param netids A collection of NetIDs (strings)
	 * @param assignmentID The AssignmentID of the assignment to check
	 * @return
	 */
	public Collection ejbFindByInviters(Collection netids, long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all enrolled, active group members within an assignment
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindActiveByAssignmentID(long assignmentID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all enrolled, active group members in groups to which the given
	 * netID has been assigned to grade at least one non-hidden subproblem 
	 */
	public Collection ejbFindAssignedActiveByAssignmentID(String netID, long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 **/
	public GroupMemberData getGroupMemberData() {
		return new GroupMemberData(getGroupID(), getNetID(), getStatus());
	}
	
	/**
	 * Creates a new group member for the given group (and course and assignment)
	 * and student with the given status
	 * @param courseid The courseID
	 * @param assignmentid The assignmentID
	 * @param groupid The groupID
	 * @param netid The student's netID
	 * @param status The status of the group member ('active','invited','rejected')
	 * @return
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local" 
	 **/
	public GroupMemberPK ejbCreate(long groupid, String netid, String status)
			throws javax.ejb.CreateException {
		setGroupID(groupid);
		setNetID(netid);
		setStatus(status);
		return null;
	}
	
	public void setEntityContext(EntityContext ctx) throws EJBException, RemoteException {
		this.ctx = ctx;
	}
	
	public void unsetEntityContext() throws EJBException, RemoteException {
		this.ctx = null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @return
	 */
	public EntityContext getEntityContext() {
		return ctx;
	}
	
}
