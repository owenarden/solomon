/*
 * Created 5 / 5 / 05
 * @author Evan
 */

package edu.cornell.csuglab.cms.base;

import java.net.InetAddress;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;
import java.sql.Timestamp;

import javax.ejb.CreateException;
import javax.ejb.EntityBean;
import javax.ejb.FinderException;

import edu.cornell.csuglab.cms.log.LogDetail;
import edu.cornell.csuglab.cms.log.LogSearchParams;

/**
 * A Log is a high-level log in a two-level logging system. Log objects handle
 * logs that represent entire transaction action, e.g. "Assignment created for course Foo".
 * LogDetail objects handle low-level logs that represent individual database 
 * changes, such as "Assignment status set to Hidden". Both are used for every 
 * transaction for which they make sense.
 * 
 * Each Log consists of an entry in the tLogs table with the basic log information.
 * Each Log is also associated with a collection of LogDetails in the tLogDetails
 * table which can contain Details which describe exactly what changes happend during the transaction, 
 * NetIDs which have been affected by this log's action, and AssignmentIDs which
 * this action affected.
 * 
 * To create a log entry (In TransactionsBean):
 * 	1. A new log is created from a LogData object.  A new LogData object
 *     should be created using the startLog(Principal p) method in TransactionsBean,
 *     which will add information from the Principal to the LogData.
 *  2. LogDetails are added to the LogData as necessary using the methods
 *     appendDetail, appendAssignment, appendReceiver in TransactionsBean.
 *     Do not use the recNetIDs or assignmentIDs collections in the LogData to
 *     add receiving NetIDs or AssignmentIDs to the log.  Instead use appendReceiver
 * 	   or appendAssignment.
 *  3. Upon being loaded, LogDetails with null detail strings will not be added
 *     as LogDetails, but will instead be interpreted as a receiving NetID or an
 *     AssignmentID and added to the proper set.
 * 
 * Notes:  
 * 	- Any LogID entries in the LogData used for creation will be ignored
 * 		(The LogID is generated when the log is entered into the database)
 *  - Logs are read-only.  None of the setters are hooked up to make changes
 * 		to the database, so create-time is the only time to affect the log entry.
 * 
 * 
 * @see LogDetail
 * @see LogSearchParams
 *
 * @ejb.bean name="Log"
 *	jndi-name="Log"
 *	type="BMP"
 *
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.LogDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.LogDAOImpl"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 * 
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/
public abstract class LogBean implements EntityBean
{
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long logID;
	private String actingNetID;
	private String simulatedNetID;				//won't always apply
	private SortedSet receivingNetIDs;			
	private InetAddress actingIPAddress;
	private Timestamp time;
	private String logName;
	private long logType;
	private Long courseID;						//won't always apply
	private SortedSet assignmentIDs; 			//won't always apply
	private Vector detailLogs;					//rows in the detail-log table: LogDetail objects
	
	//legal high-level log types (| these to combine when searching)
	public static final long
		LOG_ADMIN = 1,
		LOG_COURSE = 2,
		LOG_GROUP = 4,
		LOG_GRADE = 8,
		LOG_ALL = 31,
		//this one is meant for use only by finder functions in LogDAOImpl
		MAX_INDIVIDUAL_LOG_TYPE = 16;
	
	public static final String
		// LOG TYPES (string format)
		ADMIN = "Admin",
		COURSE = "Course",
		GROUP = "Group",
		GRADE = "Grade",
		CATEGORY = "Content",
		// LEGAL LOG NAMES (corresponding to database transactions)
		// Admin Type
		ADD_CMS_ADMIN = "Added CMS Admin",
		CREATE_CMS_USER = "Added New CMS User",
		UPDATE_USER_PW = "Updated User PW",
		CREATE_COURSE = "Created New Course",
		CREATE_SEMESTER = "Created New Semester",
		EDIT_SEMESTER = "Edited Semester",
		REMOVE_CMS_ADMIN = "Removed CMS Admin",
		SET_CUIDS = "Set User CUIDs",
		SET_CURRENT_SEMESTER = "Set Current Semester",
		SET_HOST_GROUPS_URL = "Set Server-Semester Grouping URL",
		SET_USERNAMES = "Set User Names",
		UPLOAD_STUDENT_INFO = "Uploaded Student Information", //can also be of type COURSE
		UPLOAD_SECTION_MAP = "Uploaded CourseCode-Section-ClassNumber", //can also be of type COURSE
		// Course Type
		ADD_STUDENTS = "Added Students",
		CHANGE_GROUP_TIMESLOT = "Changed Group Timeslot",
		COMPUTE_ASSIGNMENT_STATS = "Computed Assignment Stats",
		COMPUTE_TOTAL_SCORES = "Computed Total Scores",
		CREATE_ANNOUNCEMENT = "Posted New Announcement",
		CREATE_ASSIGNMENT = "Created New Assignment",
		CREATE_TIMESLOTS = "Created New Timeslots",
		DROP_STUDENTS = "Dropped Students",
		EDIT_ANNOUNCEMENT = "Edited Announcement",
		EDIT_ASSIGNMENT = "Edited Assignment",
		EDIT_COURSE_PROPS = "Edited Course Properties",
		EDIT_STAFF_PREFS = "Edited Staff Preferences",
		EDIT_STUDENT_PREFS = "Edited Student Preferences",
		REMOVE_ASSIGNMENT = "Removed Assignment",
		REMOVE_TIMESLOTS = "Removed Timeslots",
		RESTORE_ANNOUNCEMENT = "Restored Announcement",
		RESTORE_ASSIGNMENT = "Restored Assignment",
		SEND_EMAIL = "Sent Course Email",
		UPLOAD_CLASSLIST = "Uploaded Classlist",
		// Category Type
		CREATE_CATEGORY = "Create New Content",
		EDIT_CATEGORY = "Edited Content",
		ADDNEDIT_CONTENTS = "Added/Edited Content Data",
		ORDER_CATEGORIES = "Reordered Content",
		REMOVE_CATEGORY = "Removed Content",
		REMOVE_CATEGORY_ROW = "Removed Content Row",
		// Grade Type
		ASSIGN_GRADER = "Assigned Grader",
		DOWNLOAD_ASSIGNMENT_GRADES = "Downloaded Assignment Grades",
		EDIT_GRADES = "Edited Grades/Comments",
		EDIT_FINAL_GRADES = "Edited Final Grades",
		UPLOAD_FINAL_GRADES = "Uploaded Final Grades File",
		UPLOAD_GRADES_FILE = "Uploaded Grades File",
		// Group Type
		ACCEPT_INVITATION = "Accepted Group Invite",
		CANCEL_INVITATION = "Canceled Group Invite",
		CREATE_GROUP = "Created New Group",
		CREATE_INVITATION = "Invited to Join Group",
		DISBAND_GROUP = "Disband Group",
		GRANT_EXTENSION = "Granted Extension",
		LEAVE_GROUP = "Left Group",
		REJECT_INVITATION = "Rejected Group Invite",
		REMOVE_EXTENSION = "Removed Extension",
		REQUEST_REGRADE = "Requested Regrade",
		SUBMIT_FILES = "Submitted Files",
		SUBMIT_SURVEY = "Submitted Survey"
		;
	public static final long LOG_CATEGORY = 16;
	public static String logTypeToString(long type) {
	    if (type == LOG_ADMIN) {
	        return ADMIN;
	    } else if (type == LOG_COURSE) {
	        return COURSE;
	    } else if (type == LOG_GROUP) {
	        return GROUP;
	    } else if (type == LOG_GRADE) {
	        return GRADE;
	    }
	    return CATEGORY;
	}
		
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The unique log ID
	 */
	public long getLogID() {
		return logID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param logID The unique log ID
	 */
	public void setLogID(long logID) {
		this.logID = logID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The NetID that initiated the action
	 */
	public String getActingNetID() {
		return actingNetID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param netID The NetID that initiated the action
	 */
	public void setActingNetID(String netID) {
		this.actingNetID = netID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getSimulatedNetID() {
	    return simulatedNetID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param simulatedNetID
	 */
	public void setSimulatedNetID(String simulatedNetID) {
	    this.simulatedNetID = simulatedNetID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return A Collection of NetIDs (Strings)
	 */
	public SortedSet getReceivingNetIDs() {
		return receivingNetIDs;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param netIDs A Collection of NetIDs (Strings)
	 */
	public void setReceivingNetIDs(Collection netIDs) {
	    if (netIDs != null) {
	        if (this.receivingNetIDs == null) {
	            this.receivingNetIDs = new TreeSet(netIDs);
	        } else {
	            this.receivingNetIDs.addAll(netIDs);
	        }
	    }
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The acting IP address
	 */
	public InetAddress getActingIPAddress() {
		return actingIPAddress;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param ip The IP address of the acting host
	 */
	public void setActingIPAddress(InetAddress ip) {
		this.actingIPAddress = ip;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The date of the log entry
	 */
	public Timestamp getTimestamp() {
		return time;
	}

	/**
	 * @param t The time this log was created
	 */
	public void setTimestamp(Timestamp t) {
		this.time = t;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The log name
	 */
	public String getLogName() {
	    return logName;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param logName
	 */
	public void setLogName(String logName) {
	    this.logName = logName;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The log type, one of those listed above
	 */
	public long getLogType() {
		return logType;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param type The log type, one of those listed above
	 */
	public void setLogType(long type) {
		this.logType = type;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The course ID (null if course ID isn't applicable)
	 */
	public Long getCourseID() {
		return courseID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param courseID The course ID (null if course ID isn't applicable)
	 */
	public void setCourseID(Long courseID) {
		this.courseID = courseID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param courseID
	 */
	public void setCourseID(long courseID) {
	    this.courseID = new Long(courseID);
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return The assignment ID (null if assignment ID isn't applicable)
	 */
	public SortedSet getAssignmentIDs() {
	    if (this.assignmentIDs == null) {
	        this.assignmentIDs = new TreeSet();
	    }
		return this.assignmentIDs;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param assignID The assignment ID (null if assignment ID isn't applicable)
	 */
	public void addAssignmentID(long assignID) {
	    if (this.assignmentIDs == null) {
	        this.assignmentIDs = new TreeSet();
	    }
		this.assignmentIDs.add(new Long(assignID));
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return A Collection of LogDetail objects with specific info on the
	 * transactions covered by this high-level log
	 */
	public Collection getDetailLogs() {
		return detailLogs;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param details A Collection of LogDetail objects with specific info on the
	 * transactions covered by this high-level log
	 */
	public void setDetailLogs(Collection detailLogs)
	{
		this.detailLogs = new Vector();
		if (detailLogs != null) {
		    this.detailLogs.addAll(detailLogs);
		}
	}

	/**
	 * Checks to see if the given primary key can be found in the database
	 * @param key The primary key being searched for
	 * @return The key passed in if it is found
	 * @throws FinderException if the key isn't found
	 */
	public LogPK ejbFindByPrimaryKey(LogPK key) throws FinderException
	{
		return null;
	}
	
	/**
	 * THE log search function. If any parameter is null, it's ignored.
	 * @param params A LogSearchParams object completely specifying the
	 * selected search parameters. Any of its getters returning null
	 * signify a lack of preference.
	 * @return A Collection of Logs (which include their LogDetails)
	 * @throws FinderException If the database query went awry
	 */
	public Collection ejbFind(LogSearchParams params) throws FinderException
	{
		return null;
	}
	
	/* Here should be finder functions for use with a recent-events feature.
	 * These finders should look for things such as the following:
	 * - new assignment
	 *   - asgn description changed
	 *   - asgn source released
	 *   - asgn status changed to Graded
	 * - assignment solutions released
	 * - student's grade changed
	 * - comment was given by staff
	 * - student's group changed
	 *   - (a) member(s) left or joined
	 * TODO write finders
	 */
	
	/**
	 * Find comment and regrade request listings for display on the detailed grading page.
	 * @param studentNetID
	 * @param assignmentID
	 * @return A Collection of Log objects
	 * @throws FinderException
	 */
	public Collection ejbFindCommentsNRegrades(String studentNetID, long assignmentID) throws FinderException
	{
		return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local" 
	 **/
	public LogData getLogData() {
		return new LogData(getLogID(), getActingNetID(), getSimulatedNetID(), getReceivingNetIDs(), getActingIPAddress(), 
				getTimestamp(), getLogName(), getLogType(), getCourseID(), getAssignmentIDs(),
				getDetailLogs());
	}
	
	/**
	 * Creates a new high-level log in the database, and any associated low-level logs
	 * requested by means of the detailLogs parameter.
	 * @param logType The category of the action being logged, one of those
	 * listed in LogBean
	 * @param actingNetID The NetID of the user who requested this action
	 * @param receivingNetIDs The NetIDs of all users affected by this action,
	 * separated by whitespace
	 * @param receivingGroupID The group ID of the group directly affected by this action, if any
 	 * @param actingIPAddress The IP address from which this action was requested
	 * @param courseID The course ID of the course associated with this action, if any
	 * @param assignmentID The assignment ID of the assignment associated with this action, if any
	 * @param detailLogs Detailed information about specific database changes encompassed by
	 * the transaction represented by this Log
	 * @return A new Log entry
	 * @throws CreateException
	 * @ejb.create-method view-type="local"
	 */
	public LogPK ejbCreate(LogData log) throws CreateException {
	    setActingNetID(log.getActingNetID());
	    setSimulatedNetID(log.getSimulatedNetID());
	    setReceivingNetIDs(log.getReceivingNetIDs());
	    setActingIPAddress(log.getActingIPAddress());
	    setTimestamp(new Timestamp(System.currentTimeMillis()));
	    setLogName(log.getLogName());
	    setLogType(log.getLogType());
	    setCourseID(log.getCourseID());
	    setDetailLogs(log.getDetailLogs());
	    this.receivingNetIDs = new TreeSet();
	    this.assignmentIDs = new TreeSet();
	    return null;
	}
}
