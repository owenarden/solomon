/*
 * Created on Mar 12, 2005
 */
package edu.cornell.csuglab.cms.util.category;

/**
 * @author yc263
 *
 * Holds info on a single category for the course category general options page:
 * category order and whether hidden.
 * Used by CategoriesOption.
 */
public class CategoryInfo {
	long categoryID;
	int positn;
	boolean hidden;
	
	public CategoryInfo(long categoryID){
		this.categoryID = categoryID;
	}
	
	public long getCategoryID(){
		return this.categoryID;
	}
	public void setCtgPositn(int positn){
		this.positn = positn;
	}
	public int getPositn(){
		return this.positn;
	}
	public void setHidden(boolean hidden){
		this.hidden = hidden;
	}
	public boolean getHidden(){
		return this.hidden;
	}
}
