/*
 * Created on Oct 28, 2004
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import javax.ejb.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

import edu.cornell.csuglab.cms.base.*;
import edu.cornell.csuglab.cms.www.util.FileUtil;

/**
 * @author Jon
 *
 */
public class SolutionFileDAOImpl extends DAOMaster implements SolutionFileDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SolutionFileDAO#load(edu.cornell.csuglab.cms.base.SolutionFilePK, edu.cornell.csuglab.cms.base.SolutionFileBean)
	 */
	public void load(SolutionFilePK pk, SolutionFileBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tSolutionFiles "
					+ "where SolutionFileID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getSolutionFileID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setAssignmentID(rs.getLong("assignmentid"));
				ejb.setSolutionFileID(rs.getLong("SolutionFileID"));
				ejb.setFileName(rs.getString("filename"));
				ejb.setHidden(rs.getBoolean("hidden"));
				ejb.setPath(FileUtil.translateDBPath(rs.getString("Path")));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SolutionFileDAO#store(edu.cornell.csuglab.cms.base.SolutionFileBean)
	 */
	public void store(SolutionFileBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tSolutionFiles "
					+ "set hidden = ?, FileName = ?, Path = ? where SolutionFileID = ?";
			ps = conn.prepareStatement(query);
			ps.setBoolean(1, ejb.getHidden());
			if(ejb.getFileName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getFileName());
			if(ejb.getPath() == null) ps.setNull(3, java.sql.Types.VARCHAR);
			else ps.setString(3, FileUtil.translateSysPath(ejb.getPath()));
			ps.setLong(4, ejb.getSolutionFileID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SolutionFileDAO#remove(edu.cornell.csuglab.cms.base.SolutionFilePK)
	 */
	public void remove(SolutionFilePK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SolutionFileDAO#create(edu.cornell.csuglab.cms.base.SolutionFileBean)
	 */
	public SolutionFilePK create(SolutionFileBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		SolutionFilePK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createQuery = "insert into tSolutionFiles (AssignmentID, FileName, Hidden, Path) " +
			    	"values (?, ?, ?, ?)";
			String findQuery = "select @@identity as 'SolutionFileID' from tSolutionFiles";
			ps = conn.prepareStatement(createQuery);
			ps.setLong(1, ejb.getAssignmentID());
			if(ejb.getFileName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getFileName());
			ps.setBoolean(3, false);
			if(ejb.getPath() == null) ps.setNull(4, java.sql.Types.VARCHAR);
			else ps.setString(4, FileUtil.translateSysPath(ejb.getPath()));
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findQuery).executeQuery();
			if (count > 0 && rs.next()) {
				long solutionFileID = rs.getLong("SolutionFileID");
				ejb.setSolutionFileID(solutionFileID);
				result = new SolutionFilePK(solutionFileID);
			}
			else {
				throw new CreateException("Failed to create new SolutionFile");
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SolutionFileDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.SolutionFilePK)
	 */
	public SolutionFilePK findByPrimaryKey(SolutionFilePK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select SolutionFileID from tSolutionFiles "
					+ "where SolutionFileID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, pk.getSolutionFileID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find solution file with " +
						"SolutionFileID = " + pk.getSolutionFileID());
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SolutionFileDAO#findByAssignmentID(long)
	 */
	public SolutionFilePK findByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		SolutionFilePK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select SolutionFileID from tSolutionFiles "
					+ "where AssignmentID = ? and hidden = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentID);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = new SolutionFilePK(rs.getLong("SolutionFileID"));
				
			} else {
				throw new FinderException("Failed to find a Solution File for " +
						"AssignmentID = " + assignmentID);
			}
			if (rs.next()) {
				throw new FinderException("Found more than one Solution File for " +
						"AssignmentID = " + assignmentID);
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SolutionFileDAO#findHiddenByAssignmentID(long)
	 */
	public Collection findHiddenByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select SolutionFileID from tSolutionFiles "
					+ "where AssignmentID = ? and hidden = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentID);
			ps.setBoolean(2, true);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new SolutionFilePK(rs.getLong("SolutionFileID")));	
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
}
