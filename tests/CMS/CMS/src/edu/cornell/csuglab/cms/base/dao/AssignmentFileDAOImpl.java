/*
 * Created on Apr 15, 2004
 */

package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;


import edu.cornell.csuglab.cms.base.AssignmentFileBean;
import edu.cornell.csuglab.cms.base.AssignmentFileDAO;
import edu.cornell.csuglab.cms.base.AssignmentFilePK;
import edu.cornell.csuglab.cms.www.util.FileUtil;

/**
 * @author Theodore Chao
 */
public class AssignmentFileDAOImpl extends DAOMaster implements AssignmentFileDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AssignmentFileDAO#load(edu.cornell.csuglab.cms.base.AssignmentFilePK,
	 *      edu.cornell.csuglab.cms.base.AssignmentFileBean)
	 */
	public void load(AssignmentFilePK pk, AssignmentFileBean ejb)
			throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tAssignmentFiles "
					+ "where AssignmentFileID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getAssignmentFileID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setAssignmentFileID(rs.getLong("AssignmentFileID"));
				ejb.setAssignmentItemID(rs.getLong("AssignmentItemID"));
				ejb.setFileName(rs.getString("FileName"));
				ejb.setFileDate(rs.getTimestamp("FileDate"));
				ejb.setHidden(rs.getBoolean("Hidden"));
				ejb.setPath(FileUtil.translateDBPath(rs.getString("Path")));
			}
			conn.close();
			rs.close();
			ps.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AssignmentFileDAO#store(edu.cornell.csuglab.cms.base.AssignmentFileBean)
	 */
	public void store(AssignmentFileBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tAssignmentFiles "
					+ "set hidden = ?, FileDate = ? where AssignmentFileID = ?";
			ps = conn.prepareStatement(query);
			ps.setBoolean(1, ejb.getHidden());
			ps.setTimestamp(2, ejb.getFileDate());
			ps.setLong(3, ejb.getAssignmentFileID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AssignmentFileDAO#remove(edu.cornell.csuglab.cms.base.AssignmentFilePK)
	 */
	public void remove(AssignmentFilePK pk) throws RemoveException,
			EJBException {
		// TODO Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AssignmentFileDAO#create(edu.cornell.csuglab.cms.base.AssignmentFileBean)
	 */
	public AssignmentFilePK create(AssignmentFileBean ejb)
			throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AssignmentFilePK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createQuery = "insert into tAssignmentFiles (AssignmentItemID, FileName, FileDate, Hidden, Path) values (?, ?, ?, ?, ?)";
			String findQuery = "select @@identity as 'AssignmentFileID' from tAssignmentFiles";
			ps = conn.prepareStatement(createQuery);
			ps.setLong(1, ejb.getAssignmentItemID());
			if(ejb.getFileName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getFileName());
			ps.setTimestamp(3, ejb.getFileDate());
			ps.setBoolean(4, false);
			if(ejb.getPath() == null) ps.setNull(5, java.sql.Types.VARCHAR);
			else ps.setString(5, FileUtil.translateSysPath(ejb.getPath()));
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findQuery).executeQuery();
			if (count > 0 && rs.next()) {
				long assignmentFileID = rs.getLong("AssignmentFileID");
				ejb.setAssignmentFileID(assignmentFileID);
				result = new AssignmentFilePK(assignmentFileID);
			}
			else {
				throw new CreateException("Failed to create new AssignmentFile");
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AssignmentFileDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.AssignmentFilePK)
	 */
	public AssignmentFilePK findByPrimaryKey(AssignmentFilePK key)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select AssignmentFileID from tAssignmentFiles "
					+ "where AssignmentFileID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, key.getAssignmentFileID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find assignment file with " +
						"AssignmentFileID = " + key.getAssignmentFileID());
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AssignmentFileDAO#findByCourseID(long)
	 *
	public Collection findByCourseID(long courseid) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select AssignmentFileID from tAssignmentFiles "
					+ "where courseid = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseid);
			rs = ps.executeQuery();
			while (rs.next()) {
				AssignmentFilePK pk = new AssignmentFilePK(rs.getLong(1));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}*/

	public Collection findByAssignmentID(long assignmentid) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select AssignmentFileID from tAssignmentFiles "
					+ "where assignmentid = ? and hidden = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentid);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				AssignmentFilePK pk = new AssignmentFilePK(rs.getLong(1));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AssignmentFileDAO#findByAssignmentID(long)
	 */
	public AssignmentFilePK findByAssignmentItemID(long assignmentItemID)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AssignmentFilePK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select AssignmentFileID from tAssignmentFiles where " +
				"AssignmentItemID = ? and Hidden = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentItemID);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = new AssignmentFilePK(rs.getLong("AssignmentFileID"));
			} else {
				throw new FinderException("Failed to find assignment file for this item");
			}
			if (rs.next()) {
				throw new FinderException("More than one file found for assignment item");
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentFileDAO#findHiddenByAssignmentID(long)
	 */
	public Collection findHiddenByAssignmentItemID(long assignmentItemID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select AssignmentFileID from tAssignmentFiles where " +
				"AssignmentItemID = ? and Hidden = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentItemID);
			ps.setBoolean(2, true);
			rs = ps.executeQuery();
			while (rs.next()) {
				AssignmentFilePK next = new AssignmentFilePK(rs.getLong("AssignmentFileID"));
				result.add(next);
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
}
