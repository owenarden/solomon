
package edu.cornell.csuglab.cms.base;
/*
 * Created on Mar 3, 2004 To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.EJBException;
import javax.ejb.EntityBean;
import javax.ejb.FinderException;
import javax.naming.NamingException;

/**
 * @ejb.bean name="Assignment"
 *	jndi-name="AssignmentBean"
 *	type="BMP" 
 *
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.AssignmentDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.AssignmentDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 * @ejb.ejb-ref ejb-name="AssignmentFile" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="AssignmentFile" jndi-name="AssignmentFile"
 *  
 * @ejb.ejb-ref ejb-name="Comment" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Comment" jndi-name="Comment"
 * 
 **/
public abstract class AssignmentBean implements EntityBean {
	public static final String OPEN = "Open";
	public static final String CLOSED = "Closed";
	public static final String HIDDEN = "Hidden";
	public static final String GRADED = "Graded";
	
	public static final int ASSIGNMENT = 0;
	public static final int SURVEY = 1;
	public static final int QUIZ = 2;
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long assignmentID;
	private long courseID;
	private String name, nameShort;
	private String description;
	private String status;
	private float weight, maxScore;
	private boolean studentRegrades;
	private int gracePeriod; //in minutes
	private int numassignedfiles;
	//note dueDate does NOT include grace period
	private Timestamp dueDate, regradeDeadline, lateDeadline;
	private boolean showStats, showSolution, assignedGraders,
			allowLate, assignedGroups, hidden;
	private Float mean, max, median, stdDev;
	private int groupSizeMax, groupSizeMin;
	private int type;
	
	//timeslot info
	private boolean scheduled; //enable/disable scheduling
	private Integer groupLimit;
	private Long duration;
	private Timestamp timeslotLockTime; //time after which *students* can't change assigned timeslots (null = no deadline)
	
	private String defaultLatePenalty;
	
	private RequiredSubmissionLocalHome requiredSubmissionHome = null;
	private CommentLocalHome commentHome = null;
	private AssignmentItemLocalHome assignmentItemHome = null;
	private GroupLocalHome groupHome= null;
	private SolutionFileLocalHome solutionFileHome= null;
	private SubProblemLocalHome subProblemHome= null;
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the assignmentID
	 */
	public long getAssignmentID() {
		return assignmentID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param assignmentID The assignment ID to set this to
	 */
	public void setAssignmentID(long ID) {
		this.assignmentID = ID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the courseID
	 */
	public long getCourseID() {
		return courseID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param courseID The course ID to set this assignment to
	 */
	public void setCourseID(long courseID) {
		this.courseID = courseID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the full name of the assignment 
	 */
	public String getName() {
		return name;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param name The name to call this assignment
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the short name of the assignment
	 */
	public String getNameShort() {
		return nameShort;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param nameShort The short name to give this assignment
	 */
	public void setNameShort(String nameShort) {
		this.nameShort = nameShort;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the current status of the assignment: 
	 * OPEN, HIDDEN, CLOSED, or GRADED (un-capitalized)
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param status The status to set this assignment to: 
	 * OPEN, HIDDEN, CLOSED, or GRADED (un-capitalized)
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the weight of this assignment
	 */
	public float getWeight() {
		return weight;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param weight The weight to set this assignment to
	 */
	public void setWeight(float weight) {
		this.weight = weight;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the max score of this assignment
	 */
	public float getMaxScore() {
		return maxScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param maxScore The max score of this assignment
	 */
	public void setMaxScore(float maxScore) {
		this.maxScore = maxScore;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public boolean getStudentRegrades() {
		return studentRegrades;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param studentRegrades
	 */
	public void setStudentRegrades(boolean studentRegrades) {
		this.studentRegrades = studentRegrades;
	}

	/**
	 * Get the period after the due date during which students can still 
	 * submit and not be marked late (measured in minutes).
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the grace period.
	 */
	public int getGracePeriod() {
		return gracePeriod;
	}

	/**
	 * Set the period after the due date during which students can still 
	 * submit and not be marked late (measured in minutes).
	 * @ejb.interface-method view-type="local"
	 * @param graceperiod The grace period to set.
	 */
	public void setGracePeriod(int gracePeriod) {
		this.gracePeriod = gracePeriod;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The due date of the assignment
	 */
	public Timestamp getDueDate() {
		return dueDate;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param dueDate The due date to set
	 */
	public void setDueDate(Timestamp dueDate) {
		this.dueDate = dueDate;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the deadline for submitting regrades for the assignment
	 */
	public Timestamp getRegradeDeadline() {
		return regradeDeadline;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param regradeDeadline The Date to which to set the regrade's deadline to
	 */
	public void setRegradeDeadline(Timestamp regradeDeadline) {
		this.regradeDeadline = regradeDeadline;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the final deadline for late submissions
	 */
	public Timestamp getLateDeadline() {
		return lateDeadline;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param lateDeadline The Date to which to set the late deadline to
	 */
	public void setLateDeadline(Timestamp lateDeadline) {
		this.lateDeadline = lateDeadline;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns whether late submissions are allowed or not.
	 */
	public boolean getAllowLate() {
		return allowLate;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param allowLate Whether to allow late submissions (true) or not (false).
	 */
	public void setAllowLate(boolean allowLate) {
		this.allowLate = allowLate;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns whether or not to show assignment statistics on the 
	 * student side; true = show statistics, false = hide statistics
	 */
	public boolean getShowStats() {
		return showStats;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param showStats true to display assignment statistics on the student side 
	 * after status has been changed to "graded", false to hide them
	 */
	public void setShowStats(boolean showStats) {
		this.showStats = showStats;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns whether or not to always display solutions whenever
	 * the assignment status is Closed or Graded
	 */
	public boolean getShowSolution() {
	    return showSolution;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param showSolution
	 */
	public void setShowSolution(boolean showSolution) {
	    this.showSolution = showSolution;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public int getNumOfAssignedFiles() {
		return numassignedfiles;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param numassignedfiles
	 */
	public void setNumOfAssignedFiles(int numassignedfiles) {
		this.numassignedfiles = numassignedfiles;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public Long getDuration() {
		return duration;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param durationNN
	 */
	public void setDurationNN(long durationNN) {
		this.duration = new Long(durationNN);
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param duration
	 */
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public Timestamp getTimeslotLockTime()
	{
		return timeslotLockTime;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 */
	public void setTimeslotLockTime(Timestamp timeslotLockTime)
	{
		this.timeslotLockTime = timeslotLockTime;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the mean score.
	 */
	public Float getMean() {
		return mean;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param mean The mean score to set.
	 */
	public void setMean(Float mean) {
		this.mean = mean;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the max score.
	 */
	public Float getMax() {
		return max;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param max The max score to set.
	 */
	public void setMax(Float max) {
		this.max = max;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the min score.
	 */
	public Float getMedian() {
		return median;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param min The min score to set.
	 */
	public void setMedian(Float median) {
		this.median = median;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the stdDev.
	 */
	public Float getStdDev() {
		return stdDev;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param stdDev The stdDev to set.
	 */
	public void setStdDev(Float stdDev) {
		this.stdDev = stdDev;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns whether or not graders will be assigned to specific 
	 * groups and/or subproblems
	 */
	public boolean getAssignedGraders() {
		return assignedGraders;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param assignedGraders true to force specific assignment of graders 
	 * to groups and subproblems, false to allow any grader to grade any 
	 * group and subproblem.
	 */
	public void setAssignedGraders(boolean assignedGraders) {
		this.assignedGraders = assignedGraders;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public boolean getAssignedGroups() {
		return assignedGroups;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param assignedGroups
	 */
	public void setAssignedGroups(boolean assignedGroups) {
		this.assignedGroups = assignedGroups;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the maximum size for groups for this assignment
	 */
	public int getGroupSizeMax() {
		return groupSizeMax;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param groupSizeMax The number specifying the maximum group size
	 */
	public void setGroupSizeMax(int groupSizeMax) {
		this.groupSizeMax = groupSizeMax;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the maximum size for presenting groups for this assignment
	 */
	public Integer getGroupLimit() {
		return groupLimit;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param groupLimitNN The number specifying the maximum number of presenters
	 */
	public void setGroupLimitNN(int groupLimitNN) {
		this.groupLimit = new Integer(groupLimitNN);
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param groupLimit The number specifying the maximum number of presenters
	 */
	public void setGroupLimit(Integer groupLimit) {
		this.groupLimit = groupLimit;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the minimum size for groups for this assignment
	 */
	public int getGroupSizeMin() {
		return groupSizeMin;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param groupSizeMin The number specifying the minimum group size
	 */
	public void setGroupSizeMin(int groupSizeMin) {
		this.groupSizeMin = groupSizeMin;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public boolean getHidden() {
	    return hidden;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param hidden
	 */
	public void setHidden(boolean hidden) {
	    this.hidden = hidden;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Whether the assignment has attached timeslots
	 */
	public boolean getScheduled() {
	    return scheduled;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param scheduled
	 */
	public void setScheduled(boolean scheduled) {
	    this.scheduled = scheduled;
	}
	
	/**
	 * Checks to see if the given primary key can be found in the database
	 * @param pk The primary key being serarched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public AssignmentPK ejbFindByPrimaryKey(AssignmentPK pk)
			throws FinderException {
		return null;
	}
	
	/**
	 * Finds the information for the given assignmentID
	 * @param assignmentID The assignments unique ID
	 * @return The Assignment object for the given assignmentID
	 * @throws FinderException
	 */
	public AssignmentPK ejbFindByAssignmentID(long assignmentID)
			throws FinderException {
		return null;
	}
	
	/**
	 * Finds the assignment which is associated with the given group
	 * @param groupID
	 * @return
	 */
	public AssignmentPK ejbFindByGroupID(long groupID) throws FinderException {
	    return null;
	}
	
	/**
	 * Returns all *open* assignments due between now and the given deadline
	 * @param deadline The date and time before which to search
	 * @return A Collection of AssignmentPK objects
	 * @throws FinderException
	 */
	public Collection ejbFindOpenAsgnsByDeadline(Timestamp deadline) throws FinderException
	{
		return null;
	}
	
	/**
	 * Finds all (non-hidden) assignments in courses for which the given person is a 
	 * full privilege admin
	 * @param semesterID The semester to restrict the search to
	 * @param netID The NetID of the person to search for
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseAdmin(long semesterID, String netID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all (non-hidden) assignments in courses in the current semester
	 * @param semesterID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindBySemesterID(long semesterID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all (non-hidden) assignments with the current type
	 * @param type
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByType(int type) throws FinderException {
	    return null;
	}
	
	public Collection ejbFindGrouplessAssignments(long courseID, String studentNetID) throws FinderException {
		return null;
	}
	
	/**
	 * Returns all the source files associated with this assignment
	 * @return Collection of AssignmentFileDatas
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getAssignmentItems() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = assignmentItemHome().findByAssignmentID(getAssignmentID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((AssignmentItemLocal) i.next()).getAssignmentItemData());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Returns all the source files associated with this assignment
	 * @return Collection of AssignmentFileDatas
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getHiddenAssignmentItems() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = assignmentItemHome().findHiddenByAssignmentID(getAssignmentID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((AssignmentItemLocal) i.next()).getAssignmentItemData());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * @return
	 * @throws EJBException
	 * @ejb.interface-method view-type="local"
	 */
	public boolean hasSolutionFile() throws EJBException {
		try {
			solutionFileHome().findByAssignmentID(getAssignmentID());
		} catch (FinderException e) {
			return false;
		} catch (Exception e) {
			throw new EJBException(e);
		}
		return true;
	}
	
	/**
	 * @return
	 * @throws EJBException
	 * @ejb.interface-method view-type="local"
	 */
	public SolutionFileLocal getSolutionFile() throws EJBException {
		try {
			return solutionFileHome().findByAssignmentID(getAssignmentID());
		}
		catch (Exception e) {
			throw new EJBException(e);
		}
	}

	/**
	 * Returns all the files required for submission of this assignment
	 * @return Collection of RequiredSubmissionDatas
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getRequiredSubmissions() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = requiredSubmissionHome().findByAssignmentID(getAssignmentID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((RequiredSubmissionLocal)i.next()).getRequiredSubmissionData());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Returns all hidden required submissions
	 * @return Collection of required submissions with true "hidden" fields
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getHiddenRequiredSubmissions() {
	    Collection c;
	    Collection result= new ArrayList();
	    try {
	        c= requiredSubmissionHome().findHiddenByAssignmentID(getAssignmentID());
	        Iterator i= c.iterator();
	        while (i.hasNext()) {
	            result.add(((RequiredSubmissionLocal)i.next()).getRequiredSubmissionData());
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}
	
	/**
	 * Returns all hidden solution files
	 * @return Collection of hidden solutions files
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getHiddenSolutionFiles() {
	    Collection c;
	    Collection result= new ArrayList();
	    try {
	        c= solutionFileHome().findHiddenByAssignmentID(getAssignmentID());
	        Iterator i= c.iterator();
	        while (i.hasNext()) {
	            result.add(((SolutionFileLocal)i.next()).getSolutionFileData());
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}
	
	/**
	 * 
	 * @return Returns all the SubProblems for this assignment which are not hidden
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getSubProblems() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = subProblemHome().findByAssignmentID(getAssignmentID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((SubProblemLocal) i.next()).getSubProblemData());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * @return Returns all the SubProblems for this assignment which are not hidden 
	 * in a hashmap structure where the key is the subproblem's ID
	 * @ejb.interface-method view-type="local"
	 */
	public HashMap getSubProblemsMap(){
		Collection c;
		HashMap map = new HashMap();
		try {
			c = subProblemHome().findByAssignmentID(getAssignmentID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				SubProblemData subP = ((SubProblemLocal)i.next()).getSubProblemData();
				map.put(new Long(subP.getSubProblemID()), subP);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public boolean hasSubProblems() {
		try {
			return subProblemHome().findByAssignmentID(getAssignmentID()).size() > 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 
	 * @return Returns all the hidden SubProblems for this assignment
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getHiddenSubProblems() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = subProblemHome().findHiddenByAssignmentID(getAssignmentID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((SubProblemLocal) i.next()).getSubProblemData());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public int getType()
	{
		return type;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 */
	public void setType(int type)
	{
		this.type= type;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getDefaultLatePenalty()
	{
		return defaultLatePenalty;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 */
	public void setDefaultLatePenalty(String defaultLatePenalty)
	{
		this.defaultLatePenalty= defaultLatePenalty;
	}
	
	
	/**
	 * Finds and caches the proper AssignmentFileHome
	 * @return AssignmentFileHome
	 */
	private RequiredSubmissionLocalHome requiredSubmissionHome() {
		try {
			if (requiredSubmissionHome == null) {
				requiredSubmissionHome = RequiredSubmissionUtil.getLocalHome();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return requiredSubmissionHome;
	}
	
	/**
	 * Finds and caches the proper CommentHome
	 * @return CommentHome
	 */
	private CommentLocalHome commentHome() {
		try {
			if (commentHome == null) {
				commentHome = CommentUtil.getLocalHome();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return commentHome;
	}
	
	private AssignmentItemLocalHome assignmentItemHome() throws NamingException {
		if (assignmentItemHome == null) {
			assignmentItemHome = AssignmentItemUtil.getLocalHome();
		}
		return assignmentItemHome;
	}
	
	private GroupLocalHome groupHome() {
		try {
			if (groupHome == null) {
				groupHome= GroupUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return groupHome;
	}
	
	private SolutionFileLocalHome solutionFileHome() {
	    try {
	        if (solutionFileHome == null) {
	            solutionFileHome= SolutionFileUtil.getLocalHome();
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return solutionFileHome;
	}
	
	private SubProblemLocalHome subProblemHome() {
		try {
			if (subProblemHome == null) {
				subProblemHome = SubProblemUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return subProblemHome;
	}

	/**
	 * Finds all the non-hidden assignments for the given course
	 * @param courseID The courseID
	 * @return The collection of assignments in increasing order of assignmentID.
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseID(long courseID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all (including hidden) assignments for the given course
	 * (and by hidden we mean 'removed', not 'set to Hidden status')
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindAllByCourseID(long courseID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all hidden assignments in a course
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindHiddenByCourseID(long courseID) throws FinderException {
	    return null;
	}

	/**
	 * Finds all assignments within the current semester in all courses that a given student is enrolled in
	 * with a due date later than the given date (in other words, assignments
	 * that are due in the future).
	 * @param netID The student's netID
	 * @param current The earliest due date for which to return results
	 * @return The collection of assignments in increasing order of assignmentID.
	 * @throws FinderException
	 */
	public Collection ejbFindByDateNetID(String netID, Timestamp current)
			throws FinderException {
		return null;
	}

	public Collection ejbFindByDateNetIDSemester(String netID, Timestamp current, long semesterID) throws FinderException {
	    return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 **/
	public AssignmentData getAssignmentData() {
		return new AssignmentData(getAssignmentID(), getCourseID(), getName(),
				getNameShort(), getStatus(), getDescription(), getWeight(), getMaxScore(),
				getStudentRegrades(), getGracePeriod(), getDueDate(),
				getRegradeDeadline(), getLateDeadline(), getAllowLate(),
				getShowStats(), getShowSolution(), getNumOfAssignedFiles(), getDuration(),
				getTimeslotLockTime(), getMean(), getMax(), getMedian(), getStdDev(),
				getAssignedGraders(), getAssignedGroups(), getGroupSizeMax(), getGroupLimit(),
				getGroupSizeMin(), getHidden(), getScheduled(), getType(), getDefaultLatePenalty());
	}

	/**
	 * Creates a new assignment for the given course with a specified 
	 * name and due date. The unique assignmentID is assigned automatically, 
	 * and all the other fields are empty.
	 * @param courseid The courseID
	 * @param name The assignment's name
	 * @param dueDate The due date of the assignment
	 * @return The new assignments object
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local"
	 */
	public AssignmentPK ejbCreate(AssignmentData newAssignment)
			throws javax.ejb.CreateException {
		setCourseID(newAssignment.getCourseID());
		setName(newAssignment.getName());
		setNameShort(newAssignment.getNameShort());
		setStatus(newAssignment.getStatus());
		setDescription(newAssignment.getDescription());
		setWeight(newAssignment.getWeight());
		setMaxScore(newAssignment.getMaxScore());
		setStudentRegrades(newAssignment.getStudentRegrades());
		setGracePeriod(newAssignment.getGracePeriod());
		setDueDate(newAssignment.getDueDate());
		setRegradeDeadline(newAssignment.getRegradeDeadline());
		setLateDeadline(newAssignment.getLateDeadline());
		setAllowLate(newAssignment.getAllowLate());
		setShowStats(newAssignment.getShowStats());
		setShowSolution(newAssignment.getShowSolution());
		setNumOfAssignedFiles(newAssignment.getNumOfAssignedFiles());
		setAssignedGraders(newAssignment.getAssignedGraders());
		setAssignedGroups(newAssignment.getAssignedGroups());
		setGroupSizeMax(newAssignment.getGroupSizeMax());
		setGroupSizeMin(newAssignment.getGroupSizeMin());
		setHidden(false);
		setMax(null);
		setMean(null);
		setMedian(null);
		setStdDev(null);
		setGroupLimit(newAssignment.getGroupLimit());
		setDuration(newAssignment.getDuration());
		setTimeslotLockTime(newAssignment.getTimeslotLockTime());
		setScheduled(newAssignment.getScheduled());
		setType(newAssignment.getType());
		setDefaultLatePenalty(newAssignment.getDefaultLatePenalty());
		return null;
	}
}
