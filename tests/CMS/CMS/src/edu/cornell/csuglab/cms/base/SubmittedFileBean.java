/*
 * Created on Aug 1, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */

package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EntityBean;
import javax.ejb.FinderException;

/**
 * @ejb.bean name="SubmittedFile"
 *	jndi-name="SubmittedFileBean"
 *	type="BMP" 
 *
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.SubmittedFileDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.SubmittedFileDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 * Represents a file submitted by a group.
 * The file can be found at the following location:
 * <UserRootDirectory>\CMS_Files\Submissions\<CourseID>\
 *  <AssignmentID>\<OriginalGroupID>\<FileCounter>\<SubmissionID>[.FileType]
 * 
 **/
public abstract class SubmittedFileBean implements EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long submittedFileID;
	private long groupID, originalGroupID;
	private long submissionID;
	private Timestamp fileDate;
	private String fileType;					//file extension ("zip", "txt", etc)
	private String netID;
	private String MD5;
	private boolean lateSubmission;
	private String path;
	private int fileSize;
	
	/**
	 * @ejb.persistence
	 * @ejb.pk-field
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getSubmittedFileID() {
		return submittedFileID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileID
	 * @return
	 */
	public void setSubmittedFileID(long submittedFileID) {
		this.submittedFileID = submittedFileID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getGroupID() {
		return groupID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param groupID
	 */
	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getOriginalGroupID() {
		return originalGroupID;
	}
	
	/**
	 * @param originalGroupID
	 * @ejb.interface-method view-type="local"
	 */
	public void setOriginalGroupID(long originalGroupID) {
		this.originalGroupID = originalGroupID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getNetID() {
		return netID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param netID
	 */
	public void setNetID(String netID) {
		this.netID = netID;
	}
 	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getSubmissionID() {
		return submissionID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileName
	 */
	public void setSubmissionID(long submissionID) {
		this.submissionID = submissionID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public Timestamp getFileDate() {
		return fileDate;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileDate
	 */
	public void setFileDate(Timestamp fileDate) {
		this.fileDate = fileDate;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getFileType() {
		return fileType;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileType
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public int getFileSize() {
		return fileSize;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileSize
	 */
	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getMD5() {
		return MD5;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param MD5
	 */
	public void setMD5(String MD5) {
		this.MD5 = MD5;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public boolean getLateSubmission() {
		return lateSubmission;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param lateSubmission
	 */
	public void setLateSubmission(boolean lateSubmission) {
		this.lateSubmission = lateSubmission;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * Returns the path to this file relative to the system's
	 * home directory
	 * @return
	 */
	public String getPath() {
		return path;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param path
	 */
	public void setPath(String path) {
		this.path = path;
	}
	
	/**
	 * Properly appends the file type (if there is one) to a file name
	 * @param fileName
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public String appendFileType(String fileName) {
		if (getFileType() == null || getFileType().equals("")) {
			return fileName;
		}
		if (fileName.endsWith(getFileType())) return fileName;
		return fileName + "." + getFileType();
	}
	
	/**
	 * @param pk
	 * @return
	 * @throws FinderException
	 */
	public SubmittedFilePK ejbFindByPrimaryKey(SubmittedFilePK pk) throws FinderException {
		return null;
	}
	
	/**
	 * Returns all submitted files for a group
	 * Files are ordered by SubmissionName
	 * @param groupid The GroupID of the group
	 * @return A collection of Groups
	 * @throws FinderException
	 */
	public Collection ejbFindAllByGroupID(long groupid) throws FinderException {
		return null;
	}
	
	/**
	 * Returns only the most recent versions of each submitted
	 * file for a group (Ordered by SubmissionName)
	 * Does not return SubmittedFiles for which the matching
	 * RequiredSubmission has been hidden
	 * @param groupid
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupID(long groupid) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all submitted files by all groups in an assignment
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * Returns all the submitted files for a collection of groups (ordered by 
	 * SubmissionName then by FileDate)
	 * @param groupids
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindAllByGroupIDs(Collection groupids) throws FinderException {
		return null;
	}
	
	/**
	 * Returns all files submitted by any group which has one 
	 * of the netids as a member for a given assignment
	 * @param netids A collection of NetIDs (strings)
	 * @param assignmentID The AssignmentID of the assignment
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByNetIDs(Collection netids, long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @return
	 */
	public SubmittedFileData getSubmittedFileData() {
		return new SubmittedFileData(getSubmittedFileID(), getGroupID(), getOriginalGroupID(), getNetID(), 
		        getSubmissionID(), getFileDate(), getFileType(), getFileSize(),						
				getMD5(), getLateSubmission(), getPath());
	}
	
	/**
	 * Creates a new SubmittedFile entry in the database.
	 * @param fileID
	 * @param courseID
	 * @param assignmentID
	 * @param groupID
	 * @param fileName
	 * @param fileType
	 * @return
	 * @throws CreateException
	 * @ejb.create-method view-type="local"
	 */
	public SubmittedFilePK ejbCreate(long groupID, long originalGroupID, String netID, 
	        long submissionID, String fileType, int fileSize, String MD5, boolean lateSubmission, 
	        String path, Timestamp fileDate) throws CreateException {
		setGroupID(groupID);
		setOriginalGroupID(originalGroupID);
		setNetID(netID);
		setSubmissionID(submissionID);
		if (fileDate == null) {
			setFileDate(new Timestamp(System.currentTimeMillis()));
		} else { 
		    setFileDate(fileDate); 
		}
		setFileType(fileType);
		setFileSize(fileSize);
		setMD5(MD5);
		setLateSubmission(lateSubmission);
		setPath(path);
		return null;
	}
	
}
