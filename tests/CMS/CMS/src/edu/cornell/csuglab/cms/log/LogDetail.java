package edu.cornell.csuglab.cms.log;

import edu.cornell.csuglab.cms.base.LogBean;
import edu.cornell.csuglab.cms.base.LogData;

/**
 * A LogDetail is essentially a wrapper for a detail String describing an
 * individual database change. It represents a row in the detailed-log table,
 * and should be used only with the LogBean functions, since a detailed-log
 * table entry doesn't exist outside of the context of a log table entry.
 * @see LogBean
 */
public class LogDetail {
	private long logID; //refers to a log table entry
	private String detail; //format determined when the log entry is created
	private Long assignmentID;
	private String netID;
	private LogData logRef;
	
	public LogDetail(long logID) {
	    this(logID, null, null, null);
	}
	
	public LogDetail(String detail)
	{
		this(-1, detail, null, null); //log ID to be filled in later
	}
	
	public LogDetail(long logID, String detail) {
		this(logID, detail, null, null);
	}
	
	public LogDetail(long logID, String detail, String netID, Long assignmentID) {
	    this.logID = logID;
	    this.detail = detail;
	    this.assignmentID = assignmentID;
	    this.netID = netID;
	    this.logRef = null;
	}
	
	public long getLogID() {
		return logID;
	}
	
	public void setLogID(long logID) {
		this.logID = logID;
	}
	
	public Long getAssignmentID() {
	    return assignmentID;
	}
	
	public void setAssignmentID(Long assignmentID) {
	    this.assignmentID = assignmentID;
	}
	
	public String getNetID() {
	    return netID;
	}
	
	public void setNetID(String netID) {
	    this.netID = netID;
	}
	
	public void setDetailString(String detail) {
	    this.detail = detail;
	}
	
	public String getDetailString() {
		return detail;
	}
	
	public void setLogRef(LogData d) {
	    this.logRef = d;
	}
	
	public LogData getLogRef() {
	    return logRef;
	}
}
