package edu.cornell.csuglab.cms.author;

/**
 * @author jfg32
 */
public class UserCornellMember extends UserWrapper {
    
    String netID;
    String firstName;
    String lastName;
    
    public UserCornellMember(String netID) {
        this.netID = netID;
        String[] name = UserInfo.getLDAPName(netID);
        firstName = name[0];
        lastName = name[1];
    }
    
    public int getUserType() {
        return UserWrapper.UT_AUTHENTICATED;
    }
	public int getAuthoriznLevelByCourseID(long courseID) {
		return Principal.AUTHOR_CORNELL_COMMUNITY;
	}
	
	public String getUserID() {
		return netID;
	}
	
	public String getFirstName() {
	    return firstName;
	}
	
	public String getLastName() {
	    return lastName;
	}
 	
}
