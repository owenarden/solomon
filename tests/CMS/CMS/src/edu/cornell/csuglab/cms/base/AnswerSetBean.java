/*
 * Created on Nov 5, 2004
 *
 */
package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

/**
 * This contains information for a set of responses to a survey
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.bean name="AnswerSet"
 *	jndi-name="AnswerSetBean"
 *	type="BMP" 
 * 
 * @ejb.dao class="edu.cornell.csuglab.cms.base.AnswerSetDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.AnswerSetDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/
public abstract class AnswerSetBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long answerSetID;
	private long assignmentID;
	private long groupID, originalGroupID;
	private String netID;
	
	private Timestamp submissionDate;
	private int lateSubmission;
	
	private AssignmentLocalHome assignmentHome = null;
	private AnswerLocalHome answerHome = null;
	
	/**
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getAnswerSetID() {
		return answerSetID;
	}
	
	/**
	 * @param answerSetID
	 * @ejb.interface-method view-type="local"
	 */
	public void setAnswerSetID(long answerSetID) {
		this.answerSetID = answerSetID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getAssignmentID() {
		return assignmentID;
	}
	
	/**
	 * @param assignmentID
	 * @ejb.interface-method view-type="local"
	 */
	public void setAssignmentID(long assignmentID) {
		this.assignmentID = assignmentID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getGroupID() {
		return groupID;
	}
	
	/**
	 * @param group ID
	 * @ejb.interface-method view-type="local"
	 */
	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getOriginalGroupID() {
		return originalGroupID;
	}
	
	/**
	 * @param original group id
	 * @ejb.interface-method view-type="local"
	 */
	public void setOriginalGroupID(long originalGroupID) {
		this.originalGroupID = originalGroupID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getNetID() {
		return netID;
	}
	
	/**
	 * @param original group id
	 * @ejb.interface-method view-type="local"
	 */
	public void setNetID(String netID) {
		this.netID = netID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public Timestamp getSubmissionDate() {
		return submissionDate;
	}
	
	/**
	 * @param original timestamp
	 * @ejb.interface-method view-type="local"
	 */
	public void setSubmissionDate(Timestamp submissionDate) {
		this.submissionDate = submissionDate;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public int getLateSubmission() {
		return lateSubmission;
	}
	
	/**
	 * @param original timestamp
	 * @ejb.interface-method view-type="local"
	 */
	public void setLateSubmission(int lateSubmission) {
		this.lateSubmission = lateSubmission;
	}
	
	/**
	 * @return
	 * @throws EJBException
	 * @ejb.interface-method view-type="local"
	 */
	public AssignmentLocal getAssignment() throws EJBException {
		try {
			return assignmentHome.findByAssignmentID(getAssignmentID());
		}
		catch (Exception e) {
			throw new EJBException(e);
		}
	}
	
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */ 
	public AnswerSetData getAnswerSetData() {
		return new AnswerSetData(getAnswerSetID(), getAssignmentID(),
				getGroupID(), getOriginalGroupID(), getNetID(), getSubmissionDate(),
				getLateSubmission());
	}
	
	/**
	 * @param assignmentID
	 * @param itemName
	 * @return
	 * @ejb.create-method view-type="local"
	 */
	public AnswerSetPK ejbCreate(long assignmentID, long groupID, long originalGroupID,  
	        String netID, Timestamp submissionDate) throws CreateException {
		setAssignmentID(assignmentID);
		setGroupID(groupID);
		setOriginalGroupID(originalGroupID);
		setNetID(netID);
		setSubmissionDate(submissionDate);
		return null;
	}

	/**
	 * @param assignmentItemID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public AnswerSetPK ejbFindByPrimaryKey(AnswerSetPK pk) throws FinderException {
		return null;
	}
	
	/**
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection ejbFindByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public AnswerSetPK ejbFindByAnswerSetID(long answerSetID) throws FinderException {
		return null;
	}


	/**
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection ejbFindHiddenByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * 
	 * @param groupid
	 * @param assignmentid
	 * @return
	 * @throws FinderException
	 */
	public AnswerSetPK ejbFindMostRecentByGroupAssignmentID(long groupid, long assignmentid) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all the grade entries for all active members of a collection of groups
	 * 
	 * @param groupids
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupIDs(Collection groupids) throws FinderException {
		return null;
	}
	
	/**
	 * Finds only the grade entries for all active members of a collection of groups
	 * which have been assigned to a certain grader
	 * 
	 * @param groupids
	 * @param grader
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupIDsAssignedTo(Collection groupids, String grader, int numSubProbs) throws FinderException {
		return null;
	}
	
	/**
	 * 
	 * @return Returns all the Answers for this answer set
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getAnswers() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = answerHome().findByAnswerSetID(getAnswerSetID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((AnswerLocal) i.next()).getAnswerData());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private AnswerLocalHome answerHome() {
		try {
			if (answerHome == null) {
				answerHome = AnswerUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return answerHome;
	}

	
	private AssignmentLocalHome assignmentHome() {
		try {
			if (assignmentHome == null) {
				assignmentHome = AssignmentUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assignmentHome;
	}
}
