/*
 * Created on Jul 15, 2004
 */
package edu.cornell.csuglab.cms.base;

import java.net.ConnectException;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.*;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.servlet.http.HttpServletRequest;

import edu.cornell.csuglab.cms.author.Principal;
import edu.cornell.csuglab.cms.log.LogDetail;
import edu.cornell.csuglab.cms.util.*;
import edu.cornell.csuglab.cms.util.category.*;
import edu.cornell.csuglab.cms.www.AccessController;
import edu.cornell.csuglab.cms.www.TransactionHandler;
import edu.cornell.csuglab.cms.www.TransactionResult;
import edu.cornell.csuglab.cms.www.util.*;

/**
 * Some error checking is done here instead of in TransactionHandler; all transacting functions
 * of this class ought to return TransactionResult objects (see the ...cms.www package.) 
 *
 * 
 * @ejb.bean name="Transactions"
 *	jndi-name="TransactionsBean"
 *	type="Stateless" 
 *
 * @ejb.ejb-ref ejb-name="Root"
 * @jboss.ejb-ref-jndi ref-name="Root" jndi-name="Root"
 **/
public abstract class TransactionsBean implements SessionBean {
	protected SessionContext ctx;
	
	private RootLocal database = null;
	private Properties env = null;
	
	// This field is used for sending links in emails to users,
	// and telling clients which hostname to use in cross-site nav/overview
	// (links from there to here)
	// Example: "https://cms.csuglab.cornell.edu/web/auth/"
	private static String cmsServerAddress = "";
	
	
	public static String getCMSServerAddress() {
		/*String address;
		try {
			System.setProperty("java.net.preferIPv4Stack","true");
			String host = InetAddress.getLocalHost().getHostName();
			address = "https://" + host + "/web/auth/";
		}
		catch(UnknownHostException e){
			address = "https://cms.csuglab.cornell.edu/web/auth/";
		}
		
		return address;*/
		
		return cmsServerAddress;
	}
	
	public static void refreshAddressFromRequest(HttpServletRequest req) {
		String address = req.getServerName();
		
		// prefer this to not be an IP address, but an IP is better than nothing
		if (!address.matches("[0-9\\.]*") || cmsServerAddress.equals(""))
			cmsServerAddress = req.getRequestURL().toString(); // contains protocol and path, but not query string
	}
	
	/**
	 * See TransactionHandler.acceptInvitation
	 * See also TransactionsDAOImpl.acceptInvitation
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 * @throws EJBException If the update is incomplete
	 */
	public boolean acceptInvitation(Principal p, long groupid) throws EJBException {
		boolean success = false;
		try {
		    String netid = p.getUserID();
			GroupMemberLocal member = database.groupMemberHome().findByPrimaryKey(new GroupMemberPK(groupid, netid));
			Collection groupMembers = database.groupMemberHome().findActiveByGroupID(groupid);
			AssignmentLocal assignment = database.assignmentHome().findByGroupID(groupid);
			Iterator i = groupMembers.iterator();
			SortedSet groupMembersStr = new TreeSet();
			LogData log = startLog(p);
			Vector emails = new Vector();
			while (i.hasNext()) {
			    GroupMemberLocal m = (GroupMemberLocal) i.next();
			    StudentLocal s = database.studentHome().findByPrimaryKey(new StudentPK(assignment.getCourseID(), m.getNetID()));
			    if (s.getEmailGroup()) {
			        emails.add(m.getNetID());
			    }
			    groupMembersStr.add(m.getNetID());
			    appendReceiver(log, m.getNetID());
			}
			Vector details = new Vector();
			log.setDetailLogs(details);
			appendAssignment(log, assignment.getAssignmentID());
			log.setCourseID(new Long(assignment.getCourseID()));
			log.setLogName(LogBean.ACCEPT_INVITATION);
			log.setLogType(LogBean.LOG_GROUP);
			log.setReceivingNetIDs(groupMembersStr);
			appendDetail(log, netid + " joined the group of " + Util.listElements(groupMembersStr) + " for '" + assignment.getName() + "'");
			Collection currentGroups = database.groupMemberHome().findByNetIDAssignmentID(netid, assignment.getAssignmentID());
			member.setStatus("Active");
			if (emails.size() > 0) {
			    sendAcceptInviteEmail(netid, emails, assignment.getAssignmentData(), log);
			}
			i = currentGroups.iterator();
			Long activeGroup = null;
			while (i.hasNext()) {
				GroupMemberLocal gm = (GroupMemberLocal) i.next();
				if (gm.getGroupID() != member.getGroupID()) {
					if (gm.getStatus().equalsIgnoreCase("Active")) {
					    if (activeGroup != null) {
					        leaveGroup(p, activeGroup.longValue(), log);
					    }
					    activeGroup = new Long(gm.getGroupID());
					}
					else if (gm.getStatus().equalsIgnoreCase("Invited")) {
						declineInvitation(p, gm.getGroupID(), log);
					}
				}
			}
			if (activeGroup != null) {
				leaveGroup(p, activeGroup.longValue(), log);
			}
			database.logHome().create(log);
			success = true;
		}
		catch (Exception e) {
			success = false;
			ctx.setRollbackOnly();
		}
		if (!success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Adds grades, comments, and comment files as updated from the Detailed Grading page
	 * @param assignmentID
	 * @param netID
	 * @param data
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult addAllAssignsGrades(Principal p, long courseID, GradeCommentInfo data) {
		TransactionResult result = new TransactionResult();
		Profiler.enterMethod("TransactionBean.addAllAssignsGrades", "CourseID: " + courseID);
		try {
		    //Assignment assignment = database.assignmentHome().findByAssignmentID(assignmentID);
		    LogData log = startLog(p);
		    log.setCourseID(new Long(courseID));
		    log.setLogName(LogBean.EDIT_GRADES);
		    log.setLogType(LogBean.LOG_GRADE);
			Map latestGrades = database.getLastGradeMapByCourse(courseID);
			Map subProbs = database.getSubProblemNameMapByCourse(courseID);
			Map assigns = database.getAssignmentIDMap(courseID);
			Map assignNames = database.getAssignmentNameMap(courseID);
			Map groupMembers = database.getGroupMemberListMap(courseID);
			String netID = p.getUserID();
			// Process scores
			data.organizeOldScores();
			Vector scores = data.getScores();
			// The scores which were most recent when the user loaded the page
			Vector oldscores = data.getOldScores();
			Vector conflicts = new Vector(); 	// String descriptions of conflicting grades for error output
			Set updatedAssigns = new TreeSet();
			for (int i=0; i < scores.size(); i++) {
				Object[] vals = (Object[]) scores.get(i);
				String student = (String) vals[0];
				Long subProblemID = (Long) vals[1];
				// The incoming score set by user
				Float score = (Float) vals[2];
				// The score which was applicable when the user loaded the page
				Float oldscore = (Float) ((Object[])oldscores.get(i))[2];
				long groupID = ((Long) vals[3]).longValue();
				long assignID = ((Long) assigns.get(new Long(groupID))).longValue();
				String assignName = (String) assignNames.get(new Long(assignID));
				// The current grade in the database
				Float latestGrade = (Float) latestGrades.get(student + "_" + assignID + "_" + subProblemID.toString());
				// -------- CONSISTENCY CHECKS -------------
				boolean proceed = false;	// should we write the change to the database?
				boolean redoStats = true;	// do stats need to be recomputed (assuming we proceed)?
				// Is the user setting a new grade?
				if (oldscore == null) {
					if (latestGrade != null) { // Someone set this grade while the user was using the page
						if (!latestGrade.equals(score)) { // A conflict!
							conflicts.add("Another user set grade for " + assignNames.get(new Long(assignID)) +
									(subProblemID.equals(new Long(0)) ? "" : ", " + subProbs.get(subProblemID)) +
									" to " + latestGrade + ".");
						} else {
							/* We don't need to recompute stats here, since the grade doesn't
							 * change, but we will let the grade be written anyway, so that
							 * the logs show that this user made an identical change. */
							proceed = true;
							redoStats = false; 
						}
					} else { // The user is setting a fresh grade
						proceed = true;
					}
				} else { // A grade already exists
					if (!oldscore.equals(score)) {	// The user is making a change
						/* Since grades can't be nullified, this shouldn't happen, but when
						 * they can be, this would be a conflict. */
						if (latestGrade == null) {	
							conflicts.add("Another user erased the grade for " + assignNames.get(new Long(assignID)) +
									(subProblemID.equals(new Long(0)) ? "" : ", " + subProbs.get(subProblemID)) + ".");
						} // Someone changed the grade to something else 
						else if (!latestGrade.equals(oldscore) && !latestGrade.equals(score)) {
							conflicts.add("Another user set grade for " + assignNames.get(new Long(assignID)) +
									(subProblemID.equals(new Long(0)) ? "" : ", " + subProbs.get(subProblemID)) +
									" to " + latestGrade + ".");
						} else {  // The user is making a safe change to the grade
							proceed = true;
							redoStats = !latestGrade.equals(score);
						}
					}
				}
				if (conflicts.size() == 0 && proceed) {
					if (redoStats) {
						updatedAssigns.add(new Long(assignID));
					}
					boolean ng = latestGrade == null || !redoStats; 
					// This is a new or updated grade, so treat it appropriately
					float diff = (score == null ? 0 : (latestGrade == null ? score.floatValue() : score.floatValue() - latestGrade.floatValue()));
					database.gradeHome().create(assignID, subProblemID.longValue(), score, student, netID);
					LogDetail d = new LogDetail(0);
					d.setNetID(student);
					d.setAssignmentID(new Long(assignID));
					d.setDetailString("Grade for " + student + " on " + (subProblemID.longValue() == 0 ? assignName : ((String)subProbs.get(subProblemID))) + 
					        (score == null ? " erased" : (ng ? " set to " : " changed to ") + score.floatValue() + (ng ? "" : " (" + (diff > 0 ? "+" : "") + diff + ")")));
					appendDetail(log, d);
				}
			}
			if (conflicts.size() > 0) {
				throw new GradingException(conflicts);
			}
			// Proccess submitted files
			Vector submittedFiles = data.getSubmittedFiles();
			for (int i=0; i < submittedFiles.size(); i++) {
			    Object[] vals = (Object[]) submittedFiles.get(i);
			    Long assignID = (Long) vals[0];
			    String fileName = (String) vals[1];
			    SubmittedFileData file = (SubmittedFileData) vals[2];
			    String assignName = (String) assignNames.get(assignID);
			    GroupLocal group = database.groupHome().findByGroupID(file.getGroupID());
			    AssignmentLocal assign = database.assignmentHome().findByAssignmentID(assignID.longValue());
			    database.submittedFileHome().create(file.getGroupID(), file.getOriginalGroupID(), 
			            file.getNetID(), file.getSubmissionID(), file.getFileType(), 
			            file.getFileSize(), file.getMD5(), false, file.getPath(), null);
			    int submissionCount = database.submittedFileHome().findByGroupID(file.getGroupID()).size();
			    group.setRemainingSubmissions(assign.getNumOfAssignedFiles() - submissionCount);
			    LogDetail d = new LogDetail(0);
			    d.setAssignmentID(assignID);
			    d.setDetailString(p.getUserID() + " uploaded " + fileName + " for group of " + ((String) groupMembers.get(new Long(file.getGroupID()))) + " in '" + assignName + "'");
			    appendDetail(log, d);
			}
			// Process new regrade requests
			Iterator regrades = data.getNewRegrades().keySet().iterator();
			while (regrades.hasNext()) {
			    Long groupID = (Long) regrades.next();
			    Object[] vals = (Object[]) data.getNewRegrades().get(groupID);
			    String request = (String) vals[0];
			    Vector subProbIDs = (Vector) vals[1];
			    String reqNetID = (String) vals[2];
			    String assignName = (String) assignNames.get(assigns.get(groupID));
			    if (!(request.equals("") && subProbIDs.size() == 1 && ((Long)subProbIDs.get(0)).equals(new Long(0)))) {
				    for (int i=0; i < subProbIDs.size(); i++) {
						Long subProbID = (Long) subProbIDs.get(i);
						database.regradeRequestHome().create(subProbID.longValue(), groupID.longValue(), reqNetID, request);
						String probName = (subProbID.longValue() == 0 ? assignName : ((String) subProbs.get(subProbID)));
						appendDetail(log, "Added regrade request from " + reqNetID + " for " + probName);
				    }
			    }
			}
			// Process comments, files, and regrade request responses
			Vector comments = data.getComments();
			Set regradedGroups = new TreeSet();
			for (int i=0; i < comments.size(); i++) {
				Object[] vals = (Object[]) comments.get(i);
				long groupID = ((Long) vals[0]).longValue();
				String commentStr = (String) vals[1];
				CommentFileData file = (CommentFileData) vals[2];
				Vector requests = (Vector) vals[3];
				/* If there is no comment file uploaded, and no regrade request response
				 *  options are checked, then we ignore blank comments. (Do not create a comment).
				 * Files and request responses must be associated with a comment, so if 
				 *  either of them happen, we must create a comment even if it is blank.
				 * */
				if (!commentStr.equals("") || file != null || (requests != null && requests.size() > 0)) {
					CommentLocal comment = database.commentHome().create(commentStr, netID, groupID);
					appendDetail(log, "Comment added: '" + commentStr + "' to group of " + groupMembers.get(new Long(groupID)));
					if (file != null) {
						database.commentFileHome().create(comment.getCommentID(), file.getFileName(), file.getPath());
						appendDetail(log, "File appended to comment: " + file.getFileName());
					}
					if (requests != null) {
						for (int j=0; j < requests.size(); j++) {
							long requestID = ((Long) requests.get(j)).longValue();
							RegradeRequestLocal regrade = database.regradeRequestHome().findByPrimaryKey(new RegradeRequestPK(requestID));
							regrade.setStatus(RegradeRequestBean.REGRADED);
							regrade.setCommentID(new Long(comment.getCommentID()));
							regradedGroups.add(new Long(regrade.getGroupID()));
						}
						appendDetail(log, "Responded to " + requests.size() + " regrade requests");
					}
				}
			}
			if (regradedGroups.size() > 0) {
			    Iterator groups = database.groupHome().findByGroupIDs(regradedGroups).iterator();
			    Hashtable byassign = new Hashtable();
			    while (groups.hasNext()) {
			        GroupLocal group = (GroupLocal) groups.next();
			        Vector gs = (Vector) byassign.get(new Long(group.getAssignmentID()));
			        if (gs == null) gs = new Vector();			        
			        gs.add(new Long(group.getGroupID()));
			        byassign.put(new Long(group.getAssignmentID()), gs);
			    }
			    Iterator keys = byassign.keySet().iterator();
			    while (keys.hasNext()) {
			        Long key = (Long) keys.next();
			        Vector gs = (Vector) byassign.get(key);
			        if (gs != null && gs.size() > 0) {
			            AssignmentLocal assign = database.assignmentHome().findByAssignmentID(key.longValue());
			            sendRegradedEmail(gs, assign.getAssignmentData(), log);
			        }
			    }
			}
			// Process removed comments
			Iterator remcoms = data.getRemovedComments().iterator();
			while (remcoms.hasNext()) {
			    long commentID = ((Long) remcoms.next()).longValue();
			    CommentLocal comment = database.commentHome().findByPrimaryKey(new CommentPK(commentID));
			    comment.setHidden(true);
			    Iterator requests = database.regradeRequestHome().findByCommentID(commentID).iterator();
			    appendDetail(log, "Removed " + (requests.hasNext() ? "response" : "comment") + ": '" + comment.getComment() + "' from group of " + groupMembers.get(new Long(comment.getGroupID())));
			    while (requests.hasNext()) {
			        RegradeRequestLocal request = (RegradeRequestLocal) requests.next();
			        request.setCommentID(null);
			        request.setStatus(RegradeRequestBean.PENDING);
			    }
			}
			Iterator updates = updatedAssigns.iterator();
			result.setValue(updates);
			database.logHome().create(log);
		} catch (GradingException g) {
			ctx.setRollbackOnly();
			Vector conflicts = g.getConflicts();
			for (int i=0; i < conflicts.size(); i++) {
				result.addError("Conflict: " + conflicts.get(i));
			}
		} catch (Exception e) {
			ctx.setRollbackOnly();
			e.printStackTrace();
			result.setException(e);
			result.addError("An unexpected error occurred");
		}
		Profiler.exitMethod("TransactionBean.addAllAssignsGrades", "CourseID: " + courseID);
		return result;
	}
	
	/**
	 * Add a CMS admin NetID to the list
	 * @param netID
	 * @return Whether the operation was completed successfully
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean addCMSAdmin(Principal p, String netID) {
		boolean success = true;
		try {
			Vector netIDs = new Vector();
			SortedSet netIDsS = new TreeSet();
			netIDs.add(netID);
			netIDsS.add(netID);				    
			LogData log = startLog(p);
		    log.setLogName(LogBean.ADD_CMS_ADMIN);
		    log.setLogType(LogBean.LOG_ADMIN);
		    log.setReceivingNetIDs(netIDsS);
		    appendDetail(log, netID + " was added as a CMS admin");
			if (ensureUserExistence(netIDs, log).getSuccess()) {
				CMSAdminLocal newAdmin = database.cmsAdminHome().create(netID);
				if(newAdmin == null) success = false;
				else {
					database.logHome().create(log);
				}
			} else success = false;
		}
		catch(Exception e)
		{
			success = false;
		}
		if(ctx != null && !success) ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Adds a sitewide notice to the database 
	 * @param p The principal of the user
	 * @param text The text of the notice
	 * @param author The author of the notice
	 * @param exp The expiration time of the notice (may be null)
	 * @param hidden Whether the notice should be hidden
	 * @return TransactionResult
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */	
	public boolean addSiteNotice(Principal p, String text, String author, Timestamp exp, boolean hidden) {
		boolean success = true;
		try {
			SiteNoticeLocal result = database.siteNoticeHome().create(author, text, exp, hidden);
			if(result == null) success = false;
		} catch(Exception e) {
			success = false;
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Edits a sitewide notice
	 * @param p The principal of the user
	 * @param id The id of the notice
	 * @param text The text of the notice
	 * @param exp The expiration time of the notice (may be null)
	 * @param hidden Whether the notice should be hidden
	 * @param deleted Whether the notice should be marked as deleted
	 * @return TransactionResult
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean editSiteNotice(Principal p, long id, String text, Timestamp exp, boolean hidden, boolean deleted) {
		boolean success = true;
		try {
			SiteNoticeLocal notice = database.siteNoticeHome().findByPrimaryKey(new SiteNoticePK(id));
			notice.setText(text);
			notice.setExpireDate(exp);
			notice.setHidden(hidden);
			notice.setDeleted(deleted);
		} catch(Exception e) {
			success = false;
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Creates new rows of contents, changes existing contents, and hides and unhides rows and files
	 * for a specific category
	 * @param options The metadata associated with all the changes
	 * @param newCategoryFileCount The updated number of files stored in the category
	 * @return true on success, false on error
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean addNEditCtgContents(Principal p, CategoryCtntsOption options, long newCategoryFileCount)
	{
		Profiler.enterMethod("TransactionBean.addNEditCtgContents", "CategoryID: " + options.getCategoryID());
		boolean success = true;
		try
		{
		   CategoryLocal cat = database.categoryHome().findByPrimaryKey(new CategoryPK(options.getCategoryID()));
		   LogData log = startLog(p);
			log.setCourseID(new Long(cat.getCourseID()));
			log.setLogName(LogBean.ADDNEDIT_CONTENTS);
			log.setLogType(LogBean.LOG_CATEGORY);
			//create all necessary new rows	
			Map rowIDMap = new HashMap(); //map ids in option object (Longs) to database rows (CategoryRows)
			Iterator i = options.getNewRows().iterator();
			while(success && i.hasNext()){ //process next row
				CtgRowInfo row = (CtgRowInfo)i.next();
				CategoryRowLocal newRow = database.createCtgRow(row, options.getCategoryID());
				appendDetail(log, "Created row with ID " + newRow.getRowID() + " in content '" + cat.getCategoryName() + "'");
				if(newRow == null) success = false;
				rowIDMap.put(new Long(row.getRowID()), newRow);
			}
			//add all new contents, now that their rows have been created
			Iterator iCtnts = options.getNewContentsList().iterator();
			while(success && iCtnts.hasNext())
			{
				CtgNewContentInfo contentInfo = (CtgNewContentInfo)iCtnts.next();
				long ctntRowID = contentInfo.getRowID();
				CategoryRowLocal row = (CategoryRowLocal)rowIDMap.get(new Long(ctntRowID));
				if(row == null) //the row doesn't exist in the hashtable; it must already be in the database
					row = database.categoryRowHome().findByPrimaryKey(new CategoryRowPK(ctntRowID));
				contentInfo.setRowID(row.getRowID());
				if(contentInfo instanceof CtgNewFileContentInfo) //it might have files associated with it
				{
					System.out.println("file content info for new content (" + contentInfo.getRowID() + ", " + contentInfo.getColID() + "): " + ((CtgNewFileContentInfo)contentInfo).getNumFiles() + " files");
					//make sure there are actual files (that we didn't just take in a bunch of empty form fields)
					if(((CtgNewFileContentInfo)contentInfo).getNumFiles() > 0)
					{
						CategoryContentsLocal newContent = database.createCtgContent(contentInfo);
						System.out.println("created filecell: (" + contentInfo.getRowID() + ", " + contentInfo.getColID() + ")");
						appendDetail(log, "Created datum at row " + contentInfo.getRowID() + ", col " + contentInfo.getColID());
						if(newContent == null) success = false;
						//create all necessary files (actually store them on the server)
						success = database.createNEditCtgFiles((CtgNewFileContentInfo)contentInfo, newContent.getContentID(), log);
					}
				}
				else
				{
					CategoryContentsLocal newContent = database.createCtgContent(contentInfo);
					if(newContent == null) success = false;
					else
					{
						System.out.println("created cell: (" + contentInfo.getRowID() + ", " + contentInfo.getColID() + "), ctnt id " + newContent.getContentID());
						appendDetail(log, "Created datum at row " + contentInfo.getRowID() + ", col " + contentInfo.getColID());
					}
				}
			}
			
		    //update existing contents
			i = options.getOldContentsList().iterator();
			while(success && i.hasNext())
			{
				CtgCurContentInfo contentInfo = (CtgCurContentInfo)i.next();
				System.out.println("CurContentInfo: " + contentInfo);
				CategoryContentsLocal content = database.categoryContentsHome().findByPrimaryKey(new CategoryContentsPK(contentInfo.getContentID()));
				System.out.println("  has content id " + content.getContentID());
				if(content == null) success = false;
				else {
					if(contentInfo instanceof CtgCurTextContentInfo) {
						CtgCurTextContentInfo info = (CtgCurTextContentInfo)contentInfo;
						content.setText(info.getText());
						System.out.println("edited text for content " + content.getContentID());
						appendDetail(log, "Changed the text of datum " + content.getContentID() + " to '" + content.getText() + "'");
					} else if(contentInfo instanceof CtgCurDateContentInfo) {
						CtgCurDateContentInfo info = (CtgCurDateContentInfo)contentInfo;
						content.setDate(info.getDate());
						System.out.println("edited date for content " + content.getContentID());
						appendDetail(log, "Changed the date of datum " + content.getContentID() + " to '" + content.getDate() + "'");
					} else if(contentInfo instanceof CtgCurNumberContentInfo) {
						CtgCurNumberContentInfo info = (CtgCurNumberContentInfo)contentInfo;
						content.setNumber(info.getNumber());
						System.out.println("edited number for content " + content.getContentID());
						appendDetail(log, "Changed the number value of datum " + content.getContentID() + " to " + content.getNumber());
					} else if(contentInfo instanceof CtgCurURLContentInfo) {
						CtgCurURLContentInfo info = (CtgCurURLContentInfo)contentInfo;
						//set URL address and label, respectively
						content.setText(info.getAddress());
						content.setLinkName(info.getLabel());
						System.out.println("edited url for content " + content.getContentID());
						appendDetail(log, "Changed the url of datum " + content.getContentID() + " to '" + content.getText() + "', label '" + content.getLinkName() + "'");
					} else if(contentInfo instanceof CtgCurFileContentInfo) {
						CtgCurFileContentInfo info = (CtgCurFileContentInfo)contentInfo;
						ArrayList infos = info.getFileInfoList(), labels = info.getFileLabelList();
						System.out.println("file list:");
						for(int k = 0; k < infos.size(); k++) {
							CtgFileInfo file = (CtgFileInfo)infos.get(k);
							
							if(file == null)
								System.out.println("  ([null], " + (String)labels.get(k) + ")");
							else
								System.out.println("  " + file.getFileCount() + ": (" + file.getFilePath() + " " + file.getFileName() + ", " + (String)labels.get(k) + ")");
						}
						System.out.println("added file(s) to content " + content.getContentID());
						appendDetail(log, "New file(s) for content " + content.getContentID() + ":");
						/*
						 * create files for an existing content (we need to add new files
						 * before we potentially set the hidden bits on old ones below,
						 * since new ones are added based on index within the list of files
						 * in the cell and the list might be shortened when we hide files)
						 */
						success = database.createNEditCtgFiles(info, log);
					}
				}
			}
			//edit the labels of existing files
			Map fileLabels = options.getEditFileLabelsMap();
			Iterator iFileLabels = fileLabels.keySet().iterator();
			while(success && iFileLabels.hasNext())
			{
				Long fileID = (Long)iFileLabels.next();
				CategoryFileLocal file = database.categoryFileHome().findByPrimaryKey(new CategoryFilePK(fileID.longValue()));
				try
				{
					if(file.getLinkName() == null || !file.getLinkName().equals((String)fileLabels.get(fileID)))
					{
						file.setLinkName((String)fileLabels.get(fileID));
						System.out.println("edited label for file " + fileID);
						appendDetail(log, "Changed displayed name of file with ID " + fileID + " to '" + file.getLinkName() + "'");
					}
				}
				catch(Exception x)
				{
					success = false;
					System.out.println("error in XBean::addNEditCtgContents() (1): " + x.getMessage());
					x.printStackTrace();
				}
			}
			//toggle hide/show (aka remove/restore) bits for existing files
			i = options.getUpdateFileList().iterator();
			while(success && i.hasNext()){
				long fileID = ((Long)i.next()).longValue();
				CategoryFileLocal file = database.categoryFileHome().findByPrimaryKey(new CategoryFilePK(fileID));
				if(file == null) success = false;
				else
				{
					file.setHidden(!file.getHidden());
					System.out.println("toggled hiding for file " + file.getCategoryFileID());
					appendDetail(log, "Toggled hidden status of ctg file " + file.getCategoryFileID());
				}
			}
			//toggle hide/show (aka remove/restore) bits for existing rows
			i = options.getUpdateRowList().iterator();
			while(success && i.hasNext()){
				long rowID = ((Long)i.next()).longValue();
				CategoryRowLocal row  = database.categoryRowHome().findByPrimaryKey(new CategoryRowPK(rowID));
				if(row == null) success = false;
				else
				{
					row.setHidden(!row.getHidden());
					System.out.println("toggled hiding for row " + row.getRowID());
					appendDetail(log, "Toggled hidden status of ctg row " + row.getRowID());
				}
			}
			//update the count of files stored in this category
			cat.setFileCount(newCategoryFileCount);
			database.logHome().create(log);
		}
		catch(Exception e)
		{
			success = false;
			System.out.println("error in XBean::addNEditCtgContents() (2): " + e.getMessage());
			e.printStackTrace();
		}
		if(ctx != null && !success) ctx.setRollbackOnly();
		Profiler.exitMethod("TransactionBean.addNEditCtgContents", "CategoryID: " + options.getCategoryID());
		return success;
	}
	
	/**
	 * Adds grades, comments, and comment files as updated from the Detailed Grading page
	 * for one or more groups for a single assignment
	 * @param assignmentID
	 * @param data
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult addGradesComments(Principal p, long assignmentID, GradeCommentInfo data) {
		TransactionResult result = new TransactionResult();
		Profiler.enterMethod("TransactionBean.addGradesComments", "AssignmentID: " + assignmentID);
		try {
			Profiler.enterMethod("TransactionBean.addGradesComments (part)", "setup");
		    AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(assignmentID);
		    Map groupMembers = database.getGroupMemberListMap(assignment.getCourseID());
		    Collection groups = database.groupHome().findByAssignmentID(assignmentID);
		    LogData log = startLog(p);
		    log.setCourseID(new Long(assignment.getCourseID()));
		    log.setLogName(LogBean.EDIT_GRADES);
		    log.setLogType(LogBean.LOG_GRADE);
			Map latestGrades = database.getLastGradeMap(assignmentID);
			Map subProbs = database.getSubProblemNameMap(assignmentID);
			String netID = p.getUserID();
			Profiler.exitMethod("TransactionBean.addGradesComments (part)", "setup");
			
			// Process scores
			Profiler.enterMethod("TransactionBean.addGradesComments (part)", "score loop");
			data.organizeOldScores();
			Vector scores = data.getScores();
			// The scores which were most recent when the user loaded the page
			Vector oldscores = data.getOldScores();
			Vector conflicts = new Vector(); 	// String descriptions of conflicting grades for error output
			boolean gradeUpdate = false;
			for (int i=0; i < scores.size(); i++) {
				Object[] vals = (Object[]) scores.get(i);
				String student = (String) vals[0];
				Long subProblemID = (Long) vals[1];
				Float score = (Float) vals[2];
				((Long) vals[3]).longValue();
				// The score which was applicable when the user loaded the page
				Float oldscore = (Float) ((Object[])oldscores.get(i))[2];				
				Float latestGrade = (Float) latestGrades.get(student + "_" + subProblemID.toString());
				// -------- CONSISTENCY CHECKS -------------
				boolean proceed = false;	// should we write the change to the database?
				boolean redoStats = true;	// do stats need to be recomputed (assuming we proceed)?
				// Is the user setting a new grade?
				if (oldscore == null) {
					if (latestGrade != null) { // Someone set this grade while the user was using the page
						if (!latestGrade.equals(score)) { // A conflict!
							conflicts.add("Another user set grade for " + student +
									(subProblemID.equals(new Long(0)) ? "" : ", on " + subProbs.get(subProblemID)) +
									" to " + latestGrade + ".");
						} else {
							/* We don't need to recompute stats here, since the grade doesn't
							 * change, but we will let the grade be written anyway, so that
							 * the logs show that this user made an identical change. */
							proceed = true;
							redoStats = false; 
						}
					} else { // The user is setting a fresh grade
						proceed = true;
					}
				} else { // A grade already exists
					if (!oldscore.equals(score)) {	// The user is making a change
						// Nullified grade
						if (latestGrade == null) {	
							conflicts.add("Another user erased the grade for " + student +
									(subProblemID.equals(new Long(0)) ? "" : ", on " + subProbs.get(subProblemID)) + ".");
						} // Someone changed the grade to something else 
						else if (!latestGrade.equals(oldscore) && !latestGrade.equals(score)) {
							conflicts.add("Another user set grade for " + student +
									(subProblemID.equals(new Long(0)) ? "" : ", on " + subProbs.get(subProblemID)) +
									" to " + latestGrade + ".");
						} else {  // The user is making a safe change to the grade
							proceed = true;
							redoStats = !latestGrade.equals(score);
						}
					}
				}
				if (conflicts.size() == 0 && proceed) {
					if (redoStats) {
						gradeUpdate = true;
					}
					boolean ng = latestGrade == null || !redoStats;
					// This is a new or updated grade, so treat it appropriately
					float diff = (score == null ? 0 : (latestGrade == null ? score.floatValue() : score.floatValue() - latestGrade.floatValue()));
					database.gradeHome().create(assignmentID, subProblemID.longValue(), score, student, netID);
					LogDetail d = new LogDetail(0);
					d.setNetID(student);
					d.setAssignmentID(new Long(assignmentID));
					d.setDetailString("Grade for " + student + " on " + (subProblemID.longValue() == 0 ? assignment.getName() : ((String)subProbs.get(subProblemID))) + 
							(score == null ? " erased" : (ng ? " set to " : " changed to ") + score.floatValue() + (ng ? "" : " (" + (diff > 0 ? "+" : "") + diff + ")")));
					appendDetail(log, d);
					
				}
			}
			
			for(Iterator gi = groups.iterator(); gi.hasNext(); ) {
				GroupLocal group = (GroupLocal)gi.next();
				Long groupID = new Long(group.getGroupID());
				if(!data.isGroupVisited(groupID)) continue;
				
				String adjustment = (String)data.getAdjustment(groupID);
				if (adjustment != null && adjustment.length() == 0) adjustment = null;
				
				if(group.getAdjustment() == null && adjustment != null) {
					group.setAdjustment(data.getAdjustment(groupID));
					gradeUpdate = true;
					addLogDetailForMembersInGroup(group, "Adjustment created to " + data.getAdjustment(groupID), log);
				} else if (group.getAdjustment() != null && adjustment == null) {
					group.setAdjustment(null);
					gradeUpdate = true;
					addLogDetailForMembersInGroup(group, "Adjustment erased", log);
				} else if (group.getAdjustment() != null && adjustment != null &&
						!group.getAdjustment().equals(data.getAdjustment(groupID))) {
					group.setAdjustment(data.getAdjustment(groupID));
					gradeUpdate = true;
					addLogDetailForMembersInGroup(group, "Adjustment changed to " + data.getAdjustment(groupID), log);
					
				}
			}
			
			Profiler.exitMethod("TransactionBean.addGradesComments (part)", "score loop");
			if (conflicts.size() > 0) {
				throw new GradingException(conflicts);
			}
			
			// Process submitted files
			Profiler.enterMethod("TransactionBean.addGradesComments (part)", "submitted files loop");
			Vector submittedFiles = data.getSubmittedFiles();
			for (int i=0; i < submittedFiles.size(); i++) {
			    Object[] vals = (Object[]) submittedFiles.get(i);
			    Long assignID = (Long) vals[0];
			    String fileName = (String) vals[1];
			    SubmittedFileData file = (SubmittedFileData) vals[2];
			    GroupLocal group = database.groupHome().findByGroupID(file.getGroupID());
			    database.submittedFileHome().create(file.getGroupID(), file.getOriginalGroupID(), 
			            file.getNetID(), file.getSubmissionID(), file.getFileType(), 
			            file.getFileSize(), file.getMD5(), false, file.getPath(), null);
			    int submissionCount = database.submittedFileHome().findByGroupID(file.getGroupID()).size();
			    group.setRemainingSubmissions(assignment.getNumOfAssignedFiles() - submissionCount);
			    LogDetail d = new LogDetail(0);
			    d.setAssignmentID(assignID);
			    d.setDetailString(p.getUserID() + " uploaded " + fileName + " for group of " + ((String) groupMembers.get(new Long(file.getGroupID()))) + " in '" + assignment.getName() + "'");
			    
			    /* TODO Should there be some sort of notification here? */
			    
			    appendDetail(log, d);
			}
			Profiler.exitMethod("TransactionBean.addGradesComments (part)", "submitted files loop");
			
			// Process new regrade requests
			Profiler.enterMethod("TransactionBean.addGradesComments (part)", "regrade req loop");
			Iterator regrades = data.getNewRegrades().keySet().iterator();
			while (regrades.hasNext()) {
			    Long groupID = (Long) regrades.next();
			    Object[] vals = (Object[]) data.getNewRegrades().get(groupID);
			    String request = (String) vals[0];
			    Vector subProbIDs = (Vector) vals[1];
			    String reqNetID = (String) vals[2];
			    if (!(request.equals("") && subProbIDs.size() == 1 && ((Long)subProbIDs.get(0)).equals(new Long(0)))) {
				    for (int i=0; i < subProbIDs.size(); i++) {
						Long subProbID = (Long) subProbIDs.get(i);
						database.regradeRequestHome().create(subProbID.longValue(), groupID.longValue(), reqNetID, request);
						String probName = (subProbID.longValue() == 0 ? assignment.getName() : ((String) subProbs.get(subProbID)));
						appendDetail(log, "Added regrade request from " + reqNetID + " for " + probName);
				    }
			    }
			}
			Profiler.exitMethod("TransactionBean.addGradesComments (part)", "regrade req loop");
			
			// Process comments, files, and regrade request responses
			Profiler.enterMethod("TransactionBean.addGradesComments (part)", "comment/file/regrade response loop");
			Vector comments = data.getComments();
			Set regradedGroups = new TreeSet();
			for (int i=0; i < comments.size(); i++) {
				Object[] vals = (Object[]) comments.get(i);
				long groupID = ((Long) vals[0]).longValue();
				String commentStr = (String) vals[1];
				CommentFileData file = (CommentFileData) vals[2];
				Vector requests = (Vector) vals[3];
				/* If there is no comment file uploaded, and no regrade request response
				 *  options are checked, then we ignore blank comments. (Do not create a comment).
				 * Files and request responses must be associated with a comment, so if 
				 *  either of them happen, we must create a comment even if it is blank.
				 * */
				if (!commentStr.equals("") || file != null || (requests != null && requests.size() > 0)) {
					CommentLocal comment = database.commentHome().create(commentStr, netID, groupID);
					appendDetail(log, "Comment added: '" + commentStr + "' to group of " + groupMembers.get(new Long(groupID)));
					if (file != null) {
						database.commentFileHome().create(comment.getCommentID(), file.getFileName(), file.getPath());
						appendDetail(log, "File appended to comment: " + file.getFileName());
					}
					if (requests != null) {
						for (int j=0; j < requests.size(); j++) {
							long requestID = ((Long) requests.get(j)).longValue();
							RegradeRequestLocal regrade = database.regradeRequestHome().findByPrimaryKey(new RegradeRequestPK(requestID));
							regrade.setStatus(RegradeRequestBean.REGRADED);
							regrade.setCommentID(new Long(comment.getCommentID()));
							regradedGroups.add(new Long(regrade.getGroupID()));
						}
						appendDetail(log, "Responded to " + requests.size() + " regrade requests");
					}
				}
			}
			if (regradedGroups.size() > 0) {
			    sendRegradedEmail(regradedGroups, assignment.getAssignmentData(), log);
			}
			Profiler.exitMethod("TransactionBean.addGradesComments (part)", "comment/file/regrade response loop");
			
			// Process removed comments
			Profiler.enterMethod("TransactionBean.addGradesComments (part)", "comment removal loop");
			Iterator remcoms = data.getRemovedComments().iterator();
			while (remcoms.hasNext()) {
			    long commentID = ((Long) remcoms.next()).longValue();
			    CommentLocal comment = database.commentHome().findByPrimaryKey(new CommentPK(commentID));
			    comment.setHidden(true);
			    Iterator requests = database.regradeRequestHome().findByCommentID(commentID).iterator();
			    appendDetail(log, "Removed " + (requests.hasNext() ? "response" : "comment") + ": '" + comment.getComment() + "' from group of " + groupMembers.get(new Long(comment.getGroupID())));
			    while (requests.hasNext()) {
			        RegradeRequestLocal request = (RegradeRequestLocal) requests.next();
			        request.setCommentID(null);
			        request.setStatus(RegradeRequestBean.PENDING);
			    }
			}
			Profiler.exitMethod("TransactionBean.addGradesComments (part)", "comment removal loop");
			
			/* If a grade changed, we need to recompute the assignment statistics */
			if (gradeUpdate) {
				computeAssignmentStats(p, assignmentID, log);
				computeTotalScores(p, assignment.getCourseID(), log);
			}
			database.logHome().create(log);
			result.setValue(new Boolean(gradeUpdate));
		} catch (GradingException g) {
			ctx.setRollbackOnly();
			Vector conflicts = g.getConflicts();
			for (int i=0; i < conflicts.size(); i++) {
				result.addError("Conflict: " + conflicts.get(i));
			}
		} catch (Exception e) {
			ctx.setRollbackOnly();
			e.printStackTrace();
			System.out.println("GRADING EXCEPTION: " + e.getMessage());
			System.out.println("Stack trace contains " + e.getStackTrace().length + " lines");
			result.setException(e);
			result.addError("An unexpected error occurred, could not set grades");
		}
		if (!result.getSuccess() && !ctx.getRollbackOnly()) {
			ctx.setRollbackOnly();
		}
		Profiler.exitMethod("TransactionBean.addGradesComments", "AssignmentID: " + assignmentID);
		return result;
	}
	
	private void addLogDetailForMembersInGroup(GroupLocal group, String detail, LogData log) throws FinderException {
		for(Iterator mi = database.groupMemberHome().findByGroupID(group.getGroupID()).iterator(); mi.hasNext();) {
			LogDetail d = new LogDetail(0);
			d.setNetID(((GroupMemberLocal)mi.next()).getNetID());
			d.setDetailString(detail);	
			d.setAssignmentID(new Long(group.getAssignmentID()));
			appendDetail(log, d);
		}
	}
	
    /**
	 *  Submits a regrade request for a student in specified group
	 * @param netID - unique ID of student
	 * @param groupID - unique ID of student's group in this assignment
	 * @param subProblemIDs - collection of subProblem IDs that student wants regraded
	 * @param requestText - reason for regrade
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 * @return
	 */
	public boolean addRegradeRequest(Principal p, long groupID, Collection subProblemIDs,String requestText){
		boolean success = true;
		try {
		    AssignmentLocal assignment = database.assignmentHome().findByGroupID(groupID);
			Iterator groupMembers = database.groupMemberHome().findActiveByGroupID(groupID).iterator();
		    Iterator i = subProblemIDs.iterator();
		    Map subProbNames = database.getSubProblemNameMap(assignment.getAssignmentID());
			long subProblemID;
			String netID = p.getUserID();
			LogData log = startLog(p);
			appendAssignment(log, assignment.getAssignmentID());
			log.setCourseID(new Long(assignment.getCourseID()));
			log.setLogName(LogBean.REQUEST_REGRADE);
			log.setLogType(LogBean.LOG_GROUP);
			TreeSet staffs = new TreeSet();
			String groupMems = "";
			while (groupMembers.hasNext()) {
			    GroupMemberLocal m = (GroupMemberLocal) groupMembers.next();
			    groupMems += groupMems.equals("") ? m.getNetID() : ", " + m.getNetID();
			    LogDetail d = new LogDetail(0);
			    d.setNetID(m.getNetID());
			    appendDetail(log, d);
			}
			if (p.isInStaffAsStudentMode()) {
			    log.setSimulatedNetID(p.getUserID());
			}
			while(i.hasNext()) {
				subProblemID = ((Long)i.next()).longValue();
				GroupAssignedToLocal assignedTo = null;
				try {
				    assignedTo = database.groupAssignedToHome().findByPrimaryKey(new GroupAssignedToPK(groupID, subProblemID));
				} catch (Exception e) {}
				if (assignedTo != null && assignedTo.getNetID() != null) {
				    StaffLocal staff = database.staffHome().findByPrimaryKey(new StaffPK(assignment.getCourseID(), assignedTo.getNetID()));
				    if (staff.getEmailRequest()) {
				        staffs.add(staff.getNetID());
				    }
				}
				database.regradeRequestHome().create(subProblemID, groupID, netID, requestText);
				String probName = (subProblemID == 0 ? assignment.getName() : ((String) subProbNames.get(new Long(subProblemID))));
				appendDetail(log, "Requested regrade for " + probName);
			}
			if (staffs.size() > 0) {
			    sendRequestEmail(staffs, assignment.getAssignmentData(), groupMems, log);
			}
			database.logHome().create(log);
		} catch(Exception e){
			ctx.setRollbackOnly();
			success = false;
			e.printStackTrace();
		}
		return success;
	}
	
	/**
	 * Adds a group of students to a course
	 * @param ntIDs A Vector of Strings
	 * @param courseID
	 * @param sendEmail Whether to send an e-mail to all added students
	 * @return An error message that's empty ("", NOT null) if no error
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult addStudentsToCourse(Principal p, Vector netIDs, long courseID, boolean sendEmail) {
		Profiler.enterMethod("TransactionBean.addStudentsToCourse", "CourseID: " + courseID);
		TransactionResult result = new TransactionResult();
	    try {
	        CourseLocal course = database.courseHome().findByCourseID(courseID);
			/*
			 * allAssigns holds all assignment for the given course, so we can add new students
			 * to groups for all of them
			 */
			Collection allAssigns = database.assignmentHome().findAllByCourseID(courseID);
			Vector allAssignIDs = new Vector();
			Map submissionCounts = new HashMap();
			for (Iterator i = allAssigns.iterator(); i.hasNext(); ) {
				AssignmentLocal a = (AssignmentLocal) i.next();
				Long key;
				allAssignIDs.add(key = new Long(a.getAssignmentID()));
				submissionCounts.put(key, new Integer(a.getNumOfAssignedFiles()));
			}
			LogData log = startLog(p);
			log.setCourseID(new Long(courseID));
			log.setLogName(LogBean.ADD_STUDENTS);
			log.setLogType(LogBean.LOG_COURSE);
			result = ensureUserExistence(netIDs, log);
			HashSet staffMems = new HashSet();
			Iterator classstaff = database.staffHome().findByCourseID(courseID).iterator();
			while (classstaff.hasNext()) {
			    StaffLocal staff = (StaffLocal) classstaff.next();
			    staffMems.add(staff.getNetID());
			}
			Collection graded = database.getGradedAssignments(netIDs, courseID);
			for (int j=0; j < netIDs.size(); j++) {
				String netID = (String)netIDs.get(j);
				if (staffMems.contains(netID)) {
				    result.addError("Could not add '" + netID + "': Already a staff member for this course");
				    continue;
				}
				StudentLocal student = null;
				try {
				    student = database.studentHome().findByPrimaryKey(new StudentPK(courseID, netID));
				} catch (Exception e) {}
				if (student == null) {
					student = database.studentHome().create(courseID, netID);
					student.setEmailNewAssignment(true);
					student.setEmailDueDate(true);
					student.setEmailGroup(true);
					student.setEmailRegrade(true);
					
					LogDetail l = new LogDetail(0);
					l.setNetID(netID);
					l.setDetailString("Added " + netID + " as a student");
					appendDetail(log, l);
					// Place new student in a group for all assignments
					Iterator i= allAssignIDs.iterator();
					while (i.hasNext()) {
					    Long assignID = (Long)i.next();
					    GroupLocal g= database.groupHome().create(assignID.longValue(), ((Integer) submissionCounts.get(assignID)).intValue());
					    database.groupMemberHome().create(g.getGroupID(), netID, GroupMemberBean.ACTIVE);
					}
					if (sendEmail) sendAddedToCourseEmail(netID, course, log);
				} else {
					boolean isAlreadyEnrolled = student.getStatus().equals(StudentBean.ENROLLED);
					if (isAlreadyEnrolled){
						result.addWarning(netID + " was not added because he is already enrolled in this course.");
					}
					else {
						LogDetail l = new LogDetail(0);
						l.setNetID(netID);
						l.setDetailString(netID + " was reenrolled as a student");
						appendDetail(log, l);
						student.setStatus(StudentBean.ENROLLED);
						//Place student in a group for all assignments (s)he doesn't have records for
						Vector assignIDs = new Vector();
						Collection groupedAssigns = database.assignmentHome().findGrouplessAssignments(courseID, netID);
						assignIDs = (Vector) allAssignIDs.clone();
						for (Iterator i= groupedAssigns.iterator(); i.hasNext(); ) {
							assignIDs.remove(new Long(((AssignmentLocal) i.next()).getAssignmentID()));
						}
						for (int i=0; i < assignIDs.size(); i++) {
						    Long assignID = (Long) assignIDs.get(i);
						    GroupLocal g= database.groupHome().create(assignID.longValue(), ((Integer) submissionCounts.get(assignID)).intValue());
						    database.groupMemberHome().create(g.getGroupID(), netID, GroupMemberBean.ACTIVE);
						}
						
						// check whether adding this student makes any group size exceedds limit
						Iterator groups = database.groupHome().findByNetIDCourseID(netID, courseID).iterator();
						while (groups.hasNext()) {
							GroupLocal group = (GroupLocal)groups.next();
							AssignmentLocal assign = database.assignmentHome().findByAssignmentID(group.getAssignmentID());
							int currentGroupSize = TransactionHandler.getActiveGroupSize(group);
							int maxGroupSize = assign.getGroupSizeMax();
							if (currentGroupSize > maxGroupSize && maxGroupSize > 1)
								result.addWarning("Adding student " + netID + " causes a group size in "
													+ assign.getName() + " to overflow.");
							
						}
						
						if (sendEmail) sendAddedToCourseEmail(netID, course, log);
					}
				}
			}
			// Recompute assignment stats for any assignments which require it
			Iterator i = graded.iterator();
			while (i.hasNext()) {
			    Long aID = (Long) i.next();
			    computeAssignmentStats(p, aID.longValue(), log);
			    appendAssignment(log, aID.longValue());
			}
			computeTotalScores(p, courseID, log);
			database.logHome().create(log);
			Profiler.exitMethod("TransactionBean.addStudentsToCourse", "CourseID: " + courseID);
			return result;
		}
		catch(Exception e) {
			e.printStackTrace();
			ctx.setRollbackOnly();
			result.setException(e);
			result.getErrors().clear();
			result.addError("An unexpected error occurred, could not add students");
		}
		Profiler.exitMethod("TransactionBean.addStudentsToCourse", "CourseID: " + courseID);
		return result;
	 }
	
	/* functions to add details to a log */
	
	private void appendAssignment(LogData log, long assignID) {
	    Collection details = log.getDetailLogs();
	    if (details == null) {
	        details = new Vector();
	        log.setDetailLogs(details);
	    }
	    LogDetail d = new LogDetail(0);
	    d.setAssignmentID(new Long(assignID));
	    details.add(d);
	}
	
	public static void appendDetail(LogData log, LogDetail detail) {
	    Collection details = log.getDetailLogs();
	    if (details == null) {
	        details = new Vector();
	        log.setDetailLogs(details);
	    }
	    details.add(detail);
	}
	
	public static void appendDetail(LogData log, String detail) {
	    Collection details = log.getDetailLogs();
	    if (details == null) {
	        details = new Vector();
	        log.setDetailLogs(details);
	    }
	    details.add(new LogDetail(0, detail));
	}
	
	public static void appendDetail(LogData log, String detail, String affected) {
	    Collection details = log.getDetailLogs();
	    if (details == null) {
	        details = new Vector();
	        log.setDetailLogs(details);
	    }
	    LogDetail d = new LogDetail(0, detail);
	    d.setNetID(affected);
	    details.add(d);
	}
	
	public static void appendReceiver(LogData log, String netID) {
	    Collection details = log.getDetailLogs();
	    if (details == null) {
	        details = new Vector();
	        log.setDetailLogs(details);
	    }
	    LogDetail d = new LogDetail(0);
	    d.setNetID(netID);
	    details.add(d);
	}
	
	/**
	 * Assigns a TA to grade a given subproblem for 
	 * some number of groups in an assignment
	 * @param assignmentID The AssignmentID of the assignment
	 * @param subProblemID The SubProblemID of the assignment
	 * @param grader The NetID of the TA to assign to grade
	 * @param groups A collection Long objects, representing
	 * 	the GroupIDs of the groups to apply this assignment to
	 * @return Returns true iff the transaction completed successfully
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public boolean assignGrader(Principal p, long assignmentID, long subProblemID, String grader, Collection groups) {
		boolean success = false;
		try {
		    AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(assignmentID);
		    LogData log = startLog(p);
		    appendAssignment(log, assignmentID);
		    log.setCourseID(new Long(assignment.getCourseID()));
		    log.setLogName(LogBean.ASSIGN_GRADER);
		    log.setLogType(LogBean.LOG_GRADE);
		    boolean isUnassign = grader.equals(GroupAssignedToBean.UNASSIGNED);
	        if (isUnassign) {
		        boolean q = groups.size() > 1;
		        appendDetail(log, groups.size() + " TA assignment" + (q ? "s" : "") + " for grading groups " + (q ? "were" : "was") + " removed in '" + assignment.getName() + "'");
	        } else {
		        appendReceiver(log, grader);
		        appendDetail(log,  grader + " was assigned to grade " + groups.size() + " group" + (groups.size() > 1 ? "s" : "") + " in '" + assignment.getName() + "'");
		    }
			Collection subProbs = new ArrayList();
			if (subProblemID < 0) {
				Collection c = database.subProblemHome().findByAssignmentID(assignmentID);
				if (c.size() > 0) {
				    Iterator problems = c.iterator();
					while (problems.hasNext()) {
						SubProblemLocal subProb = (SubProblemLocal) problems.next();
						subProbs.add(new Long(subProb.getSubProblemID()));
					}
				} else {
				    subProbs.add(new Long(0));
				}
			} else {
				subProbs.add(new Long(subProblemID));
			}
			String newgrader = new String(grader);
			if (newgrader.equalsIgnoreCase(GroupAssignedToBean.UNASSIGNED)) {
				newgrader = null;
			}
			if (newgrader != null) {
			    StaffLocal staff = database.staffHome().findByPrimaryKey(new StaffPK(assignment.getCourseID(), newgrader));
			    if (staff.getEmailNewAssign()) {
			        sendAssignedEmail(staff.getNetID(), assignment.getAssignmentData(), groups.size(), log);
			    }
			}
			Iterator probs = subProbs.iterator();
			long subProbID;
			while (probs.hasNext()) {
				subProbID = ((Long) probs.next()).longValue();
				Collection newgroups = new ArrayList();
				Collection assigned = database.groupAssignedToHome().findByGroupIDsSubID(groups, subProbID, newgroups);
				Iterator i = newgroups.iterator();
				if (newgrader != null) {
					while (i.hasNext()) {
						long groupID = ((Long) i.next()).longValue();
						database.groupAssignedToHome().create(groupID, newgrader, subProbID);
					}
				}
				i = assigned.iterator();
				while (i.hasNext()) {
					GroupAssignedToLocal groupassign = (GroupAssignedToLocal) i.next();
					groupassign.setNetID(newgrader);
				}
			}
			database.logHome().create(log);
			success= true;
		} catch (Exception e) {
			success = false;
			e.printStackTrace();
		}
		if (!success) {
			ctx.setRollbackOnly();
		}
		return success;
	}
	
	
	/**
	 * Create a course under the given semester
	 * @param courseCode The department and number, e.g. COM S 211
	 * @param courseName The official name, e.g. Intro to Java
	 * @return the course ID
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public long batchCreateCourse(Principal p, String courseCode, String courseName, long semID) {
		boolean success = true;
		long newCourseId =-1;
		try {
			CourseLocal course = database.courseHome().create(courseName, "", courseCode, semID);
			LogData log = startLog(p);
			log.setCourseID(new Long(course.getCourseID()));
			log.setLogName(LogBean.CREATE_COURSE);
			log.setLogType(LogBean.LOG_ADMIN);
			appendDetail(log, "Created '" + courseCode + ": " + courseName + "'");
			newCourseId = course.getCourseID();
			if(course == null) success = false;
			/*By default, always create an announcement category associated with course
			else success = createAnnouncementCategory(course.getCourseID());*/
			database.logHome().create(log);
		}
		catch(Exception e) {
			success = false;
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return newCourseId;
	}
	
	
	/**
	 * Create a course under the active (current) semester
	 * @param courseCode The department and number, e.g. COM S 211
	 * @param courseName The official name, e.g. Intro to Java
	 * @return Whether the operation was completed successfully
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public Collection batchFindCourse(String Course, String Code, long semester) {
		Collection classList;
		try {
			classList = database.courseHome().findByCourseCodeAndSemester( Course+" "+Code, semester);
		}
		catch(Exception e) {
			classList = null;
		}
		return classList;
	}
	
	
	/**
	 * Cancels an invitation that has not yet been accepted by a user.
	 * @param canceller The NetID of the user cancelling the invitation
	 * @param cancelled The NetID of the user whose invitation is being cancelled
	 * @param groupid The GroupID of the group
	 * @return Returns true if the transaction completed successfully
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public boolean cancelInvitation(Principal p, String cancelled, long groupid) {
		boolean success = false;
		try {
		    String canceller = p.getUserID();
			GroupMemberLocal member = database.groupMemberHome().findByPrimaryKey(new GroupMemberPK(groupid, cancelled));
			AssignmentLocal assignment = database.assignmentHome().findByGroupID(groupid);
			Collection groupMembers = database.groupMemberHome().findActiveByGroupID(groupid);
			Iterator i = groupMembers.iterator();
			SortedSet groupMembersStr = new TreeSet();
			while (i.hasNext()) {
			    GroupMemberLocal mem = (GroupMemberLocal) i.next();
			    groupMembersStr.add(mem.getNetID());
			}
			LogData mylog = startLog(p);
			mylog.setCourseID(new Long(assignment.getCourseID()));
			appendAssignment(mylog, assignment.getAssignmentID());
			mylog.setLogName(LogBean.CANCEL_INVITATION);
			mylog.setLogType(LogBean.LOG_GROUP);
			appendReceiver(mylog, cancelled);
			appendDetail(mylog, canceller + " has cancelled the invitation to " + cancelled + " to join group of " + Util.listElements(groupMembersStr) + " for '" + assignment.getName() + "'");
			database.logHome().create(mylog);
			member.setStatus("Rejected");
			success = true;
		}
		catch (Exception e) {
			success = false;
			ctx.setRollbackOnly();
		}
		if (!success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Adds a block of timeslots to the schedule
	 * @param p Principal of user
	 * @param groupid ID of group for which scheduled slot will change
	 * @param assignid ID of assignment
	 * @param slotid ID of timeslot into which group will be placed
	 * @param addGroup Flag indicating whether group is to be added or removed from group
	 * @param tsi Data structure containing all info. to be stored regarding slots
	 * @return Whether or not transaction was successful
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean changeGroupSlot(Principal p, long groupid, long assignid, long slotid, boolean addGroup) {
		LogData log = startLog(p);
		Long slotID = new Long(slotid);
		boolean success = false;
		try {
			AssignmentLocal assign = database.assignmentHome().findByAssignmentID(assignid);
			log.setCourseID(new Long(assign.getCourseID()));
			appendAssignment(log, assignid);
			log.setLogName(LogBean.CHANGE_GROUP_TIMESLOT);
			log.setLogType(LogBean.LOG_COURSE);
			GroupLocal gr = database.groupHome().findByGroupID(groupid);
			Long currentslot = gr.getTimeSlotID();
			// do the actual changing of the time slot
			if (addGroup){
				gr.setTimeSlotID(slotID);
			}
			else {
				gr.setTimeSlotID(null);
			}
			if ((currentslot != null) && (currentslot.longValue() != 0)){
				TimeSlotLocal ts = database.timeSlotHome().findByTimeSlotID(currentslot.longValue());
				AssignmentData aData = database.assignmentHome().findByAssignmentID(assignid).getAssignmentData();
				// lower timeslot population and send notifications
				ts.setPopulation(ts.getPopulation() - 1);
				if (!addGroup) sendTSGroupChangeEmails(null, gr.getGroupID(), aData, false, log);
				appendDetail(log, "Removed group " + groupid + " from TimeSlot " + currentslot);
				appendDetail(log, "Lowered population of time slot " + slotid + " to "
									+ ts.getPopulation());
			}
			if (addGroup){
				TimeSlotLocal ts = database.timeSlotHome().findByTimeSlotID(slotid);
				AssignmentData aData = database.assignmentHome().findByAssignmentID(assignid).getAssignmentData();
				// raise timeslot population and send notifications
				ts.setPopulation(ts.getPopulation() + 1);	
				sendTSGroupChangeEmails(ts,groupid,aData,true,log);
				appendDetail (log, "Added group " + groupid + " to Timeslot " + slotid);
				appendDetail(log, "Raised population of time slot " + slotid + " to "
						+ ts.getPopulation());
			}
			database.logHome().create(log);
			success = true;
		} catch (Exception e){
			success = false;
			e.printStackTrace();
		}
		if (!success) ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Set students' final grades from a file
	 * @param p
	 * @param courseID
	 * @param table
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult commitFinalGradesFile(Principal p, long courseID, List table) {
	    TransactionResult result = new TransactionResult();
	    try {
	        LogData log = startLog(p);
	        log.setCourseID(new Long(courseID));
	        log.setLogName(LogBean.UPLOAD_FINAL_GRADES);
	        log.setLogType(LogBean.LOG_GRADE);
	        for (int i=1; i < table.size(); i++)
	        {
	        	String[] data = (String[])table.get(i);
	            String netID = data[0];
	            String grade = data[1];
	            StudentLocal student = database.studentHome().findByPrimaryKey(new StudentPK(courseID, netID));
	            if (!Util.equalNull(student.getFinalGrade(), grade)) {
		            student.setFinalGrade(grade);	                
	            }
		        appendDetail(log, "Final grade for " + netID + " changed to " + grade);
	        }
	        result.setValue("Final grades updated successfully");
	        database.logHome().create(log);
	    } catch (Exception e) {
	        ctx.setRollbackOnly();
	        e.printStackTrace();
			result.setException(e);
	        result.addError("An unexpected error occurred while trying to commit Final Grades file");
	    }
	    return result;
	}

	/**
	 * Add grades to an assignment as parsed from a csv file.
	 * @param assignmentID
	 * @param table
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean commitGradesFile(Principal p, long assignmentID, List table) {
		boolean success = false;
		Profiler.enterMethod("TransactionBean.commitGradesFile", "AssignmentID: " + assignmentID);
		try {
			AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(assignmentID);
			//GradeCommentInfo comments = new GradeCommentInfo();
			//GroupLocalHome groupHome = database.groupHome();
		    Map subProbIDMap = database.getSubProblemIDMap(assignmentID);
			Map groupIDs = database.getGroupIDMap(assignmentID);
			Map lastGrades = database.getLastGradeMap(assignmentID);
			String[] header = (String[])table.get(0);
			long[] subProbIDs = new long[header.length];
			String grader = p.getUserID();
			int[] colsFound = CSVFileFormatsUtil.parseColumnNamesFlexibly(header);
			int netIDIndex = CSVFileFormatsUtil.getFlexibleColumnNum(colsFound, CSVFileFormatsUtil.NETID);
			int commentIndex = CSVFileFormatsUtil.getFlexibleColumnNum(colsFound, CSVFileFormatsUtil.COMMENTS);
			int adjustmentIndex = CSVFileFormatsUtil.getFlexibleColumnNum(colsFound, CSVFileFormatsUtil.ADJUSTMENT);
			LogData log = startLog(p);
			log.setCourseID(new Long(assignment.getCourseID()));
			log.setLogName(LogBean.UPLOAD_GRADES_FILE);
			log.setLogType(LogBean.LOG_GRADE);
			for (int i=0; i < header.length; i++) {
				if(i != netIDIndex && i!= commentIndex && i > 0) {
					if(subProbIDMap.get(header[i]) != null)
						subProbIDs[i] = ((Long) subProbIDMap.get(header[i])).longValue();
					else if (header[i].trim().toLowerCase().equals("grade"))
						if (subProbIDMap.size() == 0)
							subProbIDs[i] = 0;
						else
							subProbIDs[i] = -1;
					else
						subProbIDs[i] = -1;
				}
			}
			
			for (int i=1; i < table.size(); i++) {
				String[] data = (String[])table.get(i);
				String netid = data[netIDIndex];
				((Long) groupIDs.get(netid)).longValue();
				for (int j=0; j < data.length; j++) {
					if(j != netIDIndex && j != commentIndex && j != adjustmentIndex && !data[j].equals("")) { //no grade
						//don't need to check for NumberFormatException here; parsing was done in XactionHandler
						float grade = Float.parseFloat(data[j]);
						long subProbID = subProbIDs[j];
						if (subProbID < 0) continue;
						Float lastGrade = (Float) lastGrades.get(netid + "_" + subProbID);
						boolean ng = (lastGrade == null);
						if (ng || !lastGrade.equals(new Float(grade))) {
							database.gradeHome().create(assignmentID, subProbID, new Float(grade), netid, grader);
							float diff = (lastGrade == null ? 0 : grade - lastGrade.floatValue());
							LogDetail l = new LogDetail(0);
							l.setNetID(netid);
							l.setAssignmentID(new Long(assignmentID));
							l.setDetailString("Grade for " + netid + " on " + (subProbID == 0 ? assignment.getName() : ((String)header[j])) + 
									(ng ? " set to " : " changed to ") + grade + (ng ? "" : " (" + (diff > 0 ? "+" : "") + diff + ")"));
							appendDetail(log, l);
						}
					}
					/*else if(j == commentIndex && !data[j].equals("")) {
						GroupLocal student = groupHome.findByNetIDAssignmentID(data[0], assignmentID);
						comments.addCommentText(student.getGroupID(), data[commentIndex]);
					}*/
				}
			}
			computeAssignmentStats(p, assignmentID, log);
			computeTotalScores(p, assignment.getCourseID(), log);
			database.logHome().create(log);
			success = true;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			e.printStackTrace();
			success = false;
		}
		if (!success && !ctx.getRollbackOnly()) {
			ctx.setRollbackOnly();
		}
		Profiler.exitMethod("TransactionBean.commitGradesFile", "AssignmentID: " + assignmentID);
		return success;
	}
	
	/**
	 * Commit general student info parsed from a CSV file uploaded by either a cms admin or a course admin
	 * (this means updating Student and/or UserBeans); assume all the incoming data
	 * have been checked for formatting
	 * @param p
	 * @param table A List of String[]s, one per student mentioned in the uploaded file;
	 * the first element holds the header strings, and all the data have been verified
	 * @param courseID The ID of the particular course involved if any, else null
	 * @param isClasslist Whether we'll need to read but ignore a CUID column
	 * (usually NetID is used instead to identify records; the classlist format is a special case)
	 * @return Success
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public boolean commitStudentInfo(Principal p, List table, Long courseID, boolean isClasslist)
	{
		boolean success = false;
		try
		{
			LogData log = startLog(p);
			if(courseID != null)
			{
				log.setCourseID(courseID);
				log.setLogType(LogBean.LOG_COURSE);
			}
			else
			{
				log.setLogType(LogBean.LOG_ADMIN);
			}
			log.setLogName(LogBean.UPLOAD_STUDENT_INFO);
			//find which is the netid column so we can get primary keys
			String[] headerLine = (String[])table.get(0);
			int netidIndex = -1, cuidIndex = -1; //we'll use one or the other
			for(int i = 0; i < headerLine.length; i++)
				if(headerLine[i].equals(CSVFileFormatsUtil.NETID)) netidIndex = i;
				else if(headerLine[i].equals(CSVFileFormatsUtil.CUID)) cuidIndex = i;
			//parse and commit data lines
			for(int i = 1; i < table.size(); i++)
			{
				String[] line = (String[])table.get(i);
				String id = (isClasslist) ? line[cuidIndex] : line[netidIndex];
				UserLocal user = null;
				if(isClasslist) user = database.userHome().findByCUID(id);
				else user = database.userHome().findByPrimaryKey(new UserPK(id));
				if(user == null) throw new FinderException("user with ID '" + id + "' not in db");
				String netid = (isClasslist) ? user.getUserID() : id;
				StudentLocal student = null;
				if(courseID != null)
				{
					student = database.studentHome().findByPrimaryKey(new StudentPK(courseID.longValue(), netid));
					if(student == null) throw new FinderException("student ('" + netid + "', " + courseID + ") not in db");
				}
				//check all fields
				for(int j = 0; j < line.length; j++)
				{
					if(p.isCMSAdmin())
					{
						if(headerLine[j].equals(CSVFileFormatsUtil.CUID)) user.setCUID(line[j]);
					}
					if(headerLine[j].equals(CSVFileFormatsUtil.FIRSTNAME)) user.setFirstName(line[j]);
					else if(headerLine[j].equals(CSVFileFormatsUtil.LASTNAME)) user.setLastName(line[j]);
					else if(headerLine[j].equals(CSVFileFormatsUtil.COLLEGE)) user.setCollege(line[j]);
					else if(courseID != null) //course-specific info
					{
						if(headerLine[j].equals(CSVFileFormatsUtil.DEPARTMENT)) student.setDepartment(line[j]);
						else if(headerLine[j].equals(CSVFileFormatsUtil.COURSE_NUM)) student.setCourseNum(line[j]);
						else if(headerLine[j].equals(CSVFileFormatsUtil.LECTURE)) student.setLecture(line[j]);
						else if(headerLine[j].equals(CSVFileFormatsUtil.SECTION)) student.setSection(line[j]);
						else if(headerLine[j].equals(CSVFileFormatsUtil.LAB)) student.setLab(line[j]);
						else if(headerLine[j].equals(CSVFileFormatsUtil.CREDITS)) student.setCredits(line[j]);
						else if(headerLine[j].equals(CSVFileFormatsUtil.GRADE_OPTION)) student.setGradeOption(line[j]);
						else if(headerLine[j].equals(CSVFileFormatsUtil.FINAL_GRADE)) student.setFinalGrade(line[j]);
					}	
				}
			}
			database.logHome().create(log);
			success = true;
		}
		catch(Exception e)
		{
			ctx.setRollbackOnly();
			e.printStackTrace();
			success = false;
		}
		if(!success && !ctx.getRollbackOnly()) ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Commit a mapping from courses and sections to external class IDs, for exporting
	 * @param p
	 * @param table A List of String[]s, one per mapping;
	 * the first element holds the header strings, and all already verified
	 * @param courseID The ID of the particular course involved if any, else null
	 * @param overwrite Whether to override existing values
	 * @return Success
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public TransactionResult commitSectionToExternalIDMap(Principal p, List table, Long courseID, boolean overwrite)
	{
		TransactionResult result = new TransactionResult();
		
		try
		{
			CourseLocal permittedCourse;
			LogData log = startLog(p);
			if(courseID != null)
			{
				log.setCourseID(courseID);
				log.setLogType(LogBean.LOG_COURSE);
				permittedCourse = database.courseHome().findByCourseID(courseID.longValue());
			}
			else
			{
				log.setLogType(LogBean.LOG_ADMIN);
				permittedCourse = null; // anything
			}
			log.setLogName(LogBean.UPLOAD_SECTION_MAP);
			
			// figure out which columns are which
			String[] headerLine = (String[])table.get(0);
			int courseCodeIndex = -1, sectionIndex = -1, numIndex = -1;
			for(int i = 0; i < headerLine.length; i++)
				if(headerLine[i].equals(CSVFileFormatsUtil.COURSE_CODE)) courseCodeIndex = i;
				else if(headerLine[i].equals(CSVFileFormatsUtil.SECTION)) sectionIndex = i;
				else if(headerLine[i].equals(CSVFileFormatsUtil.COURSE_NUM)) numIndex = i;
			
			HashMap mapToLists = new HashMap(); // HashMap<codes, <HashMap<sections, ArrayList<StudentLocal>>>
			
			long curSem = database.semesterHome().findCurrent().getSemesterID();
			
			//parse and commit data lines
			for(int i = 1; i < table.size(); i++)
			{
				String[] line = (String[])table.get(i);
				
				String code = line[courseCodeIndex];
				Long sect = canonicalSection(line[sectionIndex]); // flexibly match sections based upon their numeric value
				String num = line[numIndex];
				
				HashMap sectToLists;
				
				// grab map from sections to student lists from cache (or construct if necessary)
				if (mapToLists.containsKey(code))
				{
					sectToLists = (HashMap) mapToLists.get(code);
				}
				else
				{
					// ASSUMPTION: anticipate that most students of course will be touched from this upload
					// thus, immediately process all students in course
					
					// get course
					// already checked for existance and uniqueness in validator, but need to check equality with courseID
					CourseLocal course;
					if(courseID != null)
					{
						if (permittedCourse.getCode().equals(code)) course = permittedCourse;
						else
						{
							course = null;
							result.addWarning("Ignoring line " + i + " (course code " + code + "  doesn't match current course " + permittedCourse.getCode() + ")");
						}
					}
					else // can be any course
					{
						course = (CourseLocal) database.courseHome().findByCourseCodeAndSemester(code, curSem).toArray()[0];
					}
					
					if(course != null) // ignore if null
					{
						sectToLists = new HashMap();
						mapToLists.put(code, sectToLists);
						
						Collection students = (Collection) database.studentHome().findAllByCourseID(course.getCourseID());
						
						if (course.getHasSection())
						{
							// bin by section
							Iterator iter = students.iterator();
							while (iter.hasNext())
							{
								StudentLocal student = (StudentLocal) iter.next();
								Long thisSect = canonicalSection(student.getSection());
								if (!sectToLists.containsKey(thisSect)) sectToLists.put(thisSect, new ArrayList()); // null key permitted
								((ArrayList) sectToLists.get(thisSect)).add(student);
							}
						}
						else // course doesn't have sections
						{
							// ignore "section" data (not valid)
							// nobody is in any sections
							sectToLists.put(null, students);
						}
					}
					else sectToLists = null;
				} // end grabbing (or constructing) map from sections to student lists
				
				if (sectToLists != null)
				{
					// get student list
					if (sectToLists.containsKey(sect))
					{
						ArrayList studentsInSection = (ArrayList) sectToLists.get(sect);
						
						Iterator iter = studentsInSection.iterator();
						while (iter.hasNext())
						{
							StudentLocal student = (StudentLocal) iter.next();
							boolean canWrite = true;
							if (!overwrite)
							{
								String existingNum = student.getCourseNum();
								if (existingNum != null && existingNum.length() > 0) canWrite = false;
							}
							
							if(canWrite)
							{
								student.setCourseNum(num);
							}
						}
					}
					else
					{
						// expose both representations to user if we encounter this, to reduce confusion
						String sectWarnedOn;
						if (sect.toString().equals(line[sectionIndex]))
							sectWarnedOn = sect.toString();
						else sectWarnedOn = line[sectionIndex] + " (nor anything else that corresponds to the number " + sect + ")";
						
						// if all students are in the null section, check disabled sections or non-assigned sections
						String checkEnabled;
						if (sectToLists.keySet().size() == 1 && sectToLists.containsKey(null))
							checkEnabled = "; sections are disabled or no students have sections";
						else checkEnabled = "";
						
						result.addWarning("No students in course " + code + " with section " + sectWarnedOn + checkEnabled);
					}
				}
			}
			database.logHome().create(log);
		}
		catch(Exception e)
		{
			ctx.setRollbackOnly();
			e.printStackTrace();
			result.addError("Unexpected error occurred: " + e.getMessage(), e);
		}
		if(!result.getSuccess() && !ctx.getRollbackOnly()) ctx.setRollbackOnly();
		
		return result;
	}
	
	private static Long canonicalSection(String sect) // flexibly match sections based upon their numeric value
	{
		if (sect == null) return null;
		Long l = Long.valueOf(sect);
		if (l.longValue() == 0) return null;
		return l;
	}
	
	/**
	 * Calculate the summed assignment scores, mean, standard deviation, and max scores
	 *  for an assignment and update the database accordingly.  
	 * This includes updates to 
	 *   - the Assignment entry in tAssignment to update the statistics, 
	 *   - enters summed grade for all subproblems in tGrades with SubProblemID = 0
	 *     (if this assignment has subproblems)
	 *   - computes and enters mean, max, and min grades for each group into the 
	 *     tGroupGrades table
	 * @param assignmentID The Assignment to calculate values for
	 * @param grader The grader whose grading transaction triggered this method
	 * 	 (Any new total assignment grades which are computed for students will be 
	 *    attributed to this grader)
	 * @throws FinderException
	 * @throws RemoteException
	 * @throws CreateException
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public void computeAssignmentStats(Principal p, long assignmentID, LogData log) throws FinderException, CreateException {
		Profiler.enterMethod("TransactionBean.computeAssignmentStats", "AssignmentID: " + assignmentID);
		try {
		boolean commitLog = false;
		AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(assignmentID);
		if (log == null) {
			commitLog = true;
			log = startLog(p);
			log.setLogName(LogBean.COMPUTE_ASSIGNMENT_STATS);
			log.setLogType(LogBean.LOG_COURSE);
			log.setCourseID(new Long(assignment.getCourseID()));
			appendAssignment(log, assignmentID);
		}
		// New grade entries made here are attributed to the system
		String grader = "CMS";
		Collection subProblems = database.subProblemHome().findByAssignmentID(assignmentID);
		boolean hasSubProblems = subProblems.size() > 0;
		Collection students = database.studentHome().findByCourseIDSortByLastName(assignment.getCourseID());
		database.groupHome().findByAssignmentID(assignmentID);
		/* Key: NetID -> Value: [TotalGrade, PreviousGrade]
		 * String -> Float[2] */
		Hashtable studentSums = new Hashtable();
		/* Key: "<GroupID>_<SubProblemID>"
		 * Value: [Total (Float), EntryNum (Integer), Averaged (Boolean), LastEntry (Float)]
		 * String -> Object[4] */
		Hashtable groupSums = new Hashtable();
		Map groupsMap = database.getGroupIDMap(assignmentID);
		HashSet enrolledStudents = new HashSet();
		Iterator i;
		/* Make a Set containing enrolled students, so we know whether or not to use their grade to contribute
		 * to the group's sum */
		i = students.iterator();
		while (i.hasNext()) {
		    StudentLocal student = (StudentLocal) i.next();
		    enrolledStudents.add(student.getUserID());
		}
		// Find all grades for students in this assignment and add them to the individual sums
		Collection grades = database.gradeHome().findMostRecentByAssignmentID(assignmentID);
		if (grades.size() == 0) {
			Profiler.exitMethod("TransactionBean.computeAssignmentStats", "AssignmentID: " + assignmentID + " - (early exit)");
			return;
		}
		i = grades.iterator();
		while (i.hasNext()) {
			GradeLocal grade = (GradeLocal) i.next();
			Long groupID = (Long)groupsMap.get(grade.getNetID());
			float score = grade.getGrade().floatValue();
			// [sum of current scores (null if no scores), old total score (null if none)]
			Float[] sum = (Float[]) studentSums.get(grade.getNetID());
			if (!(grade.getSubProblemID() == 0 && hasSubProblems)) {
				if (sum != null) {
					sum[0] = sum[0] == null ? new Float(score) : new Float(sum[0].floatValue() + score);
				} else {
					sum = new Float[2];
					sum[0] = new Float(score);
					sum[1] = null;
				}
			} else {
				if (sum != null) {
					sum[1] = new Float(score);
				} else {
					sum = new Float[2];
					sum[0] = null;
					sum[1] = new Float(score);
				}
			}
			
			studentSums.put(grade.getNetID(), sum);
			if (enrolledStudents.contains(grade.getNetID()) && grade.getSubProblemID() != 0) {
			    Object[] groupSum = (Object[]) groupSums.get(groupID + "_" + grade.getSubProblemID());
				if (groupSum != null) {
					Float total = (Float) groupSum[0];
					Integer count = (Integer) groupSum[1];
					Boolean averaged = (Boolean) groupSum[2];
					Float last = (Float) groupSum[3];
					groupSum[0] = new Float(total.floatValue() + score);
					groupSum[1] = new Integer(count.intValue() + 1);
					if (!averaged.booleanValue() && !last.equals(new Float(score))) {
						groupSum[2] = new Boolean(true);
					}
				} else {
					groupSum = new Object[4];
					groupSum[0] = new Float(score);
					groupSum[1] = new Integer(1);
					groupSum[2] = new Boolean(false);
					groupSum[3] = new Float(score);
				}
				groupSums.put(groupID + "_" + grade.getSubProblemID(), groupSum);
			}
		}
		
		// Enter newly summed total grades into the database and compute the mean score
		i = students.iterator();
		float total = 0;
		ArrayList scores = new ArrayList();
		/* This loop finds a total of all assignment grades (for getting the mean), 
		 *  and creates a grade entry for the student's total assignment grade 
		 *  if necessary. */
		while (i.hasNext()) {
			StudentLocal student = (StudentLocal) i.next();
			String netid = student.getUserID();
			Float[] sums = (Float[]) studentSums.get(netid);
			if (sums != null) {
				long groupID = ((Long) groupsMap.get(netid)).longValue();
				GroupLocal group = database.groupHome().findByGroupID(groupID);
				if (sums[0] != null) { 
					// Calculate adjustments
					if (hasSubProblems) {
						String adjustment = group.getAdjustment();
						if (adjustment != null && adjustment.length() != 0) {
							if (adjustment.charAt(adjustment.length()-1) == '%') {
								sums[0] = new Float(sums[0].floatValue() +
												    sums[0].floatValue() * Float.parseFloat(adjustment.substring(0, adjustment.length()-1)) / 100);
							} else {
								sums[0] = new Float(sums[0].floatValue() + Float.parseFloat(adjustment));
							}
						}
					}
					
					// Insert recently calculated sums into the group grade sums
					Object[] groupSum = (Object[]) groupSums.get(groupID + "_0");
					if (groupSum != null) {
						Float sumtotal = (Float) groupSum[0];
						Integer count = (Integer) groupSum[1];
						Boolean averaged = (Boolean) groupSum[2];
						Float last = (Float) groupSum[3];
						groupSum[0] = new Float(sumtotal.floatValue() + sums[0].floatValue());
						groupSum[1] = new Integer(count.intValue() + 1);
						if (!averaged.booleanValue() && !last.equals(new Float(sums[0].floatValue()))) {
							groupSum[2] = new Boolean(true);
						}
					} else {
						groupSum = new Object[4];
						groupSum[0] = new Float(sums[0].floatValue());
						groupSum[1] = new Integer(1);
						groupSum[2] = new Boolean(false);
						groupSum[3] = new Float(sums[0].floatValue());
					}
					groupSums.put(groupID + "_0", groupSum);
					scores.add(new Float(sums[0].floatValue()));
					total += sums[0].floatValue();
				}
				if (hasSubProblems && (sums[0] == null || sums[1] == null || !sums[0].equals(sums[1]))) {
					database.gradeHome().create(assignmentID, 0, sums[0], netid, grader);
					float diff = ((sums[0] == null || sums[1] == null) ? 0 : sums[0].floatValue() - sums[1].floatValue());
					LogDetail d = new LogDetail(0, "Grade for " + netid + " on " + assignment.getName() + 
					        (sums[0] == null ? " erased" : " calculated to be " + sums[0].floatValue() + (sums[1] == null ? "" : (" (" + (diff > 0 ? "+" : "") + diff + ")"))));
					d.setNetID(netid);
					d.setAssignmentID(new Long(assignmentID));
					appendDetail(log, d);
				}
			}
		}
		Float mean, median, max = null, stdev;
		if (scores.size() > 0) {
			mean = new Float(total / (float) scores.size());
			float sstotal = 0;
			i = scores.iterator();
			float val;
			/* This loop calculates the sum of squared differences using the previously
			 * computed mean value (for finding the standard deviation) */
			while (i.hasNext()) {
				Float score = (Float) i.next();
				if (max == null || score.floatValue() > max.floatValue()) {
					max = score;
				}
				val = (score.floatValue() - mean.floatValue());
				sstotal += (val * val);
			}
			stdev = new Float((float) Math.sqrt(sstotal / scores.size()));
			// Find median
			median = new Float(0);
			Object[] sortedScores = scores.toArray();
			Arrays.sort(sortedScores);
			if (sortedScores.length % 2 == 0) {
				median = new Float(sortedScores.length == 0 ? 0 : (((Float) sortedScores[sortedScores.length / 2 - 1]).floatValue() 
						+ ((Float) sortedScores[sortedScores.length / 2]).floatValue()) / 2.0f);
			} else /* Odd */ {
				median = new Float(((Float) sortedScores[(sortedScores.length - 1) / 2]).floatValue());
			}
		} else {
			mean = null;
			median = null;
			stdev = null;
		}
		// First we modify all the previously EXISTING group grades
		GroupGradeLocalHome ggHome = database.groupGradeHome();
		i = ggHome.findByAssignmentID(assignmentID).iterator();
		while (i.hasNext()) {
			GroupGradeLocal groupGrade = (GroupGradeLocal) i.next();
			String key = groupGrade.getGroupID() + "_" + groupGrade.getSubProblemID();
			Object[] groupSum = (Object[]) groupSums.get(key);
			if (groupSum != null) {
				Float sum = (Float) groupSum[0];
				Integer count = (Integer) groupSum[1];
				Boolean averaged = (Boolean) groupSum[2];
				groupGrade.setScore(sum.floatValue() / count.floatValue());
				groupGrade.setIsAveraged(averaged.booleanValue());
				groupSums.remove(key);
			} else {
				ggHome.remove(groupGrade.getPrimaryKey());
			}
		}
		// Then for any left over (NON-EXISTING) group grades, we create new database entries
		Enumeration e = groupSums.keys();
		while (e.hasMoreElements()) {
			String k = (String) e.nextElement();
			String[] ks = k.split("_");
			long groupID = Long.parseLong(ks[0]);
			long subProblemID = Long.parseLong(ks[1]);
			Object[] groupSum = (Object[]) groupSums.get(k);
			Float sum = (Float) groupSum[0];
			Integer count = (Integer) groupSum[1];
			Boolean averaged = (Boolean) groupSum[2];
			ggHome.create(groupID, sum.floatValue() / count.floatValue(),
					averaged.booleanValue(), subProblemID);
		}
		assignment.setMax(max);
		assignment.setMean(mean);
		assignment.setStdDev(stdev);
		assignment.setMedian(median);
		if (commitLog) {
			database.logHome().create(log);
		}
		Profiler.exitMethod("TransactionBean.computeAssignmentStats", "AssignmentID: " + assignmentID);
		} catch(Exception x) {
			if(ctx != null) ctx.setRollbackOnly();
			x.printStackTrace();
			Profiler.exitMethod("TransactionBean.computeAssignmentStats", "AssignmentID: " + assignmentID + " - (crash)");
			throw new FinderException(x.getMessage());
		}
	}
	
	/**
	 * Calculates the total student scores for a course
	 * Formula: TotalScore = 
	 * 		Sum of (student assign score / total assign score) * assign weight
	 * 		over all non-hidden assignments in the course
	 * @param courseID
	 * @param log
	 * @throws FinderException
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public void computeTotalScores(Principal p, long courseID, LogData log) throws FinderException {
		Profiler.enterMethod("TransactionBean.computeTotalScores", "CourseID: " + courseID);
	    try {
	    	boolean commitLog = false;
	    	if (log == null) {
	    		log = startLog(p);
	    		commitLog = true;
	    		log.setLogName(LogBean.COMPUTE_TOTAL_SCORES);
	    		log.setLogType(LogBean.LOG_COURSE);
	    		log.setCourseID(new Long(courseID));
	    	}
	    	CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(courseID));
	        Collection gs = database.gradeHome().findTotalsByCourseID(courseID);
	        if (gs.size() == 0) {
				Profiler.exitMethod("TransactionBean.computeTotalScores", "CourseID: " + courseID + " - (early exit)");
				return;	// No scores to compute with
	        }
	        Collection as = database.assignmentHome().findByCourseID(courseID);
	        Collection ss = database.studentHome().findAllByCourseID(courseID);
	        Hashtable assignWeights = new Hashtable(), maxScores = new Hashtable();
	        Iterator grades = gs.iterator();
	        Iterator assigns = as.iterator();
	        Iterator students = ss.iterator();
	        float maxTotalScore = 0;
	        while (assigns.hasNext()) {
	            AssignmentLocal assign = (AssignmentLocal) assigns.next();
	            assignWeights.put(new Long(assign.getAssignmentID()), new Float(assign.getWeight()));
	            maxScores.put(new Long(assign.getAssignmentID()), new Float(assign.getMaxScore()));
	            maxTotalScore += assign.getWeight();
	        }
	        Hashtable totalScores = new Hashtable();
	        while (grades.hasNext()) {
	            GradeLocal grade = (GradeLocal) grades.next();
	            Float score = (Float) totalScores.get(grade.getNetID());
                Float w = (Float) assignWeights.get(new Long(grade.getAssignmentID()));
                Float m = (Float) maxScores.get(new Long(grade.getAssignmentID()));
                if (w == null || m == null) continue;
                if (score == null) {
	                score = new Float((grade.getGrade().floatValue() / m.floatValue()) * w.floatValue());
	            } else {
	                score = new Float((grade.getGrade().floatValue() / m.floatValue()) * w.floatValue() + score.floatValue());
	            }
                totalScores.put(grade.getNetID(), score);
	        }
	        float highTotalScore = 0;
	        float[] scoresArray = new float[ss.size()];
	        int i = 0;
	        while (students.hasNext()) {
	            StudentLocal student = (StudentLocal) students.next();
	            Float score = (Float) totalScores.get(student.getUserID());
	            if (score != null) {
	            	if (score.floatValue() > highTotalScore) {
	            		highTotalScore = score.floatValue();
	            	}
	            	scoresArray[i++] = score.floatValue();
	                if (!Util.equalNull(student.getTotalScore(), score)) {
	                    boolean isnew = student.getTotalScore() == null;
	                    float diff = isnew ? 0 : score.floatValue() - student.getTotalScore().floatValue();
	                    student.setTotalScore(score);
	                    appendDetail(log, "Total grade for " + student.getUserID() + (isnew ? " " : " re") + "calculated to be " + score.floatValue() +
	                            (isnew ? "" : ((diff > 0 ? " (+" : " (") + diff + ")")));
	                }
	            } else {
	            	if (student.getTotalScore() != null) {
	            		student.setTotalScore(null); // score null, so total score should be null
	            	}
	            }
	        }
	        float[] sortedScores;
	        if (i < ss.size()) {
	        	sortedScores = new float[i];
	        	System.arraycopy(scoresArray, 0, sortedScores, 0, i);
	        	Arrays.sort(sortedScores);
	        } else {
	        	Arrays.sort(scoresArray);
	        	sortedScores = scoresArray;
	        }
	        float meanTotalScore = 0;
	        for (int j = 0; j < sortedScores.length; j++) {
	        	meanTotalScore += sortedScores[j];
	        }
	        meanTotalScore /= sortedScores.length;
	        float stDevTotalScore = 0;
	        for (int j = 0; j < sortedScores.length; j++) {
	        	float diff = sortedScores[j] - meanTotalScore;
	        	stDevTotalScore += diff * diff;
	        }
	        stDevTotalScore = (float) Math.sqrt(stDevTotalScore / sortedScores.length);
	        float medianTotalScore;
			if (sortedScores.length % 2 == 0) {
				medianTotalScore = sortedScores.length == 0 ? 0 : ((sortedScores[sortedScores.length / 2 - 1]
						+ sortedScores[sortedScores.length / 2]) / 2.0f);
			} else /* Odd */ {
				medianTotalScore = sortedScores[(sortedScores.length - 1) / 2];
			}
			if (as.size() == 0) course.setMaxTotalScore(null);
			else course.setMaxTotalScore(new Float(maxTotalScore));
			if (as.size() == 0 || gs.size() == 0 || ss.size() == 0) {
				course.setHighTotalScore(null);
				course.setMeanTotalScore(null);
				course.setMedianTotalScore(null);
				course.setStDevTotalScore(null);
			} else {
				course.setHighTotalScore(new Float(highTotalScore));
				course.setMeanTotalScore(new Float(meanTotalScore));
				course.setMedianTotalScore(new Float(medianTotalScore));
				course.setStDevTotalScore(new Float(stDevTotalScore));
			}
	        if (commitLog) {
	        	database.logHome().create(log);
	        }
			Profiler.exitMethod("TransactionBean.computeTotalScores", "CourseID: " + courseID);
	    } catch (Exception e) {
	        ctx.setRollbackOnly();
	        e.printStackTrace();
			Profiler.exitMethod("TransactionBean.computeTotalScores", "CourseID: " + courseID + " - (crash)");
	        throw new FinderException(e.getMessage());
	    }
	}
	
	/**
	 * auxiliary to course creation function: create an announcement category under the specified course
	 * @param courseID - unique ID for specified course
	 * @return
	 */
	public boolean createAnnouncementCategory(long courseID){
		boolean success = false;
		try{
			CategoryTemplate announcementTempl = new CategoryTemplate(courseID, CategoryTemplate.ANNOUNCEMENTS, 
													CategoryTemplate.DEFAULT_NUMTOSHOW, Principal.AUTHOR_STUDENT);
			CategoryLocal announcement = database.createCategory(announcementTempl); 
			if(announcement != null){
				CtgColInfo dateInfo = new CtgColInfo(CategoryTemplate.DATE, CtgUtil.CTNT_DATE, CategoryTemplate.FIRST);
				long ctgID = announcement.getCategoryID();
				CategoryColLocal dateCol = database.createCtgColumn(dateInfo, ctgID);
				announcement.setSortByColId(dateCol.getColID());
				if(dateCol != null){
					CtgColInfo announcementInfo = new CtgColInfo(CategoryTemplate.ANNOUNCEMENT, CtgUtil.CTNT_TEXT, CategoryTemplate.SECOND);
					CategoryColLocal announcementCol = database.createCtgColumn(announcementInfo, ctgID);
					if(announcementCol != null) success = true;
				}
			}
		}catch(Exception e){}
		return success;
	}
	
	/**
	 * Creates a new category for a given course
	 * @param templ - the template(metadata regarding category and columns)
	 * @return true on success, false on error
	 * @throws EJBException If the update is incomplete
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean createCategory(Principal p, CategoryTemplate templ, CategoriesOption opt) throws EJBException {
		boolean success = false;
		try{
		    LogData log = startLog(p);
		    log.setCourseID(new Long(templ.getCourseID()));
		    log.setLogName(LogBean.CREATE_CATEGORY);
		    log.setLogType(LogBean.LOG_CATEGORY);
		    CategoryLocal category = null;
		    if (templ.getCategoryName() != null && !templ.getCategoryName().equals("")) {
				category = database.createCategory(templ);
				appendDetail(log, "Created content table '" + category.getCategoryName() + "'");
				if(category != null) success = true;
				success = createNewColumns(success, category, templ, log);
		    } else {
		        success = true;
		        log.setLogName(LogBean.ORDER_CATEGORIES);
		    }
			if (success) {
			    success = updateCategoriesOption(opt, log);
			}
			if (log.getDetailLogs().size() > 0) {
			    database.logHome().create(log);
			}
		}catch(Exception e){
			success = false;
			System.out.println(e.getMessage());
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Creates a new category for a given course, but returns new categoryID instead of success
	 * Exactly the same as createCategory otherwise.
	 * @param templ - the template(metadata regarding category and columns)
	 * @return ID of new category if successful, -1 otherwise
	 * @throws EJBException If the update is incomplete
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public long createCategoryReturnID(Principal p, CategoryTemplate templ, CategoriesOption opt) throws EJBException {
		boolean success = false;
		long catID = -1;
		try{
		    LogData log = startLog(p);
		    log.setCourseID(new Long(templ.getCourseID()));
		    log.setLogName(LogBean.CREATE_CATEGORY);
		    log.setLogType(LogBean.LOG_CATEGORY);
		    CategoryLocal category = null;
		    if (templ.getCategoryName() != null && !templ.getCategoryName().equals("")) {
				category = database.createCategory(templ);
				appendDetail(log, "Created content table '" + category.getCategoryName() + "'");
				if(category != null)
				{
					success = true;
					catID = category.getCategoryID();
				}
				success = createNewColumns(success, category, templ, log);
		    } 
		    else 
		    {
		        success = true; //???
		        log.setLogName(LogBean.ORDER_CATEGORIES);
		    }
			if (success) {
			    success = updateCategoriesOption(opt, log);
			}
			if (log.getDetailLogs().size() > 0) {
			    database.logHome().create(log);
			}
		}catch(Exception e){
			success = false;
			System.out.println(e.getMessage());
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return catID;
	}
	
	/**
	 * Create a course under the active (current) semester
	 * @param courseCode The department and number, e.g. COM S 211
	 * @param courseName The official name, e.g. Intro to Java
	 * @return Whether the operation was completed successfully
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean createCourse(Principal p, String courseCode, String courseName) {
		boolean success = true;
		try {
			CourseLocal course = database.courseHome().create(courseName, "", courseCode, database.findCurrentSemester().getSemesterID());
			LogData log = startLog(p);
			log.setCourseID(new Long(course.getCourseID()));
			log.setLogName(LogBean.CREATE_COURSE);
			log.setLogType(LogBean.LOG_ADMIN);
			appendDetail(log, "Created '" + courseCode + ": " + courseName + "'");
			if(course == null) success = false;
			/*By default, always create an announcement category associated with course
			else success = createAnnouncementCategory(course.getCourseID());*/
			database.logHome().create(log);
		}
		catch(Exception e) {
			success = false;
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * @param netids
	 * @param assignmentID
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean createGroup(Principal p, Collection netids, long assignmentID) {
		boolean success = false;
		try {
			//assume success unless an exception occurs
			privateCreateGroup(p, netids, assignmentID);
			success = true;
		} catch (Exception e) {
			success = false;
			ctx.setRollbackOnly();
			e.printStackTrace();
		}
		if (!success) {
			ctx.setRollbackOnly();
		}
		return success;
	}
	
	/**
	 * Auxiliary to both createGroup() and mergeGroups()
	 * @param p
	 * @param netids A Collection of distinct Strings
	 * @param assignmentID
	 * @return True unless an exception is thrown
	 */
	private boolean privateCreateGroup(Principal p, Collection netids, long assignmentID) throws CreateException, FinderException {
		AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(assignmentID);
		LogData log = startLog(p);
		appendAssignment(log, assignmentID);
		log.setCourseID(new Long(assignment.getCourseID()));
		log.setLogName(LogBean.CREATE_GROUP);
		log.setLogType(LogBean.LOG_GROUP);
		//So we don't keep adding the same files over and over...
		Vector submittedFilePaths = new Vector();
		Iterator i = netids.iterator();
		while (i.hasNext()) {
		    String netID = (String) i.next();
		    appendReceiver(log, netID);
		}
		appendDetail(log, "Created a new group of " + Util.listElements(netids) + " for '" + assignment.getName() + "'");
		GroupLocal group = database.groupHome().create(assignmentID, assignment.getNumOfAssignedFiles());
		long groupID = group.getGroupID();
		// Merge all submitted files 
		Collection submittedFiles = database.submittedFileHome().findByNetIDs(netids, assignmentID);
		i = submittedFiles.iterator();
		Timestamp latest = null;
		while (i.hasNext()) {
			SubmittedFileLocal file = (SubmittedFileLocal) i.next();
			if(!submittedFilePaths.contains(file.getPath())) {
				submittedFilePaths.add(file.getPath());
				if (latest == null || file.getFileDate().after(latest)) {
					latest = file.getFileDate();
				}
				database.submittedFileHome().create(groupID, file.getOriginalGroupID(), file.getNetID(), file.getSubmissionID(), 
						file.getFileType(), file.getFileSize(), file.getMD5(), 
						file.getLateSubmission(), file.getPath(), file.getFileDate());
			}
		}
		Collection oldgroups = database.groupHome().findByNetIDsAssignmentID(netids, assignmentID);
		i = oldgroups.iterator();
		while (i.hasNext()) {
			GroupLocal oldgroup = (GroupLocal) i.next();
			if (oldgroup.getTimeSlotID() != null) {
				TimeSlotLocal ts = database.timeSlotHome().findByGroupID(oldgroup.getGroupID());
				oldgroup.setTimeSlotID(null);
				ts.setPopulation(ts.getPopulation() - 1);
			}
		}
		// Remove any invitations to old groups
		Collection cancelledInvites = database.groupMemberHome().findByInviters(netids, assignmentID);
		i = cancelledInvites.iterator();
		while (i.hasNext()) {
			GroupMemberLocal member = (GroupMemberLocal) i.next();
			member.setStatus("Rejected");
		}
		// Remove members from old groups
		Collection oldMembers = database.groupMemberHome().findActiveByNetIDsAssignmentID(netids, assignmentID);
		i = oldMembers.iterator();
		while (i.hasNext()) {
			GroupMemberLocal member = (GroupMemberLocal) i.next();
			member.setStatus("Rejected");
		}
		// Add members to new group
		i = netids.iterator();
		while (i.hasNext()) {
			String netid = (String) i.next();
			database.groupMemberHome().create(groupID, netid, "Active");
		}
		Collection files = database.submittedFileHome().findByGroupID(groupID);
		int num = assignment.getNumOfAssignedFiles();
		group.setRemainingSubmissions(assignment.getNumOfAssignedFiles() - files.size());
		group.setLatestSubmission(latest);
		database.logHome().create(log);
		return true;
	}
	
	/**
	 * Creates a new assignment with these properties
	 * @param newAssignment A data describing the new assignment
	 * @param options
	 * @return Whether or not the transaction was successful
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult createNewAssignment(Principal p, AssignmentData newAssignment, AssignmentOptions options) {
		TransactionResult result = new TransactionResult();
		Profiler.enterMethod("TransactionBean.createNewAssignment", "CourseID: " + newAssignment.getCourseID());
		try {
			// Create new assignment
			AssignmentLocal assign = database.assignmentHome().create(newAssignment);
			// Set starting groups
			CourseLocal course= database.courseHome().findByPrimaryKey(new CoursePK(assign.getCourseID()));
			Collection c;
			Iterator i;
			LogData log = startLog(p);
			log.setCourseID(new Long(assign.getCourseID()));
			log.setLogName(LogBean.CREATE_ASSIGNMENT);
			log.setLogType(LogBean.LOG_COURSE);
			LogDetail l = new LogDetail(0, "Created new assignment '" + newAssignment.getName() + "'");
			l.setAssignmentID(new Long(assign.getAssignmentID()));
			appendDetail(log, l);
			if (newAssignment.getStatus().equals(AssignmentBean.OPEN)) {
			    sendNewAssignEmails(assign.getAssignmentID(), course.getCourseID(), newAssignment, log);
			}
			if (newAssignment.getStatus().equals(AssignmentBean.GRADED)) {
			    sendGradeReleaseEmails(newAssignment, log);
			}
			// Create new RequiredFiles for this assignment
			int ID = -1;
			//int index = options.getNextNewRequiredSubmissionID();
			Iterator IDs = options.getNewRequiredSubmissionIDs().iterator();
			int numOfAssignedFiles = 0;
			while (IDs.hasNext()) {
			//while (index >= 0) {
				ID = ((Integer) IDs.next()).intValue();
				String submissionName = options.getNewRequiredFileNameByID(ID);
				int maxSize = options.getNewRequiredMaxSizeByID(ID);
				numOfAssignedFiles++;
				RequiredSubmissionLocal submission = database.requiredSubmissionHome().create(assign.getAssignmentID(),
				        submissionName, maxSize);
				c = options.getNewRequiredFileTypesByID(ID);
				i = c.iterator();
				String types = "";
				while (i.hasNext()) {
					String fileType = (String) i.next();
					types += (types.equals("") ? fileType : ", " + fileType);
					database.requiredFileTypeHome().create(submission.getSubmissionID(), fileType);
				}
				appendDetail(log, "Added required submission '" + submissionName + "' with accepted types: " + types);
				//index = options.getNextNewRequiredSubmissionID();
			}
			// Set the number of assigned files to the number of required files
			assign.setNumOfAssignedFiles(numOfAssignedFiles);
			// Create new AssignmentFiles for this assignment
			ID = -1;
			IDs = options.getNewAssignmentItemIDs().iterator();
			while (IDs.hasNext()) {
				ID = ((Integer) IDs.next()).intValue();
				String itemName = options.getNewItemNameByID(ID);
				AssignmentItemLocal newItem = database.assignmentItemHome().create(assign.getAssignmentID(), itemName);
				AssignmentFileData newFileData = options.getNewFinalFileByID(ID);
				database.assignmentFileHome().create(newItem.getAssignmentItemID(), newFileData.getFileName(),
						false, newFileData.getPath());
				appendDetail(log, "Added assignment file '" + newFileData.getFileName() + "' as '" + itemName + "'");
			}
			// Create new SolutionFile if there is one
			if (options.hasSolutionFile()) {
				SolutionFileData newSolutionData = options.getFinalSolutionFile();
				database.solutionFileHome().create(assign.getAssignmentID(),
						newSolutionData.getFileName(), false, newSolutionData.getPath());
				appendDetail(log, "Added solution file '" + newSolutionData.getFileName() + "'");
			}
			// Create new SubProblems for this assignment
			Collection subProbIDs = options.getNewSubProblemIDs();
			i = subProbIDs.iterator();
			String subProbName;
			float maxScore;
			int type;
			int order;
			Iterator choiceIDs;
			SubProblemOptions choices;
			int choiceID;
			int answer;
			String choiceLetter;
			String choiceText;
			while (i.hasNext()) {
				ID = ((Integer) i.next()).intValue();
				subProbName = options.getNewSubProblemNameByID(ID);
				maxScore = options.getNewSubProblemScoreByID(ID);
				type = options.getNewSubProblemTypeByID(ID);
				order = options.getNewSubProblemOrderByID(ID);
				answer = options.getNewSubProblemAnswerByID(ID);
				choices = options.getNewSubProblemChoicesByID(ID);
				SubProblemLocal subProblem = database.subProblemHome().create(assign.getAssignmentID(), subProbName, maxScore, type, order, answer);
				appendDetail(log, "Added problem '" + subProbName + "' worth " + maxScore + " points");
				
				//Only new choices for new problems
				if(choices != null)
				{
					int choiceIndex = 0;
					choiceIDs = choices.getNewChoiceIDs().iterator();
					while(choiceIDs.hasNext())
					{
						choiceID = ((Integer) choiceIDs.next()).intValue();
						choiceLetter = choices.getNewChoiceLetterByID(choiceID);
						choiceText = choices.getNewChoiceTextByID(choiceID);
						ChoiceLocal choice = database.choiceHome().create(choiceID, subProblem.getSubProblemID(), choiceLetter, choiceText, false);
						
						if(choiceIndex == answer)
						{
							subProblem.setAnswer((int)choice.getChoiceID());
						}
						appendDetail(log, "Added choice '" + choiceText + " (" + choiceLetter + ")' to problem '" + subProbName + "'");
						choiceIndex++;
					}
				}
			}

			// Create groups for all students in the course
			if (!options.migrateGroups()) { 
				// don't import groups, just create new ones
			    appendDetail(log, "Created new groups for each student");
				c= database.studentHome().findAllByCourseID(assign.getCourseID());
				i= c.iterator();
				while (i.hasNext()) {
					StudentLocal stud= (StudentLocal)i.next();
					GroupLocal group= database.groupHome().create(assign.getAssignmentID(), numOfAssignedFiles);
					database.groupMemberHome().create(group.getGroupID(), stud.getUserID(), GroupMemberBean.ACTIVE);
				}
			} else {
				// Duplicate existing groups
				AssignmentLocal impassign= database.assignmentHome().findByAssignmentID(options.getMigrationAssignmentID());
				appendDetail(log, "Imported groups from '" + impassign.getName() + "'");
				c = database.groupMemberHome().findActiveByAssignmentID(impassign.getAssignmentID());
				i= c.iterator();
				Hashtable newgroups = new Hashtable();
				while (i.hasNext()) {
					GroupMemberLocal m = (GroupMemberLocal) i.next();
					Long key = new Long(m.getGroupID());
					Long newgid = (Long) newgroups.get(key);
					if (newgid == null) {
					    GroupLocal copy = database.groupHome().create(assign.getAssignmentID(), numOfAssignedFiles);
					    newgroups.put(key, new Long(copy.getGroupID()));
					    database.groupMemberHome().create(copy.getGroupID(), m.getNetID(), GroupMemberBean.ACTIVE);
					} else {
					    database.groupMemberHome().create(newgid.longValue(), m.getNetID(), GroupMemberBean.ACTIVE);
					}
				}
			}
			database.logHome().create(log);
		} 
		catch (Exception e) {
			ctx.setRollbackOnly();
			result.addError("An unexpected error occurred, could not create new assignment");
			result.setException(e);
			e.printStackTrace();
			System.out.println("ASSIGNMENT CREATION EXCEPTION: " + e.getMessage());
			System.out.println("Stack trace contained " + e.getStackTrace().length + " lines");
		}
		Profiler.exitMethod("TransactionBean.createNewAssignment", "CourseID: " + newAssignment.getCourseID());
		return result;
	}
	
	/**
	 * Create new columns for a specified category with the metadata passed in
	 * @param success
	 * @param category - the specified category bean
	 * @param templ - the metadata for the category and new columns
	 * @return
	 * @throws Exception
	 */
	public boolean createNewColumns(boolean success, CategoryLocal category, CategoryTemplate templ, LogData log) throws Exception{
		Collection columnList = templ.getNewColumnList();
		Iterator i = columnList.iterator();
		boolean first = true;
		while(i.hasNext() && success== true){
			CtgColInfo col = (CtgColInfo)i.next();
			CategoryColLocal ctgCol = database.createCtgColumn(col, category.getCategoryID());
			appendDetail(log, "Added column '" + col.getColName() + "' to content '" + category.getCategoryName() + "'");
			if(ctgCol == null)
				success = false;
			if(col.getSortByThis() || (first && !templ.getSortColExist())) //sort by this column
				category.setSortByColId(ctgCol.getColID());
			first = false;
		}
		return success;
	}
	
	/**
	 * Create a new semester
	 * @param semesterName The name for the new semester, checked for legality with Util.formatSemesterNameString()
	 * @return Whether the operation was completed successfully
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean createSemester(Principal p, String semesterName) {
		boolean success = true;
		try {
		    LogData log = startLog(p);
		    log.setLogName(LogBean.CREATE_SEMESTER);
		    log.setLogType(LogBean.LOG_ADMIN);
		    appendDetail(log, "Created semester: " + semesterName);
			SemesterLocal result = database.semesterHome().create(semesterName);
			if(result == null) success = false;
			database.logHome().create(log);
		} catch(Exception e) {
			success = false;
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Adds a block of timeslots to the schedule
	 * @param p Principal of user
	 * @param tsi Data structure containing all info. to be stored regarding slots
	 * @param multiplicity The number of consecutive timeslots to add to the schedule
	 * @return Whether or not transaction was successful
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean createTimeSlots(Principal p, TimeSlotData tsd, int multiplicity) {
		boolean success = false;
		LogData log = startLog(p);
		log.setCourseID(new Long(tsd.getCourseID()));
		appendAssignment(log, tsd.getAssignmentID());
		log.setLogName(LogBean.CREATE_TIMESLOTS);
		log.setLogType(LogBean.LOG_COURSE);
		try {
			// grab basic information about the first timeslot in the block to be added
			Timestamp startTime = tsd.getStartTime();
			Long duration = database.assignmentHome().findByAssignmentID(tsd.getAssignmentID()).getDuration();
			long firstStart = startTime.getTime();
			// convert interval to microseconds, to be added to the timestamp
			long interval;
			if (duration == null) interval = 600000;
			else interval = duration.longValue() * 60000;
			for (int i = 0; i < multiplicity; i++){
				// increment currentStart to reflect the start time of the next timeslot in the batch
				Timestamp currentStart = new Timestamp((long) ((firstStart) + (i)*interval));
				tsd.setStartTime(currentStart);
				// add timeslot to database
				database.timeSlotHome().create(tsd);
			}
			appendDetail(log, "Added " + multiplicity + " TimeSlot" + (multiplicity>1 ? "s" : "") + " '" + ((tsd.getName()==null) ? (" named " + tsd.getName()) : "") + 
					" assigned to " + tsd.getStaff());
			database.logHome().create(log);
			success = true;
		} catch (Exception e) { 
			e.printStackTrace();
			success = false; 
		}
		if (!success) ctx.setRollbackOnly();
		return success;	
	}
	
	/**
	 * See declineInvitation(Principal, long, LogData)
	 * @ejb.interface-method view-type="local"
	 */
	public boolean declineInvitation(Principal p, long groupid) {
	    return declineInvitation(p, groupid, null);
	}
	
	/**
	 * Declines an invitation for a user to join a given group.
	 * @param p	The Principal of the user declining the invitation
	 * @param groupid The GroupID of the group being declined
	 * @param details A vector to which log details will be added in
	 * 	the case that this method is a submethod of another transaction.
	 * 	If this method is used as a transaction itself, then details should be
	 * 	null (in this case it will do its own logging)
	 * @return Returns true if the transaction completed successfully
	 * @ejb.transaction type="Required"
	 */
	public boolean declineInvitation(Principal p, long groupid, LogData log) {
		boolean success = false;
		try {
		    String netid = p.getUserID();
			GroupMemberLocal member = database.groupMemberHome().findByPrimaryKey(new GroupMemberPK(groupid, netid));
			AssignmentLocal assignment = database.assignmentHome().findByGroupID(groupid);
			Collection groupMembers = database.groupMemberHome().findActiveByGroupID(groupid);
			Iterator i = groupMembers.iterator();
			LogData mylog = log;
			if (mylog == null) {
			    mylog = startLog(p);
			    mylog.setCourseID(new Long(assignment.getCourseID()));
			    mylog.setLogName(LogBean.REJECT_INVITATION);
			    mylog.setLogType(LogBean.LOG_GROUP);
			    appendAssignment(mylog, assignment.getAssignmentID());
			}
			SortedSet groupMembersStr = new TreeSet();
			Vector emails = new Vector();
			while (i.hasNext()) {
			    GroupMemberLocal m = (GroupMemberLocal) i.next();
			    groupMembersStr.add(m.getNetID());
			    appendReceiver(mylog, m.getNetID());
			    StudentLocal s = database.studentHome().findByPrimaryKey(new StudentPK(assignment.getCourseID(), m.getNetID()));
			    if (s.getEmailGroup()) {
			        emails.add(s.getUserID());
			    }
			}
			appendDetail(mylog, netid + " declined to join the group of " + Util.listElements(groupMembersStr) + " for '" + assignment.getName() + "'");
			if (log == null) {
			    database.logHome().create(mylog);
			}
			member.setStatus("Rejected");
			if (emails.size() > 0) {
			    sendDeclineInviteEmail(netid, emails, assignment.getAssignmentData(), log);
			}
			success = true;
		}
		catch (Exception e) {
			success = false;
			ctx.setRollbackOnly();
		}
		if (!success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Destroy all groups for assignment toAssignID and replace them with
	 * groups in assignment fromAssignID.  Remove all pending invitations to those groups.
	 * Any student in a destroyed group which has submitted files brings those files
	 * to his new group.
	 * Requires: fromAssignID and toAssignID are in the same course
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public TransactionResult duplicateGroups(Principal p, long fromAssignID, long toAssignID, LogData log) {
		TransactionResult result = new TransactionResult();
		try {
			Iterator toGroups = database.groupHome().findByAssignmentID(toAssignID).iterator();
			Iterator fromMembers = database.groupMemberHome().findActiveByAssignmentID(fromAssignID).iterator();
			Iterator toMembers = database.groupMemberHome().findNonRejectedByAssignmentID(toAssignID).iterator();
			Iterator toFiles = database.submittedFileHome().findByAssignmentID(toAssignID).iterator();			
			AssignmentLocal toAssign = database.assignmentHome().findByAssignmentID(toAssignID);
			// Remove scheduled timeslots from old groups
			while (toGroups.hasNext()) {
				GroupLocal group = (GroupLocal) toGroups.next();
				group.setTimeSlotID(null);
			}
			// Create maps to make merging easier
			// GroupID => Vector of SubmittedFiles
			HashMap files = new HashMap();
			while (toFiles.hasNext()) {
				SubmittedFileLocal file = (SubmittedFileLocal) toFiles.next();
				Long key = new Long(file.getGroupID());
				Vector groupFiles = (Vector) files.get(key);
				if (groupFiles == null) {
					groupFiles = new Vector();
					groupFiles.add(file);
					files.put(key, groupFiles);
				} else {
					groupFiles.add(file);
				}
			}
			// FromGroupID => Vector of active members' NetIDs (String)
			HashMap members = new HashMap();
			while (fromMembers.hasNext()) {
				GroupMemberLocal member = (GroupMemberLocal) fromMembers.next();
				Long key = new Long(member.getGroupID());
				Vector groupMembers = (Vector) members.get(key);
				if (groupMembers == null) {
					groupMembers = new Vector();
					groupMembers.add(member.getNetID());
					members.put(key, groupMembers);
				} else {
					groupMembers.add(member.getNetID());
				}
			}
			// Old group member NetID (String) => Old GroupID (Long)
			HashMap oldGroups = new HashMap();
			// GroupID (Long) => List of active member names (String)  (for logging)
			HashMap groupNames = new HashMap();
			// Collection of Pair (groupID, invited user's NetID), for logging invitations
			Vector invitations = new Vector();
			// Collection of required submissions for this assignment
			Collection reqSub = database.requiredSubmissionHome().findByAssignmentID(toAssignID);
			// Also, reject all invitations and remove active members from all old groups
			while (toMembers.hasNext()) {
				GroupMemberLocal member = (GroupMemberLocal) toMembers.next();
				String netID = member.getNetID();
				Long groupID = new Long(member.getGroupID());
				if (member.getStatus().equals(GroupMemberBean.ACTIVE)) {
					oldGroups.put(netID, groupID);
					String names = (String) groupNames.get(groupID);
					if (names == null) {
						groupNames.put(groupID, netID);
					} else {
						names += ", " + netID;
						groupNames.put(groupID, netID);
					}
				} else {
					invitations.add(new Pair(groupID, netID));					
				}
				member.setStatus(GroupMemberBean.REJECTED);
			}
			// Log group disbandments
			Iterator groupIDs = groupNames.keySet().iterator();
			while (groupIDs.hasNext()) {
				String names = (String) groupNames.get(groupIDs.next());
				appendDetail(log, "Group of " + names + " was disbanded");
			}
			// Log invitation rejections
			for (int i=0; i < invitations.size(); i++) {
				Pair pair = (Pair) invitations.get(i);
				String netID = (String) pair.two();
				String names = (String) groupNames.get(pair.one());
				appendDetail(log, "Invitation to " + netID + " to join group of " + names + " was removed");
			}
			// Create new groups
			Iterator fromGroups = members.keySet().iterator();
			while (fromGroups.hasNext()) {
				Long groupID = (Long) fromGroups.next();
				Vector groupMembers = (Vector) members.get(groupID);
				// Create new group
				GroupLocal copiedGroup = database.groupHome().create(toAssignID, 0);
				long copiedGroupID = copiedGroup.getGroupID();
				TreeSet oldGroupIDs = new TreeSet();
				String memberNames = "";
				// Find all applicable old groups, and add members to group
				for (int i=0; i < groupMembers.size(); i++) {
					String netID = (String) groupMembers.get(i);
					memberNames += memberNames.length() == 0 ? netID : ", " + netID; 
					oldGroupIDs.add(oldGroups.get(netID));
					database.groupMemberHome().create(copiedGroupID, netID, GroupMemberBean.ACTIVE);
				}
				appendDetail(log, memberNames + " added to new group");
				// Count required submissions, minus duplicates
				TreeSet submittedTracker = new TreeSet();
				// Track latest submission
				Timestamp latest = null;
				// Duplicate submitted files
				Iterator itOldGroupIDs = oldGroupIDs.iterator();
				while (itOldGroupIDs.hasNext()) {
					Long oldGroupID = (Long) itOldGroupIDs.next();
					Vector oldFiles = (Vector) files.get(oldGroupID);
					if (oldFiles == null) continue;
					for (int i=0; i < oldFiles.size(); i++) {
						SubmittedFileLocal file = (SubmittedFileLocal) oldFiles.get(i);
						long submissionID = file.getSubmissionID();
						Timestamp date = file.getFileDate();
						if (latest == null) latest = date;
						else if (latest.before(date)) latest = date;
						if (reqSub.contains(new RequiredSubmissionPK(submissionID)))
							submittedTracker.add(new Long(submissionID));
						database.submittedFileHome().create(copiedGroupID, file.getOriginalGroupID(), 
								file.getNetID(), submissionID, file.getFileType(), file.getFileSize(), 
								file.getMD5(), file.getLateSubmission(), file.getPath(), file.getFileDate());
					}
				}
				copiedGroup.setRemainingSubmissions(toAssign.getNumOfAssignedFiles() - submittedTracker.size());
				copiedGroup.setLatestSubmission(latest);
			}
 		} catch (Exception e) {
			result.addError("An unexpected error occurred while trying to duplicate groups");
			result.setException(e);
			e.printStackTrace();
			ctx.setRollbackOnly();
		}
		return result;
	}
	
	/**
	 * @param groupID
	 * @return
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public boolean disbandGroup(Principal p, long groupID) {
		boolean success = false;
		try {
			GroupLocal group = database.groupHome().findByGroupID(groupID);
			AssignmentLocal assignment = database.assignmentHome().findByGroupID(groupID);
			Collection members = database.groupMemberHome().findActiveByGroupID(groupID);
			Collection files = database.submittedFileHome().findAllByGroupID(groupID);
			LogData log = startLog(p);
			appendAssignment(log, assignment.getAssignmentID());
			log.setCourseID(new Long(assignment.getCourseID()));
			log.setLogName(LogBean.DISBAND_GROUP);
			log.setLogType(LogBean.LOG_GROUP);
			Map submissionNames = database.getSubmissionNameMap(assignment.getAssignmentID());
			TreeSet recNetIDs = new TreeSet();
			Iterator i = members.iterator();
			while (i.hasNext()) {
			    GroupMemberLocal member = (GroupMemberLocal) i.next();
			    recNetIDs.add(member.getNetID());
			    appendReceiver(log, member.getNetID());
			}
			log.setReceivingNetIDs(recNetIDs);
			appendDetail(log, "Disbanded the group of " + Util.listElements(recNetIDs) + " for '" + assignment.getName() + "'");
			i = members.iterator();
			// Create a new group for each member
			boolean first = true;
			while (i.hasNext()) {
				GroupMemberLocal member = (GroupMemberLocal) i.next();
				// Everyone gets their own group (except the last person who inherits the original group)
				if (i.hasNext()) {
					GroupLocal newgroup = database.groupHome().create(group.getAssignmentID(), group.getRemainingSubmissions());
					member.setStatus("Rejected");
					newgroup.setRemainingSubmissions(group.getRemainingSubmissions());
					newgroup.setExtension(group.getExtension());
					newgroup.setLatestSubmission(group.getLatestSubmission());
					database.groupMemberHome().create(newgroup.getGroupID(), member.getNetID(), "Active");
					Iterator j = files.iterator();
					// Duplicate submitted files for the new groups
					while (j.hasNext()) {
						SubmittedFileLocal file = (SubmittedFileLocal) j.next();
						String subName = (String) submissionNames.get(new Long(file.getSubmissionID()));
						database.submittedFileHome().create(newgroup.getGroupID(), file.getOriginalGroupID(), file.getNetID(), file.getSubmissionID(), 
								file.getFileType(), file.getFileSize(), file.getMD5(), file.getLateSubmission(), 
								file.getPath(), file.getFileDate());
						if (first) {
						    appendDetail(log, "Duplicated submission for '" + subName + "' (" + DateTimeUtil.formatDate(file.getFileDate()) + ") for each group member");
						}
					}
				}
				first = false;
			}
			database.logHome().create(log);
			success = true;
		} catch (Exception e) {
			success = false;
			ctx.setRollbackOnly();
		}
		if (!success) {
			ctx.setRollbackOnly();
		}
		return success;
	}
	
	/**
	 * Disband all groups whose IDs are in the given list and have multiple members;
	 * assume it's valid to do so (none have been graded already, etc)
	 * @param p
	 * @param groupIDs A List of Longs
	 * @param asgnID The ID of the assignment concerned
	 * @return Success
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean disbandGroups(Principal p, List groupIDs, long asgnID)
	{
		boolean success = false;
		try
		{
			//can assume success unless there's an exception
			privateDisbandGroups(p, groupIDs, asgnID);
			success = true;
		}
		catch (Exception e)
		{
			success = false;
			ctx.setRollbackOnly();
		}
		if (!success) {
			ctx.setRollbackOnly();
		}
		return success;
	}
	
	/**
	 * Auxiliary to both disbandGroups() and mergeGroups().
	 * @param p
	 * @param groupIDs
	 * @param asgnID
	 * @return True unless an exception occurs.
	 * @throws CreateException, FinderException
	 */
	private boolean privateDisbandGroups(Principal p, List groupIDs, long asgnID) throws CreateException, FinderException {
		if(groupIDs.size() == 0) {
			return true;
		}
		LogData log = startLog(p);
		appendAssignment(log, asgnID);
		AssignmentLocal assignment = database.assignmentHome().findByPrimaryKey(new AssignmentPK(asgnID));
		log.setCourseID(new Long(assignment.getCourseID()));
		log.setLogName(LogBean.DISBAND_GROUP);
		log.setLogType(LogBean.LOG_GROUP);
		TreeSet recNetIDs = new TreeSet();
		for(int j = 0; j < groupIDs.size(); j++) {
			long groupID = ((Long)groupIDs.get(j)).longValue();
			GroupLocal group = database.groupHome().findByGroupID(groupID);
			Collection members = database.groupMemberHome().findActiveByGroupID(groupID);
			if(members.size() > 1) { //don't need to disband singlet groups 
				Collection files = database.submittedFileHome().findAllByGroupID(groupID);
				Map submissionNames = database.getSubmissionNameMap(assignment.getAssignmentID());
				Iterator i = members.iterator();
				while (i.hasNext()) {
					GroupMemberLocal member = (GroupMemberLocal) i.next();
					recNetIDs.add(member.getNetID());
					appendReceiver(log, member.getNetID());
				}
				appendDetail(log, "Disbanded the group of " + Util.listElements(recNetIDs) + " for '" + assignment.getName() + "'");
				i = members.iterator();
				// Create a new group for each member
				boolean first = true;
				while (i.hasNext()) {
					GroupMemberLocal member = (GroupMemberLocal) i.next();
					// Everyone gets their own group (except the last person who inherits the original group)
					if (i.hasNext()) {
						GroupLocal newgroup = database.groupHome().create(group.getAssignmentID(), group.getRemainingSubmissions());
						member.setStatus("Rejected");
						newgroup.setRemainingSubmissions(group.getRemainingSubmissions());
						newgroup.setExtension(group.getExtension());
						newgroup.setLatestSubmission(group.getLatestSubmission());
						database.groupMemberHome().create(newgroup.getGroupID(), member.getNetID(), "Active");
						Iterator i2 = files.iterator();
						// Duplicate submitted files for the new groups
						while (i2.hasNext()) {
							SubmittedFileLocal file = (SubmittedFileLocal) i2.next();
							String subName = (String) submissionNames.get(new Long(file.getSubmissionID()));
							database.submittedFileHome().create(newgroup.getGroupID(), file.getOriginalGroupID(), file.getNetID(), file.getSubmissionID(), 
									file.getFileType(), file.getFileSize(), file.getMD5(), file.getLateSubmission(), 
									file.getPath(), file.getFileDate());
							if (first) {
								appendDetail(log, "(Duplicated submission for '" + subName + "' (" + DateTimeUtil.formatDate(file.getFileDate()) + ") for each group member)");
							}
						}
					}
					first = false;
				}
			}
		}
		log.setReceivingNetIDs(recNetIDs);
		database.logHome().create(log);
		return true;
	}
	
	/**
	 *  * Deletes a collection of students from a given course
	 * @param p
	 * @param netIDs
	 * @param courseID
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean deleteStudents(Principal p, Collection netIDs, long courseID) {
		try {
		    Iterator i = netIDs.iterator();
		    LogData log = startLog(p);
		    log.setCourseID(new Long(courseID));
		    log.setLogName(LogBean.DROP_STUDENTS);
		    log.setLogType(LogBean.LOG_COURSE);
		    SortedSet affectedNetIDs = new TreeSet();
		    while (i.hasNext()) {
		        String netID = (String) i.next();
		        StudentLocal student = null;
		        try {
		            student = database.studentHome().findByPrimaryKey(new StudentPK(courseID, netID));
		        } catch (Exception e) {}
		        if (student == null) {
		            return false;
		        } else {
		        	student.remove();
		        	appendDetail(log, netID + " was deleted from the course");
		        	affectedNetIDs.add(netID);
		        }	
		    }
		    log.setReceivingNetIDs(affectedNetIDs);
		    
		    database.logHome().create(log);
		    return true;
	    } catch (Exception e) {
	        e.printStackTrace();
	        ctx.setRollbackOnly();
	    }
	    return false;
	}
	
	/**
	 * Set a collection of students to dropped status in a given course
	 * @param p
	 * @param netIDs
	 * @param courseID
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean dropStudents(Principal p, Collection netIDs, long courseID) {
	    try {
		    Iterator i = netIDs.iterator();
		    LogData log = startLog(p);
		    log.setCourseID(new Long(courseID));
		    log.setLogName(LogBean.DROP_STUDENTS);
		    log.setLogType(LogBean.LOG_COURSE);
		    Collection graded = database.getGradedAssignments(netIDs, courseID);
		    SortedSet affectedNetIDs = new TreeSet();
		    while (i.hasNext()) {
		        String netID = (String) i.next();
		        StudentLocal student = null;
		        try {
		            student = database.studentHome().findByPrimaryKey(new StudentPK(courseID, netID));
		        } catch (Exception e) {}
		        if (student == null) {
		            return false;
		        } else {
		            student.setStatus(StudentBean.DROPPED);
		            appendDetail(log, netID + " was dropped from the course");
		            affectedNetIDs.add(netID);
		        }
		    }
		    log.setReceivingNetIDs(affectedNetIDs);
		    i = graded.iterator();
		    while (i.hasNext()) {
		        Long aID = (Long) i.next();
		        computeAssignmentStats(p, aID.longValue(), log);
		        appendAssignment(log, aID.longValue());
		    }
		    computeTotalScores(p, courseID, log);
		    database.logHome().create(log);
		    return true;
	    } catch (Exception e) {
	        e.printStackTrace();
	        ctx.setRollbackOnly();
	    }
	    return false;
	}
	
	/**
	 * Edits an existing announcement
	 * @param announceID ID of the announcement to edit
	 * @param message Body of the announcement
	 * @param poster Person editing this announcement
	 * @return Whether or not the edit was successful
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean editAnnouncement(Principal p, long announceID, String message, boolean remove) {
		boolean success= false;
		try {
			AnnouncementLocal announce= database.announcementHome().findByPrimaryKey(new AnnouncementPK(announceID));
			LogData log = null;
			if (!message.equals(announce.getText())) {
				String oldMessage = announce.getText() +"\n"+ announce.getEditInfo();
				database.oldAnnouncementHome().create(announceID, oldMessage);
				Timestamp now= new Timestamp(System.currentTimeMillis());
				String editInfo = "Edited by " + p.getUserID() + " on " + DateTimeUtil.DATE_TIME_AMPM.format(now);
				announce.setText(message);
				announce.setEditInfo(editInfo);
				log = startLog(p);
				log.setCourseID(new Long(announce.getCourseID()));
				log.setLogName(LogBean.EDIT_ANNOUNCEMENT);
				log.setLogType(LogBean.LOG_COURSE);
				appendDetail(log, "From: " + announce.getText());
				appendDetail(log, "To: " + message);
			}
			if (remove) {
			    announce.setHidden(true);
			    if (log == null) {
			        log = startLog(p);
			        log.setCourseID(new Long(announce.getCourseID()));
			        log.setLogName(LogBean.EDIT_ANNOUNCEMENT);
			        log.setLogType(LogBean.LOG_COURSE);
			    }
		        appendDetail(log, "Removed announcement: '" + message + "'");
			}
			if (log != null) {
				database.logHome().create(log);
			}
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
			if (ctx != null) {
				System.out.println("Error in TransactionsBean.editAnnouncement(), Rolling back...");
				ctx.setRollbackOnly();
			} else System.out.println("Error in TransactionsBean.editAnnouncement(), Could not roll back!");
		}
		return success;
	}

	/**
	 * Edit the semester with the given ID to have the properties of the given data
	 * @return Whether the operation was completed successfully
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean editSemester(Principal p, SemesterData data) {
		boolean success = true;
		try {
			SemesterLocal sem = database.semesterHome().findByPrimaryKey(new SemesterPK(data.getSemesterID()));
			boolean nameChange, hiddenChange;
			nameChange = !sem.getSemesterName().equals(data.getSemesterName());
			sem.setSemesterName(data.getSemesterName());
			hiddenChange = sem.getHidden() != data.getHidden();
			sem.setHidden(data.getHidden());
			LogData log = new LogData();
			log.setActingIPAddress(p.getIPAddress());
			log.setActingNetID(p.getUserID());
			log.setLogName(LogBean.EDIT_SEMESTER);
			log.setLogType(LogBean.LOG_ADMIN);
			Vector details = new Vector();
			if (hiddenChange) {
				details.add(new LogDetail(0, "Semester " + data.getSemesterID() + " was " + (data.getHidden() ? "hidden." : "unhidden.")));
			}
			if (nameChange) { // Name changes not actually possible currently
			    details.add(new LogDetail(0, "Semester " + data.getSemesterID() + " was renamed " + data.getSemesterName()));
			}
			log.setDetailLogs(details);
			// XXX Do we create empty log entries or not?
			database.logHome().create(log);
		} catch(Exception e) {
			success = false;
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return success;
	}
	
	public void ejbCreate() throws CreateException {
		try {
			RootLocalHome rootLocalHome = RootUtil.getLocalHome();
			//logger = LogUtil.getHome();
			database = rootLocalHome.create();
			env = new Properties();
			env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
			env.put("java.naming.provider.url", "ldap://directory.cornell.edu");
			database.ensureStartupSettings();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Make sure all users whose NetIDs are given exist in the CMS database. Do this
	 * by querying both the database and the LDAP server if necessary.  Remove
	 * any NetIDs from the input vector which cannot be added as a user (due to errors).
	 * 
	 * This function is not a transaction; it's called from within other functions
	 * that are. That's why it doesn't roll back on an exception.
	 * @param netIDs A Vector of Strings
	 * @return A TransactionResult describing the result of the transaction
	 *  
	 */
	protected TransactionResult ensureUserExistence(Vector netIDs, LogData log) {
		TransactionResult result = new TransactionResult();
		Collection LDAPInput = new ArrayList();
		Hashtable existingUsers = new Hashtable();
		try {
			for(int i = 0; i < netIDs.size(); i++) {
				String netID = (String)netIDs.get(i);
				UserLocal user = null;
				try {
				    user = database.userHome().findByUserID(netID);
				} catch (Exception e) {}
				if(user == null) LDAPInput.add(netID);
				else if(user.getFirstName() == null || user.getLastName() == null || user.getFirstName().equals("") || user.getLastName().equals("")) {
					LDAPInput.add(netID);
					existingUsers.put(netID, user);
				}
			}
			if(!LDAPInput.isEmpty()) {
				String[][] LDAPNames = null;
				try {
					result = getLDAPNames(LDAPInput);
					LDAPNames = (String[][]) result.getValue();
				} catch(NamingException e) {
				    String names = "";
				    netIDs.removeAll(LDAPInput);
				    result.addError("Could not add " + Util.listElements(LDAPInput) + ": Could not connect to LDAP");
				    return result;
				}
				for(int i=0; i < LDAPNames.length; i++) {
					String netid = LDAPNames[i][0];
					String firstName = LDAPNames[i][1];
					String lastName = LDAPNames[i][2];
					if (firstName == null || lastName == null) {
					    netIDs.remove(netid);
					    continue;
					}
					if (existingUsers.containsKey(netid)) {
						UserLocal user = (UserLocal) existingUsers.get(netid);
						if ((user.getFirstName() == null || user.getFirstName().equals("")) && !firstName.equals(""))
							user.setFirstName(firstName);
						if ((user.getLastName() == null || user.getLastName().equals("")) && !lastName.equals(""))
							user.setLastName(lastName);
					} else {
						try {
							database.userHome().create(netid, firstName, lastName, 1, null, null, null);
							appendDetail(log, "Created new user (" + lastName + ", " +  firstName + ") as " + netid);
						} catch(CreateException e) {
						    result.addError("Could not add " + netid + ": Unknown create error");
							netIDs.remove(netid);
						}
					}
				}
			}
		} catch(Exception e) {
		    e.printStackTrace();
			result.setException(e);
			result.addError("Could not add users due to an unexpected error");
			netIDs = new Vector();
		}
		return result;
	}

	
	/**
	 * Get and increment the course-specific file counter
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public long getCourseFileCounter(long courseID) throws EJBException {
		long result = 0;
		try {
			CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(courseID));
			result = course.getFileCounter();
			course.setFileCounter(result + 1);
		}
		catch (Exception e) {
			ctx.setRollbackOnly();
			e.printStackTrace();
			throw new EJBException("Failed to get a unique file counter");
		}
		return result;
	}

	
	/**
	 * check if the netID is in the database
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean checkNetID(String netID) throws EJBException {
		TransactionResult result = new TransactionResult();
		Collection LDAPInput = new ArrayList();
		try{
			
			UserLocal user = null;
			boolean found = true;
			try {
			    user = database.userHome().findByUserID(netID);
			} catch (Exception e) {}
			if(user == null){
				LDAPInput.add(netID);
				found = false;
			}
			else if(user.getFirstName() == null || user.getLastName() == null || user.getFirstName().equals("") || user.getLastName().equals("")) {
				return true;
			}
			if(found)
				return true;
			if(!LDAPInput.isEmpty()) {
				String[][] LDAPNames = null;
				result = getLDAPNames(LDAPInput);
				LDAPNames = (String[][]) result.getValue();
				for(int i=0; i < LDAPNames.length; i++) {
					String netid = LDAPNames[i][0];
					String firstName = LDAPNames[i][1];
					String lastName = LDAPNames[i][2];
					if (firstName == null || lastName == null) {
						if(netid.equals(netID))
							return false;
					}
					if (netid.equals(netID))
						return true;
				}
			}
			return false;
		}
		catch(Exception e) {
		    e.printStackTrace();
			throw new EJBException("Failed to get a unique file counter");
		}
	}

	
	/**
	 * Get and increment the group-specific file counter
	 * @param groupID The GroupID of the group
	 * @return 
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public int getGroupFileCounter(long groupID) throws EJBException {
		int result = 0;
		try {
			GroupLocal group = database.groupHome().findByGroupID(groupID);
			result = group.getFileCounter();
			group.setFileCounter(result + 1);
		}
		catch (Exception e) {
			ctx.setRollbackOnly();
			throw new EJBException("Failed to retrieve group file counter");
		}
		return result;
	}
	
	/**
	 * Auxiliary to ensureUserExistence():
	 * Query the LDAP server to find the first and last name associated with the given netID
	 * @param netids	The NetIDs of the users to find information on
	 * @return 	Returns a TransactionResult containing any errors which occurred,
	 * 	and has as it's value a two dimensional string array containing:
	 * 				result[i][0] = the ith NetID in netids (This entry is null if an error occurred at this row)
	 * 				result[i][1] = the first name given by LDAP for the ith netid
	 * 				result[i][2] = the last name given by LDAP for the ith netid
	 * 				First and last name are in the form 
	 * 					[Capital letter][all lowercase letters]
	 */
	public TransactionResult getLDAPNames(Collection netids) throws NamingException {
		TransactionResult result = new TransactionResult();
		String[][] value = new String[netids.size()][3];
		DirContext ctx = new InitialDirContext(env);
		Iterator i = netids.iterator();
		int count = 0;
		while (i.hasNext()) {
			value[count][0] = (String) i.next();
			try {
				Attributes attrs = ctx.getAttributes("uid=" + value[count][0] + ", ou=People, " + 
    				"o=Cornell University, c=US");
				if (attrs.getAll() != null) {
				    try {
				        value[count][1] = (String)attrs.get("givenName").get();
				    } catch (NullPointerException e) {}
				    try {
				        value[count][2] = (String)attrs.get("sn").get();
				    } catch (NullPointerException e) {}
				}
				//if a name comes back empty, replace with initials
				if (value[count][1] == null) {
					value[count][1] = String.valueOf(value[count][0].charAt(0)) + ".";
				}
				if (value[count][2] == null) {
					if (Character.isLetter(value[count][0].charAt(2)))
						value[count][2] = String.valueOf(value[count][0].charAt(2)) + ".";
					else
						value[count][2] = String.valueOf(value[count][0].charAt(1)) + ".";
				}
				
			} catch (NameNotFoundException e) {
				result.addError("Could not add '" + value[count][0] + "': Not a registered Cornell NetID");
				value[count][1] = null;
				value[count][2] = null;
			}
			// Correct capitalization:
			for (int j= 1; j != 3; j++) {
			    if (value[count][j] == null) continue;
			    int length= value[count][j].length();
			    if (length > 0) 
			        value[count][j]= Character.toUpperCase(value[count][j].charAt(0)) + 
			        	value[count][j].substring(1, length).toLowerCase();
			}
			count++;
		}
        ctx.close();
        result.setValue(value);
        return result;
	}
	
	/**
	 * Returns the Root object
	 * @ejb.interface-method view-type="local"
	 * @return
	 */
	public RootLocal getRoot() {
		return database;
	}
	
	/**
	 * Creates an invitation for a user to join a group.
	 * @param netid The NetID of the user to invite
	 * @param groupid The GroupID of the group
	 * @return Returns true on successful transaction
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public boolean inviteUser(Principal p, String invited, long groupid) {
		boolean success = false;
		try {
		    String inviter = p.getUserID();
			GroupMemberLocal member = null;
			try {
			    member = database.groupMemberHome().findByPrimaryKey(new GroupMemberPK(groupid, invited));
			} catch (Exception e) {}
			GroupLocal group = database.groupHome().findByPrimaryKey(new GroupPK(groupid));
			AssignmentLocal assignment = database.assignmentHome().findByGroupID(groupid);
			Collection groupMembers = database.groupMemberHome().findActiveByGroupID(groupid);
			Iterator i = groupMembers.iterator();
			SortedSet groupMembersStr = new TreeSet();
			while (i.hasNext()) {
			    GroupMemberLocal m = (GroupMemberLocal) i.next();
			    groupMembersStr.add(m.getNetID());
			}
			boolean noInvite = false;
			if (member == null) {
				member = database.groupMemberHome().create(groupid, invited, "Invited");
			}
			else if (!member.getStatus().equalsIgnoreCase("active")) {
				member.setStatus("Invited");
			} else {
			    noInvite = true;
			}
			if (!noInvite) {
				LogData log = startLog(p);
				log.setCourseID(new Long(assignment.getCourseID()));
				log.setLogName(LogBean.CREATE_INVITATION);
				log.setLogType(LogBean.LOG_GROUP);
				StudentLocal student = database.studentHome().findByPrimaryKey(new StudentPK(assignment.getCourseID(), invited));
				if (student.getEmailGroup()) {
				    sendInvitationEmail(invited, groupMembersStr, assignment.getAssignmentData(), log);
				}
				appendAssignment(log, assignment.getAssignmentID());
				appendReceiver(log, invited);
				appendDetail(log, invited + " was invited to join the group of " + Util.listElements(groupMembersStr) +  " for '" + assignment.getName() + "'");
				database.logHome().create(log);
			}
			success = true;
		}
		catch (Exception e) {
			success = false;
			ctx.setRollbackOnly();
		}
		if (!success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * See leaveGroup(String, long, LogData)
	 * @ejb.interface-method view-type="local"
	 */
	public boolean leaveGroup(Principal p, long groupid) {
	    return leaveGroup(p, groupid, null);
	}
	
	/**
	 * See TransactionHandler.leaveGroup
	 * See also TransactionsDAOImpl.leaveGroup
	 * @ejb.transaction type="Required"
	 * @param netid The NetID of the person 
	 * @return
	 */
	private boolean leaveGroup(Principal p, long groupid, LogData logIN) {
		boolean success = false;
		try {
		    String netid = p.getUserID();
			GroupMemberLocal gm = database.groupMemberHome().findByPrimaryKey(new GroupMemberPK(groupid, netid));
			AssignmentLocal assignment = database.assignmentHome().findByGroupID(groupid);
			Collection groupMembers = database.groupMemberHome().findActiveByGroupID(groupid);
			Iterator i = groupMembers.iterator();
			boolean isRootLog = logIN == null;
			LogData log = (isRootLog ? startLog(p) : logIN);
			if (isRootLog) {
			    log.setCourseID(new Long(assignment.getCourseID()));
			    log.setLogName(LogBean.LEAVE_GROUP);
			    log.setLogType(LogBean.LOG_GROUP);
			    appendAssignment(log, assignment.getAssignmentID());
			}
			SortedSet groupMembersStr = new TreeSet();
			while (i.hasNext()) {
			    GroupMemberLocal m = (GroupMemberLocal) i.next();
			    if (!m.getNetID().equals(netid)) {
			        groupMembersStr.add(m.getNetID());
			    }
			    appendReceiver(log, m.getNetID());
			}
			appendDetail(log, netid + " left the group of " + Util.listElements(groupMembersStr) + " for '" + assignment.getName() + "'");
			long assignmentid = 0, courseid = 0;
			assignmentid = assignment.getAssignmentID();
			courseid = assignment.getCourseID();
			GroupLocal oldgroup = database.groupHome().findByGroupID(groupid);
			gm.setStatus("Rejected");
			Collection remainingMembers = database.groupMemberHome().findActiveByGroupID(groupid);
			if (remainingMembers.size() < 1) {
				Collection invites = database.groupMemberHome().findByGroupIDStatus(groupid, GroupMemberBean.INVITED);
				i = invites.iterator();
				while (i.hasNext()) {
					GroupMemberLocal invited = (GroupMemberLocal) i.next();
					invited.setStatus(GroupMemberBean.REJECTED);
				}
				if (oldgroup.getTimeSlotID() != null) {
					TimeSlotLocal ts = database.timeSlotHome().findByGroupID(groupid);
					oldgroup.setTimeSlotID(null);
					ts.setPopulation(ts.getPopulation() - 1);
				}
			}
			GroupMemberLocal current = null;
			try {
			    current = database.groupMemberHome().findActiveByNetIDAssignmentID(netid, assignmentid);
			} catch (Exception e) {}
			if (current == null) {
				GroupLocal newgroup = database.groupHome().create(assignmentid, oldgroup.getRemainingSubmissions());
				GroupMemberLocal solo = database.groupMemberHome().create(newgroup.getGroupID(), netid, "Active");
				newgroup.setRemainingSubmissions(oldgroup.getRemainingSubmissions());
				newgroup.setExtension(oldgroup.getExtension());
				newgroup.setLatestSubmission(oldgroup.getLatestSubmission());
				// Duplicate Submitted Files
				Iterator files = database.submittedFileHome().findAllByGroupID(groupid).iterator();
				Map subNames = database.getSubmissionNameMap(assignment.getAssignmentID());
				while (files.hasNext()) {
					SubmittedFileLocal file = (SubmittedFileLocal) files.next();
					String fileName = ((String) subNames.get(new Long(file.getSubmissionID()))) +
							((file.getFileType()!= null && !file.getFileType().equals("")) ? ("." + file.getFileType()) : "");
					SubmittedFileLocal newfile = database.submittedFileHome().create(newgroup.getGroupID(), file.getOriginalGroupID(), file.getNetID(), file.getSubmissionID(), 
						file.getFileType(), file.getFileSize(),	file.getMD5(), file.getLateSubmission(), file.getPath(), file.getFileDate());
					appendDetail(log, "Duplicated submitted file '" + fileName + "' from group of " + Util.listElements(groupMembersStr) + " for " + netid + " for '" + assignment.getName() + "'");
				}
			}
			if (isRootLog) {
			    database.logHome().create(log);
			}
			success = true;
		} catch (Exception e) {
		    e.printStackTrace();
			success = false;
			ctx.setRollbackOnly();
		}
		if (!success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Merge all students in all indicated groups into a single group for the given assignment;
	 * assume it's valid to do so (none have been graded already, etc)
	 * @param p
	 * @param groupIDs A List of Longs
	 * @param asgnID The ID of the assignment concerned
	 * @return Success
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean mergeGroups(Principal p, List groupIDs, long asgnID) {
		boolean success = false;
		try {
			LogData log = startLog(p);
			appendAssignment(log, asgnID);
			AssignmentLocal assignment = database.assignmentHome().findByPrimaryKey(new AssignmentPK(asgnID));
			log.setCourseID(new Long(assignment.getCourseID()));
			log.setLogName(LogBean.CREATE_GROUP);
			log.setLogType(LogBean.LOG_GROUP);
			//disband all existing multiperson groups
			List groupsToDisband = new ArrayList();
			SortedSet allMembers = new TreeSet();
			for(int i = 0; i < groupIDs.size(); i++) {
				Collection members = database.groupMemberHome().findActiveByGroupID(((Long)groupIDs.get(i)).longValue());
				if(members.size() > 1)
					groupsToDisband.add((Long)groupIDs.get(i));
				Iterator j = members.iterator();
				while(j.hasNext())
					allMembers.add(((GroupMemberLocal)j.next()).getNetID());
			}
			privateDisbandGroups(p, groupsToDisband, asgnID);
			//if success so far (no exception), form a new supergroup (let's call it ABBA)
			privateCreateGroup(p, allMembers, asgnID); //you are the coding queen...
			//finalize log properties
			log.setReceivingNetIDs(allMembers);
			database.logHome().create(log);
			success = true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			success = false;
			ctx.setRollbackOnly();
		}
		if(!success) ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Posts a new announcement
	 * @param courseID ID of the course in which to post the announcement
	 * @param message Body of the announcement
	 * @param poster Person who posted the announcement
	 * @return Whether or not the post was successful
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean postAnnouncement(Principal p, long courseID, String message) {
		boolean success= false;
		try {
		    String poster = p.getUserID();
		    LogData log = new LogData();
		    log.setActingIPAddress(p.getIPAddress());
		    log.setActingNetID(p.getPrincipalID());
		    log.setCourseID(new Long(courseID));
		    log.setLogName(LogBean.CREATE_ANNOUNCEMENT);
		    log.setLogType(LogBean.LOG_COURSE);
			Vector details = new Vector();
		    AnnouncementLocal announce= database.announcementHome().create(courseID, poster, message);
		    details.add(new LogDetail(0, "Created AnnouncementID=" + announce.getAnnouncementID()));
		    log.setDetailLogs(details);
		    database.logHome().create(log);
		    success= true;
		} catch (Exception e) {
		    e.printStackTrace();
			if (ctx != null) {
				System.out.println("Error in TransactionsBean.postAnnouncement(), Rolling back...");
				ctx.setRollbackOnly();
			} else System.out.println("Error in TransactionsBean.postAnnouncement(), Could not roll back!");
		}
		return success;
	}
	
	/**
	 * Removes (Hides) an assignment and recomputes total scores as neccessary
	 * @param p
	 * @param assignmentID
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult removeAssignment(Principal p, long assignmentID) {
	    TransactionResult result = new TransactionResult();
	    try {
	        AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(assignmentID);
	        assignment.setHidden(true);
	        LogData log = startLog(p);
	        log.setCourseID(new Long(assignment.getCourseID()));
	        appendAssignment(log, assignmentID);
	        log.setLogName(LogBean.REMOVE_ASSIGNMENT);
	        log.setLogType(LogBean.LOG_COURSE);
	        appendDetail(log, "'" + assignment.getName() + "' was removed from the course");
	        computeTotalScores(p, assignment.getCourseID(), log);
	        database.logHome().create(log);
	    } catch (Exception e) {
	        e.printStackTrace();
			result.setException(e);
	        result.addError("Unexpected error, course not remove assignment");
	    }
	    return result;
	}

	/**
	 * @param p
	 * @param categoryID
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean removeCategory(Principal p, long categoryID) {
	    try {
	        CategoryLocal cat = database.categoryHome().findByPrimaryKey(new CategoryPK(categoryID));
	        cat.setHidden(true);
	        LogData log = startLog(p);
	        log.setCourseID(new Long(cat.getCourseID()));
	        log.setLogName(LogBean.REMOVE_CATEGORY);
	        log.setLogType(LogBean.LOG_CATEGORY);
	        appendDetail(log, "Removed content with ID " + categoryID);
	        database.logHome().create(log);
	    } catch (Exception e) {
	        ctx.setRollbackOnly();
	        e.printStackTrace();
	        return false;
	    }
	    return true;
	}
	
	/**
	 * Remove a CMS admin from the list
	 * Precondition: the given NetID belongs to a current CMS admin
	 * @param netID
	 * @return Whether the operation was completed successfully
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean removeCMSAdmin(Principal p, String netID) {
		boolean success = true;
		try {
		    TreeSet n = new TreeSet();
		    n.add(netID);
		    LogData log = startLog(p);
		    log.setLogName(LogBean.REMOVE_CMS_ADMIN);
		    log.setLogType(LogBean.LOG_ADMIN);
		    log.setReceivingNetIDs(n);
		    appendDetail(log, "Removed " + netID + " from the CMS admin list");
			database.cmsAdminHome().findByPrimaryKey(new CMSAdminPK(netID)).remove();
			database.logHome().create(log);
		} catch(Exception e) {
			success = false;
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * @param p
	 * @param rowID
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean removeCtgRow(Principal p, long rowID) {
	    try {
	        CategoryRowLocal cr = database.categoryRowHome().findByPrimaryKey(new CategoryRowPK(rowID));
	        CategoryLocal cat = database.categoryHome().findByPrimaryKey(new CategoryPK(cr.getCategoryID()));
	        cr.setHidden(true);
	        LogData log= startLog(p);
	        log.setCourseID(new Long(cat.getCourseID()));
	        log.setLogName(LogBean.REMOVE_CATEGORY_ROW);
	        log.setLogType(LogBean.LOG_CATEGORY);
	        appendDetail(log, "Removed content row with ID " + rowID);
	        database.logHome().create(log);
	    } catch (Exception e) {
	        ctx.setRollbackOnly();
	        e.printStackTrace();
	        return false;
	    }
	    return true;
	}

	/**
	 * Removes an extension previously granted to a group
	 * @param p
	 * @param groupID
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult removeExtension(Principal p, long groupID) {
	    TransactionResult result = new TransactionResult();
	    try {
	        GroupLocal group = database.groupHome().findByGroupID(groupID);
	        AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(group.getAssignmentID());
	        long assignID = assignment.getAssignmentID();
	        Iterator members = database.groupMemberHome().findActiveByGroupID(groupID).iterator();
	        LogData log = startLog(p);
	        log.setCourseID(new Long(assignment.getCourseID()));
	        log.setLogName(LogBean.REMOVE_EXTENSION);
	        log.setLogType(LogBean.LOG_GROUP);
	        String mems = "";
	        while (members.hasNext()) {
	            GroupMemberLocal mem = (GroupMemberLocal) members.next();
	            LogDetail d = new LogDetail(0);
	            d.setAssignmentID(new Long(assignID));
	            d.setNetID(mem.getNetID());
	            appendDetail(log, d);
	            mems += mems.equals("") ? mem.getNetID() : ", " + mem.getNetID();
	        }
	        appendDetail(log, "Removed extension from " + mems + " on '" + assignment.getName() + "'");
	        group.setExtension(null);
	        database.logHome().create(log);
	    } catch (Exception e) {
	        e.printStackTrace();
	        ctx.setRollbackOnly();
			result.setException(e);
	        result.addError("An unexpected error occurred, could not remove extension");
	    }
	    return result;
	}
	
	/**
	 * Removes one or more checked timeslots from a course schedule
	 * @param p Principal of user
	 * @param assignID Assignment ID
	 * @param arr Set of timeslots to be deleted
	 * @return Whether or not transaction was successful
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean removeTimeSlots(Principal p, long assignID, ArrayList arr) {
		boolean success = false;
		try {
			AssignmentLocal assign = database.assignmentHome().findByAssignmentID(assignID);
			LogData log = startLog(p);
			log.setCourseID(new Long(assign.getCourseID()));
			appendAssignment(log, assignID);
			log.setLogName(LogBean.REMOVE_TIMESLOTS);
			log.setLogType(LogBean.LOG_COURSE);
			// iterate over all timeslots to be removed
			for (int i = 0; i < arr.size(); i++){
				TimeSlotLocal ts = (TimeSlotLocal) arr.get(i);
				ts.setHidden(true);
				// find all groups in the timeslot
				Collection groups = database.groupHome().findByTimeSlotID(ts.getTimeSlotID());
				Iterator ite = groups.iterator();
				ArrayList observedGroups = new ArrayList();
				while (ite.hasNext()){
					GroupLocal g = (GroupLocal) ite.next();
					if (!observedGroups.contains(g)){
						// remove their timeslot status
						g.setTimeSlotID(null);
						// send notifications as applicable
						sendTSGroupChangeEmails(null,g.getGroupID(),assign.getAssignmentData(),false,log);
						observedGroups.add(g);
					}
				}
				appendDetail(log, "Deleted TimeSlot with index " + ts.getTimeSlotID() + " from schedule");
			}
			database.logHome().create(log);
			success = true;
		} catch (Exception e) { 
			success = false; 
			e.printStackTrace();
		}
		if (!success) ctx.setRollbackOnly();
		return success;
	}

	/**
	 * Restores a hidden announcement
	 * @param p
	 * @param announceID
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult restoreAnnouncement(Principal p, long announceID) {
	    TransactionResult result = new TransactionResult();
	    try {
	        AnnouncementLocal announce = database.announcementHome().findByPrimaryKey(new AnnouncementPK(announceID));
	        announce.setHidden(false);
	        LogData log = startLog(p);
	        log.setCourseID(new Long(announce.getCourseID()));
	        log.setLogName(LogBean.RESTORE_ANNOUNCEMENT);
	        log.setLogType(LogBean.LOG_COURSE);
	        appendDetail(log, "Restored annoucement: '" + announce.getText() + "'");
	        database.logHome().create(log);
	    } catch (Exception e) {
	        e.printStackTrace();
	        ctx.setRollbackOnly();
			result.setException(e);
	        result.addError("An unexpected error occurred, could not restore announcement");
	    }
	    return result;
	}
	
	/**
	 * Restores a hidden assignment and recomputes total scores as necessary
	 * @param p
	 * @param assignmentID
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult restoreAssignment(Principal p, long assignmentID) {
	    TransactionResult result = new TransactionResult();
	    try {
	        AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(assignmentID);
	        assignment.setHidden(false);
	        LogData log = startLog(p);
	        log.setCourseID(new Long(assignment.getCourseID()));
	        appendAssignment(log, assignmentID);
	        log.setLogName(LogBean.RESTORE_ASSIGNMENT);
	        log.setLogType(LogBean.LOG_COURSE);
	        appendDetail(log, "'" + assignment.getName() + "' was restored");
	        computeTotalScores(p, assignment.getCourseID(), log);
	        database.logHome().create(log);
	    } catch (Exception e) {
	        e.printStackTrace();
			result.setException(e);
	        result.addError("Unexpected error, course not remove assignment");
	    }
	    return result;
	}
	
	/**
	 * Add new CMS User
	 * 
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public TransactionResult addNewCMSUser(Principal p, String netid, String firstname, String lastname, int domainID, String email) {
		Profiler.enterMethod("TransactionBean.addNewCMSUser", "userID: " + netid);
		TransactionResult result = new TransactionResult();
		
		LogData log = startLog(p);
		log.setLogName(LogBean.CREATE_CMS_USER);
		log.setLogType(LogBean.LOG_ADMIN);
		
		UserLocal user = null;
		try
		{
			user = database.userHome().create(netid, firstname, lastname, domainID, email, null, null);
		}
		catch(Exception e)
		{
			result.addError(e.getMessage());
			return result;
		}
		
		String newPW = user.resetPassword();
		user.setPWExpired(true);
		
		if(!result.hasErrors())
		{
			result.setValue(user);
			appendDetail(log, "Added new CMS user: " + netid);
			try{
				sendUserAddedToCMSEmail(netid, newPW, log);
			}
			catch(Exception e)
			{
				result.addError(e.getMessage());
			}
			try
			{
				if (log.getDetailLogs() != null && log.getDetailLogs().size() > 0) {
				    database.logHome().create(log);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		newPW = null;
		Profiler.exitMethod("TransactionBean.addNewCMSUser", "userID: " + netid);
		return result;
	}
	
	/**
	 * Resets a user's password with a randomly generated password.
	 * Then emails the user with the new password.
	 * The user will be required to change their password at their next login.
	 * 
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public TransactionResult resetUserPassword(Principal p, String netid) {
		Profiler.enterMethod("TransactionBean.resetUserPassword", "userID: " + netid);
		TransactionResult result = new TransactionResult();
		
		LogData log = startLog(p);
		log.setLogName(LogBean.UPDATE_USER_PW);
		log.setLogType(LogBean.LOG_ADMIN);
		
		UserLocal user = null;
		try
		{
			user = database.userHome().findByUserID(netid);
		}
		catch(FinderException e)
		{
			result.addError("Could not find user in database. User update failed.");
			return result;
		}
		
		String newPW = user.resetPassword();
		user.setIsDeactivated(false); //set deactivated to false, in case of re-activation.
		appendDetail(log, "Reset PW for user: " + netid);
		try
		{
			database.logHome().create(log);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		try {
			TransactionResult emailResult = sendUserPWResetEmail(p, newPW, netid);
			result.appendErrors(emailResult);
		} catch (FinderException e) {
			result.addError("Error sending PWReset email to " + netid);
		}
		
		newPW = null;
		Profiler.exitMethod("TransactionBean.resetUserPassword", "userID: " + netid);
		return result;
	}
	
	/**
	 * This function will add the emails of the users with the given netIDs to the emailer.
	 * If includeCornellUsers is true, then it will include any users that are Cornell (domain = 1) users
	 * If includeExternalUsers is true, then it will include any external (non-Cornell) users
	 * These booleans can be used to email just the users that are in one category or both.
	 */
	private String addUserEmails(Collection netIDs, Emailer emailer, boolean includeCornellUsers, boolean includeExternalUsers) throws FinderException
	{
		String[] netIDStrings = new String[netIDs.size()];
		Iterator i = netIDs.iterator();
		int index = 0;
		
        while (i.hasNext()) {
        	
        	Object nextObj = i.next();
        	
        	if(nextObj instanceof StudentLocal)
        	{
        		StudentLocal student = (StudentLocal) nextObj;
        		netIDStrings[index] = student.getUserID();
        	}
        	else if(nextObj instanceof StaffLocal)
        	{
        		StaffLocal staff = (StaffLocal) nextObj;
        		netIDStrings[index] = staff.getNetID();
        	}
        	else if(nextObj instanceof String)
        	{
        		netIDStrings[index] = (String) nextObj;
        	}
        	index++;
        }
        
		Collection users = database.userHome().findByNetIDs(netIDStrings);
		String addedUsers = "";
		i = users.iterator();
		while(i.hasNext())
		{
			UserLocal user = (UserLocal) i.next();
			if((user.getDomainID() == 1 && includeCornellUsers) || (user.getDomainID() != 1 && includeExternalUsers))
			{
				addedUsers += addedUsers.equals("") ? user.getUserID() : ", " + user.getUserID();
				emailer.addTo(user.getEmail());
			}
		}
		return addedUsers;
	}
	
	/**
	 * Used to send out emails that go out to individual users
	 * Takes care of setting proper links for external users
	 *  
	 * @param course - specific course this email is regarding, use null if no specific course
	 * @param user - recipient
	 * @param subject - email subject
	 * @param baseMsg - base message body
	 * @param queryString - querystring portion of the link you want included with the email, leave blank or null if no link desired
	 * @param logMsg - message you want logged.
	 * @param log
	 * @throws ConnectException
	 * @throws FinderException
	 */
	private void sendSingleEmailToIndividual(CourseLocal course, UserLocal user, String subject, String baseMsg, String queryString, String logMsg, LogData log) throws ConnectException, FinderException
	{
		Emailer email = new Emailer();
        email.setFrom("CMS _do not reply_ <cms-devnull@csuglab.cornell.edu>");
        email.setSubject(subject);
        
        String serverAddress = "";
        String queryStringExternalUserFlag = "";
        if(user.getDomainID() == 1){ //if cornell user
        	//change server address to make sure any email generated by external users prompt Cornell users for kerberos auth
        	serverAddress = getCMSServerAddress().replaceFirst("guest","auth");
        }
        else{ //if external user
        	//change server address to make sure any email generated by cornell users do not prompt external users for kerberos auth
        	serverAddress = getCMSServerAddress().replaceFirst("auth","guest");
        	
        	//if base link is not already to the login page, add flag to redirect to external login page
        	if(queryString.indexOf(AccessController.ACT_EXTERNALLOGIN) < 0)
        		queryStringExternalUserFlag = "&" + AccessController.P_ISEXTERNALUSERLINK + "=true&" + AccessController.P_DOMAINID + "=" + user.getDomainID();
        }
        
        String msg = baseMsg;
        if(queryString.length() > 0) //if there is a link to be put in 
        	msg += "   " + serverAddress + queryString + queryStringExternalUserFlag;
        
        //add message footer
        if (course != null)
        	email.setMessage(Emailer.appendEmailFooter(msg, course.getCode()));
        else
        {
        	msg += "\n\n\n" +
    				"-----------------------------------\n" +
    				"This message was auto-generated by Cornell CMS.  Do not reply to this message directly.  ";
        	email.setMessage(msg);
        }
        
        email.addTo(user.getEmail());
        
        if (!email.sendEmail()) throw new ConnectException("");
        appendDetail(log, "Sent New Invitation Email to " + user.getUserID());
	}
	
	/**
	 * Used to send out emails that go out to groups of users
	 * Sends one email to Cornell users, and another to any non-Cornell users, each with the proper links for logging in
	 *  
	 * @param course - specific course this email is regarding, use null if no specific course
	 * @param user - recipient
	 * @param subject - email subject
	 * @param baseMsg - base message body
	 * @param queryString - querystring portion of the link you want included with the email, leave blank or null if no link desired
	 * @param logMsg - message you want logged.
	 * @param log
	 * @throws ConnectException
	 * @throws FinderException
	 */
	private void sendMassEmailToGroup(CourseLocal course, Collection group, String subject, String baseMsg, String queryString, String logMsg, String logMsgUserType, LogData log) throws ConnectException, FinderException
	{
		/* when i=1, send email to just Cornell users (no external users) 
    	 * when i=2, send email to just external users, they require special links that allow external logins
    	 * */
    	for(int i=1; i <= 2; i++)
    	{
	        Emailer email = new Emailer();
	        email.setFrom("CMS _do not reply_ <cms-devnull@csuglab.cornell.edu>");
	        email.setSubject(subject);
	        
	        String serverAddress = "";
	        String queryStringExternalUserFlag = "";
	        boolean includeCornellUsers; //flag for adding different users to email recipients list
	        String logUserTypeQualifier = "";
	        if(i==1){
	        	//change server address to make sure any email generated by external users prompt Cornell users for kerberos auth
	        	serverAddress = getCMSServerAddress().replaceFirst("guest","auth");
	        	includeCornellUsers = true;
	        }
	        else{ 
	        	//change server address to make sure any email generated by cornell users do not prompt external users for kerberos auth
	        	serverAddress = getCMSServerAddress().replaceFirst("auth","guest");

	        	//if base link is not already to the login page, add flag to redirect to external login page
	        	if(queryString.indexOf(AccessController.ACT_EXTERNALLOGIN) < 0)
	        		queryStringExternalUserFlag = "&" + AccessController.P_ISEXTERNALUSERLINK + "=true";
	        	
	        	includeCornellUsers = false;
	        	logUserTypeQualifier = "non-";
	        }
	        
	        String msg = baseMsg;
	        if(queryString.length() > 0) //if there is a link to be put in 
	        	msg += "   " + serverAddress + queryString + queryStringExternalUserFlag;

	        //add message footer
	        if (course != null)
	        	email.setMessage(Emailer.appendEmailFooter(msg, course.getCode()));
	        else
	        {
	        	msg += "\n\n\n" +
        				"-----------------------------------\n" +
        				"This message was auto-generated by Cornell CMS.  Do not reply to this message directly.  ";
	        	email.setMessage(msg);
	        }
	        
	        String netids = addUserEmails(group, email, includeCornellUsers, !includeCornellUsers);
	        
            if (!email.sendEmail()) throw new ConnectException("");
            if (netids.length() > 0) {
            	appendDetail(log, logMsg + logUserTypeQualifier + "Cornell " + logMsgUserType + ": " + netids);
            }
        }
	}
	
	/**
	 * Sends email to newly added user of CMS
	 * @ejb.interface-method view-type="local"
	 */
	public void sendUserAddedToCMSEmail(String netID, String newPW, LogData log) throws FinderException {
		UserLocal user = database.userHome().findByUserID(netID);
		try {
	    	
			//Construct base message + subject + log message + link queryString
	    	String baseMsg = "You have been added to the Cornell CMS.\n\n";
	    	baseMsg += "Your Username: " + user.getUserID() + "\n";
	    	baseMsg += "Your Password: " + newPW + "\n";
	    	String subject = "Welcome to Cornell CMS";
	    	String logMsg = "Sent Added-to-CMS Email to ";
	    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_EXTERNALLOGIN + "&" + AccessController.P_DOMAINID + "=" + user.getDomainID();
	    	
	    	sendSingleEmailToIndividual(null, user, subject, baseMsg, queryString, logMsg, log);
	        
	    } catch (ConnectException e) {
            appendDetail(log, "Failed to create a connection for sending email.  Could not send Added-to-CMS email to " + netID + ".");	        
	    }
	    newPW = null;
	}
	
	/**
	 * Sends email when pw has been reset.
	 * @ejb.interface-method view-type="local"
	 */
	public TransactionResult sendUserPWResetEmail(Principal p, String newPW, String netID) throws FinderException {
		TransactionResult result = new TransactionResult();
		UserLocal user = database.userHome().findByUserID(netID);
		LogData log = startLog(p);
		log.setLogName(LogBean.CREATE_CMS_USER);
		log.setLogType(LogBean.LOG_ADMIN);
		try {

			//Construct base message + subject + log message + link queryString
	    	String baseMsg = "Your password has been reset.\n\n";
	    	baseMsg += "Your Username: " + user.getUserID() + "\n";
	    	baseMsg += "Your new Password: " + newPW + "\n";
	    	String subject = "[Cornell CMS] Your Password";
	    	String logMsg = "PW Reset Email to ";
	    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_EXTERNALLOGIN + "&" + AccessController.P_DOMAINID + "=" + user.getDomainID();
	    	
	    	sendSingleEmailToIndividual(null, user, subject, baseMsg, queryString, logMsg, log);
	    	
	    } catch (ConnectException e) {
            appendDetail(log, "Failed to create a connection for sending email.  Could not send PWReset-to-CMS email to " + netID + ".");
            result.setException(e);
            result.addError("Failed to create a connection for sending email.  Could not send PWReset-to-CMS email to " + netID + ".");
	    }
	    newPW = null;
	    return result;
	}
	
	public void sendAcceptInviteEmail(String accepter, Collection groupMems, AssignmentData assign, LogData log) throws FinderException {
        CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(assign.getCourseID()));
	    UserLocal user = database.userHome().findByUserID(accepter);
        try {
        	
        	//Construct base message + subject + log message + link queryString
	    	String baseMsg = user.getFirstName() + " " + user.getLastName() + " (" + accepter + ") has joined your group for " + assign.getName() + ".\n\n";
	    	baseMsg += "To view this assignment, navigate to the following page:\n";
	    	String subject = "[" + course.getCode() + "]" + " - Your Invitation was Acccepted for " + assign.getName();
	    	String logMsg = "Sent Accepted Invitation Email to the following ";
	    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_ASSIGN + "&" + AccessController.P_ASSIGNID + "=" + assign.getAssignmentID();
	    	
	    	sendMassEmailToGroup(course, groupMems, subject, baseMsg, queryString, logMsg, "users", log);
	    	
	    } catch (ConnectException e) {
            appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic Accepted Invitation emails to " + Util.listElements(groupMems) + ".");	        
	    }
	}
	
	public void sendAddedToCourseEmail(String netID, CourseLocal course, LogData log) throws FinderException {
		UserLocal user = database.userHome().findByUserID(netID);
		try {
			
			//Construct base message + subject + log message + link queryString
	    	String baseMsg = "You have been added to the CMS roster for the course " + course.getName() + ".\n\n";
	    	baseMsg += "By default, you will receive some notifications about this course, such as when new assignments are released or your group is changed.  To change these e-mail preferences, navigate to the following page:\n";
	    	String subject = "[" + course.getCode() + "]" + " - You Have Been Added to " + course.getName();
	    	String logMsg = "Sent Added-to-Course Email to ";
	    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_STUDENTPREFS + "&" + AccessController.P_COURSEID + "=" + course.getCourseID();
	    	
	    	sendSingleEmailToIndividual(course, user, subject, baseMsg, queryString, logMsg, log);
			
	    } catch (ConnectException e) {
            appendDetail(log, "Failed to create a connection for sending email.  Could not send Added-to-Course email to " + netID + ".");	        
	    }
	}
	
	public void sendGroupSubmitEmail(String netID, AssignmentData assign, LogData log) throws FinderException {
		CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(assign.getCourseID()));
		UserLocal user = database.userHome().findByUserID(netID);
		try {

			//Construct base message + subject + log message + link queryString
	    	String baseMsg = "Your group has submitted one or more files for " + assign.getName() + ".\n\n";
	    	baseMsg += "To view this assignment, navigate to the following page:\n";
	    	String subject = "[" + course.getCode() + "]" + " - Your Group has Submitted Files for " + assign.getName();
	    	String logMsg = "Sent New Assigned To Email to ";
	    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_ASSIGN + "&" + AccessController.P_ASSIGNID + "=" + assign.getAssignmentID();
	    	
	    	sendSingleEmailToIndividual(course, user, subject, baseMsg, queryString, logMsg, log);
	    	
		} catch (ConnectException e) {
			appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic New Assignment Submission email to " + netID + ".");
		}
		
	}

	public void sendAssignedEmail(String grader, AssignmentData assign, int groupCount, LogData log) throws FinderException {
        CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(assign.getCourseID()));
        UserLocal user = database.userHome().findByUserID(grader);
	    try {

			//Construct base message + subject + log message + link queryString
	    	String baseMsg = "You have been assigned to grade " + groupCount + " groups in " + assign.getName() + ".\n\n";
	    	baseMsg += "To begin grading these groups, navigate to the following page:\n";
	    	String subject = "[" + course.getCode() + "]" + " - You Have Been Assigned to Grade " + assign.getName();
	    	String logMsg = "Sent New Assigned To Email to ";
	    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_GRADEASSIGN + "&" + AccessController.P_ASSIGNID + "=" + assign.getAssignmentID();
	    	
	    	sendSingleEmailToIndividual(course, user, subject, baseMsg, queryString, logMsg, log);
	    	
	    } catch (ConnectException e) {
            appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic New Assigned To emails to " + grader + ".");	        
	    }
	}
	
	public void sendAssignSubmitEmail(String grader, AssignmentData assign, long groupID, LogData log) throws FinderException {
		CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(assign.getCourseID()));
		UserLocal user = database.userHome().findByUserID(grader);
		try {

			//Construct base message + subject + log message + link queryString
	    	String baseMsg = "A group you have been assigned to grade in " + assign.getName() + " has submitted all required files.\n\n";
	    	baseMsg += "To begin grading this group, navigate to the following page:\n";
	    	String subject = "[" + course.getCode() + "]" + " - A Group You Are Assigned to Grade Submitted Files in " + assign.getName();
	    	String logMsg = "Sent New Assigned To Email to ";
	    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_GRADESTUDENTS + "&" + AccessController.P_ASSIGNID + "=" + assign.getAssignmentID() + "&" + AccessController.P_GROUPID + "=" + groupID;
	    	
	    	sendSingleEmailToIndividual(course, user, subject, baseMsg, queryString, logMsg, log);
	    	
		} catch (ConnectException e) {
			appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic New Assignment Submission email to " + grader + ".");
		}
		
	}
	
	public void sendDeclineInviteEmail(String decliner, Collection groupMems, AssignmentData assign, LogData log) throws FinderException {
        CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(assign.getCourseID()));
	    UserLocal user = database.userHome().findByUserID(decliner);
        try {
        	
        	//Construct base message + subject + log message + link queryString
	    	String baseMsg = user.getFirstName() + " " + user.getLastName() + " (" + decliner + ") has declined the invitation to join your group for " + assign.getName() + ".\n\n";
	    	baseMsg += "To view this assignment, navigate to the following page:\n";
	    	String subject = "[" + course.getCode() + "]" + " - Your Invitation was Declined for " + assign.getName();
	    	String logMsg = "Sent Declined Invitation Email to the following ";
	    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_ASSIGN + "&" + AccessController.P_ASSIGNID + "=" + assign.getAssignmentID();
	    	
	    	sendMassEmailToGroup(course, groupMems, subject, baseMsg, queryString, logMsg, "users", log);
	    	
	    } catch (ConnectException e) {
            appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic Declined Invitation emails to " + Util.listElements(groupMems) + ".");	        
	    }
	}
	
	public void sendDueDateEmails(long courseID, AssignmentData assign, LogData log) throws FinderException {
	    Collection students = database.studentHome().findEmailDueDate(courseID);
	    Collection staffs = database.staffHome().findEmailDueDates(courseID);
	    if (students.size() > 0 || staffs.size() > 0) {
	        CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(courseID));
	        try {
	        	
	        	//Construct base message + subject + log message + link queryString
		    	String baseMsg = "The due date for " + assign.getName() + " is now " + DateTimeUtil.formatDate(assign.getDueDate()) + ".\n\n";
		    	baseMsg += "To view this assignment, navigate to the following page:\n";
		    	String subject = "[" + course.getCode() + "]" + " - The due date for " + assign.getName() + " has changed";
		    	String logMsg = "Sent Due Date Change email to the following ";
		    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_ASSIGN + "&" + AccessController.P_ASSIGNID + "=" + assign.getAssignmentID();
		    	
		    	sendMassEmailToGroup(course, students, subject, baseMsg, queryString, logMsg, "students", log);
		    	sendMassEmailToGroup(course, staffs, subject, baseMsg, queryString, logMsg, "staff members", log);
	        	
	        } catch (ConnectException e) {
	            appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic Due Date Change emails to staff and students.");
	        }
	    }
	}
	
	public void sendFinalGradeEmails(CourseLocal course, LogData log) throws FinderException {
	    Collection students = database.studentHome().findEmailFinalGrades(course.getCourseID());
	    Collection staffs = database.staffHome().findEmailFinalGrades(course.getCourseID());
	    if (students.size() > 0 || staffs.size() > 0) {
	        try {
	        	
	        	//Construct base message + subject + log message + link queryString
		    	String baseMsg = "Final Grades are now available on CMS for " + course.getCode() + ".\n\n";
		    	baseMsg += "To see your final grade, navigate to the following page:\n";
		    	String subject = "[" + course.getCode() + "]" + " - Final Grades Released";
		    	String logMsg = "Sent Final Grade email to the following ";
		    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_COURSE + "&" + AccessController.P_COURSEID + "=" + course.getCourseID();
		    	
		    	sendMassEmailToGroup(course, students, subject, baseMsg, queryString, logMsg, "students", log);
		    	sendMassEmailToGroup(course, staffs, subject, baseMsg, queryString, logMsg, "staff members", log);
	            
	        } catch (ConnectException e) {
	            appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic Final Grade emails to staff and students.");
	        }
	    }
	}
	
	/**
	 * Sends an email
	 * @param p
	 * @param courseID
	 * @param email
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult sendEmail(Principal p, long courseID, Emailer email) {
	    TransactionResult result = new TransactionResult();
	    try {
	        try {
	            email.sendEmail();
	        } catch (Exception e) {
	            result.addError("Failed to send email");
	            return result;
	        }
	        database.emailHome().create(courseID, p.getUserID(), email.getSubject(), email.getMessage(), 
	                email.getRecipient().equals("staff") ? EmailBean.STAFF
	                : (email.getRecipient().equals("students") ? EmailBean.STUDENTS
	                : (email.getRecipient().equals("all") ? EmailBean.ALL
	                : EmailBean.CUSTOM)));
	        LogData log = startLog(p);
	        log.setCourseID(new Long(courseID));
	        log.setLogName(LogBean.SEND_EMAIL);
	        log.setLogType(LogBean.LOG_COURSE);
	        appendDetail(log, "Created email with subject: '" + email.getSubject() + "'");
	        appendDetail(log, "Sent email to " +
	        		(email.getRecipient().equals("staff") ? "all staff members"
	        		: (email.getRecipient().equals("students") ? "all enrolled students"
	        		: (email.getRecipient().equals("all")) ? "all staff and students"
	        		: email.getRecipient() + " - " + email.getToAddrString())));
	        database.logHome().create(log);
	        result.setValue("Email sent successfully");
	    } catch (Exception e) {
	        e.printStackTrace();
			result.setException(e);
	        result.addError("Successfully sent email, failed to log event");
	    }
	    return result;
	}
	
	public void sendGradeReleaseEmails(AssignmentData assign, LogData log) throws FinderException {
	    Collection students = database.studentHome().findEmailGrades(assign.getCourseID());
	    if (students.size() > 0) {
	        CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(assign.getCourseID()));
	        try {
	        	
	        	//Construct base message + subject + log message + link queryString
		    	String baseMsg = "Grades are now available to students for " + assign.getName() + ".  If you do not see your grades when you log on, the staff have not assigned you a grade yet.";
	            if (assign.getShowSolution()) {
	            	baseMsg += "  Solutions are also available online.";
	            }
	            baseMsg += "\n\nTo view this assignment, navigate to the following page:\n";
		    	String subject = "[" + course.getCode() + "]" + " - Grades Released for " + assign.getName();
		    	String logMsg = "Sent Grade Release email to the following ";
		    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_ASSIGN + "&" + AccessController.P_ASSIGNID + "=" + assign.getAssignmentID();
		    	
		    	sendMassEmailToGroup(course, students, subject, baseMsg, queryString, logMsg, "students", log);
	        	
	        } catch (ConnectException e) {
	            appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic Grade Release emails to students.");
	        }
	    }
	}
	
	public void sendInvitationEmail(String invited, Collection groupMems, AssignmentData assign, LogData log) throws FinderException {
        CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(assign.getCourseID()));
        UserLocal user = database.userHome().findByUserID(invited);
        try {

			//Construct base message + subject + log message + link queryString
	    	String baseMsg = "You have been invited to join the group of " + Util.listElements(groupMems) + " for " + assign.getName() + ".\n\n";
	    	baseMsg += "To view this assignment, navigate to the following page:\n";
	    	String subject = "[" + course.getCode() + "]" + " - You Received an Invitation for " + assign.getName();
	    	String logMsg = "Sent New Invitation Email to ";
	    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_ASSIGN + "&" + AccessController.P_ASSIGNID + "=" + assign.getAssignmentID();
	    	
	    	sendSingleEmailToIndividual(course, user, subject, baseMsg, queryString, logMsg, log);
	    	
	    } catch (ConnectException e) {
            appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic New Invitation email to " + invited + ".");	        
	    }
	}
	
	public void sendNewAssignEmails(long assignID, long courseID, AssignmentData assign, LogData log) throws FinderException {
	    Collection students = database.studentHome().findEmailNewAssigns(courseID);
	    Collection staffs = database.staffHome().findEmailNewAssigns(courseID);
	    if (students.size() > 0 || staffs.size() > 0) {
	        CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(courseID));
	        try {

		    	//Construct base message + subject + log message + link queryString
		    	String baseMsg = assign.getName() + " has been released and is due " + DateTimeUtil.formatDate(assign.getDueDate()) + ".\n\n";
		    	baseMsg += "To view this assignment, navigate to the following page:\n";
		    	String subject = "[" + course.getCode() + "]" + " - New Assignment Released";
		    	String logMsg = "Sent New Assignment email to the following ";
		    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_ASSIGN + "&" + AccessController.P_ASSIGNID + "=" + assignID;
		    	
		    	sendMassEmailToGroup(course, students, subject, baseMsg, queryString, logMsg, "students", log);
		    	sendMassEmailToGroup(course, staffs, subject, baseMsg, queryString, logMsg, "staff members", log);
		    	
	        } catch (ConnectException e) {
	            appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic New Assignment emails to staff and students.");
	        }
	    }
	}
	
	public void sendRegradedEmail(Collection groupIDs, AssignmentData assign, LogData log) throws FinderException {
	    CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(assign.getCourseID()));
	    Collection students = database.studentHome().findEmailRegrade(groupIDs);
	    if (students.size() > 0) {
		    try {
		    	
		    	//Construct base message + subject + log message + link queryString
		    	String baseMsg = "A staff member has responded to your regrade request for " + assign.getName() + ".\n\n";
		    	baseMsg += "To view this assignment, navigate to the following page:\n";
		    	String subject = "[" + course.getCode() + "]" + " - Your Regrade Request for " + assign.getName() + " has Received a Response";
		    	String logMsg = "Sent Regrade Response Email to ";
		    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_ASSIGN + "&" + AccessController.P_ASSIGNID + "=" + assign.getAssignmentID();
		    	
		    	sendMassEmailToGroup(course, students, subject, baseMsg, queryString, logMsg, "students", log);
	            
		    } catch (ConnectException e) {
	            appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic Regrade Response emails to students.");	        
		    }	        
	    }
	}
	
	public void sendRequestEmail(Collection staffs, AssignmentData assign, String groupMems, LogData log) throws FinderException {
        CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(assign.getCourseID()));
	    try {
	    	
	    	//Construct base message + subject + log message + link queryString
	    	String baseMsg = groupMems + " have requested a regrade on " + assign.getName() + ".\n\n";
	    	baseMsg += "To view groups for this assignment, navigate to the following page:\n";
	    	String subject = "[" + course.getCode() + "]" + " - A Regrade was Requested in " + assign.getName();
	    	String logMsg = "Sent New Regrade Request Email to ";
	    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_GRADEASSIGN + "&" + AccessController.P_ASSIGNID + "=" + assign.getAssignmentID();
	    	
	    	sendMassEmailToGroup(course, staffs, subject, baseMsg, queryString, logMsg, "staff members", log);
	    	
	    } catch (ConnectException e) {
            appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic New Regrade Request emails to " + Util.listElements(staffs) + ".");	        
	    } catch (FinderException e) {
	    	appendDetail(log, "Failed to load recipient information.  Could not send automatic New Regrade Request emails to " + Util.listElements(staffs) + ".");
		}
	}
	/*
	 * Sends emails to students in a group that has been added to or removed from a time slot
	 */
	public void sendTSGroupChangeEmails(TimeSlotLocal ts, long groupid, AssignmentData assign, boolean groupAdd, LogData log) throws FinderException {
		CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(assign.getCourseID()));
		Collection groups = new ArrayList();
		groups.add(new Long(groupid));
		// get list of all students to whom to send emails
		Collection students = database.studentHome().findEmailTimeSlots(groups);
		if (students.size() > 0) {
		    try {
		    	
	        	//Construct base message + subject + log message + link queryString
		    	String subject = "[" + course.getCode() + "]" + " - Your group has been "+ (groupAdd ? "added to" : "removed from") +" a time slot for " + assign.getName();
		    	String logMsg = "Sent Time Slot "+ ((groupAdd) ? "addition" : "removal") +" Email to the following ";
		    	String queryString = "?" + AccessController.P_ACTION + "=" + AccessController.ACT_ASSIGN + "&" + AccessController.P_ASSIGNID + "=" + assign.getAssignmentID();
		    	
		    	String baseMsg = "";
		    	if (!groupAdd){
		    		baseMsg = "Your group has been removed from its scheduled time slot for " + assign.getName() + ".\n\n";
		        } else {
		        	// get start time, start AM/PM, and start date of timeslot in readable form
		        	Timestamp start = ts.getStartTime();
		        	String startAMPM = DateTimeUtil.AMPM.format(start);
		        	String startTime = DateTimeUtil.TIME.format(start);
		        	String startDate = DateTimeUtil.DATE.format(start);
		        	String staffid = ts.getStaff();
		        	String staffName = "";
		        	try {
		        		UserLocal staffer = database.userHome().findByUserID(staffid);
		        		staffName = staffer.getFirstName() + " " + staffer.getLastName();				
		        	} catch (Exception e) {}
		        	// construct message
		        	baseMsg = "For " + assign.getName() + " your group has been scheduled for a time slot starting at " +
		        			 startTime + " " + startAMPM + " on " + startDate;
		        	if (staffName != null && staffName != "")
		        		baseMsg += (" with staff member " + staffName);			 
		        	if (ts.getLocation() == null || ts.getLocation().length() == 0){
		        		baseMsg += ".\n\n";
		        	} else {
		        		baseMsg += (" in location " + ts.getLocation() + ".\n\n");
		        	}
		        }
		    	
		    	sendMassEmailToGroup(course, students, subject, baseMsg, queryString, logMsg, "users", log);
		    	
		    } catch (ConnectException e){
	            appendDetail(log, "Failed to create a connection for sending email.  Could not send automatic Time Slot "+ ((groupAdd) ? "addition" : "removal") +" emails to students.");	        
		    }
	    }
	}
	
	/**
	 * Set course general properties as well as staff permissions, and remove/add staff as requested.
	 * This is all in one function so that all the functions of the courseprops page can be together
	 * in one transaction.
	 * @see TransactionHandler#setCourseProps()
	 * @param courseID
	 * @param staff2remove A list of netIDs in String form
	 * @param staffPerms A Map from Strings (netIDs) to CourseAdminPermissions objects
	 * @param generalProperties
	 * @return An error string that's empty ("", NOT null) if no error
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public TransactionResult setAllCourseProperties(Principal p, long courseID, Vector staff2remove, Map staffPerms, CourseProperties generalProperties) {
		Profiler.enterMethod("TransactionBean.setAllCourseProperties", "CourseID: " + courseID);
		TransactionResult result = new TransactionResult();
		try {
			CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(courseID));
			LogData log = startLog(p);
			log.setCourseID(new Long(courseID));
			log.setLogName(LogBean.EDIT_COURSE_PROPS);
			log.setLogType(LogBean.LOG_COURSE);
			//general properties
			if (!Util.equalNull(course.getName(), generalProperties.getName())) {
			    appendDetail(log, "Course '" + course.getName() + "' renamed to '" + generalProperties.getName() + "'");
				course.setName(generalProperties.getName());
			}
			if (p.isCMSAdmin()) {
				if (!Util.equalNull(course.getCode(), generalProperties.getCode())) {
				    appendDetail(log, "Course Code changed to: " + generalProperties.getCode());
				    course.setCode(generalProperties.getCode());
				}
			}
			if (!Util.equalNull(course.getDisplayedCode(), generalProperties.getDisplayedCode())) {
			    appendDetail(log, "Displayed Course Code changed to: " + generalProperties.getDisplayedCode());
			    course.setDisplayedCode(generalProperties.getDisplayedCode());
			}
			if (course.getHasSection() != generalProperties.hasSection()) {
				appendDetail(log, "Section is now " + (generalProperties.hasSection() ? "enabled" : "disabled"));
				course.setHasSection(generalProperties.hasSection());
			}
			if (course.getShowGraderNetID() != generalProperties.isShowGraderNetID()) {
			    appendDetail(log, "Grader NetIDs are now " + (generalProperties.isShowGraderNetID() ? "shown to " : "hidden from ") + "students");
			    course.setShowGraderNetID(generalProperties.isShowGraderNetID());
			}
			if (course.getFreezeCourse() != generalProperties.isFreezeCourse()) {
			    appendDetail(log, "Course is now " + (generalProperties.isFreezeCourse() ? "" : "un") + "frozen");
			    course.setFreezeCourse(generalProperties.isFreezeCourse());
			}
			if (course.getShowFinalGrade() != generalProperties.isShowFinalGrades()) {
			    appendDetail(log, "Final grades are now " + (generalProperties.isShowFinalGrades() ? "shown to" : "hidden from") + " students");
			    if (generalProperties.isShowFinalGrades()) {
			        sendFinalGradeEmails(course, log);
			    }
			    course.setShowFinalGrade(generalProperties.isShowFinalGrades());
			} 
			if (course.getShowTotalScores() != generalProperties.isShowTotalScores()) {
				appendDetail(log, "Total scores are now " + (generalProperties.isShowTotalScores() ? "shown to" : "hidden from") + " students");
				course.setShowTotalScores(generalProperties.isShowTotalScores());
			}
			if (course.getShowAssignWeights() != generalProperties.isShowAssignWeights()) {
				appendDetail(log, "Assignment weights are now " + (generalProperties.isShowAssignWeights() ? "shown to" : "hidden from") + " students");
				course.setShowAssignWeights(generalProperties.isShowAssignWeights());
			}
			if (course.getAnnounceGuestAccess() != generalProperties.isAnnounceGuestAccess()) {
			    appendDetail(log, "Guest access to announcements is now " + (generalProperties.isAnnounceGuestAccess() ? "" : "dis") + "allowed");
			    course.setAnnounceGuestAccess(generalProperties.isAnnounceGuestAccess());
			}
			if (course.getAssignGuestAccess() != generalProperties.isAssignGuestAccess()) {
			    appendDetail(log, "Guest access to assignments is now " + (generalProperties.isAssignGuestAccess() ? "" : "dis") + "allowed");
			    course.setAssignGuestAccess(generalProperties.isAssignGuestAccess());
			}
			if (course.getSolutionGuestAccess() != generalProperties.isSolutionGuestAccess()) {
			    appendDetail(log, "Guest access to solution files is now " + (generalProperties.isSolutionGuestAccess() ? "" : "dis") + "allowed");
			    course.setSolutionGuestAccess(generalProperties.isSolutionGuestAccess());
			}
			if (course.getCourseGuestAccess() != generalProperties.isCourseGuestAccess()) {
			    appendDetail(log, "Guest access to course is now " + (generalProperties.isCourseGuestAccess()? "" : "dis") + "allowed");
			    course.setCourseGuestAccess(generalProperties.isCourseGuestAccess());
			}
			if (course.getAnnounceCCAccess() != generalProperties.isAnnounceCCAccess()) {
			    appendDetail(log, "Cornell community access to announcements is now " + (generalProperties.isAnnounceCCAccess() ? "" : "dis") + "allowed");
			    course.setAnnounceCCAccess(generalProperties.isAnnounceCCAccess());
			}
			if (course.getAssignCCAccess() != generalProperties.isAssignCCAccess()) {
			    appendDetail(log, "Cornell community access to assignments is now " + (generalProperties.isAssignCCAccess() ? "" : "dis") + "allowed");
			    course.setAssignCCAccess(generalProperties.isAssignCCAccess());
			}
			if (course.getSolutionCCAccess() != generalProperties.isSolutionCCAccess()) {
			    appendDetail(log, "Cornell community access to solution files is now " + (generalProperties.isSolutionCCAccess() ? "" : "dis") + "allowed");
			    course.setSolutionCCAccess(generalProperties.isSolutionCCAccess());
			}
			if (course.getCourseCCAccess() != generalProperties.isCourseCCAccess()) {
			    appendDetail(log, "Cornell community access to course is now " + (generalProperties.isCourseCCAccess()? "" : "dis") + "allowed");
			    course.setCourseCCAccess(generalProperties.isCourseCCAccess());
			}
			//remove staff
			for(int i = 0; i < staff2remove.size(); i++) {
				StaffLocal staff= database.staffHome().findByPrimaryKey(new StaffPK(courseID, (String)staff2remove.get(i)));
				staff.setStatus(StaffBean.INACTIVE);
				appendDetail(log, staff.getNetID() + " was removed as a staff member");
				//staff.remove();
			}
			Iterator students = database.studentHome().findByCourseIDSortByLastName(courseID).iterator();
			HashSet studentSet = new HashSet();
			while (students.hasNext()) {
			    StudentLocal student = (StudentLocal) students.next();
			    studentSet.add(student.getUserID());
			}
			//set staff permissions
			Set staffNetIDs = staffPerms.keySet();
			Vector netIDVector = new Vector();
			netIDVector.addAll(staffNetIDs);
			result = this.ensureUserExistence(netIDVector, log);
			Iterator i = netIDVector.iterator();
			while(i.hasNext()) {
				String netID = (String)i.next();
				StaffLocal staff = null;
				boolean change = false;
				if (studentSet.contains(netID)) {
				    result.addError("Could not add " + netID + ": Already enrolled as a student");
				    continue;
				}
				try {
				    staff = database.staffHome().findByPrimaryKey(new StaffPK(courseID, netID));
				} catch (FinderException e) {}
				if(staff == null) { //the user isn't staff yet; add him/her 
					staff = database.staffHome().create(netID, courseID);
					appendDetail(log, netID + " was added as a staff member", netID);
				}
				if (!staff.getStatus().equals(StaffBean.ACTIVE)) {
				    appendDetail(log, netID + " was restored as a staff member", netID);
				    staff.setStatus(StaffBean.ACTIVE);
				}
				CourseAdminPermissions perms = (CourseAdminPermissions)staffPerms.get(netID);
				if (staff.getAdminPriv() != perms.getAdmin()) {
				    staff.setAdminPriv(perms.getAdmin());
				    appendDetail(log, "Full Admin privilege was " + (perms.getAdmin() ? "granted to " : "revoked from ") + netID, netID);
				}
				if (staff.getGroupsPriv() != perms.getGroups()) {
				    staff.setGroupsPriv(perms.getGroups());
				    appendDetail(log, "Groups privilege was " + (perms.getGroups() ? "granted to " : "revoked from ") + netID, netID);
				}
				if (staff.getGradesPriv() != perms.getGrading()) {
				    staff.setGradesPriv(perms.getGrading());
				    appendDetail(log, "Grades privilege was " + (perms.getGrading() ? "granted to " : "revoked from ") + netID, netID);
				}
				if (staff.getAssignmentsPriv() != perms.getAssignments()) {
				    staff.setAssignmentsPriv(perms.getAssignments());
				    appendDetail(log, "Assignments privilege was " + (perms.getAssignments() ? "granted to " : "revoked from ") + netID, netID);
				}
				if (staff.getCategoryPriv() != perms.getCategory()) {
				    staff.setCategoryPriv(perms.getCategory());
				    appendDetail(log, "Content privilege was " + (perms.getCategory() ? "granted to " : "revoked from ") + netID);
				}
			}
			if (log.getDetailLogs() != null && log.getDetailLogs().size() > 0) {
			    database.logHome().create(log);
			}
		} catch(Exception e) {
			result.addError("An unexpected error occurred while trying to set course properties");
			ctx.setRollbackOnly();
			result.setException(e);
			e.printStackTrace();
		}
		Profiler.exitMethod("TransactionBean.setAllCourseProperties", "CourseID: " + courseID);
		return result;
	}
	
	/**
	 * Sets whether or not to show final grades
	 * @see TransactionHandler#setFinalGrade()
	 * @param courseID
	 * @param releaseFinalGrades true to release grades
	 * @return An error string that's empty ("", NOT null) if no error
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public TransactionResult setReleaseFinalGrades(Principal p, long courseID, 
			boolean releaseFinalGrades) {
		Profiler.enterMethod("TransactionBean.setReleaseFinalGrades", "CourseID: " + courseID);
		TransactionResult result = new TransactionResult();
		try {
			CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(courseID));
			LogData log = startLog(p);
			log.setCourseID(new Long(courseID));
			log.setLogName(LogBean.EDIT_COURSE_PROPS);
			log.setLogType(LogBean.LOG_COURSE);
			if (course.getShowFinalGrade() != releaseFinalGrades) {
			    appendDetail(log, "Final grades are now " + (releaseFinalGrades ? "shown to" : "hidden from") + " students");
			    if (releaseFinalGrades) {
			        sendFinalGradeEmails(course, log);
			    }
			    course.setShowFinalGrade(releaseFinalGrades);
			} 
			if (log.getDetailLogs() != null && log.getDetailLogs().size() > 0) {
			    database.logHome().create(log);
			}
		} catch(Exception e) {
			result.addError("An unexpected error occurred while trying to set course properties");
			ctx.setRollbackOnly();
			result.setException(e);
			e.printStackTrace();
		}
		Profiler.exitMethod("TransactionBean.setReleaseFinalGrades", "CourseID: " + courseID);
		return result;
	}
	
	/**
	 * Set the course textual description, which is edited from the main course page rather than
	 * from the courseprops page like every single other nice little property
	 * @param p
	 * @param courseID
	 * @param newDescription
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public TransactionResult editCourseDescription(Principal p, long courseID, String newDescription)
	{
		TransactionResult result = new TransactionResult();
		try
		{
			CourseLocal course = database.courseHome().findByPrimaryKey(new CoursePK(courseID));
			LogData log = startLog(p);
			log.setCourseID(new Long(courseID));
			log.setLogName(LogBean.EDIT_COURSE_PROPS);
			log.setLogType(LogBean.LOG_COURSE);
			String cleanDesc = StringUtil.sanitizeHTML(newDescription);
			course.setDescription(cleanDesc);
			appendDetail(log, "Course description changed by " + p.getNetID() + " to:\n" + newDescription + "");
			database.logHome().create(log);
		}
		catch(Exception e)
		{
			result.addError("Error while trying to set course description");
			ctx.setRollbackOnly();
			e.printStackTrace();
			result.setException(e);
		}
		return result;
	}
	
	private static void reorderExistingSubProblems(TreeMap orderToSubProblem) throws FinderException {
		int order = 1;
		Iterator i = orderToSubProblem.keySet().iterator();
		while (i.hasNext()) {
			Stack subproblems = (Stack) orderToSubProblem.get(i.next());
			while (!subproblems.empty()) {
				SubProblemLocal sp = (SubProblemLocal) subproblems.pop();
				if (!sp.getHidden()) {
					sp.setOrder(order);
					order++;
				}
			}
		}
	}
	
	private static void reorderChoiceLetters(TreeMap letterToChoice) {
		int index = 0;
		Iterator i = letterToChoice.keySet().iterator();
		while (i.hasNext()) {
			ChoiceLocal choice = (ChoiceLocal) letterToChoice.get(i.next());
			if (!choice.getHidden()) {
				choice.setLetter(Character.toString((char)(index + 97)));
				index++;
			}
		}
	}
	
	/**
	 * Sets the properties of an assignment
	 * @param data A data that uniquely identifies its assignment
	 * @param opts
	 * @return Whether or not transaction was successful
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult setAssignmentProps(Principal p, AssignmentData data, AssignmentOptions opts) {
		Profiler.enterMethod("TransactionBean.setAssignmentProps", "AssignmentID: " + data.getAssignmentID());
		TransactionResult result = new TransactionResult();
		try {
		    long assignID= data.getAssignmentID();
			AssignmentLocal assign= database.assignmentHome().findByAssignmentID(assignID);
			long courseID= assign.getCourseID();
			boolean computeTotalScores = false;
			LogData log = startLog(p);
			appendAssignment(log, assignID);
			log.setCourseID(new Long(courseID));
			log.setLogName(LogBean.EDIT_ASSIGNMENT);
			log.setLogType(LogBean.LOG_COURSE);
			if (!assign.getName().equals(data.getName())) {
			    assign.setName(data.getName());
			    appendDetail(log, "Name changed to: " + data.getName());
			}
			if (!assign.getNameShort().equals(data.getNameShort())) {
			    assign.setNameShort(data.getNameShort());
			    appendDetail(log, "Short name changed to: " + data.getNameShort());
			}
			boolean dueDateChange = false;
			if (!assign.getDueDate().equals(data.getDueDate())) {
			    dueDateChange = true;
			    assign.setDueDate(data.getDueDate());
			    appendDetail(log, "Due date changed to: " + DateTimeUtil.formatDate(data.getDueDate()));
			}
			if (assign.getGracePeriod() != data.getGracePeriod()) {
			    assign.setGracePeriod(data.getGracePeriod());
			    appendDetail(log, "Grade period changed to: " + data.getGracePeriod() + " minutes");
			}
			if (assign.getAllowLate() != data.getAllowLate()) {
			    assign.setAllowLate(data.getAllowLate());
			    appendDetail(log, "Late submissions are now " + (data.getAllowLate() ? " allowed" : " disallowed")); 
			}
			if (assign.getDefaultLatePenalty() != data.getDefaultLatePenalty()) {
				if(data.getDefaultLatePenalty() == null) {
					appendDetail(log, "Default late penalty erased");
				} else if(assign.getDefaultLatePenalty() == null || 
						!assign.getDefaultLatePenalty().equals(
								data.getDefaultLatePenalty())) {
					appendDetail(log, "Default late penalty changed to " + data.getDefaultLatePenalty());
				}
				assign.setDefaultLatePenalty(data.getDefaultLatePenalty());
			}
			if (!Util.equalNull(assign.getLateDeadline(), data.getLateDeadline())) {
			    assign.setLateDeadline(data.getLateDeadline());
			    if (data.getLateDeadline() == null) {
			        appendDetail(log, "Late submission deadline removed");
			    } else {
			        appendDetail(log, "Late submission deadline changed to: " + DateTimeUtil.formatDate(data.getLateDeadline()));
			    }
			}
			if (!assign.getStatus().equals(data.getStatus())) {
			    if (data.getStatus().equals(AssignmentBean.OPEN) && assign.getStatus().equals(AssignmentBean.HIDDEN)) {
			        sendNewAssignEmails(assign.getAssignmentID(), courseID, data, log);
			    } else if (!data.getStatus().equals(AssignmentBean.HIDDEN) && dueDateChange) {
			        sendDueDateEmails(courseID, data, log);
			    }
			    if (data.getStatus().equals(AssignmentBean.GRADED)) {
			        sendGradeReleaseEmails(data, log);
			    }
			    assign.setStatus(data.getStatus());
			    appendDetail(log, "Status changed to: " + data.getStatus());
			} else if (!data.getStatus().equals(AssignmentBean.HIDDEN) && dueDateChange) {
			    sendDueDateEmails(courseID, data, log);
			}
			if (!Util.equalNull(assign.getDescription(), data.getDescription())) {
			    assign.setDescription(data.getDescription());
			    appendDetail(log, "Description changed to: " + data.getDescription());
			}
			if (assign.getGroupSizeMin() != data.getGroupSizeMin() || assign.getGroupSizeMax() != data.getGroupSizeMax()) {
			    assign.setGroupSizeMin(data.getGroupSizeMin());
			    assign.setGroupSizeMax(data.getGroupSizeMax());
			    if (data.getGroupSizeMin() == 0 && data.getGroupSizeMax() == 0) {
			        // This detail generated in the setAssignedGroups field
			    } else if (data.getGroupSizeMin() == 1 && data.getGroupSizeMax() == 1) {
			        appendDetail(log, "Group options changed so all students work independently");
			    } else {
			        appendDetail(log, "Group size options changed to between " + data.getGroupSizeMin() + " and " + data.getGroupSizeMax());
			    }
			}
			if (assign.getAssignedGraders() != data.getAssignedGraders()) {
			    assign.setAssignedGraders(data.getAssignedGraders());
			    appendDetail(log, "Assigned graders is " + (data.getAssignedGraders() ? "now being enforced" : "no longer being enforced"));
			}
			if (assign.getAssignedGroups() != data.getAssignedGroups()) {
			    assign.setAssignedGroups(data.getAssignedGroups());
			    appendDetail(log, "Staff assigned groups is " + (data.getAssignedGroups() ? "now being enforced" : "no longer being enforced"));
			}
			if (assign.getStudentRegrades() != data.getStudentRegrades() || !Util.equalNull(assign.getRegradeDeadline(), data.getRegradeDeadline())) {
			    assign.setStudentRegrades(data.getStudentRegrades());
			    assign.setRegradeDeadline(data.getRegradeDeadline());
			    if (data.getStudentRegrades()) {
			        appendDetail(log, "Student regrades are now " + (data.getRegradeDeadline() == null ? "allowed in paper form only" : "allowed until " + DateTimeUtil.formatDate(data.getRegradeDeadline())));
			    } else {
			        appendDetail(log, "Student regrades are now disallowed");
			    }
			}
			if (assign.getMaxScore() != data.getMaxScore()) {
			    assign.setMaxScore(data.getMaxScore());
			    appendDetail(log, "MaxScore changed to: " + data.getMaxScore());
			    // Max score was updated, so TotalScores need to be recomputed
			    computeTotalScores = true;
			}
			if (assign.getWeight() != data.getWeight()) {
			    assign.setWeight(data.getWeight());
			    appendDetail(log, "Weight changed to: " + data.getWeight());
			    // Assignment weights were updated, so TotalScores need to be recomputed
			    computeTotalScores = true;
			}
			if (assign.getScheduled() != data.getScheduled()){
				assign.setScheduled(data.getScheduled());
				appendDetail(log, "Schedule is now " + (data.getScheduled() ? " in use" : " not in use"));
			}
			if (assign.getDuration() != data.getDuration()) {
				if (data.getScheduled()){
					assign.setDuration(data.getDuration());
					appendDetail(log, "Timeslot duration (in seconds) is now: " + (data.getDuration()));
				}
			}
			if (assign.getGroupLimit() != data.getGroupLimit()){
				if (data.getScheduled()){
					assign.setGroupLimit(data.getGroupLimit());
					appendDetail(log, "Max number of groups in a timeslot is now: " + data.getGroupLimit());
				}
			}
			if(assign.getTimeslotLockTime() != data.getTimeslotLockTime())
			{
				if(data.getScheduled())
				{
					assign.setTimeslotLockTime(data.getTimeslotLockTime());
					if(data.getTimeslotLockTime() == null)
						appendDetail(log, "Student schedule change deadline removed");
					else appendDetail(log, "Student schedule change deadline is now " + data.getTimeslotLockTime());
				}
			}
			if (assign.getShowStats() != data.getShowStats()) {
			    assign.setShowStats(data.getShowStats());
			    appendDetail(log, "Grade statistics are now " + (data.getShowStats() ? "shown to students" : "hidden from students"));
			}
			if (assign.getShowSolution() != data.getShowSolution()) {
			    assign.setShowSolution(data.getShowSolution());
			    appendDetail(log, "Solutions are now " + (data.getShowSolution() ? "shown to all students when assignment status is Closed or Graded" : 
			        "shown only to students who have been graded when assignment status is Graded"));
			}
			if (assign.getType() != data.getType()) {
			    assign.setType(data.getType());
			    appendDetail(log, "Type changed to: " + data.getType());
			}
			int numOfAssignedFiles = assign.getNumOfAssignedFiles();
			// Required files processing
			Collection c;
			Iterator i;
			long submissionID = -1;
			Iterator submissionIDs = opts.getRequiredSubmissionIDs().iterator();
			while (submissionIDs.hasNext()) {
				submissionID = ((Long) submissionIDs.next()).longValue();
				RequiredSubmissionLocal submission = database.requiredSubmissionHome().findByPrimaryKey(new RequiredSubmissionPK(submissionID));
				submission.removeRequiredFileTypes();
				String subName = opts.getRequiredFileNameByID(submissionID);
				int maxSize = opts.getRequiredMaxSizeByID(submissionID);
				if (!submission.getSubmissionName().equals(subName)) {
				    appendDetail(log, "Required submission '" + submission.getSubmissionName() + "' renamed '" + subName + "'");
				    submission.setSubmissionName(subName);
				}
				if (submission.getMaxSize() != maxSize) {
				    submission.setMaxSize(opts.getRequiredMaxSizeByID(submissionID));
				    appendDetail(log, "Required submission '" + subName + "' max size changed to:" + maxSize);
				}
				c = opts.getRequiredFileTypesByID(submissionID);
				i = c.iterator();
				if (c.size() == 0) {
				    database.requiredFileTypeHome().create(submissionID, RequiredSubmissionBean.ANY_TYPE);
				}
				while (i.hasNext()) {
					String fileType = (String) i.next();
					database.requiredFileTypeHome().create(submissionID, fileType);
				}
			}
			boolean submissionsChange = false;
			int ID = -1;
			Iterator IDs = opts.getNewRequiredSubmissionIDs().iterator();
			submissionsChange = IDs.hasNext();
			while (IDs.hasNext()) {
				ID = ((Integer) IDs.next()).intValue();
				String submissionName = opts.getNewRequiredFileNameByID(ID);
				int maxSize = opts.getNewRequiredMaxSizeByID(ID);
				numOfAssignedFiles++;
				RequiredSubmissionLocal submission = database.requiredSubmissionHome().create(assignID, submissionName, maxSize);
				c = opts.getNewRequiredFileTypesByID(ID);
				i = c.iterator();
				String types = "";
				while (i.hasNext()) {
					String fileType = (String) i.next();
					types += (types.equals("") ? fileType : ", " + fileType);
					database.requiredFileTypeHome().create(submission.getSubmissionID(), fileType);
				}
				appendDetail(log, "Added required submission '" + submissionName + "' with accepted types: " + types);
			}
			c= opts.getRestoredSubmissions();
			i= c.iterator();
			submissionsChange = submissionsChange || i.hasNext();
			while (i.hasNext()) {
			    submissionsChange = true;
				Long subID = (Long) i.next();
				RequiredSubmissionLocal sub = database.requiredSubmissionHome().findByPrimaryKey(new RequiredSubmissionPK(subID.longValue()));
				sub.setHidden(false);
				appendDetail(log, "Required submission '" + sub.getSubmissionName() + "' was restored");
				numOfAssignedFiles++;
			}
			c= opts.getRemovedSubmissions();
			i= c.iterator();
			submissionsChange = submissionsChange || i.hasNext();
			while(i.hasNext()) {
				Long subID = (Long) i.next();
				RequiredSubmissionLocal sub = database.requiredSubmissionHome().findByPrimaryKey(new RequiredSubmissionPK(subID.longValue()));
				sub.setHidden(true);
				appendDetail(log, "Required submission '" + sub.getSubmissionName() + "' was removed");
				numOfAssignedFiles--;
			}
			assign.setNumOfAssignedFiles(numOfAssignedFiles);
			if (submissionsChange) {
			    Iterator gs = database.groupHome().findByAssignmentID(assignID).iterator();
			    Map remSubs = database.getRemainingSubmissionMap(assignID);
			    while (gs.hasNext()) {
			        GroupLocal group = (GroupLocal) gs.next();
			        Integer remSub = (Integer) remSubs.get(new Long(group.getGroupID()));
			        if (remSub == null) {
			            group.setRemainingSubmissions(numOfAssignedFiles);
			        } else {
			            group.setRemainingSubmissions(numOfAssignedFiles - remSub.intValue());
			        }
			    }
			}
			// Assignment File processing
			long itemID = -1;
			/* For each AssignmentItem, update the name, and check to see if any of the 
			 * associated files have been replaced or restored */
			Iterator itemIDs = opts.getAssignmentItemIDs().iterator();
			while (itemIDs.hasNext()) {
				itemID = ((Long) itemIDs.next()).longValue();
				String itemName = opts.getItemNameByID(itemID);
				AssignmentFileData replacement = opts.getReplacementFinalFileByID(itemID);
				long restoredFileID = opts.getRestoredFileIDByItemID(itemID);
				AssignmentItemLocal item = database.assignmentItemHome().findByPrimaryKey(new AssignmentItemPK(itemID));
				if (!item.getItemName().equals(itemName)) {
				    appendDetail(log, "Assignment file '" + item.getItemName() + "' was renamed '" + itemName + "'");
				    item.setItemName(itemName);
				}
				if (restoredFileID > 0) {
					AssignmentFileLocal oldFile = item.getAssignmentFile();
					oldFile.setHidden(true);
					AssignmentFileLocal newFile = database.assignmentFileHome().findByPrimaryKey(new AssignmentFilePK(restoredFileID));
					newFile.setHidden(false);
					appendDetail(log, "Restored file '" + newFile.getFileName() + "' as '" + itemName + "'");
				}
				if (replacement != null) {
					AssignmentFileLocal oldFile = item.getAssignmentFile();
					oldFile.setHidden(true);
					AssignmentFileLocal newFile = database.assignmentFileHome().create(itemID, replacement.getFileName(),
							false, replacement.getPath());
					appendDetail(log, "Replaced assignment file '" + itemName + "' with file '" + newFile.getFileName() + "'");
				}
			}
			// Restore AssignmentItems which were previously hidden
			Collection restored = opts.getRestoredAssignmentItems();
			i = restored.iterator();
			while (i.hasNext()) {
			 	long restoredid = ((Long) i.next()).longValue(); 
				AssignmentItemLocal restore = database.assignmentItemHome().findByPrimaryKey(new AssignmentItemPK(restoredid));
				restore.setHidden(false);
				appendDetail(log, "Restored assignment file '" + restore.getItemName() + "'");
			}
			// Hide AssignmentItems which have been removed
			Collection removed = opts.getRemovedAssignmentItems();
			i = removed.iterator();
			while (i.hasNext()) {
				itemID = ((Long) i.next()).longValue();
				AssignmentItemLocal removedItem = database.assignmentItemHome().findByPrimaryKey(new AssignmentItemPK(itemID));
				removedItem.setHidden(true);
				appendDetail(log, "Removed assignment file '" + removedItem.getItemName() + "'");
			}
			// Create new AssignmentItems and associated AssignmentFiles
			ID = -1;
			IDs = opts.getNewAssignmentItemIDs().iterator();
			while (IDs.hasNext()) {
			//while ((ID = opts.getNextNewAssignmentItemID()) >= 0) {
				ID = ((Integer) IDs.next()).intValue();
				String itemName = opts.getNewItemNameByID(ID);
				AssignmentItemLocal newItem = database.assignmentItemHome().create(assign.getAssignmentID(), itemName);
				AssignmentFileData newFileData = opts.getNewFinalFileByID(ID);
				AssignmentFileLocal newFile = database.assignmentFileHome().create(newItem.getAssignmentItemID(), newFileData.getFileName(),
						false, newFileData.getPath());
				appendDetail(log, "Added assignment file '" + newFileData.getFileName() + "' as '" + itemName + "'");

			}
			if (opts.isSolutionRemoved()) {
				SolutionFileLocal currentSolution = assign.getSolutionFile();
				currentSolution.setHidden(true);
				appendDetail(log, "Solution file '" + currentSolution.getFileName() + "' removed");
			}
			// Solution File processing
			if (opts.hasSolutionFile()) {
			    boolean replaced = false;
				if (assign.hasSolutionFile()) {
					SolutionFileLocal oldSolution = assign.getSolutionFile();
					oldSolution.setHidden(true);
					replaced = true;
				}
				SolutionFileData newSolutionData = opts.getFinalSolutionFile();
				SolutionFileLocal newSolution = database.solutionFileHome().create(assign.getAssignmentID(),
						newSolutionData.getFileName(), false, newSolutionData.getPath());
				appendDetail(log, (replaced ? "Current solution file replaced with " : "Added solution file ") +  "'" + newSolution.getFileName() + "'");
			} else if (opts.hasRestoredSolution()) {
				long solID = opts.getRestoredSolutionID();
				if (assign.hasSolutionFile()) {
					SolutionFileLocal currentSolution = assign.getSolutionFile();
					currentSolution.setHidden(true);
				}
				SolutionFileLocal solution = database.solutionFileHome().findByPrimaryKey(new SolutionFilePK(solID));
				solution.setHidden(false);
				appendDetail(log, "Solution file '" + solution.getFileName() + "' set as the current solution");
			}
			// SubProblem processing
			String subProbName;
			float maxScore;
			int type;
			int order;
			boolean problemChange = false;
			
			SubProblemOptions choices;
			Iterator choiceIDs;
			int newChoiceID;
			long choiceID;
			int answer;
			String choiceLetter;
			String choiceText;
			// Create new SubProblems
			Collection newSubProbIDs = opts.getNewSubProblemIDs(),
				oldSubProbIDs = opts.getSubProblemIDs();
			TreeMap orderToSubProblem = new TreeMap();
			
			/* Remove TA assignments to grade SubProblemID = 0 if 
			 * there had been no subproblems and now there are subproblems */
			if (oldSubProbIDs.size() == 0 && newSubProbIDs.size() > 0) {
			    i = database.groupAssignedToHome().findByAssignIDSubProbID(assignID, 0).iterator();
			    while (i.hasNext()) {
			        GroupAssignedToLocal a = (GroupAssignedToLocal) i.next();
			        a.setNetID(null);
			    }
			    computeAssignmentStats(p, assignID, log);
			}
			
			//Hide SubProblems which have been deleted
			Collection removedSubProblems = opts.getRemovedSubProblems();
			i = removedSubProblems.iterator();
			boolean hasRemovedSubProblems = (removedSubProblems.size() > 0);
			long subID;
			while (i.hasNext()) {
			    problemChange = true;
				subID = ((Long) i.next()).longValue();
				SubProblemLocal subProblem = database.subProblemHome().findByPrimaryKey(new SubProblemPK(subID));
				subProblem.setHidden(true);
				appendDetail(log, "Problem '" + subProblem.getSubProblemName() + "' was removed");
			}
			
			problemChange = oldSubProbIDs.size() > 0;
			i = newSubProbIDs.iterator();
			while (i.hasNext()) {
				ID = ((Integer) i.next()).intValue();
				subProbName = opts.getNewSubProblemNameByID(ID);
				maxScore = opts.getNewSubProblemScoreByID(ID);
				type = opts.getNewSubProblemTypeByID(ID);
				order = opts.getNewSubProblemOrderByID(ID);
				choices = opts.getNewSubProblemChoicesByID(ID);
				answer = opts.getNewSubProblemAnswerByID(ID);
				SubProblemLocal subProblem = database.subProblemHome().create(assign.getAssignmentID(), subProbName, maxScore, type, order, answer);
				
				Stack subProblems = new Stack();
				subProblems.push(subProblem);
				orderToSubProblem.put(new Integer(order), subProblems);
				
				appendDetail(log, "Added problem '" + subProbName + "' worth " + maxScore + " points");
				
				//Only new choices for new problems
				if(choices != null)
				{
					choiceIDs = choices.getNewChoiceIDs().iterator();
					while(choiceIDs.hasNext())
					{
						newChoiceID = ((Integer) choiceIDs.next()).intValue();
						choiceLetter = choices.getNewChoiceLetterByID(newChoiceID);
						choiceText = choices.getNewChoiceTextByID(newChoiceID);
						database.choiceHome().create(newChoiceID, subProblem.getSubProblemID(), choiceLetter, choiceText, false);
						appendDetail(log, "Added choice '" + choiceText + " (" + choiceLetter + ")' to problem '" + subProbName + "'");
					}
				}
			}
			// Update previously existing SubProblems
			i = oldSubProbIDs.iterator();
			while (i.hasNext()) {
				subID = ((Long) i.next()).longValue();
				subProbName = opts.getSubProblemNameByID(subID);
				maxScore = opts.getSubProblemScoreByID(subID);
				type = opts.getSubProblemTypeByID(subID);
				order = opts.getSubProblemOrderByID(subID);
				choices = opts.getSubProblemChoicesByID(subID);
				answer = opts.getSubProblemAnswerByID(subID);
				SubProblemLocal subProblem = database.subProblemHome().findByPrimaryKey(new SubProblemPK(subID));
				
				Stack subProblems = new Stack();
				subProblems.push(subProblem);
				orderToSubProblem.put(new Integer(order), subProblems);
				
				if (!subProblem.getSubProblemName().equals(subProbName)) {
				    appendDetail(log, "Problem '" + subProblem.getSubProblemName() + "' was renamed '" + subProbName + "'");
				    subProblem.setSubProblemName(subProbName);
				}
				if (subProblem.getMaxScore() != maxScore) {
				    subProblem.setMaxScore(maxScore);
				    appendDetail(log, "Problem '" + subProbName + "' now worth " + maxScore + " points");
				}
				if (subProblem.getOrder() != order) {
				    subProblem.setOrder(order);
				    appendDetail(log, "Problem '" + subProbName + "' is now in position " + order);
				}
				
				if (subProblem.getAnswer() != answer) {
				    subProblem.setAnswer(answer);
				    appendDetail(log, "Problem '" + subProbName + "' now has answer " + answer);
				}
				
				if (subProblem.getType() != type) {
					String subProblemType = "a fill-in.";
					if (type == SubProblemBean.MULTIPLE_CHOICE) subProblemType = "a multiple choice.";
					else if (type == SubProblemBean.SHORT_ANSWER) subProblemType = "a short answer.";
					
					// if converting from a multiple choice, we need to map all the choice ids
					// in the answer text of submitted answers to actual choice texts
					if (subProblem.getType() == SubProblemBean.MULTIPLE_CHOICE) {
						Iterator k = database.answerSetHome().findByAssignmentID(assignID).iterator();
						while (k.hasNext()) {
							Iterator l = ((AnswerSetLocal) k.next()).getAnswers().iterator();
							while(l.hasNext()) {
								AnswerData adata = (AnswerData) l.next();
								try {
									// submitted answer text from a multiple choice
									// would be a choice id
									ChoiceLocal ch = database.choiceHome()
														.findByChoiceID(Long.parseLong(adata.getText()));
									AnswerLocal a = database.answerHome().findByAnswerID(adata.getAnswerID());
									a.setText(ch.getText());	
								} catch (Exception e)  {}
							}
						}
					}
					
					
					subProblem.setType(type);
					
					
					/*
					// get choices of this subproblem 
					Collection chs = database.choiceHome()
											.findBySubProblemID(subProblem.getSubProblemID(), false);
		
					
					// generate a new suproblem clone
					SubProblemLocal subProblemClone = 
						database.subProblemHome().create
							(assign.getAssignmentID(), subProbName, maxScore, type, order, answer);
					
					// update the subID to reference the new copy of subproblem
					subID = subProblemClone.getSubProblemID();
					
					
					//hide the old subproblem
					subProblem.setHidden(true);
					
					// update orderToSubProblem to reference this new subproblem instead of the original one
					orderToSubProblem.put(new Integer(order), subProblemClone);
					
					// make all choices reference the newly created subproblem clone
					long subProblemCloneID = subProblemClone.getSubProblemID();
					Iterator choiceIterator = chs.iterator();
					while ( choiceIterator.hasNext() ) {
						ChoiceLocal choice = (ChoiceLocal) choiceIterator.next();
						choice.setSubProblemID(subProblemCloneID);
						choice.setHidden(false);
					} */
						
					appendDetail(log, "Problem '" + subProbName + "' now " + subProblemType);
				}
				
				//Handle Choices
				if(choices != null)
				{
					choiceIDs = choices.getNewChoiceIDs().iterator();
					while(choiceIDs.hasNext())
					{
						newChoiceID = ((Integer) choiceIDs.next()).intValue();
						choiceLetter = choices.getNewChoiceLetterByID(newChoiceID);
						choiceText = choices.getNewChoiceTextByID(newChoiceID);
						database.choiceHome().create(newChoiceID, subID, choiceLetter, choiceText, false);
						appendDetail(log, "Added choice '" + choiceText + "' to problem '" + subProbName + "'");
					}
					
					TreeMap letterToChoice = new TreeMap();
					choiceIDs = choices.getChoiceIDs().iterator();
					boolean hasRemoved = choices.getRemovedChoices().size() > 0;
					while(choiceIDs.hasNext())
					{
						choiceID = ((Long) choiceIDs.next()).longValue();
						ChoiceLocal choice = database.choiceHome().findByPrimaryKey(new ChoicePK(choiceID));
						choiceLetter = choices.getChoiceLetterByID(choiceID);
						choiceText = choices.getChoiceTextByID(choiceID);
						letterToChoice.put(choiceLetter, choice);
						
						if(!choice.getLetter().equals(choiceLetter))
						{
							appendDetail(log, "Choice '" + choiceText + "(" + choice.getLetter() + ")' is now letter '" + choiceLetter + "'");
							choice.setLetter(choiceLetter);
						}
						
						if(!choice.getText().equals(choiceText))
						{
							appendDetail(log, "Choice '" + choice.getText() + "' is now '" + choiceText + "'");
							choice.setText(choiceText);
						}
						
						//Hide Choices which have been deleted
						Iterator choiceIterator = choices.getRemovedChoices().iterator();
						while (choiceIterator.hasNext()) {
							choiceID = ((Long) choiceIterator.next()).longValue();
							choice = database.choiceHome().findByPrimaryKey(new ChoicePK(choiceID));
							choice.setHidden(true);
							appendDetail(log, "Choice '" + choice.getText() + "' was removed");
						}
						
						// Unhide SubProblems which have been restored
						/*choiceIterator = opts.getRestoredSubProblems().iterator();
						while (choiceIterator.hasNext()) {
							choiceID = ((Long) choiceIterator.next()).longValue();
							choice = database.choiceHome().findByPrimaryKey(new ChoicePK(choiceID));
							choice.setHidden(false);
							appendDetail(log, "Choice '" + choice.getText() + "' was removed");
						}*/
					}
					
					// if there's choice remove, reorder the existing choice letters
					if (hasRemoved) reorderChoiceLetters(letterToChoice);
				}
			}
			// Unhide SubProblems which have been restored
			Collection restoredSubProblems = opts.getRestoredSubProblems();
			boolean hasRestoredSubProblems = (restoredSubProblems.size() > 0);
			i = restoredSubProblems.iterator();
			while (i.hasNext()) {
			    problemChange = true;
				subID = ((Long) i.next()).longValue();
				SubProblemLocal subProblem = database.subProblemHome().findByPrimaryKey(new SubProblemPK(subID));
				subProblem.setHidden(false);
				
				Stack subProblems;
				Integer key = new Integer(subProblem.getOrder());
				if (orderToSubProblem.containsKey(key))
					subProblems = (Stack)orderToSubProblem.get(key);
				else
					subProblems = new Stack();
				
				subProblems.push(subProblem);
				orderToSubProblem.put(key, subProblems);
				
				float assignScore = assign.getMaxScore();
				assign.setMaxScore(assignScore + subProblem.getMaxScore());
				
				appendDetail(log, "Problem '" + subProblem.getSubProblemName() + "' was restored");
			}
			if (opts.getMigrationAssignmentID() != 0) {
				long fromAssignID = opts.getMigrationAssignmentID();
				appendDetail(log, "Importing groups from assignment " + fromAssignID);
				duplicateGroups(p, fromAssignID, assignID, log);
				problemChange = true;
			}
			if (problemChange) {
			    computeAssignmentStats(p, assignID, log);
			    computeTotalScores(p, assign.getCourseID(), log);
			} else if (computeTotalScores) {
				computeTotalScores(p, assign.getCourseID(), log);
			}
			
			if (hasRemovedSubProblems || hasRestoredSubProblems)
				reorderExistingSubProblems(orderToSubProblem);
			
			database.logHome().create(log);
		} catch (Exception e) {
			e.printStackTrace();
			ctx.setRollbackOnly();
			result.addError("An unexpected error occurred, could not update assignment");
			result.setException(e);
		}
		Profiler.exitMethod("TransactionBean.setAssignmentProps", "AssignmentID: " + data.getAssignmentID());
		return result;
	}
	
	/**
	 * Set the active semester for future operations
	 * @param semesterID
	 * @return Whether the operation was completed successfully
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean setCurrentSemester(Principal p, long semesterID) {
		boolean success = true;
		try {
		    SemesterLocal curr = database.semesterHome().findCurrent();
		    if (curr.getSemesterID() != semesterID) {
		        success = database.setCurrentSemester(semesterID);
		        SemesterLocal repl = database.semesterHome().findByPrimaryKey(new SemesterPK(semesterID)); 
		        LogData log = startLog(p);
		        log.setLogName(LogBean.SET_CURRENT_SEMESTER);
				log.setLogType(LogBean.LOG_ADMIN);
				appendDetail(log, "Current semester changed from " + curr.getSemesterName() + " to " + repl.getSemesterName());
			    database.logHome().create(log);
		    }
		} catch(Exception e) {
			success = false;
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Save the URL for the XML page specifying server groupings
	 * @param url
	 * @return resultof the transaction
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult setHostGroupsURL(Principal p, String url) {
		TransactionResult result = new TransactionResult();
		boolean success = false;
		try {
			String oldURL = database.getHostGroupsURL();
			success = database.setHostGroupsURL(url);
	        LogData log = startLog(p);
	        log.setLogName(LogBean.SET_HOST_GROUPS_URL);
			log.setLogType(LogBean.LOG_ADMIN);
			appendDetail(log, "Server grouping XML URL changed from " + oldURL + " to " + url);
		    database.logHome().create(log);
		} catch(Exception e) {
			success = false;
			e.printStackTrace();
			result.addError("Unknown error occurred, could not change server grouping URL.", e);
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return result;
	}
	
	/**
	 * Gives a group an extension until the given date
	 * @param p
	 * @param groupID
	 * @param extension
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult setExtension(Principal p, long groupID, Timestamp extension) {
	    TransactionResult result = new TransactionResult();
	    try {
	        GroupLocal group = database.groupHome().findByGroupID(groupID);
	        AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(group.getAssignmentID());
	        long assignID = assignment.getAssignmentID();
	        Iterator members = database.groupMemberHome().findActiveByGroupID(groupID).iterator();
	        LogData log = startLog(p);
	        log.setCourseID(new Long(assignment.getCourseID()));
	        log.setLogName(LogBean.GRANT_EXTENSION);
	        log.setLogType(LogBean.LOG_GROUP);
	        String mems = "";
	        while (members.hasNext()) {
	            GroupMemberLocal mem = (GroupMemberLocal) members.next();
	            LogDetail d = new LogDetail(0);
	            d.setAssignmentID(new Long(assignID));
	            d.setNetID(mem.getNetID());
	            appendDetail(log, d);
	            mems += mems.equals("") ? mem.getNetID() : ", " + mem.getNetID();
	        }
	        appendDetail(log, "Granted extension to " + mems + " on '" + assignment.getName() + "' until " 
	        					+ DateTimeUtil.formatDate(extension));
	        group.setExtension(extension);
	        database.logHome().create(log);
	    } catch (Exception e) {
	        e.printStackTrace();
	        ctx.setRollbackOnly();
			result.setException(e);
	        result.addError("An unexpected error occurred, could not set extension");
	    }
	    return result;
	}
	
	/**
	 * Sets the final grades for a course
	 * @param p
	 * @param courseID
	 * @param grades A collection containing 2-element String arrays where the
	 * 	first entry is the NetID and the second entry is the Grade
	 * 	(Grade = "null" if the action is meant to set the student's final grade to null)
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean setFinalGrades(Principal p, long courseID, Collection grades) {
	    try {
	        LogData log = startLog(p);
	        log.setCourseID(new Long(courseID));
	        log.setLogName(LogBean.EDIT_FINAL_GRADES);
	        log.setLogType(LogBean.LOG_GRADE);
		    Iterator i = grades.iterator();
		    boolean changedGrade = false;
		    while (i.hasNext()) {
		        String[] grade = (String[]) i.next();
		        String netID = grade[0], finalGrade = (grade[1].equals("null") ? null : grade[1]);
		        StudentLocal student = database.studentHome().findByPrimaryKey(new StudentPK(courseID, netID));
			    if (!Util.equalNull(student.getFinalGrade(), finalGrade)) {
			        changedGrade = true;
			        student.setFinalGrade(finalGrade);
			        appendDetail(log, "Final grade for " + netID + (finalGrade == null ? " removed" : " changed to " + finalGrade));
			    }
		    }
		    if (changedGrade) {
		        database.logHome().create(log);
		    }
	    } catch (Exception e) {
	        e.printStackTrace();
	        ctx.setRollbackOnly();
	        return false;
	    }
		return true;
	}
	
    /**
     * setSessionContext.
     * @param ctx
     * @throws javax.ejb.EJBException
     * @throws java.rmi.RemoteException
     */
    public void setSessionContext(javax.ejb.SessionContext ctx)
            throws javax.ejb.EJBException {
            this.ctx = ctx;
    }
	
	/**
	 * Set staff preferences
	 * @param p
	 * @param courseID
	 * @param data
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult setStaffPrefs(Principal p, long courseID, StaffData data) {
	    TransactionResult result = new TransactionResult();
	    try {
	        StaffLocal staff = database.staffHome().findByPrimaryKey(new StaffPK(courseID, p.getUserID()));
	        LogData log = startLog(p);
	        log.setCourseID(new Long(courseID));
	        log.setLogName(LogBean.EDIT_STAFF_PREFS);
	        log.setLogType(LogBean.LOG_COURSE);
	        if (data.getEmailDueDate() != staff.getEmailDueDate()) {
	            appendDetail(log, "Email for Due Date Changes turned " + (data.getEmailDueDate() ? "ON" : "OFF"));
	            staff.setEmailDueDate(data.getEmailDueDate());
	        }
	        if (data.getEmailNewAssign() != staff.getEmailNewAssign()) {
	            appendDetail(log, "Email for New Assignment Release turned " + (data.getEmailNewAssign() ? "ON" : "OFF"));
	            staff.setEmailNewAssign(data.getEmailNewAssign());	            
	        }
	        if (data.getEmailFinalGrade() != staff.getEmailFinalGrade()) {
	            appendDetail(log, "Email for Final Grades Release turned " + (data.getEmailFinalGrade() ? "ON" : "OFF"));
	            staff.setEmailFinalGrade(data.getEmailFinalGrade());	            
	        }
	        if (data.getEmailAssignedTo() != staff.getEmailAssignedTo()) {
	            appendDetail(log, "Email for When Assigned To turned " + (data.getEmailAssignedTo() ? "ON" : "OFF"));
	            staff.setEmailAssignedTo(data.getEmailAssignedTo());	            
	        }
	        if (data.getEmailAssignSubmit() != staff.getEmailAssignSubmit()) {
	            appendDetail(log, "Email for When Submission Received turned " + (data.getEmailAssignSubmit() ? "ON" : "OFF"));
	            staff.setEmailAssignSubmit(data.getEmailAssignSubmit());	            
	        }
	        if (data.getEmailRequest() != staff.getEmailRequest()) {
	            appendDetail(log, "Email for New Regrade Requests turned " + (data.getEmailRequest() ? "ON" : "OFF")); 
	            staff.setEmailRequest(data.getEmailRequest());	            
	        }
	        database.logHome().create(log);
	        result.setValue("Course preferences set successfully");
	    } catch (Exception e) {
	        ctx.setRollbackOnly();
	        e.printStackTrace();
			result.setException(e);
	        result.addError("An error occurred while setting student preferences");
	    }
	    return result;
	}

	/**
	 * Set student preferences
	 * @param p
	 * @param courseID
	 * @param data
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult setStudentPrefs(Principal p, long courseID, StudentData data) {
	    TransactionResult result = new TransactionResult();
	    try {
	        StudentLocal student = database.studentHome().findByPrimaryKey(new StudentPK(courseID, p.getUserID()));
	        changeStudentEntry(p, courseID, student, data);
	        result.setValue("Course preferences set successfully");
	    } catch (Exception e) {
	        ctx.setRollbackOnly();
	        e.printStackTrace();
			result.setException(e);
	        result.addError("An error occurred while setting student preferences");
	    }
	    return result;
	}
	
	/**
	 * Set student preferences for all courses a student is in
	 * @param p
	 * @param data
	 * @return result
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public TransactionResult setAllStudentPrefs(Principal p, StudentData data) {
	    TransactionResult result = new TransactionResult();
	    try {
	        Collection courses = database.courseHome().findStudentCourses(p.getPrincipalID());
	        Iterator iter = courses.iterator();
	        while (iter.hasNext())
	        {
	            long courseID = ((CourseLocal) iter.next()).getCourseID();
		        StudentLocal student = database.studentHome().findByPrimaryKey(new StudentPK(courseID, p.getUserID()));
		        changeStudentEntry(p, courseID, student, data);
	        }
	        result.setValue("Course preferences set successfully");
	    } catch (Exception e) {
	        ctx.setRollbackOnly();
	        e.printStackTrace();
			result.setException(e);
	        result.addError("An error occurred while setting student preferences");
	    }
	    return result;    
	}
	
	private void changeStudentEntry(Principal p, long courseID, StudentLocal student, StudentData data) throws CreateException
	{
        LogData log = startLog(p);
        log.setCourseID(new Long(courseID));
        log.setLogName(LogBean.EDIT_STUDENT_PREFS);
        log.setLogType(LogBean.LOG_COURSE);
        if (data.getEmailDueDate() != student.getEmailDueDate()) {
            appendDetail(log, "Email for Due Date Changes turned " + (data.getEmailDueDate() ? "ON" : "OFF"));
            student.setEmailDueDate(data.getEmailDueDate());
        }
        if (data.getEmailFile() != student.getEmailFile()) {
            appendDetail(log, "Email for Group File Submission turned " + (data.getEmailFile() ? "ON" : "OFF"));
            student.setEmailFile(data.getEmailFile());	            
        }
        if (data.getEmailTimeSlot() != student.getEmailTimeSlot()) {
            appendDetail(log, "Email for Time Slot changes turned " + (data.getEmailTimeSlot() ? "ON" : "OFF"));
            student.setEmailTimeSlot(data.getEmailTimeSlot());	            
        }  
        if (data.getEmailFinalGrade() != student.getEmailFinalGrade()) {
            appendDetail(log, "Email for Final Grades Release turned " + (data.getEmailFinalGrade() ? "ON" : "OFF"));
            student.setEmailFinalGrade(data.getEmailFinalGrade());	            
        }
        if (data.getEmailGroup() != student.getEmailGroup()) {
            appendDetail(log, "Email for Group Activity turned " + (data.getEmailGroup() ? "ON" : "OFF"));
            student.setEmailGroup(data.getEmailGroup());	            
        }
        if (data.getEmailNewAssignment() != student.getEmailNewAssignment()) {
            appendDetail(log, "Email for New Assignment Release turned " + (data.getEmailNewAssignment() ? "ON" : "OFF"));
            student.setEmailNewAssignment(data.getEmailNewAssignment());	            
        }
        if (data.getEmailNewGrade() != student.getEmailNewGrade()) {
            appendDetail(log, "Email for New Grades Release turned " + (data.getEmailNewGrade() ? "ON" : "OFF"));
            student.setEmailNewGrade(data.getEmailNewGrade());	            
        }
        if (data.getEmailRegrade() != student.getEmailRegrade()) {
            appendDetail(log, "Email for Grade Changes turned " + (data.getEmailRegrade() ? "ON" : "OFF"));
            student.setEmailRegrade(data.getEmailRegrade());	            
        }
        if (data.getEmailTimeSlot() != student.getEmailTimeSlot()) {
        	appendDetail(log, "Email for Time Slot Changes turned " + (data.getEmailTimeSlot() ? "ON" : "OFF"));
        	student.setEmailTimeSlot(data.getEmailTimeSlot());
        }
        if (log.getDetailLogs() == null || log.getDetailLogs().size() == 0) {
            database.logHome().create(log);
        }
	}
	
	private	LogData startLog(Principal p) {
	    LogData log = new LogData();
	    log.setActingIPAddress(p.getIPAddress());
	    log.setActingNetID(p.getPrincipalID());
	    if (p.isInStaffAsBlankMode()) {
	        log.setSimulatedNetID(p.getUserID());
	    }
	    return log;
	}
	
	/**
	 * Perform a submission transaction for a user to submit a required file for
	 * an assignment.
	 * Database changes made in this transaction:
	 * 1) New file created in tFiles table
	 * 2) New submitted file created in the tSubmittedFiles table
	 * 3) File submission recorded in the tGroupActivity table
	 * 4) File submission recorded in the tLogs table
	 * @param netID The NetID of the user submitting the file
	 * @param assignmentID The AssignmentID of the assignment currently involved
	 * @param submittedFile The File which the user uploaded for submission
	 * @param requiredFile The AssignmentFileData with information about the
	 * 		file the submitted file is supposed to match.
	 * @return Returns true iff the transaction completed successfully
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public boolean submitFile(String netID, long assignmentID, SubmissionInfo subInfo) {
		boolean success = false;
		try {
			RequiredSubmissionData subData = subInfo.getSubmission();
			GroupMemberLocal member = database.groupMemberHome().findActiveByNetIDAssignmentID(netID, assignmentID);
			AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(assignmentID);
			CourseLocal course = database.courseHome().findByAssignmentID(assignmentID);
			String path = new java.io.File(FileUtil.getSubmittedFileSystemPath(course.getCourseID(), assignmentID, 
														member.getGroupID(), subInfo.getFileCounter(), 
														subData.getSubmissionID(), subInfo.getFileType())).getParent();
			if(!path.endsWith("" + FileUtil.SYS_SLASH)) path += FileUtil.SYS_SLASH;
			SubmittedFileLocal submission = database.submittedFileHome().create(member.getGroupID(), member.getGroupID(), 
			        netID, subData.getSubmissionID(), subInfo.getFileType(), subInfo.getFileLength(), 
					subInfo.getMD5(), subInfo.getIsLate(), path, null);
			success = true;
		}
		catch (Exception e) {
			success = false;
			ctx.setRollbackOnly();
			e.printStackTrace();
		}
		if (!success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Attempt to accept submitted files as sent through the given
	 * HttpServletRequest from the user.
	 * @param netid
	 * @param request
	 * @return
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public String submitFiles(Principal p, long assignmentid, Collection files) {
		Profiler.enterMethod("TransactionBean.submitFiles", "AssignmentID: " + assignmentid);
		String error = null;
		boolean fail = false;
		try {
		    String netid = p.getUserID();
			AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(assignmentid);
			GroupLocal group = database.groupHome().findByNetIDAssignmentID(netid, assignmentid);
			long groupID = group.getGroupID();
			Iterator groupMembers = database.groupMemberHome().findActiveByGroupID(groupID).iterator();
			Iterator i = files.iterator();
			LogData log = new LogData();
			log.setActingIPAddress(p.getIPAddress());
			log.setActingNetID(p.getPrincipalID());
			appendAssignment(log, assignmentid);
			log.setCourseID(new Long(assignment.getCourseID()));
			log.setLogName(LogBean.SUBMIT_FILES);
			log.setLogType(LogBean.LOG_GROUP);
			while (groupMembers.hasNext()) {
			    GroupMemberLocal m = (GroupMemberLocal) groupMembers.next();
			    appendReceiver(log, m.getNetID());
			}
			while (i.hasNext() && !fail) {
				SubmissionInfo subInfo = (SubmissionInfo) i.next();
				String subName = subInfo.getSubmission().getSubmissionName();
				appendDetail(log, p.getUserID() + " submitted " + subInfo.getFileName() + " as " + subName);
				fail = !(submitFile(netid, assignmentid, subInfo));
				group.setLatestSubmission(new Timestamp(System.currentTimeMillis()));
				if (fail) {
					error = "Database error caused submission transaction to fail";
				}
			}
			Collection uniquefiles = database.submittedFileHome().findByGroupID(groupID);
			group.setRemainingSubmissions(assignment.getNumOfAssignedFiles() - uniquefiles.size());
			
			/* TODO Add e-mail notification for group members and staff */
			
			if (!fail) {
				Collection theGroup = database.groupMemberHome().findActiveByGroupID(groupID);
				if (theGroup.size() > 1) {
					groupMembers = theGroup.iterator();
					while (groupMembers.hasNext()) {
					    GroupMemberLocal m = (GroupMemberLocal) groupMembers.next();
					    StudentLocal s = database.studentHome().findByGroupIDNetID(groupID, m.getNetID());
					    if ( s.getEmailFile() && !(m.getNetID().equals(p.getPrincipalID())) ) {
					    	sendGroupSubmitEmail(m.getNetID(), assignment.getAssignmentData(), log);
					    }
					}
				}
				if (group.getRemainingSubmissions() == 0) {
					Collection groupAssignedTo = database.groupAssignedToHome().findByGroupID(groupID);
					Iterator assignedTo = groupAssignedTo.iterator();
					while (assignedTo.hasNext()) {
						GroupAssignedToLocal atl = (GroupAssignedToLocal) assignedTo.next();
						StaffLocal s = database.staffHome().findByAssignmentIDNetID(assignment.getAssignmentID(), atl.getNetID());
						if ( s.getEmailAssignSubmit() ) {
							sendAssignSubmitEmail(atl.getNetID(), assignment.getAssignmentData(), groupID, log);
						}
					}
				}
				long graceperiod = assignment.getGracePeriod() * 60000;
				Timestamp now = new Timestamp(System.currentTimeMillis() - graceperiod);
				boolean extensionLate = group.getExtension() != null && group.getExtension().before(now);
				boolean isLate = extensionLate || (group.getExtension() == null) && assignment.getDueDate().before(now);
				if(isLate && assignment.getDefaultLatePenalty() != null) {
					appendDetail(log, "Setting adjustment to default late penalty: " + assignment.getDefaultLatePenalty());
					group.setAdjustment(assignment.getDefaultLatePenalty());
				}
			}
			
			database.logHome().create(log);
		}
		catch (Exception e) {
			e.printStackTrace();
			ctx.setRollbackOnly();
			error = "Database failed to accept submitted files";
		}
		if (fail)
			ctx.setRollbackOnly();
		Profiler.exitMethod("TransactionBean.submitFiles", "AssignmentID: " + assignmentid);
		return error;
	}
	
	public void unsetSessionContext() {
		this.ctx = null;
	}
	
	/**
	 * Creates a new assignment with these properties
	 * @param newAssignment A data describing the new assignment
	 * @param options
	 * @return Whether or not the transaction was successful
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public String submitSurvey(Principal p, long assignmentID, Collection answers) {
		TransactionResult result = new TransactionResult();
		Profiler.enterMethod("TransactionBean.submitSurvey", "CourseID: " + assignmentID);
		String error = null;
		boolean fail = false;
		try {
			String netID = p.getUserID();
			AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(assignmentID);
			GroupLocal group = database.groupHome().findByNetIDAssignmentID(netID, assignmentID);
			long groupID = group.getGroupID();
			Iterator groupMembers = database.groupMemberHome().findActiveByGroupID(groupID).iterator();
			LogData log = new LogData();
			log.setActingIPAddress(p.getIPAddress());
			log.setActingNetID(p.getPrincipalID());
			appendAssignment(log, assignmentID);
			log.setCourseID(new Long(assignment.getCourseID()));
			log.setLogName(LogBean.SUBMIT_SURVEY);
			log.setLogType(LogBean.LOG_GROUP);
			while (groupMembers.hasNext()) {
			    GroupMemberLocal m = (GroupMemberLocal) groupMembers.next();
			    appendReceiver(log, m.getNetID());
			}
			
			AnswerSetLocal answerSet = database.answerSetHome().create(assignmentID, groupID, groupID, netID, new Timestamp(System.currentTimeMillis()));
			
			Iterator i = answers.iterator();
			while (i.hasNext()) {
				AnswerData answerData = (AnswerData) i.next();
				long subID = answerData.getSubProblemID();
				String text = answerData.getText();
				database.answerHome().create(answerSet.getAnswerSetID(), subID, text);
			}
			appendDetail(log, p.getUserID() + " submitted response to survey " + assignment.getName());
			database.logHome().create(log);
		}
		catch (Exception e) {
			e.printStackTrace();
			ctx.setRollbackOnly();
			error = "Database failed to accept survey responses";
		}
		if (fail)
			ctx.setRollbackOnly();
		Profiler.exitMethod("TransactionBean.submitSurvey", "AssignmentID: " + assignmentID);
		return error;
	}
	
	/**
	 * Update the metadata for all the categories under a specific course
	 * @param option
	 * @return success
	 * @throws EJBException
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean updateCategoriesOption(CategoriesOption option, LogData log) throws EJBException{
		boolean success = true;
		try{
			Collection c = option.getCtgCollection();
			Iterator i = c.iterator();
			int numOfCtg = 0;
			while(i.hasNext() && success){//Update Categories that Already Exist
				CategoryInfo ctgInfo = (CategoryInfo)i.next();
				CategoryLocal ctg = database.categoryHome().findByPrimaryKey(new CategoryPK(ctgInfo.getCategoryID()));
				if(ctg == null) success = false;
				else{
				    if (ctg.getPositn() != ctgInfo.getPositn()) {
				        ctg.setPositn(ctgInfo.getPositn());
				        appendDetail(log, "Content table '" + ctg.getCategoryName() + "' moved to position " + ctgInfo.getPositn());
				    }
				    if (ctg.getHidden() != ctgInfo.getHidden()) {
					    ctg.setHidden(ctgInfo.getHidden());
					    appendDetail(log, "Content table '" + ctg.getCategoryName() + "' was " + (ctgInfo.getHidden() ? "removed" : "restored"));
					}
				}
				numOfCtg++;
			}
			c = option.getRestoreCollection();
			i = c.iterator();
			while(i.hasNext() && success){ //Restore Hidden Categories
				CategoryInfo ctgInfo = (CategoryInfo)i.next();
				CategoryLocal ctg = database.categoryHome().findByPrimaryKey(new CategoryPK(ctgInfo.getCategoryID()));
				if(ctg == null)success = false;
				else{
				    if (ctg.getHidden() != ctgInfo.getHidden()) {
				        ctg.setHidden(ctgInfo.getHidden());
				        appendDetail(log, "Content table '" + ctg.getCategoryName() + "' was " + (ctgInfo.getHidden() ? "removed" : "restored"));
				    }
					ctg.setPositn(numOfCtg++);
				}
			}
		}catch(Exception e){
			success = false;
			System.out.println(e.getMessage());
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Update the metadata for a given category and it's columns 
	 * @param templ - the template(metadata regarding category and columns)
	 * @return true on success, false on error
	 * @throws EJBException If the update is incomplete
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Required"
	 */
	public boolean updateCategory(Principal p, CategoryTemplate templ) throws EJBException {
		boolean success = false;
		try{
			CategoryLocal category = database.categoryHome().findByPrimaryKey(new CategoryPK(templ.getCategoryID()));
			LogData log = startLog(p);
			log.setCourseID(new Long(category.getCourseID()));
			log.setLogName(LogBean.EDIT_CATEGORY);
			log.setLogType(LogBean.LOG_CATEGORY);
			if(category != null){
				updateCategoryBean(category, templ, log);
				success = true;
			}
			appendDetail(log, "Updated template for content table '" + category.getCategoryName() + "'");
			success = createNewColumns(success, category, templ, log);
			success = updateOldColumns(success, category, templ, log);
			database.logHome().create(log);
		}catch(Exception e){
			success = false;
			System.out.println(e.getMessage());
		}
		if(ctx != null && !success)
			ctx.setRollbackOnly();
		return success;
	}
	
	/**
	 * Update the category database entry for the specified category; does not touch related tables
	 * @param category
	 * @param templ
	 * @throws Exception
	 */
	public void updateCategoryBean(CategoryLocal category, CategoryTemplate templ, LogData log) throws Exception {
		if (!category.getCategoryName().equals(templ.getCategoryName())) {
		    appendDetail(log, "Content '" + category.getCategoryName() + "' was renamed '" + templ.getCategoryName() + "'");
		    category.setCategoryName(templ.getCategoryName());
		}
		category.setAscending(templ.getAscending());
		category.setNumShowContents(templ.getNumShowItems());
		category.setAuthorzn(templ.getAuthorzn());
	}
	
	/**
	 * Update the metadata for columns that already exist for a specified category
	 * @param success
	 * @param category - the specified category bean
	 * @param templ - the metadata for the category and columns
	 * @return
	 * @throws Exception
	 */
	public boolean updateOldColumns(boolean success, CategoryLocal category, CategoryTemplate templ, LogData log) throws Exception{
		Collection oldColumnList = templ.getOldColumnList();
		Iterator i = oldColumnList.iterator();
		long totNumCol = templ.getTotalNumOfCol();
		while(i.hasNext() && success== true){
			CtgColInfo colInfo = (CtgColInfo)i.next();
			CategoryColLocal ctgCol = database.categoryColHome().findByPrimaryKey(new CategoryColPK(colInfo.getColumnID()));
			if(ctgCol == null) success = false;
			else
			{
				if(!ctgCol.getRemoved()) //column not removed
				{
					if(!ctgCol.getHidden()) //column not hidden
					{
						if(!ctgCol.getColName().equals(colInfo.getColName()))
						{
							appendDetail(log, "Column '" + ctgCol.getColName() + "' was renamed '" + colInfo.getColName() + "' in content '" + category.getCategoryName() + "'");
							ctgCol.setColName(colInfo.getColName());
						}
					}
				}
				else //column removed; want to restore
					ctgCol.setPosition(totNumCol++); //add it at the end so it does get a legal position
				//update the position whether the column is hidden, removed or not
				ctgCol.setPosition(colInfo.getPosition());
				if(ctgCol.getRemoved() != colInfo.getRemoved())
				{
					ctgCol.setRemoved(colInfo.getRemoved());
					appendDetail(log, "Column '" + colInfo.getColName() + "' in content '" + category.getCategoryName() + "' was " + (colInfo.getHidden() ? "removed" : "restored"));
				}
				else if(ctgCol.getHidden() != colInfo.getHidden())
				{
				    ctgCol.setHidden(colInfo.getHidden());
				    appendDetail(log, "Column '" + colInfo.getColName() + "' in content '" + category.getCategoryName() + "' was " + (colInfo.getHidden() ? "hidden" : "unhidden"));
				}
				if(colInfo.getSortByThis()) category.setSortByColId(ctgCol.getColID());
				//if we removed the column we were sorting by, we need to pick another column
				if(ctgCol.getRemoved() && category.getSortByColId() == ctgCol.getColID())
				{
					Collection columns = category.getColumns(false, false);
					Iterator i2 = columns.iterator();
					if(i2.hasNext()) category.setSortByColId(((CategoryColData)i2.next()).getColID());
				}
			}
		}
		return success;
	}
	
	/**
	 * @param p
	 * @param assignmentID
	 * @return
	 * @ejb.transaction type="Required"
	 * @ejb.interface-method view-type="local"
	 */
	public boolean uploadGradesTemplate(Principal p, long assignmentID) {
	    try {
	        AssignmentLocal assignment = database.assignmentHome().findByAssignmentID(assignmentID);
	        LogData log = startLog(p);
	        appendAssignment(log, assignmentID);
	        log.setCourseID(new Long(assignment.getCourseID()));
	        log.setLogName(LogBean.DOWNLOAD_ASSIGNMENT_GRADES);
	        log.setLogType(LogBean.LOG_GRADE);
	        Vector details = new Vector();
	        log.setDetailLogs(details);
	        database.logHome().create(log);
	    } catch (Exception e) {
	        ctx.setRollbackOnly();
	        e.printStackTrace();
	        return false;
	    }
	    return true;
	}
}
// vim: ts=4
