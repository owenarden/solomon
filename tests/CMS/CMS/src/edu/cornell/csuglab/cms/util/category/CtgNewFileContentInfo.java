/*
 * Created on Aug 31, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.util.category;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author evan
 *
 * Holds info for one category content of type file. This means all files
 * that are held within a certain row and column of a category; this can be
 * any non-negative number of files.
 * 
 * File info should be added a piece at a time with setFile() and setFileLabel();
 * each needs to be called once for each index within the cell (row-column combo).
 * 
 * It's safe to add file data where the file doesn't exist and the label is empty;
 * these data will not be returned by getFileInfoMap(), getFileInfos() or getFileLabels().
 */
public class CtgNewFileContentInfo extends CtgNewContentInfo implements CtgFileContentInfo
{
	private ArrayList fileInfos; //holds objects of type CtgFileInfo, in order as they are to appear
	private ArrayList fileLabels; //holds objects of type String, in order as they are to appear
	
	public CtgNewFileContentInfo(long rowID, long colID)
	{
		super(rowID, colID);
		this.fileInfos = new ArrayList();
		this.fileLabels = new ArrayList();
	}
	
	public long getNumFiles()
	{
		//assume fileInfos and fileLabels have the same size at this point; else doesn't make sense to ask for size
		return fileInfos.size();
	}
	
	/**
	 * Return the file info in our records corresponding to the given index, or null if there isn't
	 * one at index
	 * @param index
	 * @return
	 */
	public CtgFileInfo getFileInfo(int index)
	{
		if(index >= fileInfos.size()) return null;
		return (CtgFileInfo)fileInfos.get(index);
	}
	
	/**
	 * Return the file label in our records corresponding to the given index, or null if there isn't
	 * one at index. So null should be interpreted as the lack of a label rather than an empty label.
	 * @param index
	 * @return String
	 */
	public String getFileLabel(int index)
	{
		if(index >= fileLabels.size()) return null;
		return (String)fileLabels.get(index);
	}
	
	/**
	 * Should not return null.
	 * @return A List of FilePlusLabel objects, each of which holds (gasp!) a file info and a String.
	 * The Infos give file path info, and the Strings are file labels to be displayed online.
	 */
	public List getFileInfoLabelList()
	{
		/*
		 * use a list so we can be sure the order they'll come out of it in is the order we put them in in,
		 * and so the same order in which they'll appear in the database and on the relevant JSPs
		 */
		ArrayList list = new ArrayList();
		for(int i = 0; i < fileLabels.size(); i++)
		{
			list.add(new FilePlusLabel((CtgFileInfo)fileInfos.get(i), (String)fileLabels.get(i)));
		}
		return list;
	}
	
	/**
	 * Add a file info at the given index in the list, extending the list if necessary
	 * @param file
	 * @param index
	 */
	public void addFile(CtgFileInfo file, int index)
	{
		while(index > fileInfos.size()) fileInfos.add(null); //pad the list with empty data
		if(index == fileInfos.size()) fileInfos.add(file);
		else
		{
			fileInfos.remove(index);
			fileInfos.add(index, file);
		}
	}
	
	/**
	 * Add a file label at the given index in the list, extending the list if necessary
	 * @param fileLabel
	 * @param index
	 */
	public void addFileLabel(String fileLabel, int index)
	{
		while(index > fileLabels.size()) fileLabels.add(null); //pad the list with empty data
		if(index == fileLabels.size()) fileLabels.add(fileLabel);
		else
		{
			fileLabels.remove(index);
			fileLabels.add(index, fileLabel);
		}
	}
	
	/**
	 * Remove the file and label at the given index in our lists
	 * @param index
	 */
	public void removeByIndex(int index)
	{
		fileInfos.remove(index);
		fileLabels.remove(index);
	}
	
	/*
	 * CtgNewContentInfo functions. A null return value signifies no data.
	 */
	
	public String getColType()
	{
		return CtgUtil.CTNT_FILE;
	}
	
	public Timestamp getDate()       //used by contents of type date
	{
		return null;
	}
	
	public String getText()          //used by contents of type text
	{
		return null;
	}
	
	public Long getNumber()          //used by contents of type number
	{
		return null;
	}
	
	public String getURLAddress()    //used by contents of type url
	{
		return null;
	}
	
	public String getURLLabel()      //used by contents of type url
	{
		return null;
	}
	
	public ArrayList getFileInfos()  //used by contents of type file
	{
		return this.fileInfos;
	}
	
	public ArrayList getFileLabels() //used by contents of type file
	{
		return this.fileLabels;
	}
}
