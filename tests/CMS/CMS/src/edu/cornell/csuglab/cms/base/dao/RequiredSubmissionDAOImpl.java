/*
 * Created on Nov 4, 2004
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.RequiredSubmissionBean;
import edu.cornell.csuglab.cms.base.RequiredSubmissionDAO;
import edu.cornell.csuglab.cms.base.RequiredSubmissionPK;

/**
 * @author Jon
 *
 */
public class RequiredSubmissionDAOImpl extends DAOMaster implements RequiredSubmissionDAO {

	/*
	 * (non-Javadoc)
	 */
	public void load(RequiredSubmissionPK pk, RequiredSubmissionBean ejb)
			throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tRequiredSubmissions "
					+ "where SubmissionID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getSubmissionID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setSubmissionID(rs.getLong("SubmissionID"));
				ejb.setSubmissionName(rs.getString("SubmissionName"));
				ejb.setAssignmentID(rs.getLong("AssignmentID"));
				ejb.setMaxSize(rs.getInt("MaxSize"));
				ejb.setHidden(rs.getBoolean("Hidden"));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.RequiredFileDAO#store(edu.cornell.csuglab.cms.base.RequiredFileBean)
	 */
	public void store(RequiredSubmissionBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tRequiredSubmissions set SubmissionName = ?, " +
					"MaxSize = ?, Hidden = ? where AssignmentID = ? " +
					"and SubmissionID = ?";
			ps = conn.prepareStatement(query);
			if(ejb.getSubmissionName() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getSubmissionName());
			ps.setInt(2, ejb.getMaxSize());
			ps.setBoolean(3, ejb.getHidden());
			ps.setLong(4, ejb.getAssignmentID());
			ps.setLong(5, ejb.getSubmissionID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {
				f.printStackTrace();
			}
			throw new EJBException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.RequiredFileDAO#remove(edu.cornell.csuglab.cms.base.RequiredFilePK)
	 */
	public void remove(RequiredSubmissionPK pk) throws RemoveException,
			EJBException {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.RequiredFileDAO#create(edu.cornell.csuglab.cms.base.RequiredFileBean)
	 */
	public RequiredSubmissionPK create(RequiredSubmissionBean ejb)
			throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		RequiredSubmissionPK pk = null;
		try {
			conn = jdbcFactory.getConnection();
			String updateString = "insert into tRequiredSubmissions "
					+ "(AssignmentID, SubmissionName, MaxSize, Hidden) "
					+ "values (?, ?, ?, ?)";
			String findString = "select @@identity as 'SubmissionID' from tRequiredSubmissions";
			ps = conn.prepareStatement(updateString);
			ps.setLong(1, ejb.getAssignmentID());
			if(ejb.getSubmissionName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getSubmissionName());
			ps.setInt(3, ejb.getMaxSize());
			ps.setBoolean(4, ejb.getHidden());
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findString).executeQuery();
			if (count == 1 && rs.next()) {
				long submissionID = rs.getLong("SubmissionID");
				pk = new RequiredSubmissionPK(submissionID);
				ejb.setSubmissionID(submissionID);
			} else {
				throw new CreateException("Failed to create a new required submission");
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				e.printStackTrace();
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if(rs != null) rs.close();
			}
			catch (Exception f) {
				f.printStackTrace();
			}
			throw new CreateException(e.getMessage());
		}
		return pk;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.RequiredFileDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.RequiredFilePK)
	 */
	public RequiredSubmissionPK findByPrimaryKey(RequiredSubmissionPK key)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select SubmissionID from tRequiredSubmissions "
					+ "where SubmissionID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, key.getSubmissionID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find submission " + key.getSubmissionID());
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {
			}
			throw new FinderException(e.getMessage());
		}
		return key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.RequiredFileDAO#findByAssignmentID(long)
	 */
	public Collection findByAssignmentID(long assignmentid)
			throws FinderException {
		return findByAssignmentID(assignmentid, false);
	}
	
	public Collection findHiddenByAssignmentID(long assignmentid) throws FinderException {
		return findByAssignmentID(assignmentid, true);
	}
	
	private Collection findByAssignmentID(long assignmentid, boolean hidden) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select SubmissionID from tRequiredSubmissions "
					+ "where AssignmentID = ? and Hidden = ? order by SubmissionName ASC";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentid);
			ps.setBoolean(2, hidden);
			rs = ps.executeQuery();
			while (rs.next()) {
				RequiredSubmissionPK key = new RequiredSubmissionPK(rs.getLong("SubmissionID"));
				result.add(key);
			}
			conn.close();
			rs.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/**
	 * find all required submissions for currently-not-hidden assignments in the given course
	 */
	public Collection findByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select SubmissionID from tRequiredSubmissions r INNER JOIN " +
					"tAssignment a ON r.AssignmentID = a.AssignmentID " +
					"where a.CourseID = ? and a.Hidden = 0 and r.Hidden = ? order by a.DueDate, r.SubmissionName ASC";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				RequiredSubmissionPK key = new RequiredSubmissionPK(rs.getLong("SubmissionID"));
				result.add(key);
			}
			conn.close();
			rs.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RequiredSubmissionDAO#findByGroupID(long)
	 */
	public Collection findByGroupID(long groupID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select SubmissionID,SubmissionName from tRequiredSubmissions r, " +
					"tGroups g where r.AssignmentID = g.AssignmentID and g.GroupID = ? and " +
					"r.Hidden = ? order by SubmissionName";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, groupID);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				RequiredSubmissionPK key = new RequiredSubmissionPK(rs.getLong("SubmissionID"));
				result.add(key);
			}
			conn.close();
			rs.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

}
