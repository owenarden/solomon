/*
 * Created on Sep 8, 2005
 *
 * 
 */
package edu.cornell.csuglab.cms.util.category;

import java.util.List;

/**
 * @author evan
 *
 * Interface for any info that can add files
 */
public interface CtgFileContentInfo
{
	/**
	 * Should not return null.
	 * @return A List of FilePlusLabel objects, each of which holds (gasp!) a file info and a String.
	 * The Infos give file path info, and the Strings are file labels to be displayed online.
	 */
	public List getFileInfoLabelList();
	
	public long getNumFiles();
	
	/**
	 * Return the file info in our records corresponding to the given index, or null if there isn't
	 * one at index
	 * @param index
	 * @return
	 */
	public CtgFileInfo getFileInfo(int index);
	
	/**
	 * Return the file label in our records corresponding to the given index, or null if there isn't
	 * one at index. So null should be interpreted as the lack of a label rather than an empty label.
	 * @param index
	 * @return String
	 */
	public String getFileLabel(int index);
	
	/**
	 * Add a file info at the given index in the list, extending the list if necessary
	 * @param file
	 * @param index
	 */
	public void addFile(CtgFileInfo file, int index);
	
	/**
	 * Add a file label at the given index in the list, extending the list if necessary
	 * @param fileLabel
	 * @param index
	 */
	public void addFileLabel(String fileLabel, int index);
	
	/**
	 * Remove the file and label at the given index in our lists
	 * @param index
	 */
	public void removeByIndex(int index);
}
