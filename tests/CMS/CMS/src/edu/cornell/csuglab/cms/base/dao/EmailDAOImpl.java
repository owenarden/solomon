package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.EmailBean;
import edu.cornell.csuglab.cms.base.EmailDAO;
import edu.cornell.csuglab.cms.base.EmailPK;

/**
 * @author jfg32
 */
public class EmailDAOImpl extends DAOMaster implements EmailDAO {

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.EmailDAO#load(edu.cornell.csuglab.cms.base.EmailPK, edu.cornell.csuglab.cms.base.EmailBean)
     */
    public void load(EmailPK pk, EmailBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tEmails where EmailID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getEmailID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setEmailID(rs.getLong("EmailID"));
				ejb.setCourseID(rs.getLong("CourseID"));
				ejb.setRecipient(rs.getInt("Recipient"));
				ejb.setSubject(rs.getString("Subject"));
				try {
					ejb.setMessage(rs.getString("Message"));
				} catch (SQLException e) {
					ejb.setMessage("");
				}
				ejb.setDateSent(rs.getTimestamp("DateSent"));
				ejb.setSender(rs.getString("Sender"));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (rs != null) rs.close();
				if (ps != null) ps.close();
			} catch (Exception f) {}
			throw new EJBException(e);
		}
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.EmailDAO#store(edu.cornell.csuglab.cms.base.EmailBean)
     */
    public void store(EmailBean ejb) throws EJBException {
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.EmailDAO#remove(edu.cornell.csuglab.cms.base.EmailPK)
     */
    public void remove(EmailPK pk) throws RemoveException, EJBException {
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.EmailDAO#create(edu.cornell.csuglab.cms.base.EmailBean)
     */
    public EmailPK create(EmailBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		EmailPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "insert into tEmails "
					+ "(CourseID, Sender, Subject, Message, Recipient, "
					+ "DateSent) values "
					+ "(?, ?, ?, ?, ?, ?)";
			String getKey = "select @@identity as 'EmailID' from tEmails";
			ps = conn.prepareStatement(createString);
			ps.setLong(1, ejb.getCourseID());
			if(ejb.getSender() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getSender());
			if(ejb.getSubject() == null) ps.setNull(3, java.sql.Types.VARCHAR);
			else ps.setString(3, ejb.getSubject());
			if(ejb.getMessage() == null) ps.setNull(4, java.sql.Types.VARCHAR);
			else ps.setString(4, ejb.getMessage());
			ps.setInt(5, ejb.getRecipient());
			ps.setTimestamp(6, ejb.getDateSent());
			ps.executeUpdate();
			rs = conn.prepareStatement(getKey).executeQuery();
			if (rs.next()) {
				result = new EmailPK(rs.getLong("EmailID"));
				ejb.setEmailID(result.getEmailID());
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return result;
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.EmailDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.EmailPK)
     */
    public EmailPK findByPrimaryKey(EmailPK key) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select EmailID from tEmails where EmailID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, key.getEmailID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find email with ID = " + key.getEmailID());
			}
			conn.close();
			rs.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return key;
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.EmailDAO#findByCourseID(long)
     */
    public Collection findByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT EmailID FROM tEmails WHERE CourseID = ? " +
					"ORDER BY DateSent DESC";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
				EmailPK k = new EmailPK(rs.getLong("EmailID"));
				result.add(k);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
    }

}
