package edu.cornell.csuglab.cms.util;

/**
 * @author Evan
 * 
 * CourseAdminPermissions represents the various activity permissions staff members for a course can have.
 * It's just a data wrapper.
 * 
 * Unfortunately, it's hard to use this class much because StaffBean, which could return it from getters,
 * has to have other function signatures to fit into J2EE.
 */
public class CourseAdminPermissions
{
	//types of permissions
	private boolean admin;
	private boolean assignments;
	private boolean groups;
	private boolean grading;
	private boolean category;
	
	public CourseAdminPermissions(boolean admin, boolean assignments, boolean groups, boolean grading, boolean category)
	{
		this.admin = admin;
		if(this.admin) //admin precludes not having any other permissions
		{
			this.assignments = true;
			this.groups = true;
			this.grading = true;
			this.category = true;
		}
		else
		{
			this.assignments = assignments;
			this.groups = groups;
			this.grading = grading;
			this.category = category;
		}
	}
	
	public boolean getAdmin()
	{
		return this.admin;
	}
	
	public boolean getAssignments()
	{
		return this.assignments;
	}
	
	public boolean getGroups()
	{
		return this.groups;
	}
	
	public boolean getGrading()
	{
		return this.grading;
	}
	
	public boolean getCategory()
	{
		return this.category;
	}
	
	public String toString()
	{
		return "{adm=" + getAdmin() + ",asgn=" + getAssignments() + ",grp=" + getGroups() 
			+ ",grd=" + getGrading() + ",ctg=" + getCategory() + "}";
	}
}
