package edu.cornell.csuglab.cms.www.util;

import java.util.Iterator;
import java.util.List;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class FileTypeCategory {
	
	private String category;
	private String fileTypes;
	private List fileTypeList;
	//private String[] fileTypeArray;
	private Set fileTypeSet;
	
	public FileTypeCategory(String category, String fileTypes) {
		this.category = category;
		this.fileTypes = fileTypes;
		this.fileTypeList = EditAssignUtil.getFileTypeList(fileTypes);
		this.fileTypeSet = new TreeSet(fileTypeList);
		//this.fileTypeArray = ftListToFtArray(fileTypeList);
	}
	
	/*
	public FileTypeCategory(String category, String[] fileTypeArray) {
		this.category = category;
		this.fileTypeArray = fileTypeArray;
		//this.fileTypeList = ftArrayToFtList(fileTypeArray);
	}
	*/
	
	public String getCategory() {
		return category;
	}
	
	public String getFileTypes() {
		return fileTypes;
	}
	
	
	/*
	// input: fta = {"ft1", "ft2", ...}
	// output: ftl = "ft1, ft2, ..."
	private String ftArrayToFtList(String[] fta) {
		String ftl = "";
		for (int i = 0; i < fta.length; i++) {
			if (i > 0) {
				ftl += ", ";
			}
			ftl += fta[i];
		}
		return ftl;
	}
	
	// input: ftl = "ft1, ft2, ..."
	// output: fta = {"ft1", "ft2", ...}
	private String[] ftListToFtArray(String ftl) {
		// split on anything that is not a letter, number, or period
		String[] fta = fileTypeList.split("[^a-zA-Z0-9\\.]");
		return fta;
	}
	*/
	
	public boolean matchesFileTypes(List ftl) {
		Set ftSet = new TreeSet(ftl);
		return fileTypeSet.equals(ftSet);
	}

}
