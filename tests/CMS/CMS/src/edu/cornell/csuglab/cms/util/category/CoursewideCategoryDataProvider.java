package edu.cornell.csuglab.cms.util.category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.cornell.csuglab.cms.base.CategoryColData;
import edu.cornell.csuglab.cms.base.CategoryContentsData;
import edu.cornell.csuglab.cms.base.CategoryFileData;
import edu.cornell.csuglab.cms.base.CategoryRowData;

/**
 * A data provider that handles all categories for a given course
 */
public class CoursewideCategoryDataProvider implements CategoryDataProvider
{
	//hold all category data for a given course
	private HashMap idContentsMap, //map category IDs (Longs) to ArrayLists of CategoryColDatas
		idFilesMap, //map category IDs (Longs) to ArrayLists of CategoryFileDatas
		idColumnsMap, //map category IDs (Longs) to ArrayLists of CategoryColDatas
		rowIDMap, //map row IDs (Longs) to CategoryRowDatas
		colIDMap, //map col IDs (Longs) to CategoryColDatas
		sortedRowsMap; //map category IDs (Longs) to Lists of row IDs (Longs) in sorted order
	private long currentCtgID; //which category in the course we're currently representing
	
	/**
	 * Set up structures so we can return columns, contents and files by category later
	 * (note this object takes in all data for a given course and provides just the data for
	 * one category at a time). This includes sorting data within each category by that category's
	 * sort order.
	 * @param columns All CategoryColDatas belonging to a given course, ordered by category position, 
	 * then column position within category
	 * @param rows All CategoryRowDatas belonging to a given course, ordered by category position
	 * @param contents All CategoryContentsDatas belonging to a given course, ordered by category ID
	 * @param files All CategoryFileDatas belonging to a given course, ordered by parent content ID
	 */
	public CoursewideCategoryDataProvider(List columns, List rows, List contents, List files)
	{
		//rows are only needed by getContentsCollection() below; map them by ID to make search quick
		this.rowIDMap = new HashMap();
		for(int i = 0; i < rows.size(); i++)
		{
			CategoryRowData row = (CategoryRowData)rows.get(i);
			rowIDMap.put(new Long(row.getRowID()), row);
		}
		//columns are needed to determine data types of contents that don't exist in the database
		//also map category IDs to all of contents, files and columns to make them easier to find later:
		this.idContentsMap = new HashMap();
		this.idFilesMap = new HashMap();
		this.idColumnsMap = new HashMap();
		this.colIDMap = new HashMap();
		//run through columns, get category ID
		for(int i = 0; i < columns.size(); i++)
		{
			CategoryColData column = (CategoryColData)columns.get(i);
			colIDMap.put(new Long(column.getColID()), column); //build the colID -> colData map
			Long ctgID = new Long(column.getCategoryID());
			addToArrayByID(idColumnsMap, ctgID, column);
			//run through contents belonging to this column
			for(int j = 0; j < contents.size(); j++)
			{
				CategoryContentsData content = (CategoryContentsData)contents.get(j);
				if(content.getColumnID() == column.getColID())
				{
					addToArrayByID(idContentsMap, new Long(column.getCategoryID()), content);
					//run through files belonging to this content
					for(int k = 0; k < files.size(); k++)
					{
						CategoryFileData file = (CategoryFileData)files.get(k);
						if(file.getContentID() == content.getContentID())
						{
							addToArrayByID(idFilesMap, new Long(column.getCategoryID()), file);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Add the given value to the given map from Longs (IDs of some kind) to ArrayLists
	 * @param map The map to which to add the value
	 * @param id The key for the value
	 * @param value The value to add to an array
	 */
	private void addToArrayByID(Map map, Long id, Object value)
	{
		if(map.containsKey(id)) ((ArrayList)map.get(id)).add(value);
		else
		{
			ArrayList array = new ArrayList();
			array.add(value);
			map.put(id, array);
		}
	}
	
	public void setCategoryID(long id)
	{
		this.currentCtgID = id;
	}
	
	/**
	 * 
	 * @return The ID of the category we represent
	 */
	public long getCategoryID() 
	{
		return this.currentCtgID;
	}
	
	/**
	 * Return the column data for the column with the given id, or null if the ID isn't found in our list
	 * (assume the column belongs to our currently set category)
	 * @param colID
	 * @return CategoryColData
	 */
	public CategoryColData findColumnByID(long colID) 
	{
		ArrayList colsByCategory = (ArrayList)idColumnsMap.get(new Long(currentCtgID));
		for(int i = 0; i < colsByCategory.size(); i++)
		{
			CategoryColData column = (CategoryColData)colsByCategory.get(i);
			if(column.getColID() == colID)
				return column;
		}
		return null;
	}
	
	/**
	 * Return a Collection of contents belonging to our category,
	 * limiting search to visible rows and/or columns as desired
	 * @param visibleRows Whether to limit our search to contents whose rows are visible
	 * @param visibleCols Whether to limit our search to contents whose columns are visible
	 * @return A non-null Collection of CategoryContentsDatas
	 */
	public Collection getContentsCollection(boolean visibleRows, boolean visibleCols) 
	{
		ArrayList toReturn = new ArrayList();
		ArrayList contentsByCategory = (ArrayList)idContentsMap.get(new Long(currentCtgID));
		if(contentsByCategory == null) return toReturn; //no contents yet in this category
		for(int i = 0; i < contentsByCategory.size(); i++)
		{
			CategoryContentsData content = (CategoryContentsData)contentsByCategory.get(i);
			//if we need to test for visibility of row, do so; same for column
			if(	(!visibleRows || !((CategoryRowData)rowIDMap.get(new Long(content.getRowID()))).getHidden())
				&& (!visibleCols || !findColumnByID(content.getColumnID()).getHidden())
				&& !findColumnByID(content.getColumnID()).getRemoved())
				toReturn.add(content);
		}
		return toReturn;
	}
	
	/**
	 * Return a list of all files belonging to our category, visible and hidden,
	 * ordered by ID of parent content
	 * @return A non-null Collection of CategoryFileDatas
	 */
	public Collection getFilesCollection() 
	{
		ArrayList fileList = (ArrayList)idFilesMap.get(new Long(currentCtgID));
		return (fileList == null) ? new ArrayList() : fileList;
	}
	
	/**
	 * Return a list of all columns belonging to our category, restricting to visible columns if desired,
	 * ordered by position within the category, but excluding removed columns
	 * @param visible Whether to limit our search to visible columns
	 * @return A Collection of CategoryColumnDatas
	 */
	public Collection getNonremovedColumnsCollection(boolean visible) {
		ArrayList columnsByCategory = (ArrayList)idColumnsMap.get(new Long(currentCtgID));
		//if the current category has no columns, the returned ArrayList will be null; reassign it to avoid errors
		if(columnsByCategory == null) columnsByCategory = new ArrayList();
		ArrayList toReturn = new ArrayList();
		for(int i = 0; i < columnsByCategory.size(); i++) {
			CategoryColData column = (CategoryColData)columnsByCategory.get(i);
			if((!visible || !column.getHidden()) && !column.getRemoved())
				toReturn.add(column);
		}
		return toReturn;
	}
	
	/**
	 * Return a list of all currently removed columns belonging to our category
	 * @return A Collection of CategoryColumnDatas
	 */
	public Collection getRemovedColumnsCollection()	{
		ArrayList columnsByCategory = (ArrayList)idColumnsMap.get(new Long(currentCtgID));
		//if the current category has no columns, the returned ArrayList will be null; reassign it to avoid errors
		if(columnsByCategory == null) columnsByCategory = new ArrayList();
		ArrayList toReturn = new ArrayList();
		for(int i = 0; i < columnsByCategory.size(); i++) {
			CategoryColData column = (CategoryColData)columnsByCategory.get(i);
			if(column.getRemoved()) toReturn.add(column);
		}
		return toReturn;
	}
}
