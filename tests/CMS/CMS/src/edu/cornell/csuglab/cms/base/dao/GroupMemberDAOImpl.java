/*
 * Created on Mar 18, 2004
 */

package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.GroupMemberBean;
import edu.cornell.csuglab.cms.base.GroupMemberDAO;
import edu.cornell.csuglab.cms.base.GroupMemberPK;
import edu.cornell.csuglab.cms.base.StudentBean;

/**
 * @author Theodore Chao
 */
public class GroupMemberDAOImpl extends DAOMaster implements GroupMemberDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupMemberDAO#load(edu.cornell.csuglab.cms.base.GroupMemberPK,
	 *      edu.cornell.csuglab.cms.base.GroupMemberBean)
	 */
	public void load(GroupMemberPK pk, GroupMemberBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tGroupMembers "
					+ "where NetID = ? and GroupID = ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, pk.getNetID());
			ps.setLong(2, pk.getGroupID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setGroupID(rs.getLong("GroupID"));
				ejb.setNetID(rs.getString("NetID").trim());
				ejb.setStatus(rs.getString("Status").trim());
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupMemberDAO#store(edu.cornell.csuglab.cms.base.GroupMemberBean)
	 */
	public void store(GroupMemberBean ejb) throws EJBException {
		preStore("GroupMember", ejb);
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tGroupMembers set Status = ? "
					+ "where NetID = ? and GroupID = ?";
			ps = conn.prepareStatement(query);
			if(ejb.getStatus() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getStatus());
			if(ejb.getNetID() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getNetID());
			ps.setLong(3, ejb.getGroupID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupMemberDAO#remove(edu.cornell.csuglab.cms.base.GroupMemberPK)
	 */
	public void remove(GroupMemberPK pk) throws RemoveException, EJBException {
		preRemove("GroupMember");
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "delete from tGroupMembers "
					+ "where GroupID = ? and NetID = ?";
			ps = conn.prepareStatement(update);
			ps.setLong(1, pk.getGroupID());
			ps.setString(2, pk.getNetID());
			ps.executeUpdate(); 
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupMemberDAO#create(edu.cornell.csuglab.cms.base.GroupMemberBean)
	 */
	public GroupMemberPK create(GroupMemberBean ejb) throws CreateException,
			EJBException {
		preCreate("GroupMember", ejb);
		Connection conn = null;
		PreparedStatement ps = null;
		GroupMemberPK pk = null;
		try {
			conn = jdbcFactory.getConnection();
			String updateString = "insert into tGroupMembers "
					+ "(GroupID, NetID, Status) values "
					+ "(?, ?, ?)";
			ps = conn.prepareStatement(updateString);
			ps.setLong(1, ejb.getGroupID());
			if(ejb.getNetID() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getNetID());
			if(ejb.getStatus() == null) ps.setNull(3, java.sql.Types.VARCHAR);
			else ps.setString(3, ejb.getStatus());
			int count = ps.executeUpdate();
			if (count > 0) {
				pk = new GroupMemberPK(ejb.getGroupID(), ejb.getNetID());
			}
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			}
			catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return pk;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupMemberDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.GroupMemberPK)
	 */
	public GroupMemberPK findByPrimaryKey(GroupMemberPK key)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select GroupID, NetID from tGroupMembers "
					+ "where GroupID = ? and NetID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, key.getGroupID());
			ps.setString(2, key.getNetID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find group member with NetID = " + key.getNetID() +
						" in group " + key.getGroupID());
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupMemberDAO#findByAssignmentID(long)
	 */
	public Collection findActiveByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT g.GroupID, m.NetID FROM tGroups g INNER JOIN " +
				"tGroupMembers m ON g.GroupID = m.GroupID INNER JOIN " +
				"tAssignment a ON g.AssignmentID = a.AssignmentID INNER JOIN " +
				"tStudent s ON m.NetID = s.NetID AND a.CourseID = s.CourseID INNER JOIN " +
				"tUser u ON m.NetID = u.NetID " +
				"WHERE (s.Status = ?) AND (m.Status = ?) AND (a.AssignmentID = ?) " +
				"ORDER BY m.NetID, u.LastName, u.FirstName";
			ps = conn.prepareStatement(queryString);
		    ps.setString(1, StudentBean.ENROLLED);
			ps.setString(2, GroupMemberBean.ACTIVE);
			ps.setLong(3, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GroupMemberPK(rs.getLong("GroupID"), rs.getString("NetID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findAssignedActiveByAssignmentID(String netID, long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT DISTINCT g.GroupID, u.Lastname, u.Firstname, m.NetID FROM tGroups g INNER JOIN " +
	            "tGroupMembers m ON g.GroupID = m.GroupID INNER JOIN " +
	            "tGroupAssignedTo ga ON m.GroupID = ga.GroupID INNER JOIN " +
	            "tAssignment a ON g.AssignmentID = a.AssignmentID INNER JOIN " +
	            "tStudent s ON a.CourseID = s.CourseID AND m.NetID = s.NetID INNER JOIN " +
	            "tUser u ON m.NetID = u.NetID CROSS JOIN tSubProblems p " +
	            "WHERE ((s.Status = ?) AND (m.Status = ?) AND (p.SubProblemID = ga.SubProblemID) AND (p.AssignmentID = a.AssignmentID) AND (p.Hidden = ?) AND (g.AssignmentID = ?) AND (ga.NetID = ?)) " +
	            "OR ((s.Status = ?) AND (m.Status = ?) AND (g.AssignmentID = ?) AND (ga.NetID = ?) AND (ga.SubProblemID = ?)) " +
	            "ORDER BY m.NetID, u.LastName, u.FirstName";			
			ps = conn.prepareStatement(queryString);
		    ps.setString(1, StudentBean.ENROLLED);
		    ps.setString(2, GroupMemberBean.ACTIVE);
			ps.setBoolean(3, false);
			ps.setLong(4, assignmentID);
			ps.setString(5, netID);
		    ps.setString(6, StudentBean.ENROLLED);
		    ps.setString(7, GroupMemberBean.ACTIVE);
			ps.setLong(8, assignmentID);
			ps.setString(9, netID);
			ps.setLong(10, 0);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GroupMemberPK(rs.getLong("GroupID"), rs.getString("NetID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findByNetIDCourseID(String netID, long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT you.NetID, you.GroupID " +
					"FROM tAssignment a INNER JOIN " +
                      "tGroups g ON a.AssignmentID = g.AssignmentID INNER JOIN " +
                      "tGroupMembers me INNER JOIN tGroupMembers you ON me.GroupID = you.GroupID " +
                      "ON g.GroupID = me.GroupID " +
                      "WHERE (a.CourseID = ?) AND (me.NetID = ?) AND (me.Status = ?) AND (you.Status = ?) " +
                      "ORDER BY a.DueDate";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setString(2, netID);
			ps.setString(3, GroupMemberBean.ACTIVE);
			ps.setString(4, GroupMemberBean.ACTIVE);
			rs = ps.executeQuery();
			while (rs.next()) {
				GroupMemberPK pk = new GroupMemberPK(rs.getLong("GroupID"), rs
						.getString("NetID").trim());
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupMemberDAO#findByGroupID(long)
	 */
	public Collection findByGroupID(long groupID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select GroupID, NetID from tGroupMembers "
					+ "where GroupID = ? order by NetID asc";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, groupID);
			rs = ps.executeQuery();
			while (rs.next()) {
				GroupMemberPK pk = new GroupMemberPK(rs.getLong(1), rs
						.getString(2).trim());
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	public Collection findActiveByGroupID(long groupID) throws FinderException {
		return findByGroupIDStatus(groupID, GroupMemberBean.ACTIVE);
	}
	
	public Collection findByGroupIDStatus(long groupID, String status) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select distinct GroupID, NetID from tGroupMembers "
					+ "where GroupID = ? and Status = ? order by NetID asc";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, groupID);
			ps.setString(2, status);
			rs = ps.executeQuery();
			while (rs.next()) {
				GroupMemberPK pk = new GroupMemberPK(rs.getLong("GroupID"), rs.getString("NetID").trim());
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	public Collection findInvitesByGroupID(long groupID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select GroupID, NetID from tGroupMembers "
					+ "where GroupID = ? and Status = ? order by NetID asc";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, groupID);
			ps.setString(2, GroupMemberBean.INVITED);
			rs = ps.executeQuery();
			while (rs.next()) {
				GroupMemberPK pk = new GroupMemberPK(rs.getLong(1), rs
						.getString(2).trim());
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GroupMemberDAO#findByNetID(java.lang.String)
	 */
	public Collection findByNetID(String netID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select GroupID, NetID from tGroupMembers "
					+ "where NetID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netID);
			rs = ps.executeQuery();
			while (rs.next()) {
				GroupMemberPK pk = new GroupMemberPK(rs.getLong(1), rs
						.getString(2).trim());
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupMemberDAO#findByNetIDAssignmentID(java.lang.String, long)
	 */
	public Collection findByNetIDAssignmentID(String netID, long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select m.GroupID, m.NetID from tGroupMembers m, tGroups g " +
					"where g.GroupID = m.GroupID and m.NetID = ? and " + 
					"g.AssignmentID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netID);
			ps.setLong(2, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				GroupMemberPK pk = new GroupMemberPK(rs.getLong(1), 
						rs.getString(2).trim());
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findNonRejectedByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT m.GroupID, m.NetID FROM tGroupMembers m " +
				"INNER JOIN tGroups g ON m.GroupID = g.GroupID " +
				"WHERE (g.AssignmentID = ?) AND (NOT m.Status = ?)";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentID);
			ps.setString(2, GroupMemberBean.REJECTED);
			rs = ps.executeQuery();
			while (rs.next()) {
				GroupMemberPK pk = new GroupMemberPK(rs.getLong(1), 
						rs.getString(2).trim());
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupMemberDAO#findByNetIDAssignmentID(java.lang.String, long)
	 */
	public GroupMemberPK findActiveByNetIDAssignmentID(String netID, long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		GroupMemberPK pk = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select m.GroupID, m.NetID from tGroupMembers m, tGroups g " +
					"where g.GroupID = m.GroupID and m.NetID = ? and " + 
					"g.AssignmentID = ? and m.Status = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netID);
			ps.setLong(2, assignmentID);
			ps.setString(3, GroupMemberBean.ACTIVE);
			rs = ps.executeQuery();
			boolean found = false;
			while (rs.next()) {
				if (!found) {
					pk = new GroupMemberPK(rs.getLong(1), rs.getString(2).trim());
					found = true;
				}
				else
					throw new FinderException("User is active in more than one group for this assignment!");
			}
			if (!found) {
				throw new FinderException("User is not active in any group");
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupMemberDAO#findByInviters(java.util.Collection, long)
	 */
	public Collection findByInviters(Collection netids, long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select two.NetID, two.GroupID from tGroupMembers one, tGroupMembers two, tGroups g where " +
					"one.GroupID = two.GroupID and g.GroupID = one.GroupID and g.AssignmentID = ? and " +	
					"one.Status = ? and two.Status = ? and (";
			for (int i=0; i < netids.size() - 1; i++) {
				query += "one.NetID = ? or ";
			}
			query += "one.NetID = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			ps.setString(2, "Active");
			ps.setString(3, "Invited");
			int count = 0;
			Iterator i = netids.iterator();
			while (i.hasNext()) {
				ps.setString((count++) + 4, (String) i.next());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GroupMemberPK(rs.getLong("GroupID"),
						rs.getString("NetID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupMemberDAO#findActiveByNetIDsAssignmentID(java.util.Collection, long)
	 */
	public Collection findActiveByNetIDsAssignmentID(Collection netids, long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (netids.size() == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select m.GroupID, m.NetID from tGroupMembers m, tGroups g " +
					"where g.GroupID = m.GroupID and g.AssignmentID = ? and m.Status = ? and (";
			for (int i=0; i < netids.size() - 1; i++) {
				query += "m.NetID = ? or ";
			}
			query += "m.NetID = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			ps.setString(2, GroupMemberBean.ACTIVE);
			Iterator i = netids.iterator();
			int count = 0;
			while (i.hasNext()) {
				ps.setString((count++) + 3, (String) i.next());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GroupMemberPK(rs.getLong("GroupID"), rs.getString("NetID").trim()));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findInvitersByNetIDAssignmentID(String invitedNetID, long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT m2.NetID, m2.GroupID " +
					"FROM tGroupMembers m1 INNER JOIN " +
					  "tGroups g ON m1.GroupID = g.GroupID INNER JOIN " +
					  "tGroupMembers m2 ON g.GroupID = m2.GroupID " +
					"WHERE (m1.NetID = ?) AND (m1.Status = ?) AND (m2.Status = ?) AND (g.AssignmentID = ?)";
			ps = conn.prepareStatement(query);
			ps.setString(1, invitedNetID);
			ps.setString(2, GroupMemberBean.INVITED);
			ps.setString(3, GroupMemberBean.ACTIVE);
			ps.setLong(4, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GroupMemberPK(rs.getLong("GroupID"), rs.getString("NetID").trim()));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	public Collection findByGroupIDsAssignedTo(String grader, long assignmentID, Collection groupids) throws FinderException {
		Connection conn = null;
		PreparedStatement getPrivs = null, getGroups = null;
		ResultSet privs = null, groups = null;
		Collection result = new ArrayList();
		if (groupids.size() == 0) return result; 
		try {
		    conn= jdbcFactory.getConnection();
		    /*
			String privsQuery = "select s.AdminPriv, a.AssignedGraders from tStaff s, tAssignment a where " +
			"a.AssignmentID = ? and s.CourseID = a.CourseID and s.NetID = ? and " +
			"(s.GradesPriv = ? or s.AdminPriv = ?)";
			getPrivs = conn.prepareStatement(privsQuery);
			getPrivs.setLong(1, assignmentID);
			getPrivs.setString(2, grader);
			getPrivs.setBoolean(3, true);
			getPrivs.setBoolean(4, true);
			privs = getPrivs.executeQuery();
			boolean fullPrivilege = false;
			if (privs.next()) {
				if (privs.getBoolean("AdminPriv")) fullPrivilege = true;
				if (!privs.getBoolean("AssignedGraders")) fullPrivilege = true;
			} else {
				throw new FinderException("User does not have access to view this page");
			}*/
			/* Find all groups in the assignment with active group members, 
			 * and how many submissions they still have left to submit */
			String groupsQueryAll = "select g.GroupID, m.netID ,g.RemainingSubmissions, u.FirstName, u.LastName " + 
					"from tGroups g, tGroupMembers m, tStudent s, tAssignment a, tUser u where g.GroupID = m.GroupID " + 
					"and m.Status = ? and g.AssignmentID = a.AssignmentID and a.AssignmentID = ? and s.NetID = m.NetID and u.NetID = s.NetID and " +
					"s.CourseID = a.CourseID and s.Status = ? and (";
			/* Finds all groups to which the given netID has been assigned to at least one
			 * non-hidden subproblem */
			String groupsQueryAssigned = "select distinct g.GroupID, m.netID ,g.RemainingSubmissions, u.FirstName, u.LastName " +
					"from tGroups g, tGroupMembers m, tGroupAssignedTo ga, tSubProblems p, tStudent s, tAssignment a, tUser u where g.GroupID = m.GroupID " +
					"and ga.GroupID = m.GroupID and p.AssignmentID = m.AssignmentID and " +
					"((p.SubProblemID = ga.SubProblemID and p.Hidden = ?) or ga.SubProblemID = ?) " +
					"and m.Status = ? and g.AssignmentID = a.AssignmentID and a.AssignmentID = ? and ga.NetID = ? and " +
					"s.NetID = m.NetID and u.NetID = s.NetID and s.CourseID = a.CourseID and s.Status = ? and (";
			for (int i=0; i < groupids.size() - 1; i++) {
				groupsQueryAll += "g.GroupID = ? or ";
				groupsQueryAssigned += "g.GroupID = ? or ";
			}
			groupsQueryAll += "g.GroupID = ?) order by u.LastName, u.FirstName, m.NetID ASC";
			groupsQueryAssigned += "g.GroupID = ?) order by u.LastName, u.FirstName, m.NetID ASC";
			//String groupsQuery = (fullPrivilege ? groupsQueryAll : groupsQueryAssigned);
			getGroups = conn.prepareStatement(groupsQueryAll);
			//if (fullPrivilege) {
				getGroups.setString(1, "Active");
				getGroups.setLong(2, assignmentID);
				getGroups.setString(3, StudentBean.ENROLLED);
			/*} else {
				getGroups.setBoolean(1, false);
				getGroups.setLong(2, 0);
				getGroups.setString(3, "Active");
				getGroups.setLong(4, assignmentID);
				getGroups.setString(5, grader);
				getGroups.setString(6, StudentBean.ENROLLED);
			}*/
			int c=4;
			for (Iterator i=groupids.iterator(); i.hasNext(); ) {
				//getGroups.setLong((fullPrivilege ? 4 : 7) + i, groupids[i]);
			    getGroups.setLong(c++, ((Long)i.next()).longValue());
			}
			groups = getGroups.executeQuery();
			while (groups.next()) {
				long groupID = groups.getLong("GroupID");
				String netID = groups.getString("NetID");
				result.add(new GroupMemberPK(groupID, netID));
			}
			conn.close();
			//getPrivs.close();
			//privs.close();
			getGroups.close();
			groups.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (getPrivs != null) getPrivs.close();
				if (privs != null) privs.close();
				if (getGroups != null) getGroups.close();
				if (groups != null) groups.close();
			} catch (Exception f) {}
			e.printStackTrace();
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
}
