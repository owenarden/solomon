/*
 * Created on Apr 7, 2005
 */
package edu.cornell.csuglab.cms.author;

import java.util.Collection;
import edu.cornell.csuglab.cms.base.*;

/**
 * @author yc263
 *
 * UserAuthenticated represents a user that has been authenticated into Kerberos
 */
public class UserAuthenticated extends UserWrapper {
	
	public UserAuthenticated(UserLocal user){
		super(user);
	}
	
	public int getUserType(){
		return UT_AUTHENTICATED;
	}
	
	public int getAuthoriznLevelByCourseID(long courseID) {
		if(isStudentInCourseByCourseID(courseID)) return Principal.AUTHOR_STUDENT;
  		else if(isStaffInCourseByCourseID(courseID)) return Principal.AUTHOR_STAFF;
  		else return Principal.AUTHOR_CORNELL_COMMUNITY;
	}
	
	public String getUserID() {
		return user.getUserID();
	}
	
	public String getCUID()  {
		return user.getCUID();
	}
	
	public String getFirstName()  {
		return user.getFirstName();
	}
	
	public String getLastName()  {
		return user.getLastName();
	}

	public String getCollege()  {
		return user.getCollege();
	}
	
	public UserData getUserData()  {
		return user.getUserData();	
	} 
	
	public boolean isStudentInCourseByCourseID(long courseID) {
		return user.isStudent(courseID);
	}
	
	public boolean isStaffInCourseByCourseID(long courseID) {
		return user.isStaff(courseID);
	}
	
	public boolean isAdminPrivByCourseID(long courseID) {
		return user.isAdminPrivByCourseID(courseID);
	}
	
	public boolean isAssignPrivByCourseID(long courseID) {
		return user.isAssignPrivByCourseID(courseID);
	}
	public boolean isCategoryPrivByCourseID(long courseID) {
		return user.isCategoryPrivByCourseID(courseID);
	}
	public boolean isGradesPrivByCourseID(long courseID) {
		return user.isGradesPrivByCourseID(courseID);
	}
	public boolean isGroupsPrivByCourseID(long courseID) {
		return user.isGroupsPrivByCourseID(courseID);
	}
	public boolean isCMSAdmin() {
		return user.isCMSAdmin();
	}
	
	public boolean isCMSSubAdmin() {
		return user.isCMSSubAdmin();
	}
	
	public int getCMSSubAdminDomain() {
		return user.getCMSSubAdminDomain();
	}
	
	public boolean isPWExpired() {
		return user.isPWExpired();
	}
	
	public boolean isDeactivated() {
		return user.isDeactivated();
	}
	
	/**
	 * Returns the student courses that user is in
	 * @return
	 * @
	 */
	public Collection getStudentCourses()  {
		return user.getStudentCourses();
	}
	
	/**
	 * Returns the staff courses that user is in
	 * @return
	 * @
	 */
	public Collection getStaffCourses()  {
		return  user.getStaffCourses();
	}
	
}


