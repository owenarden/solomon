package edu.cornell.csuglab.cms.util;

import java.util.Map;

/**
 * @author Evan
 * 
 * CourseProperties represents the general properties a course can have.
 * It's just a data wrapper.
 */
public class CourseProperties
{
	private String name;
	private String code;					//the actual code, e.g. COM S 211
	private String displayedCode;		//the code the professor wants shown, e.g. CS 211
	private String description;
	private boolean courseFrozen;
	private boolean showFinalGrades;
	private boolean showTotalScores;
	private boolean showAssignWeights;
	private boolean showGraderNetID;
	private boolean hasSection;
	private boolean courseGuestAccess;
	private boolean assignGuestAccess;
	private boolean announceGuestAccess;
	private boolean solutionGuestAccess;
	private boolean courseCCAccess;
	private boolean assignCCAccess;
	private boolean announceCCAccess;
	private boolean solutionCCAccess;	
	
	private Map staffPermissions; //maps Strings (netIDs) to CourseAdminPermissions objects
	
	public CourseProperties(String name, String code, String displayedCode, String description, boolean courseFrozen, boolean showFinalGrades, boolean showTotalScores, boolean showAssignWeights, boolean showGraderNetID,
			boolean hasSection, boolean courseGuestAccess, boolean assignGuestAccess, boolean announceGuestAccess, boolean solutionGuestAccess, boolean courseCCAccess, boolean assignCCAccess,
			boolean announceCCAccess, boolean solutionCCAccess)
	{
		this(name, code, displayedCode, description, courseFrozen, showFinalGrades, showTotalScores, showAssignWeights, showGraderNetID, hasSection, courseGuestAccess, assignGuestAccess, announceGuestAccess, 
		     solutionGuestAccess, courseCCAccess, assignCCAccess, announceCCAccess, solutionCCAccess, null);
	}
	
	public CourseProperties(String name, String code, String displayedCode, String description, boolean courseFrozen, boolean showFinalGrades, boolean showTotalScores, boolean showAssignWeights, boolean showGraderNetID,
			boolean hasSection, boolean courseGuestAccess, boolean assignGuestAccess, boolean announceGuestAccess, boolean solutionGuestAccess, boolean courseCCAccess, boolean assignCCAccess,
			boolean announceCCAccess, boolean solutionCCAccess, Map staffPermissions)
	{
		this.name = name;
		this.code = code;
		this.displayedCode = displayedCode;
		this.description = description;
		this.courseFrozen = courseFrozen;
		this.showFinalGrades = showFinalGrades;
		this.showTotalScores = showTotalScores;
		this.showAssignWeights = showAssignWeights;
		this.showGraderNetID = showGraderNetID;
		this.hasSection = hasSection;
		this.staffPermissions = staffPermissions;
		this.courseGuestAccess = courseGuestAccess;
		this.announceGuestAccess = announceGuestAccess;
		this.assignGuestAccess = assignGuestAccess;
		this.solutionGuestAccess = solutionGuestAccess;
		this.courseCCAccess = courseCCAccess;
		this.assignCCAccess = assignCCAccess;
		this.announceCCAccess = announceCCAccess;
		this.solutionCCAccess = solutionCCAccess;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getCode()
	{
		return code;
	}
	
	public String getDisplayedCode()
	{
		return displayedCode;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public boolean isFreezeCourse()
	{
		return courseFrozen;
	}
	
	public boolean isShowFinalGrades()
	{
		return showFinalGrades;
	}
	
	public boolean isShowTotalScores()
	{
		return showTotalScores;
	}
	
	public boolean isShowAssignWeights()
	{
		return showAssignWeights;
	}
	
	public boolean isShowGraderNetID()
	{
		return showGraderNetID;
	}
	
	public Map getStaffPermissions()
	{
		return staffPermissions;
	}
	/**
	 * @return Returns the announceGuestAccess.
	 */
	public boolean isAnnounceGuestAccess() {
		return announceGuestAccess;
	}

	/**
	 * @return Returns the assignGuestAccess.
	 */
	public boolean isAssignGuestAccess() {
		return assignGuestAccess;
	}

	/**
	 * @return Returns the courseGuestAccess.
	 */
	public boolean isCourseGuestAccess() {
		return courseGuestAccess;
	}
	
	public boolean isSolutionGuestAccess() {
	    return solutionGuestAccess;
	}
	
	public boolean isCourseCCAccess() {
	    return courseCCAccess;
	}
	
	public boolean isAssignCCAccess() {
	    return assignCCAccess;
	}
	
	public boolean isAnnounceCCAccess() {
	    return announceCCAccess;
	}
	
	public boolean isSolutionCCAccess() {
	    return solutionCCAccess;
	}
	
	public boolean hasSection() {
		return hasSection;
	}

}
