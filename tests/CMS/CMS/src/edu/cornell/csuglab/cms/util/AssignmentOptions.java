/*
 * Created on Sep 29, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package edu.cornell.csuglab.cms.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;

import edu.cornell.csuglab.cms.base.AssignmentBean;
import edu.cornell.csuglab.cms.base.AssignmentFileData;
import edu.cornell.csuglab.cms.base.SolutionFileData;
import edu.cornell.csuglab.cms.www.util.EditAssignUtil;

/**
 * @author Jon
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class AssignmentOptions {
	
	// Groups info
	boolean migrateGroups = false;
	long migrationAssignmentID = 0;
	
	/* Required Submission Info */
	Hashtable requiredSubmissions = new Hashtable();
	Hashtable newRequiredSubmissions = new Hashtable();
	Collection removedSubmissions = new ArrayList();
	Collection restoredSubmissions = new ArrayList();
	//Enumeration requiredEnum = null;
	//Enumeration newRequiredEnum = null;
	
	/* SubProblem Info */
	Hashtable subProblems = new Hashtable();
	Hashtable newSubProblems = new Hashtable();
	Collection restoredSubProblems = new ArrayList();
	Collection removedSubProblems = new ArrayList();
	
	/* Assignment File Info
	 * assignType 				The type of this assignment (default to Assignment)
	 * assignmentItems			Holds information about added/edited assignment items
	 * restoredAssignmentFiles	'' '' about assignment files which have been undeleted
	 * restoredAssignmentItems	'' '' about assignment items which have been undeleted
	 * assignmentFilesEnum		Used to cache data for enumerating items
	 * restoredFileEnum			Used to cache data for enumerating restored files
	 * current					The currently visited assignment item
	 * finalAssignmentFiles		Collection of AssignmentFileDatas with fileCounters filled in
	 * finalAssignmentItems		Collection of AssignmentItemDatas with new item names specified
	 * removedItems				Collection of AssignmentItems which have been deleted
	 */
	int assignType = AssignmentBean.ASSIGNMENT;
	Hashtable assignmentItems = new Hashtable();
	Hashtable newAssignmentItems = new Hashtable();
	Hashtable restoredAssignmentFiles = new Hashtable();
	Collection restoredAssignmentItems = new ArrayList();
	//Enumeration replacementFilesEnum = null;
	//AssignmentFileHelper currentReplacement = null;
	HashSet replacedAssignmentItems = new HashSet();
	
	//Enumeration newFilesEnum = null;
	//Enumeration assignmentFilesEnum = null;
	//Enumeration restoredFileEnum = null;
	ArrayList finalAssignmentFiles = new ArrayList();
	ArrayList finalAssignmentItems = new ArrayList();
	Collection removedItems = new ArrayList();
	
	// Solution File Info
	boolean solutionfilechecked = false;
	SolutionFileData finalSolutionFile = null;
	FileItem solutionFile = null;
	boolean hasNewSolutionFile = false; //Used for import
	boolean removeCurrentSolution = false;
	Long restoredSolutionID = null;
	
	public void setAssignmentType(int type) { 
		assignType = type; 
	}
	
	public int getAssignmentType() {
		return assignType;
	}
	
	/* Collision detection to make sure we don't try to replace an assignment
	 * item's file with a new one and restore an old one at the same time */
	//Hashtable replacedAssignmentItems = new Hashtable();
	
	/**
	 * Add a new Item field for an assignment item by assignment item ID
	 * @param name
	 * @param itemID
	 */
	public void addAssignmentItemName(String name, long itemID) {
		if (!name.equals("")) {
			Long key = new Long(itemID);
			if (!assignmentItems.containsKey(key)) {
				AssignmentFileHelper file = new AssignmentFileHelper();
				file.name = name;
				file.isNew = false;
				assignmentItems.put(key, file);
			}
			else {
				AssignmentFileHelper file = (AssignmentFileHelper) assignmentItems.get(key);
				file.name = name;
			}
		}
	}
	
	public void addReplacementAssignmentFile(FileItem data, long itemID) throws FileUploadException {
		Long key = new Long(itemID);
		if (!assignmentItems.containsKey(key)) {
			AssignmentFileHelper file = new AssignmentFileHelper();
			file.data = data;
			file.isNew = false;
			assignmentItems.put(key, file);
		} else {
			AssignmentFileHelper file = (AssignmentFileHelper) assignmentItems.get(key);
			file.data = data;
		}
		if (replacedAssignmentItems.contains(key)) {
			throw new FileUploadException("Error: Conflicting files chosen to replace assignment item.<br>");
		} else {
			replacedAssignmentItems.add(key);
		}
	}
	
	/*public long getNextReplacementAssignmentFileItemID() {
		long result = -1;
		if (replacementFilesEnum == null) {
			replacementFilesEnum = assignmentItems.keys();
		}
		while (replacementFilesEnum.hasMoreElements()) {
			result = ((Long) replacementFilesEnum.nextElement()).longValue();
			AssignmentFileHelper currentReplacement = (AssignmentFileHelper) assignmentItems.get(new Long(result));
			if (currentReplacement.data != null) {
				return result;
			}
		}
		return -1;
	}*/
	
	/*public void resetNextReplacementAssignmentFileEnum() {
		replacementFilesEnum = null;
	}*/
	
	/*public long getNextAssignmentItemID() {
		long result = -1;
		if (assignmentFilesEnum == null) {
			assignmentFilesEnum = assignmentItems.keys();
		}
		if (assignmentFilesEnum.hasMoreElements()) {
			result = ((Long) assignmentFilesEnum.nextElement()).longValue();
		}
		return result;
	}*/
	
	public Collection getAssignmentItemIDs() {
		LinkedList result = new LinkedList();
		Enumeration enumer = assignmentItems.keys();
		while (enumer.hasMoreElements()) {
			result.addFirst(enumer.nextElement());
		}
		return result;
	}
	
	public Collection getReplacedAssignmentItemIDs() {
		LinkedList result = new LinkedList();
		Enumeration enumer = assignmentItems.keys();
		while (enumer.hasMoreElements()) {
			Long key = (Long) enumer.nextElement();
			AssignmentFileHelper current = (AssignmentFileHelper) assignmentItems.get(key);
			if (current.data != null) {
				result.addFirst(key);
			}
		}
		return result;
	}
	
	public Collection getNewAssignmentItemIDs() {
		LinkedList result = new LinkedList();
		Enumeration enumer = newAssignmentItems.keys();
		while (enumer.hasMoreElements()) {
			result.addFirst(enumer.nextElement());
		}
		return result;
	}
	
	public FileItem getFileItemByID(long itemID) {
		AssignmentFileHelper current = (AssignmentFileHelper) assignmentItems.get(new Long(itemID));
		return current.data;
	}
	
	public FileItem getNewFileItemByID(int ID) {
		AssignmentFileHelper current = (AssignmentFileHelper) newAssignmentItems.get(new Integer(ID));
		return current.data;
	}
	
	public String getItemNameByID(long itemID) {
		AssignmentFileHelper current = (AssignmentFileHelper) assignmentItems.get(new Long(itemID));
		return current.name;
	}
	
	public String getNewItemNameByID(int ID) {
		AssignmentFileHelper current = (AssignmentFileHelper) newAssignmentItems.get(new Integer(ID));
		return current.name;
	}
	
	public AssignmentFileData getReplacementFinalFileByID(long itemID) {
		AssignmentFileHelper current = (AssignmentFileHelper) assignmentItems.get(new Long(itemID));
		return current.finalData;		
	}
	
	public void setReplacementFinalFileByID(long itemID, AssignmentFileData finalData) {
		AssignmentFileHelper current = (AssignmentFileHelper) assignmentItems.get(new Long(itemID));
		current.finalData = finalData;
	}
	
	public AssignmentFileData getNewFinalFileByID(int ID) {
		AssignmentFileHelper current = (AssignmentFileHelper) newAssignmentItems.get(new Integer(ID));
		return current.finalData;
	}
	
	public void setNewFinalFileByID(int ID, AssignmentFileData finalData) {
		AssignmentFileHelper current = (AssignmentFileHelper) newAssignmentItems.get(new Integer(ID));
		current.finalData = finalData;
	}
	
	public void restoreAssignmentItem(long itemID) {
		restoredAssignmentItems.add(new Long(itemID));
	}
	
	public Collection getRestoredAssignmentItems() {
		return restoredAssignmentItems;
	}
	
	public void restoreAssignmentFile(long itemID, long fileID) throws FileUploadException {
		Long key = new Long(itemID);
		if (restoredAssignmentFiles.containsKey(key) || replacedAssignmentItems.contains(key)) {
			throw new FileUploadException("Error: Conflicting files chosen to replace assignment item.<br>");
		}
		replacedAssignmentItems.add(key);
		restoredAssignmentFiles.put(key, new Long(fileID));
	}
	
	/*public long getNextRestoredAssignmentItemID() {
		long result = 0;
		if (restoredFileEnum == null) {
			restoredFileEnum = restoredAssignmentFiles.keys();
		}
		if (restoredFileEnum.hasMoreElements()) {
			result = ((Long) restoredFileEnum.nextElement()).longValue();
		} else {
			result = -1;
		}
		return result;
	}
	
	public int getNextNewAssignmentItemID() {
		int result = -1;
		if (newFilesEnum == null) {
			newFilesEnum = newAssignmentItems.keys();
		}
		if (newFilesEnum.hasMoreElements()) {
			result = ((Integer) newFilesEnum.nextElement()).intValue();
		}
		return result;
	}
	
	public void resetNewAssignmentItemEnum() {
		newFilesEnum = null;
	}*/
	
	/**
	 * Returns -1 when no restoration for this item.
	 * @param itemID
	 * @return
	 */
	public long getRestoredFileIDByItemID(long itemID) {
		Long key = new Long(itemID);
		Long result = (Long) restoredAssignmentFiles.get(key);
		if (result == null) {
			return -1;
		}
		return result.longValue();
	}
	
	public void removeAssignmentItem(long itemID) {
		removedItems.add(new Long(itemID));
	}
	
	public Collection getRemovedAssignmentItems() {
		return removedItems;
	}
	
	/**
	 * Add a new Name field for an assignment file using unique number num
	 * which matches a FileItem that will be or has already been added.
	 * @param name The name to use for the assignment file
	 * @param num The unique number to store this name by
	 */
	public void addNewAssignmentItemName(String name, int num) {
		if (!name.equals("")) {
			Integer key = new Integer(num);
			if (!newAssignmentItems.containsKey(key)) {
				AssignmentFileHelper file = new AssignmentFileHelper();
				file.name = (name.equals("")) ? null : name;
				file.isNew = true;
				newAssignmentItems.put(key, file);
			}
			else {
				AssignmentFileHelper file = (AssignmentFileHelper) newAssignmentItems.get(key);
				file.name = name;
			}
		}
	}
	
	/**
	 * Add a new FileItem representing the data for a new assignment file
	 * using unique number num which matches a Name which will be or has
	 * already been added.
	 * @param data The FileItem representing the file
	 * @param num The unique number to store this file by
	 */
	public void addNewAssignmentFile(FileItem data, int num) {
		Integer key = new Integer(num);
		if (!newAssignmentItems.containsKey(key)) {
			AssignmentFileHelper file = new AssignmentFileHelper();
			file.data = data;
			file.isNew = true;
			newAssignmentItems.put(key, file);
		}
		else {
			AssignmentFileHelper file = (AssignmentFileHelper) newAssignmentItems.get(key);
			file.data = data;
		}
	}
/*
	public void resetAssignmentFileEnum() {
		assignmentFilesEnum = assignmentItems.keys();
	}*/
	
	/**
	 * Returns the name of the next assignment file.
	 * @return
	 */
	public String getAssignmentItemNameByID(long itemID) throws FileUploadException {
		AssignmentFileHelper current = (AssignmentFileHelper) assignmentItems.get(new Long(itemID));
		if (current.name == null) {
			throw new FileUploadException("Error: Assignment file name cannot be empty.<br>");
		}
		return current.name;
	}
	
	/**
	 * Returns the FileItem associated with the next assignment file.
	 * @return
	 */
	public FileItem getAssignmentItemFileByID(long itemID) throws FileUploadException {
		AssignmentFileHelper current = (AssignmentFileHelper) assignmentItems.get(new Long(itemID));
		if (current.data == null) {
			throw new FileUploadException("Error: Must upload a file for assignment file '" + current.name + "'.<br>");
		}
		return current.data;
	}

	/**
	 * Adds a new RequiredFile file name to the assignment creation options.
	 * @param fileName The name of the file we're adding
	 * @param num The unique number associating this name with accepted types
	 */
	public void addRequiredFileName(String fileName, long subID) throws FileUploadException {
		Long key = new Long(subID);
		if (fileName.equals("")) {
			throw new FileUploadException("Error: Required Submission must have a name.<br>");
		}
		if (!requiredSubmissions.containsKey(key)) {
			RequiredFileHelper file = new RequiredFileHelper();
			file.fileName = fileName;
			requiredSubmissions.put(key, file);
		}
		else {
			RequiredFileHelper file = (RequiredFileHelper) requiredSubmissions.get(key);
			file.fileName = fileName;
		}
	}
	
	/**
	 * Adds a file type to a previously added RequiredFile.
	 * @param fileType The file type we're adding
	 * @param num The unique number associated with the file name that this
	 * 		file type should be added to
	 */
	public void addRequiredFileTypes(String fileTypes, long subID) throws FileUploadException {
		Long key = new Long(subID);
		// take file type input list, santitize it, and break it up into individual file types
		String[] fta = EditAssignUtil.getFileTypeArray(fileTypes);
		if (!requiredSubmissions.containsKey(key)) {
			RequiredFileHelper file = new RequiredFileHelper();
			for (int i = 0; i < fta.length; i++) {
				String fileType = fta[i];
				file.fileTypes.add(fileType);
			}
			requiredSubmissions.put(key, file);
		} else {
			RequiredFileHelper file = (RequiredFileHelper) requiredSubmissions.get(key);
			for (int i = 0; i < fta.length; i++) {
				String fileType = fta[i];
				file.fileTypes.add(fileType);
			}
		}
	}
	
	/**
	 * Adds a max size to a previously added RequiredFile.
	 * @param maxSize The maximum size for this required file
	 * @param num The unique number associated with the file that this max
	 * 		size value is associated with
	 */
	public void addRequiredFileMaxSize(int maxSize, long subID) {
		Long key = new Long(subID);
		if (!requiredSubmissions.containsKey(key)) {
			RequiredFileHelper file = new RequiredFileHelper();
			file.maxSize = maxSize;
			requiredSubmissions.put(key, file);
		}
		else {
			RequiredFileHelper file = (RequiredFileHelper) requiredSubmissions.get(key);
			file.maxSize = maxSize;
		}
	}
	
	/**
	 * Adds a new required file name to the assignment.
	 * @param name
	 * @param num
	 */
	public void addNewRequiredFileName(String name, int num) {
		if (!name.equals("")) {
			Integer key = new Integer(num);
			if (!newRequiredSubmissions.containsKey(key)) {
				RequiredFileHelper file = new RequiredFileHelper();
				file.fileName = name;
				newRequiredSubmissions.put(key, file);
			} else {
				RequiredFileHelper file = (RequiredFileHelper) newRequiredSubmissions.get(key);
				file.fileName = name;
			}
		}
	}
	
	/**
	 * Adds a new required file type to the assignment.
	 * @param name
	 * @param num
	 */
	public void addNewRequiredFileTypes(String types, int num) {
		Integer key = new Integer(num);
		String[] fta = EditAssignUtil.getFileTypeArray(types);
		if (!newRequiredSubmissions.containsKey(key)) {
			RequiredFileHelper file = new RequiredFileHelper();
			for (int i = 0; i < fta.length; i++) {
				String type = fta[i];
				file.fileTypes.add(type);
			}
			newRequiredSubmissions.put(key, file);
		} else {
			RequiredFileHelper file = (RequiredFileHelper) newRequiredSubmissions.get(key);
			for (int i = 0; i < fta.length; i++) {
				String type = fta[i];
				file.fileTypes.add(type);
			}
		}
	}
	
	/**
	 * Adds a new required max size to a file in the assignment.
	 * @param name
	 * @param num
	 */
	public void addNewRequiredMaxSize(int maxSize, int num) {
		Integer key = new Integer(num);
		if (!newRequiredSubmissions.containsKey(key)) {
			RequiredFileHelper file = new RequiredFileHelper();
			file.maxSize = maxSize;
			newRequiredSubmissions.put(key, file);
		} else {
			RequiredFileHelper file = (RequiredFileHelper) newRequiredSubmissions.get(key);
			file.maxSize = maxSize;
		}
	}
	
	/**
	 * Returns the next submission ID in sequence, returns -1 when no more required
	 * submissions.
	 * @return
	 *
	public long getNextRequiredSubmissionID() {
		long result;
		if (requiredEnum == null) {
			requiredEnum = requiredSubmissions.keys();
		}
		if (requiredEnum.hasMoreElements()) {
			result = ((Long) requiredEnum.nextElement()).longValue();
		} else {
			result = -1;
		}
		return result;
	}*/
	
	/**
	 * Returns the next index value used to store required submissions in sequence,
	 * returns -1 when there are none left.
	 * @return
	 */
	/*
	public int getNextNewRequiredSubmissionID() {
		int result;
		if (newRequiredEnum == null) {
			newRequiredEnum = newRequiredSubmissions.keys();
		}
		if (newRequiredEnum.hasMoreElements()) {
			result = ((Integer) newRequiredEnum.nextElement()).intValue();
			String name = getNewRequiredFileNameByID(result);
			if (name == null) {
				return getNextNewRequiredSubmissionID();
			}
		} else {
			result = -1;
		}
		return result;
	}*/
	
	public Collection getRequiredSubmissionIDs() {
		LinkedList result = new LinkedList();
		Enumeration enumer = requiredSubmissions.keys();
		while (enumer.hasMoreElements()) {
			result.addFirst(enumer.nextElement());
		}
		return result;
	}
	
	public Collection getNewRequiredSubmissionIDs() {
		LinkedList result = new LinkedList();
		Enumeration enumer = newRequiredSubmissions.keys();
		while (enumer.hasMoreElements()) {
			result.addFirst(enumer.nextElement());
		}
		return result;
	}
	
	public String getRequiredFileNameByID(long subID) {
		Long key = new Long(subID);
		RequiredFileHelper file = (RequiredFileHelper) requiredSubmissions.get(key);
		return file.fileName;
	}
	
	public Collection getRequiredFileTypesByID(long subID) {
		Long key = new Long(subID);
		RequiredFileHelper file = (RequiredFileHelper) requiredSubmissions.get(key);
		return file.fileTypes;
	}
	
	public int getRequiredMaxSizeByID(long subID) {
		Long key = new Long(subID);
		RequiredFileHelper file = (RequiredFileHelper) requiredSubmissions.get(key);
		return file.maxSize;
	}
	
	public String getNewRequiredFileNameByID(int ID) {
		Integer key = new Integer(ID);
		RequiredFileHelper file = (RequiredFileHelper) newRequiredSubmissions.get(key);
		return file.fileName;
	}
	
	public Collection getNewRequiredFileTypesByID(int ID) {
		Integer key = new Integer(ID);
		RequiredFileHelper file = (RequiredFileHelper) newRequiredSubmissions.get(key);
		return file.fileTypes;
	}
	
	public int getNewRequiredMaxSizeByID(int ID) {
		Integer key = new Integer(ID);
		RequiredFileHelper file = (RequiredFileHelper) newRequiredSubmissions.get(key);
		return file.maxSize;
	}
	
	/**
	 * Adds the name of a RequiredFile to be removed from this assignment
	 * @param filename Name of RequiredFile to remove
	 */
	public void removeSubmission(long submissionID) {
		removedSubmissions.add(new Long(submissionID));
	}
	
	/**
	 * @return Collection of submission ids (Longs) to remove
	 */
	public Collection getRemovedSubmissions() {
		return removedSubmissions;
	}
	
	public void restoreSubmission(long submissionID) {
		restoredSubmissions.add(new Long(submissionID));
	}
	
	public Collection getRestoredSubmissions() {
		return restoredSubmissions;
	}
	
	/**
	 * Adds a new solution file to this assignment
	 * @param solutionFile
	 */
	public void addSolutionFile(FileItem solutionFile) throws FileUploadException {
		if (this.solutionFile != null) {
			throw new FileUploadException("Error: Conflicting solution files (cannot both restore and replace solution files)<br>");
		}
		this.solutionFile = solutionFile;
	}
	
	/**
	 * Adds a new imported solution file to this assignment
	 * Had to get around using FileItem objects like the addSolutionFile function requires
	 * @param solutionFile
	 */
	public void addImportedSolutionFile(SolutionFileData solutionFile)
	{
		this.finalSolutionFile = solutionFile;
		this.hasNewSolutionFile = true;
	}
	
	
	
	/**
	 * Returns true iff a solution file has been uploaded
	 * @return
	 */
	public boolean hasUncheckedSolutionFile() {
		if (solutionFile != null && !solutionfilechecked) {
			return true;
		}
		return false;
	}
	
	public boolean hasSolutionFile() {
		if (solutionFile != null || hasNewSolutionFile) {
			return true;
		}
		return false;
	}
	
	/**
	 * Returns the file newly uploaded as a solution to this assignment
	 * @return
	 */
	public FileItem getSolutionFile() {
		solutionfilechecked = true;
		return solutionFile;
	}
	
	public void removeCurrentSolutionFile() {
		removeCurrentSolution = true;
	}
	
	public boolean isSolutionRemoved() {
		return removeCurrentSolution;
	}
	
	public void restoreSolutionFile(long solutionFileID) throws FileUploadException {
		if (restoredSolutionID != null) {
			throw new FileUploadException("Error: Conflicting solution files (cannot restore more than one solution file)<br>");
		}
		restoredSolutionID = new Long(solutionFileID);
	}
	
	public boolean hasRestoredSolution() {
		if (restoredSolutionID != null) {
			return true;
		}
		return false;
	}
	
	public long getRestoredSolutionID() {
		return restoredSolutionID.longValue();
	}
	
	public void addFinalSolutionFile(SolutionFileData solutionFile) {
		this.finalSolutionFile = solutionFile;
	}
	
	public SolutionFileData getFinalSolutionFile() {
		return finalSolutionFile;
	}
	
	public void setGroupMigration(long assignmentID) {
		migrateGroups = true;
		migrationAssignmentID = assignmentID;
	}
	
	public void unsetGroupMigration() {
		migrateGroups = false;
		migrationAssignmentID = 0;
	}
	
	public boolean migrateGroups() {
		return migrateGroups;
	}
	
	public float getMaxScore() {
		float maxScore = 0.0f;
		
		// adding scores from existing problems
		Iterator keys = subProblems.keySet().iterator();
		while (keys.hasNext()) {
			Object key = keys.next();
			if ( !removedSubProblems.contains(key) ) {
				SubProblemHelper subproblem = (SubProblemHelper) subProblems.get(key);
				maxScore += subproblem.maxScore;
			}
		}
		
		// adding scores from new problems
		keys = newSubProblems.keySet().iterator();
		while (keys.hasNext()) {
			// don't have to check if this subproblem is in the removed list since it's new
			SubProblemHelper subproblem = (SubProblemHelper) newSubProblems.get(keys.next());
			maxScore += subproblem.maxScore;
		}
		return maxScore;
	}
	
	public long getMigrationAssignmentID() {
		return migrationAssignmentID;
	}
	
	public void addSubProblemName(String subProblemName, long subProblemID) {
		Long key = new Long(subProblemID);
		if (subProblems.containsKey(key)) {
			SubProblemHelper prob = (SubProblemHelper) subProblems.get(key);
			prob.subProblemName = subProblemName;
		}
		else {
			SubProblemHelper prob = new SubProblemHelper();
			prob.subProblemName = subProblemName;
			subProblems.put(key, prob);
		}
	}
	
	public void addNewSubProblemName(String subProblemName, int ID) {
		Integer key = new Integer(ID);
		if (newSubProblems.containsKey(key)) {
			SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(key);
			prob.subProblemName = subProblemName;
		}
		else {
			SubProblemHelper prob = new SubProblemHelper();
			prob.subProblemName = subProblemName;
			newSubProblems.put(key, prob);
		}
	}
	
	public void addSubProblemScore(float maxScore, long subProblemID) {
		Long key = new Long(subProblemID);
		if (subProblems.containsKey(key)) {
			SubProblemHelper prob = (SubProblemHelper) subProblems.get(key);
			prob.maxScore = maxScore;
		}
		else {
			SubProblemHelper prob = new SubProblemHelper();
			prob.maxScore = maxScore;
			subProblems.put(key, prob);
		}
	}
	
	public void addNewSubProblemScore(float maxScore, int ID) {
		Integer key = new Integer(ID);
		if (newSubProblems.containsKey(key)) {
			SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(key);
			prob.maxScore = maxScore;
		}
		else {
			SubProblemHelper prob = new SubProblemHelper();
			prob.maxScore = maxScore;
			newSubProblems.put(key, prob);
		}
	}
	
	public void addSubProblemType(int type, long subProblemID) {
		Long key = new Long(subProblemID);
		if (subProblems.containsKey(key)) {
			SubProblemHelper prob = (SubProblemHelper) subProblems.get(key);
			prob.type = type;
		}
		else {
			SubProblemHelper prob = new SubProblemHelper();
			prob.type = type;
			subProblems.put(key, prob);
		}
	}
	
	public void addNewSubProblemType(int type, int ID) {
		Integer key = new Integer(ID);
		if (newSubProblems.containsKey(key)) {
			SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(key);
			prob.type = type;
		}
		else {
			SubProblemHelper prob = new SubProblemHelper();
			prob.type = type;
			newSubProblems.put(key, prob);
		}
	}
	
	public void addSubProblemOrder(int order, long subProblemID) {
		Long key = new Long(subProblemID);
		if (subProblems.containsKey(key)) {
			SubProblemHelper prob = (SubProblemHelper) subProblems.get(key);
			prob.order = order;
		}
		else {
			SubProblemHelper prob = new SubProblemHelper();
			prob.order = order;
			subProblems.put(key, prob);
		}
	}
	
	public void addNewSubProblemOrder(int order, int ID) {
		Integer key = new Integer(ID);
		if (newSubProblems.containsKey(key)) {
			SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(key);
			prob.order = order;
		}
		else {
			SubProblemHelper prob = new SubProblemHelper();
			prob.order = order;
			newSubProblems.put(key, prob);
		}
	}
 	
	public void addSubProblemAnswer(int answer, long subProblemID) {
		Long key = new Long(subProblemID);
		if (subProblems.containsKey(key)) {
			SubProblemHelper prob = (SubProblemHelper) subProblems.get(key);
			prob.answer = answer;
		}
		else {
			SubProblemHelper prob = new SubProblemHelper();
			prob.answer = answer;
			subProblems.put(key, prob);
		}
	}
	
	public void addNewSubProblemAnswer(int answer, int ID) {
		Integer key = new Integer(ID);
		if (newSubProblems.containsKey(key)) {
			SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(key);
			prob.answer = answer;
		}
		else {
			SubProblemHelper prob = new SubProblemHelper();
			prob.answer = answer;
			newSubProblems.put(key, prob);
		}
	}
	
	public void addSubProblemChoices(SubProblemOptions choices, long subProblemID) {
		Long key = new Long(subProblemID);
		if (subProblems.containsKey(key)) {
			SubProblemHelper prob = (SubProblemHelper) subProblems.get(key);
			prob.choices = choices;
		}
		else {
			SubProblemHelper prob = new SubProblemHelper();
			prob.choices = choices;
			subProblems.put(key, prob);
		}
	}
	
	public void addNewSubProblemChoices(SubProblemOptions choices, int ID) {
		Integer key = new Integer(ID);
		if (newSubProblems.containsKey(key)) {
			SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(key);
			prob.choices = choices;
		}
		else {
			SubProblemHelper prob = new SubProblemHelper();
			prob.choices = choices;
			newSubProblems.put(key, prob);
		}
	}
	
	public void removeSubProblem(long subProblemID) {
		removedSubProblems.add(new Long(subProblemID));
	}
	
	public void restoreSubProblem(long subProblemID) {
		restoredSubProblems.add(new Long(subProblemID));
	}
	
	/*
	 * Returns a collection of Longs representing the SubProblemIDs of the
	 * subproblems from this assignment.
	 */
	public Collection getSubProblemIDs() {
		ArrayList result = new ArrayList();
		Enumeration keys = subProblems.keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			SubProblemHelper prob = (SubProblemHelper) subProblems.get(key);
			if (prob.maxScore >= 0.0f && !prob.subProblemName.equals("")) {
				result.add(key);
			}
		}
		return result;
	}
	
	/*
	 * Returns a collection of Integers which represents the IDs used
	 * to store new SubProblems which need to be added to the assignment.
	 */
	public Collection getNewSubProblemIDs() {
		LinkedList result = new LinkedList();
		Enumeration keys = newSubProblems.keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(key);
			if (prob.maxScore >= 0.0f && !prob.subProblemName.equals("")) {
				result.addFirst(key);
			}
		}
		return result;
	}
	
	public String getSubProblemNameByID(long subProblemID) {
		SubProblemHelper prob = (SubProblemHelper) subProblems.get(new Long(subProblemID));
		return prob.subProblemName;
	}
	
	public float getSubProblemScoreByID(long subProblemID) {
		SubProblemHelper prob = (SubProblemHelper) subProblems.get(new Long(subProblemID));
		return prob.maxScore;
	}
	
	public int getSubProblemTypeByID(long subProblemID) {
		SubProblemHelper prob = (SubProblemHelper) subProblems.get(new Long(subProblemID));
		return prob.type;
	}
	
	public int getSubProblemOrderByID(long subProblemID) {
		SubProblemHelper prob = (SubProblemHelper) subProblems.get(new Long(subProblemID));
		return prob.order;
	}
	
	public int getSubProblemAnswerByID(long subProblemID) {
		SubProblemHelper prob = (SubProblemHelper) subProblems.get(new Long(subProblemID));
		return prob.answer;
	}
	
	public SubProblemOptions getSubProblemChoicesByID(long subProblemID) {
		SubProblemHelper prob = (SubProblemHelper) subProblems.get(new Long(subProblemID));
		return prob.choices;
	}
	
	public String getNewSubProblemNameByID(int ID) {
		SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(new Integer(ID));
		return prob.subProblemName;
	}
	
	public float getNewSubProblemScoreByID(int ID) {
		SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(new Integer(ID));
		return prob.maxScore;
	}
	
	public int getNewSubProblemTypeByID(int ID) {
		SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(new Integer(ID));
		return prob.type;
	}
	
	public int getNewSubProblemOrderByID(int ID) {
		SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(new Integer(ID));
		return prob.order;
	}	
	
	public int getNewSubProblemAnswerByID(int ID) {
		SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(new Integer(ID));
		return prob.answer;
	}
	
	public SubProblemOptions getNewSubProblemChoicesByID(int ID) {
		SubProblemHelper prob = (SubProblemHelper) newSubProblems.get(new Integer(ID));
		return prob.choices;
	}
		
	public Collection getRemovedSubProblems() {
		return removedSubProblems;
	}
	
	public Collection getRestoredSubProblems() {
		return restoredSubProblems;
	}
	
}

class RequiredFileHelper {
	String fileName;
	ArrayList fileTypes = new ArrayList();
	int maxSize;
}

class SubProblemHelper {
	String subProblemName;
	float maxScore;
	int type;
	int order;
	int answer;
	SubProblemOptions choices;
}

class AssignmentFileHelper {
	String name = null;
	FileItem data = null;
	AssignmentFileData finalData;
	boolean isNew = false;
}
