package edu.cornell.csuglab.cms.util;

/**
 * @author rd94
 */
public class NoSuchNetIDException extends Exception {
    public NoSuchNetIDException(String message) {super(message);}
}
