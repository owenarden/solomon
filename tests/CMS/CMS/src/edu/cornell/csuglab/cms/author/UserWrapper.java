/*
 * Created on Apr 10, 2005
 */
package edu.cornell.csuglab.cms.author;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import edu.cornell.csuglab.cms.base.*;

/**
 * @author yc263
 *
 *Wrapper Class for a user of CMS
 *The return values for each of the functions for an object that extends UserWrapper
 *are defined by default in this class. It is only different if the class actually overrides
 *the method 
 */
public class UserWrapper {
	public static final int 
	UT_DEFAULT = -1,
	UT_GUEST = 0,  //not authenticated user
	UT_AUTHENTICATED = 1, //authenticated user
	UT_STAFF_AS_STUDENT = 2, //staff as student, where the student is encapsulated by the user bean
	UT_STAFF_AS_CORNELLMEM = 3, //staff as a general cornell member 
	UT_STAFF_AS_GUEST = 4; // staff as an nonauthenticated user 


protected UserLocal user;
protected long courseID;

public UserWrapper(){
	this.user = null;
	this.courseID = 0;
}

public UserWrapper(long courseID){
	this.courseID = courseID;
	this.user = null;
}

public UserWrapper(UserLocal user){
	this.user = user;
	this.courseID = 0;
}

public UserWrapper(long courseID, UserLocal user){
	this.courseID = courseID;
	this.user = user;
}

public long getCourseID(){
	return this.courseID;
}

public int getUserType(){
	return UT_DEFAULT;
}

public int getAuthoriznLevelByCourseID(long courseID){
	return Principal.AUTHOR_GUEST;
}

public String getUserID(){
	return "";
}

public String getCUID(){
	return null;
}

public String getFirstName() {
	return null;
}

public String getLastName() {
	return null;
}

public String getCollege() {
	return null;
}

public String getDepartment() {
	return null;
}

public boolean isPWExpired() {
	return false;
}

public boolean isDeactivated() {
	return false;
}

public UserData getUserData() {
	return null;	
} 

public Collection getAllEvents() {
	return new ArrayList();
}

public void resetPassword() {}

public boolean  isStudentInCourseByCourseID(long courseID){
	return false;
}

public boolean isStaffInCourseByCourseID(long courseID){
	return false;

}

public boolean isAdminPrivByCourseID(long courseID){
	return false;
}

public boolean isAssignPrivByCourseID(long courseID){
	return false;
}
public boolean isCategoryPrivByCourseID(long courseID){
	return false;
}
public boolean isGradesPrivByCourseID(long courseID){
	return false;
}
public boolean isGroupsPrivByCourseID(long courseID){
	return false;
}
public boolean isCMSAdmin(){
	return false;
}

public boolean isCMSSubAdmin(){
	return false;
}

public int getCMSSubAdminDomain(){
	return -1;
}

/**
 * Returns the student courses that user is in
 * @return
 * @throws RemoteException
 */
public Collection getStudentCourses() {
	return new ArrayList();
}

/**
 * Returns the staff courses that user is in
 * @return
 * @throws RemoteException
 */
public Collection getStaffCourses() {
	return new ArrayList();
}


/**
 * Returns all a user's due assignments
 * @return Collection of AssignmentData objects
 */
public Collection getDueAssignments() {
	return new ArrayList();
}

/**
 * Returns announcements from the past week from any of the user's courses
 * @return Collection of AnnouncementData objects
 */
public Collection getRecentAnnouncements() {
	return new ArrayList();
} 

/**
 * Returns a collection of groups that have invited this user to join for the given assignment.
 * @param assignmentID The ID of the assignment we're looking for	 
 * @return Collection of GroupDatas
 */
public Collection getInvitedGroupsByAssignment(long assignmentID) {
	return new ArrayList();
}

/**
 * Returns the user's grades for assignments under this course
 * @return
 * @throws RemoteException
 */
public HashMap getAssignmentGrades(long courseID){
	return new HashMap();
}

/**
 * Returns the user's grades for subproblems under specified assignment
 * @param assignID
 * @return
 * @throws RemoteException
 */
public HashMap getSubProblemGrades(long assignID) {
	return new HashMap();
}

/**
 * Returns the user's comments for specified group
 * @param groupID
 * @return
 * @throws RemoteException
 */
public Collection getAssignmentCommentsByGroupID(long groupID){
	return new ArrayList();
}

/**
 * Return's the user's comment files for speciied group
 * @param groupID
 * @return
 * @throws RemoteException
 */
public HashMap getCommentFilesByGroupID(long groupID){
	return new HashMap();
}

/**
 * Return's the user's group's regrade requests
 * @param groupID
 * @return
 * @throws RemoteException
 */
public Collection getRegradeRequestsByGroupID(long groupID){
	return new ArrayList();
}
}