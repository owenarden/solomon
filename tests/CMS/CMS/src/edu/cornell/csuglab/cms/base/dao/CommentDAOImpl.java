/*
 * Created on Mar 31, 2004
 */

package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.CommentBean;
import edu.cornell.csuglab.cms.base.CommentDAO;
import edu.cornell.csuglab.cms.base.CommentPK;

/**
 * @author Theodore Chao
 */
public class CommentDAOImpl extends DAOMaster implements CommentDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.CommentDAO#load(edu.cornell.csuglab.cms.base.CommentPK,
	 *      edu.cornell.csuglab.cms.base.CommentBean)
	 */
	public void load(CommentPK pk, CommentBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tComments where CommentID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getCommentID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setCommentID(rs.getLong("CommentID"));
				ejb.setGroupID(rs.getLong("GroupID"));
				try {
					ejb.setComment(rs.getString("Comment"));
				} catch (SQLException e) {
					ejb.setComment("");
				}
				ejb.setNetID(rs.getString("NetID"));
				ejb.setDateEntered(rs.getTimestamp("DateEntered"));
				ejb.setHidden(rs.getBoolean("Hidden"));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null)
					conn.close();
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception f) {
			}
			throw new EJBException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.CommentDAO#store(edu.cornell.csuglab.cms.base.CommentBean)
	 */
	public void store(CommentBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "update tComments set Comment = ?, NetID = ?, DateEntered = ?, Hidden = ? "
					+ "where CommentID = ?";
			ps = conn.prepareStatement(update);
			ps.setString(1, (ejb.getComment().length() == 0) ? " " : ejb
					.getComment());
			ps.setString(2, ejb.getNetID());
			ps.setTimestamp(3, ejb.getDateEntered());
			ps.setBoolean(4, ejb.getHidden());
			ps.setLong(5, ejb.getCommentID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
			} catch (Exception f) {
			}
			throw new EJBException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.CommentDAO#remove(edu.cornell.csuglab.cms.base.CommentPK)
	 */
	public void remove(CommentPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.CommentDAO#create(edu.cornell.csuglab.cms.base.CommentBean)
	 */
	public CommentPK create(CommentBean ejb) throws CreateException,
			EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		CommentPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "insert into tComments "
					+ "(comment, groupid," + "netid, dateentered) values "
					+ "(?, ?, ?, ?)";
			String getKey = "select @@identity as 'CommentID' from tComments";
			ps = conn.prepareStatement(createString);
			ps.setString(1, (ejb.getComment().length() == 0) ? " " : ejb
					.getComment());
			ps.setLong(2, ejb.getGroupID());
			ps.setString(3, ejb.getNetID());
			ps.setTimestamp(4, ejb.getDateEntered());
			ps.executeUpdate();
			rs = conn.prepareStatement(getKey).executeQuery();
			if (rs.next()) {
				result = new CommentPK(rs.getLong(1));
				ejb.setCommentID(result.getCommentID());
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (Exception f) {
			}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.CommentDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.CommentPK)
	 */
	public CommentPK findByPrimaryKey(CommentPK key) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select CommentID from tcomments where CommentID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, key.getCommentID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find comment with ID = "
						+ key.getCommentID());
			}
			conn.close();
			rs.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (Exception f) {
			}
			throw new FinderException(e.getMessage());
		}
		return key;
	}

	public Collection findByGroupIDs(Collection groupids)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT CommentID FROM tComments WHERE Hidden = ? AND (";
			for (int i = 0; i < groupids.size() - 1; i++) {
				query += "GroupID = ? OR ";
			}
			query += "GroupID = ?) ORDER BY DateEntered DESC";
			ps = conn.prepareStatement(query);
			ps.setBoolean(1, false);
			int c = 2;
			for (Iterator i = groupids.iterator(); i.hasNext();) {
				ps.setLong(c++, ((Long) i.next()).longValue());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new CommentPK(rs.getLong("CommentID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (Exception f) {
			}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.CommentDAO#findByGroupID(long)
	 */
	public Collection findByGroupID(long groupID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT * FROM tComments WHERE groupID = ? and Hidden = ? "
					+ "ORDER BY DateEntered DESC";
			ps = conn.prepareStatement(query);
			ps.setLong(1, groupID);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				CommentPK ck = new CommentPK(rs.getLong("commentID"));
				result.add(ck);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (Exception f) {
			}
			e.printStackTrace();
			throw new FinderException(e.getMessage());
		}
		return result;
	}
}
