/*
 * Created on Mar 15, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

/**
 *
 * <!-- begin-user-doc --> You can insert your documentation for '<em><b>GroupAssignedToBean</b></em>'. <!-- end-user-doc --> *
 <!--  begin-lomboz-definition -->
 <?xml version="1.0" encoding="UTF-8"?>
 <lomboz:EJB xmlns:j2ee="http://java.sun.com/xml/ns/j2ee" xmlns:lomboz="http://lomboz.objectlearn.com/xml/lomboz">
 <lomboz:entity>
 <lomboz:entityEjb>
 <j2ee:display-name>GroupAssignedTo</j2ee:display-name>
 <j2ee:ejb-name>GroupAssignedTo</j2ee:ejb-name>
 <j2ee:ejb-class>edu.cornell.csuglab.cms.base.GroupAssignedToBean</j2ee:ejb-class>
 <j2ee:persistence-type>Bean</j2ee:persistence-type>
 <j2ee:cmp-version>2.x</j2ee:cmp-version>
 <j2ee:abstract-schema-name>mySchema</j2ee:abstract-schema-name>
 </lomboz:entityEjb>
 <lomboz:tableName></lomboz:tableName>
 <lomboz:dataSourceName></lomboz:dataSourceName>
 </lomboz:entity>
 </lomboz:EJB>
 <!--  end-lomboz-definition -->
 *
 * <!-- begin-xdoclet-definition -->



 /**
 * @ejb.bean name="GroupAssignedTo"
 *	jndi-name="GroupAssignedTo"
 *	type="BMP"
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.GroupAssignedToDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.GroupAssignedToDAOImpl"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 * <!-- end-xdoclet-defintion -->
 * @generated
 **/
public abstract class GroupAssignedToBean implements javax.ejb.EntityBean {

    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long groupID;
	private long subProblemID;
	private String netID;
	
	public static String UNASSIGNED = "<unassigned>";
	public static String ALLPARTS = "<All Parts>";
	
	/**
	 * @return Returns the groupID.
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getGroupID() {
		return groupID;
	}
	
	/**
	 * @param groupID The groupID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}
	
	/**
	 * @return Returns the netID.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getNetID() {
		return netID;
	}
	
	/**
	 * @param netID The netID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setNetID(String netID) {
		this.netID = netID;
	}
	
	/**
	 * @return Returns the subProblemID.
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getSubProblemID() {
		return subProblemID;
	}
	
	/**
	 * @param subProblemID The subProblemID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setSubProblemID(long subProblemID) {
		this.subProblemID = subProblemID;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public GroupAssignedToData getGroupAssignedToData() {
		return new GroupAssignedToData(getGroupID(), getNetID(), getSubProblemID());
	}
	
	public GroupAssignedToPK ejbFindByPrimaryKey(GroupAssignedToPK pk) throws FinderException {
		return null;
	}
	
	public Collection ejbFindByNetIDAssignmentID(String netid, long assignmentID) throws FinderException {
		return null;
	}
	
	public Collection ejbFindByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	public Collection ejbFindByAssignIDSubProbID(long assignID, long subProbID) throws FinderException {
	    return null;
	}
	
	public Collection ejbFindByGroupID(long groupID) throws FinderException {
		return null;
	}
	
	public Collection ejbFindBySubProblemID(long subProblemID) throws FinderException {
		return null;
	}

	/**
	 * Finds all the GroupAssignedTos of the groups in the collection for a given SubProblem.
	 * For each (groupID, subProblemID) pair where no GroupAssignedTo exists with such a
	 * primary key in the database, the group id is added to the collection nonExisting (as a Long)
	 * @param groupIDs A collection of GroupIDs to look for (Longs)
	 * @param subProblemID The SubProblemID of the SubProblem to get grader assignments for
	 * @param nonExisting A collection to which, for any groups which do not have an assignment
	 *  entry in the database, the group's GroupID will be added (Long)
	 * @return
	 */
	public Collection ejbFindByGroupIDsSubID(Collection groupIDs, long subProblemID, Collection nonExisting) throws FinderException {
		return null;
	}
	
	/**
	 * @param assignmentID
	 * @param courseID
	 * @param groupID
	 * @param netID
	 * @param subProblemID
	 * @return
	 * @throws CreateException
	 * @ejb.create-method view-type="local"
	 */
	public GroupAssignedToPK ejbCreate(long groupID, String netID, long subProblemID) throws CreateException {
		setGroupID(groupID);
		setNetID(netID);
		setSubProblemID(subProblemID);
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * The container invokes this method immediately after it calls ejbCreate.
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void ejbPostCreate() throws javax.ejb.CreateException {
	    // begin-user-code
		// end-user-code
	}

}
