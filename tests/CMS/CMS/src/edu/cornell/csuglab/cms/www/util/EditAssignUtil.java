package edu.cornell.csuglab.cms.www.util;

import java.util.List;
import java.util.ArrayList;

public class EditAssignUtil {
	
	// the category and file type for accepting any file type
	public static String FTC_ANY = "Accept any";
	public static String FT_ANY = "any";
	
	// the category for custom file types
	public static String FTC_CUSTOM = "Custom...";
	
	// list which holds file types categories
	public static final List ftcs = new ArrayList();
	
	static {
		ftcs.add(new FileTypeCategory(FTC_ANY, FT_ANY));
		ftcs.add(new FileTypeCategory("Plain text", "txt"));
		ftcs.add(new FileTypeCategory("Text documents", "txt, doc, rtf"));
		ftcs.add(new FileTypeCategory("Image files", "bmp, gif, jpg, jpeg, png"));
		ftcs.add(new FileTypeCategory("JPG images", "jpg, jpeg"));
		ftcs.add(new FileTypeCategory("Adobe PDF", "pdf"));
		ftcs.add(new FileTypeCategory("ZIP archive", "zip"));
		ftcs.add(new FileTypeCategory("Unix archives", "zip, tar.gz, tgz"));
		ftcs.add(new FileTypeCategory("C++ source file", "cpp"));
		ftcs.add(new FileTypeCategory("C++ header file", "h"));
		ftcs.add(new FileTypeCategory("Java source file", "java"));
		ftcs.add(new FileTypeCategory("Matlab script", "m"));
		ftcs.add(new FileTypeCategory("Word documents", "doc, docx"));
		ftcs.add(new FileTypeCategory("Excel spreadsheets", "xls, xlsx"));
		ftcs.add(new FileTypeCategory("Visio drawing", "vsd"));
	}

	/* Returns a list of the file types in the file type category */
	public static String getFileTypesForCategory(String fileTypeCategory) {
		for (int i = 0; i < ftcs.size(); i++) {
			FileTypeCategory ftc = (FileTypeCategory)(ftcs.get(i));
			if (ftc.getCategory().equals(fileTypeCategory)) {
				return ftc.getFileTypes();
			}
		}
		return null;
	}
	
	
	/* Returns the category for the specified comma-separated file type list */
	public static String findFileTypeCategory(String fileTypes) {
		List ftl = sanitizeFileTypes(fileTypes);
		for (int i = 0; i < ftcs.size(); i++) {
			FileTypeCategory ftc = (FileTypeCategory)(ftcs.get(i));
			if (ftc.matchesFileTypes(ftl)) {
				return ftc.getCategory();
			}
		}
		return FTC_CUSTOM;
	}
	
	/* Takes a string list of file types, sanitizes it, and puts it back in a string */
	public static String cleanFileTypes(String fileTypeList) {
		List ftl = sanitizeFileTypes(fileTypeList);
		String cleaned = "";
		for (int i = 0; i < ftl.size(); i++) {
			if (i > 0) {
				cleaned += ", ";
			}
			cleaned += (String)(ftl.get(i));
		}
		return cleaned;
	}
	
	/* Takes a string list of file types and returns a sanitized list */
	public static List getFileTypeList(String fileTypeList) {
		return sanitizeFileTypes(fileTypeList);
	}
	
	/* Takes a string list of file types and returns a sanitized array */
	public static String[] getFileTypeArray(String fileTypeList) {
		List ftl = sanitizeFileTypes(fileTypeList);
		String[] fta = new String[ftl.size()];
		for (int i = 0; i < ftl.size(); i++) {
			fta[i] = (String)(ftl.get(i));
		}
		return fta;
	}
	
	/*
	 * Takes the file type list and returns an array of String objects corresponding to the
	 * file types in the list. See comments below for algorithm for interpreting input strings.
	 * 
	 * Test: getFileTypes("*.txt,*.txt, pdf, *.rtf., .tar..gz zip")
	 */
	private static List sanitizeFileTypes(String fileTypeList) {
		// Split on anything that is not a letter, number, or period
		String[] fts = fileTypeList.split("[^a-zA-Z0-9\\.]");
		java.util.List ftl = new java.util.ArrayList();
		for (int i = 0; i < fts.length; i++) {
			// Only include elements that are not empty
			if (fts[i].length() != 0) {
				// Convert the file type to lower case and trim it
				String ft = fts[i].toLowerCase().trim();
				// Remove any leading or trailing periods
				if (ft.startsWith(".")) {
					ft = ft.substring(1);
				}
				if (ft.endsWith(".")) {
					ft = ft.substring(0, ft.length() - 1);
				}
				// Replace any consecutive duplications of periods with a single period
				ft = ft.replaceAll("\\.{2,}",".");
				// Make sure there are no duplicates
				boolean dupes = false;
				for (int j = 0; j < ftl.size(); j++) {
					String s = (String) ftl.get(j);
					if (ft.equals(s)) {
						dupes = true;
					}
				}
				if (!dupes) {
					ftl.add(ft);
				}
			}
		}
		return ftl;
	}
	
}
