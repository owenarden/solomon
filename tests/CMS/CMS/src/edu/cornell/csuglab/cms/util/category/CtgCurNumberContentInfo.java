package edu.cornell.csuglab.cms.util.category;

/**
 * @author evan
 * 11 / 4 / 05
 *
 * Holds info representing an existing info of type number, to be edited
 */
public class CtgCurNumberContentInfo extends CtgCurContentInfo
{
	private Long number;
	
	public CtgCurNumberContentInfo(long contentID, Long number)
	{
		super(contentID);
		this.number = number;
	}
	
	public Long getNumber()
	{
		return this.number;
	}
	
	public void setNumber(Long number)
	{
		this.number = number;
	}
	
	public String toString()
	{
		return "CurNumberContentInfo{#: " + getNumber() + "}";
	}
}
