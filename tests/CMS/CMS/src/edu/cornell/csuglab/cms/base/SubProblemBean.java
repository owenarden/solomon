/*
 * Created on Jan 31, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.FinderException;

 /**
 * @ejb.bean name="SubProblem"
 *	jndi-name="SubProblem"
 *	type="BMP"
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.SubProblemDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.SubProblemDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 **/
public abstract class SubProblemBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long subProblemID;
	private long assignmentID;
	private String subProblemName;
	private float maxScore;
	private boolean hidden;
	private int type;
	private int order;
	private int answer;
	
	public static int MULTIPLE_CHOICE = 0;
	public static int FILL_IN = 1;
	public static int SHORT_ANSWER = 2;
	
//	private AnswerLocalHome answerHome= null;
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the subassignment ID
	 */
	public long getSubProblemID() {
		return subProblemID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param subassignmentID The subassignment ID to assign to this
	 */
	public void setSubProblemID(long subProblemID) { 
		this.subProblemID = subProblemID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the parent assignment ID of this subassignment
	 */
	public long getAssignmentID() {
		return assignmentID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param assignmentID The parent assignmentID to set
	 */
	public void setAssignmentID(long assignmentID) {
		this.assignmentID = assignmentID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the name of this subassignment
	 */
	public String getSubProblemName() {
		return subProblemName;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param name The name to give to this subassignment
	 */
	public void setSubProblemName(String subProblemName) {
		this.subProblemName = subProblemName;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Return the max score for this subassignment
	 */
	public float getMaxScore() {
		return maxScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param maxScore The max score to set
	 */
	public void setMaxScore(float maxScore) {
		this.maxScore = maxScore;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Return the type of this quiz question
	 */
	public int getType() {
		return type;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param type The type of the quiz question
	 */
	public void setType(int type) {
		this.type = type;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Return the order of this quiz question
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param order The order of the quiz question
	 */
	public void setOrder(int order) {
		this.order = order;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Return the type of this quiz question
	 */
	public int getAnswer() {
		return answer;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param type The type of the subproblem
	 */
	public void setAnswer(int answer) {
		this.answer = answer;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the status of the subassignment; 0 = OPEN, 1 = HIDDEN
	 */
	public boolean getHidden() {
		return hidden;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param status The status to set this subassignment to; 
	 * 1 = OPEN, 0 = HIDDEN
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	/*
	private AnswerLocalHome answerHome() {
		try {
			if (answerHome == null) {
				answerHome = AnswerUtil.getLocalHome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return answerHome;
	}
	*/
	
	/**
	 * 
	 * @param pk
	 * @return
	 */
	public SubProblemPK ejbFindByPrimaryKey(SubProblemPK pk) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all non-hidden SubProblems in a given assignment
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all non-hidden SubProblems in a group of assignments
	 * @param assignmentIDs
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByAssignmentIDs(long[] assignmentIDs) throws FinderException {
	    return null;
	}
	
	/**
	 * 
	 * @return Returns all the Answers for this subproblem which are not hidden
	 * @ejb.interface-method view-type="local"
	 */
	/*
	public Collection getChoices() {
		Collection c;
		Collection result = new ArrayList();
		try {
			c = choiceHome().findBySubProblemID(getSubProblemID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				result.add(((AnswerLocal) i.next()).getAnswerData());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	*/
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public SubProblemData getSubProblemData() {
		return new SubProblemData(getSubProblemID(), getAssignmentID(), 
				getSubProblemName(), getMaxScore(), getType(), getOrder(),
				getAnswer(), getHidden());
	}

	/**
	 *
	 * @ejb.create-method view-type="local"
	 * @generated
	 */
	public SubProblemPK ejbCreate(long assignmentID, String subProblemName,
			float maxScore, int type, int order, int answer) throws javax.ejb.CreateException {
		setAssignmentID(assignmentID);
		setSubProblemName(subProblemName);
		setMaxScore(maxScore);
		setType(type);
		setOrder(order);
		setHidden(false);
		setAnswer(answer);
		return null;
	}
	
	/**
	 *
	 * @ejb.create-method view-type="local"
	 * @generated
	 */
	public SubProblemPK ejbCreate(long assignmentID, String subProblemName,
			float maxScore) throws javax.ejb.CreateException {
		setAssignmentID(assignmentID);
		setSubProblemName(subProblemName);
		setMaxScore(maxScore);
		setHidden(false);
		return null;
	}
  /**
   * <!-- begin-user-doc -->
   * The container invokes this method immediately after it calls ejbCreate.
   * <!-- end-user-doc -->
   * 
   * @generated
   */
  public void ejbPostCreate() throws javax.ejb.CreateException {
    // begin-user-code
    // end-user-code
  }

/**
 * Finds all hidden SubProblems in a given assignment
 * @param assignmentID
 * @return
 * @throws FinderException
 */
public Collection ejbFindHiddenByAssignmentID(long assignmentID) throws FinderException {
	return null;
}

}
