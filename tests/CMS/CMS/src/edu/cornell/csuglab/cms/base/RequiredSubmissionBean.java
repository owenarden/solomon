/*
 * Created on Nov 4, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

/**
 * Contains information pertaining to files that students will need to 
 * submit for an assignment. Has the assignment's ID, the filename 
 * that the file will be named to in the server's file-system, and 
 * submission limitations.
 * 
 * @ejb.bean name="RequiredSubmission"
 *	jndi-name="RequiredSubmissionBean"
 *	type="BMP" 
 *
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.RequiredSubmissionDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.RequiredSubmissionDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/
public abstract class RequiredSubmissionBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long submissionID, assignmentID;
	private String submissionName;
	private int maxSize;
	private boolean hidden;
	
	private RequiredFileTypeLocalHome requiredFileTypeHome;
	
	public static final String ANY_TYPE = "any";
	public static final String MATCHING_TYPE = "tgz";

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The assignmentID
	 */
	public long getAssignmentID() {
		return assignmentID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param assignmentID The assignmentID
	 */
	public void setAssignmentID(long assignmentID) {
		this.assignmentID = assignmentID;
	}
	
	/**
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getSubmissionID() {
		return submissionID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param submissionID
	 */
	public void setSubmissionID(long submissionID) {
		this.submissionID = submissionID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getSubmissionName() {
		return submissionName;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param submissionName
	 */
	public void setSubmissionName(String submissionName) {
		this.submissionName = submissionName;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public int getMaxSize() {
		return maxSize;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param maxSize
	 */
	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public boolean getHidden() {
		return hidden;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param hidden
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	
	/**
	 * @return
	 * @throws EJBException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection getRequiredFileTypes() throws EJBException {
		Collection c, result = new ArrayList();
		try {
			c = requiredFileTypeHome().findBySubmissionID(getSubmissionID());
			Iterator i = c.iterator();
			while (i.hasNext()) {
				RequiredFileTypeLocal type = (RequiredFileTypeLocal) i.next();
				result.add(type.getRequiredFileTypeData());
			}
		}
		catch (Exception e) {
			throw new EJBException(e);
		}
		return result;
	}
	
	/**
	 * Finds a RequiredFileTypeData file to match the given file type if one
	 * exists.  Otherwise returns null.
	 * @param fileType
	 * @return
	 * @throws EJBException
	 * @ejb.interface-method view-type="local"
	 */
	public RequiredFileTypeData matchFileType(String fileType) throws EJBException {
		RequiredFileTypeData result;
		try {
			RequiredFileTypeLocal file = requiredFileTypeHome().findByPrimaryKey(new RequiredFileTypePK(
					getSubmissionID(), fileType));
			result = file.getRequiredFileTypeData();
		} catch (FinderException e) {
			try {
				RequiredFileTypeLocal any = requiredFileTypeHome().findByPrimaryKey(new RequiredFileTypePK(
					getSubmissionID(), ANY_TYPE));
				result = any.getRequiredFileTypeData();
				result.setFileType(fileType);
			} catch (FinderException f) {
				return null;
			} catch (Exception g) {
				throw new EJBException(g);
			}
		} catch (Exception e) {
			throw new EJBException(e);
		}
		return result;
	}
	
	/**
	 * @throws EJBException
	 * @ejb.interface-method view-type="local"
	 */
	public int removeRequiredFileTypes() throws EJBException {
		int count = 0;
		try {
			Collection types = requiredFileTypeHome().findBySubmissionID(getSubmissionID());
			Iterator i = types.iterator();
			while (i.hasNext()) {
				RequiredFileTypeLocal next = (RequiredFileTypeLocal) i.next();
				next.remove();
				count++;
			}
		}
		catch (Exception e) {
			throw new EJBException(e);
		}
		return count;
	}
	
	/**
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public RequiredSubmissionData getRequiredSubmissionData() {
		return new RequiredSubmissionData(getAssignmentID(), 
				getSubmissionID(), getSubmissionName(), getMaxSize(), getHidden());
	}
	
	/**
	 * @param pk
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public RequiredSubmissionPK ejbFindByPrimaryKey(RequiredSubmissionPK pk) throws FinderException {
		return null;
	}
	
	/**
	 * @param assignmentID
	 * @return
	 * @ejb.interface-method view-type="local"
	 */
	public Collection ejbFindByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * @param courseID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection ejbFindByCourseID(long courseID) throws FinderException {
	    return null;
	}
	
	/**
	 * 
	 * @param assignmentID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection ejbFindHiddenByAssignmentID(long assignmentID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds the RequiredSubmissions relevant to a given
	 * group (Only finds non-hidden RequiredSubmissions)
	 * Result is sorted by SubmissionName
	 * @param groupID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByGroupID(long groupID) throws FinderException {
		return null;
	}
	
	private RequiredFileTypeLocalHome requiredFileTypeHome() throws RemoteException, NamingException {
		if (requiredFileTypeHome == null) {
			this.requiredFileTypeHome = RequiredFileTypeUtil.getLocalHome();
		}
		return requiredFileTypeHome;
	}
	
	/**
	 * @param courseID
	 * @param assignmentID
	 * @param submissionName
	 * @param maxSize
	 * @return
	 * @throws CreateException
	 * @ejb.create-method view-type="local"
	 */
	public RequiredSubmissionPK ejbCreate(long assignmentID, String submissionName,
			int maxSize) throws CreateException {
		setAssignmentID(assignmentID);
		setSubmissionName(submissionName);
		setMaxSize(maxSize);
		setHidden(false);
		return null;
	}
	
}
