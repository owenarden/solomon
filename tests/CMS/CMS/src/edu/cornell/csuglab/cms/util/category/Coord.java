package edu.cornell.csuglab.cms.util.category;

/**
 * Represents (row ID, column ID) combos to uniquely identify content cells
 * 
 * Used by CategoryXMLBuilder::buildRowListSubtrees() to iterate through contents by row & column,
 * and by TransactionsBean's add/edit ctg contents function to map to new content IDs
 */
public class Coord
{
	private long row, col;
	
	public Coord(long r, long c)
	{
		row = r;
		col = c;
	}
	
	public int hashCode()
	{
		return 17 + (int)(row * 37 + col);
	}
	
	public boolean equals(Object obj)
	{
		Coord c = (Coord)obj;
		return row == c.row && col == c.col;
	}
}