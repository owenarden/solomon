/*
 * Created on Sep 1, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.util.category;



/**
 * @author evan
 *
 * Holds info on a content currently in a category, and to be updated
 * (base class for CtgCurContent<TYPE>Info classes)
 */
public abstract class CtgCurContentInfo implements CtgContentInfo
{
	protected long contentID; //the database ID of the existing content
	
	public CtgCurContentInfo(long contentID)
	{
		this.contentID = contentID;
	}
	
	public void setContentID(long contentID){
		this.contentID = contentID;
	}
	
	public long getContentID(){
		return this.contentID;
	}
}
