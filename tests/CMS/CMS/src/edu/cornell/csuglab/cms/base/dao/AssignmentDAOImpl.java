/*
 * Created on Mar 4, 2004
 */

package edu.cornell.csuglab.cms.base.dao;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import edu.cornell.csuglab.cms.base.AssignmentBean;
import edu.cornell.csuglab.cms.base.AssignmentDAO;
import edu.cornell.csuglab.cms.base.AssignmentPK;
import edu.cornell.csuglab.cms.base.GroupMemberBean;
import edu.cornell.csuglab.cms.base.StaffBean;
import edu.cornell.csuglab.cms.base.StudentBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * @author Theodore Chao, Evan
 */
public class AssignmentDAOImpl extends DAOMaster implements AssignmentDAO {


	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentDAO#load(edu.cornell.csuglab.cms.base.AssignmentPK,
	 *      edu.cornell.csuglab.cms.base.AssignmentBean)
	 */
	public void load(AssignmentPK pk, AssignmentBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tassignment where AssignmentID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getAssignmentID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setAssignmentID(rs.getLong("AssignmentID"));
				ejb.setCourseID(rs.getLong("CourseID"));
				ejb.setName(rs.getString("Name"));
				ejb.setNameShort(rs.getString("NameShort"));
				ejb.setStatus(rs.getString("Status"));
				ejb.setWeight(rs.getFloat("Weight"));
				ejb.setMaxScore(rs.getFloat("MaxScore"));
				ejb.setDueDate(rs.getTimestamp("DueDate"));
				ejb.setGracePeriod(rs.getInt("GracePeriod"));
				ejb.setStudentRegrades(rs.getBoolean("StudentRegrade"));
				ejb.setRegradeDeadline(rs.getTimestamp("RegradeDeadline"));
				ejb.setLateDeadline(rs.getTimestamp("LateDeadline"));
				ejb.setAllowLate(rs.getBoolean("AllowLate"));
				ejb.setNumOfAssignedFiles(rs.getInt("NumAssignedFiles"));
				float val = rs.getFloat("StatMedian");
				if (rs.wasNull()) {
				    ejb.setMedian(null);
				} else {
				    ejb.setMedian(new Float(val));
				}
				val = rs.getFloat("StatMax");
				if (rs.wasNull()) {
				    ejb.setMax(null);
				} else {
				    ejb.setMax(new Float(val));
				}
				val = rs.getFloat("StatMean");
				if (rs.wasNull()) {
				    ejb.setMean(null);
				} else {
				    ejb.setMean(new Float(val));
				}
				val = rs.getFloat("StatStDev");
				if (rs.wasNull()) {
				    ejb.setStdDev(null);
				} else {
				    ejb.setStdDev(new Float(val));
				}
				ejb.setShowStats(rs.getBoolean("ShowStats"));
				ejb.setShowSolution(rs.getBoolean("ShowSolution"));
				ejb.setAssignedGraders(rs.getBoolean("AssignedGraders"));
				ejb.setGroupSizeMax(rs.getInt("GroupSizeMax"));
				ejb.setGroupSizeMin(rs.getInt("GroupSizeMin"));
				try {
					ejb.setDescription(rs.getString("Description"));
				} catch (SQLException e) {
					ejb.setDescription("");
				}
				ejb.setAssignedGroups(rs.getBoolean("AssignedGroups"));
				int limit = rs.getInt("GroupLimit");
				if (rs.wasNull()) {
				    ejb.setGroupLimit(null);
				} else {
				    ejb.setGroupLimitNN(limit);
				}
				long duration = rs.getLong("Duration");
				if (rs.wasNull()) {
				    ejb.setDuration(null);
				} else {
				    ejb.setDurationNN(duration);
				}
				ejb.setScheduled(rs.getBoolean("Scheduled"));
				Timestamp tsLockTime = rs.getTimestamp("ScheduleLockTime");
				if(rs.wasNull()) ejb.setTimeslotLockTime(null);
				else ejb.setTimeslotLockTime(tsLockTime);
				ejb.setHidden(rs.getBoolean("Hidden"));
				ejb.setType(rs.getInt("Type"));
				ejb.setDefaultLatePenalty(rs.getString("DefaultLatePenalty"));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			EJBException f = new EJBException(e.getMessage());
			f.setStackTrace(e.getStackTrace());
			throw f;
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentDAO#store(edu.cornell.csuglab.cms.base.AssignmentBean)
	 */
	public void store(AssignmentBean ejb) throws EJBException {
		preStore("Assignment", ejb);
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "update tassignment "
					+ "set Name = ?, NameShort = ?, Status = ?, Description = ?,  Weight = ?, "
					+ "MaxScore = ?, StudentRegrade = ?, Graceperiod = ?, duedate = ?, RegradeDeadline = ?, "
					+ "LateDeadline = ?, allowlate = ?, numAssignedFiles = ?, ShowStats = ?, ShowSolution = ?,  statmean = ?, statmax = ?, "
					+ "statmedian = ?, statstdev = ?, AssignedGraders = ?, AssignedGroups = ?, GroupSizeMax = ?, "
					+ "GroupSizeMin = ?, Hidden = ?, Scheduled = ?, Duration = ?, GroupLimit = ?, ScheduleLockTime = ?, Type = ?, DefaultLatePenalty = ? "
					+ "where AssignmentID = ? and CourseID = ?";
			ps = conn.prepareStatement(update);
			if(ejb.getName() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getName());
			if(ejb.getNameShort() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getNameShort());
			if(ejb.getStatus() == null) ps.setNull(3, java.sql.Types.VARCHAR);
			else ps.setString(3, ejb.getStatus());
			if(ejb.getDescription() == null) ps.setNull(4, java.sql.Types.VARCHAR);
			else ps.setString(4, ejb.getDescription());
			ps.setFloat(5, ejb.getWeight());
			ps.setFloat(6, ejb.getMaxScore());
			ps.setBoolean(7, ejb.getStudentRegrades());
			ps.setInt(8, ejb.getGracePeriod());
			ps.setTimestamp(9, ejb.getDueDate());
			ps.setTimestamp(10, ejb.getRegradeDeadline());
			ps.setTimestamp(11, ejb.getLateDeadline());
			ps.setBoolean(12, ejb.getAllowLate());
			ps.setInt(13, ejb.getNumOfAssignedFiles());
			ps.setBoolean(14, ejb.getShowStats());
			ps.setBoolean(15, ejb.getShowSolution());
			if (ejb.getMean() == null) {
			    ps.setNull(16, Types.FLOAT);
			} else {
			    ps.setFloat(16, ejb.getMean().floatValue());
			}
			if (ejb.getMax() == null) {
			    ps.setNull(17, Types.FLOAT);
			} else {
			    ps.setFloat(17, ejb.getMax().floatValue());
			}
			if (ejb.getMedian() == null) {
			    ps.setNull(18, Types.FLOAT);
			} else {
			    ps.setFloat(18, ejb.getMedian().floatValue());
			}
			if (ejb.getStdDev() == null) {
			    ps.setNull(19, Types.FLOAT);
			} else {
			    ps.setFloat(19, ejb.getStdDev().floatValue());
			}
			ps.setBoolean(20, ejb.getAssignedGraders());
			ps.setBoolean(21, ejb.getAssignedGroups());
			ps.setInt(22, ejb.getGroupSizeMax());
			ps.setInt(23, ejb.getGroupSizeMin());
			ps.setBoolean(24, ejb.getHidden());
			ps.setBoolean(25,ejb.getScheduled());
			if (ejb.getDuration() == null) {
			    ps.setNull(26, Types.BIGINT);
			} else {
			    ps.setLong(26, ejb.getDuration().longValue());
			}
			if (ejb.getGroupLimit() == null) {
			    ps.setNull(27, Types.INTEGER);
			} else {
			    ps.setInt(27, ejb.getGroupLimit().intValue());
			}
			if(ejb.getTimeslotLockTime() == null)
				ps.setNull(28, Types.TIMESTAMP);
			else ps.setTimestamp(28, ejb.getTimeslotLockTime());
			ps.setInt(29, ejb.getType());
			ps.setString(30, ejb.getDefaultLatePenalty());
			ps.setLong(31, ejb.getAssignmentID());
			ps.setLong(32, ejb.getCourseID());
			
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentDAO#remove(edu.cornell.csuglab.cms.base.AssignmentPK)
	 */
	public void remove(AssignmentPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		preRemove("Assignment");
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentDAO#create(edu.cornell.csuglab.cms.base.AssignmentBean)
	 */
	public AssignmentPK create(AssignmentBean ejb) throws CreateException,
			EJBException {
		preCreate("Assignment", ejb);
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AssignmentPK pk = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "insert into tassignment "
					+ "(courseid, name, nameshort, status, weight, "
					+ "maxscore, duedate, graceperiod, studentregrade, regradedeadline, "
					+ "latedeadline, allowlate, numassignedfiles, showstats, showsolution, assignedgraders, "
					+ "groupsizemax, groupsizemin, description, assignedgroups, scheduled, duration, "
					+ "grouplimit, schedulelocktime, type, defaultlatepenalty) "
					+ "values (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,? )";
			String findString = "select @@identity as 'AssignmentID' from tassignment";
			ps = conn.prepareStatement(createString);
			ps.setLong(1, ejb.getCourseID());
			ps.setString(2, ejb.getName());
			ps.setString(3, ejb.getNameShort());
			ps.setString(4, ejb.getStatus());
			ps.setFloat(5, ejb.getWeight());
			ps.setFloat(6, ejb.getMaxScore());
			ps.setTimestamp(7, ejb.getDueDate());
			ps.setInt(8, ejb.getGracePeriod());
			ps.setBoolean(9, ejb.getStudentRegrades());
			ps.setTimestamp(10, ejb.getRegradeDeadline());
			ps.setTimestamp(11, ejb.getLateDeadline());
			ps.setBoolean(12, ejb.getAllowLate());
			ps.setInt(13, ejb.getNumOfAssignedFiles());
			ps.setBoolean(14, ejb.getShowStats());
			ps.setBoolean(15, ejb.getShowSolution());
			ps.setBoolean(16, ejb.getAssignedGraders());
			ps.setInt(17, ejb.getGroupSizeMax());
			ps.setInt(18, ejb.getGroupSizeMin());
			if(ejb.getDescription() == null) ps.setNull(19, java.sql.Types.VARCHAR);
			else ps.setString(19, ejb.getDescription());
			ps.setBoolean(20, ejb.getAssignedGroups());
			ps.setBoolean(21, ejb.getScheduled());
			if (ejb.getDuration() == null) {
			    ps.setNull(22, Types.BIGINT);
			} else {
			    ps.setLong(22, ejb.getDuration().longValue());
			}
			if (ejb.getGroupLimit() == null) {
			    ps.setNull(23, Types.INTEGER);
			} else {
			    ps.setInt(23, ejb.getGroupLimit().intValue());
			}
			if(ejb.getTimeslotLockTime() == null) ps.setNull(24, Types.TIMESTAMP);
			else ps.setTimestamp(24, ejb.getTimeslotLockTime());
			ps.setInt(25, ejb.getType());
			ps.setString(26, ejb.getDefaultLatePenalty());
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findString).executeQuery();
			if (count == 1 && rs.next()) {
				pk = new AssignmentPK(rs.getLong("AssignmentID"));
				ejb.setAssignmentID(pk.getAssignmentID());
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			CreateException f= new CreateException(e.getMessage());
			f.setStackTrace(e.getStackTrace());
			throw f;
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.AssignmentPK)
	 */
	public AssignmentPK findByPrimaryKey(AssignmentPK pk)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select AssignmentID from tassignment where AssignmentID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, pk.getAssignmentID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find asssignment with ID = " + pk.getAssignmentID());
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentDAO#findByAssignmentID(long)
	 */
	public AssignmentPK findByAssignmentID(long assignmentID) throws FinderException {
		return findByPrimaryKey(new AssignmentPK(assignmentID));
	}
	
	public AssignmentPK findByGroupID(long groupID) throws FinderException {
	    Connection conn = null;
	    PreparedStatement ps = null;
		ResultSet rs = null;
		AssignmentPK pk;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select AssignmentID from tGroups where GroupID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, groupID);
			rs = ps.executeQuery();
			rs.next();
			pk = new AssignmentPK(rs.getLong("AssignmentID"));
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}


	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.AssignmentDAO#findByCourseID(long)
	 */
	public Collection findByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select AssignmentID from tAssignment where CourseID = ? and Hidden = ? order by DueDate";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				AssignmentPK pk = new AssignmentPK(rs.getLong("AssignmentID"));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findAllByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select AssignmentID from tAssignment where CourseID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
				AssignmentPK pk = new AssignmentPK(rs.getLong("AssignmentID"));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/**
	 * For the CMSadmin page, so the admins know when it's safe to do system maintenance
	 * (when no assignments are due soon)
	 * @see AssignmentDao#findOpenAsgnsByDeadline(Timestamp)
	 */
	public Collection findOpenAsgnsByDeadline(Timestamp deadline) throws FinderException
	{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//subtract a bit from current time to allow for grace periods of very-recently-due assignments
		long MILLISECS_PER_HOUR = 3600000;
		Timestamp now = new Timestamp(new Date().getTime() - 8 * MILLISECS_PER_HOUR);
		Collection result = new ArrayList(); //assignments will be returned in some reasonable order
		try {
			conn = jdbcFactory.getConnection();
			//String queryString = "select AssignmentID, DueDate from tAssignment as asgn WHERE asgn.Hidden = ? ORDER BY asgn.DueDate";
			//Select only due assignments for the current semester -da10
			String queryString = "SELECT asgn.AssignmentID, asgn.DueDate FROM tAssignment asgn " +
								 "INNER JOIN tCourse c ON asgn.CourseID = c.CourseID " +
								 "INNER JOIN tSystemProperties s ON c.SemesterID = s.CurrentSemester " +
								 "WHERE asgn.Hidden = ? ORDER BY asgn.DueDate";
			ps = conn.prepareStatement(queryString);
			ps.setBoolean(1, false);
			rs = ps.executeQuery();
			//take only asgns due at and onwards -da10
			while (rs.next())
			{
				Timestamp dueTime = rs.getTimestamp("DueDate");
				//if(dueTime.after(now) && dueTime.before(deadline))
				if(dueTime.after(now))
					result.add(new AssignmentPK(rs.getLong("AssignmentID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			e.printStackTrace();
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	public Collection findByCourseAdmin(long semesterID, String netID) throws FinderException {
    	Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try {
        	conn = jdbcFactory.getConnection();
        	String query = "select AssignmentID from tCourse c, tAssignment a, tStaff s where c.CourseID = a.CourseID " + 
        			"and c.CourseID = s.CourseID and s.AdminPriv = ? and s.Status = ? and a.Hidden = ? " +
        			"and c.SemesterID = ? and s.NetID = ? order by c.Code ASC";
        	ps = conn.prepareStatement(query);
        	ps.setBoolean(1, true);
        	ps.setString(2, StaffBean.ACTIVE);
        	ps.setBoolean(3, false);
        	ps.setLong(4, semesterID);
        	ps.setString(5, netID);
        	rs = ps.executeQuery();
        	while(rs.next()) {
        		result.add(new AssignmentPK(rs.getLong("AssignmentID")));
        	}
        	ps.close();
        	rs.close();
        	conn.close();
  		} catch (Exception e) {
        	try {
        		if (rs != null) rs.close();
        		if (ps != null) ps.close();
        		if (conn != null) conn.close();
        	}
        	catch (Exception f) {}
        	throw new FinderException(e.getMessage());
  		}
        return result;
	}
	
	public Collection findBySemesterID(long semesterID) throws FinderException {
    	Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try {
        	conn = jdbcFactory.getConnection();
        	String query = "select AssignmentID from tCourse c, tAssignment a where c.CourseID = a.CourseID " + 
        			"and a.Hidden = ? and c.SemesterID = ? order by c.Code ASC";
        	ps = conn.prepareStatement(query);
        	ps.setBoolean(1, false);
        	ps.setLong(2, semesterID);
        	rs = ps.executeQuery();
        	while(rs.next()) {
        		result.add(new AssignmentPK(rs.getLong("AssignmentID")));
        	}
        	ps.close();
        	rs.close();
        	conn.close();
  		} catch (Exception e) {
        	try {
        		if (rs != null) rs.close();
        		if (ps != null) ps.close();
        		if (conn != null) conn.close();
        	}
        	catch (Exception f) {}
        	throw new FinderException(e.getMessage());
  		}
        return result;
	}
	
	public Collection findByType(int type) throws FinderException {
    	Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try {
        	conn = jdbcFactory.getConnection();
        	String query = "select AssignmentID from tCourse c, tAssignment a where c.CourseID = a.CourseID " + 
        			"and a.Hidden = ? and a.Type = ? order by c.Code ASC";
        	ps = conn.prepareStatement(query);
        	ps.setBoolean(1, false);
        	ps.setInt(2, type);
        	rs = ps.executeQuery();
        	while(rs.next()) {
        		result.add(new AssignmentPK(rs.getLong("AssignmentID")));
        	}
        	ps.close();
        	rs.close();
        	conn.close();
  		} catch (Exception e) {
        	try {
        		if (rs != null) rs.close();
        		if (ps != null) ps.close();
        		if (conn != null) conn.close();
        	}
        	catch (Exception f) {}
        	throw new FinderException(e.getMessage());
  		}
        return result;
	}
	
	/* (non-Javadoc)
	 * Find all assignments currently open and due for all the given student's courses
	 * @see edu.cornell.csuglab.cms.base.AssignmentDAO#findByDateNetID(String,Date)
	 */
	public Collection findByDateNetID(String netID, Timestamp current)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select a.AssignmentID from tAssignment a inner join "
			    	+ "tCourse c on c.CourseID = a.CourseID inner join " 
			    	+ "tSystemProperties p on p.CurrentSemester = c.SemesterID inner join "
					+ "tstudent s on a.courseid = s.courseid "
					+ "where netid = ? and duedate > ? and a.status = ? and a.Hidden = ? and s.status = ? and p.ID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netID);
			ps.setTimestamp(2, current);
			ps.setString(3, AssignmentBean.OPEN);
			ps.setBoolean(4, false);
			ps.setString(5, StudentBean.ENROLLED);
			ps.setInt(6, 1);
			rs = ps.executeQuery();
			while (rs.next()) {
				AssignmentPK pk = new AssignmentPK(rs.getLong("AssignmentID"));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findByDateNetIDSemester(String netID, Timestamp current, long semesterID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select a.AssignmentID from tAssignment a inner join "
			    	+ "tCourse c on a.CourseID = c.CourseID inner join "
					+ "tstudent s on a.courseid = s.courseid "
					+ "where netid = ? and duedate > ? and a.status = ? and c.SemesterID = ? and a.hidden = ? and s.status = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netID);
			ps.setTimestamp(2, current);
			ps.setString(3, AssignmentBean.OPEN);
			ps.setLong(4, semesterID);
			ps.setBoolean(5, false);
			ps.setString(6, StudentBean.ENROLLED);
			rs = ps.executeQuery();
			while (rs.next()) {
				AssignmentPK pk = new AssignmentPK(rs.getLong("AssignmentID"));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.AssignmentDAO#findHiddenByCourseID(long)
     */
    public Collection findHiddenByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select AssignmentID from tAssignment where CourseID = ? and Hidden = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setBoolean(2, true);
			rs = ps.executeQuery();
			while (rs.next()) {
				AssignmentPK pk = new AssignmentPK(rs.getLong("AssignmentID"));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
    }
    
      /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.AssignmentDAO#findAllNotRecordedForStudent(long)
     */
    public Collection findGrouplessAssignments(long courseID, String studentNetID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT a.AssignmentID	FROM tGroupMembers m INNER JOIN " +
	        	"tGroups g ON m.GroupID = g.GroupID INNER JOIN " +
	            "tAssignment a ON a.AssignmentID = g.AssignmentID " +
				"WHERE (m.NetID = ?) AND (m.Status = ?) AND (a.CourseID = ?)";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, studentNetID);
			ps.setString(2, GroupMemberBean.ACTIVE);
			ps.setLong(3, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
				AssignmentPK pk = new AssignmentPK(rs.getLong("AssignmentID"));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
    }
}
