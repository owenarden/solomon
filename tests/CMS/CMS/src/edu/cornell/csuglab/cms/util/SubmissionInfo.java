/*
 * Created on Sep 17, 2004
 *
 */
package edu.cornell.csuglab.cms.util;

import edu.cornell.csuglab.cms.base.RequiredSubmissionData;

/**
 * @author Jon
 *
 */
public class SubmissionInfo {
	
	private RequiredSubmissionData submission;
	private int fileCounter;
	private String md5, fileType;
	private int fileLength;
	// full file name as uploaded by user
	private String fileName;
	private boolean isLate;
	
	public SubmissionInfo(RequiredSubmissionData submission, String fileType,
			int fileCounter, String md5, int fileLength, String fileName, boolean isLate) {
		this.submission = submission;
		this.fileType = fileType;
		this.fileCounter = fileCounter;
		this.md5 = md5;
		this.fileLength = fileLength;
		this.fileName = fileName;
		this.isLate = isLate;
	}
	
	public RequiredSubmissionData getSubmission() {
		return submission;
	}
	
	public int getFileCounter() {
		return fileCounter;
	}
	
	public String getFileType() {
		return fileType;
	}
	
	public String getFileName() {
	    return fileName;
	}
	
	public String getMD5() {
		return md5;
	}
	
	public int getFileLength() {
		return fileLength;
	}
	
	public boolean getIsLate() {
		return isLate;
	}
	
}
