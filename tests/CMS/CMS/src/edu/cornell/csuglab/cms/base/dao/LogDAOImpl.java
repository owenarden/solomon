package edu.cornell.csuglab.cms.base.dao;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.*;
import edu.cornell.csuglab.cms.log.*;

/**
 * @author Evan created 5 / 8 / 05
 */
public class LogDAOImpl extends DAOMaster implements LogDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.LogDAO#load(edu.cornell.csuglab.cms.base.LogPK,
	 *      edu.cornell.csuglab.cms.base.LogBean)
	 */
	public void load(LogPK pk, LogBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tLogs where LogID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getLogID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setLogID(rs.getLong("LogID"));
				ejb.setActingNetID(rs.getString("ActingNetID"));
				ejb.setSimulatedNetID(rs.getString("SimulatedNetID"));
				ejb.setActingIPAddress(InetAddress.getByName(rs
						.getString("ActingIPAddress")));
				ejb.setTimestamp(rs.getTimestamp("Dte"));
				ejb.setLogName(rs.getString("LogName"));
				ejb.setLogType(rs.getLong("LogType"));
				if (rs.getLong("CourseID") == 0) {
					ejb.setCourseID(null);
				} else {
					ejb.setCourseID(new Long(rs.getLong("CourseID")));
				}
				ps.close();
				rs.close();
				LogData logdata = ejb.getLogData();
				// get detailed logs
				String detailsQuery = "select * from tLogDetails where LogID = ?";
				ps = conn.prepareStatement(detailsQuery);
				ps.setLong(1, pk.getLogID());
				rs = ps.executeQuery();
				Vector details = new Vector();
				SortedSet recNetIDs = new TreeSet();
				while (rs.next()) {
					LogDetail detail = new LogDetail(pk.getLogID());
					detail.setLogID(rs.getLong("LogID"));
					String detailstr = null;
					try {
						detailstr = rs.getString("Details");
					} catch (SQLException e) {
						detailstr = "";
					}
					String netID = rs.getString("NetID");
					long aID = rs.getLong("AssignmentID");
					Long assignID = aID == 0 ? null : new Long(aID);
					if (netID != null) {
						recNetIDs.add(netID);
						detail.setNetID(netID);
					}
					if (assignID != null) {
						ejb.addAssignmentID(assignID.longValue());
						detail.setAssignmentID(assignID);
					}
					if (detailstr != null) {
						detail.setDetailString(detailstr);
						detail.setLogRef(logdata);
						details.add(detail);
					}
				}
				ejb.setReceivingNetIDs(recNetIDs);
				ejb.setDetailLogs(details);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null)
					conn.close();
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception f) {
			}
			throw new EJBException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.LogDAO#store(edu.cornell.csuglab.cms.base.LogBean)
	 */
	public void store(LogBean ejb) throws EJBException {
		// store() is called at odd times; don't really want to throw an
		// exception because
		// I don't fully understand when store() might be called -- Evan
		// throw new EJBException("LogDAOImpl.store() called. No logs should be
		// edited!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.LogDAO#remove(edu.cornell.csuglab.cms.base.LogPK)
	 */
	public void remove(LogPK pk) throws RemoveException, EJBException {
		throw new EJBException(
				"LogDAOImpl.remove() called. No logs should be removed!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.LogDAO#create(edu.cornell.csuglab.cms.base.LogBean)
	 */
	public LogPK create(LogBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		LogPK pk = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			// add high-level log
			String createString = "insert into tLogs (ActingNetID, SimulatedNetID, "
					+ "ActingIPAddress, Dte, LogName, LogType, CourseID)"
					+ "values (?, ?, ?, ?, ?, ?, ?)";
			ps = conn.prepareStatement(createString);
			ps.setString(1, ejb.getActingNetID());
			if (ejb.getSimulatedNetID() == null)
				ps.setNull(2, java.sql.Types.VARCHAR);
			else
				ps.setString(2, ejb.getSimulatedNetID());
			ps.setString(3, ejb.getActingIPAddress().getHostAddress());
			ps.setTimestamp(4, ejb.getTimestamp());
			if (ejb.getLogName() == null)
				ps.setNull(5, java.sql.Types.VARCHAR);
			else
				ps.setString(5, ejb.getLogName());
			ps.setLong(6, ejb.getLogType());
			Long cid = ejb.getCourseID();
			if (cid == null)
				ps.setNull(7, java.sql.Types.BIGINT);
			else
				ps.setLong(7, cid.longValue());
			int rows = ps.executeUpdate();
			rs = conn.createStatement().executeQuery(
					"select @@identity as 'LogID' from tLogs");
			long logID = 0;
			if (rows > 0 && rs.next()) {
				logID = rs.getLong("LogID");
				pk = new LogPK(logID);
				ejb.setLogID(logID);
			} else
				throw new CreateException(
						"Failed to create log entry in tLogs table");
			// add any necessary low-level logs
			Iterator i = ejb.getDetailLogs().iterator();
			ps.close();
			ps = conn
					.prepareStatement("insert into tLogDetails (LogID, Details, NetID, AssignmentID) values (?, ?, ?, ?)");
			ps.setLong(1, logID);
			Vector newDetails = new Vector();
			TreeSet recNetIDs = new TreeSet();
			while (i.hasNext()) {
				LogDetail detail = (LogDetail) i.next();
				/*
				 * FIXME do we need this check? but probably should leave it in
				 * until we're sure it's not needed (note we will eventually
				 * want to get rid of it) 4 / 2 / 06: I just edited the
				 * LogDetail class and I'm not sure whether now there might be
				 * some completely empty details getting created, and not sure
				 * what actions would cause them if indeed they do occur -- Evan
				 * (I'm trying to get rid of details with assignment IDs and
				 * nothing else getting added for actions that aren't associated
				 * with assignments, like adding students)
				 */
				if (detail.getDetailString() == null
						&& detail.getNetID() == null
						&& detail.getAssignmentID() == null) {
					System.out
							.println("LogDAOImpl::create(): would have added a null detail for log id "
									+ logID);
				} else {
					if (detail.getDetailString() == null) {
						ps.setNull(2, Types.VARCHAR);
					} else {
						newDetails.add(detail);
						ps.setString(2, detail.getDetailString());
					}
					if (detail.getNetID() == null) {
						ps.setNull(3, Types.VARCHAR);
					} else {
						recNetIDs.add(detail.getNetID());
						ps.setString(3, detail.getNetID());
					}
					if (detail.getAssignmentID() == null) {
						ps.setNull(4, Types.BIGINT);
					} else {
						ejb.addAssignmentID(detail.getAssignmentID()
								.longValue());
						ps.setLong(4, detail.getAssignmentID().longValue());
					}
					ps.executeUpdate();
				}
			}
			ejb.setDetailLogs(newDetails);
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				e.printStackTrace();
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (Exception f) {
				f.printStackTrace();
			}
			throw new CreateException(
					"Database failed to create high-level log entry");
		}
		return pk;
	}

	/**
	 * auxiliary to create(): add a low-level log to the detailed-log table
	 * 
	 * @param logDetail
	 * @throws CreateException
	 */
	protected void createLowLevelLogEntry(LogDetail logDetail)
			throws CreateException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			// add low-level log
			String createString = "insert into tLogDetails (LogID, Details) values (?, ?)";
			ps = conn.prepareStatement(createString);
			ps.setLong(1, logDetail.getLogID());
			ps.setString(2, logDetail.getDetailString());
			int rows = ps.executeUpdate();
			if (rows < 1)
				throw new CreateException("Error inserting detailed log entry");
			conn.close();
			ps.close();
		} catch (Exception e) {
			try {
				e.printStackTrace();
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
			} catch (Exception f) {
				f.printStackTrace();
			}
			throw new CreateException(
					"Database failed to create low-level log entry");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.LogDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.LogPK)
	 */
	public LogPK findByPrimaryKey(LogPK key) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select LogID from tLogs where LogID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, key.getLogID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find log with ID = "
						+ key.getLogID());
			}
			conn.close();
			rs.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (Exception f) {
			}
			throw new FinderException(e.getMessage());
		}
		return key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.LogDAO#find(LogSearchParams)
	 */
	public Collection find(LogSearchParams params) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Vector results = new Vector();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select distinct l.LogID from tLogs as l LEFT OUTER JOIN tLogDetails as n ON l.LogID = n.LogID", conditionString = " where"; // to
																																								// be
																																								// appended
																																								// if
																																								// anything
																																								// gets
																																								// tacked
																																								// on
			// check for conditions imposed by all available search parameters
			String actingNetID = params.getActingNetID();
			if (actingNetID != null) {
				conditionString = addFindCondition(conditionString,
						"l.ActingNetID = ?");
			}
			String receivingNetID = params.getReceivingNetID();
			if (receivingNetID != null) {
				conditionString = addFindCondition(conditionString,
						"n.NetID = ?");
			}
			String simulatedNetID = params.getSimulatedNetID();
			if (simulatedNetID != null) {
				conditionString = addFindCondition(conditionString,
						"l.SimulatedNetID = ?");
			}
			String ip = params.getActingIPAddress();
			if (ip != null) {
				conditionString = addFindCondition(conditionString,
						"l.ActingIPAddress like ?");
			}
			Timestamp startTime = params.getStartTime(), endTime = params
					.getEndTime();
			if (startTime != null && endTime == null) {
				conditionString = addFindCondition(conditionString,
						"l.Dte >= ?");
			} else if (startTime == null && endTime != null) {
				conditionString = addFindCondition(conditionString,
						"l.Dte <= ?");
			} else if (startTime != null && endTime != null) {
				conditionString = addFindCondition(conditionString,
						"l.Dte between ? and ?");
			}
			long logTypes = params.getLogTypes();
			if (logTypes != 0 || logTypes == LogBean.LOG_ALL) {
				String typeCondition = "";
				for (long type = 1; type <= LogBean.MAX_INDIVIDUAL_LOG_TYPE; type <<= 1) {
					if ((logTypes & type) > 0) {
						if (typeCondition.equals("")) {
							typeCondition = "LogType = ?";
						} else {
							typeCondition += " or LogType = ?";
						}
					}
				}
				typeCondition = "(" + typeCondition + ")";
				conditionString = addFindCondition(conditionString,
						typeCondition);
			}
			Collection logNames = params.getLogNames();
			Iterator i = logNames.iterator();
			String typeCondition = "";
			while (i.hasNext()) {
				i.next();
				if (typeCondition.equals("")) {
					typeCondition = "LogName = ?";
				} else {
					typeCondition += " or LogName = ?";
				}
			}
			if (!typeCondition.equals("")) {
				conditionString = addFindCondition(conditionString, "("
						+ typeCondition + ")");
			}
			Long courseID = params.getCourseID();
			if (courseID != null) {
				conditionString = addFindCondition(conditionString,
						"CourseID = ?");
			}
			Long assignmentID = params.getAssignmentID();
			if (assignmentID != null) {
				conditionString = addFindCondition(conditionString,
						"n.AssignmentID = ?");
			}
			// check for conditions given by the params object
			if (!conditionString.equals(" where")) // at least one filter was
													// added
				queryString += conditionString;
			ps = conn.prepareStatement(queryString);
			int count = 1;
			if (actingNetID != null) {
				ps.setString(count++, actingNetID);
			}
			if (receivingNetID != null) {
				ps.setString(count++, receivingNetID);
			}
			if (simulatedNetID != null) {
				ps.setString(count++, simulatedNetID);
			}
			if (ip != null) {
				ps.setString(count++, ip);
			}
			if (startTime != null && endTime == null) {
				ps.setTimestamp(count++, startTime);
			} else if (startTime == null && endTime != null) {
				ps.setTimestamp(count++, endTime);
			} else if (startTime != null && endTime != null) {
				ps.setTimestamp(count++, startTime);
				ps.setTimestamp(count++, endTime);
			}
			if (logTypes != 0) {
				for (long type = 1; type <= LogBean.MAX_INDIVIDUAL_LOG_TYPE; type <<= 1) {
					if ((logTypes & type) > 0) {
						ps.setLong(count++, type);
					}
				}
			}
			i = logNames.iterator();
			while (i.hasNext()) {
				ps.setString(count++, (String) i.next());
			}
			if (courseID != null) {
				ps.setLong(count++, courseID.longValue());
			}
			if (assignmentID != null) {
				ps.setLong(count++, assignmentID.longValue());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				results.add(new LogPK(rs.getLong("LogID")));
			}
			conn.close();
			rs.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (Exception f) {
			}
			e.printStackTrace();
			throw new FinderException(e.getMessage());
		}
		return results;
	}

	/**
	 * auxiliary to find(): appends a condition to the end of the condition
	 * string for the general find SQL query, adding "and" if necessary between
	 * conditions
	 * 
	 * @param conditionString
	 *            the current conditional part of the SQL query, e.g. " where"
	 *            or " where X = 1 and Y = 2" (should not end in a space)
	 * @param newCondition
	 *            the condition being added to the query, e.g. "Z = 5" (this
	 *            gets trimmed, so spaces don't matter)
	 */
	private String addFindCondition(String conditionString, String newCondition) {
		newCondition = newCondition.trim();
		if (conditionString.endsWith(" where")) // there are no conditions
												// appended yet
			return conditionString + " " + newCondition;
		else
			// there have been conditions appended
			return conditionString + " and " + newCondition;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.LogDAO#findCommentsNRegrades(String,
	 *      long) TODO finish me?
	 */
	public Collection findCommentsNRegrades(String studentNetID,
			long assignmentID) throws FinderException {
		throw new FinderException(
				"LogDAOImpl.findCommentsNRegrades() not yet implemented!");
	}
}
