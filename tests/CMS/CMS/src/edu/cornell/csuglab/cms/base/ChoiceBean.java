/*
 * Created on Nov 5, 2004
 *
 */
package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

/**
 * This contains information for source files associated with 
 * an survey choice. An Choice holds the choice to one Subproblem
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.bean name="Choice"
 *	jndi-name="ChoiceBean"
 *	type="BMP" 
 * 
 * @ejb.dao class="edu.cornell.csuglab.cms.base.ChoiceDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.ChoiceDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/
public abstract class ChoiceBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long choiceID, subProblemID;
	private String letter;
	private String text;
	boolean hidden;
	
	private ChoiceLocalHome choiceHome = null;
	
	/**
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getChoiceID() {
		return choiceID;
	}
	
	/**
	 * @param choiceID
	 * @ejb.interface-method view-type="local"
	 */
	public void setChoiceID(long choiceID) {
		this.choiceID = choiceID;
	}
		
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getSubProblemID() {
		return subProblemID;
	}
	
	/**
	 * @param choiceID
	 * @ejb.interface-method view-type="local"
	 */
	public void setSubProblemID(long subProblemID) {
		this.subProblemID = subProblemID;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getLetter() {
		return letter;
	}
	
	/**
	 * @param letter
	 * @ejb.interface-method view-type="local"
	 */
	public void setLetter(String letter) {
		this.letter = letter;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * @param itemName
	 * @ejb.interface-method view-type="local"
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the status of the subassignment; 0 = OPEN, 1 = HIDDEN
	 */
	public boolean getHidden() {
		return hidden;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param status The status to set this subassignment to; 
	 * 1 = OPEN, 0 = HIDDEN
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	
	/**
	 * @param choiceID
	 * @param itemName
	 * @return
	 * @ejb.create-method view-type="local"
	 */
	public ChoicePK ejbCreate(long choiceID, long subproblemID, String letter, String text, boolean hidden) throws CreateException {
		setChoiceID(choiceID);
		setSubProblemID(subproblemID);
		setLetter(letter);
		setText(text);
		setHidden(hidden);
		return null;
	}

	/**
	 * @param choiceID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public ChoicePK ejbFindByChoiceID(long choiceID) throws FinderException {
		return null;
	}

	/**
	 * @param choiceID
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public ChoicePK ejbFindByPrimaryKey(ChoicePK pk) throws FinderException {
		return null;
	}
	
	/**
	 * @param subProblemID
	 * @param hidden
	 * @return
	 * @throws FinderException
	 * @ejb.interface-method view-type="local"
	 */
	public Collection ejbFindBySubProblemID(long subProblemID, boolean hidden) throws FinderException {
		return null;
	}
}
