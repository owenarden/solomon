/*
 * Created on Apr 7, 2005
 */
package edu.cornell.csuglab.cms.author;


/**
 * @author yc263
 *
 * UserStaffAsCornellMember represents the apparent guest(not authenticated) user 
 * for a staff who has decided to view it's course page as that apparent user; 
 */
public class UserStaffAsGuest extends UserGuest{

	public UserStaffAsGuest(long courseID){
		super(courseID);
	}

	public int getUserType(){
		return UT_STAFF_AS_GUEST;
	}
	
}
