/*
 * Created on Nov 2, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.FinderException;

import edu.cornell.csuglab.cms.author.Principal;



 /**
 * @ejb.bean name="CategoryCol"
 *	jndi-name="CategoryColBean"
 *	type="BMP"
 *
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class = "edu.cornell.csuglab.cms.base.CategoryColDAO"
 * impl-class = "edu.cornell.csuglab.cms.base.dao.CategoryColDAOImpl"
 * 
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * */

public abstract class CategoryColBean implements javax.ejb.EntityBean {
	
	//legal column data types
    public static final String 
		DATE = "date",
		TEXT = "text",
		FILE = "file",
		URL = "url",
		NUMBER = "number";
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long colID;
	private String colName;
	private String colType; //one of the types enumerated above
	private long categoryID;
	/*
	 * "Hidden" now means a column is not shown on the course home page.
	 * The term used for removing a column from the active category is now "removed".
	 * Hidden columns are still visible on the layout and addNedit pages;
	 * removed columns can be viewed and re-added on the layout page.
	 * In this way staff with category privileges are the only ones who can see
	 * (hidden + nonremoved) columns, but the home page is never cluttered by them.
	 * 
	 * (Hidden + nonremoved) columns are useful, for example, for user-defined
	 * ordering of rows: use a number-format hidden column and provide indices.
	 * Or they can provide space for working notes.
	 * 
	 * - Evan, 4 / 06
	 */
	private boolean hidden; //whether non-categoryadmins can see/edit the column
	private boolean removed; //whether the column can be seen/edited at all
	private long position; //starts at 1
	
	/**
	 * @ejb.pk-field
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the ID that uniquely identifies a category column
	 */
	public long getColID(){
		return this.colID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param ID - the ID that uniquely identifies a category column
	 */
	public void setColID(long ID){
		this.colID = ID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the name of the column
	 */
	public String getColName(){
		return this.colName;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param colName - the name of the column
	 */
	public void setColName(String colName){
		this.colName = colName;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the column type(only four types available at the moment)
	 */
	public String getColType(){
		return this.colType;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param type
	 */
	public void setColType(String type){
		if(isLegalColumnType(type))
			this.colType = type;
		else throw new RuntimeException("CategoryColBean::setColType(): given invalid type '" + type + "'");
	}
	
	public static boolean isLegalColumnType(String type)
	{
		return type.equals(DATE) || type.equals(TEXT) || type.equals(FILE) || type.equals(URL) || type.equals(NUMBER);
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the unique id of the category that this column associated with
	 */
	public long getCategoryID(){
		return this.categoryID;
	}

	/**
	 *  @ejb.interface-method view-type="local"
	 * @param categoryID - the unique id of the category that this column associated with
	 */
	public void setCategoryID(long categoryID){
		this.categoryID = categoryID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the column number
	 */
	public long getPosition(){
		return this.position;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param position
	 */
	public void setPosition(long position){
		this.position = position;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return true if this column is hidden, false otherwise
	 */
	public boolean getHidden(){
		return this.hidden;
	}
	
	/**
	 *  @ejb.interface-method view-type="local"
	 * @param hidden - true if column should be hidden, false otherwise
	 */
	public void setHidden(boolean hidden){
		this.hidden = hidden;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return true if this column is removed from the active list, false otherwise
	 */
	public boolean getRemoved(){
		return this.removed;
	}
	
	/**
	 *  @ejb.interface-method view-type="local"
	 * @param hidden - true if column should be removed from the active list, false otherwise
	 */
	public void setRemoved(boolean removed){
		this.removed = removed;
	}
	
	/**
	 * Checks to see if the given primary key can be found in the database
	 * @param pk The primary key being searched for
	 * @return The key passed in if it's found
	 * @throws FinderException
	 */
	public CategoryColPK ejbFindByPrimaryKey(CategoryColPK pk) throws FinderException{
		return null;
	}
	
	/**
	 * @param categoryID
	 * @param removed Whether we should return removed or active columns
	 * (we never return a mix of the two)
	 * @param visible If removed is false, whether we should limit our search to visible columns
	 * @return a collection of (all or only visible) columns for the given category
	 * @throws FinderException
	 */
	public Collection ejbFindByCategoryID(long categoryID, boolean removed, boolean visible) throws FinderException{
		return null;
	}
	
	/**
	 * @param courseID
	 * @return a collection of visible category columns for that course
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseID(long courseID, Principal p) throws FinderException{
		return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	*/
	public CategoryColData getCategoryColData(){
		return new CategoryColData(getColID(), getColName(), getColType(), getCategoryID(),
				getPosition(), getHidden(), getRemoved());
	}
	
	/**
	 * @param courseID
	 * @param categoryName
	 * @param ascending
	 * @return
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local"
	 */
	public CategoryColPK ejbCreate(String colName, String colType, long categoryID, boolean hidden, boolean removed, long position)
		throws javax.ejb.CreateException{
		setColName(colName);
		setColType(colType);
		setCategoryID(categoryID);
		setHidden(hidden);
		setRemoved(removed);
		setPosition(position);
		return null;
	}
}
