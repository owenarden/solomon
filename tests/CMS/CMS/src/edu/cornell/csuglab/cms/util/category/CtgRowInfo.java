/*
 * Created on Jan 27, 2005
 */
package edu.cornell.csuglab.cms.util.category;

import java.sql.Timestamp;

/**
 * @author yc263
 *
 * Holds metadata on a row to be created; does *not* hold data on the contents to be put into this row
 */
public class CtgRowInfo {
	private long rowID;
	private long categoryID;
	private boolean hidden;
	private Timestamp releaseDate;
	
	public CtgRowInfo(long rowID){
		this.rowID = rowID;
		this.releaseDate = null;
	}
	
	public CtgRowInfo(long rowID, boolean hidden){
		this.rowID = rowID;
		this.hidden = hidden;
		this.releaseDate = null;
	}
	
	public void setReleaseDate(Timestamp releaseDate){
		this.releaseDate = releaseDate;
	}
	
	public Timestamp getReleaseDate(){
		return this.releaseDate;
	}
	
	public void setRowID(long rowID){
		this.rowID = rowID;
	}
	
	public long getRowID(){
		return this.rowID;
	}
	
	public void setCtgID(long ctgID){
		this.categoryID = ctgID;
	}
	
	public long getCtgID(){
		return this.categoryID; 
	}
	
	public void setHidden(boolean hidden){
		this.hidden = hidden;
	}
	
	public boolean getHidden(){
		return this.hidden ;
	}
}
