/*
 * Created on Feb 27, 2004
 */

package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.GroupMemberBean;
import edu.cornell.csuglab.cms.base.StudentBean;
import edu.cornell.csuglab.cms.base.StaffBean;
import edu.cornell.csuglab.cms.base.UserBean;
import edu.cornell.csuglab.cms.base.UserDAO;
import edu.cornell.csuglab.cms.base.UserPK;
import edu.cornell.csuglab.cms.www.util.Profiler;

/**
 * @author surge
 */
public class UserDAOImpl extends DAOMaster implements UserDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.UserDAO#create(edu.cornell.csuglab.cms.base.UserBean)
	 */
	public UserPK create(UserBean ejb) throws CreateException, EJBException {
		preCreate("User", ejb);
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String updateString = "insert into tUser "
					+ "(NetID, FirstName, LastName, CUID, DomainID, PasswordExpired, Email, Deactivated, PasswordHash, PasswordSalt) values "
					+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			ps = conn.prepareStatement(updateString);
			if(ejb.getUserID() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getUserID());
			if(ejb.getFirstName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getFirstName());
			if(ejb.getLastName() == null) ps.setNull(3, java.sql.Types.VARCHAR);
			else ps.setString(3, ejb.getLastName());
			if(ejb.getCUID() == null) ps.setNull(4, java.sql.Types.VARCHAR);
			else ps.setString(4, ejb.getCUID());
			ps.setInt(5, ejb.getDomainID());
			ps.setBoolean(6, ejb.isPWExpired());
			if(ejb.getEmail() == null) ps.setNull(7, java.sql.Types.VARCHAR);
			else ps.setString(7, ejb.getEmail());
			ps.setBoolean(8, ejb.isDeactivated());
			ps.setBytes(9, ejb.getPWHash());
			ps.setBytes(10, ejb.getPWSalt());
			ps.executeUpdate();
			conn.close();
			ps.close();
        } catch (Exception e) {
        	try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return new UserPK(ejb.getUserID());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.UserDAO#load(edu.cornell.csuglab.cms.base.UserPK,
	 *      edu.cornell.csuglab.cms.base.UserBean)
	 */
	public void load(UserPK pk, UserBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Profiler.enterMethod("UserDAOImpl.load", pk.getUserID());
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID, FirstName, LastName, CUID, College, DomainID, PasswordExpired, Email, Deactivated, PasswordHash, PasswordSalt from tUser where NetID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, pk.getUserID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setUserID(rs.getString("NetID"));
				ejb.setFirstName(DAOUtils.getResultSetString(rs, "FirstName"));
				ejb.setLastName(DAOUtils.getResultSetString(rs, "LastName"));
				ejb.setCUID(rs.getString("CUID"));
				ejb.setCollege(rs.getString("College"));
				ejb.setDomainID(rs.getInt("DomainID"));
				ejb.setPWExpired(rs.getBoolean("PasswordExpired"));
				ejb.setEmail(rs.getString("Email"));
				ejb.setIsDeactivated(rs.getBoolean("Deactivated"));
				ejb.setPWHash(rs.getBytes("PasswordHash"));
				ejb.setPWSalt(rs.getBytes("PasswordSalt"));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
		Profiler.exitMethod("UserDAOImpl.load", pk.getUserID());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.UserDAO#remove(edu.cornell.csuglab.cms.base.UserPK)
	 */
	public void remove(UserPK pk) throws RemoveException, EJBException {
		preRemove("User");
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String removeString = "delete from tUser where NetID = ?";
			ps = conn.prepareStatement(removeString);
			ps.setString(1, pk.getUserID());
			ps.executeUpdate();
			ps.close();
			conn.close();
		}
		catch (SQLException e)
		{
			try
			{
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}
			catch(Exception f) {}
			throw new EJBException("Row id " + pk.userID + " failed to delete.", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.UserDAO#store(edu.cornell.csuglab.cms.base.UserBean)
	 */
	public void store(UserBean ejb) throws EJBException {
		preStore("User", ejb);
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String updateString = "update tUser "
					+ "set FirstName = ?, LastName = ?, CUID = ?, "
					+ "College = ?, DomainID = ?, PasswordExpired = ?, Email = ?, Deactivated = ?," 
					+ "PasswordHash = ?, PasswordSalt = ? "
					+ "where NetID = ?";
			ps = conn.prepareStatement(updateString);
			if(ejb.getFirstName() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getFirstName());
			if(ejb.getLastName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getLastName());
			if(ejb.getCUID() == null) ps.setNull(3, java.sql.Types.VARCHAR);
			else ps.setString(3, ejb.getCUID());
			if(ejb.getCollege() == null) ps.setNull(4, Types.VARCHAR);
			else ps.setString(4, ejb.getCollege());
			ps.setInt(5, ejb.getDomainID());
			ps.setBoolean(6, ejb.isPWExpired());
			if(ejb.getEmail() == null) ps.setNull(7, java.sql.Types.VARCHAR);
			else ps.setString(7, ejb.getEmail());
			ps.setBoolean(8, ejb.isDeactivated());
			ps.setBytes(9, ejb.getPWHash());
			ps.setBytes(10, ejb.getPWSalt());
			ps.setString(11, ejb.getUserID());
			ps.executeUpdate();
			conn.close();
			ps.close();
        } catch (Exception e) {
        	try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.UserDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.UserPK)
	 */
	public UserPK findByPrimaryKey(UserPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;		
		Profiler.enterMethod("UserDAOImpl.findByPrimaryKey","","CheckUserDB");
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID from tUser where NetID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, pk.userID);
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find user with NetID " + pk.getUserID());
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		Profiler.exitMethod("UserDAOImpl.findByPrimaryKey","","CheckUserDB");
		return pk;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.UserDAO#findByUserID(java.lang.String)
	 */
	public UserPK findByUserID(String userID) throws FinderException {
		return findByPrimaryKey(new UserPK(userID));
	}
	 
	public UserPK findByCUID(String cuid) throws FinderException
	{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		UserPK user = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT NetID FROM tUser WHERE CUID = ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, cuid);
			rs = ps.executeQuery();
			if(!rs.next()) throw new FinderException("Could not find user with CUID " + cuid);
			else user = new UserPK(rs.getString("NetID"));
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    e.printStackTrace();
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    throw new FinderException(e.getMessage());
		}
		return user;
	}

	public Collection findMissingNames() throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT NetID FROM tUser WHERE FirstName = ? OR LastName = ? OR FirstName is NULL OR LastName is NULL OR FirstName LIKE ? OR LastName LIKE ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, "");
			ps.setString(2, "");
			ps.setString(3, "_.");
			ps.setString(4, "_.");
			rs = ps.executeQuery();
			while (rs.next()) {
			    UserPK user = new UserPK(rs.getString("NetID"));
			    result.add(user);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    e.printStackTrace();
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    throw new FinderException(e.getMessage());
		}
		return result;
	}

	public Collection findStaffByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT * FROM tUser u INNER JOIN " +
                      "tStaff s ON u.NetID = s.NetID WHERE (s.CourseID = ?) AND (s.Status = ?) "
							+ "ORDER BY u.NetID";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			ps.setString(2, StaffBean.ACTIVE);
			rs = ps.executeQuery();
			while (rs.next()) {
			    result.add(new UserPK(rs.getString("NetID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    e.printStackTrace();
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    throw new FinderException(e.getMessage());
		}
		return result;
	}	
	
	public Collection findByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT * FROM tUser u INNER JOIN " +
                      "tStudent s ON u.NetID = s.NetID WHERE (s.CourseID = ?) AND (s.Status = ?) "
							+ "ORDER BY u.NetID";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			ps.setString(2, StudentBean.ENROLLED);
			rs = ps.executeQuery();
			while (rs.next()) {
			    result.add(new UserPK(rs.getString("NetID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    e.printStackTrace();
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findByNetIDs(String[] netids) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (netids.length == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT NetID FROM tUser WHERE (";
			for (int i=0; i < netids.length - 1; i++) {
			    query += "NetID = ? OR ";
			}
			query += "NetID = ?) order by NetID";
			ps = conn.prepareStatement(query);
			for (int i=0; i < netids.length; i++) {
			    ps.setString(i+1, netids[i]);
			}
			rs = ps.executeQuery();
			while (rs.next()) {
			    UserPK user = new UserPK(rs.getString("NetID"));
			    result.add(user);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    e.printStackTrace();
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.UserDAO#findByDomain()
	 */
	public Collection findByDomain(int domainID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID from tUser Where DomainID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setInt(1, domainID);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new UserPK(rs.getString(1)));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}

		return result;
	}
	
	public Collection findByGroupIDs(long[] groupids) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (groupids.length == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select u.NetID from tUser u, tGroupMembers g where " +
					"u.NetID = g.NetID and g.Status = ? and (";
			for (int i=0; i < groupids.length - 1; i++) {
				query += "g.GroupID = ? or ";
			}
			query += "g.GroupID = ?) order by u.NetID ASC";
			ps = conn.prepareStatement(query);
			ps.setString(1, GroupMemberBean.ACTIVE);
			for (int i=0; i < groupids.length; i++) {
				ps.setLong(2+i, groupids[i]);
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new UserPK(rs.getString("NetID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException (e.getMessage());
		}
		return result;
	}
	
	public Collection findByGroupIDStatus(long groupID, String status) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select u.NetID from tUser u, tGroupMembers g where " +
					"u.NetID = g.NetID and g.Status = ? and g.GroupID = ? order by u.NetID ASC";
			ps = conn.prepareStatement(query);
			ps.setString(1, status);
			ps.setLong(2, groupID);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new UserPK(rs.getString("NetID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException (e.getMessage());
		}
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.UserDAO#findAll()
	 */
	public Collection findAll() throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select NetID from tUser";
			ps = conn.prepareStatement(queryString);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new UserPK(rs.getString(1)));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}

		return result;
	}
	
	
	
    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.UserDAO#findActiveStudentsWithoutCUID()
     */
    public Collection findActiveStudentsWithoutCUID() throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT DISTINCT TOP 50 u.NetID FROM tUser u INNER JOIN " +
	        		"tStudent s ON u.NetID = s.NetID " +
	        		"WHERE (s.Status = ?) AND (u.CUID IS NULL OR u.CUID = ?) ORDER BY u.NetID";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, StudentBean.ENROLLED);
			ps.setInt(2, 0);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new UserPK(rs.getString("NetID")));
			}
			conn.close();
			ps.close();
			rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}

		return result;
    }
}
