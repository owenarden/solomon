/*
 * Created on Mar 8, 2004
 */

package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.Collection;

import javax.ejb.EntityBean;
import javax.ejb.FinderException;

/**
 * @ejb.bean name="Group"
 *	jndi-name="GroupBean"
 *	type="BMP" 
 *  
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.GroupDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.GroupDAOImpl"
 * 
 * @ejb.ejb-ref ejb-name="GroupMember" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="GroupMember" jndi-name="GroupMember"
 *
 * @ejb.ejb-ref ejb-name="SubmittedFile" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="SubmittedFile" jndi-name="SubmittedFile"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 * 
 **/
public abstract class GroupBean implements EntityBean {
    
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long assignmentID;
	private long groupID;
	private Timestamp latestSubmission;
	private Timestamp extension;
	private int fileCounter;               //count both submissions and comment files
	private int remainingSubmissions;
	private Long timeSlotID;
	private String adjustment;

	private GroupMemberLocalHome groupMemberHome;
	private SubmittedFileLocalHome submittedFileHome;
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The assignmentID
	 */
	public long getAssignmentID() {
		return assignmentID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param assignmentID The assignmentID
	 */
	public void setAssignmentID(long assignmentID) {
		this.assignmentID = assignmentID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param timeslotID The timeslotID
	 */
	public void setTimeSlotID(Long timeslotID) {
		this.timeSlotID = timeslotID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The assignmentID
	 */
	public Long getTimeSlotID() {
		return timeSlotID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The groupID
	 */
	public long getGroupID() {
		return groupID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param groupID The groupID
	 */
	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The date of the last submission
	 */
	public Timestamp getLatestSubmission() {
		return latestSubmission;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param latest The date of the last submission
	 */
	public void setLatestSubmission(Timestamp latest) {
		this.latestSubmission = latest;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The date the extension granted this group is due (if any)
	 */
	public Timestamp getExtension() {
		return extension;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param extension The date the extension granted this group is due (if any)
	 */
	public void setExtension(Timestamp extension) {
		this.extension = extension;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public int getFileCounter() {
		return fileCounter;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param fileCounter
	 */
	public void setFileCounter(int fileCounter) {
		this.fileCounter = fileCounter;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getAdjustment() {
		return adjustment;
	}
	
	/**
	 * @param adjustment
	 * @ejb.interface-method view-type="local"
	 */
	public void setAdjustment(String adjustment) {
		this.adjustment = adjustment;
	}
	
	/**
	 * @return
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public int getRemainingSubmissions() {
		return remainingSubmissions;
	}
	
	/**
	 * @param remainingSubmissions
	 * @ejb.interface-method view-type="local"
	 */
	public void setRemainingSubmissions(int remainingSubmissions) {
		this.remainingSubmissions = remainingSubmissions;
	}
	
	/**
	 * Checks to see if the given primary key can be found in the database
	 * 
	 * @param pk
	 *            The primary key being serarched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public GroupPK ejbFindByPrimaryKey(GroupPK key) throws FinderException {
		return null;
	}

	/**
	 * Finds all groups in the assignment which have at least one
	 * enrolled, active group member
	 */
	public Collection ejbFindByAssignmentID(long assignmentID)
			throws FinderException {
		return null;
	}
	
	/**
	 * Finds all groups in an assignment which have submitted
	 * files after the late deadline
	 */
	public Collection ejbFindLateByAssignmentID(long assignmentID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all groups in the given timeslot
	 */
	public Collection ejbFindByTimeSlotID(long timeSlotID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds the information for a groupID
	 * 
	 * @param groupID
	 *            The groupID
	 * @return The single-entry collection with the group object
	 * @throws FinderException
	 */
	public GroupPK ejbFindByGroupID(long groupID) throws FinderException {
		return null;
	}
	
	public Collection ejbFindByGroupIDs(Collection groupIDs) throws FinderException {
	    return null;
	}

	/**
	 * Finds all the groups a given student is a member of (including invites)
	 * in a given course
	 * 
	 * @param netID
	 *            The student's netID
	 * @param courseID
	 *            The courseID
	 * @return The collection of groups
	 * @throws FinderException
	 */
	public Collection ejbFindByNetIDCourseID(String netID, long courseID)
			throws FinderException {
		return null;
	}

	/**
	 * Find the group a given student is an active member in a given
	 * assignment
	 * 
	 * @param netID
	 *            The student's netID
	 * @param assignmentID
	 *            The assignmentID
	 * @return The Group corresponding to this student
	 * @throws FinderException
	 */
	public GroupPK ejbFindByNetIDAssignmentID(String netID, long assignmentID)
			throws FinderException {
		return null;
	}
	
	/**
	 * Finds all the groups for the given students in a given assignment
	 * @param netIDs A collection of NetIDs
	 * @param assignmentID the assignmentID of the assignmnet
	 * @return The collection of Groups
	 * @throws FinderException
	 */
	public Collection ejbFindByNetIDsAssignmentID(Collection netIDs, long assignmentID) throws FinderException {
		return null;
	}

	/**
	 * Finds all the groups a given student is invited to join in a given
	 * assignment (or has just rejected joining)
	 * 
	 * @param netID
	 *            The student's netID
	 * @param assignmentID
	 *            The assignmentID
	 * @return The collection of invited-to groups
	 * @throws FinderException
	 */
	public Collection ejbFindInvitedByNetIDAssignmentID(String netID,
			long assignmentID) throws FinderException {
		return null;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 **/
	public GroupData getGroupData() {
		return new GroupData(getAssignmentID(), getTimeSlotID(), getGroupID(),
				getLatestSubmission(), getExtension(), getFileCounter(), getAdjustment(),
				getRemainingSubmissions());
	}

	/**
	 * Creates a new group for the given course and assignment
	 * @param courseid
	 * @param assignmentid
	 * @return The group object
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local" 
	 **/
	public GroupPK ejbCreate(long assignmentid, int remainingSubmissions)
			throws javax.ejb.CreateException {
		setAssignmentID(assignmentid);
		setTimeSlotID(null);
		setRemainingSubmissions(remainingSubmissions);
		setExtension(null);
		setFileCounter(1);
		setLatestSubmission(null);
		setAdjustment(null);
		return null;
	}
}
