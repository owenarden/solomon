package edu.cornell.csuglab.cms.util.category;

/**
 * Represents a category file plus a label string to be displayed for it.
 * Useful as a List entry so files can be iterated through in a "nice" order
 * (ie by index within a cell).
 */
public class FilePlusLabel
{
	public CtgFileInfo file;
	public String label;
	
	public FilePlusLabel(CtgFileInfo file, String label)
	{
		this.file = file;
		this.label = label;
	}
}
