/*
 * Created on Sep 7, 2005
 *
 * 
 */
package edu.cornell.csuglab.cms.util.category;

import edu.cornell.csuglab.cms.base.CategoryColBean;

/**
 * @author evan
 *
 * Provides utilities for adding/editing category contents
 * 
 * TODO is this class used any more?
 */
public class CtgUtil
{
	//all the allowable content types, in a format suitable for database storage
	public static final String
		CTNT_DATE = CategoryColBean.DATE, 
		CTNT_FILE = CategoryColBean.FILE, 
		CTNT_TEXT = CategoryColBean.TEXT, 
		CTNT_URL = CategoryColBean.URL, 
		CTNT_NUMBER = CategoryColBean.NUMBER;
	
	/**
	 * Create a string to be used as a request parameter, using all the given information
	 * @param contentType One of AccessController.P_CONTENT_{DATE, TEXT, etc}
	 * @param ids All useful IDs in order. For an edit of existing contents, this includes
	 * the content ID; for an add of content, it includes row and column IDs; for a change to
	 * or add of a file to a cell, it includes an index within the cell.
	 * @return A String to be used as a request parameter
	 */
	public static String createCtntRequestString(String contentType, long[] ids)
	{
		String req = contentType;
		for(int i = 0; i < ids.length; i++)
			req += "_" + ids[i];
		return req;
	}
}
