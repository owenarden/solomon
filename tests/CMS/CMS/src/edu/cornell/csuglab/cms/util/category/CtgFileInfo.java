/*
 * Created on Feb 15, 2005
 */
package edu.cornell.csuglab.cms.util.category;

/**
 * @author yc263
 *
 * Holds locating data for a file that already exists in the database and on the server.
 * Does not include a label for the file, just the filename.
 */
public class CtgFileInfo {
	private long categoryID;
	private long fileCount; //the index of the file within its cell (a count of files in the cell)
	private String fileName;
	private String filePath; //path except for name (eg "c:/dir1/dir2/"); note it includes the ending slash
	
	public CtgFileInfo(long categoryID, String fileName, long fileCount, String filePath){
		this.categoryID = categoryID;
		this.fileName =  fileName;
		this.fileCount = fileCount;
		this.filePath = filePath;
	}
	
	public long getCategoryID(){
		return this.categoryID;
	}
	
	public String getFileName(){
		return this.fileName;
	}
	
	public long getFileCount(){
		return this.fileCount ;
	}
	
	public String getFilePath(){
		return this.filePath;
	}
	
	/**
	 * Return whether this info represents an actual file or just a placeholder
	 * @return
	 */
	public boolean fileExists()
	{
		return fileName != null && fileName.length() > 0 && filePath != null && filePath.length() > 0;
	}
	
	//for debugging purposes
	public String toString()
	{
		return "[file: " + filePath + "/" + fileName + "]";
	}
}
