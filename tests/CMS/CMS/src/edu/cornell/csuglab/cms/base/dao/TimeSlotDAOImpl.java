/*
 * Created on Feb 20, 2006
 */

package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.StaffBean;
import edu.cornell.csuglab.cms.base.TimeSlotBean;
import edu.cornell.csuglab.cms.base.TimeSlotDAO;
import edu.cornell.csuglab.cms.base.TimeSlotPK;

/**
 * @author Alex Emmet
 */
public class TimeSlotDAOImpl extends DAOMaster implements TimeSlotDAO {


	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.TimeSlotDAO#load(edu.cornell.csuglab.cms.base.TimeSlotPK,
	 *      edu.cornell.csuglab.cms.base.TimeSlotBean)
	 */
	public void load(TimeSlotPK pk, TimeSlotBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tTimeSlot where TimeSlotID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getTimeSlotID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setTimeSlotID(rs.getLong("TimeSlotID"));
				ejb.setAssignmentID(rs.getLong("AssignmentID"));
				String name = rs.getString("TimeSlotName");
				if (rs.wasNull()) {
				    ejb.setName(null);
				} else {
				    ejb.setName(new String(name));
				}
				ejb.setStaff(rs.getString("StaffNetID"));
				String location = rs.getString("Location");
				if (rs.wasNull()) {
				    ejb.setLocation(null);
				} else {
				    ejb.setLocation(new String(location));
				}
				ejb.setStartTime(rs.getTimestamp("StartTime"));
				ejb.setPopulation(rs.getInt("Population"));
				ejb.setHidden(rs.getBoolean("Hidden"));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.TimeSlotDAO#store(edu.cornell.csuglab.cms.base.TimeSlotBean)
	 */
	public void store(TimeSlotBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "update tTimeSlot "
					+ "set TimeSlotName = ?, Location = ?, StaffNetID = ?, StartTime = ?, Population = ?, "
					+ "Hidden = ? where TimeSlotID = ?";
			ps = conn.prepareStatement(update);
			ps.setString(1, ejb.getName());
			ps.setString(2, ejb.getLocation());
			ps.setString(3, ejb.getStaff());
			ps.setTimestamp(4, ejb.getStartTime());
			ps.setInt(5, ejb.getPopulation());
			ps.setBoolean(6, ejb.getHidden());
			ps.setLong(7, ejb.getTimeSlotID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.TimeSlotDAO#remove(edu.cornell.csuglab.cms.base.TimeSlotPK)
	 */
	public void remove(TimeSlotPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.TimeSlotDAO#create(edu.cornell.csuglab.cms.base.TimeSlotBean)
	 */
	public TimeSlotPK create(TimeSlotBean ejb) throws CreateException,
			EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		TimeSlotPK pk = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "insert into tTimeSlot "
					+ "(assignmentid, staffnetid, starttime, timeslotname, location, population, hidden) "
					+ "values (?,?,?,?,?,?,?)";
			String findString = "select @@identity as 'TimeSlotID' from tTimeSlot";
			ps = conn.prepareStatement(createString);
			ps.setLong(1, ejb.getAssignmentID());
			ps.setString(2, ejb.getStaff());
			ps.setTimestamp(3, ejb.getStartTime());
			ps.setString(4, ejb.getName());
			ps.setString(5, ejb.getLocation());
			ps.setInt(6, ejb.getPopulation());
			ps.setBoolean(7, ejb.getHidden());
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findString).executeQuery();
			if (count == 1 && rs.next()) {
				pk = new TimeSlotPK(rs.getLong("TimeSlotID"));
				ejb.setTimeSlotID(pk.getTimeSlotID());
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.TimeSlotDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.TimeSlotPK)
	 */
	public TimeSlotPK findByPrimaryKey(TimeSlotPK pk)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select TimeSlotID from tTimeSlot where TimeSlotID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, pk.getTimeSlotID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find timeslot with ID = " + pk.getTimeSlotID());
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.TimeSlotDAO#findByTimeSlotID(long)
	 */
	public TimeSlotPK findByTimeSlotID(long TimeSlotID)
			throws FinderException {
		return findByPrimaryKey(new TimeSlotPK(TimeSlotID));
	}
	
	/* (non-Javadoc)
	 * Find the timeslotID for a given groupid
	 * @see edu.cornell.csuglab.cms.base.TimeSlotDAO#findByGroupID(long)
	 */
	public TimeSlotPK findByGroupID(long groupID) throws FinderException {
	    Connection conn = null;
	    PreparedStatement ps = null;
		ResultSet rs = null;
		TimeSlotPK pk;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select TimeSlotID from tGroups where GroupID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, groupID);
			rs = ps.executeQuery();
			rs.next();
			pk = new TimeSlotPK(rs.getLong("TimeSlotID"));
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}
	
	/* (non-Javadoc)
	 * Find all TimeSlots for a given course
	 * @see edu.cornell.csuglab.cms.base.TimeSlotDAO#findAllByAssignmentID(long)
	 */
	public Collection findAllByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select TimeSlotID from tTimeSlot where AssignmentID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				TimeSlotPK pk = new TimeSlotPK(rs.getLong("TimeSlotID"));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * Find all TimeSlots for a give course admin for a given semester
	 * @see edu.cornell.csuglab.cms.base.TimeSlotDAO#findByCourseAdmin(long, String)
	 */
	public Collection findByCourseAdmin(long semesterID, String netID) throws FinderException {
    	Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try {
        	conn = jdbcFactory.getConnection();
        	String query = "select TimeSlotID from tTimeSlot ts inner join tAssignment a " +
					"on ts.AssignmentID = a.AssignmentID inner join tCourse c " + 
					"on c.CourseID = a.CourseID inner join tStaff s " + 
					"on s.CourseID = c.CourseID where " +
					"s.AdminPriv = ? and s.Status = ? and ts.Hidden = ? and c.SemesterID = ? and s.NetID = ? " +
					"order by c.Code ASC";
        	ps = conn.prepareStatement(query);
        	ps.setBoolean(1, true);
        	ps.setString(2, StaffBean.ACTIVE);
        	ps.setBoolean(3, false);
        	ps.setLong(4, semesterID);
        	ps.setString(5, netID);
        	rs = ps.executeQuery();
        	while(rs.next()) {
        		result.add(new TimeSlotPK(rs.getLong("TimeSlotID")));
        	}
        	ps.close();
        	rs.close();
        	conn.close();
  		} catch (Exception e) {
        	try {
        		if (rs != null) rs.close();
        		if (ps != null) ps.close();
        		if (conn != null) conn.close();
        	}
        	catch (Exception f) {}
        	throw new FinderException(e.getMessage());
  		}
        return result;
	}

	/* Unused as of Feb 2008 - Alex 
	 * 
	 * (non-Javadoc)
	 * Find all TimeSlots for a current semester
	 * @see edu.cornell.csuglab.cms.base.TimeSlotDAO#findBySemesterID(long)
	 *
	public Collection findBySemesterID(long semesterID) throws FinderException {
    	Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try {
        	conn = jdbcFactory.getConnection();
        	String query = "select TimeSlotID from tTimeSlot t inner join " +
        			"tAssignment a on t.AssignmentID = a.AssignmentID inner join " +
					"tCourse c on c.CourseID = a.CourseID where " + 
        			"t.Hidden = ? and c.SemesterID = ? order by c.Code ASC";
        	ps = conn.prepareStatement(query);
        	ps.setBoolean(1, false);
        	ps.setLong(2, semesterID);
        	rs = ps.executeQuery();
        	while(rs.next()) {
        		result.add(new TimeSlotPK(rs.getLong("TimeSlotID")));
        	}
        	ps.close();
        	rs.close();
        	conn.close();
  		} catch (Exception e) {
        	try {
        		if (rs != null) rs.close();
        		if (ps != null) ps.close();
        		if (conn != null) conn.close();
        	}
        	catch (Exception f) {}
        	throw new FinderException(e.getMessage());
  		}
        return result;
	}*/

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.TimeSlotDAO#findHiddenByAssignmentID(long)
     */
    public Collection findHiddenByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select TimeSlotID from tTimeSlot where AssignmentID = ? and Hidden = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentID);
			ps.setBoolean(2, true);
			rs = ps.executeQuery();
			while (rs.next()) {
				TimeSlotPK pk = new TimeSlotPK(rs.getLong("TimeSlotID"));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
    }
    
	public Collection findByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select TimeSlotID from tTimeSlot where AssignmentID = ? and Hidden = ? order by StartTime";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentID);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				TimeSlotPK pk = new TimeSlotPK(rs.getLong("TimeSlotID"));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	public Collection findConflictingByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT DISTINCT t.TimeSlotID FROM tTimeSlot t INNER JOIN " +
                      "(SELECT t1.TimeSlotID as ID1, t2.TimeSlotID as ID2 FROM tTimeSlot t1 INNER JOIN " +
                      "tTimeSlot t2 ON t1.StaffNetID = t2.StaffNetID AND t1.AssignmentID = t2.AssignmentID " +
					  "AND t1.StartTime <= t2.StartTime AND t1.TimeSlotID <> t2.TimeSlotID INNER JOIN " +
                      "tAssignment a ON t1.AssignmentID = a.AssignmentID AND t2.StartTime < DATEADD(mi, a.Duration, t1.StartTime) " +
					  "WHERE (a.AssignmentID = ?) AND (t1.Hidden = ?) AND (t2.Hidden = ?)) as c " +
					  "ON t.TimeSlotID = c.ID1 or t.TimeSlotID = c.ID2";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentID);
			ps.setBoolean(2, false);
			ps.setBoolean(3, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				TimeSlotPK pk = new TimeSlotPK(rs.getLong("TimeSlotID"));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
}

