/*
 * Created on Sep 29, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package edu.cornell.csuglab.cms.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;

/**
 * @author Yan
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class SubProblemOptions {
	
	/* Choice Info */
	Hashtable choices = new Hashtable();
	Hashtable newChoices= new Hashtable();
	Collection removedChoices = new ArrayList();
	Collection restoredChoices = new ArrayList();
		
	public void addChoiceLetter(String choiceLetter, long choiceID) {
		Long key = new Long(choiceID);
		if (choices.containsKey(key)) {
			ChoiceHelper choice = (ChoiceHelper) choices.get(key);
			choice.letter = choiceLetter;
		}
		else {
			ChoiceHelper choice = new ChoiceHelper();
			choice.letter = choiceLetter;
			choices.put(key, choice);
		}
	}
	
	public void addNewChoiceLetter(String choiceLetter, int ID) {
		Integer key = new Integer(ID);
		if (newChoices.containsKey(key)) {
			ChoiceHelper choice = (ChoiceHelper) newChoices.get(key);
			choice.letter = choiceLetter;
		}
		else {
			ChoiceHelper choice = new ChoiceHelper();
			choice.letter = choiceLetter;
			newChoices.put(key, choice);
		}
	}
	
	public void addChoiceText(String text, long choiceID) {
		Long key = new Long(choiceID);
		if (choices.containsKey(key)) {
			ChoiceHelper choice= (ChoiceHelper) choices.get(key);
			choice.text = text;
		}
		else {
			ChoiceHelper choice = new ChoiceHelper();
			choice.text = text;
			choices.put(key, choice);
		}
	}
	
	public void addNewChoiceText(String text, int ID) {
		Integer key = new Integer(ID);
		if (newChoices.containsKey(key)) {
			ChoiceHelper choice = (ChoiceHelper) newChoices.get(key);
			choice.text = text;
		}
		else {
			ChoiceHelper choice = new ChoiceHelper();
			choice.text = text;
			newChoices.put(key, choice);
		}
	}
	
	
	public void removeChoice(long choiceID) {
		removedChoices.add(new Long(choiceID));
	}
	
	public void restoreChoice(long choiceID) {
		restoredChoices.add(new Long(choiceID));
	}
	
	/*
	 * Returns a collection of Longs representing ChoiceblemIDs of the
	 * choices from this assignment.
	 */
	public Collection getChoiceIDs() {
		ArrayList result = new ArrayList();
		Enumeration keys = choices.keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			ChoiceHelper choice = (ChoiceHelper) choices.get(key);
			result.add(key);
		}
		return result;
	}
	
	/*
	 * Returns a collection of Integers which represents the IDs used
	 * to store new Choices which need to be added to the assignment.
	 */
	public Collection getNewChoiceIDs() {
		LinkedList result = new LinkedList();
		Enumeration keys = newChoices.keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			ChoiceHelper choice = (ChoiceHelper) newChoices.get(key);
			result.addFirst(key);
		}
		return result;
	}
	
	public String getChoiceLetterByID(long choiceID) {
		ChoiceHelper choice = (ChoiceHelper) choices.get(new Long(choiceID));
		return choice.letter;
	}
	
	public String getChoiceTextByID(long choiceID) {
		ChoiceHelper choice = (ChoiceHelper) choices.get(new Long(choiceID));
		return choice.text;
	}
	
	public String getNewChoiceLetterByID(int ID) {
		ChoiceHelper choice = (ChoiceHelper) newChoices.get(new Integer(ID));
		return choice.letter;
	}
	
	public String getNewChoiceTextByID(int ID) {
		ChoiceHelper choice = (ChoiceHelper) newChoices.get(new Integer(ID));
		return choice.text;
	}
		
	public Collection getRemovedChoices() {
		return removedChoices;
	}
	
	public Collection getRestoredChoices() {
		return restoredChoices;
	}
	
}

class ChoiceHelper {
	String letter = null;
	String text = null;
}
