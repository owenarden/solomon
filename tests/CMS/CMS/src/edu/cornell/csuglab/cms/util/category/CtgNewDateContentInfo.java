/*
 * Created on Aug 31, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.util.category;

import java.sql.Timestamp;
import java.util.ArrayList;


/**
 * @author evan
 *
 * Holds info for one category content of type date
 */
public class CtgNewDateContentInfo extends CtgNewContentInfo
{
	private Timestamp date;
	
	public CtgNewDateContentInfo(long rowID, long colID, Timestamp date)
	{
		super(rowID, colID);
		this.date = date;
	}
	
	/*
	 * CtgNewContentInfo functions. A null return value signifies no data.
	 */
	
	public String getColType()
	{
		return CtgUtil.CTNT_DATE;
	}
	
	public Timestamp getDate()       //used by contents of type date
	{
		return date;
	}
	
	public String getText()          //used by contents of type text
	{
		return null;
	}
	
	public Long getNumber()          //used by contents of type number
	{
		return null;
	}
	
	public String getURLAddress()    //used by contents of type url
	{
		return null;
	}
	
	public String getURLLabel()      //used by contents of type url
	{
		return null;
	}
	
	public ArrayList getFileInfos()  //used by contents of type file
	{
		return null;
	}
	
	public ArrayList getFileLabels() //used by contents of type file
	{
		return null;
	}
}
