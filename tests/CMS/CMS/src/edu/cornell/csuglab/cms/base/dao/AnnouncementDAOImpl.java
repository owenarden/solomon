/*
 * Created on Mar 18, 2004
 */

package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.AnnouncementBean;
import edu.cornell.csuglab.cms.base.AnnouncementDAO;
import edu.cornell.csuglab.cms.base.AnnouncementPK;
import edu.cornell.csuglab.cms.base.StudentBean;

/**
 * @author Theodore Chao
 */
public class AnnouncementDAOImpl extends DAOMaster implements AnnouncementDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AnnouncementDAO#load(edu.cornell.csuglab.cms.base.AnnouncementPK,
	 *      edu.cornell.csuglab.cms.base.AnnouncementBean)
	 */
	public void load(AnnouncementPK pk, AnnouncementBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tannouncement where announcementid = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getAnnouncementID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setAnnouncementID(rs.getLong("AnnouncementID"));
				ejb.setCourseID(rs.getLong("CourseID"));
				ejb.setAuthor(rs.getString("Author"));
				ejb.setPostedDate(rs.getTimestamp("PostDate"));
				try {
					ejb.setText(rs.getString("Text"));
				} catch (SQLException e) {
					ejb.setText("");
				}
				ejb.setHidden(rs.getBoolean("Hidden"));
				ejb.setEditInfo(rs.getString("editInfo"));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AnnouncementDAO#store(edu.cornell.csuglab.cms.base.AnnouncementBean)
	 */
	public void store(AnnouncementBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "update tannouncement "
					+ "set author = ?, postdate = ?, text = ?, editInfo=?, Hidden = ? "
					+ "where announcementid = ? and courseid = ?";
			ps = conn.prepareStatement(update);
			if(ejb.getAuthor() == null) ps.setNull(1, java.sql.Types.VARCHAR);
			else ps.setString(1, ejb.getAuthor());
			ps.setTimestamp(2, ejb.getPostedDate());
			if(ejb.getText() == null) ps.setNull(3, java.sql.Types.VARCHAR);
			else ps.setString(3, ejb.getText());
			if(ejb.getEditInfo() == null) ps.setNull(4, java.sql.Types.VARCHAR);
			else ps.setString(4, ejb.getEditInfo());
			ps.setBoolean(5, ejb.getHidden());
			ps.setLong(6, ejb.getAnnouncementID());
			ps.setLong(7, ejb.getCourseID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AnnouncementDAO#remove(edu.cornell.csuglab.cms.base.AnnouncementPK)
	 */
	public void remove(AnnouncementPK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AnnouncementDAO#create(edu.cornell.csuglab.cms.base.AnnouncementBean)
	 */
	public AnnouncementPK create(AnnouncementBean ejb) throws CreateException,
			EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AnnouncementPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "insert into tannouncement "
					+ "(courseid, author, postdate, text, editInfo) values "
					+ "(?,?,?,?,?)";
			String findString = "select @@identity as 'announcementid' from tannouncement";
			ps = conn.prepareStatement(createString);
			ps.setLong(1, ejb.getCourseID());
			if(ejb.getAuthor() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getAuthor());
			ps.setTimestamp(3, ejb.getPostedDate());
			if(ejb.getText() == null) ps.setNull(4, java.sql.Types.VARCHAR);
			else ps.setString(4, ejb.getText());
			if(ejb.getEditInfo() == null) ps.setNull(5, java.sql.Types.VARCHAR);
			else ps.setString(5, ejb.getEditInfo());
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findString).executeQuery();
			if ((count == 1) && rs.next()) {
				result = new AnnouncementPK(rs.getLong(1));
				ejb.setAnnouncementID(result.getAnnouncementID());
			} else {
				throw new CreateException("Failed to create new row in the database");
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AnnouncementDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.AnnouncementPK)
	 */
	public AnnouncementPK findByPrimaryKey(AnnouncementPK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select AnnouncementID from tannouncement where AnnouncementID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, pk.getAnnouncementID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find announcement with ID = " + pk.getAnnouncementID());
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AnnouncementDAO#findByCourseID(long)
	 */
	public Collection findByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT AnnouncementID from tannouncement "
								+ "WHERE courseID = ? and hidden = ? "
								+ "ORDER BY postdate DESC";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				AnnouncementPK pk = new AnnouncementPK(rs.getLong(1));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	public Collection findHiddenByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT AnnouncementID from tannouncement "
								+ "WHERE courseID = ? and hidden = ? "
								+ "ORDER BY postdate DESC";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, courseID);
			ps.setBoolean(2, true);
			rs = ps.executeQuery();
			while (rs.next()) {
				AnnouncementPK pk = new AnnouncementPK(rs.getLong(1));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.AnnouncementDAO#findByCourseDate(long,
	 *      java.sql.Date)
	 */
	public Collection findByNetIDDate(String netID, Timestamp fromDate) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT a.AnnouncementID from tannouncement a inner join "
			    	+ "tCourse c on a.CourseID = c.CourseID inner join "
			    	+ "tSystemProperties p on p.CurrentSemester = c.SemesterID inner join "
					+ "tstudent s on a.courseid = s.courseid "
					+ "WHERE s.netid = ? and a.postdate > ? and a.hidden = ? and p.ID = ? and s.Status = ? "
					+ "ORDER BY postdate DESC";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netID);
			ps.setTimestamp(2, fromDate);
			ps.setBoolean(3, false);
			ps.setInt(4, 1);
			ps.setString(5, StudentBean.ENROLLED);
			rs = ps.executeQuery();
			while (rs.next()) {
				AnnouncementPK pk = new AnnouncementPK(rs.getLong(1));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findByNetIDDateSemester(String netID, Timestamp fromDate, long semesterID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT a.AnnouncementID from tannouncement a inner join "
			    	+ "tCourse c on a.CourseID = c.CourseID inner join "
					+ "tstudent s on a.courseid = s.courseid "
					+ "WHERE s.netid = ? and a.postdate > ? and a.Hidden = ? and c.SemesterID = ? and s.Status = ? "
					+ "ORDER BY postdate DESC";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, netID);
			ps.setTimestamp(2, fromDate);
			ps.setBoolean(3, false);
			ps.setLong(4, semesterID);
			ps.setString(5, StudentBean.ENROLLED);
			rs = ps.executeQuery();
			while (rs.next()) {
				AnnouncementPK pk = new AnnouncementPK(rs.getLong(1));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
}
