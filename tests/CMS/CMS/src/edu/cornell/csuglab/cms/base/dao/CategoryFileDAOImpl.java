/*
 * Created on Dec 7, 2004
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.author.Principal;
import edu.cornell.csuglab.cms.base.CategoryFileBean;
import edu.cornell.csuglab.cms.base.CategoryFileDAO;
import edu.cornell.csuglab.cms.base.CategoryFilePK;
import edu.cornell.csuglab.cms.www.util.FileUtil;

/**
 * @author yc263
 *
 */
public class CategoryFileDAOImpl extends DAOMaster implements CategoryFileDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryFileDAO#load(edu.cornell.csuglab.cms.base.CategoryFilePK, edu.cornell.csuglab.cms.base.CategoryFileBean)
	 */
	public void load(CategoryFilePK pk, CategoryFileBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = jdbcFactory.getConnection();
			String query = "select * from tCategoryFiles where CategoryFileID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getCategoryFileID());
			rs = ps.executeQuery();
			if(rs.next()){
				ejb.setCategoryFileID(rs.getLong("CategoryFileID"));
				ejb.setContentID(rs.getLong("ContentID"));
				ejb.setFileName(rs.getString("FileName"));
				ejb.setHidden(rs.getBoolean("Hidden"));
				ejb.setPath(FileUtil.translateDBPath(rs.getString("Path")));
				ejb.setLinkName(rs.getString("LinkName"));
			}
			else throw new EJBException("Error in CategoryFilesDAOImpl.load");
			rs.close();
			ps.close();
			conn.close();	
		}catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new EJBException("CategoryFile id " + pk.categoryFileID + "not found in tCategoryFile", e);
		}
		}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryFileDAO#store(edu.cornell.csuglab.cms.base.CategoryFileBean)
	 */
	public void store(CategoryFileBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			conn = jdbcFactory.getConnection();
			String query = "UPDATE tCategoryFiles SET ContentID = ?, fileName = ?, hidden = ?, "
				+ "Path = ?, LinkName = ? WHERE CategoryFileID = ? ";
			ps = conn.prepareStatement(query);
			ps.setLong(1, ejb.getContentID());
			if(ejb.getFileName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getFileName());
			ps.setBoolean(3, ejb.getHidden());
			if(ejb.getPath() == null) ps.setNull(4, java.sql.Types.VARCHAR);
			else ps.setString(4, FileUtil.translateSysPath(ejb.getPath()));
			if(ejb.getLinkName() == null) ps.setNull(5, java.sql.Types.VARCHAR);
			else ps.setString(5, ejb.getLinkName());
			ps.setLong(6, ejb.getCategoryFileID());
			ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (Exception e) {
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryFileDAO#remove(edu.cornell.csuglab.cms.base.CategoryFilePK)
	 */
	public void remove(CategoryFilePK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryFileDAO#create(edu.cornell.csuglab.cms.base.CategoryFileBean)
	 */
	public CategoryFilePK create(CategoryFileBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int count;
		CategoryFilePK result = null;
		try{
			conn = jdbcFactory.getConnection();
			String query1 = "INSERT INTO tCategoryFiles " + 
							"(ContentID, FileName, Hidden, Path, LinkName) VALUES " + 
							"(?, ?, ?, ?, ?)";
			String query2 = "SELECT @@identity AS 'CategoryFileID' FROM tCategoryFiles";
			ps = conn.prepareStatement(query1);
			ps.setLong(1, ejb.getContentID());
			if(ejb.getFileName() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getFileName());
			ps.setBoolean(3, ejb.getHidden());
			if(ejb.getPath() == null) ps.setNull(4, java.sql.Types.VARCHAR);
			else ps.setString(4, FileUtil.translateSysPath(ejb.getPath()));
			if(ejb.getLinkName() == null) ps.setNull(5, java.sql.Types.VARCHAR);
			else ps.setString(5, ejb.getLinkName());
			count = ps.executeUpdate();
			if(count == 0 )
				throw new CreateException("Error in creating new category file");
			else{
				rs = (conn.prepareStatement(query2)).executeQuery();
				if(rs.next()){
					result = new CategoryFilePK(rs.getLong("CategoryFileID"));
					ejb.setCategoryFileID(result.getCategoryFileID());
				}
				else
					throw new CreateException("Error in creating new category file");
			}
			if(rs != null) rs.close();
			ps.close();
			conn.close();
		}
		catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new CreateException(e.getMessage());
		}
		return result;		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryFileDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.CategoryFilePK)
	 */
	public CategoryFilePK findByPrimaryKey(CategoryFilePK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = jdbcFactory.getConnection();
			String query = "SELECT CategoryFileID FROM tCategoryFiles "+
					"WHERE CategoryFileID = ? ";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getCategoryFileID());
			rs = ps.executeQuery();
			if(!rs.next())
				throw new FinderException("Couldn't find category file with id "
						+pk.getCategoryFileID()+" in tCategoryFiles");
			rs.close();
			ps.close();
			conn.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryFileDAO#findByCourseID(long)
	 */
	public Collection findByCourseID(long courseID, Principal p) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try{
			conn = jdbcFactory.getConnection();
			String query = "SELECT	Files.CategoryFileID, Files.ContentID " + 
						 	"FROM   tCategoryFiles Files INNER JOIN "+
									"tCtgContents Contents ON Files.ContentID = Contents.ContentID INNER JOIN "+
									"tCategoryRow Row ON Contents.RowID = Row.RowID INNER JOIN "+
									"tCategoryCol Col ON Contents.ColID = Col.ColID INNER JOIN "+
									"tCategori Category ON Row.CategoryID = Category.CategoryID AND Col.CategoryID = Category.CategoryID "+
							"WHERE  (Files.Hidden = 0) AND (Row.Hidden = 0) AND (Col.Hidden = 0) AND (Category.CourseID = ?) "+
									"AND (Category.Authorzn >= ?) AND (Category.Hidden = 0) "+
							"ORDER BY Files.ContentID ";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			ps.setInt(2, p.getAuthoriznLevelByCourseID(courseID));
			rs = ps.executeQuery();
			while(rs.next()){
				long fileID = rs.getLong("CategoryFileID");
				result.add(new CategoryFilePK(fileID));
			}
			rs.close();
			ps.close();
			conn.close();
		}catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	private Collection queryByCategoryID(String query,long categoryID) throws FinderException{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try{
			conn = jdbcFactory.getConnection();
			ps = conn.prepareStatement(query);
			ps.setLong(1, categoryID);
			ps.setLong(2, categoryID);
			rs = ps.executeQuery();
			while(rs.next()){
				long fileID = rs.getLong(1);
				result.add(new CategoryFilePK(fileID));
			}
			rs.close();
			ps.close();
			conn.close();
		}catch(Exception e){
			try{
				e.printStackTrace();
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				if(conn != null) conn.close();
			}catch(Exception f){
				f.printStackTrace();
			}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryFileDAO#findByCategoryID(long)
	 */
	public Collection findByCategoryID(long categoryID) throws FinderException {
		String query = 	"SELECT Files.CategoryFileID, Files.ContentID "+
						"FROM tCategoryFiles Files, tCtgContents Contents, tCategoryCol Col, tCategoryRow Row "+ 
						"WHERE (Row.Hidden = 0) AND (Col.Hidden = 0) AND (Files.Hidden = 0) AND (Files.ContentID = Contents.ContentID) AND"+
							"(Col.CategoryID = ?) AND (Row.CategoryID = ?) AND (Contents.RowID = Row.RowID) AND (Contents.ColID = Col.ColID)"+
						"ORDER BY Files.ContentID ";
		return queryByCategoryID(query, categoryID);
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.CategoryFileDAO#findByCategoryIDAll(long)
	 */
	public Collection findByCategoryIDAll(long categoryID) throws FinderException {
		String query = "SELECT Files.CategoryFileID, Files.ContentID "
			+ "FROM tCategoryFiles Files, tCtgContents Contents, tCategoryCol Col, tCategoryRow Row "
			+ "WHERE (Col.Hidden = 0) AND (Files.ContentID = Contents.ContentID) AND"
			+ "(Col.CategoryID = ?) AND (Row.CategoryID = ?) AND (Contents.RowID = Row.RowID)"
			+ " AND (Contents.ColID = Col.ColID) ORDER BY Files.ContentID";
		return queryByCategoryID(query, categoryID);
	}
}
