package edu.cornell.csuglab.cms.util.category;

import java.util.Collection;
import java.util.Iterator;

import edu.cornell.csuglab.cms.base.CategoryLocal;
import edu.cornell.csuglab.cms.base.CategoryColData;

/**
 * A category data provider that is initialized from a live bean
 * (which means we don't cache data, so if getContentsCollection() is called twice,
 * it will send the database a large query twice)
 * 
 * If we're worried about data corruption during XML page building, we ought to cache
 * data in the constructor here. However, since the worst that can happen if the database
 * does change during extraction of all category info is that a few users get internal
 * server errors, it probably isn't worth the extra work for the system.
 * - Evan, 10 / 16 / 05
 */
public class CategoryBeanDataProvider implements CategoryDataProvider
{
	private CategoryLocal ctg;
	
	public CategoryBeanDataProvider(CategoryLocal ctg)
	{
		this.ctg = ctg;
	}
	
	/**
	 * 
	 * @return The ID of the category we represent
	 */
	public long getCategoryID() 
	{
		return ctg.getCategoryID();
	}
	
	/**
	 * Return a Collection of contents belonging to our category,
	 * limiting search to visible rows and/or columns as desired
	 * @param visibleRows Whether to limit our search to contents whose rows are visible
	 * @param visibleCols Whether to limit our search to contents whose columns are visible
	 * @return A Collection of CategoryContentsDatas
	 */
	public Collection getContentsCollection(boolean visibleRows, boolean visibleCols) 
	{
		return ctg.getContents(visibleCols, visibleRows);
	}
	
	/**
	 * Return a list of all files belonging to our category, ordered by ID of parent content
	 * @return A Collection of CategoryFileDatas
	 */
	public Collection getFilesCollection() 
	{
		return ctg.getContentFiles();
	}
	
	/**
	 * Return a list of all columns belonging to our category, restricting to visible columns if desired,
	 * ordered by position within the category, but excluding removed columns
	 * @param visible Whether to limit our search to visible columns
	 * @return A Collection of CategoryColumnDatas
	 */
	public Collection getNonremovedColumnsCollection(boolean visible)
	{
		return ctg.getColumns(false, visible);
	}
	
	/**
	 * Return a list of all currently removed columns belonging to our category
	 * @return A Collection of CategoryColumnDatas
	 */
	public Collection getRemovedColumnsCollection()
	{
		return ctg.getColumns(true, false); //second parameter doesn't matter if first is true
	}
	
	/**
	 * Return the column data for the nonremoved column with the given id, or null if the ID isn't found in our list
	 * @param colID
	 * @return CategoryColData
	 */
	public CategoryColData findColumnByID(long colID) 
	{
		Collection cols = getNonremovedColumnsCollection(false);
		Iterator i = cols.iterator();
		while(i.hasNext())
		{
			CategoryColData data = (CategoryColData)i.next();
			if(data.getColID() == colID)
				return data;
		}
		return null;
	}
}
