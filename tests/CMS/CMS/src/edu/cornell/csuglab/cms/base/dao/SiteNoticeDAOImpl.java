/*
 * Created on Feb 14, 2007
 */

package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.SiteNoticeBean;
import edu.cornell.csuglab.cms.base.SiteNoticeDAO;
import edu.cornell.csuglab.cms.base.SiteNoticePK;

public class SiteNoticeDAOImpl extends DAOMaster implements SiteNoticeDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.SiteNoticeDAO#load(edu.cornell.csuglab.cms.base.SiteNoticePK,
	 *      edu.cornell.csuglab.cms.base.SiteNoticeBean)
	 */
	public void load(SiteNoticePK pk, SiteNoticeBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tsitenotice where noticeid = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getNoticeID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setNoticeID(rs.getLong("NoticeID"));
				ejb.setAuthor(rs.getString("Author"));
				ejb.setPostedDate(rs.getTimestamp("PostDate"));
				ejb.setExpireDate(rs.getTimestamp("ExpDate"));
				try {
					ejb.setText(rs.getString("text"));
				} catch (SQLException e) {
					ejb.setText("");
				}
				ejb.setHidden(rs.getBoolean("Hidden"));
				ejb.setDeleted(rs.getBoolean("Deleted"));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
			}
			throw new EJBException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.SiteNoticeDAO#store(edu.cornell.csuglab.cms.base.SiteNoticeBean)
	 */
	public void store(SiteNoticeBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "update tsitenotice "
					+ "set author = ?, postdate = ?, text = ?, expdate = ?, hidden = ?, deleted = ? "
					+ "where noticeid = ? ";
			ps = conn.prepareStatement(update);
			ps.setString(1, ejb.getAuthor());
			ps.setTimestamp(2, ejb.getPostedDate());
			ps.setString(3, ejb.getText());
			if (ejb.getExpireDate() == null)
				ps.setNull(4, java.sql.Types.TIMESTAMP);
			else
				ps.setTimestamp(4, ejb.getExpireDate());
			ps.setBoolean(5, ejb.getHidden());
			ps.setBoolean(6, ejb.getDeleted());
			ps.setLong(7, ejb.getNoticeID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (Exception e) {
			try {
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
			}
			throw new EJBException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.SiteNoticeDAO#remove(edu.cornell.csuglab.cms.base.SiteNoticePK)
	 */
	public void remove(SiteNoticePK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.SiteNoticeDAO#create(edu.cornell.csuglab.cms.base.SiteNoticeBean)
	 */
	public SiteNoticePK create(SiteNoticeBean ejb) throws CreateException,
			EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		SiteNoticePK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "insert into tsitenotice "
					+ "(author, postdate, expdate, text, hidden, deleted) values "
					+ "(?,?,?,?,?,?)";
			String findString = "select @@identity as 'noticeid' from tsitenotice";
			ps = conn.prepareStatement(createString);
			if (ejb.getAuthor() == null)
				ps.setNull(1, java.sql.Types.VARCHAR);
			else
				ps.setString(1, ejb.getAuthor());
			ps.setTimestamp(2, ejb.getPostedDate());
			if (ejb.getExpireDate() == null)
				ps.setNull(3, java.sql.Types.TIMESTAMP);
			else
				ps.setTimestamp(3, ejb.getExpireDate());
			if (ejb.getText() == null)
				ps.setNull(4, java.sql.Types.VARCHAR);
			else
				ps.setString(4, ejb.getText());
			ps.setBoolean(5, ejb.getHidden());
			ps.setBoolean(6, ejb.getDeleted());
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findString).executeQuery();
			if ((count == 1) && rs.next()) {
				result = new SiteNoticePK(rs.getLong(1));
				ejb.setNoticeID(result.getNoticeID());
			} else {
				throw new CreateException(
						"Failed to create new row in the database");
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
			}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.SiteNoticeDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.SiteNoticePK)
	 */
	public SiteNoticePK findByPrimaryKey(SiteNoticePK pk)
			throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select noticeid from tsitenotice where noticeid = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, pk.getNoticeID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException(
						"Could not find site notice with ID = "
								+ pk.getNoticeID());
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
			}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.SiteNoticeDAO#findCurrentShowing(edu.cornell.csuglab.cms.base.SiteNoticePK)
	 */
	public Collection findCurrentShowing() throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();

			Timestamp now = new Timestamp(System.currentTimeMillis());

			String query = "SELECT noticeid from tsitenotice "
					+ "WHERE hidden = ? and deleted = ? and (expdate is null or expdate > ?) "
					+ "order by postdate desc";
			ps = conn.prepareStatement(query);
			ps.setBoolean(1, false);
			ps.setBoolean(2, false);
			ps.setTimestamp(3, now);
			rs = ps.executeQuery();

			while (rs.next()) {
				long noticeid = rs.getLong("noticeid");
				result.add(new SiteNoticePK(noticeid));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
			}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.SiteNoticeDAO#findAllLiving(edu.cornell.csuglab.cms.base.SiteNoticePK)
	 */
	public Collection findAllLiving() throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT noticeid from tsitenotice "
					+ "WHERE deleted = ? order by postdate desc";
			ps = conn.prepareStatement(query);
			ps.setBoolean(1, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				long noticeid = rs.getLong("noticeid");
				result.add(new SiteNoticePK(noticeid));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
			}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.SiteNoticeDAO#findDeleted(edu.cornell.csuglab.cms.base.SiteNoticePK)
	 */
	public Collection findDeleted() throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT noticeid from tsitenotice "
					+ "WHERE deleted = ? order by postdate desc";
			ps = conn.prepareStatement(query);
			ps.setBoolean(1, true);
			rs = ps.executeQuery();

			while (rs.next()) {
				long noticeid = rs.getLong("noticeid");
				result.add(new SiteNoticePK(noticeid));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception f) {
			}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
}
