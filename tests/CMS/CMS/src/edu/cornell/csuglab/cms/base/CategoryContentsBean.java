/*
 * Created on Oct 7, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.Collection;

import javax.ejb.FinderException;

import edu.cornell.csuglab.cms.author.Principal;


 /**
 * @ejb.bean name="CategoryContents"
 *	jndi-name="CategoryContentsBean"
 *	type="BMP"
 *
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class = "edu.cornell.csuglab.cms.base.CategoryContentsDAO"
 * impl-class = "edu.cornell.csuglab.cms.base.dao.CategoryContentsDAOImpl"
 *  
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/

public abstract class CategoryContentsBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	//identifying data
   private long contentID;
	private long colID;
	private String colType;
	private long rowID;
	//content data (the content can be of one of types date/text/URL/filelist/number)
	private Long number; //if type is number, the number; this is a Long and not a long because it can be empty
	private Timestamp date;
	private String text; //if type is text, text; if type is URL, URL address
	private String linkName; //if type is URL, URL label (displayed name)
	
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the unique identifying long associated with category content
	 */
	public long getContentID(){
		return this.contentID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param id The unique identifying long associated with category content
	 */
	public void setContentID(long id){
		this.contentID = id;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The column id that this content is under
	 */
	public long getColumnID(){
		return this.colID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param colID Sets the column id that this content is under
	 */
	public void setColumnID(long colID){
		this.colID = colID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The name of the content
	 */
	public String getColumnType(){
		return this.colType;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param type
	 */
	public void setColumnType(String type){
		if(CategoryColBean.isLegalColumnType(type))
			this.colType = type;
		else throw new RuntimeException("CategoryContentsBean::setColumnType(): given invalid type '" + type + "'");
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the unique ID of the row this content is associated with
	 */
	public long getRowID(){
		return this.rowID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param rowID the unique ID of the row this content is associated with
	 */
	public void setRowID(long rowID){
		this.rowID = rowID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return the numeric value for this content, or null if there is no value
	 */
	public Long getNumber(){
		return this.number;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param number The numeric value for this content, or null if no value
	 */
	public void setNumber(Long number){
		this.number = number;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The timestamp, if this is a column of type date
	 */
	public Timestamp getDate(){
		return this.date;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param date
	 */
	public void setDate(Timestamp date){
		this.date = date;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The text data, if this content is of type text
	 */
	public String getText(){
		return this.text;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param txt
	 */
	public void setText(String txt){
		this.text = txt;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The URL address, if this content is of type URL
	 */
	public String getLinkName(){
		return this.linkName;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param linkName
	 */
	public void setLinkName(String linkName){
		this.linkName = linkName;
	}
	
	/**
	 * 
	 * @param pk
	 * @return
	 * @throws FinderException
	 */
	public CategoryContentsPK ejbFindByPrimaryKey(CategoryContentsPK pk) throws FinderException{
		return null;
	}
	
	/**
	 * Returns the collection of contents for which associated column and row are NOT hidden
	 * @param categoryID
	 * @param ascending
	 * @param visibleCol Whether we should look only for contents whose columns are visible (true)
	 * or not filter on column visibility (false)
	 * @param visibleRow Whether we should look only for contents whose rows are visible (true)
	 * or not filter on row visibility (false)
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByCategoryID(long categoryID, long sortByColID, boolean ascending, boolean visibleCol, boolean visibleRow) throws FinderException{
		return null;
	}
	
	/**
	 * 
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseID(long courseID, Principal p) throws FinderException{
		return null;
	}
	
	/**
	 * 
	 * @param colID
	 * @param colType
	 * @param rowID
	 * @param date
	 * @param text
	 * @param linkName
	 * @return
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local"
	 */
	public CategoryContentsPK ejbCreate(long colID, String colType, long rowID, Long number, Timestamp date, 
			String text, String linkName)
				throws javax.ejb.CreateException
	{
		this.setColumnID(colID);
		this.setColumnType(colType);
		this.setRowID(rowID);
		this.setNumber(number);
		this.setDate(date);
		this.setText(text);
		this.setLinkName(linkName);
		return null;
	}
	
	
	/**
	 * @ejb.interface-method view-type="local"
	 */
	public CategoryContentsData getCategoryContentsData(){
		return new CategoryContentsData(contentID, colID, colType, rowID, number, date, text, linkName);
	}
}
