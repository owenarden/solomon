/*
 * Created on Mar 4, 2004
 */

package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.EntityBean;
import javax.ejb.FinderException;

/**
 * @ejb.bean name="Student"
 *	jndi-name="StudentBean"
 *	type="BMP" 
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.StudentDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.StudentDAOImpl"
 * 
 * @ejb.ejb-ref ejb-name="Group" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="Group" jndi-name="Group"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/
public abstract class StudentBean implements EntityBean {
    
    /* possible status values */
    public static final String ENROLLED= "Enrolled", DROPPED = "Dropped";
    
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private String userID;
	private long courseID;
	private String finalGrade;
	private String status;
	private Float totalScore;
	private String lecture, lab, section, credits, gradeOption;
	private boolean emailNewAssignment, emailDueDate, emailGroup, 
			emailNewGrade, emailRegrade, emailFile, emailFinalGrade, emailTimeSlot;
	/* for registrar's purposes: students in cs211 could have department COM S
	 * or ENGRD, but tCourse.Code for 211 can only hold COM S 211
	 * (courseNum would be 211 in either case)
	 */
	private String department, courseNum;

	
	private GroupHome groupHome = null;

	/**
	 * @return Returns the courseID.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public long getCourseID() {
		return courseID;
	}
	
	/**
	 * @param courseID The courseID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setCourseID(long courseID) {
		this.courseID = courseID;
	}

	/**
	 * @return Returns the userID.
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 * @param userID The userID to set.
	 * @ejb.interface-method view-type="local"
	 */
	public void setUserID(String userID) {
		this.userID = userID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the final grade (letter grade)
	 */
	public String getFinalGrade() {
		return finalGrade;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param finalGrade The final letter grade to set
	 */
	public void setFinalGrade(String finalGrade) {
		this.finalGrade = finalGrade;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @return The total weighted score for the student.
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 */
	public Float getTotalScore() {
		return totalScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param totalScore The total weighted score to set.
	 */
	public void setTotalScore(Float totalScore) {
		this.totalScore = totalScore;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getLecture() {
	    return lecture;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param lecture
	 */
	public void setLecture(String lecture) {
	    this.lecture = lecture;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getLab() {
	    return lab;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param lecture
	 */
	public void setLab(String lab) {
	    this.lab = lab;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getSection() {
	    return section;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param lecture
	 */
	public void setSection(String section) {
	    this.section = section;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getDepartment() {
	    return department;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param department
	 */
	public void setDepartment(String department) {
	    this.department = department;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getCourseNum() {
	    return courseNum;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param courseNum
	 */
	public void setCourseNum(String courseNum) {
	    this.courseNum = courseNum;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getCredits() {
	    return credits;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param lecture
	 * @ejb.transaction type="Supports" 
	 */
	public void setCredits(String credits) {
	    this.credits = credits;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getGradeOption() {
	    return gradeOption;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param lecture
	 */
	public void setGradeOption(String gradeOption) {
	    this.gradeOption = gradeOption;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The enrollment status (StudentBean.ENROLLED, StudentBean.DROPPED)
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param status The enrollment status (StudentBean.ENROLLED, StudentBean.DROPPED) to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return True if the student wants to be emailed when a new assignment is opened
	 */
	public boolean getEmailNewAssignment() {
		return emailNewAssignment;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param emailAssignment True if the student wants to be emailed when a new 
	 * 						  assignment is opened
	 */
	public void setEmailNewAssignment(boolean emailNewAssignment) {
		this.emailNewAssignment = emailNewAssignment;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return True if the student wants to be emailed when a due date on an 
	 * 		   open assignment is changed
	 */
	public boolean getEmailDueDate() {
		return emailDueDate;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param emailDueDate True if the student wants to be emailed when a due date on an 
	 * 					   open assignment is changed
	 */
	public void setEmailDueDate(boolean emailDueDate) {
		this.emailDueDate = emailDueDate;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return True if the student wants to be emailed when a group activity happens
	 */
	public boolean getEmailGroup() {
		return emailGroup;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param emailGroup True if the student wants to be emailed when a group 
	 * 					 activity happens
	 */
	public void setEmailGroup(boolean emailGroup) {
		this.emailGroup = emailGroup;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return True if the student wants to be emailed when graded or grade is 
	 * 		   changed
	 */
	public boolean getEmailNewGrade() {
		return emailNewGrade;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param emailGrade True if the student wants to be emailed when graded or 
	 * 					 grade is changed
	 */
	public void setEmailNewGrade(boolean emailNewGrade) {
		this.emailNewGrade = emailNewGrade;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return True if the student wants to be emailed when submitting a regrade or 
	 * 		   regrade is answered.
	 */
	public boolean getEmailRegrade() {
		return emailRegrade;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param emailGrade True if the student wants to be emailed when submitting a 
	 * 					 regrade or regrade is answered.
	 */
	public void setEmailRegrade(boolean emailRegrade) {
		this.emailRegrade = emailRegrade;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return True if the student wants to be emailed after a successful submission
	 */
	public boolean getEmailFile() {
		return emailFile;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param emailFile True if the student wants to be emailed after a successful submission
	 */
	public void setEmailFile(boolean emailFile) {
		this.emailFile = emailFile;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return True if the student wants to be emailed when final grades are released
	 */
	public boolean getEmailFinalGrade() {
		return emailFinalGrade;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param emailFinalGrade True if the student wants to be emailed when final grades 
	 * 						  are released
	 */
	public void setEmailFinalGrade(boolean emailFinalGrade) {
		this.emailFinalGrade = emailFinalGrade;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @ejb.persistence
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public boolean getEmailTimeSlot() {
		return emailTimeSlot;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param emailTimeSlot
	 */
	public void setEmailTimeSlot(boolean emailTimeSlot) {
		this.emailTimeSlot = emailTimeSlot;
	}

	/**
	 * Checks to see if the given primary key can be found in the database
	 * 
	 * @param pk
	 *            The primary key being serarched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public StudentPK ejbFindByPrimaryKey(StudentPK pk) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all students associated with a course, including
	 * those no longer enrolled
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindAllByCourseID(long courseID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all students in courses for which the given netID is a staff member
	 * @param netID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByStaff(String netID) throws FinderException {
		return null;
	}

	
	/**
	 * Finds all the enrolled students in a given course
	 * @param courseID
	 *            The courseID
	 * @return The collection of student objects
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseIDSortByLastName(long courseID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all the enrolled students in a given course
	 * @param courseID
	 *            The courseID
	 * @return The collection of student objects
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseIDSortByNetID(long courseID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds the student bean associated with the given NetID 
	 * @throws FinderException if there is no such student
	 */
	public StudentPK ejbFindByAssignmentIDNetID(long assignID, String netID) throws FinderException {
		return null;
	}
	
	public StudentPK ejbFindByGroupIDNetID(long groupID, String netID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all the enrolled students which are active in groups for which
	 * the given NetID is assigned to grade at least one problem
	 * @param courseID
	 * @param netID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByAssignmentIDAssignedTo(long assignID, String netID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all enrolled students in the given course who have the 
	 * option enabled to receive emails when new assignments are released
	 * @return
	 */
	public Collection ejbFindEmailNewAssigns(long courseID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all enrolled students in the given course who have the 
	 * option enabled to receive emails when the due date on an assignment is changed
	 * @return
	 */
	public Collection ejbFindEmailDueDate(long courseID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all enrolled students in the given groups who have the TimeSlot emailing
	 * option enabled
	 */
	public Collection ejbFindEmailTimeSlots(Collection groupIDs) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all enrolled students in the given course who have the 
	 * option enabled to receive emails when grades for an assignment are released
	 * @return
	 */
	public Collection ejbFindEmailGrades(long courseID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all enrolled students active in a given group who have the 
	 * option enabled to receive email when their group submits a file.
	 * @param groupID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindEmailFileSubmit(long groupID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all enrolled students in the given course who have the 
	 * option enabled to receive email when final grades for the course are released
	 * @return
	 */
	public Collection ejbFindEmailFinalGrades(long courseID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all enrolled students who are active in the given groups and have
	 * requested to receive emails when their regrade requests have been responded to.
	 * @param groupIDs
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindEmailRegrade(Collection groupIDs) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all the students in a given course which have "" for at least one
	 * of their FirstName/LastName fields
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindEmptyNameByCourseID(long courseID) throws FinderException {
		return null;
	}

	/**
	 * Finds all the courses a given user is a student in
	 * 
	 * @param userID
	 *            The user's netID
	 * @return The collection of student objects
	 * @throws FinderException
	 */
	public Collection ejbFindByUserID(String userID) throws FinderException {
		return null;
	}

	/**
	 * Finds if a given user is a student in a given course and returns the
	 * object, or throws a FinderException if the find fails
	 * 
	 * @param netid
	 *            The user's netID.
	 * @param courseid
	 *            The courseID
	 * @return The single student object
	 * @throws FinderException
	 */
	public StudentPK ejbFindByUserCourse(String netid, long courseid)
			throws FinderException {
		return null;
	}

	/**
	 * Finds all students in all courses.
	 * 
	 * @return The collection of ALL student objects
	 */
	public Collection ejbFindAll() throws FinderException {
		return null;
	}

	private GroupHome groupHome() {
		try {
			if (groupHome == null) {
				groupHome = GroupUtil.getHome();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return groupHome;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 **/
	public StudentData getStudentData() {
		return new StudentData(getCourseID(), getUserID(), getFinalGrade(),
				getTotalScore(), getLecture(), getLab(), getSection(), getDepartment(), 
				getCourseNum(), getCredits(), getGradeOption(), getStatus(), 
				getEmailNewAssignment(), getEmailDueDate(), getEmailGroup(), getEmailNewGrade(), 
				getEmailRegrade(), getEmailFile(), getEmailFinalGrade(), getEmailTimeSlot());
	}

	/**
	 * Make a given user a student in a given course. 
	 * TODO: Nothing should happen if the user is already enrolled in the course 
	 * (though the return should still be the correct object).
	 * @param courseid The courseID
	 * @param netid The user's netID
	 * @return The student obj
	 * @throws javax.ejb.CreateException
	 *
	 * @ejb.create-method view-type="local" 
	 **/
	public StudentPK ejbCreate(long courseid, String netid)
			throws javax.ejb.CreateException {
		setUserID(netid);
		setCourseID(courseid);
		setCredits(null);
		setEmailDueDate(false);
		setEmailFile(false);
		setEmailFinalGrade(false);
		setEmailGroup(false);
		setEmailNewAssignment(false);
		setEmailNewGrade(false);
		setEmailRegrade(false);
		setEmailTimeSlot(false);
		setFinalGrade(null);
		setGradeOption(null);
		setLab(null);
		setLecture(null);
		setSection(null);
		setStatus(ENROLLED);
		setTotalScore(null);
		return null;
	}
}
