/*
 * Created on Apr 22, 2005
 */
package edu.cornell.csuglab.cms.base;

import java.util.Collection;

import javax.ejb.FinderException;

/**
 * @ejb.bean name="OldAnnouncement"
 *	jndi-name="OldAnnouncement"
 *	type="BMP"
 *
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.OldAnnouncementDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.OldAnnouncementDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/

public abstract class OldAnnouncementBean implements javax.ejb.EntityBean {
	
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
    private long oldAnnouncementID;
	private long announcementID;
	private String text;
	
	/**
	 * @ejb.pk-field
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getOldAnnouncementID(){
		return this.oldAnnouncementID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param oldAnnouncementID
	 */
	public void setOldAnnouncementID(long oldAnnouncementID){
		this.oldAnnouncementID = oldAnnouncementID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public long getAnnouncementID(){
		return this.announcementID;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param announcementID
	 */
	public void setAnnouncementID(long announcementID){
		this.announcementID = announcementID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public String getText(){
		return this.text;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param text
	 */
	public void setText(String text){
		this.text = text;
	}
	
	/**
	 * Finds history of all the old versions of this announcement
	 * @param announcementID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindByAnnouncementID(long announcementID) throws FinderException{
		return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @return
	 */
	public OldAnnouncementData getOldAnnouncementData(){
		return new OldAnnouncementData(getOldAnnouncementID(),getAnnouncementID(), getText());
	}
	
	/**
	 * Finds the oldAnnouncement associated with this primary key
	 * @param pk
	 * @return
	 * @throws FinderException
	 */
	public OldAnnouncementPK ejbFindByPrimaryKey(OldAnnouncementPK pk)throws FinderException {
		return null;
	}
	
	/**
	 * Creates the history for an announcement
	 * @param announcementID - unique id identifying announcement
	 * @param text - old version of the announcement
	 * @return
	 * @throws javax.ejb.CreateException
	 * 	@ejb.create-method view-type="local"
	 */
	public OldAnnouncementPK ejbCreate(long announcementID, String text) throws javax.ejb.CreateException {
		setAnnouncementID(announcementID);
		setText(text);
		return null;
  }
	
	


}
