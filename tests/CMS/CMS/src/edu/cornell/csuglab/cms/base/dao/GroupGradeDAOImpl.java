/*
 * Created on Mar 16, 2005
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.GroupGradeBean;
import edu.cornell.csuglab.cms.base.GroupGradeDAO;
import edu.cornell.csuglab.cms.base.GroupGradePK;

/**
 * @author Jon
 *
 */
public class GroupGradeDAOImpl extends DAOMaster implements GroupGradeDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupGradeDAO#load(edu.cornell.csuglab.cms.base.GroupGradePK, edu.cornell.csuglab.cms.base.GroupGradeBean)
	 */
	public void load(GroupGradePK pk, GroupGradeBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tGroupGrades where GroupID = ? and SubProblemID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getGroupID());
			ps.setLong(2, pk.getSubProblemID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setGroupID(rs.getLong("GroupID"));
				ejb.setSubProblemID(rs.getLong("SubProblemID"));
				ejb.setScore(rs.getFloat("Score"));
				ejb.setIsAveraged(rs.getBoolean("IsAveraged"));
			} else throw new EJBException("Error loading GroupGradeBean");
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new EJBException(e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupGradeDAO#store(edu.cornell.csuglab.cms.base.GroupGradeBean)
	 */
	public void store(GroupGradeBean ejb) throws EJBException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = jdbcFactory.getConnection();
            String updateString = "update tGroupGrades set Score = ?, isAveraged = ? where "
            		+ "SubProblemID = ? and GroupID = ?";
            ps = conn.prepareStatement(updateString);
            ps.setFloat(1, ejb.getScore());
            ps.setBoolean(2, ejb.getAveraged());
            ps.setLong(3, ejb.getSubProblemID());
            ps.setLong(4, ejb.getGroupID());
            ps.executeUpdate();
            conn.close();
            ps.close();
        } catch (Exception e) {
        	try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupGradeDAO#remove(edu.cornell.csuglab.cms.base.GroupGradePK)
	 */
	public void remove(GroupGradePK pk) throws RemoveException, EJBException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = jdbcFactory.getConnection();
            String updateString = "delete from tGroupGrades "
                    + "where GroupID = ? and SubProblemID = ?";
            ps = conn.prepareStatement(updateString);
            ps.setLong(1, pk.getGroupID());
            ps.setLong(2, pk.getSubProblemID());
            ps.execute();
            conn.close();
            ps.close();
        } catch (Exception e) {
        	try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			RemoveException f = new RemoveException(e.getMessage());
			f.setStackTrace(e.getStackTrace());
			throw f;
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupGradeDAO#create(edu.cornell.csuglab.cms.base.GroupGradeBean)
	 */
	public GroupGradePK create(GroupGradeBean ejb) throws CreateException, EJBException {
        Connection conn = null;
        PreparedStatement ps = null;
        GroupGradePK result = null;
        try {
            conn = jdbcFactory.getConnection();
            String updateString = "insert into tGroupGrades "
                    + "(GroupID, Score, isAveraged, SubProblemID) "
                    + "values (?,?,?,?)";
            ps = conn.prepareStatement(updateString);
            ps.setLong(1, ejb.getGroupID());
            ps.setFloat(2, ejb.getScore());
            ps.setBoolean(3, ejb.getAveraged());
            ps.setLong(4, ejb.getSubProblemID());
            int count = ps.executeUpdate();
            if (count == 1) {
                result = new GroupGradePK(ejb.getGroupID(), ejb.getSubProblemID());
            } else {
            	throw new CreateException("Failed to create a new GroupGrade");
            }
            conn.close();
            ps.close();
        } catch (Exception e) {
        	try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
        return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupGradeDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.GroupGradePK)
	 */
	public GroupGradePK findByPrimaryKey(GroupGradePK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select GroupID,SubProblemID from tGroupGrades where GroupID = ? and SubProblemID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getGroupID());
			ps.setLong(2, pk.getSubProblemID());
			rs = ps.executeQuery();
			if (!rs.next()) 
				throw new EJBException("Error finding PrimaryKey for GroupGradeBean");
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GroupGradeDAO#findByAssignmentID(long)
	 */
	public Collection findByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT g.GroupID, g.SubproblemID FROM tGroupGrades g INNER JOIN " +
				"tGroups gr ON g.GroupID = gr.GroupID WHERE (gr.AssignmentID = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GroupGradePK(rs.getLong("GroupID"), rs.getLong("SubProblemID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}

	public Collection findByGraderAssignmentID(String netID, long assignmentID, boolean adminPriv, int numSubProbs) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String gradesQuery = "SELECT DISTINCT g.GroupID, g.SubproblemID " +
				"FROM tGroupGrades g INNER JOIN " +
				"tGroups gr ON g.GroupID = gr.GroupID LEFT OUTER JOIN " +
				"tGroupAssignedTo ga ON g.GroupID = ga.GroupID INNER JOIN " +
				"tAssignment a ON gr.AssignmentID = a.AssignmentID LEFT OUTER JOIN " +
	            "(SELECT GroupID, COUNT(DISTINCT SubProblemID) AS Assigned " +
	            	"FROM tGroupAssignedTo WHERE (NetID = ?) AND NOT (SubProblemID = ?) " +
	                "GROUP BY GroupID) t ON t.GroupID = g.GroupID " +
	            "WHERE " + (adminPriv ? "gr.AssignmentID = ? " : 
	            "((gr.AssignmentID = ?) AND (a.AssignedGraders = ?) AND (ga.NetID = ?) AND (t.Assigned = ? OR g.SubproblemID = ga.SubProblemID)) OR " +
	            "((gr.AssignmentID = ?) AND (a.AssignedGraders = ?)) ") +
	            "ORDER BY g.GroupID, g.SubproblemID";
			ps = conn.prepareStatement(gradesQuery);
			ps.setString(1, netID);
			ps.setLong(2, 0);
			ps.setLong(3, assignmentID);
			if (!adminPriv) {
				ps.setBoolean(4, true);
				ps.setString(5, netID);
				ps.setInt(6, numSubProbs);
				ps.setLong(7, assignmentID);
				ps.setBoolean(8, false);
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GroupGradePK(rs.getLong("GroupID"), rs.getLong("SubProblemID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
	
}
