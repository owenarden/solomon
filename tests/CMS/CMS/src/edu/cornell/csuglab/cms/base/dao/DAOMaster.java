package edu.cornell.csuglab.cms.base.dao;

import javax.ejb.EntityBean;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import edu.cornell.csuglab.cms.www.util.ResultCache;

/**
 * Basic initialization and database access setup which is the same across
 * all DAOImpl classes.
 * Also constantly monitors and adjusts that part of the Force that is required
 * for proper running of cms.
 * @author jfg32
 */
public abstract class DAOMaster {
    
	protected DataSource jdbcFactory;

	public void init() {
		InitialContext c = null;
		if (null == this.jdbcFactory) {
			try {
				c = new InitialContext();
				this.jdbcFactory = (DataSource) c.lookup("java:/MSSQLDS");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	protected void preCreate(String table, EntityBean ejb) {
		ResultCache.invalidateByTable(table);
	}
	
	protected void preStore(String table, EntityBean ejb) {
		ResultCache.invalidateByTable(table);	
	}
	
	protected void preRemove(String table) {
		ResultCache.invalidateByTable(table);	
	}
	
}
