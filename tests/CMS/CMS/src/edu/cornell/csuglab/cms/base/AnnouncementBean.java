/*
 * Created on Mar 18, 2004
 */

package edu.cornell.csuglab.cms.base;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.EntityBean;
import javax.ejb.FinderException;

/**
 * @ejb.bean name="Announcement"
 *	jndi-name="AnnouncementBean"
 *	type="BMP" 
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.AnnouncementDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.AnnouncementDAOImpl"
 * 
 * @ejb.ejb-ref ejb-name="OldAnnouncement" view-type="remote"
 * @jboss.ejb-ref-jndi ref-name="OldAnnouncement" jndi-name="OldAnnouncement"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/
public abstract class AnnouncementBean implements EntityBean {
    
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long announcementID;
	private long courseID;
	private String author;
	private Timestamp postedDate;
	private String text;
	private String editInfo;
	private boolean hidden;
	private OldAnnouncementLocalHome historyHome = null;

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the announcement's unique identifying long
	 */
	public long getAnnouncementID() {
		return announcementID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param id Sets the announcement's unique identifying long
	 */
	public void setAnnouncementID(long id) {
		this.announcementID = id;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the courseID that this announcement is for
	 */
	public long getCourseID() {
		return courseID;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param courseID Sets the courseID that this announcement is for
	 */
	public void setCourseID(long courseID) {
		this.courseID = courseID;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The author of the announcement (as a name)
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param author The author of the announcement (as a name); 
	 * 				 beginning and ending white-spaces get trimmed
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The date the announcement was posted
	 */
	public Timestamp getPostedDate() {
		return postedDate;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param postedDate The date the announcement was posted
	 */
	public void setPostedDate(Timestamp postedDate) {
		this.postedDate = postedDate;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return The text-body of the announcement
	 */
	public String getText() {
		return text;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 * @param text The text-body of the announcement
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the editInfo.
	 */
	public String getEditInfo() {
		return editInfo;
	}
	/**
	 * @ejb.interface-method view-type="local"
	 * @param editInfo The editInfo to set.
	 */
	public void setEditInfo(String editInfo) {
		this.editInfo = editInfo;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return
	 */
	public boolean getHidden() {
	    return hidden;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param hidden
	 */
	public void setHidden(boolean hidden) {
	    this.hidden = hidden;
	}
	
	/**
	 * Checks to see if the given primary key can be found in the database
	 * @param pk The primary key being searched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public AnnouncementPK ejbFindByPrimaryKey(AnnouncementPK pk)
			throws FinderException {
		return null;
	}

	/**
	 * Finds all non-hidden announcements for a single course
	 * @param courseID The courseID
	 * @return A collection of all the announcements for the course, in
	 *         increasing order of the unique ID, which also corresponds to
	 *         increasing chronological order of posted date.
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseID(long courseID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all hidden announcements for a single course
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindHiddenByCourseID(long courseID) throws FinderException {
	    return null;
	}

	/**
	 * Finds all the non-hidden announcements from a certain date onwards for all the
	 * courses in which the given student is registered (as a student).
	 * 
	 * @param netID
	 *            The student's netID
	 * @param fromDate
	 *            The earliest posted date for which to return results
	 * @return A collection of all the recent announcements for all of the
	 *         student's courses, in increasing order of the unique ID, which
	 *         also corresponds to increasing chronological order of posted
	 *         date. Returns an empty collection if no results were returned, or
	 *         if the query failed.
	 * @throws FinderException
	 */
	public Collection ejbFindByNetIDDate(String netID, Timestamp fromDate)
			throws FinderException {
		return null;
	}
	
	public Collection ejbFindByNetIDDateSemester(String netID, Timestamp fromDate, long semseterID)
			throws FinderException {
	    return null;
	}

	/**
	 * @ejb.interface-method view-type="local"
	 **/
	public AnnouncementData getAnnouncementData() {
		return new AnnouncementData(getAnnouncementID(), getCourseID(),
				getAuthor(), getPostedDate(), getText(), getEditInfo(), getHidden());
	}

	/**
	 * Creates an announcement with the given associated courseID, 
	 * the author's name, and text-body. The date is automatically filled 
	 * in with the computer's current time. 
	 * TODO: adjust time for timezone difference to EST 
	 * @param courseid The associated courseID
	 * @param author The author's name
	 * @param text The main text of the announcement
	 * @return The primary key for the announcement
	 * @throws javax.ejb.CreateException
	 * @ejb.create-method view-type="local"
	 **/
	public AnnouncementPK ejbCreate(long courseid, String author, String text) throws javax.ejb.CreateException {
		setCourseID(courseid);
		setAuthor(author);
		setText(text);
		Timestamp today = new Timestamp(System.currentTimeMillis());
		setPostedDate(today);
		setEditInfo("");
		setHidden(false);
		return null;
	}
	
	private OldAnnouncementLocalHome historyHome(){
		try{
			if(historyHome == null)
				historyHome = OldAnnouncementUtil.getLocalHome();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return historyHome;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @return
	 */
	public Collection getAnnouncementHistory(){
		Collection result = new ArrayList();
		try{
			Collection c = historyHome().findByAnnouncementID(getAnnouncementID());
			Iterator i = c.iterator();
			while(i.hasNext())
				result.add(((OldAnnouncementLocal)i.next()).getOldAnnouncementData());
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
}
