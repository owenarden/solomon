/*
 * 
 * Created on Feb 27, 2004
 *  
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.CourseBean;
import edu.cornell.csuglab.cms.base.CourseDAO;
import edu.cornell.csuglab.cms.base.CoursePK;
import edu.cornell.csuglab.cms.base.StaffBean;
import edu.cornell.csuglab.cms.base.StudentBean;

/**
 * @author surge
 *  
 */
public class CourseDAOImpl extends DAOMaster implements CourseDAO {

    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CourseDAO#create(edu.cornell.csuglab.cms.base.CourseBean)
     *  
     */
    public CoursePK create(CourseBean ejb) throws CreateException, EJBException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        CoursePK result = null;
        try {
            conn = jdbcFactory.getConnection();
            String updateString = "insert into tcourse "
                    + "(name, code, displayedcode, description, semesterid, showfinalgrade, showtotalscore, showassignweights, showgradernetid, freezecourse, "
					+ "courseGuestAccess, assignGuestAccess, announceGuestAccess, solutionGuestAccess, " 
					+ "courseCCAccess, assignCCAccess, announceCCAccess, solutionCCAccess,"
					+ "maxTotalScore, highTotalScore, meanTotalScore, medianTotalScore, stDevTotalScore, hassection)" 
                    + " values (?,?,?,?, ?,?,?,?, ?,?,?,?, ?,?,?,?, ?,?,?,?, ?,?,?,?)";
            String findString = "select @@identity as 'courseid' from tcourse";
            ps = conn.prepareStatement(updateString);
            if(ejb.getName() == null) ps.setNull(1, java.sql.Types.VARCHAR);
   			else ps.setString(1, ejb.getName());
            if(ejb.getCode() == null) ps.setNull(2, java.sql.Types.VARCHAR);
   			else ps.setString(2, ejb.getCode());
            if(ejb.getDisplayedCode() == null) ps.setNull(3, java.sql.Types.VARCHAR);
   			else ps.setString(3, ejb.getDisplayedCode());
            if(ejb.getDescription() == null) ps.setNull(4, java.sql.Types.VARCHAR);
   			else ps.setString(4, ejb.getDescription());
            ps.setLong(5, ejb.getSemesterID());
            ps.setBoolean(6, ejb.getShowFinalGrade());
            ps.setBoolean(7, ejb.getShowTotalScores());
            ps.setBoolean(8, ejb.getShowAssignWeights());
            ps.setBoolean(9, ejb.getShowGraderNetID());
            ps.setBoolean(10, ejb.getFreezeCourse());
            ps.setBoolean(11, ejb.getCourseGuestAccess());
            ps.setBoolean(12, ejb.getAssignGuestAccess());
            ps.setBoolean(13, ejb.getAnnounceGuestAccess());
            ps.setBoolean(14, ejb.getSolutionGuestAccess());
            ps.setBoolean(15, ejb.getCourseCCAccess());
            ps.setBoolean(16, ejb.getAssignCCAccess());
            ps.setBoolean(17, ejb.getAnnounceCCAccess());
            ps.setBoolean(18, ejb.getSolutionCCAccess());
            if (ejb.getMaxTotalScore() == null) {
            	ps.setNull(19, Types.FLOAT);
            } else {
            	ps.setFloat(19, ejb.getMaxTotalScore().floatValue());
            }
            if (ejb.getHighTotalScore() == null) {
            	ps.setNull(20, Types.FLOAT);
            } else {
            	ps.setFloat(20, ejb.getHighTotalScore().floatValue());
            }
            if (ejb.getMeanTotalScore() == null) {
            	ps.setNull(21, Types.FLOAT);
            } else {
            	ps.setFloat(21, ejb.getMeanTotalScore().floatValue());
            }
            if (ejb.getMedianTotalScore() == null) {
            	ps.setNull(22, Types.FLOAT);
            } else {
            	ps.setFloat(22, ejb.getMedianTotalScore().floatValue());
            }
            if (ejb.getStDevTotalScore() == null) {
            	ps.setNull(23, Types.FLOAT);
            } else {
            	ps.setFloat(23, ejb.getStDevTotalScore().floatValue());
            }
            ps.setBoolean(24, ejb.getHasSection());
            
            int count = ps.executeUpdate();
            rs = conn.prepareStatement(findString).executeQuery();
            if (count == 1 && rs.next()) {
                result = new CoursePK(rs.getLong(1));
                ejb.setCourseID(result.courseID);
            } else {
            	throw new CreateException("Failed to create a new course");
            }
            conn.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CourseDAO#load(edu.cornell.csuglab.cms.base.CoursePK,
     *      edu.cornell.csuglab.cms.base.CourseBean)
     *  
     */
    public void load(CoursePK pk, CourseBean ejb) throws EJBException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = jdbcFactory.getConnection();
            String queryString = "select * from tCourse where CourseID = ?";
            ps = conn.prepareStatement(queryString);
            ps.setLong(1, pk.getCourseID());
            rs = ps.executeQuery();
            if (rs.next()) {
                ejb.setCourseID(rs.getLong("CourseID"));
                ejb.setCode(rs.getString("Code"));
                ejb.setDisplayedCode(rs.getString("DisplayedCode"));
                ejb.setName(rs.getString("Name"));
                try {
                	ejb.setDescription(rs.getString("Description"));
                } catch (SQLException e) {
                	ejb.setDescription("");
                }
                ejb.setSemesterID(rs.getLong("SemesterID"));
                ejb.setShowFinalGrade(rs.getBoolean("showfinalgrade"));
                ejb.setShowTotalScores(rs.getBoolean("ShowTotalScore"));
                ejb.setShowAssignWeights(rs.getBoolean("ShowAssignWeights"));
                ejb.setShowGraderNetID(rs.getBoolean("showgradernetid"));
                ejb.setFreezeCourse(rs.getBoolean("freezecourse"));
                ejb.setFileCounter(rs.getLong("FileCounter"));
                rs.getBoolean("Hidden"); // not used currently
                ejb.setCourseGuestAccess(rs.getBoolean("courseGuestAccess"));
                ejb.setAssignGuestAccess(rs.getBoolean("assignGuestAccess"));
                ejb.setAnnounceGuestAccess(rs.getBoolean("announceGuestAccess"));
                ejb.setSolutionGuestAccess(rs.getBoolean("solutionGuestAccess"));
                ejb.setCourseCCAccess(rs.getBoolean("courseCCAccess"));
                ejb.setAssignCCAccess(rs.getBoolean("assignCCAccess"));
                ejb.setAnnounceCCAccess(rs.getBoolean("announceCCAccess"));
                ejb.setSolutionCCAccess(rs.getBoolean("solutionCCAccess"));
                float score;
                score = rs.getFloat("MaxTotalScore");
                if (rs.wasNull()) {
                	ejb.setMaxTotalScore(null);
                } else {
                	ejb.setMaxTotalScore(new Float(score));
                }
                score = rs.getFloat("HighTotalScore");
                if (rs.wasNull()) {
                	ejb.setHighTotalScore(null);
                } else {
                	ejb.setHighTotalScore(new Float(score));
                }
                score = rs.getFloat("MeanTotalScore");
                if (rs.wasNull()) {
                	ejb.setMeanTotalScore(null);
                } else {
                	ejb.setMeanTotalScore(new Float(score));
                }
                score = rs.getFloat("MedianTotalScore");
                if (rs.wasNull()) {
                	ejb.setMedianTotalScore(null);
                } else {
                	ejb.setMedianTotalScore(new Float(score));
                }
                score = rs.getFloat("StDevTotalScore");
                if (rs.wasNull()) {
                	ejb.setStDevTotalScore(null);
                } else {
                	ejb.setStDevTotalScore(new Float(score));
                }
                
                ejb.setHasSection(rs.getBoolean("hassection"));
            }
            conn.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CourseDAO#remove(edu.cornell.csuglab.cms.base.CoursePK)
     *  
     */
    public void remove(CoursePK pk) throws RemoveException, EJBException {
        // TODO Auto-generated method stub
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CourseDAO#store(edu.cornell.csuglab.cms.base.CourseBean)
     *  
     */
    public void store(CourseBean ejb) throws EJBException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = jdbcFactory.getConnection();
            String updateString = "update tCourse "
                    + "set Code = ?, DisplayedCode = ?, Name = ?, Description = ?, SemesterID = ?, "
                    + "showfinalgrade = ?, showtotalscore = ?, showassignweights = ?, showgradernetid = ?, "
                    + "freezecourse = ?, filecounter = ?, " 
					+ "announceGuestAccess = ?, assignGuestAccess = ?, courseGuestAccess = ?, solutionGuestAccess = ?, " 
					+ "announceCCAccess = ?, assignCCAccess = ?, courseCCAccess = ?, solutionCCAccess = ?, "
					+ "maxTotalScore = ?, highTotalScore = ?, meanTotalScore = ?, medianTotalScore = ?, stDevTotalScore = ?, hassection = ? "
					+ "where CourseID = ?";
            ps = conn.prepareStatement(updateString);
            if(ejb.getCode() == null) ps.setNull(1, java.sql.Types.VARCHAR);
   			else ps.setString(1, ejb.getCode());
            if(ejb.getDisplayedCode() == null) ps.setNull(2, java.sql.Types.VARCHAR);
   			else ps.setString(2, ejb.getDisplayedCode());
            if(ejb.getName() == null) ps.setNull(3, java.sql.Types.VARCHAR);
   			else ps.setString(3, ejb.getName());
            if(ejb.getDescription() == null) ps.setNull(4, java.sql.Types.VARCHAR);
   			else ps.setString(4, ejb.getDescription());
            ps.setLong(5, ejb.getSemesterID());
            ps.setBoolean(6, ejb.getShowFinalGrade());
            ps.setBoolean(7, ejb.getShowTotalScores());
            ps.setBoolean(8, ejb.getShowAssignWeights());
            ps.setBoolean(9, ejb.getShowGraderNetID());
            ps.setBoolean(10, ejb.getFreezeCourse());
            ps.setLong(11, ejb.getFileCounter());
            ps.setBoolean(12, ejb.getAnnounceGuestAccess());
            ps.setBoolean(13, ejb.getAssignGuestAccess());
            ps.setBoolean(14, ejb.getCourseGuestAccess());
            ps.setBoolean(15, ejb.getSolutionGuestAccess());
            ps.setBoolean(16, ejb.getAnnounceCCAccess());
            ps.setBoolean(17, ejb.getAssignCCAccess());
            ps.setBoolean(18, ejb.getCourseCCAccess());
            ps.setBoolean(19, ejb.getSolutionCCAccess());
            if (ejb.getMaxTotalScore() == null) {
            	ps.setNull(20, Types.FLOAT);
            } else {
            	ps.setFloat(20, ejb.getMaxTotalScore().floatValue());
            }
            if (ejb.getHighTotalScore() == null) {
            	ps.setNull(21, Types.FLOAT);
            } else {
            	ps.setFloat(21, ejb.getHighTotalScore().floatValue());
            }
            if (ejb.getMeanTotalScore() == null) {
            	ps.setNull(22, Types.FLOAT);
            } else {
            	ps.setFloat(22, ejb.getMeanTotalScore().floatValue());
            }
            if (ejb.getMedianTotalScore() == null) {
            	ps.setNull(23, Types.FLOAT);
            } else {
            	ps.setFloat(23, ejb.getMedianTotalScore().floatValue());
            }
            if (ejb.getStDevTotalScore() == null) {
            	ps.setNull(24, Types.FLOAT);
            } else {
            	ps.setFloat(24, ejb.getStDevTotalScore().floatValue());
            }
            ps.setBoolean(25, ejb.getHasSection());
            ps.setLong(26, ejb.getCourseID());
            ps.executeUpdate();
            conn.close();
            ps.close();
        } catch (Exception e) {
        	try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new EJBException(e.getMessage());
		}
    }

    public CoursePK findByCourseID(long courseID) throws FinderException {
    	return findByPrimaryKey(new CoursePK(courseID));
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CourseDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.CoursePK)
     *  
     */
    public CoursePK findByPrimaryKey(CoursePK pk) throws FinderException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = jdbcFactory.getConnection();
            String queryString = "select CourseID from tCourse where CourseID = ?";
            ps = conn.prepareStatement(queryString);
            ps.setLong(1, pk.courseID);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new FinderException("Failed to find course with ID = " + pk.getCourseID());
            }
            conn.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
        return pk;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CourseDAO#getStudentCoursesByNetID()
     *  
     */
    public Collection findStudentCourses(String netid) throws FinderException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try {
            conn = jdbcFactory.getConnection();
            String queryString = "select c.CourseID from tCourse c "
                    + "inner join tStudent s on c.CourseID = s.courseid "
                    + "inner join tSystemProperties p on c.SemesterID = p.CurrentSemester "
                    + "where s.netid = ? and s.status = ? and " 
                    + "p.ID = ? order by c.Code ASC";
            ps = conn.prepareStatement(queryString);
            ps.setString(1, netid);
            ps.setString(2, StudentBean.ENROLLED);
            ps.setInt(3, 1);
            rs = ps.executeQuery();
            while (rs.next()) {
                CoursePK key = new CoursePK(rs.getLong(1));
                result.add(key);
            }
            conn.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.cornell.csuglab.cms.base.CourseDAO#getStaffCoursesByNetID()
     *  
     */
    public Collection findStaffCourses(String netid) throws FinderException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try {
            conn = jdbcFactory.getConnection();
            String queryString = "select c.CourseID from tCourse c "
                    + "inner join tStaff s on c.CourseID = s.courseid "
                    + "inner join tSystemProperties p on c.SemesterID = p.CurrentSemester "
                    + "where s.netid = ? and s.Status = ? and " 
                    + "p.ID = ? order by c.Code ASC";
            ps = conn.prepareStatement(queryString);
            ps.setString(1, netid);
            ps.setString(2, StaffBean.ACTIVE);
            ps.setInt(3, 1);
            rs = ps.executeQuery();
            while (rs.next()) {
                CoursePK key = new CoursePK(rs.getLong(1));
                result.add(key);
            }
            conn.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
        return result;
    }
    
    public CoursePK findByAssignmentID(long assignmentID) throws FinderException {
    	Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        CoursePK result = null;
        try {
        	conn = jdbcFactory.getConnection();
        	String query = "select CourseID from tAssignment where AssignmentID = ?";
        	ps = conn.prepareStatement(query);
        	ps.setLong(1, assignmentID);
        	rs = ps.executeQuery();
        	if (rs.next()) {
        		result = new CoursePK(rs.getLong("CourseID"));
        	} else {
        		throw new FinderException("Could not find a Course for assignment " + assignmentID);
        	}
        	ps.close();
        	rs.close();
        	conn.close();
        } catch (Exception e) {
        	try {
        		if (rs != null) rs.close();
        		if (ps != null) ps.close();
        		if (conn != null) conn.close();
        	} catch (Exception f) {}
        	throw new FinderException(e.getMessage());
  		}
        return result;
    }

	private Collection queryBySemesterID(String query, long semID) throws FinderException{
    	Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try
  			{
        	conn = jdbcFactory.getConnection();
        	ps = conn.prepareStatement(query);
        	ps.setLong(1, semID);
        	rs = ps.executeQuery();
        	while(rs.next())
        	{
        		result.add(new CoursePK(rs.getLong(1)));
        	}
        	ps.close();
        	rs.close();
        	conn.close();
  			}
        catch (Exception e)
  			{
        	try {
        		if (rs != null) rs.close();
        		if (ps != null) ps.close();
        		if (conn != null) conn.close();
        	}
        	catch (Exception f) {}
        	throw new FinderException(e.getMessage());
  			}
        return result;
	}
	
	/* (non-Javadoc)
     * @see CourseDAO#findBySemesterID(long)
     */
    public Collection findBySemesterID(long semID) throws FinderException
	{
    	String query = "select CourseID from tCourse where SemesterID = ?";
    	return queryBySemesterID(query, semID);
	}

	
	/* (non-Javadoc)
     * @see CourseDAO#findByCourseCode(String,long)
     */
    public Collection findByCourseCodeAndSemester(String code, long semID) throws FinderException
	{
    	Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try
  			{
        	conn = jdbcFactory.getConnection();
        	ps = conn.prepareStatement("select CourseID from tCourse where Code = ? and SemesterID = ?");
        	ps.setString(1, code);
        	ps.setLong(2, semID);
        	rs = ps.executeQuery();
        	while(rs.next())
        	{
        		result.add(new CoursePK(rs.getLong(1)));
        	}
        	ps.close();
        	rs.close();
        	conn.close();
  			}
        catch (Exception e)
  			{
        	try {
        		if (rs != null) rs.close();
        		if (ps != null) ps.close();
        		if (conn != null) conn.close();
        	}
        	catch (Exception f) {}
        	throw new FinderException(e.getMessage());
  			}
        return result;
	}
    
    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.CourseDAO#findStaffAdminCourses(java.lang.String)
     */
    public Collection findStaffAdminCourses(long semesterID, String netid) throws FinderException {
    	Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try {
        	conn = jdbcFactory.getConnection();
        	String query = "select CourseID from tCourse c, tStaff s where c.CourseID = s.CourseID " +
        			"and s.AdminPriv = ? and s.Status = ? and c.SemesterID = ? and s.NetID = ? order by c.Code ASC";
        	ps = conn.prepareStatement(query);
        	ps.setBoolean(1, true);
        	ps.setString(2, StaffBean.ACTIVE);
        	ps.setLong(3, semesterID);
        	ps.setString(4, netid);
        	rs = ps.executeQuery();
        	while(rs.next()) {
        		result.add(new CoursePK(rs.getLong("CourseID")));
        	}
        	ps.close();
        	rs.close();
        	conn.close();
  		} catch (Exception e) {
        	try {
        		if (rs != null) rs.close();
        		if (ps != null) ps.close();
        		if (conn != null) conn.close();
        	}
        	catch (Exception f) {}
        	throw new FinderException(e.getMessage());
  		}
        return result;
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.CourseDAO#findStudentCoursesBySemester(java.lang.String, long)
     */
    public Collection findStudentCoursesBySemester(String netid, long semesterID) throws FinderException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try {
            conn = jdbcFactory.getConnection();
            String queryString = "select c.CourseID from tCourse c "
                    + "inner join tStudent s on c.CourseID = s.courseid "
                    + "where s.netid = ? and s.status = ? and " 
                    + "c.SemesterID = ? order by c.Code ASC";
            ps = conn.prepareStatement(queryString);
            ps.setString(1, netid);
            ps.setString(2, StudentBean.ENROLLED);
            ps.setLong(3, semesterID);
            rs = ps.executeQuery();
            while (rs.next()) {
                CoursePK key = new CoursePK(rs.getLong(1));
                result.add(key);
            }
            conn.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
        return result;
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.CourseDAO#findStaffCoursesBySemester(java.lang.String, long)
     */
    public Collection findStaffCoursesBySemester(String netid, long semesterID) throws FinderException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try {
            conn = jdbcFactory.getConnection();
            String queryString = "select c.CourseID from tCourse c "
                    + "inner join tStaff s on c.CourseID = s.courseid "
                    + "where s.netid = ? and s.Status = ? and " 
                    + "c.SemesterID = ? order by c.Code ASC";
            ps = conn.prepareStatement(queryString);
            ps.setString(1, netid);
            ps.setString(2, StaffBean.ACTIVE);
            ps.setLong(3, semesterID);
            rs = ps.executeQuery();
            while (rs.next()) {
                CoursePK key = new CoursePK(rs.getLong(1));
                result.add(key);
            }
            conn.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
        return result;
    }

    private Collection findByAccess(String access, Long semesterID) throws FinderException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection result = new ArrayList();
        try {
            conn = jdbcFactory.getConnection();
            String queryString = null;
            if (semesterID == null) {
                queryString = "select c.CourseID from tCourse c "
                    + "inner join tSystemProperties s on c.SemesterID = s.CurrentSemester "
                    + "where c." + access + " = ? and s.ID = ? order by c.Code ASC";
            } else {
                queryString = "select CourseID from tCourse c "
                	+ "where c." + access + " = ? and c.SemesterID = ? "
                	+ "order by c.Code ASC";
            }
            ps = conn.prepareStatement(queryString);
            ps.setBoolean(1, true);
            if (semesterID == null) {
                ps.setInt(2, 1);
            } else {
                ps.setLong(2, semesterID.longValue());
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                CoursePK key = new CoursePK(rs.getLong(1));
                result.add(key);
            }
            conn.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
        	try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
			throw new FinderException(e.getMessage());
		}
        return result;
    }
    
    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.CourseDAO#findGuestAccess()
     */
    public Collection findGuestAccess() throws FinderException {
        return findByAccess("CourseGuestAccess", null);
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.CourseDAO#findGuestAccessBySemester(long)
     */
    public Collection findGuestAccessBySemester(long semesterID) throws FinderException {
        return findByAccess("CourseGuestAccess", new Long(semesterID));
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.CourseDAO#findCCAccess()
     */
    public Collection findCCAccess() throws FinderException {
        return findByAccess("CourseCCAccess", null);
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.CourseDAO#findCCAccessBySemester(long)
     */
    public Collection findCCAccessBySemester(long semesterID) throws FinderException {
        return findByAccess("CourseCCAccess", new Long(semesterID));
    }
}
