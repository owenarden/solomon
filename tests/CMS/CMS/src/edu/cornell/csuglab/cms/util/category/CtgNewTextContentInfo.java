/*
 * Created on Aug 31, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.cornell.csuglab.cms.util.category;

import java.sql.Timestamp;
import java.util.ArrayList;


/**
 * @author evan
 *
 * Holds info for one category content of type text
 */
public class CtgNewTextContentInfo extends CtgNewContentInfo
{
	private String text;
	
	public CtgNewTextContentInfo(long rowID, long colID, String text)
	{
		super(rowID, colID);
		this.text = text;
	}
	
	/*
	 * CtgNewContentInfo functions. A null return value signifies no data.
	 */
	
	public String getColType()
	{
		return CtgUtil.CTNT_TEXT;
	}
	
	public Timestamp getDate()       //used by contents of type date
	{
		return null;
	}
	
	public String getText()          //used by contents of type text
	{
		return text;
	}
	
	public Long getNumber()          //used by contents of type number
	{
		return null;
	}
	
	public String getURLAddress()    //used by contents of type url
	{
		return null;
	}
	
	public String getURLLabel()      //used by contents of type url
	{
		return null;
	}
	
	public ArrayList getFileInfos()  //used by contents of type file
	{
		return null;
	}
	
	public ArrayList getFileLabels() //used by contents of type file
	{
		return null;
	}
}
