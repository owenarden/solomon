/*
 * Created on Apr 7, 2004
 */
package edu.cornell.csuglab.cms.base;
import java.util.Collection;

import javax.ejb.EntityBean;
import javax.ejb.FinderException;
/**
 * @ejb.bean name="Staff"
 *	jndi-name="StaffBean"
 *	type="BMP" 
 * 
 * @jboss.method-attributes pattern="get*" read-only="true" 
 *
 * @ejb.dao class="edu.cornell.csuglab.cms.base.StaffDAO"
 * impl-class="edu.cornell.csuglab.cms.base.dao.StaffDAOImpl"
 *
 * @ejb.resource-ref res-ref-name="jdbc/MSSQLDS"
 * res-type="javax.sql.Datasource"
 * res-auth="Container"
 *
 * @jboss.resource-ref res-ref-name="jdbc/MSSQLDS"
 * jndi-name="java:/MSSQLDS"
 **/
public abstract class StaffBean implements EntityBean {
    
    /* Do NOT set these fields to have default values upon instantiation.
     * Since beans may not be newly created objects when ejbCreate is called
     * we cannot rely on any of the values entered here. */
	private long courseID;
	private String netID;
	private String status; //see below
	private boolean AdminPriv, GroupsPriv, 
		GradesPriv, AssignmentsPriv, CategoryPriv;
	private boolean emailNewAssign, emailDueDate,
		emailAssignedTo, emailAssignSubmit, emailFinalGrade, emailRequest;
	
	//possible statuses
	public static String ACTIVE = "Active", INACTIVE = "Inactive";
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the courseID.
	 */
	public long getCourseID() {
		return courseID;
	}
	/**
	 * @ejb.interface-method view-type="local"
	 * @param courseID The courseID to set.
	 */
	public void setCourseID(long courseID) {
		this.courseID = courseID;
	}
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.pk-field
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the netID.
	 */
	public String getNetID() {
		return netID;
	}
	/**
	 * @ejb.interface-method view-type="local"
	 * @param netID The netID to set.
	 */
	public void setNetID(String netID) {
		this.netID = netID;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @ejb.interface-method view-type="local"
	 * @param status The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns if the user has adminPriv.
	 */
	public boolean getAdminPriv() {
		return AdminPriv;
	}
	/**
	 * @ejb.interface-method view-type="local"
	 * @param adminPriv Set if the user has adminPriv.
	 */
	public void setAdminPriv(boolean adminPriv) {
		AdminPriv = adminPriv;
	}
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns if the user has groupsPriv.
	 */
	public boolean getGroupsPriv() {
		return GroupsPriv;
	}
	/**
	 * @ejb.interface-method view-type="local"
	 * @param groupsPriv Set if the user has groupsPriv.
	 */
	public void setGroupsPriv(boolean groupsPriv) {
		GroupsPriv = groupsPriv;
	}
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns if the user has gradesPriv.
	 */
	public boolean getGradesPriv() {
		return GradesPriv;
	}
	/**
	 * @ejb.interface-method view-type="local"
	 * @param gradesPriv Set if the user has gradesPriv.
	 */
	public void setGradesPriv(boolean gradesPriv) {
		GradesPriv = gradesPriv;
	}
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return Returns if the user has assignmentsPriv.
	 */
	public boolean getAssignmentsPriv() {
		return AssignmentsPriv;
	}
	/**
	 * @ejb.interface-method view-type="local"
	 * @param assignmentsPriv Set if the user has assignmentsPriv.
	 */
	public void setAssignmentsPriv(boolean assignmentsPriv) {
		AssignmentsPriv = assignmentsPriv;
	}
	
	/**
	 * @ejb.persistence
	 * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
	 * @return if the user has categoryPriv
	 */
	public boolean getCategoryPriv(){
		return CategoryPriv;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	 * @param categoryPriv Set if the user has categoryPriv
	 */
	public void setCategoryPriv(boolean categoryPriv){
		CategoryPriv = categoryPriv;
	}

	
    /**
     * @return Returns the emailAssignedTo.
     * @ejb.persistence
     * @ejb.interface-method view-type="local"
 	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
    */
    public boolean getEmailAssignedTo() {
        return emailAssignedTo;
    }
    
    /**
     * @param emailAssignedTo The emailAssignedTo to set.
     * @ejb.interface-method view-type="local"
     */
    public void setEmailAssignedTo(boolean emailAssignedTo) {
        this.emailAssignedTo = emailAssignedTo;
    }
    
    
    
    /**
     * @return Returns the emailAssignSubmit.
     * @ejb.persistence
     * @ejb.interface-method view-type="local"
 	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
    */
    public boolean getEmailAssignSubmit() {
        return emailAssignSubmit;
    }
    
    /**
     * @param emailAssignSubmit The emailAssignSubmit to set.
     * @ejb.interface-method view-type="local"
     */
    public void setEmailAssignSubmit(boolean emailAssignSubmit) {
        this.emailAssignSubmit = emailAssignSubmit;
    }
    
    
    /**
     * @return Returns the emailDueDate.
     * @ejb.persistence
     * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
     */
    public boolean getEmailDueDate() {
        return emailDueDate;
    }
    /**
     * @param emailDueDate The emailDueDate to set.
     * @ejb.interface-method view-type="local"
     */
    public void setEmailDueDate(boolean emailDueDate) {
        this.emailDueDate = emailDueDate;
    }
    /**
     * @return Returns the emailFinalGrade.
     * @ejb.persistence
     * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
     */
    public boolean getEmailFinalGrade() {
        return emailFinalGrade;
    }
    /**
     * @param emailFinalGrade The emailFinalGrade to set.
     * @ejb.interface-method view-type="local"
     */
    public void setEmailFinalGrade(boolean emailFinalGrade) {
        this.emailFinalGrade = emailFinalGrade;
    }
    /**
     * @return Returns the emailNewAssign.
     * @ejb.persistence
     * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
     */
    public boolean getEmailNewAssign() {
        return emailNewAssign;
    }
    /**
     * @param emailNewAssign The emailNewAssign to set.
     * @ejb.interface-method view-type="local"
     */
    public void setEmailNewAssign(boolean emailNewAssign) {
        this.emailNewAssign = emailNewAssign;
    }
    /**
     * @return Returns the emailRegrade.
     * @ejb.persistence
     * @ejb.interface-method view-type="local"
	 * @ejb.transaction type="Supports" 
	 * @jboss.method-attributes read-only="true"
     */
    public boolean getEmailRequest() {
        return emailRequest;
    }
    /**
     * @param emailRegrade The emailRegrade to set.
     * @ejb.interface-method view-type="local"
     */
    public void setEmailRequest(boolean emailRequest) {
        this.emailRequest = emailRequest;
    }
	/**
	 * Checks to see if the given primary key can be found in the database
	 * @param pk The primary key being serarched for
	 * @return The key passed in if it is found
	 * @throws FinderException
	 */
	public StaffPK ejbFindByPrimaryKey(StaffPK pk) throws FinderException {
		return null;
	}

	/**
	 * Finds all active staff members in a given course.
	 * @param courseID The courseID
	 * @return The collection of staff objects
	 * @throws FinderException
	 */
	public Collection ejbFindByCourseID(long courseID) throws FinderException {
		return null;
	}
	
	public StaffPK ejbFindByAssignmentIDNetID(long assignID, String netID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all staff members associated with a given course, including 
	 * inactive ones.
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindAllByCourseID(long courseID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all the courses that a given user is a staff in
	 * @param userID The user's netID
	 * @return The collection of staff objects
	 * @throws FinderException
	 */
	public Collection ejbFindByUserID(String userID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all staff members in a course who have permission
	 * to assign and edit grades
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindGradersByCourseID(long courseID) throws FinderException {
		return null;
	}
	
	/**
	 * Finds if a given user is a staff in a given course, or throws a FinderException 
	 * if nothing is found.
	 * @param netid The user's netID
	 * @param courseid The courseID
	 * @return The collection of staff objects
	 * @throws FinderException
	 */
	public StaffPK ejbFindByUserCourse(String netid, long courseid) throws FinderException {
		return null;
	}
	
	/**
	 * Finds all staff members who have requested to have emails sent to them
	 * when a new assignment is released to students.
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindEmailNewAssigns(long courseID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all staff members who have requested to have emails sent to them
	 * when an assignment's due date is changed.
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindEmailDueDates(long courseID) throws FinderException {
	    return null;
	}
	
	/**
	 * Finds all staff members who have requested to have emails sent to them
	 * when final grades are committed to the registar.
	 * @param courseID
	 * @return
	 * @throws FinderException
	 */
	public Collection ejbFindEmailFinalGrades(long courseID) throws FinderException {
	    return null;
	}
	
	/**
	 * @ejb.interface-method view-type="local"
	**/
	public StaffData getStaffData() {
		return new StaffData (getCourseID(), getNetID(), getStatus(), getAdminPriv(), 
				getGroupsPriv(), getGradesPriv(), getAssignmentsPriv(), getCategoryPriv(),
				getEmailAssignedTo(), getEmailAssignSubmit(), getEmailDueDate(), getEmailFinalGrade(),
				getEmailNewAssign(), getEmailRequest());
	}
	
	/**
	 * Create a new staff member for the given netID and course.
	 * @param netID NetID of the new Staff Member
	 * @param courseID ID of the course in which to create the staff member
	 * @return The new StaffPK object
	 * @throws javax.ejb.CreateException If the creation fails
	 * @ejb.create-method view-type="local"
	 */
	public StaffPK ejbCreate(String netID, long courseID) throws javax.ejb.CreateException {
		setNetID(netID);
		setCourseID(courseID);
		setStatus(ACTIVE);
		setAdminPriv(false);
		setAssignmentsPriv(false);
		setCategoryPriv(false);
		setEmailAssignedTo(false);
		setEmailAssignSubmit(false);
		setEmailDueDate(false);
		setEmailFinalGrade(false);
		setEmailNewAssign(false);
		setEmailRequest(false);
		setGradesPriv(false);
		setGroupsPriv(false);
		return null;
	}
}
