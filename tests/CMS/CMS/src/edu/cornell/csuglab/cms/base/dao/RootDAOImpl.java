package edu.cornell.csuglab.cms.base.dao;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.FinderException;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.cornell.csuglab.cms.base.GroupMemberBean;
import edu.cornell.csuglab.cms.base.LogBean;
import edu.cornell.csuglab.cms.base.LogData;
import edu.cornell.csuglab.cms.base.RequiredSubmissionBean;
import edu.cornell.csuglab.cms.base.RootDAO;
import edu.cornell.csuglab.cms.base.SemesterPK;
import edu.cornell.csuglab.cms.base.StaffBean;
import edu.cornell.csuglab.cms.base.StudentBean;
import edu.cornell.csuglab.cms.base.UserData;
import edu.cornell.csuglab.cms.log.LogDetail;
import edu.cornell.csuglab.cms.www.util.DateTimeUtil;
import edu.cornell.csuglab.cms.www.util.ResultCache;

public class RootDAOImpl extends DAOMaster implements RootDAO {

	private static TreeMap hostGroupMap; // Long semesterID -> JSON string (errors stored to id -1)
	private static Document foreignServerGroups; // XML with names and server URLs of groups to which this server doesn't belong
	
	public void ensureStartupSettings() throws FinderException {
	    Connection conn = null;
	    ResultSet rs = null;
	    PreparedStatement ps = null;
	    try {
	        String query = "SELECT TOP 1 SemesterID FROM tSemesters";
	        conn = jdbcFactory.getConnection();
	        ps = conn.prepareStatement(query);
	        rs = ps.executeQuery();
	        if (!rs.next()) {
	            String year = DateTimeUtil.getYear(new Timestamp(System.currentTimeMillis()));
	            String insQuery = "insert into tSemesters (SemesterName, Hidden) values (?, ?)";
	            ps = conn.prepareStatement(insQuery);
	            ps.setString(1, "Fall " + year);
	            ps.setBoolean(2, false);
	            ps.executeUpdate();
	            ps = conn.prepareStatement("select @@identity as 'SemesterID' from tSemesters");
	            rs = ps.executeQuery();
	            rs.next();
	        }
	        long semesterID = rs.getLong("SemesterID");
	        query = "select * from tSystemProperties where ID = ?";
	        ps = conn.prepareStatement(query);
	        ps.setInt(1, 1);
	        rs = ps.executeQuery();
	        if (!rs.next()) { // need to insert a row with default settings
	            String insQuery = "insert into tSystemProperties (ID, CurrentSemester, DebugMode, UploadMaxFileSize,  HostGroupsURL) " +
	            		"values (?, ?, ?, ?, ?)";
	            ps = conn.prepareStatement(insQuery);
	            ps.setInt(1, 1);
	            ps.setLong(2, semesterID);
	            ps.setBoolean(3, false);
	            ps.setLong(4, 10000000);
	            ps.setString(5, "");
	            ps.executeUpdate();
	        }
	        // XXX removed April 2, 2010 by aip23 - obsolete
	        /*
	        query = "select FileType from tFileTypes";
	        ps = conn.prepareStatement(query);
	        rs = ps.executeQuery();
	        if (!rs.next()) {
	            ps = conn.prepareStatement("insert into tFileTypes (FileType, Description) values (?,?)");
	            ps.setString(1, RequiredSubmissionBean.ANY_TYPE);
	            ps.setString(2, "Any file type");
	            ps.executeUpdate();
	            ps.setString(1, "txt");
	            ps.setString(2, "Text document");
	            ps.executeUpdate();
	            ps.setString(1, "zip");
	            ps.setString(2, "Zip archive");
	            ps.executeUpdate();
	            ps.setString(1, "java");
	            ps.setString(2, "Java source file");
	            ps.executeUpdate();
	            ps.setString(1, "pdf");
	            ps.setString(2, "Adobe Acrobat portable document file");
	            ps.executeUpdate();
	        }
	        */
	        conn.close();
	        ps.close();
	        rs.close();
	    } catch (Exception e) {
	        try {
	            if (conn != null) conn.close();
	            if (ps != null) ps.close();
	            if (rs != null) rs.close();
	        } catch (Exception f) {}
	        e.printStackTrace();
	        throw new FinderException(e.getMessage());
	    }
	}
	
	/*(non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#
	 */
	public void refreshHostGroups(String urlstring)
	{
		// clear the old stuff
		hostGroupMap = new TreeMap();
		
		javax.xml.parsers.DocumentBuilder db;
		try {
			db = javax.xml.parsers.DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			hostGroupMap.put(new Long(-1), "Couldn't make default-configured XML Parser:\n" + e.getMessage());
			return;
		} catch (FactoryConfigurationError e) {
			hostGroupMap.put(new Long(-1), "Couldn't make default-configured DocumentBuilderFactory:\n" + e.getMessage());
			return;
		}
		
		foreignServerGroups = db.newDocument();
		Element foreignServerGroupsRoot = foreignServerGroups.createElement("root");
		foreignServerGroups.appendChild(foreignServerGroupsRoot);
		
		// if blank URL, disable feature
		if (urlstring == null || urlstring.equals(""))
		{
			hostGroupMap.put(new Long(-1), "URL is blank; feature disabled.");
			return;
		}
		
		String thisServerBase = edu.cornell.csuglab.cms.base.TransactionsBean.getCMSServerAddress();
		
		// parse
		URL u; InputStream s = null;
		
		try {
			u = new URL(urlstring);
			URL thisServer = new URL(thisServerBase);
			InetAddress thisServerIP = InetAddress.getByName(thisServer.getHost());
			
			// If HostGroupURL is self-referential (refers to this same server), this can result in an infinite loop
			// Such a situation would need a database adjustment to fix (blanking the HostGroupURL field of tSystemProperties)
			// Thus, this is bad and needs to be prevented
			if (thisServerIP.equals(InetAddress.getByName(u.getHost())))
			{
				hostGroupMap.put(new Long(-1), "The URL refers to myself; feature disabled.  Please place the XML file on a different server to avoid potential circular dependencies (infinite loops).");
				return;
			}
			
			s = u.openStream();
			
			Document xml = db.parse(s);
			
			NodeList sems = xml.getElementsByTagName("semester");
			for (int i = 0; i < sems.getLength(); i++)
			{	
				Element sem = (Element) sems.item(i);
				//String name = sem.getAttribute("name"); (used only for foreign server groups)
				
				NodeList hosts = sem.getElementsByTagName("host");
				
				long selfSemID = -1;
				StringBuffer json = new StringBuffer("[");
				boolean needComma = false;
				
				for (int j = 0; j < hosts.getLength(); j++)
				{
					Element host = (Element) hosts.item(j);
					String hosturl = host.getAttribute("baseurl");
					String semid = host.getAttribute("semid");
					
					URL remoteCMSImplURL = new URL(hosturl);
					
					if (thisServerIP.equals(InetAddress.getByName(remoteCMSImplURL.getHost())) && thisServer.getPath().equals(remoteCMSImplURL.getPath()))
					{
						// found myself, don't add myself to the JSON
						selfSemID = Long.parseLong(semid);
					}
					else {
						if (needComma) json.append(',');
						
						json.append("{'host': '").append(hosturl).append("', 'semID': ").append(semid).append('}'); 
						needComma = true;
					}
				}
				if (selfSemID > -1) {
					// found self in the list, so this server is part of the group
					json.append(']');
					hostGroupMap.put(new Long(selfSemID), json.toString());
				}
				else
				{
					// didn't find self in list, so this is a foreign group
					Node semTemp = foreignServerGroups.importNode(sem, true);
					foreignServerGroupsRoot.appendChild(semTemp);
				}
			}
		}
		catch (MalformedURLException e) {
			hostGroupMap.put(new Long(-1), "URL for host groups is not valid.");
		} catch (IOException e) {
			hostGroupMap.put(new Long(-1), "IOException encountered when attempting to read file:\n" + e.getMessage());
		} catch (org.xml.sax.SAXException e) {
			hostGroupMap.put(new Long(-1), "Could not parse XML:\n" + e.getMessage());
		} catch (Exception e) {
			hostGroupMap.put(new Long(-1), "Unknown error:\n" + e.getMessage());
		}
		try {if (s != null) s.close();} catch (Exception e) {}
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getHostGroup()
	 */
	public String getHostGroup(Long semesterID) throws FinderException {
		if (hostGroupMap == null){
		    Connection conn = null;
		    ResultSet rs = null;
		    PreparedStatement ps = null;
		    
	        String query = "select * from tSystemProperties where ID = ?";
	        try {
		        conn = jdbcFactory.getConnection();
				ps = conn.prepareStatement(query);
		        ps.setInt(1, 1);
		        rs = ps.executeQuery();
		        
		        if (rs.next())
		        	refreshHostGroups(rs.getString("HostGroupsURL"));
		        else
		        	throw new FinderException("No rows in the System Properties table!");
		        
		        conn.close();
		        ps.close();
		        rs.close();
			} catch (SQLException e) {
		        try {
		            if (conn != null) conn.close();
		            if (ps != null) ps.close();
		            if (rs != null) rs.close();
		        } catch (Exception f) {}
		        e.printStackTrace();
		        throw new FinderException(e.getMessage());
			}
		}
		return (String) hostGroupMap.get(semesterID == null ? new Long(findCurrentSemester().semesterID) : semesterID);
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getFileTypes()
	 */
	// XXX removed April 2, 2010 by aip23 - obsolete
	/*
	public Collection getFileTypes() {
		Connection conn = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT * FROM tFileTypes ORDER BY FileType ASC";
			rs = conn.prepareStatement(query).executeQuery();
			while (rs.next()) {
				result.add(rs.getString("FileType"));
			}
			conn.close();
			rs.close();
		} 
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (rs != null) rs.close();
				e.printStackTrace();
			} catch (Exception f) {}
		}
		return result;
	}
	*/

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#isDebugMode()
	 */
	public boolean isDebugMode() {
		Connection conn = null;
		ResultSet rs = null;
		boolean result = false;
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT DebugMode FROM tSystemProperties WHERE ID = 1";
			rs = conn.prepareStatement(query).executeQuery();
			rs.next();
			result = rs.getBoolean("DebugMode");
			conn.close();
			rs.close();
		} 
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (rs != null) rs.close();
				e.printStackTrace();
			} catch (Exception f) {}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getMaxFileSize()
	 */
	public int getMaxFileSize() {
		Connection conn = null;
		ResultSet rs = null;
		int result = 0;
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT UploadMaxFileSize FROM tSystemProperties WHERE ID = 1";
			rs = conn.prepareStatement(query).executeQuery();
			rs.next();
			result = rs.getInt("UploadMaxFileSize");
			conn.close();
			rs.close();
		} 
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}

	public Map getCIDCourseCodeMap() {
		Connection conn = null;
		ResultSet rs = null;
		Map result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT CourseID,Code FROM tCourse";
			rs = conn.prepareStatement(query).executeQuery();
			while (rs.next()) {
			    result.put(new Long(rs.getLong("CourseID")), rs.getString("Code"));
			}
			conn.close();
			rs.close();
		} 
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
	private Map getAssignmentNameMap(Long courseID) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		Map result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT AssignmentID,Name FROM tAssignment";
			if (courseID != null) {
			    query += " WHERE CourseID = ?";
			    ps = conn.prepareStatement(query);
			    ps.setLong(1, courseID.longValue());
			    rs = ps.executeQuery();
			} else {
			    rs = conn.prepareStatement(query).executeQuery();
			}
			while (rs.next()) {
			    result.put(new Long(rs.getLong("AssignmentID")), rs.getString("Name"));
			}
			conn.close();
			rs.close();
			if (ps != null) ps.close();
		} 
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (rs != null) rs.close();
				if (ps != null) ps.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
	public Map getAssignmentNameMap() {
	    return getAssignmentNameMap(null);
	}
	
	public Map getAssignmentNameMap(long courseID) {
	    return getAssignmentNameMap(new Long(courseID));
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getNonSoloGroupMembers(java.util.Collection, long)
	 */
	public Collection getNonSoloGroupMembers(Collection netids, long assignmentID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			if (netids.size() > 0) {
				Iterator i = netids.iterator();	
				String query = "select one.NetID from tGroupMembers one, tGroupMembers two, tGroups g " +
						"where one.GroupID = g.GroupID and g.AssignmentID = ? and " +
						"one.Status = ? and two.Status = ? and one.GroupID = two.GroupID and (";
				while (i.hasNext()) {
					i.next();
					query += "one.NetID = ?";
					if (i.hasNext()) {
						query += " or ";
					}
				}
				query += ") and not (one.NetID = two.NetID)";
				int count = 0;
				i = netids.iterator();
				ps= conn.prepareStatement(query);
				ps.setLong(1, assignmentID);
				ps.setString(2, "Active");
				ps.setString(3, "Active");
				while (i.hasNext()) {
					ps.setString((count++) + 4, (String) i.next());
				}
				rs = ps.executeQuery();
				while (rs.next()) {
					result.add(rs.getString("NetID"));
				}
				conn.close();
				ps.close();
				rs.close();
			}
		} 
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getNonStudentNetIDs(java.util.Collection, long)
	 */
	public Collection getNonStudentNetIDs(Collection netids, long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		LinkedList result = new LinkedList(netids);
		try {
			conn = jdbcFactory.getConnection();
			if (netids.size() > 0) {
				String query = "select NetID from tStudent where CourseID = ? and Status = ? and (";
				for (int i=0; i < result.size() - 1; i++) {
					query += "NetID = ? or ";
				}
				query += "NetID = ?)";
				ps = conn.prepareStatement(query);
				ps.setLong(1, courseID);
				ps.setString(2, StudentBean.ENROLLED);
				for (int j=0; j < result.size(); j++) {
					ps.setString(j+3, (String) result.get(j));
				}
				rs = ps.executeQuery();
				while (rs.next()) {
					result.remove(rs.getString("NetID"));
				}
				conn.close();
				ps.close();
				rs.close();
			}
		} 
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getGradedStudents(java.util.Collection, long)
	 */
	public Collection getGradedStudents(Collection netids, long assignmentID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			if (netids.size() > 0) {
				String query = "select distinct NetID from tGrades where AssignmentID = ? and (";
				for (int i=0; i < netids.size() - 1; i++) {
					query += "NetID = ? or ";
				}
				query += "NetID = ?) order by NetID ASC";
				ps = conn.prepareStatement(query);
				ps.setLong(1, assignmentID);
				Iterator i = netids.iterator();
				int count = 0;
				while (i.hasNext()) {
					ps.setString((count++) + 2, (String) i.next());
				}
				rs = ps.executeQuery();
				while (rs.next()) {
					result.add(rs.getString("NetID"));
				}
				conn.close();
				ps.close();
				rs.close();
			}
		} 
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
	public Collection getGradedGroups(Collection groupIDs, long asgnID)
	{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			if(groupIDs.size() > 0)
			{
				String query = "select distinct gg.GroupID from tGroupGrades as gg"
					+ " left join tGroups as g on gg.GroupID = g.GroupID"
					+ " where g.AssignmentID = ? and (";
				for (int i=0; i < groupIDs.size() - 1; i++) {
					query += "g.GroupID = ? or ";
				}
				query += "g.GroupID = ?) order by gg.GroupID asc";
				ps = conn.prepareStatement(query);
				ps.setLong(1, asgnID);
				Iterator i = groupIDs.iterator();
				int count = 2;
				while (i.hasNext()) {
					ps.setLong(count++, ((Long)i.next()).longValue());
				}
				rs = ps.executeQuery();
				while (rs.next()) {
					result.add(new Long(rs.getLong("GroupID")));
				}
				conn.close();
				ps.close();
				rs.close();
			}
		} 
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
	/* Unused as of Feb 2008 - Alex
	 * 
	 * 
	public boolean assignedToGroups(List groupIDs, String grader) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean result = false;
		if (groupIDs.size() == 0) return true;
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT COUNT(DISTINCT GroupID) AS GroupCount " +
					"FROM tGroupAssignedTo WHERE (NetID = ?) AND (";
			for (int i=0; i < groupIDs.size() - 1; i++) {
			    query += "GroupID = ? OR ";
			}
			query += "GroupID = ?)";
			ps = conn.prepareStatement(query);
			ps.setString(1, grader);
			for (int i=0; i < groupIDs.size(); i++) {
			    ps.setLong(2 + i, ((Long)groupIDs.get(i)).longValue());
			}
			rs = ps.executeQuery();
			rs.next();
			int count = rs.getInt("GroupCount");
			result = count == groupIDs.size();
			conn.close();
			ps.close();
			rs.close();
		} 
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}*/

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getGradedStudents(java.util.Collection, long)
	 */
	public Collection getGradedAssignments(Collection netids, long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (netids.size() == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			if (netids.size() > 0) {
				String query = "select distinct a.AssignmentID from tGrades g, tAssignment a where g.AssignmentID = a.AssignmentID " +
						"and a.CourseID = ? and (";
				for (int i=0; i < netids.size() - 1; i++) {
					query += "NetID = ? or ";
				}
				query += "NetID = ?)";
				ps = conn.prepareStatement(query);
				ps.setLong(1, courseID);
				Iterator i = netids.iterator();
				int count = 0;
				while (i.hasNext()) {
					ps.setString((count++) + 2, (String) i.next());
				}
				rs = ps.executeQuery();
				while (rs.next()) {
					result.add(new Long(rs.getLong("AssignmentID")));
				}
				conn.close();
				ps.close();
				rs.close();
			}
		} 
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getHostGroupsURL()
	 */
	public String getHostGroupsURL() throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String result = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select hostGroupsURL from tSystemProperties where ID = 1";
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = rs.getString(1);
			}
			conn.close();
			rs.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			throw new FinderException("Database failed to find host grouping by semester URL");
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#setHostGroupsURL(String)
	 */
	public boolean setHostGroupsURL(String url)
	{
		Connection conn = null;
		PreparedStatement ps = null;
		boolean success = true;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tSystemProperties set hostGroupsURL = ? where ID = 1";
			ps = conn.prepareStatement(query);
			ps.setString(1, url);
			if (ps.executeUpdate() == 1) success = true;
			conn.close();
			ps.close();
			
			refreshHostGroups(url); // most errors will be handled here, and stored for later display
		}
		catch (Exception e) {
			success = false;
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			}
			catch (Exception f) {}
		}
		return success;
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getForeignSemesters(String)
	 */
	public Document getForeignSemesters() throws FinderException
	{
		return foreignServerGroups;
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#findCurrentSemester()
	 */
	public SemesterPK findCurrentSemester() throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		SemesterPK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select CurrentSemester from tSystemProperties where ID = 1";
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = new SemesterPK(rs.getLong(1));
			}
			conn.close();
			rs.close();
			ps.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			throw new FinderException("Database failed to find current semester");
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#setCurrentSemester(long)
	 */
	public boolean setCurrentSemester(long semesterID)
	{
		Connection conn = null;
		PreparedStatement ps = null;
		boolean success = true;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tSystemProperties set CurrentSemester = ? where ID = 1";
			ps = conn.prepareStatement(query);
			ps.setLong(1, semesterID);
			if(ps.executeUpdate() == 1) success = true; //should return the row count
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			success = false;
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
			}
			catch (Exception f) {}
		}
		return success;
	}

	public Map getRemainingSubmissionMap(long assignmentID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT GroupID, sCount FROM " +
					"(SELECT g.GroupID, COUNT(DISTINCT f.SubmissionID) AS sCount " +
	                  "FROM tGroups g, tSubmittedFiles f, tRequiredSubmissions r " +
	                  "WHERE g.GroupID = f.GroupID AND f.SubmissionID = r.SubmissionID " +
	                  "AND r.Hidden = ? AND r.AssignmentID = ? GROUP BY g.GroupId) t";
			ps = conn.prepareStatement(query);
			ps.setBoolean(1, false);
			ps.setLong(2, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long groupID = new Long(rs.getLong("GroupID"));
				Integer remSubs = new Integer(rs.getInt("sCount"));
				result.put(groupID, remSubs);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;	    
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getSubmissionNameMap(long)
	 */
	public Map getSubmissionNameMap(long assignmentID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select SubmissionName, SubmissionID from tRequiredSubmissions " +
					"where AssignmentID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long subID = new Long(rs.getLong("SubmissionID"));
				String subName = rs.getString("SubmissionName");
				result.put(subID, subName);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
	public Map getSubmissionNameMapByCourse(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select SubmissionName, SubmissionID from tRequiredSubmissions r, " +
					"tAssignment a where r.AssignmentID = a.AssignmentID AND a.CourseID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long subID = new Long(rs.getLong("SubmissionID"));
				String subName = rs.getString("SubmissionName");
				result.put(subID, subName);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
	public Map getCommentFileRequestIDMap(long assignmentID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select f.CommentFileID, r.RequestID from tCommentFiles f, tComments c, tGroups g, tRegradeRequests r where " +
					"c.CommentID = f.CommentID and c.CommentID = r.CommentID and " +
					"c.GroupID = g.GroupID and g.AssignmentID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long commentFileID = new Long(rs.getLong("CommentFileID"));
				Collection requests = (Collection) result.get(commentFileID);
				if (requests == null) {
				    requests = new ArrayList();
				}
				Long requestID = new Long(rs.getLong("RequestID"));
				requests.add(requestID);
				result.put(commentFileID, requests);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * @return the number of distinct CUIDs found in the user table
	 */
	public int getCUIDCount() {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		int result = 0;
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT COUNT(DISTINCT CUID) AS Count FROM " +        
					"tUser WHERE (CUID <> ? AND CUID IS NOT NULL)";
			ps = conn.prepareStatement(query);
			ps.setInt(1, 0);
			rs = ps.executeQuery();
			rs.next();
			result = rs.getInt("Count");
			rs.close();
			ps.close();
			conn.close();
		} 
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (rs != null) rs.close();
				if (ps != null) ps.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	public Map getCommentFileRequestIDMapByCourse(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select f.CommentFileID, r.RequestID from tCommentFiles f, tComments c, tGroups g, tAssignment a, tRegradeRequests r where " +
					"c.CommentID = f.CommentID and c.CommentID = r.CommentID and " +
					"c.GroupID = g.GroupID and g.AssignmentID = a.AssignmentID and a.CourseID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long commentFileID = new Long(rs.getLong("CommentFileID"));
				Collection requests = (Collection) result.get(commentFileID);
				if (requests == null) {
				    requests = new ArrayList();
				}
				Long requestID = new Long(rs.getLong("RequestID"));
				requests.add(requestID);
				result.put(commentFileID, requests);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
	public Map getGroupIDMap(long assignmentID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT m.NetID, m.GroupID " +
					"FROM tGroups g INNER JOIN " +
					  "tGroupMembers m ON g.GroupID = m.GroupID " +
					"WHERE (g.AssignmentID = ?) AND (m.Status = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			ps.setString(2, GroupMemberBean.ACTIVE);
			rs = ps.executeQuery();
			while (rs.next()) {
				String netID = rs.getString("NetID");
				Long groupID = new Long(rs.getLong("GroupID"));
				result.put(netID, groupID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;		
	}
	
	public Map getGroupIDMap(String netID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT g.AssignmentID, g.GroupID " +
					"FROM tGroups g INNER JOIN tGroupMembers m ON g.GroupID = m.GroupID " +
					"WHERE (m.NetID = ?) AND (m.Status = ?)";
			ps = conn.prepareStatement(query);
			ps.setString(1, netID);
			ps.setString(2, GroupMemberBean.ACTIVE);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long assignID = new Long(rs.getLong("AssignmentID"));
				Long groupID = new Long(rs.getLong("GroupID"));
				result.put(assignID, groupID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;	
	}
	
	public Map getGroupIDMapByCourse(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT m.NetID, g.GroupID, g.AssignmentID " +
					"FROM tGroups g INNER JOIN tAssignment a ON " +
					"g.AssignmentID = a.AssignmentID INNER JOIN " +
                    "tGroupMembers m ON g.GroupID = m.GroupID " +
                    "WHERE (m.Status = ?) AND (a.CourseID = ?)";
			ps = conn.prepareStatement(query);
			ps.setString(1, GroupMemberBean.ACTIVE);
			ps.setLong(2, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
				String netID = rs.getString("NetID");
				long groupID = rs.getLong("GroupID");
				long assignID = rs.getLong("AssignmentID");
				result.put(netID + "_" + assignID, new Long(groupID));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;	
	}
	
	public Map getGroupMemberListMap(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT m.GroupID, m.NetID " +
					"FROM tGroupMembers m INNER JOIN " +
                    "tGroups g ON m.GroupID = g.GroupID INNER JOIN " +
                    "tAssignment a ON g.AssignmentID = a.AssignmentID " +
                    "WHERE (a.CourseID = ?) AND (m.Status = ?) " +
                    "ORDER BY m.GroupID, m.NetID";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			ps.setString(2, GroupMemberBean.ACTIVE);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long groupID = new Long(rs.getLong("GroupID"));
				if (result.containsKey(groupID)) {
				    String names = (String) result.get(groupID);
				    names += ", " + rs.getString("NetID");
				    result.put(groupID, names);
				} else {
				    result.put(groupID, rs.getString("NetID"));
				}
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;			    
	}
	
	public Map getGroupMembersMap(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = (HashMap)ResultCache.getCache("GroupMembersMap-" + 
				String.valueOf(courseID));
		if(result == null) {
			result = new HashMap();
			try {
				conn = jdbcFactory.getConnection();
				String query = "SELECT m.GroupID, m.NetID " +
						"FROM tGroupMembers m INNER JOIN " +
	                    "tGroups g ON m.GroupID = g.GroupID INNER JOIN " +
	                    "tAssignment a ON g.AssignmentID = a.AssignmentID " +
	                    "WHERE (a.CourseID = ?) AND (m.Status = ?) " +
	                    "ORDER BY m.GroupID, m.NetID";
				ps = conn.prepareStatement(query);
				ps.setLong(1, courseID);
				ps.setString(2, GroupMemberBean.ACTIVE);
				rs = ps.executeQuery();
				while (rs.next()) {
					Long groupID = new Long(rs.getLong("GroupID"));
					ArrayList names = null;
					if (result.containsKey(groupID)) {
					    names = (ArrayList) result.get(groupID);
					} else {
						names = new ArrayList();
					}
					names.add(rs.getString("NetID"));
					result.put(groupID, names);
				}
				conn.close();
				ps.close();
				rs.close();
				
				ResultCache.addCacheEntry("GroupMembersMap-" + 
						String.valueOf(courseID), 
						new String[] {"GroupMember", "Group", "Assignment"}, 
						result);
			} catch (Exception e) {
				try {
					if (conn != null) conn.close();
					if (ps != null) ps.close();
					if (rs != null) rs.close();
				} catch (Exception f) {}
			}
		}
		return result;			    
	}
	
	/* XXX THIS WAS NOT IMPLEMENTED CORRECTLY - no such column as AssignmentID - Alex, Feb 2008 
	public Map getGroupSizeMap(long assignmentID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select count(distinct NetID) as GroupSize, GroupID from tGroupMembers where " +
					"AssignmentID = ? and Status = ? group by GroupID";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			ps.setString(2, GroupMemberBean.ACTIVE);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long groupID = new Long(rs.getLong("GroupID"));
				Integer groupSize = new Integer(rs.getInt("GroupSize"));
				result.put(groupID, groupSize);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;		
	}*/
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getSubProblemIDMap(long)
	 */
	public Map getSubProblemIDMap(long assignmentID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		result.put("Grade", new Long(0));
		try {
			conn = jdbcFactory.getConnection();
			String query = "select SubProblemName, SubProblemID from tSubProblems " +
					"where AssignmentID = ? and Hidden = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long subID = new Long(rs.getLong("SubProblemID"));
				String subName = rs.getString("SubProblemName");
				result.put(subName, subID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;
	}

	public Map getCourseCodeMap() {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select AssignmentID, Code from tAssignment a, tCourse c " + 
					"where c.CourseID = a.CourseID";
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long assignmentID = new Long(rs.getLong("AssignmentID"));
				String code = rs.getString("Code");
				result.put(assignmentID, code);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;
	}
	
	public Map getStaffNameMap(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT u.NetID, u.FirstName, u.LastName FROM " +
					"tUser u INNER JOIN tStaff s ON u.NetID = s.NetID " +
					"WHERE (s.CourseID = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
			    String netID = rs.getString("NetID");
			    String name = rs.getString("FirstName");
			    name = name.length() > 0 ? name + " " + rs.getString("LastName") : rs.getString("LastName");
				result.put(netID, name);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;
	}
	
	public Map getStaffFirstLastNameMap(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT u.NetID, u.FirstName, u.LastName FROM " +
					"tUser u INNER JOIN tStaff s ON u.NetID = s.NetID " +
					"WHERE (s.CourseID = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
			    String netID = rs.getString("NetID");
			    String[] name = { rs.getString("FirstName"), rs.getString("LastName") };
				result.put(netID, name);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;
	}
	
	public Map getStaffNameMap(String studentNetID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT DISTINCT u.NetID, u.FirstName, u.LastName FROM " +
					  "tUser u INNER JOIN tStaff s ON u.NetID = s.NetID INNER JOIN " +
                      "tStudent t ON s.CourseID = t.CourseID " +
                      "WHERE (t.NetID = ?) AND (t.Status = ?)";
			ps = conn.prepareStatement(query);
			ps.setString(1, studentNetID);
			ps.setString(2, StudentBean.ENROLLED);
			rs = ps.executeQuery();
			while (rs.next()) {
			    String netID = rs.getString("NetID");
			    String name = rs.getString("FirstName");
			    name = name.length() > 0 ? name + " " + rs.getString("LastName") : rs.getString("LastName");
				result.put(netID, name);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getSubProblemNameMap(long)
	 */
	public Map getSubProblemNameMap(long assignmentID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select SubProblemName, SubProblemID from tSubProblems " +
					"where AssignmentID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long subID = new Long(rs.getLong("SubProblemID"));
				String subName = rs.getString("SubProblemName");
				result.put(subID, subName);
			}
			rs.close();
			ps.close();
			query = "select Name from tAssignment where AssignmentID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			rs = ps.executeQuery();
			rs.next();
			// Adds the assignment name as sub problem 0 
			result.put(new Long(0), rs.getString("Name"));
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.RootDAO#getSubProblemNameMap(long)
	 */
	public Map getSubProblemNameMapByCourse(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select SubProblemName, SubProblemID from tSubProblems s, tAssignment a " +
					"where s.AssignmentID = a.AssignmentID and a.CourseID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long subID = new Long(rs.getLong("SubProblemID"));
				String subName = rs.getString("SubProblemName");
				result.put(subID, subName);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.RootDAO#getCategoryIDMap(long)
     */
    public Map getCategoryIDMap(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT c.CategoryID, cn.ContentID " +
					"FROM tCtgContents cn INNER JOIN " +
                      "tCategoryRow r ON cn.RowID = r.RowID INNER JOIN " +
                      "tCategori c ON r.CategoryID = c.CategoryID " +
                    "WHERE (c.CourseID = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long contentID = new Long(rs.getLong("ContentID"));
				Long categoryID = new Long(rs.getLong("CategoryID"));
				result.put(contentID, categoryID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.RootDAO#getCommentFileGroupIDMap(long)
     */
    public Map getCommentFileGroupIDMap(long assignmentID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT f.CommentFileID, g.GroupID " +
					"FROM tCommentFiles f INNER JOIN " +
					  "tComments c ON f.CommentID = c.CommentID INNER JOIN " +
					  "tGroups g ON c.GroupID = g.GroupID " +
					"WHERE (g.AssignmentID = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long commentFileID = new Long(rs.getLong("CommentFileID"));
				Long groupID = new Long(rs.getLong("GroupID"));
				result.put(commentFileID, groupID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;	
    }

    public Map getCommentFileGroupIDMapByCourse(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT f.CommentFileID, g.GroupID " +
					"FROM tCommentFiles f INNER JOIN " +
					  "tComments c ON f.CommentID = c.CommentID INNER JOIN " +
					  "tGroups g ON c.GroupID = g.GroupID INNER JOIN " +
					  "tAssignment a ON g.AssignmentID = a.AssignmentID " +
					"WHERE (a.CourseID = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long commentFileID = new Long(rs.getLong("CommentFileID"));
				Long groupID = new Long(rs.getLong("GroupID"));
				result.put(commentFileID, groupID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;	
    }
    
    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.RootDAO#getNameMap(long)
     */
    public Map getNameMap(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT u.FirstName, u.LastName, u.NetID " +
					"FROM tUser u INNER JOIN tStudent s ON u.NetID = s.NetID " +
					"WHERE (s.CourseID = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
			    String netID = rs.getString("NetID");
			    String name = rs.getString("FirstName") + " " + rs.getString("LastName");
				result.put(netID, name);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;	
    }

    public Map getFirstLastNameMap(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = (HashMap)ResultCache.getCache("FirstLastNameMap-" + 
				String.valueOf(courseID));
		if(result == null) {
			result = new HashMap();
			try {
				conn = jdbcFactory.getConnection();
				String query = "SELECT u.FirstName, u.LastName, s.NetID " +
						"FROM tUser u INNER JOIN tStudent s ON u.NetID = s.NetID " +
						"WHERE (s.CourseID = ?)";
				ps = conn.prepareStatement(query);
				ps.setLong(1, courseID);
				rs = ps.executeQuery();
				while (rs.next()) {
				    String netID = rs.getString("NetID");
				    String[] name = { rs.getString("FirstName"),  rs.getString("LastName") };
					result.put(netID, name);
				}
				conn.close();
				ps.close();
				rs.close();
				
				ResultCache.addCacheEntry("FirstLastNameMap-" + 
						String.valueOf(courseID), 
						new String[] {"User", "Student"}, 
						result);
			} catch (Exception e) {
				try {
					if (conn != null) conn.close();
					if (ps != null) ps.close();
					if (rs != null) rs.close();
				} catch (Exception f) {}
			}
		}
		return result;	
    }
    
    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.RootDAO#getAssignmentIDMap(long)
     */
    public Map getAssignmentIDMap(long courseID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap result = new HashMap();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT g.GroupID, g.AssignmentID FROM tGroups g INNER JOIN " +
				"tAssignment a ON g.AssignmentID = a.AssignmentID " +
				"WHERE a.CourseID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			rs = ps.executeQuery();
			while (rs.next()) {
			    Long groupID = new Long(rs.getLong("GroupID"));
			    Long assignmentID = new Long(rs.getLong("AssignmentID"));
				result.put(groupID, assignmentID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;	
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.RootDAO#findGradeLogDetails(long, long[])
     */
    public Collection findGradeLogDetails(long courseID, Collection groupids) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT l.LogID, l.ActingNetID, l.SimulatedNetID," +
					  " l.ActingIPAddress, l.Dte, l.LogName, l.LogType," +
					  " d.AssignmentID, d.NetID, d.Details FROM tLogs l INNER JOIN " +
                      "tLogDetails d ON l.LogID = d.LogID INNER JOIN " +
                      "tGroupMembers m ON m.NetID = d.NetID INNER JOIN " +
                      "tGroups g ON g.GroupID = m.GroupID AND g.AssignmentID = d.AssignmentID " +
                      "WHERE (m.Status = ?) AND (l.LogName = ? OR l.LogName = ? OR l.LogName = ?) AND " + 
                      "(l.CourseID = ?) AND (";
			for (int i=0; i < groupids.size() - 1; i++) {
			    query += "g.GroupID = ? or ";	
			}
			query += "g.GroupID = ?) ORDER BY l.Dte DESC";
			ps = conn.prepareStatement(query);
			ps.setString(1, GroupMemberBean.ACTIVE);
			ps.setString(2, LogBean.EDIT_GRADES);
			ps.setString(3, LogBean.UPLOAD_GRADES_FILE);
			ps.setString(4, LogBean.COMPUTE_ASSIGNMENT_STATS);
			ps.setLong(5, courseID);
			int c = 6;
			for (Iterator i = groupids.iterator(); i.hasNext(); ) {
			    ps.setLong(c++, ((Long)i.next()).longValue());
			}
			rs = ps.executeQuery();
			HashMap logdatas = new HashMap();
			while (rs.next()) {
				Long logID = new Long(rs.getLong("LogID"));
			    LogData data = (LogData) logdatas.get(logID);
				if (data == null) {
				    data = new LogData();
				    data.setLogID(logID.longValue());
					data.setActingNetID(rs.getString("ActingNetID"));
					data.setSimulatedNetID(rs.getString("SimulatedNetID"));
					data.setActingIPAddress(InetAddress.getByName(rs.getString("ActingIPAddress")));
					data.setTimestamp(rs.getTimestamp("Dte"));
					data.setLogName(rs.getString("LogName"));
					data.setLogType(rs.getLong("LogType"));
					logdatas.put(logID, data);
				}
			    LogDetail d = new LogDetail(logID.longValue());
			    d.setAssignmentID(new Long(rs.getLong("AssignmentID")));
			    d.setNetID(rs.getString("NetID"));
			    d.setDetailString(rs.getString("Details"));
			    d.setLogRef(data);
			    result.add(d);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
    }
    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.RootDAO#hasSoloCourse(java.lang.String)
     */
    public Long hasSoloCourse(String netID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long result = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT c.CourseID FROM tStudent s INNER JOIN " +
                      "tCourse c ON s.CourseID = c.CourseID INNER JOIN " +
                      "tSystemProperties p ON p.CurrentSemester = c.SemesterID " +
                      "WHERE (s.NetID = ?) AND (s.Status = ?) AND (p.ID = ?)";
			ps = conn.prepareStatement(query);
			ps.setString(1, netID);
			ps.setString(2, StudentBean.ENROLLED);
			ps.setInt(3, 1);
			rs = ps.executeQuery();
			if (rs.next()) {
			    result = new Long(rs.getLong("CourseID"));
			    if (rs.next()) {
			        conn.close();
			        ps.close();
			        rs.close();
			        return null;
			    }
			}
			query = "SELECT c.CourseID FROM tStaff s INNER JOIN " +
            		"tCourse c ON s.CourseID = c.CourseID INNER JOIN " +
            		"tSystemProperties p ON p.CurrentSemester = c.SemesterID " +
            		"WHERE (s.NetID = ?) AND (s.Status = ?) AND (p.ID = ?)";
			ps = conn.prepareStatement(query);
			ps.setString(1, netID);
			ps.setString(2, StaffBean.ACTIVE);
			ps.setInt(3, 1);
			rs = ps.executeQuery();
			if (rs.next()) {
			    if (result != null) {
			        conn.close();
			        ps.close();
			        rs.close();
			        return null;
			    }
			    result = new Long(rs.getLong("CourseID"));
			    if (rs.next()) {
			        conn.close();
			        ps.close();
			        rs.close();
			        return null;
			    }
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			e.printStackTrace();
		}
		return result;	
    }
    
    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.RootDAO#hasSoloCourseBySemester(java.lang.String, long)
     */
    public Long hasSoloCourseBySemester(String netID, long semesterID) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long result = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT c.CourseID FROM tStudent s INNER JOIN " +
                      "tCourse c ON s.CourseID = c.CourseID " +
                      "WHERE (s.NetID = ?) AND (s.Status = ?) AND (c.SemesterID = ?)";
			ps = conn.prepareStatement(query);
			ps.setString(1, netID);
			ps.setString(2, StudentBean.ENROLLED);
			ps.setLong(3, semesterID);
			rs = ps.executeQuery();
			if (rs.next()) {
			    if (rs.next()) {
			        conn.close();
			        ps.close();
			        rs.close();
			        return null;
			    }
			    result = new Long(rs.getLong("CourseID"));
			}
			query = "SELECT s.CourseID FROM tStaff s INNER JOIN " +
            		"tCourse c ON s.CourseID = c.CourseID " +
            		"WHERE (s.NetID = ?) AND (s.Status = ?) AND (c.SemesterID = ?)";
			ps = conn.prepareStatement(query);
			ps.setString(1, netID);
			ps.setString(2, StaffBean.ACTIVE);
			ps.setLong(3, semesterID);
			if (rs.next()) {
			    if (result != null) {
			        conn.close();
			        ps.close();
			        rs.close();
			        return null;
			    }
			    result = new Long(rs.getLong("CourseID"));
			    if (rs.next()) {
			        conn.close();
			        ps.close();
			        rs.close();
			        return null;
			    }
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
		}
		return result;	
    }
    
    public boolean isAssignedTo(String netID, Collection groupIDs) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean result = false;
        if (groupIDs.size() == 0) return true;
        try {
            conn = jdbcFactory.getConnection();
            String query = "SELECT COUNT(DISTINCT GroupID) AS GroupCount FROM tGroupAssignedTo " +
            		"WHERE (NetID = ?) AND (";
            for (int i=0; i < groupIDs.size() - 1; i++) {
                query += "GroupID = ? OR ";
            }
            query += "GroupID = ?) GROUP BY NetID";
            ps = conn.prepareStatement(query);
            ps.setString(1, netID);
            Iterator groups = groupIDs.iterator();
            int i = 2;
            while (groups.hasNext()) {
                Long groupID = (Long) groups.next();
                ps.setLong(i++, groupID.longValue());
            }
            rs = ps.executeQuery();
            if (rs.next()) {
                int count = rs.getInt("GroupCount");
                result = count == groupIDs.size();
            }
            conn.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                if (conn != null) conn.close();
                if (ps != null) ps.close();
                if (rs != null) rs.close();
            } catch (Exception f) {}
        }
        return result;
    }
 
    public Collection getAllUsers() {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList result = new ArrayList();
        try {
            conn = jdbcFactory.getConnection();
            String query = "SELECT * FROM tUser";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            if (rs.next()) {
            	UserData user = new UserData();
            	user.setUserID(rs.getString("NetID"));
            	user.setFirstName(rs.getString("FirstName"));
            	user.setLastName(rs.getString("LastName"));
            	user.setCUID(rs.getString("CUID"));
            	user.setCollege(rs.getString("College"));
            	result.add(user);
            }
            conn.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                if (conn != null) conn.close();
                if (ps != null) ps.close();
                if (rs != null) rs.close();
            } catch (Exception f) {}
        }
        return result;
    }
    
}
