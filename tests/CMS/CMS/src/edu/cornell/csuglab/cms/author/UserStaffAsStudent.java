/*
 * Created on Apr 7, 2005
 */
package edu.cornell.csuglab.cms.author;

import edu.cornell.csuglab.cms.base.*;

/**
 * @author yc263
 *
 * UserStaffAsCornellMember represents the apparent student(not authenticated) user 
 * for a staff who has decided to view it's course page as a specified student in it's course
 */
public class UserStaffAsStudent extends UserWrapper{
	public UserStaffAsStudent(long courseID, UserLocal user){
		super(courseID, user);
	}
	
	public int getUserType(){
		return UserWrapper.UT_STAFF_AS_STUDENT;
	}
	
	public int getAuthoriznLevelByCourseID(long courseID) {
		return Principal.AUTHOR_STUDENT;
	}
	
	public String getUserID() {
		return user.getUserID();
	}
	
	public String getCUID() {
		return user.getCUID();
	}
	
	public String getFirstName()  {
		return user.getFirstName();
	}
	
	public String getLastName()  {
		return user.getLastName();
	}

	public String getCollege()  {
		return user.getCollege();
	}
	
	public UserData getUserData()  {
		return user.getUserData();	
	} 
	
	public boolean isStudentInCourseByCourseID(long courseID) {
		boolean result = user.isStudent(courseID);
		return result;
	}

}
