/**
 * Provide data about a category: list contents; list files; list columns
 */
package edu.cornell.csuglab.cms.util.category;

import java.util.Collection;

import edu.cornell.csuglab.cms.base.CategoryColData;

/**
 * A CategoryDataProvider can give out all kinds of information about a single certain category.
 * It's a snapshot of the category; each getter function always returns data representing the category
 * at the time the Provider was created.
 * 
 * Subclasses:
 * CategoryBeanDataProvider (provide information from a live bean)
 * CoursewideCategoryDataProvider (initialize with all info for a given course, give out per category)
 */
public interface CategoryDataProvider
{
	/**
	 * 
	 * @return The ID of the category we represent
	 */
	public long getCategoryID() ;
	
	/**
	 * Return a Collection of contents belonging to our category,
	 * limiting search to visible rows and/or columns as desired
	 * @param visibleRows Whether to limit our search to contents whose rows are visible
	 * @param visibleCols Whether to limit our search to contents whose columns are visible
	 * @return A Collection of CategoryContentsDatas
	 */
	public Collection getContentsCollection(boolean visibleRows, boolean visibleCols) ;
	
	/**
	 * Return a list of all files belonging to our category, ordered by ID of parent content
	 * @return A Collection of CategoryFileDatas
	 */
	public Collection getFilesCollection() ;
	
	/**
	 * Return a list of all columns belonging to our category, restricting to visible columns if desired,
	 * ordered by position within the category, but excluding removed columns
	 * @param visible Whether to limit our search to visible columns
	 * @return A Collection of CategoryColumnDatas
	 */
	public Collection getNonremovedColumnsCollection(boolean visible) ;
	
	/**
	 * Return a list of all currently removed columns belonging to our category
	 * @return A Collection of CategoryColumnDatas
	 */
	public Collection getRemovedColumnsCollection();
	
	/**
	 * Return the column data for the nonremoved column with the given id, or null if the ID isn't found in our list
	 * @param colID
	 * @return CategoryColData
	 */
	public CategoryColData findColumnByID(long colID) ;
}
