/*
 * Created on Mar 30, 2004
 */

package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.GradeBean;
import edu.cornell.csuglab.cms.base.GradeDAO;
import edu.cornell.csuglab.cms.base.GradePK;
import edu.cornell.csuglab.cms.base.GroupMemberBean;
import edu.cornell.csuglab.cms.base.StudentBean;
import edu.cornell.csuglab.cms.www.util.ResultCache;

/**
 * @author Theodore Chao
 */
public class GradeDAOImpl extends DAOMaster implements GradeDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GradeDAO#load(edu.cornell.csuglab.cms.base.GradePK,
	 *      edu.cornell.csuglab.cms.base.GradeBean)
	 */
	public void load(GradePK pk, GradeBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tgrades where GradeID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getGradeID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setGradeID(rs.getLong("GradeID"));
				ejb.setAssignmentID(rs.getLong("assignmentid"));
				ejb.setSubProblemID(rs.getLong("subproblemid"));
				ejb.setNetID(rs.getString("netid"));
				float score = rs.getFloat("score");
				if (rs.wasNull()) ejb.setGrade(null);
				else ejb.setGrade(new Float(score));
				ejb.setGrader(rs.getString("gradernetid"));
				ejb.setDateEntered(rs.getTimestamp("dateentered"));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new EJBException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GradeDAO#store(edu.cornell.csuglab.cms.base.GradeBean)
	 */
	public void store(GradeBean ejb) throws EJBException {
		preStore("Grade", ejb);
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String update = "update tgrades "
					+ "set score = ?, gradernetid = ?, dateentered = ? "
					+ "where gradeid = ? and assignmentid = ? and subproblemid = ? and netid = ?";
			ps = conn.prepareStatement(update);
			if (ejb.getGrade() == null) ps.setNull(1, java.sql.Types.FLOAT);
			else ps.setFloat(1, ejb.getGrade().floatValue());
			if(ejb.getGrader() == null) ps.setNull(2, java.sql.Types.VARCHAR);
			else ps.setString(2, ejb.getGrader());
			ps.setTimestamp(3, ejb.getDateEntered());
			ps.setLong(4, ejb.getGradeID());
			ps.setLong(5, ejb.getAssignmentID());
			ps.setLong(6, ejb.getSubProblemID());
			if(ejb.getNetID() == null) ps.setNull(7, java.sql.Types.VARCHAR);
			else ps.setString(7, ejb.getNetID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new EJBException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GradeDAO#remove(edu.cornell.csuglab.cms.base.GradePK)
	 */
	public void remove(GradePK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		preRemove("Grade");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GradeDAO#create(edu.cornell.csuglab.cms.base.GradeBean)
	 */
	public GradePK create(GradeBean ejb) throws CreateException, EJBException {
		preCreate("Grade", ejb);
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		GradePK result = null;
		try {
			conn = jdbcFactory.getConnection();
			String createString = "insert into tGrades "
					+ "(assignmentid, SubProblemID, NetID, Score, GraderNetID, DateEntered) " +
					"values (?, ?, ?, ?, ?, ?)";
			String findString = "select @@identity as 'GradeID' from tGrades";
			ps = conn.prepareStatement(createString);
			ps.setLong(1, ejb.getAssignmentID());
			ps.setLong(2, ejb.getSubProblemID());
			if(ejb.getNetID() == null) ps.setNull(3, java.sql.Types.VARCHAR);
			else ps.setString(3, ejb.getNetID());
			if (ejb.getGrade() == null) ps.setNull(4, java.sql.Types.FLOAT);
			else ps.setFloat(4, ejb.getGrade().floatValue());
			if(ejb.getGrader() == null) ps.setNull(5, java.sql.Types.VARCHAR);
			else ps.setString(5, ejb.getGrader());
			ps.setTimestamp(6, ejb.getDateEntered());
			ps.executeUpdate();
			rs = conn.prepareStatement(findString).executeQuery();
			if (rs.next()) {
				long gradeID = rs.getLong("GradeID");
				result = new GradePK(gradeID);
				ejb.setGradeID(gradeID);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			} catch (Exception f) {}
			throw new CreateException(e.getMessage());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GradeDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.GradePK)
	 */
	public GradePK findByPrimaryKey(GradePK key) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "select GradeID from tgrades where GradeID = ?";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, key.getGradeID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find grade with ID = " + key.getGradeID());
			}
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return key;
	}

	public Collection findByGroupIDs(Collection groupids) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (groupids.size() == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT GradeID FROM (SELECT g.NetID, g.SubProblemID, g.AssignmentID, MAX(g.DateEntered) as LatestDate FROM tGrades g INNER JOIN " +
					"tGroupMembers m ON g.NetID = m.NetID INNER JOIN " + 
                    "tGroups gr ON m.GroupID = gr.GroupID AND g.AssignmentID = gr.AssignmentID WHERE (";
			for (int i=0; i < groupids.size() - 1; i++) {
				queryString += "gr.GroupID = ? or ";
			}
			queryString += "gr.GroupID = ?) group by g.NetID, g.SubProblemID, g.AssignmentID) l INNER JOIN "
				+ "tGrades d ON l.NetID = d.NetID AND l.AssignmentID = d.AssignmentID AND l.SubProblemID = d.SubProblemID AND l.LatestDate = d.DateEntered "
				+ "WHERE d.Score IS NOT NULL";
			ps = conn.prepareStatement(queryString);
			int c=1;
			for (Iterator i=groupids.iterator(); i.hasNext(); ) {
				ps.setLong(c++, ((Long)i.next()).longValue());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				GradePK pk = new GradePK(rs.getLong(1));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	public Collection findByGroupIDsAssignedTo(Collection groupids, String grader, int numSubProbs) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (groupids.size() == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT GradeID FROM (SELECT g.NetID, g.AssignmentID, g.SubProblemID, MAX(DateEntered) AS LatestDate " +
			    	"FROM tGrades g INNER JOIN " +
			    	"tGroupMembers m ON g.NetID = m.NetID INNER JOIN " + 
			        "tGroups gr ON m.GroupID = gr.GroupID AND g.AssignmentID = gr.AssignmentID INNER JOIN " +
			        "tGroupAssignedTo a ON gr.GroupID = a.GroupID LEFT OUTER JOIN " +
			        "(SELECT x.GroupID, COUNT(DISTINCT x.SubProblemID) AS Assigned " +
                       "FROM tGroupAssignedTo x INNER JOIN tSubProblems y ON x.SubProblemID = y.SubProblemID " +
                       "WHERE (x.NetID = ?) AND (y.Hidden = ?) GROUP BY x.GroupID) t ON t.GroupID = gr.GroupID " +
			        "WHERE (a.NetID = ?) AND (Assigned = ? OR a.SubProblemID = g.SubProblemID) AND (";
			for (int i=0; i < groupids.size() - 1; i++) {
				queryString += "gr.GroupID = ? or ";
			}
			queryString += "gr.GroupID = ?) group by g.NetID, g.AssignmentID, g.SubProblemID) l "
				+ "INNER JOIN tGrades d ON l.NetID = d.NetID AND l.AssignmentID = d.AssignmentID AND l.SubProblemID = d.SubProblemID AND l.LatestDate = d.DateEntered "
				+ "WHERE d.Score IS NOT NULL";
			ps = conn.prepareStatement(queryString);
			ps.setString(1, grader);
			ps.setBoolean(2, false);
			ps.setString(3, grader);
			ps.setInt(4, numSubProbs);
			int c=5;
			for (Iterator i=groupids.iterator(); i.hasNext(); ) {
				ps.setLong(c++, ((Long)i.next()).longValue());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				GradePK pk = new GradePK(rs.getLong(1));
				result.add(pk);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.cornell.csuglab.cms.base.GradeDAO#findMostRecentByNetAssignmentID(java.lang.String,
	 *      long)
	 */
	public Collection findMostRecentByNetAssignmentID(String netid,
			long assignmentid) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String queryString = "SELECT g.GradeID "
					+ "FROM (SELECT subproblemid, max(dateentered) as currentdate "
					+ "FROM tgrades WHERE assignmentid = ? AND netid = ? "
					+ "GROUP BY subproblemid ) AS t, tgrades g "
					+ "WHERE g.assignmentid = ? AND netid = ? "
					+ "AND g.subproblemid = t.subproblemid AND dateentered = currentdate AND "
					+ "g.Score IS NOT NULL";
			ps = conn.prepareStatement(queryString);
			ps.setLong(1, assignmentid);
			ps.setString(2, netid);
			ps.setLong(3, assignmentid);
			ps.setString(4, netid);
			rs = ps.executeQuery();
			while (rs.next()) {
				GradePK cd = new GradePK(rs.getLong(1));
				result.add(cd);
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.GradeDAO#findMostRecentByAssignmentID(long)
	 */
	public Collection findMostRecentByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null, getSubProbs = null;
		ResultSet rs = null, subProbs = null;
		Collection result = (Collection)ResultCache.getCache("MostRecentGradesByAssignmentID-" + 
				String.valueOf(assignmentID));
		if(result == null) {
			result = new ArrayList();
			try {
				conn = jdbcFactory.getConnection();
				String querySubProbs = "select * from tSubProblems where AssignmentID = ? and Hidden = ?";
				getSubProbs = conn.prepareStatement(querySubProbs);
				getSubProbs.setLong(1, assignmentID);
				getSubProbs.setBoolean(2, false);
				subProbs = getSubProbs.executeQuery();
				boolean hasSubProblems = subProbs.next();
				/*
				String queryWithProbs = "select distinct GradeID, g.NetID, g.SubProblemID from " +
						"(select SubProblemID, NetID, max(DateEntered) as LatestDate from tGrades where AssignmentID = ? group by SubProblemID, NetID) l, " +
						"(select min(SubProblemName) as DefName from tSubProblems where AssignmentID = ? and Hidden = ?) m, tSubProblems p, tGrades g " +
						"where (g.SubProblemID = l.SubProblemID and g.NetID = l.NetID and g.DateEntered = l.LatestDate AND g.Score IS NOT NULL) and " +
						"((p.Hidden = ? and p.SubProblemID = g.SubProblemID) or (g.SubProblemID = ? and p.SubProblemName = m.DefName)) order by g.NetID ASC, g.SubProblemID ASC";
				*/
				String queryWithProbs = "SELECT DISTINCT GradeID, g.NetID, g.SubProblemID FROM " +
						"(SELECT SubProblemID, NetID, MAX(DateEntered) AS LatestDate FROM tGrades " +
					    "WHERE AssignmentID = ? " +
					    "GROUP BY SubProblemID, NetID) l " +
					    "INNER JOIN tGrades g ON l.SubProblemID = g.SubProblemID AND l.NetID = g.NetID AND l.LatestDate = g.DateEntered " +
					    "LEFT OUTER JOIN tSubProblems p ON l.SubProblemID = p.SubProblemID " +
					    "WHERE (p.Hidden = ?) AND (g.Score IS NOT NULL) AND (p.SubProblemID = g.SubProblemID) OR (g.Score IS NOT NULL) AND (g.SubProblemID = ?)" +
					    "ORDER BY g.NetID ASC, g.SubProblemID ASC";
				
				String queryWithNoProbs = "select distinct GradeID,g.NetID from " +
						"(select NetID,SubProblemID,max(DateEntered) as LatestDate from tGrades where AssignmentID = ? and SubProblemID = ? group by NetID,SubProblemID) l, tGrades g " +
						"where g.NetID = l.NetID and g.SubProblemID = l.SubProblemID and g.DateEntered = l.LatestDate and g.Score IS NOT NULL order by g.NetID ASC";
				if (hasSubProblems) {
					ps = conn.prepareStatement(queryWithProbs);
					ps.setLong(1, assignmentID);
					ps.setBoolean(2, false);
					ps.setLong(3, 0);
				} else {
					ps = conn.prepareStatement(queryWithNoProbs);
					ps.setLong(1, assignmentID);
					ps.setLong(2, 0);
				}
				rs = ps.executeQuery();
				int count = 0;
				while (rs.next()) {
					count++;
					result.add(new GradePK(rs.getLong("GradeID")));
				}
				conn.close();
				ps.close();
				rs.close();
				getSubProbs.close();
				subProbs.close();
				
				ResultCache.addCacheEntry("MostRecentGradesByAssignmentID-" + 
						String.valueOf(assignmentID), 
						new String[] {"Grade"}, 
						result);
				
			} catch (Exception e) {
			    try {
			        if (conn != null) conn.close();
			        if (ps != null) ps.close();
			        if (rs != null) rs.close();
			        if (getSubProbs != null) getSubProbs.close();
			        if (subProbs != null) subProbs.close();
			    } catch (Exception f) {}
			    e.printStackTrace();
			    throw new FinderException(e.getMessage());
			}
		}
		return result;
	}
	
	public Collection findMostRecentByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
	        String query = "SELECT r.GradeID FROM " +
	        		"(SELECT g.AssignmentID, g.NetID, g.SubProblemID, MAX(g.DateEntered) AS LatestDate " +
	        		 "FROM tGrades g INNER JOIN tStudent s ON g.NetID = s.NetID INNER JOIN " +
	        		 "tAssignment a ON g.AssignmentID = a.AssignmentID " + 
	                 "WHERE (s.CourseID = ?) AND (s.Status = ?) AND (a.Hidden = ?) " +
	                 "GROUP BY g.AssignmentID, g.NetID, g.SubProblemID) t INNER JOIN " +
	                 "tGrades r ON t.SubProblemID = r.SubProblemID AND t.NetID = r.NetID AND t.AssignmentID = r.AssignmentID AND t.LatestDate = r.DateEntered " +
					 "WHERE r.Score IS NOT NULL";
	 		ps = conn.prepareStatement(query);
			ps.setLong(1, courseID);
			ps.setString(2, StudentBean.ENROLLED);
			ps.setBoolean(3, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GradePK(rs.getLong("GradeID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;
	    
	}
	
	public Collection findTotalsByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
	        String query = "SELECT r.GradeID FROM " +
	        		"(SELECT g.AssignmentID, g.NetID, g.SubProblemID, MAX(g.DateEntered) AS LatestDate " +
	        		 "FROM tGrades g INNER JOIN tStudent s ON g.NetID = s.NetID INNER JOIN " +
	        		 "tAssignment a ON g.AssignmentID = a.AssignmentID AND s.CourseID = a.CourseID " +
	                 "WHERE (s.CourseID = ?) AND (s.Status = ?) AND (g.SubProblemID = ?) AND (a.Hidden = ?) " +
	                 "GROUP BY g.AssignmentID, g.NetID, g.SubProblemID) t INNER JOIN " +
	                 "tGrades r ON t.SubProblemID = r.SubProblemID AND t.NetID = r.NetID AND t.AssignmentID = r.AssignmentID AND t.LatestDate = r.DateEntered " +
					 "WHERE r.Score IS NOT NULL";
	 		ps = conn.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, 
	 				ResultSet.CONCUR_READ_ONLY);
	 		ps.setFetchSize(500);
			ps.setLong(1, courseID);
			ps.setString(2, StudentBean.ENROLLED);
			ps.setLong(3, 0);
			ps.setBoolean(4, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GradePK(rs.getLong("GradeID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;
	}

	public Collection findOverMaxByCourseID(long courseID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT g.GradeID FROM tGrades g INNER JOIN " +
	            "(SELECT gr.netid, gr.assignmentid, MAX(dateentered) AS dateentered, duedate, p.GroupID " +
	            "FROM tgrades gr, tGroups p, tGroupMembers m, tassignment a " +
	            "WHERE gr.assignmentid = a.assignmentid AND subproblemid = ? AND a.courseid = ? AND p.GroupID = m.GroupID AND " +
	            "p.AssignmentID = a.AssignmentID AND m.NetID = gr.NetID AND m.Status = ? AND a.Hidden = ? " +
	            "GROUP BY gr.netid, gr.assignmentid, duedate, p.GroupID) m ON g.DateEntered = m.dateentered AND g.NetID = m.netid AND " +
	            "g.AssignmentID = m.assignmentid INNER JOIN " +
	            "tAssignment a ON a.AssignmentID = m.assignmentid AND g.Score > a.MaxScore " +
	            "WHERE (g.SubProblemID = ?) AND (g.Score IS NOT NULL)";
	 		ps = conn.prepareStatement(query);
			ps.setLong(1, 0);
			ps.setLong(2, courseID);
			ps.setString(3, GroupMemberBean.ACTIVE);
			ps.setBoolean(4, false);
			ps.setLong(5, 0);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GradePK(rs.getLong("GradeID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;
	}
	
    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.GradeDAO#findMostRecentByNetID(java.lang.String, long, long)
     */
    public GradePK findMostRecentByNetID(String netid, long assignmentid, long subProblemID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		GradePK result = null;
		try {
			conn = jdbcFactory.getConnection();
	        String query = "SELECT g.GradeID " +
	        	"FROM (SELECT NetID, AssignmentID, SubProblemID, MAX(DateEntered) AS LatestDate " +
	             	"FROM tGrades " +
	             	"WHERE (SubProblemID = ?) AND (NetID = ?) AND (AssignmentID = ?) " +
	                "GROUP BY NetID, AssignmentID, SubProblemID) t INNER JOIN " + 
	                "tGrades g ON t.SubProblemID = g.SubProblemID AND t.NetID = g.NetID AND t.AssignmentID = g.AssignmentID AND t.LatestDate = g.DateEntered " +
					"WHERE g.Score IS NOT NULL";
	 		ps = conn.prepareStatement(query);
			ps.setLong(1, subProblemID);
			ps.setString(2, netid);
			ps.setLong(3, assignmentid);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = new GradePK(rs.getLong("GradeID"));
			} else throw new FinderException("Could not find a grade with for NetID = " + netid + ", AssignmentID = " + assignmentid + ", SubProblemID = " + subProblemID);
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.GradeDAO#findMostRecentByNetIDCourseID(java.lang.String, long)
     */
    public Collection findMostRecentByNetIDCourseID(String netid, long courseid) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
	        String query = "SELECT g.GradeID " +
	        		"FROM  (SELECT NetID, a.AssignmentID, SubProblemID, MAX(DateEntered) AS LatestDate " +
                       "FROM tGrades g INNER JOIN tAssignment a ON g.AssignmentID = a.AssignmentID " +
                       "WHERE (g.NetID = ?) AND (a.CourseID = ?) AND (a.Hidden = ?) " +
                       "GROUP BY NetID, a.AssignmentID, SubProblemID) t INNER JOIN " +
                       "tGrades g ON t.SubProblemID = g.SubProblemID AND t.NetID = g.NetID AND t.AssignmentID = g.AssignmentID AND t.LatestDate = g.DateEntered " +
                       "WHERE g.Score IS NOT NULL ORDER BY g.SubProblemID, g.DateEntered";
	 		ps = conn.prepareStatement(query);
			ps.setString(1, netid);
			ps.setLong(2, courseid);
			ps.setBoolean(3, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GradePK(rs.getLong("GradeID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;
    }

    public Collection findRecentByNetIDCourseID(String netid, long courseid, boolean admin, String grader) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
	        String query = "SELECT s.GradeID FROM tGrades s INNER JOIN " +
                           "(SELECT gr.NetID, gr.AssignmentID, gr.SubProblemID, MAX(gr.DateEntered) AS LatestDate " +
                           "FROM tGroups g CROSS JOIN tGroupAssignedTo ga INNER JOIN " +
                           "tGroupMembers me ON g.GroupID = me.GroupID INNER JOIN " +
                           "tGroupMembers you ON g.GroupID = you.GroupID INNER JOIN " +
                           "tGrades gr ON gr.NetID = you.NetID INNER JOIN " +
                           "tAssignment a ON gr.AssignmentID = a.AssignmentID AND g.AssignmentID = a.AssignmentID " +
                           "WHERE ((a.AssignedGraders = ? OR a.AssignedGraders = ?) OR " +
                           	"((ga.SubProblemID = gr.SubProblemID) AND (ga.GroupID = g.GroupID) AND (ga.NetID = ?))) AND " +
                           	"(a.CourseID = ?) AND (me.NetID = ?) AND (me.Status = ?) AND (you.Status = ?) AND (a.Hidden = ?) " + 
                            "GROUP BY gr.NetID, gr.AssignmentID, gr.SubProblemID) t ON s.NetID = t.NetID AND s.AssignmentID = t.AssignmentID AND " +
                            "s.SubProblemID = t.SubProblemID AND s.DateEntered = t.LatestDate " +
							"WHERE s.Score IS NOT NULL";
	 		ps = conn.prepareStatement(query);
	 		ps.setBoolean(1, false);
	 		ps.setBoolean(2, admin);
	 		ps.setString(3, grader);
			ps.setLong(4, courseid);
			ps.setString(5, netid);
			ps.setString(6, GroupMemberBean.ACTIVE);
			ps.setString(7, GroupMemberBean.ACTIVE);
			ps.setBoolean(8, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new GradePK(rs.getLong("GradeID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;
    }

    /* (non-Javadoc)
     * @see edu.cornell.csuglab.cms.base.GradeDAO#findMostRecentByAssignmentIDGrader(long, java.lang.String)
     */
    public Collection findMostRecentByAssignmentIDGrader(long assignmentID, String grader) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT a.GradeID, a.NetID, a.SubProblemID " +
					"FROM tGrades a INNER JOIN " +
                    "(SELECT gr.NetID, gr.SubProblemID, gr.AssignmentID, MAX(DateEntered) AS LatestDate " +
                    "FROM tGrades gr INNER JOIN " +
                    "tGroupMembers m ON gr.NetID = m.NetID INNER JOIN " +
                    "tGroups g ON g.GroupID = m.GroupID INNER JOIN " +
                    "tAssignment a ON gr.AssignmentID = a.AssignmentID AND g.AssignmentID = a.AssignmentID INNER JOIN " +
                    "tGroupAssignedTo ga ON ga.GroupID = g.GroupID " +
                    "WHERE a.AssignmentID = ? AND ga.NetID = ? AND m.Status = ? " +
                    "GROUP BY gr.NetID, gr.SubProblemID, gr.AssignmentID) b ON a.NetID = b.NetID AND a.SubProblemID = b.SubProblemID AND " +
                    "a.AssignmentID = b.AssignmentID AND a.DateEntered = b.LatestDate " +
                    "WHERE a.Score IS NOT NULL ORDER BY a.NetID, a.SubProblemID";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			ps.setString(2, grader);
			ps.setString(3, GroupMemberBean.ACTIVE);
			rs = ps.executeQuery();
			int count = 0;
			while (rs.next()) {
				count++;
				result.add(new GradePK(rs.getLong("GradeID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
		    try {
		        if (conn != null) conn.close();
		        if (ps != null) ps.close();
		        if (rs != null) rs.close();
		    } catch (Exception f) {}
		    e.printStackTrace();
		    throw new FinderException(e.getMessage());
		}
		return result;

    }

}