/*
 * Created on Aug 1, 2004
 *
 */
package edu.cornell.csuglab.cms.base.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import edu.cornell.csuglab.cms.base.SubmittedFileBean;
import edu.cornell.csuglab.cms.base.SubmittedFileDAO;
import edu.cornell.csuglab.cms.base.SubmittedFilePK;
import edu.cornell.csuglab.cms.www.util.FileUtil;

/**
 * @author Jon
 *
 */
public class SubmittedFileDAOImpl extends DAOMaster implements SubmittedFileDAO {

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubmittedFileDAO#load(edu.cornell.csuglab.cms.base.SubmittedFilePK, edu.cornell.csuglab.cms.base.SubmittedFileBean)
	 */
	public void load(SubmittedFilePK pk, SubmittedFileBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select * from tSubmittedFiles where SubmittedFileID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getSubmittedFileID());
			rs = ps.executeQuery();
			if (rs.next()) {
				ejb.setSubmittedFileID(rs.getLong("SubmittedFileID"));
				ejb.setGroupID(rs.getLong("GroupID"));
				ejb.setOriginalGroupID(rs.getLong("OriginalGroupID"));
				ejb.setNetID(rs.getString("NetID"));
				ejb.setSubmissionID(rs.getLong("SubmissionID"));
				ejb.setFileDate(rs.getTimestamp("FileDate"));
				ejb.setFileType(rs.getString("FileType"));
				ejb.setFileSize(rs.getInt("FileSize"));
				ejb.setMD5(rs.getString("MD5"));
				ejb.setPath(FileUtil.translateDBPath(rs.getString("Path")));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {
			}
			throw new EJBException("Row id " + pk.getSubmittedFileID()
					+ " failed to load in tSubmittedFiles.", e);
		}
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubmittedFileDAO#store(edu.cornell.csuglab.cms.base.SubmittedFileBean)
	 */
	public void store(SubmittedFileBean ejb) throws EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "update tSubmittedFiles set GroupID = ?, OriginalGroupID = ?, " +
					"FileDate = ?, FileSize = ?, NetID = ? where SubmittedFileID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, ejb.getGroupID());
			ps.setLong(2, ejb.getOriginalGroupID());
			ps.setTimestamp(3, ejb.getFileDate());
			ps.setInt(4, ejb.getFileSize());
			if(ejb.getNetID() == null) ps.setNull(5, java.sql.Types.VARCHAR);
			else ps.setString(5, ejb.getNetID());
			ps.setLong(6, ejb.getSubmittedFileID());
			ps.executeUpdate();
			conn.close();
			ps.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				if (ps != null) ps.close();
				if (conn != null) conn.close();
			}
			catch (Exception f) {
				f.printStackTrace();
			}
			throw new EJBException("Row id " + ejb.getSubmittedFileID() + ", failed to store properly", e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubmittedFileDAO#remove(edu.cornell.csuglab.cms.base.SubmittedFilePK)
	 */
	public void remove(SubmittedFilePK pk) throws RemoveException, EJBException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubmittedFileDAO#create(edu.cornell.csuglab.cms.base.SubmittedFileBean)
	 */
	public SubmittedFilePK create(SubmittedFileBean ejb) throws CreateException, EJBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		SubmittedFilePK pk = null;
		try {
			conn = jdbcFactory.getConnection();
			String updateQuery = "insert into tSubmittedFiles " +
					"(GroupID, OriginalGroupID, SubmissionID, FileDate, FileType, " +
					"FileSize, NetID, MD5, Path, LateSubmission) values " +
					"(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			String findQuery = "select @@identity as SubmittedFileID from tSubmittedFiles";
			ps = conn.prepareStatement(updateQuery);
			ps.setLong(1, ejb.getGroupID());
			ps.setLong(2, ejb.getOriginalGroupID());
			ps.setLong(3, ejb.getSubmissionID());
			ps.setTimestamp(4, ejb.getFileDate());
			if(ejb.getFileType() == null) ps.setNull(5, java.sql.Types.VARCHAR);
			else ps.setString(5, ejb.getFileType());
			ps.setInt(6, ejb.getFileSize());
			if(ejb.getNetID() == null) ps.setNull(7, java.sql.Types.VARCHAR);
			else ps.setString(7, ejb.getNetID());
			if(ejb.getMD5() == null) ps.setNull(8, java.sql.Types.VARCHAR);
			else ps.setString(8, ejb.getMD5());
			if(ejb.getPath() == null) ps.setNull(9, java.sql.Types.VARCHAR);
			else ps.setString(9, FileUtil.translateSysPath(ejb.getPath()));
			ps.setBoolean(10, ejb.getLateSubmission());
			int count = ps.executeUpdate();
			rs = conn.prepareStatement(findQuery).executeQuery();
			if (count > 0 && rs.next()) {
				long submittedFileID = rs.getLong("SubmittedFileID"); 
				pk = new SubmittedFilePK(submittedFileID);
				ejb.setSubmittedFileID(submittedFileID);
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				e.printStackTrace();
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {
				f.printStackTrace();
			}
			throw new CreateException(e.getMessage());
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubmittedFileDAO#findByPrimaryKey(edu.cornell.csuglab.cms.base.SubmittedFilePK)
	 */
	public SubmittedFilePK findByPrimaryKey(SubmittedFilePK pk) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select SubmittedFileID from tSubmittedFiles where SubmittedFileID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, pk.getSubmittedFileID());
			rs = ps.executeQuery();
			if (!rs.next()) {
				throw new FinderException("Could not find submitted file with SubmittedFileID = " + pk.getSubmittedFileID());
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			try {
				throw ((FinderException) e);  //XXX wtf?
			}
			catch (ClassCastException x) {
				throw new FinderException("Caught Exception: " + e.getMessage());
			}
		}
		return pk;
	}

	/* (non-Javadoc)
	 * @see edu.cornell.csuglab.cms.base.SubmittedFileDAO#findByGroupID(long)
	 */
	public Collection findAllByGroupID(long groupid) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select SubmittedFileID from tSubmittedFiles s, " +
				"tRequiredSubmissions r where GroupID = ? and s.SubmissionID = r.SubmissionID " +
				"order by r.SubmissionName ASC";
			ps = conn.prepareStatement(query);
			ps.setLong(1, groupid);
			rs = ps.executeQuery();
			while (rs.next()) {
				long submittedFileID = rs.getLong("SubmittedFileID");
				result.add(new SubmittedFilePK(submittedFileID));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}

	public Collection findByAssignmentID(long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "SELECT f.SubmittedFileID FROM tSubmittedFiles f " + 
				"INNER JOIN tGroups g ON f.GroupID = g.GroupID " +
				"WHERE g.AssignmentID = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			rs = ps.executeQuery();
			while (rs.next()) {
				long submittedFileID = rs.getLong("SubmittedFileID");
				result.add(new SubmittedFilePK(submittedFileID));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
	public Collection findByGroupID(long groupid) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select max(s.SubmittedFileID) as SubmittedFileID, s.SubmissionID, r.SubmissionName from " +
					"(select GroupID, SubmissionID, max(FileDate) as currdate from tSubmittedFiles " +
					"where GroupID = ? group by GroupID,SubmissionID) as t, tSubmittedFiles s, " +
					"tRequiredSubmissions r where s.GroupID = t.GroupID and s.SubmissionID = t.SubmissionID " +
					"and r.SubmissionID = t.SubmissionID and r.Hidden = ? and s.FileDate = currdate " +
					"group by s.SubmissionID, r.SubmissionName " +
					"order by r.SubmissionName ASC";
			ps = conn.prepareStatement(query);
			
			ps.setLong(1, groupid);
			ps.setBoolean(2, false);
			rs = ps.executeQuery();
			while (rs.next()) {
				long submittedFileID = rs.getLong("SubmittedFileID");
				result.add(new SubmittedFilePK(submittedFileID));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
		}
		return result;
	}
	
	public Collection findAllByGroupIDs(Collection groupids) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		if (groupids.size() == 0) return result;
		try {
			conn = jdbcFactory.getConnection();
			String query = "select SubmittedFileID from tSubmittedFiles s, tRequiredSubmissions r where " +
					"r.SubmissionID = s.SubmissionID and (";
			for (int i=0; i < groupids.size() - 1; i++) {
				query += "s.GroupID = ? or ";
			}
			query += "s.GroupID = ?) order by r.SubmissionName ASC, FileDate DESC";
			ps = conn.prepareStatement(query);
			int c = 1;
			for (Iterator i=groupids.iterator(); i.hasNext(); ) {
				ps.setLong(c++, ((Long)i.next()).longValue());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				long submittedFileID = rs.getLong("SubmittedFileID");
				result.add(new SubmittedFilePK(submittedFileID));
			}
			conn.close();
			ps.close();
			rs.close();
		}
		catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			}
			catch (Exception f) {}
			e.printStackTrace();
		}
		return result;		
	}
	
	public Collection findByNetIDs(Collection netids, long assignmentID) throws FinderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection result = new ArrayList();
		try {
			conn = jdbcFactory.getConnection();
			String query = "select distinct s.SubmittedFileID from tSubmittedFiles s, tGroupMembers m, tGroups g where " + 
					"g.AssignmentID = ? and m.GroupID = g.GroupID and s.GroupID = g.GroupID and " +
					"m.Status = ? and (";
			for (int i=0; i < netids.size() - 1; i++) {
				query += "m.NetID = ? or ";
			}
			query += "m.NetID = ?)";
			ps = conn.prepareStatement(query);
			ps.setLong(1, assignmentID);
			ps.setString(2, "Active");
			Iterator i = netids.iterator();
			int count = 0;
			while (i.hasNext()) {
				ps.setString((count++) + 3, (String) i.next());
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new SubmittedFilePK(rs.getLong("SubmittedFileID")));
			}
			conn.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			try {
				if (conn != null) conn.close();
				if (ps != null) ps.close();
				if (rs != null) rs.close();
			} catch (Exception f) {}
			throw new FinderException(e.getMessage());
		}
		return result;
	}
}
