<%@ page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.author.*, edu.cornell.csuglab.cms.www.xml.*, edu.cornell.csuglab.cms.base.AssignmentBean, java.io.StringWriter, edu.cornell.csuglab.cms.www.util.*" %>
<%
Document displayData = (Document)session.getAttribute(AccessController.A_DISPLAYDATA);
Principal p = (Principal)session.getAttribute(AccessController.A_PRINCIPAL);
Element root = (Element)displayData.getElementsByTagName(XMLBuilder.TAG_ROOT).item(0);
NodeList stati = (NodeList)root.getElementsByTagName(XMLBuilder.TAG_STATUS);
String domainID = (String)session.getAttribute(AccessController.A_DOMAINID);
boolean PWExpired = Boolean.valueOf((String)session.getAttribute(AccessController.A_PASSWORDEXPIRED)).booleanValue();


boolean hasError = false;
for (int i=0; i < stati.getLength(); i++) 
{
	Element status = (Element)stati.item(i); 
	if (status != null) 
	{
		if (status.hasAttribute(XMLBuilder.A_ISERROR))
		{
			hasError = true;
			break;
		}
	}
}
%>

<script language="Javascript">
	function confirmAndSubmit(textbox1Name, textbox2Name, MismatchErrorDivName, StrengthErrorDivName)
	{
		document.getElementById(StrengthErrorDivName).style.display = "none";
		document.getElementById(MismatchErrorDivName).style.display = "none";
		
		var pw = document.getElementById(textbox1Name).value;
		
		if (pw==document.getElementById(textbox2Name).value)
		{
			if (checkPWStrength(pw))
			{
				form1.submit();
			}
			else
			{
				document.getElementById(textbox1Name).value="";
				document.getElementById(textbox2Name).value="";
				document.getElementById(StrengthErrorDivName).style.display = "";
			}
		}
		else
		{
			document.getElementById(textbox1Name).value="";
			document.getElementById(textbox2Name).value="";
			document.getElementById(MismatchErrorDivName).style.display = "";
		}
	}
	
	function checkPWStrength(pw)
	{
		if (pw.length < 8)
		{
			return false;
		}
		return true;
	}
</script>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <link href="/web/cuwebbanner/styles/screen.css" rel="stylesheet" type="text/css">
  <link href="/web/style.css" rel="stylesheet" type="text/css">
  <link href="/web/navbar_style.css" rel="stylesheet" type="text/css">
  <title>
      Cornell University Computer Science Course Management System
  </title>
</head>
<body>

<jsp:include page="cuwebbanner/banner.jsp" />

<div id="navbar_course">
    <span class="navlink_course" id="navlink_course_overview">
        *** CUCS CMS Version 3.3 ***
    </span>
</div>

<div id="content">
   <div id="dmenu">
	  	<%if (!PWExpired) {%>	
		<form id="form1" action="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_EXTERNALLOGINVALIDATION%>" method="post">
		<%}
		else {%>
		<form id="form1" action="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_EXTERNALLOGINPASSWORDCHANGE%>" method="post">
		<%}%>
		
			<jsp:include page="problem-report.jsp"/>
			<input type="hidden" id="<%=AccessController.P_PWCHANGE%>" name="<%=AccessController.P_PWCHANGE%>" value="<%=PWExpired%>">
			
			<table cellpadding="0" cellspacing="1" border="0">
			<tr>
			<%
			if (domainID == null)
			{%>
			 <td>Domain: </td><td><select id="<%=AccessController.P_DOMAINID%>" name="<%=AccessController.P_DOMAINID%>">
			<% Element domRoot = (Element)root.getElementsByTagName(XMLBuilder.TAG_DOMAINS).item(0);
				NodeList doms = domRoot.getElementsByTagName(XMLBuilder.TAG_DOMAIN);
				for (int i = 0; i < doms.getLength(); i++)
				{
					Element item = (Element)doms.item(i);
					if (!item.getAttribute(XMLBuilder.A_ID).equals("1"))
					{%>
					<option value="<%= item.getAttribute(XMLBuilder.A_ID)%>"><%= item.getAttribute(XMLBuilder.A_NAME) %></input>
					<%}
				}
			%>
			 </select></td>
			<%}
			else
			{
			Element domRoot = (Element)root.getElementsByTagName(XMLBuilder.TAG_DOMAINS).item(0);
			NodeList doms = domRoot.getElementsByTagName(XMLBuilder.TAG_DOMAIN);
			for (int i = 0; i < doms.getLength(); i++)
			{
				Element item = (Element)doms.item(i);
				if (item.getAttribute(XMLBuilder.A_ID).equals(domainID))
				{%>
					<td>Domain: </td><td><%=item.getAttribute(XMLBuilder.A_NAME)%></td>
				<%
					break;
				}
			}%>
			<input type="hidden" id="<%=AccessController.P_DOMAINID%>" name="<%=AccessController.P_DOMAINID%>" value="<%=domainID%>">
			<%}%>
			</tr>
				
			<%
			if (!PWExpired) {%>			
			<tr>
			 <td>Username: </td><td><input id="<%=AccessController.P_EXTERNALLOGIN%>" name="<%=AccessController.P_EXTERNALLOGIN%>" type="text" value=""></td>
			</tr>
			<tr> 
			 <td>Password: </td><td><input id="<%=AccessController.P_EXTERNALPASSWORD%>" name="<%=AccessController.P_EXTERNALPASSWORD%>" type="password" value=""></td>
			</tr>
			<tr> 
			 <td colspan=2 align="center"><input type="submit" value="Login"></td>
			</tr>
			<%}
			else {%>
			<tr>
			 <td colspan=2>
			 <div id="PWMismatchError" style="color:red;display:none;">
			 Passwords do not match! Please re-enter.
			 </div>
			 <div id="PWStrengthError" style="color:red;display:none;">
			 Password is not strong enough. It should be at least 8 characters long.<br>Please re-enter.
			 </div>
			 </td>
			</tr>
			<tr>
			 <td>Input New Password: </td><td><input id="<%=AccessController.P_EXTERNALPASSWORD%>" name="<%=AccessController.P_EXTERNALPASSWORD%>" type="password" value=""></td>
			</tr>
			<tr>
			 <td>Confirm New Password: </td><td><input id="<%=AccessController.P_EXTERNALPASSWORD%>2" name="<%=AccessController.P_EXTERNALPASSWORD%>2" type="password" value=""></td>
			</tr>
			<tr>
			 <td colspan=2 align="center"><input type="button" value="Submit" onclick="confirmAndSubmit('<%=AccessController.P_EXTERNALPASSWORD%>','<%=AccessController.P_EXTERNALPASSWORD%>2','PWMismatchError','PWStrengthError');"></td>
			</tr>
			<%}%>	
			
			</table>
		</form>
   </div>
</div>

</body>
</html>