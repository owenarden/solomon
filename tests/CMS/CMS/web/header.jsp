<%@ page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.author.*, edu.cornell.csuglab.cms.www.xml.*"%>
<% /* The top of all CMS pages. */
Document displayData = (Document) session.getAttribute(AccessController.A_DISPLAYDATA);
Principal p = (Principal)session.getAttribute(AccessController.A_PRINCIPAL);
Element root = (Element) displayData.getChildNodes().item(0);
boolean debug= ((Boolean)session.getAttribute(AccessController.A_DEBUG)).booleanValue();
String json = root.getAttribute(XMLBuilder.A_HOSTGROUPJSON);%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <link href="/web/style.css" rel="stylesheet" type="text/css">
    <link href="/web/navbar_style.css" rel="stylesheet" type="text/css">
	<%-- Fix for navbar for IE 6 --%><!--[if lte IE 6]><style type="text/css">#sidenav li.menuhead ul {left: 85%;}</style><![endif]-->
    <title>
      Cornell University Course Management System
    </title>
	<!-- Internet Explorer standards compatability package; see http://code.google.com/p/ie7-js/ -->
	<!--[if lt IE 8]>
	<script src="IE8-2.0b3.js" type="text/javascript"></script>
	<![endif]-->
	<script type="text/javascript" src="cookies.js"></script>
	<script type="text/javascript" src="header.js"></script>
	<script type="text/javascript" src="popupmenu.js"></script>
	<script type="text/javascript" src="sorttable.js"></script>
	<script type="text/javascript" src="staff/grading/gradeassign.js"></script>
	<script type="text/javascript">
		var CURRENT_USER = "<%=p.getPrincipalID()%>"; <%-- // netID of the principal currently logged in, or "guest" if a guest
		
		// JSON array of {host, semester_id} objects, describing group of servers, to be populated by JSP code
		// should NOT include the current server itself in the array (RootDAOImpl.refreshHostGroups should filter it out) --%>
		var HOSTGROUP = <%= json == null || json.equals("") ? "[]" : json %>;<%-- Example:
		/*[
			{"host":"https://www3.csuglab.cornell.edu/web/auth/", "semID": 13},
			{"host":"https://www3.csuglab.cornell.edu/web/auth/", "semID": 3}
			];*/ --%>
	</script>
<%-- Pages should now insert any head code they require before importing header-page.jsp --%>