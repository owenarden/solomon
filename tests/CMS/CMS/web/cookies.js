/***********************************************************************************
* cookies.js: the essential functions for dealing with cookies
***********************************************************************************/
/* derived from http://www.webreference.com/js/column8/functions.html */

/*
 * Gotcha: can't set a session cookie here, use setSessionCookie instead -- Alex, Apr. 2008 
 * (this is being left alone to avoid breaking other javascript)
   name - name of the cookie
   value - value of the cookie (default: ""); don't need to escape beforehand.
   [expires] - expiration date of the cookie as a Date object (default: 12/31/2037 16:00 GMT)
   (default occurs when null is passed in)
*/
function setCookie(name, value, expires)
{
	if(value == null) value = '';
	if(expires == null)
	{
		//I think this is a couple weeks before end of epoch -- Evan
		expires = new Date();
		expires.setYear(2037);
		expires.setMonth(11);
		expires.setDate(31);
		expires.setHours(16);
	}
	
	setRealCookie(name, value, expires);
}

function setSessionCookie(name, value)
{
	setRealCookie(name, value, null);
}

function setRealCookie(name, value, expires) // because in the original setCookie, null expiration means forever
{
	var path = '/';
	var curCookie = name + "=" + escape(value) +
		((expires) ? "; expires=" + expires.toGMTString() : "") +
		((path) ? "; path=" + path : "");
	document.cookie = curCookie;
}

function setRawCookie(name, value, expires) // don't auto-escape
{
	var path = '/';
	var curCookie = name + "=" + value +
		((expires) ? "; expires=" + expires.toGMTString() : "") +
		((path) ? "; path=" + path : "");
	document.cookie = curCookie;
}

/*
  name - name of the desired cookie
  return string containing value of specified cookie, or null if cookie does not exist
*/
function getCookie(name) {
  return unescape(getRawCookie(name));
}

function getRawCookie(name) { // don't unescape
  var dc = document.cookie;
  var prefix = name + "=";
  var begin = dc.indexOf("; " + prefix);
  if (begin == -1) {
    begin = dc.indexOf(prefix);
    if (begin != 0) return null;
  } else
    begin += 2;
  var end = document.cookie.indexOf(";", begin);
  if (end == -1)
    end = dc.length;
  return dc.substring(begin + prefix.length, end);
}

/*
   name - name of the cookie
   [path] - path of the cookie (must be same as path used to create cookie)
   [domain] - domain of the cookie (must be same as domain used to create cookie)
   -- path and domain default if assigned null or omitted if no explicit argument proceeds
*/
function deleteCookie(name, path, domain) {
  if (getCookie(name)) {
    document.cookie = name + "=" +
    ((path) ? "; path=" + path : "") +
    ((domain) ? "; domain=" + domain : "") +
    "; expires=Thu, 01-Jan-70 00:00:01 GMT";
  }
}
