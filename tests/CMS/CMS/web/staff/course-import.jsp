<%@ page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.author.*, edu.cornell.csuglab.cms.www.xml.*, edu.cornell.csuglab.cms.base.AssignmentBean, java.io.StringWriter, edu.cornell.csuglab.cms.www.util.*" %><%
Document displayData = (Document)session.getAttribute(AccessController.A_DISPLAYDATA);
Principal p = (Principal)session.getAttribute(AccessController.A_PRINCIPAL);
Element root = (Element)displayData.getElementsByTagName(XMLBuilder.TAG_ROOT).item(0);
Element course = XMLUtil.getFirstChildByTagName(root, XMLBuilder.TAG_COURSE); 
String courseid = course.getAttribute(XMLBuilder.A_COURSEID);
boolean cmsAdminOnly = p.isCMSAdmin() && !course.hasAttribute(XMLBuilder.A_ISADMIN); 
NodeList stati = (NodeList)root.getElementsByTagName(XMLBuilder.TAG_STATUS);

boolean hasError = false;
for(int i=0; i < stati.getLength(); i++) 
{
	Element status = (Element)stati.item(i); 
	if(status != null) 
	{
		if(status.hasAttribute(XMLBuilder.A_ISERROR))
		{
			hasError = true;
			break;
		}
	}
}
%>
<jsp:include page="../header.jsp" />
<script type="text/javascript">
/*
permissions checking: which of {staff, students, Cornell community, guests} can see what?
*/

  function checkCCs(type) {
    var ths = getElementById('guest' + type);
    if (ths.checked) {
      var coursecc = getElementById('cccoursepage');
      var courseguest = getElementById('guestcoursepage');
      var thscc = getElementById('cc' + type);
      coursecc.checked = true;
      courseguest.checked = true;
      thscc.checked = true;
      if (type == 'solution') {
        var assigncc = getElementById('ccassigns');
        var assignguest = getElementById('guestassigns');
        assigncc.checked = true;
        assignguest.checked = true;
      }
    }
  }
  
  function checkNone() {
      var coursecc = getElementById('cccoursepage');
      var assigncc = getElementById('ccassigns');
      var announcecc = getElementById('ccannounce');
      var solutioncc = getElementById('ccsolution');
      coursecc.checked = false;
      assigncc.checked = false;
      announcecc.checked = false;
      solutioncc.checked = false;
      checkNoneGuest();
  }
  
  function checkNoneGuest() {
      var courseguest = getElementById('guestcoursepage');
      var assignguest = getElementById('guestassigns');
      var announceguest = getElementById('guestannounce');
      var solutionguest = getElementById('guestsolution');
      courseguest.checked = false;
      assignguest.checked = false;
      announceguest.checked = false;
      solutionguest.checked = false;
  }
  
</script>
<jsp:include page="courseprops-staffperms.js.jsp" />
<jsp:include page="../header-page.jsp"/>
<script language="Javascript">
	
	//Make sure if parent is unchecked, child is unchecked.
	function fixCheckBoxes(parentCheckboxID,childCheckboxID1,childCheckboxID2)
	{
		parentCheckbox = document.getElementById(parentCheckboxID);
		childCheckbox1 = document.getElementById(childCheckboxID1);
		
		childCheckbox2 = null;
		if(childCheckboxID2 != null)
		{
			childCheckbox2 = document.getElementById(childCheckboxID2);
		}
		if(!parentCheckbox.checked)
		{
			if(childCheckbox1 != null)
			{
				childCheckbox1.checked = false;
				childCheckbox1.disabled = true;
			}
			if(childCheckbox2 != null)
			{
				childCheckbox2.checked = false;
				childCheckbox2.disabled = true;
			}
		}
		else
		{
			if(childCheckbox1 != null)
			{
				childCheckbox1.disabled = false;
			}
		}
	}
</script>
<div id="course_wrapper_withnav">
<table id="course_wrapper_table" summary="course wrapper" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<% if (!cmsAdminOnly)
	   { %>
		<jsp:include page="navbar.jsp"/>
	<% } 
	Document importData = (Document)session.getAttribute(AccessController.A_PARSEDCOURSETEMPLATE);
	Element importRoot = (Element)importData.getElementsByTagName(XMLBuilder.TAG_ROOT).item(0);
	Element importCourse = XMLUtil.getFirstChildByTagName(importRoot, XMLBuilder.TAG_COURSE); 
	NodeList categoryList = importRoot.getElementsByTagName(XMLBuilder.TAG_CATEGORY);
	%>
    <td valign="top"  id="course_page_container">
    <div id="xmltree" class="showhide" style="display:none">
<%-- print the full xml tree to the page --%>
<% 
StringWriter writer2 = new StringWriter();
DOMWriter.write(importData, writer2); %>
		<pre><%-- avoid having the browser think the XML is HTML --%>
<%=writer2.toString() 
%>
		</pre>
	</div>
		<form action="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_COURSEIMPORTCONFIRMED + "&amp;" + AccessController.P_COURSEID + "=" + courseid %>" method="post">
        <div id="course_page">
       	 <%if(hasError){%>
			<span class="status_err">There are errors in the template zipfile (as noted below).<br>
			Please fix the errors and try again (the easiest method would be to re-do the export).<br>
			You will not be able to continue the import process until these errors are resolved.<br>
			Or if you do not know how to fix the errors, please contact <a target="blank" href="http://helpdesk.cs.cornell.edu">CS HelpDesk</a>.</span><br>
			<%}%>
          <jsp:include page="../problem-report.jsp"/>
          <span class="assignment_title">Course Template Import</span>
          <br><br>          
          <div class="assignment_left">
            Please select the different types of content that you wish to import from the zip file. <br/>
            You are only given choices of materials that were exported.<br/>
            All items imported will have a status of "Hidden" as default.  Please change as appropriate, after the import.<br>
			Any associated dates (due dates, late dates, etc.) will be forwarded to proper semester, but using very basic logic. Please double-check and correct these as necessary.<br/><br/>
			<h2>General Items:</h2>
            <table class="assignment_table" cellpadding="0" cellspacing="0" border="0">
				<tr>
	              	<td width="250">
	              		General Course Properties<br><i>(Description, Guest Access settings, etc.)</i>: 
					</td>
					<td>
						<input type="checkbox" id="ImportCourseProperties" name="ImportCourseProperties"></input>
					</td>
					<td>
					</td>
				</tr>
				<%
				NodeList AssignmentList = importRoot.getElementsByTagName(XMLBuilder.TAG_ASSIGNMENT);
				boolean hasFiles = false;
				boolean hasAssignments = false;
				boolean hasQuizzes = false;
				boolean hasSurveys = false;
				String assignType;
				int assignTypeInt = -1;
				if(AssignmentList.getLength() > 0)
				{
					for(int i = 0; i < AssignmentList.getLength(); i++)
            		{ 
            			Element assignment = (Element)AssignmentList.item(i);
            			
            			assignType = assignment.getAttribute(XMLBuilder.A_ASSIGNTYPE);
            			if(assignType != null) assignTypeInt = Integer.parseInt(assignType);

            			switch(assignTypeInt)
            			{
            				case AssignmentBean.ASSIGNMENT:
            					hasAssignments = true;
            					break;
            				case AssignmentBean.QUIZ:
            					hasQuizzes = true;
            					break;
            				case AssignmentBean.SURVEY:
            					hasSurveys = true;
            					break;
            			}
            			
            			if(assignment.getElementsByTagName(XMLBuilder.TAG_FILE).getLength() > 0 || assignment.getElementsByTagName(XMLBuilder.TAG_SOLFILE).getLength() > 0)
            			{
            				hasFiles = true;
            			}
            		}
            	}
				%>
				<%if(hasAssignments) {%>
				<tr>
	              	<td colspan="3">
	              		<label>
	              		Assignments: 
	              		<input type="checkbox" id="ImportCourseAssignments" name="ImportCourseAssignments" onclick="javascript:fixCheckBoxes('ImportCourseAssignments','ImportCourseAssignmentFiles','');"></input> 
	              		</label>
              			<%if(hasFiles) {%>
              			<label>
	              		Include Attached Files?: <input type="checkbox" id="ImportCourseAssignmentFiles" name="ImportCourseAssignmentFiles" disabled></input>
	              		</label>
	              		<%}%>
	              	</td>
				</tr>
				<%}%>
				
				<%if(hasSurveys){%>
				<tr>
	              	<td colspan="3">
	              		<label>
	              		Surveys: <input type="checkbox" id="ImportCourseSurveys" name="ImportCourseSurveys"></input>
	              		</label>
					</td>
				</tr>
				<%}%>
				
				<%if(hasQuizzes){%>
				<tr>
	              	<td colspan="3">
	              		<label>
	              		Quizzes: <input type="checkbox" id="ImportCourseQuizzes" name="ImportCourseQuizzes"></input>
	              		</label>
					</td>
				</tr>
				<%}%>
			</table>
			</div>
			<br>
			<div class="assignment_left">
            <h2>Custom Content</h2>
            <table class="assignment_table" cellpadding="0" cellspacing="0" border="0">
            <% for(int i = 0; i < categoryList.getLength(); i++)
            	{ 
            	Element cat = (Element)categoryList.item(i);
				String name = cat.getAttribute(XMLBuilder.A_NAME).replaceAll("\"","");
				String catID = cat.getAttribute(XMLBuilder.A_ID);
            	String schemaCheckboxID = "ImportCourseContentSchema_"+catID;
            	String dataCheckboxID = "ImportCourseContentData_"+catID;
            	String fileCheckboxID = "ImportCourseContentFiles_"+catID;
            	%>
              <tr>
              	<td width="200">
              		<%= cat.getAttribute(XMLBuilder.A_NAME) %>:
              	</td>
              	<td width="150">
              		<label>
              		<input type="checkbox" id="<%=schemaCheckboxID%>" name="ImportCourseContentSchema" value="<%=catID%>" onclick="javascript:fixCheckBoxes('<%=schemaCheckboxID%>','<%=dataCheckboxID%>','<%=fileCheckboxID%>');">Column definitions</input>
              		</label>
              	</td>
              	<td width="100">
              		<%if(cat.getElementsByTagName(XMLBuilder.TAG_CTGROW).getLength() > 0) {%>
              			<label>
              			<input type="checkbox" id="<%=dataCheckboxID%>" name="ImportCourseContentData" value="<%=catID%>" disabled onclick="javascript:fixCheckBoxes('<%=dataCheckboxID%>','<%=fileCheckboxID%>','');">Rows</input> 
              			</label>
              			</td>
              			<td width="100">
	              		<%if(cat.getElementsByTagName(XMLBuilder.TAG_CTGFILE).getLength() > 0) {%>
	              			<label>
	              			<input type="checkbox" id="<%=fileCheckboxID%>" name="ImportCourseContentFiles" value="<%=catID%>" disabled>Attached files</input>
	              			</label>
	              		<%}%>
              		<%}
              		else
              		{%>
              		</td>
              		<td width="100">
              		<%}%>
				</td>
				<td></td>
			  </tr>
				<% } %>
			</table>
			</div>
			<input type="submit" value="Import">
		</div>
	</form>
		</td>
	</tr>
</table>
</div>