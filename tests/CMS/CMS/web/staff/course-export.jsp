<%@ page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.author.*, edu.cornell.csuglab.cms.www.xml.*, edu.cornell.csuglab.cms.base.AssignmentBean" %><%
Document displayData = (Document)session.getAttribute(AccessController.A_DISPLAYDATA);
Principal p = (Principal)session.getAttribute(AccessController.A_PRINCIPAL);
Element root = (Element)displayData.getElementsByTagName(XMLBuilder.TAG_ROOT).item(0);
Element course = XMLUtil.getFirstChildByTagName(root, XMLBuilder.TAG_COURSE); 
String courseid = course.getAttribute(XMLBuilder.A_COURSEID);
NodeList categoryList = root.getElementsByTagName(XMLBuilder.TAG_CATEGORY);
boolean cmsAdminOnly = p.isCMSAdmin() && !course.hasAttribute(XMLBuilder.A_ISADMIN); 
NodeList stati = (NodeList)root.getElementsByTagName(XMLBuilder.TAG_STATUS);

boolean hasError = false;
for(int i=0; i < stati.getLength(); i++) 
{
	Element status = (Element)stati.item(i); 
	if(status != null) 
	{
		if(status.hasAttribute(XMLBuilder.A_ISERROR))
		{
			hasError = true;
			break;
		}
	}
}
%>
<jsp:include page="../header.jsp" />
<script type="text/javascript">
/*
permissions checking: which of {staff, students, Cornell community, guests} can see what?
*/

  function checkCCs(type) {
    var ths = getElementById('guest' + type);
    if (ths.checked) {
      var coursecc = getElementById('cccoursepage');
      var courseguest = getElementById('guestcoursepage');
      var thscc = getElementById('cc' + type);
      coursecc.checked = true;
      courseguest.checked = true;
      thscc.checked = true;
      if (type == 'solution') {
        var assigncc = getElementById('ccassigns');
        var assignguest = getElementById('guestassigns');
        assigncc.checked = true;
        assignguest.checked = true;
      }
    }
  }
  
  function checkNone() {
      var coursecc = getElementById('cccoursepage');
      var assigncc = getElementById('ccassigns');
      var announcecc = getElementById('ccannounce');
      var solutioncc = getElementById('ccsolution');
      coursecc.checked = false;
      assigncc.checked = false;
      announcecc.checked = false;
      solutioncc.checked = false;
      checkNoneGuest();
  }
  
  function checkNoneGuest() {
      var courseguest = getElementById('guestcoursepage');
      var assignguest = getElementById('guestassigns');
      var announceguest = getElementById('guestannounce');
      var solutionguest = getElementById('guestsolution');
      courseguest.checked = false;
      assignguest.checked = false;
      announceguest.checked = false;
      solutionguest.checked = false;
  }
  
</script>
<jsp:include page="courseprops-staffperms.js.jsp" />
<jsp:include page="../header-page.jsp"/>
<script language="Javascript">
	
	//Make sure if parent is unchecked, child is unchecked.
	function fixCheckBoxes(parentCheckboxID,childCheckboxID1,childCheckboxID2)
	{
		parentCheckbox = document.getElementById(parentCheckboxID);
		childCheckbox1 = document.getElementById(childCheckboxID1);
		
		childCheckbox2 = null;
		if(childCheckboxID2 != null)
		{
			childCheckbox2 = document.getElementById(childCheckboxID2);
		}
		if(!parentCheckbox.checked)
		{
			if(childCheckbox1 != null)
			{
				childCheckbox1.checked = false;
				childCheckbox1.disabled = true;
			}
			if(childCheckbox2 != null)
			{
				childCheckbox2.checked = false;
				childCheckbox2.disabled = true;
			}
		}
		else
		{
			if(childCheckbox1 != null)
			{
				childCheckbox1.disabled = false;
			}
		}
	}
</script>
<div id="course_wrapper_withnav">
<table id="course_wrapper_table" summary="course wrapper" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<% if (!cmsAdminOnly)
	   { %>
		<jsp:include page="navbar.jsp"/>
	<% } %>
    <td valign="top"  id="course_page_container">
      <form action="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_COURSEEXPORTCONFIRMED + "&amp;" + AccessController.P_COURSEID + "=" + courseid %>" method="post">
        <div id="course_page">
        	<%if(hasError){%>
			<span class="status_err">The following errors were found during export (as noted below).<br>
			There could be inconsistencies with your course setup that is causing these issues,<br>
			please contact <a target="blank" href="http://helpdesk.cs.cornell.edu">CS HelpDesk</a> with the errors.</span><br>
			<%}%>
          <jsp:include page="../problem-report.jsp"/>
          <span class="assignment_title">Course Template Export</span>
          <br><br>          
          <div class="assignment_left">
            <h2>Select items to export in template:</h2>
            Please select the course materials to be included in the exported zip file.<br/>
            <table class="assignment_table" cellpadding="0" cellspacing="0" border="0">
				<!--<tr>
	              	<td>
	              		General Course Properties (Description, Course code, etc.): <input type="checkbox" id="ExportCourseProperties" name="ExportCourseProperties" checked></input>
					</td>
				</tr>-->
				<tr>
	              	<td>
	              		<label>
	              		Assignments: <input type="checkbox" id="ExportCourseAssignments" name="ExportCourseAssignments" checked onclick="javascript:fixCheckBoxes('ExportCourseAssignments','ExportCourseAssignmentFiles',null);"></input>
	              		</label>
	              		<label>
	              		Include Attached Files?: <input type="checkbox" id="ExportCourseAssignmentFiles" name="ExportCourseAssignmentFiles" checked></input>
	              		</label>
					</td>
				</tr>
				<tr>
	              	<td>
	              		<label>
	              		Surveys: <input type="checkbox" id="ExportCourseSurveys" name="ExportCourseSurveys" checked></input>
	              		</label>
					</td>
				</tr>
				<tr>
	              	<td>
	              		<label>
	              		Quizzes: <input type="checkbox" id="ExportCourseQuizzes" name="ExportCourseQuizzes" checked></input>
	              		</label>
					</td>
				</tr>
			</table>
			</div>
			<br>
			<div class="assignment_left">
            <h2>Custom Content</h2>
            <table class="assignment_table" cellpadding="0" cellspacing="0" border="0" width="550">
            <% for(int i = 0; i < categoryList.getLength(); i++)
            	{ 
            	Element cat = (Element)categoryList.item(i);
            	String catID = cat.getAttribute(XMLBuilder.A_ID);
            	String schemaCheckboxID = "ExportCourseCustomSchema_" + catID;
            	String dataCheckboxID = "ExportCourseCustomData_" + catID;
            	String fileCheckboxID = "ExportCourseCustomFiles_" + catID;
            	%>
              <tr>
              	<td width="200">
              		<%= cat.getAttribute(XMLBuilder.A_NAME) %>: 
              	</td>
              	<td width="150">
              		<label>
              		<input type="checkbox" id="<%=schemaCheckboxID%>" name="<%=schemaCheckboxID%>" checked onclick="javascript:fixCheckBoxes('<%=schemaCheckboxID%>','<%=dataCheckboxID%>','<%=fileCheckboxID%>');">Column definitions</input> 
              		</label>
              	</td>
              	<td width="100">
              		<label>
              		<input type="checkbox" id="<%=dataCheckboxID%>" name="<%=dataCheckboxID%>" checked onclick="javascript:fixCheckBoxes('<%=dataCheckboxID%>','<%=fileCheckboxID%>','');">Rows</input> 
              		</label>
              	</td>
              	<td width="100">
	              	<label>
              		<input type="checkbox" id="<%=fileCheckboxID%>" name="<%=fileCheckboxID%>" checked>Attached files</input>
              		</label>
				</td>
				<td></td>
			  </tr>
				<% } %>
			</table>
			</div>
			<input type="submit" value="Export"><br>
			*Please note where you save the exported file, as you will need it later when performing an import.
		</div>
	</form>
		</td>
	</tr>
</table>
</div>