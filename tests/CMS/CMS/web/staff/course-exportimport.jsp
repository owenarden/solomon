<%@page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.author.*, edu.cornell.csuglab.cms.www.xml.*"%><%
Document displayData = (Document)session.getAttribute(AccessController.A_DISPLAYDATA);
Principal p = (Principal)session.getAttribute(AccessController.A_PRINCIPAL);
Element root = (Element)displayData.getElementsByTagName(XMLBuilder.TAG_ROOT).item(0);
Element course = XMLUtil.getFirstChildByTagName(root, XMLBuilder.TAG_COURSE); 
String courseid = course.getAttribute(XMLBuilder.A_COURSEID); 
boolean cmsAdminOnly = p.isCMSAdmin() && !course.hasAttribute(XMLBuilder.A_ISADMIN); 
NodeList semesters = root.getElementsByTagName(XMLBuilder.TAG_SEMESTER);
%>
<jsp:include page="../header.jsp" />
<script type="text/javascript">
/*
permissions checking: which of {staff, students, Cornell community, guests} can see what?
*/

  function checkCCs(type) {
    var ths = getElementById('guest' + type);
    if (ths.checked) {
      var coursecc = getElementById('cccoursepage');
      var courseguest = getElementById('guestcoursepage');
      var thscc = getElementById('cc' + type);
      coursecc.checked = true;
      courseguest.checked = true;
      thscc.checked = true;
      if (type == 'solution') {
        var assigncc = getElementById('ccassigns');
        var assignguest = getElementById('guestassigns');
        assigncc.checked = true;
        assignguest.checked = true;
      }
    }
  }
  
  function checkNone() {
      var coursecc = getElementById('cccoursepage');
      var assigncc = getElementById('ccassigns');
      var announcecc = getElementById('ccannounce');
      var solutioncc = getElementById('ccsolution');
      coursecc.checked = false;
      assigncc.checked = false;
      announcecc.checked = false;
      solutioncc.checked = false;
      checkNoneGuest();
  }
  
  function checkNoneGuest() {
      var courseguest = getElementById('guestcoursepage');
      var assignguest = getElementById('guestassigns');
      var announceguest = getElementById('guestannounce');
      var solutionguest = getElementById('guestsolution');
      courseguest.checked = false;
      assignguest.checked = false;
      announceguest.checked = false;
      solutionguest.checked = false;
  }
  
</script>
<jsp:include page="courseprops-staffperms.js.jsp" />
<jsp:include page="../header-page.jsp"/>
<div id="course_wrapper_withnav">
<table id="course_wrapper_table" summary="course wrapper" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<% if (!cmsAdminOnly)
	   { %>
		<jsp:include page="navbar.jsp"/>
	<% } %>
    <td valign="top"  id="course_page_container">
    	<div id="course_page">
          <jsp:include page="../problem-report.jsp"/>
          <% if (!cmsAdminOnly)
        	 { %>
                <span class="assignment_title">Course Export/Import</span>
            	<br><br>
          <% } %>
          <% if (cmsAdminOnly)
             { %>
                <jsp:include page="course-title.jsp" />
		  <% } %>
		  <div style="width: 700px;" class="assignment_left">
		  	You can migrate course materials (e.g., assignments, content tables, quizzes) between one course and another, or between different semesters:<br/>
			<ol>
			<li>Export selected course materials from the current course as a zip file.</li>
			<li>Navigate to the new course and import selected course materials from the zip file.</li>
			</ol>
		  </div>
          <div class="assignment_left">
            <h2>Course Export</h2>
      <form action="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_COURSEEXPORTSTART + "&amp;" + AccessController.P_COURSEID + "=" + courseid %>" method="post">
      	<div class="assignment_left">
			<input type="submit" value="Begin Export"/>
		</div>
		  </form><br>

	<h2>Course Import</h2>
      <form enctype="multipart/form-data" action="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_COURSEIMPORTSTART + "&amp;" + AccessController.P_COURSEID + "=" + courseid %>" method="post">
	      	<div class="assignment_left">
				Select a zip file containing course materials to import into the current course.
				<br/><input type="file" name="uploadedcoursetemplatezipfile" size="70"/>
				<br>
				<input type="submit" value="Begin Import"/>
			</div>
		  </form>
		  </div>
    </td>
      <td id="course_menu_container" width="14px"><div id="course_menu_top">&nbsp;</div></td>
  </tr>
<jsp:include page="../footer.jsp"/>