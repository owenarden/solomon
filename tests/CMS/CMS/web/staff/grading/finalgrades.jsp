<%@ page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.www.xml.*" %><%
/************************************************************************
* display name, course options (lecture number, credit hours, etc),
* and final grade for each student in a course
************************************************************************/
Document displayData= (Document) session.getAttribute(AccessController.A_DISPLAYDATA);
Element root= (Element)displayData.getFirstChild();
Element course = XMLUtil.getFirstChildByTagName(root, XMLBuilder.TAG_COURSE);
String courseID = course.getAttribute(XMLBuilder.A_COURSEID);
boolean hasSection = course.hasAttribute(XMLBuilder.A_HASSECTION);
boolean isStaff = root.hasAttribute(XMLBuilder.A_ISADMIN);
NodeList students = XMLUtil.getChildrenByTagName(root, XMLBuilder.TAG_STUDENT);
%>
<jsp:include page="../../header.jsp"/>
<jsp:include page="../../header-page.jsp"/>
<div id="course_wrapper_withnav">
<table id="course_wrapper_table" cellpadding="0" cellspacing="0" width="100%">
  <tr>
  <% if (isStaff) { %>
		<jsp:include page="../navbar.jsp"/>
  <% } %>
    <td valign="top" id="course_page_container">
      <div id="course_page">
		<jsp:include page="../../problem-report.jsp"/>
		<span class="assignment_title">Final Grades</span>
        <br><br>
        <h2>Student Information &amp; Final Grades (<%= course.getAttribute(XMLBuilder.A_CODE)%>)</h2>

        <table width="100%" class="assignment_table" cellpadding="1" cellspacing="0" border="0">
          <tr>
            <th>Last Name</th>
            <th>First Name</th>
            <th>NetID</th>
            <th>Student ID</th>
        <% if (isStaff) { %>
            <th>Final Grade</th>
        <% } %>
       	<% if (hasSection) { %>
            <th>Section</th>
        <% } %>
            <th>Grade Basis</th>
          </tr>
          <% for(int i=0; i < students.getLength(); i++) {
              Element student = (Element) students.item(i); %>
              <tr>
                <td><%= student.getAttribute(XMLBuilder.A_LASTNAME) %></td>
                <td><%= student.getAttribute(XMLBuilder.A_FIRSTNAME) %></td>
                <td><%= student.getAttribute(XMLBuilder.A_NETID) %></td>
                <td><%= student.getAttribute(XMLBuilder.A_CUID) %></td>
            <% if (isStaff) { %>
                <td><%= student.getAttribute(XMLBuilder.A_FINALGRADE) %></td>
            <% } %> 
          	<% if (hasSection) { %>
                <td><%= student.getAttribute(XMLBuilder.A_SECTION) %></td>
            <% } %>
                <td><%= student.getAttribute(XMLBuilder.A_GRADEOPTION) %></td>
              </tr>
          <% } %>
        </table>
        <br />
        <div style="float:left">
        	<a href="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_VIEWUPLOAD + "&amp;" + AccessController.P_COURSEID + "=" + courseID %>">Upload class info</a>
        </div>
        <div style="float: right">
	        Export as <a href="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_EXPORTFINALGRADES + "&amp;" + AccessController.P_COURSEID + "=" + courseID %>">CSV</a>
	        <br>
	        Export as <a href="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_EXPORTFINALGRADESPS + "&amp;" + AccessController.P_COURSEID + "=" + courseID %>">CSV for
	        PeopleSoft upload</a>
	    </div>
        <br />
        <br />
	    <div class="assignment_left">
	    <form action="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_RELEASEFINALGRADES + "&amp;" + AccessController.P_COURSEID + "=" + courseID %>" method="post">
	     <br />
	     <table class="assignment_table" cellpadding="0" cellspacing="0" border="0">
	     <tr>
          <td>
        	<input name="<%= AccessController.P_FINALGRADES %>" type="checkbox" <%= Boolean.valueOf(course.getAttribute(XMLBuilder.A_SHOWFINALGRADES)).booleanValue() ? "checked=\"checked\"" : "" %>>
           Release Final Grades to students
          </td>
          <td>Allow students to see their final grades online</td>
         </tr>
         </table>

          <br />
          <input type="submit" value="Set">

        </form>
	    </div>
      </div>
    </td>
   <td id="course_menu_container"><div id="course_menu_top">&nbsp;</div></td>
  </tr>
<jsp:include page="../../footer.jsp"/>
