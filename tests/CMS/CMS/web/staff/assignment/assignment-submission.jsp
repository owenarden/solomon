<%@ page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.www.util.*, edu.cornell.csuglab.cms.www.xml.*, java.util.*" %>
<%
Document displayData = (Document) session.getAttribute(AccessController.A_DISPLAYDATA); 
Element root = (Element) displayData.getElementsByTagName(XMLBuilder.TAG_ROOT).item(0); 
Element assignment = (Element) XMLUtil.getFirstChildByTagName(root, XMLBuilder.TAG_ASSIGNMENT);
Element submissions = (Element) assignment.getElementsByTagName(XMLBuilder.TAG_SUBMISSIONS).item(0); 
// XXX removed April 2, 2010 by aip23 - obsolete
// Element filetypes = (Element) root.getElementsByTagName(XMLBuilder.TAG_FILETYPES).item(0);
// NodeList types = filetypes.getChildNodes();
%>
<h2>
  Required Submissions
  <span id="submissionshead">
    <a href="#" onClick="hide('submissions', '(hide)', '(show)'); return false;" class="hide">(hide)</a>
  </span>
</h2>
<div id="submissions" class="showhide">
<table id="subtable" class="assignment_table" cellpadding="0" cellspacing="0" border="0" width="100%" style="text-align: center">
  <tr>
    <th scope="col">File Name</th>
    <th scope="col">File Type(s)</th>
    <th scope="col">Max File Size</th>
    <th scope="col">Remove</th>
  </tr>
<%
/* Existing Files */
NodeList files = submissions.getElementsByTagName(XMLBuilder.TAG_ITEM); 
for (int i = 0; i < files.getLength(); i++) {
  int rowNum = i + 1;
  Element file = (Element) files.item(i); 
  String id = file.getAttribute(XMLBuilder.A_ID);
  String name = file.getAttribute(XMLBuilder.A_NAME);
  String size = file.getAttribute(XMLBuilder.A_SIZE);
  String typelist = file.getAttribute(XMLBuilder.A_TYPELIST);
  if (typelist.equals("accept any")) typelist = EditAssignUtil.FT_ANY;
  String ftcategory = EditAssignUtil.findFileTypeCategory(typelist);
  System.out.println(ftcategory);
%>
  <tr>
    <td align="center">
      <span id="spryFileName<%= rowNum %>">
        <input name="<%= AccessController.P_REQFILENAME + id %>" type="text" value="<%= name %>" onkeypress="keyListener(event, this);" size="35" maxlength="63">
        <span class="textfieldRequiredMsg">Value required</span>
      </span>
    </td>
    <td align="center">
      <select onChange="checkFileTypeMenu(this)" name="<%= AccessController.P_REQFILECAT + id %>">
<%
  for (int j = 0; j < EditAssignUtil.ftcs.size(); j++) {
    FileTypeCategory ftc = (FileTypeCategory)(EditAssignUtil.ftcs.get(j));
    String cat = ftc.getCategory();
    String fts = ftc.getFileTypes();
    if (cat.equals(EditAssignUtil.FTC_ANY)) {
%>
        <option value="<%= cat %>" <%= ftcategory.equals(cat) ? "selected" : "" %>><%= cat %></option>
<%
    } else {
%>
        <option value="<%= cat %>" <%= ftcategory.equals(cat) ? "selected" : "" %>><%= cat %> (<%= fts %>)</option>
<%
    }
  }
%>
        <option value="<%= EditAssignUtil.FTC_CUSTOM %>" <%= ftcategory.equals(EditAssignUtil.FTC_CUSTOM) ? "selected" : "" %>><%= EditAssignUtil.FTC_CUSTOM %></option>
      </select>
      <div <%= ftcategory.equals(EditAssignUtil.FTC_CUSTOM) ? "" : "style=\"display: none\"" %>>
        <span style="font-size: 10px;">File extensions (comma separated): </span>
        <br>
        <input name="<%= AccessController.P_REQFILETYPES + id %>" type="text" value="<%= typelist %>" size="20">
      </div>
    </td>
    <td align="center">
      <span id="spryFileSize<%= rowNum %>">
        <input name="<%= AccessController.P_REQSIZE + id %>" type="text" value="<%= size %>" size="8" maxlength="8">&nbsp;KB
        <span class="textfieldRequiredMsg">Value required</span>
        <span class="textfieldMinValueMsg">Value too small</span>
        <span class="textfieldMaxValueMsg">Value too large</span>
      </span>
    </td>
    <td align="center">
      <input name="<%= AccessController.P_REMOVEREQ + id %>" type="checkbox" onChange="checkRow(this)">
    </td>
  </tr>
<%
}
%>
</table>
<br>
<div id="addsub">
  <div class="joke" style="float: right">The system maximum allowed file size is <%= AccessController.maxFileSize / 1000000 %> MB</div>
  <a class="button" onClick="appendReqSubRow(); return false;" href="#">(Add a Row)</a>
<%--  <input name="remChkdReqSubRows" type="button" onClick="removeCheckedRows(reqSubTable, reqSubChkCol)" value="Remove Selected Rows"> --%>
</div>


<script type="text/javascript">
<!--
var reqSubTable = document.getElementById('subtable');
var reqSubChkCol = 3;  // column containing checkbox, 0-based
var reqSubTypeCol = 1;  // column containing file type menu

<%
/* Existing Files */
for (int i = 0; i < files.getLength(); i++) {
	int rowNum = i + 1;
%>
	var spryFileName<%= rowNum %> = new Spry.Widget.ValidationTextField("spryFileName<%= rowNum %>","custom", {characterMasking:/[\w]/, useCharacterMasking:true, hint:"File Name"});
	var spryFileSize<%= rowNum %> = new Spry.Widget.ValidationTextField("spryFileSize<%= rowNum %>", "real", {minValue:0.001, maxValue:100000, useCharacterMasking:true});
<%
}
%>

//-->
</script>




</div>