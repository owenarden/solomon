<%@ page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.www.xml.*, edu.cornell.csuglab.cms.base.*" %><%
Document displayData= (Document) session.getAttribute(AccessController.A_DISPLAYDATA); 
Element root= (Element) displayData.getElementsByTagName(XMLBuilder.TAG_ROOT).item(0); 
Element assignment= (Element) XMLUtil.getFirstChildByTagName(root, XMLBuilder.TAG_ASSIGNMENT);
NodeList subproblems= assignment.getElementsByTagName(XMLBuilder.TAG_SUBPROBLEM);
NodeList hiddensubprobs= assignment.getElementsByTagName(XMLBuilder.TAG_HIDDENSUBPROB);
int assignType = Integer.parseInt(assignment.getAttribute(XMLBuilder.A_ASSIGNTYPE));
boolean isQuiz = assignType == AssignmentBean.QUIZ;
boolean isAssignment = assignType == AssignmentBean.ASSIGNMENT;
boolean isSurvey = assignType == AssignmentBean.SURVEY;
%>

<h2>
  <%= isSurvey ? "Survey" : "Quiz"%> Questions
  <span id="questionshead">
    <a href="#" onClick="hide('questions', '(hide)', '(show)'); return false;" class="hide">(hide)</a>
  </span>
</h2>
<div id="questions" class="showhide">
  <table id="questtable" class="assignment_table" cellpadding="0" cellspacing="0" border="0" style="width: 100%; text-align: center">
    <tr>
      <th colspan = "2">Question Number</th>
      <th>Question Name</th>
      <th>Question Type</th>
      <%=assignType == AssignmentBean.QUIZ ? "<th>Score</th>" : ""%>
	  <th>Remove</th>
    </tr>
    <% for (int i= 0; i != subproblems.getLength(); i++) {
     Element subprob= (Element) subproblems.item(i);
     int subprobType = Integer.parseInt(subprob.getAttribute(XMLBuilder.A_TYPE));
     String subID= subprob.getAttribute(XMLBuilder.A_ID); %>
    <tr id = "quest<%=subprob.getAttribute(XMLBuilder.A_ORDER)%>">
      <td style="text-align: left">
        <%= Integer.parseInt(subprob.getAttribute(XMLBuilder.A_ORDER))%>.
      </td>
      <td style="text-align: left">
      	<a href="#" onClick="moveUp('quest<%=subprob.getAttribute(XMLBuilder.A_ORDER)%>'); return false;")>(Up)</a> 
      	<a href="#" onClick="moveDown('quest<%=subprob.getAttribute(XMLBuilder.A_ORDER)%>'); return false;")>(Down)</a>
      </td>
      <td style="text-align: left" id="sub<%=subID%>">
        <input size="40" name="<%= AccessController.P_SUBPROBNAME + subID %>" value="<%= org.apache.commons.lang.StringEscapeUtils.escapeHtml(subprob.getAttribute(XMLBuilder.A_NAME)) %>">
        
		<% String choiceDisplay = "none";
      	 if(subprobType == SubProblemBean.MULTIPLE_CHOICE){
      		choiceDisplay = "block";
      	 }%>

		<div id='choices<%= subID %>' style="display: <%= choiceDisplay %>">
		<%
	        NodeList choices = subprob.getElementsByTagName(XMLBuilder.TAG_CHOICE);
			String answer = subprob.getAttribute(XMLBuilder.A_CORRECTANSWER);
	        
	        for(int j = 0; j < choices.getLength(); j++){
	        	Element choice = (Element)choices.item(j);
	        	String choiceDivID = "choice_" + subID + "_" + j;
	        	%>
				<div id="<%= choiceDivID %>">
				<span><%=choice.getAttribute(XMLBuilder.A_LETTER)%>.</span><input value="<%=choice.getAttribute(XMLBuilder.A_ID)%>" name="<%=AccessController.P_CORRECTCHOICE%><%=subprob.getAttribute(XMLBuilder.A_ID)%>" type="radio" <%=(choice.getAttribute(XMLBuilder.A_ID).equals(answer)) ? "checked" : ""%>>
				<input value="<%=org.apache.commons.lang.StringEscapeUtils.escapeHtml(choice.getAttribute(XMLBuilder.A_TEXT))%>" name="<%=AccessController.P_CHOICE%><%=subprob.getAttribute(XMLBuilder.A_ID)%>_<%=choice.getAttribute(XMLBuilder.A_ID)%>" size="20" type="text">
	        	<input type="checkbox" name="<%= AccessController.P_REMOVECHOICE + choice.getAttribute(XMLBuilder.A_ID) %>">
	        	<strong>Remove</strong>
	        	</div>
			<%}
        %>
		</div>
		<div id="addChoice_<%= subID %>" style="display: <%= choiceDisplay %>; padding: 3px"><a class="replace" href="#" onClick="addChoice('choices<%= subID %>', <%=subID%>, false); return false">(Add Choice)</a></div>
		
		<script type="text/javascript">
			choiceindex[<%=subID%>] = <%=choices.getLength()%>;
			var i = <%= subprobType %>;
		</script>

      </td>
      <td style="text-align: center">
	      <select onChange="showAddChoice(this, '<%= subID %>')" style="width: 100%;" name="<%= AccessController.P_SUBPROBTYPE + subID %>" size="1">
	      	<%int probType = Integer.parseInt(subprob.getAttribute(XMLBuilder.A_TYPE));%>
	      	<option value="<%=SubProblemBean.MULTIPLE_CHOICE%>" <%=probType == SubProblemBean.MULTIPLE_CHOICE ? "selected" : ""%>>Multiple Choice</option>
	      	<option value="<%=SubProblemBean.FILL_IN%>" <%=probType == SubProblemBean.FILL_IN ? "selected" : ""%>>Fill In</option>
	      	<option value="<%=SubProblemBean.SHORT_ANSWER%>" <%=probType == SubProblemBean.SHORT_ANSWER ? "selected" : ""%>>Short Answer</option>
	      </select>
      </td>
      <% if (isQuiz) { 
      		String scoreInputName = AccessController.P_SUBPROBSCORE + subID;
      %>
      <td style="text-align: center">
        <input onkeyup="updateTotalScore();return false" id="<%= scoreInputName %>" size="3" name="<%= scoreInputName %>" value="<%= subprob.getAttribute(XMLBuilder.A_TOTALSCORE) %>">
      </td>
      <script type="text/javascript">
      	scoreInputs[<%= i %>] = "<%= scoreInputName %>";
      </script>
      <% } %>
      <td style="text-align: center">
        <input type="checkbox" name="<%= AccessController.P_REMOVESUBPROB + subID %>">
      </td>
    </tr>
<% } %>
	
  </table>
<script type="text/javascript">
	questindex = <%= subproblems.getLength()%>;
</script>
  <table id="addsub" cellpaddign='5' cellspacing='0' border='0' width='100%'>
  	<tr>
  		<td>
    <a href="#" onClick="addQuestRow(<%=assignType==AssignmentBean.QUIZ ? "true" : "false"%>); return false;" class="button">(New Row)</a>&nbsp;
  		</td>
	<% if (isQuiz) { %>
	<!-- display total score here -->
		<td align='right'>
		<strong>Total Score:</strong>
		<span id='total_score'><%= assignment.getAttribute(XMLBuilder.A_TOTALSCORE) %></span>
		</span>
	<% } %>
	</tr>
  </table>
  
 <!-- RESTORE OPTIONS FOR REMOVED SUBPROBLEMS -->
<% if (hiddensubprobs.getLength() != 0) { %>
  <div class="replace">
    <span id="removedprobshead">
      <a href="#" onClick="show('removedprobs', 'Removed problems &raquo;', '&laquo; Removed problems'); return false;">Removed problems &raquo;</a>
    </span>
    <table class="replace" id="removedprobs" cellpadding="0" cellspacing="0" style="display:none">
      <tr>
        <th>Problem Name</th>
        <th>Total Score</th>
        <th>Restore</th>
      </tr>
  <% for (int i= 0; i != hiddensubprobs.getLength(); i++) { 
     Element prob= (Element) hiddensubprobs.item(i);
     String probID = prob.getAttribute(XMLBuilder.A_ID); %>
      <tr class="<%= i % 2 == 0 ? "row_even" : "row_odd" %>">
        <td><input type="hidden" name="<%= AccessController.P_HIDDENPROBNAME + probID %>" value="<%= prob.getAttribute(XMLBuilder.A_NAME) %>">
        	<%= prob.getAttribute(XMLBuilder.A_NAME) %></td>
        <td><input type="hidden" name="<%= AccessController.P_HIDDENPROBSCORE + probID %>" value="<%= prob.getAttribute(XMLBuilder.A_TOTALSCORE) %>">
        	<%= prob.getAttribute(XMLBuilder.A_TOTALSCORE) %></td>
        <td>
          <input type="checkbox" name="<%= AccessController.P_RESTORESUBPROB + probID %>"<%= prob.hasAttribute(XMLBuilder.A_RESTORED) ? " checked" : "" %>>
        </td>      
      </tr>
  <% } %>
    </table>
  </div>
<% } %>

