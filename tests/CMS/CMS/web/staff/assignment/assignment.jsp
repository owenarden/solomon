<%@ page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.www.xml.*,edu.cornell.csuglab.cms.base.AssignmentBean;"%><%
Document displayData= (Document)session.getAttribute(AccessController.A_DISPLAYDATA);
Element root= (Element)displayData.getChildNodes().item(0);
Element course= XMLUtil.getFirstChildByTagName(root, XMLBuilder.TAG_COURSE); 
Element assignment= XMLUtil.getFirstChildByTagName(root, XMLBuilder.TAG_ASSIGNMENT); 
Element status= (Element)root.getElementsByTagName(XMLBuilder.TAG_STATUS).item(0);
int assignType = Integer.parseInt(assignment.getAttribute(XMLBuilder.A_ASSIGNTYPE));
String assignID= assignment.getAttribute(XMLBuilder.A_ASSIGNID);
%><jsp:include page="../../header.jsp" />

<link rel="stylesheet" href="SpryAssets/SpryValidationTextField.css" type="text/css">
<link rel="stylesheet" href="SpryAssets/SpryValidationSelect.css" type="text/css">
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script src="CalendarPopup.js" type="text/javascript"></script> <%-- required by formattedtextboxes.js --%>
<script src="datetime.js" type="text/javascript"></script> <%-- required by formattedtextboxes.js --%>
<script src="formattedtextboxes.js" type="text/javascript"></script>
<script type="text/javascript"><jsp:include page="assignscript.js.jsp"/></script>
<jsp:include page="../../header-page.jsp" />
<div id="course_wrapper_withnav">
<table id="course_wrapper_table" summary="course wrapper" cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<jsp:include page="../navbar.jsp"/>
  <td id="course_page_container">
  	<%
	String assignTypeName = "";
	if(assignType == AssignmentBean.ASSIGNMENT)
	{
		assignTypeName = "Assignment";
	}
	else if(assignType == AssignmentBean.QUIZ)
	{
		assignTypeName = "Quiz";
	}
	else if(assignType == AssignmentBean.SURVEY)
	{
		assignTypeName = "Survey";
	}
  	%>
    <form action="?<%= AccessController.P_ACTION %>=<%=AccessController.ACT_SETASSIGN%>&amp;<%= AccessController.P_COURSEID %>=<%= course.getAttribute(XMLBuilder.A_COURSEID) %>&amp;<%= AccessController.P_ASSIGNID %>=<%= assignment.getAttribute(XMLBuilder.A_ASSIGNID) %>" 
     method="post" enctype="multipart/form-data" <%--onSubmit="return validateAllFormattedTextboxes() && checkData();" --%>>
      <input type="hidden" name="<%= AccessController.P_ASSIGNID %>" value="<%= assignID %>">
      <input type="hidden" name="<%= AccessController.P_ASSIGNMENTTYPE %>" value="<%= assignType %>">
      <div id="course_page">
				<jsp:include page="../../problem-report.jsp"/>
        <span class="assignment_title"><%= assignID.equals("0") ? "New " + assignTypeName : assignment.getAttribute(XMLBuilder.A_NAME) %></span>
        <% if(assignID.equals("0")) { %>
        <div>
        <p>
        	<% if(assignTypeName.equals("Assignment")) { %>
        		An assignment is graded with optional online file submissions.
        	<% } else if(assignTypeName.equals("Quiz")) { %>
        		A quiz is graded and taken online by students through CMS.
        	<% } else if(assignTypeName.equals("Survey")) { %>
        		A survey is ungraded and used to receive response from students anonymously. 
        	<% } %>
        </p> <br />
        </div>
        <% } %>
        <div class="assignment_left">
          <jsp:include page="assignment-general.jsp"/>
        </div>
        <div class="assignment_left">
         <jsp:include page="assignment-description.jsp"/>
        </div>
        <% if (assignType == AssignmentBean.ASSIGNMENT) { %>
        <div class="assignment_left">
          <jsp:include page="assignment-groups.jsp"/>
        </div>
        <div class="assignment_left">
          <jsp:include page="assignment-submission.jsp"/>
        </div>
        <div class="assignment_left">
          <jsp:include page="assignment-files.jsp"/>
        </div>
        <div class="assignment_left">
          <jsp:include page="assignment-problems.jsp"/>
        </div>
        <div class="assignment_left">
          <jsp:include page="assignment-schedule.jsp"/>
        </div>
        <%}
        else{%>
   	    <div class="assignment_left">
          <jsp:include page="survey-questions.jsp"/>
        </div>
        <%}%>
        <div class="assignment_left">
          <input type="submit" onclick="return checkData();" value="Submit">
        </div>
        <div class="assignment_left">

        </div>
      </div>
    </form>
  </td>
  <td id="course_menu_container"><div id="course_menu_top">&nbsp;</div></td>
</tr>
<jsp:include page="../../footer.jsp"/>