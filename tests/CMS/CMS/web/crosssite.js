// Requires: cookies.js, setup javascript variables CURRENT_USER and HOSTGROUP by jsp

/*** Things which should be set up by the JSP (for reference):
 *
 * netID of the principal currently logged in, or "guest" if a guest
 * Example:
 *   var CURRENT_USER = "da10"; 
 *
 * JSON array of {host, semester_id} objects, describing group of servers, to be populated by JSP code
 * should NOT include the current server itself in the array (JSP and/or java should filter it out)
 * Example:
 *   var HOSTGROUP = [
 *	 {"host":"https://www3.csuglab.cornell.edu/web/auth/", "semID": 13},
 *	 {"host":"https://www3.csuglab.cornell.edu/web/auth/", "semID": 3}
 *	 ];
 *
 **/

// global vars
var onOverviewPage; // global var, whether we need to get the full overview data
var xmlCache = {}; // the global object where the cache will be
var pendingCacheKey = ""; // var which holds the key of where the currently-loading XML should go in the cache
var xmlParseVersion = 0; // version of XML response from server, currently formatted as date/integer YYYYMMDD
var currentServerBase = ''; // global var which contains the host of the other cms server (URL building block)

// actual constants
var TEMPTAG = 'dataReq';

var XML_COOKIE_NAME = "xsitexmlstore";
var XML_COOKIE_SEP = '!'; var XML_COOKIE_STRIDE_SEP = '#'; // both of these are escaped in escape(); do NOT escape these when going into the cookie
var XML_CACHE_FULL = 'full'; var XML_CACHE_BRIEF = 'brief';
var XML_CACHE_TIME = 15*60*1000; // 15 minutes, in milliseconds

var LOAD_TIMEOUT = 20*1000; // 20 seconds, in milliseconds

//  TODO: These should eventually be handled in the .js.jsp, to avoid desynchronization
var MAIN_TABLES_DIV_ID = "main_table_container";
var STUDENT_COURSE_MENU_ID = "studCourseMenu";
var STAFF_MENU_ID = "staffCourseMenu";
var STUDENT_COURSE_TABLE_DIV_ID = "studCourseTableDiv";
var ASSIGNMENT_TABLE_DIV_ID = "assignmentTableDiv";
var COURSE_ANNOUNCE_DIV_ID = "courseAnnouncementTableDiv";
var ACTIVE_COURSE_TABLE_ID = "activeCourseTable";
var ASSIGNMENT_TABLE_ID = "assignmentTable";
var COURSE_ANNOUNCE_ID = "courseAnnouncementTable";
var CROSSITE_MESSAGE_TABLE_ID = "crossiteStatusTable";
var CROSSITE_MESSAGE_LIST_ID = "crossiteMessageList";
var CROSSITE_STATUS_ID = "crossiteStatus";

/*** CACHE MANAGEMENT ***/

/* Todo: generalize
 * Todo: utilize multiple cookies if data is very large
 *
 * Cache is a map, i.e.
 *  xmlCache[host].type = 'full' or 'brief'
 *  xmlCache[host].data = xml
 *  xmlCache[host].exp = date, in ms from epoch
 * 
 * the cache stored to and loaded from a cookie as the principal, then a strided list of 4-tuples
 * principal!host#type#expdate#escaped_xml!host#type#expdate#escaped_xml! ...
 *
 * host: escaped base url
 * isfull: true if this is the overview data, else this is just the nav stuff
 * expdate: milliseconds from epoch
 * escaped_xml: escaped cached xml
 */
 
function prepXMLCache() // Initialize the xmlCache and also remove anything that's been expired
{
	var now = (new Date()).getTime();
	
	var rawcookie = getRawCookie(XML_COOKIE_NAME); // from cookies.js
	if (rawcookie == null) return; // we're done
	var strides = rawcookie.split(XML_COOKIE_SEP);
	
	// strides[0] is the principal
	if (strides[0] != CURRENT_USER)
	{
		deleteCookie(XML_COOKIE_NAME); // clear cookie cache
		return; // we're finished
	}
	
	for (var i = 1; i < strides.length; i++)
	{
		var tuple = strides[i].split(XML_COOKIE_STRIDE_SEP);
		// 0 = host, 1 = type, 2 = exp date, 3 = escaped data
		var host = tuple[0];
		var exp = parseInt(tuple[2]);
		if (now < exp) {
			var obj = {}; // new Object();
			obj.type = tuple[1];
			obj.data = unescape(tuple[3]);
			obj.exp = exp;
			xmlCache[host] = obj;
		}
	}
	
	saveXMLCache(); // throw away anything that has expired within the cookie
}

function saveXMLCache() // TODO: check if we're over the 4KB 'limit', and split into multiple cookies
{
	var cookiedata = CURRENT_USER; // first portion is always the user being cached
	var now = (new Date()).getTime();
	var maxExp = now;
	var blank = true;
	
	for (host in xmlCache)
	{
		var expTime = xmlCache[host].exp;
		// don't save any expired items in cache
		if (now < expTime)
		{
			if (maxExp < expTime) maxExp = xmlCache[host].exp;
			
			var stride = buildCacheStride(host, xmlCache[host].type, xmlCache[host].exp, escape(xmlCache[host].data));
			cookiedata += XML_COOKIE_SEP + stride;
			blank = false;
		}
	}
	
	var maxExpDate = new Date();
	maxExpDate.setTime(maxExp);
	
	if (blank) deleteCookie(XML_COOKIE_NAME);
	else setRawCookie(XML_COOKIE_NAME, cookiedata, maxExpDate);
}

function addXMLCache(host, type, exp, xml) // for freshly downloaded data. Adds both to xmlCache and cookie.  string, string, Date, string
{
	var obj = {}; // new Object();
	obj.type = type;
	obj.data = xml;
	obj.exp = exp.getTime();
	xmlCache[host] = obj;
	
	// append the new data onto the end of the old cookie
	var oldcookie = getRawCookie(XML_COOKIE_NAME);
	var stride = buildCacheStride(host, type, exp.getTime(), escape(xml));
	var newcookie = ((oldcookie == null) ? CURRENT_USER : oldcookie) + XML_COOKIE_SEP + stride;
	
	if (newcookie.length > 4000) // too big!
		saveXMLCache(); // try a clean save
	else setRawCookie(XML_COOKIE_NAME, newcookie, exp);
}

function buildCacheStride(key, meta, datems, data) // dumb string, string, int, string
{
	return key + XML_COOKIE_STRIDE_SEP + meta + XML_COOKIE_STRIDE_SEP + datems + XML_COOKIE_STRIDE_SEP + data;
}

/*** End cache stuff ***/

/*** CROSS-SITE FETCHING LOGIC ***/

// on (just after) document load, call the following function:
function getCrossSiteData()
{
	// init
	onOverviewPage = checkIfOverview(); // global
	prepXMLCache();
	// temp globals
	tempHostIndex = 0;
	hasCrossiteError = false; // signals whether or not there has been any errors
	attemptedHost = ''; // which host was supposed to have been contacted (for errors)
	waitingForLoad = 0;
	
	setStatus("Fetching data from other instances of CMS...");
	loadXMLHelper(); // start loop
}

function loadXMLHelper()
{
	var now = (new Date()).getTime();
	
	if (waitingForLoad != 0) { // still waiting for the previous load to finish
		var time = now - waitingForLoad;
		if (time > LOAD_TIMEOUT) { // timed out
			addError("Couldn't get data from <a href=\"" + attemptedHost + "\">" + extractHostFromURL(attemptedHost) + "</a>; no response.");
		}
		else {
			var secs = time/1000;
			setStatus("Waiting for data from <a href=\"" + attemptedHost + "\">" + extractHostFromURL(attemptedHost) + "</a> (" + tempHostIndex + " of " + HOSTGROUP.length + ") (" + secs.toFixed(1) + " seconds...)");
			setTimeout(loadXMLHelper, 100); // arbitrary delay
			return;
		}
	}
	
	if (tempHostIndex < HOSTGROUP.length)
	{
		var pair = HOSTGROUP[tempHostIndex];
		var url = pair.host + "?action=" + (onOverviewPage ? "overviewdata" : "navdata") + "&semesterid=" + pair.semID;
		var key = pair.host + '?semid=' + pair.semID;
		
		attemptedHost = pair.host;
		
		tempHostIndex += 1;
		setStatus("Fetching your data from <a href=\"" + pair.host + "\">" + extractHostFromURL(pair.host) + "</a> (" + tempHostIndex + " of " + HOSTGROUP.length + ")...");
		
		if (typeof xmlCache[key] != "undefined" && (!onOverviewPage || xmlCache[key].type == XML_CACHE_FULL) && xmlCache[key].exp > now)
			addDataToPage(xmlCache[key].data);
		else
		{
			pendingCacheKey = key; // save the key for later
			waitingForLoad = now;
			loadJSXML(url);
		}
		setTimeout(loadXMLHelper, 100); // arbitrary delay
		return;
	}
	else
	{
		// done!
		setStatus(null);
		delete tempHostIndex;
		delete hasCrossiteError;
		delete attemptedHost;
		delete waitingForLoad;
	}
}

function checkIfOverview() // this should really be called only once
{
	return (document.getElementById(MAIN_TABLES_DIV_ID) != null);
	
	/* OLD CODE: Below isn't quite correct; action=loginview can send someone to their class instead of overview if they have only one course
	
	// figure out if we're on the overview page
	query = window.location.search; // includes the question mark
	if (query.length < 2) return false; // need at least the "?"
	query = query.substring(1, query.length); // strip the question mark
	pairs = query.split('&');
	for (i = 0; i < pairs.length; i++)
	{
		pair = pairs[i].split('=');
		if (pair[0] == "action") return (pair[1] == "overview" || pair[1] == "loginview");
	}
	return false;
	*/
}

function loadJSXML(jsurl)
{
	// remove old script
	var garbage = document.getElementById(TEMPTAG);
	if (garbage != null) {
		garbage.parentNode.removeChild(garbage);
		delete garbage;
	}

	var head = document.getElementsByTagName("head")[0];
	scrNode = document.createElement("script");
	scrNode.setAttribute("id", TEMPTAG);
	scrNode.setAttribute("type", "text/javascript");
	scrNode.setAttribute("language", "JavaScript");
	scrNode.setAttribute("src", jsurl);
	head.appendChild(scrNode);
}

function processRemoteXML() // xml should be in global variable xml; only remote scripts call this
{
	var exp = new Date(); // current time
	exp.setTime(exp.getTime() + XML_CACHE_TIME);
	addXMLCache(pendingCacheKey, (onOverviewPage ? XML_CACHE_FULL : XML_CACHE_BRIEF), exp, xml);
	addDataToPage(xml);
	
	waitingForLoad = 0;
}

function addDataToPage(xml)
{
	var xmlDoc;
	// parse
	try {
		var parser=new DOMParser();
		xmlDoc=parser.parseFromString(xml,"text/xml");
	}
	catch(e)
	{
		try {
			xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async="false";
			xmlDoc.loadXML(xml);
		}
		catch(e) {
			addError("Couldn't process data because no XML parser is available from your browser.");
			return;
		}
	}

	try {
		if (xmlDoc)
		{
			// global vars: base url and version
			currentServerBase = xmlDoc.getElementsByTagName("thishost")[0].firstChild.nodeValue;
			// Note: force parse ints as base 10, in case someone something starts with a zero (which gets interpreted to mean octal)
			xmlParseVersion = parseInt(xmlDoc.getElementsByTagName("version")[0].firstChild.nodeValue, 10);
			if (xmlParseVersion > 20081206) {
				addError("Couldn't process data from <a href=\"" + currentServerBase + "\">" + extractHostFromURL(currentServerBase) + "</a> because it is using a version of CMS I am not compatible with");
				return;
			}
			
			var pNetID = xmlDoc.getElementsByTagName("principal")[0].getAttribute("netid");
			if (pNetID != CURRENT_USER) { // principals don't match, something's wrong, don't add this data
				addError("Couldn't integrate data from <a href=\"" + currentServerBase + "\">" + extractHostFromURL(currentServerBase) + "</a> because you are not logged in there.");
				return;
			}
			
			setStatus("Processing your data from <a href=\"" + currentServerBase + "\">" + extractHostFromURL(currentServerBase) + "</a> (" + tempHostIndex + " of " + HOSTGROUP.length + ")...");
			
			// add student courses to top menu and table
	
			// Gotcha: don't use childNodes, will interpret whitespace between the course tags as text nodes
			var stuCourXMLs = xmlDoc.getElementsByTagName("studentCourses")[0].getElementsByTagName("course");
	
			// Gotcha: don't try "for(index in stuCourXMLs)", stuCourXMLs is not a real array and you'll get 0, 1,... and the string "length"
			// (Same for "for each(course in stuCourXMLs)", will attempt to process stuCourXMLs.length as a course)
			for (i = 0; i < stuCourXMLs.length; i++)
			{
				appendStudCourse(stuCourXMLs[i]);
				if (onOverviewPage) appendCourseToTable(stuCourXMLs[i], false);
			}
			
			// add staff courses to top menu and table
			var staffcourXMLs = xmlDoc.getElementsByTagName("staffCourses")[0].getElementsByTagName("course");
			for (i = 0; i < staffcourXMLs.length; i++)
			{
				appendStaffCourse(staffcourXMLs[i]);
				if (onOverviewPage) appendCourseToTable(staffcourXMLs[i], true);
			}
			
			if (!onOverviewPage) { // we're done
				setStatus("Finished integrating your data from <a href=\"" + currentServerBase + "\">" + extractHostFromURL(currentServerBase) + "</a> (" + tempHostIndex + " of " + HOSTGROUP.length + ").");
				return;
			}
			
			// add assignments
			var assignXMLs = xmlDoc.getElementsByTagName("allDueAssignments")[0].getElementsByTagName("assignment");
			for (i = 0; i < assignXMLs.length; i++)
			{
				appendAssign(assignXMLs[i]);
			}
			
			setStatus("Finished integrating your data from <a href=\"" + currentServerBase + "\">" + extractHostFromURL(currentServerBase) + "</a> (" + tempHostIndex + " of " + HOSTGROUP.length + ").");
		}
		else
		{
			addError("Couldn't process data from <a href=\"" + attemptedHost + "\">" + extractHostFromURL(attemptedHost) + "</a>, possibly because your browser's XML parser couldn't process the file.");
		}
	}//end try
	catch(e) {
		addError("Couldn't process data from <a href=\"" + attemptedHost + "\">" + extractHostFromURL(attemptedHost) + "</a>, because the file was malformed.");
	}
}

/*** APPENDERS: add data to page, will create/unhide missing page elements as needed **/

function appendStudCourse(courseXML) {
	// add to top menu
	var courseNum = courseXML.getAttribute("courseid");
	var courseCode = courseXML.getAttribute("displayedcode");

	var menu = document.getElementById(STUDENT_COURSE_MENU_ID);
	if (menu == null) menu = makeStudMenu();
	var elem = document.createElement("li");
	var link = document.createElement("a");
	link.href = currentServerBase + "?action=course&courseid=" + courseNum + "&reset=1";
	link.appendChild(document.createTextNode(courseCode));
	elem.appendChild(link);
	menu.appendChild(elem);
}

function appendStaffCourse(courseXML) {
	var courseNum = courseXML.getAttribute("courseid");
	var courseCode = courseXML.getAttribute("displayedcode");

	var menu = document.getElementById(STAFF_MENU_ID);
	if (menu == null) menu = makeStaffMenu();
	var elem = document.createElement("li");
	var link = document.createElement("a");
	link.href = currentServerBase + "?action=courseadmin&courseid=" + courseNum + "&reset=1";
	link.appendChild(document.createTextNode(courseCode));
	elem.appendChild(link);
	menu.appendChild(elem);
}

function appendCourseToTable(courseXML, staff) {
	var courseNum = courseXML.getAttribute("courseid");
 	var courseCode = courseXML.getAttribute("displayedcode");
	var courseName = courseXML.getAttribute("coursename");

	showMainTables();
	var table = document.getElementById(ACTIVE_COURSE_TABLE_ID);

	var tableBody = table.getElementsByTagName("tbody")[0];

	var tableRow = document.createElement("tr");

	var codeCell = document.createElement("td");
		var codeLink = document.createElement("a");
		if (staff)
			codeLink.href = currentServerBase + "?action=courseadmin&courseid=" + courseNum;
		else
			codeLink.href = currentServerBase + "?action=course&courseid=" + courseNum;
		codeLink.appendChild(document.createTextNode(courseCode));
		codeCell.appendChild(codeLink);
		if (staff)
			codeCell.appendChild(document.createTextNode(" (Staff)"));
		
	var nameCell = document.createElement("td");
	nameCell.appendChild(document.createTextNode(courseName));
	
	tableRow.appendChild(codeCell);
	tableRow.appendChild(nameCell);

	tableBody.appendChild(tableRow);
}

function appendAssign(assignXML) {
	var assignID = assignXML.getAttribute("assignid");
	var assignName = assignXML.getAttribute("name");
	var assignTimeLeft = assignXML.getAttribute("duedate");
	var assignStatus = assignXML.getAttribute("status");
	var courseID = assignXML.getAttribute("courseid");
	var courseCode = assignXML.getAttribute("coursename");
	
	showAssnTable();
	var assignTable = document.getElementById(ASSIGNMENT_TABLE_ID);
	var tableBody = assignTable.getElementsByTagName("tbody")[0];
	
	var tableRow = document.createElement("tr");
	
	var courseCell = document.createElement("td");
		courseCell.style.textAlign = "left";
		courseCell.style.whiteSpace = "nowrap";
		var courseLink = document.createElement("a");
			courseLink.href = currentServerBase + "?action=course&courseid=" + courseID;
			courseLink.appendChild(document.createTextNode(courseCode));
		courseCell.appendChild(courseLink);
	var assnCell = document.createElement("td");
		assnCell.style.textAlign = "left";
		assnCell.style.whiteSpace = "nowrap";
		var assnLink = document.createElement("a");
			assnLink.href = currentServerBase + "?action=assignment&assignid=" + assignID;
			assnLink.appendChild(document.createTextNode(assignName));
		assnCell.appendChild(assnLink);
	var timeCell = document.createElement("td");
		timeCell.style.textAlign = "center";
		timeCell.style.whiteSpace = "nowrap";
		timeCell.appendChild(document.createTextNode(assignTimeLeft));
	var statusCell = document.createElement("td");
		statusCell.style.textAlign = "right";
		statusCell.style.whiteSpace = "nowrap";
		statusCell.appendChild(document.createTextNode(assignStatus));
	
	tableRow.appendChild(courseCell);
	tableRow.appendChild(assnCell);
	tableRow.appendChild(timeCell);
	tableRow.appendChild(statusCell);
	
	tableBody.appendChild(tableRow);
}

/* Status and error messages */
function setStatus(str) { // Pass in null or undefined to signal completion, which causes the whole display to disappear iff no errors
	if (str) {
		var statusLi = document.getElementById(CROSSITE_STATUS_ID);
		statusLi.innerHTML = str; //  Maybe add " <a href=\"#\" onclick=\"refreshCrossSite();\">(interrupt and retry?)</a>"
	}
	else if (hasCrossiteError) {
		var statusLi = document.getElementById(CROSSITE_STATUS_ID);
		statusLi.innerHTML = "Finished integrating data from other CMS servers, but with errors <a href=\"#\" onclick=\"refreshCrossSite();\">(retry?)</a>:";
	}
	else { // no error, remove table
		var msgTable = document.getElementById(CROSSITE_MESSAGE_TABLE_ID);
		msgTable.style.display = 'none';
	}
}

function extractHostFromURL(str) { // extract the actual host from the URL to use as the text, helper for status and error messages
	var host = str;
	var searchIndex = host.indexOf("//");
	if (searchIndex > -1) host = host.substring(searchIndex+2);
	searchIndex = host.indexOf("/");
	if (searchIndex > -1) host = host.substring(0, searchIndex);
	return host;
}

function addError(err) {
	hasCrossiteError = true;
	
	var msgList = document.getElementById(CROSSITE_MESSAGE_LIST_ID);
	
	var item = document.createElement("li");
	item.innerHTML = err;
	
	msgList.appendChild(item);
}

/*** DOM CREATORS: creates/shows elements which may be missing on initial page load ***/

function makeStudMenu() {
	var topmenus = document.getElementById("topnav").getElementsByTagName("ul")[0];
	var title = (document.getElementById(STAFF_MENU_ID) == null) ? "Courses" : "Student Courses";
	var menuhead = document.createElement("li");
	menuhead.className = "menuhead";
	menuhead.innerHTML = "<a>" + title + "</a>";
	
	var menu = document.createElement("ul");
	menu.id = STUDENT_COURSE_MENU_ID;

	menuhead.appendChild(menu);
	topmenus.insertBefore(menuhead, top.firstChild);
	return menu;
}

function makeStaffMenu() {
	var topmenus = document.getElementById("topnav").getElementsByTagName("ul")[0];

	// change title of student menu to say "Student Courses", if it exists
	var studMenu = document.getElementById(STUDENT_COURSE_MENU_ID); // need to go up one in the DOM tree for title
	if (studMenu != null) studMenu.parentNode.getElementsByTagName("a")[0].innerHTML = "Student Courses";

	var menuhead = document.createElement("li");
	menuhead.className = "menuhead";
	menuhead.innerHTML = "<a>Staff Courses</a>";
	
	var menu = document.createElement("ul");
	menu.id = STAFF_MENU_ID;

	menuhead.appendChild(menu);
	topmenus.appendChild(menuhead);
	return menu;
}

// (this is much much better than the messy "create DOM on demand" like makeStudMenu/makeStaffMenu...
// TODO: would it be faster to check if (blah.style.display != '')?
//   maybe blah.style.display assignment is expensive
function showMainTables() {
	var container = document.getElementById(MAIN_TABLES_DIV_ID);
	
	container.style.display = ''; // clear the "none", if it's there
}

function showAssnTable() {
	showMainTables();
	var assnDiv = document.getElementById(ASSIGNMENT_TABLE_DIV_ID);
	
	assnDiv.style.display = '';
}

function showAnnounceTable() {
	showMainTables();
	var annDiv = document.getElementById(COURSE_ANNOUNCE_DIV_ID);
	
	annDiv.style.display = '';
}

/*
function prepMainTables() {
  container = document.getElementById(MAIN_TABLES_DIV_ID);
  
  // automatically add course table too (no reason to want any of these tables without courses)
  courseDiv = document.createElement("div");
  courseDiv.className = "assignment_left";
  courseDiv.id = STUDENT_COURSE_TABLE_DIV_ID;

  courseHeader = document.createElement("h2");
  courseHeader.appendChild(document.createTextNode("Current Assignments"));

  courseTable = document.createElement("table");
    courseTable.id = ACTIVE_COURSE_TABLE_ID;
    courseTable.className = "assignment_table";
    courseTable.cellpadding = 0;
    courseTable.cellspacing = 0;
    courseTable.border = 0;
    courseTableCSS = "width: 100%";
    courseTable.style.cssText = courseTableCSS; // works for some browsers
    courseTable.setAttribute("style", courseTableCSS); // works for the rest
  courseTBody = document.createElement("tbody");
  courseTHeadRow = document.createElement("tr");
  courseTHeadCell1 = document.createElement("th");
    courseTHeadCell1.appendChild(document.createTextNode("Course Code"));
  courseTHeadCell2 = document.createElement("th");
    courseTHeadCell2.appendChild(document.createTextNode("Course Name"));

  courseTHeadRow.appendChild(courseTHeadCell1);
  courseTHeadRow.appendChild(courseTHeadCell2);
  courseTBody.appendChild(courseTHeadRow);
  courseTable.appendChild(courseTBody);
  
  courseDiv.appendChild(courseHeader);
  courseDiv.appendChild(courseTable);
  container.appendChild(courseDiv);

  assignmentDiv = document.createElement("div");
  assignmentDiv.className = "assignment_left";
  assignmentDiv.id = ASSIGNMENT_TABLE_DIV_ID;

  container.appendChild(assignmentDiv);

  cannounceDiv = document.createElement("div");
  cannounceDiv.className = "assignment_left";
  cannounceDiv.id = COURSE_ANNOUNCE_DIV_ID;

  container.appendChild(cannounceDiv);
}

function makeAssnTable() {
	assnDiv = document.getElementById(ASSIGNMENT_TABLE_DIV_ID);
	if (assnDiv == null)
	{
		prepMainTables();
		document.getElementById(ASSIGNMENT_TABLE_DIV_ID);
	}
	
	assnTable = document.createElement("table");
		assnTable.id = ASSIGNMENT_TABLE_ID;
		assnTable.id = "assignment_table";
		assnTable.cellpadding = 0;
		assnTable.cellspacing = 0;
		assnTable.border = 0;
		assnTableCSS = "width: 100%";
		assnTable.style.cssText = assnTableCSS; // works for some browsers
		assnTable.setAttribute("style", assnTableCSS); // works for the rest
	assnTBody = document.createElement("tbody");
	assnTHeadRow = document.createElement("tr");
	assnTHeadCell1 = document.createElement("th");
		assnTHeadCell1.appendChild(document.createTextNode("Course"));
		assnTHeadCell1Style = "text-align: left";
		assnTHeadCell1.style.cssText = assnTableCSS;
		assnTHeadCell1.setAttribute("style", assnTableCSS);
	assnTHeadCell2 = document.createElement("th");
		assnTHeadCell2.appendChild(document.createTextNode("Assignment"));
	assnTHeadCell3 = document.createElement("th");
		assnTHeadCell3.appendChild(document.createTextNode("Time Remaining"));
	assnTHeadCell4 = document.createElement("th");
		assnTHeadCell4.appendChild(document.createTextNode("Status"));
}*/

// Clears cache in order to do a clean re-attempt
function refreshCrossSite() {
	deleteCookie(XML_COOKIE_NAME);
	window.location.reload(true); /* Force uncached */
}