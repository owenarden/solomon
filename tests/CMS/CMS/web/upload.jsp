<%@ page language="java" import="org.w3c.dom.*, java.util.List, edu.cornell.csuglab.cms.author.Principal, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.www.util.*, edu.cornell.csuglab.cms.www.xml.*" %>
<%
/***************************************************************************************************
* upload a file with any kind of student info, or download a template for a final-grade upload
***************************************************************************************************/
Document displayData = (Document)session.getAttribute(AccessController.A_DISPLAYDATA);
Principal p = (Principal)session.getAttribute(AccessController.A_PRINCIPAL);
Element root = (Element)displayData.getElementsByTagName(XMLBuilder.TAG_ROOT).item(0);
Element course = XMLUtil.getFirstChildByTagName(root, XMLBuilder.TAG_COURSE);
String courseID = null;
if(course != null) /* course-specific page */
{
	courseID = course.getAttribute(XMLBuilder.A_COURSEID);
}%>
<style type="text/css">
div.filetransfer {padding: 10px; margin: 5px 0px; background-color: #f7f7f0; border: 1px solid #ddd}
div.filetransfer h1 {font-size: 1em; font-weight: bold; padding: 0px; margin: 0px}
div.filetransfer div {margin: 1em 0em 0em 1em}
</style>
	<div id="upload_general_div" class="filetransfer">
		Upload <a target="_blank" href="http://www.cs.cornell.edu/Projects/CMS/userdocs/uploadcsv.html#studentinfo">general student information</a>:
		<br><br>
		<form method="post" enctype="multipart/form-data" action="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_UPLOADSTUDENTINFO + ((course != null) ? ("&amp;" + AccessController.P_COURSEID + "=" + courseID) : "") %>">
		<input type="file" size="30" name="<%= AccessController.P_UPLOADEDCSV %>">
		&nbsp;<input type="submit" value="Upload">
		</form>
		<br>
		<a href="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_EXPORTSTUDENTINFOTEMPLATE + ((course != null) ? ("&amp;" + AccessController.P_COURSEID + "=" + courseID) : "") %>">Download</a> 
		a full template, from which you can remove the columns you are not interested in.
	</div>
	<!-- Old classlist format
	<br>
	<div id="upload_classlist_div" class="filetransfer">
		Upload <a target="blank" href="http://www.cs.cornell.edu/Projects/CMS/userdocs/finalclasslist.html">final classlist</a> (specific format needed at end of semester):
		<br><br>
		<form method="post" enctype="multipart/form-data" action="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_UPLOADSTUDENTINFO + ((course != null) ? ("&amp;" + AccessController.P_COURSEID + "=" + courseID) : "") + "&amp;" + AccessController.P_ISCLASSLIST + "=yes" %>">
		<input type="file" size="30" name="<%= AccessController.P_UPLOADEDCSV %>">
		&nbsp;<input type="submit" value="Upload">
		</form>
	</div>
	-->
	<br>
	<div id="upload_classnummap_div" class="filetransfer">
		Upload <a target="_blank" href="http://www.cs.cornell.edu/Projects/CMS/userdocs/uploadcsv.html#classnumber">class numbers</a> by course code and section:
		<br><br>
		<form method="post" enctype="multipart/form-data" action="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_UPLOADSTUDENTINFO + ((course != null) ? ("&amp;" + AccessController.P_COURSEID + "=" + courseID) : "") + "&amp;" + AccessController.P_ISSECTIONMAPPING + "=yes" %>">
		<input type="file" size="30" name="<%= AccessController.P_UPLOADEDCSV %>">
		&nbsp;<input type="submit" value="Upload">
		</form>
	</div>