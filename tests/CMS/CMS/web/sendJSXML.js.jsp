<%@page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.www.xml.*, java.io.StringWriter, edu.cornell.csuglab.cms.www.util.DOMWriter, javax.xml.transform.*, javax.xml.transform.stream.StreamResult, javax.xml.transform.dom.DOMSource;" %><%
/* This page simply wraps the XML document in javascript, then calls back processXML().
 * Be absolutely certain that all XML data that gets here has had correct permission checked already
 * Secure authentication (https) is recommended.
 */
Document displayData = (Document) session.getAttribute(AccessController.A_DISPLAYDATA);
StringWriter tempWriter = new StringWriter(); // I'd like to just chain these together, but must write to a temporary string at the moment
//DOMWriter.write(displayData, temp); // this is bad, escapes too much stuff
Transformer t = TransformerFactory.newInstance().newTransformer();
t.transform(new DOMSource(displayData), new StreamResult(tempWriter));
//Apache lang's escapeJavaScript also escapes too much stuff
%>var xml = '<%=tempWriter.toString().replaceAll("[\n\r]", "").replaceAll("\\'", "\\\\\\'") /* TODO: This is not an optimal implementation */ %>';
processRemoteXML();