/************************************
* methods used by assignment pages
*************************************/

function verifyFormData(form, table) {
    var filePaths = new Array();
    var fileTypes = new Array();
    var c = 0;
    for (var i = 0; i < form.elements.length; i++) {
        if (form.elements[i].type == "file") {
            filePaths[c] = form.elements[i].value;
            c++;
        }
    }
    c = 0;
    var tableData = table.getElementsByTagName("td");
    for (var i = 0; i < tableData.length; i++) {
        if (tableData[i].className == "filetype") {
        	fileTypes[c] = tableData[i].firstChild.nodeValue;
        	c++;
        }
    }
    
    var doUpload = false;
  	var errorMessage = '';
  	
  	for (var i = 0; i < filePaths.length; i++) {
  	    if ((fileTypes[i].toLowerCase()).indexOf('any') == -1 && filePaths[i].length > 0) {
  	        var valid = false;
  	        var error = '';
  		    var types = fileTypes[i].split(', ');
  		    for (var j = 0; j < types.length; j++) {
  		        if (!valid) {
  		            var result = checkExtension(filePaths[i]);
  		            var validExtension = result[0];
      	            if (validExtension) {
      	                var ext = result[1];
      	                var matchesAFileType = checkFileType(ext, types);
      	                if (matchesAFileType) {
      	                    valid = true;
      	                }
      	                else {
      	                    error = 'fails to match an accepted type: ' + fileTypes[i];
      	                }
      	            }
      	            else {
      	            	error = 'contains no file type extension';
      	            }
      	        }
  		    }
  		    if (valid) {
      	        doUpload = true;
      	    }
      	    else {
                errorMessage += 'File "' + filePaths[i] + '" ' + error + '.\n';
            }
  	    }
  	    else {
  	         doUpload = true;
  	    }
  	}
  	
  	if (errorMessage.length > 0) {
        errorMessage = 'The following files had problems:\n\n' + errorMessage + '\nPlease try again.';
        alert(errorMessage);
    }
    return doUpload;
}

function checkExtension(filePath) {
    var valid = false;
    var ext = '';
    var lastDotIndex = filePath.lastIndexOf('.');
    if (lastDotIndex > 0) {
        ext = filePath.substring(lastDotIndex+1, filePath.length);
        if (ext.length > 0) {
        	valid = true;
        }
    }
    return [valid, ext];
}

//checks extension against type; note type can be an array of valid types
function checkFileType(ext, type) {
	var valid = false;
	for (var i = 0; i < type.length; i++) {
	    if (!valid) {
	        if (ext.toLowerCase() == type[i].toLowerCase()) {
	            valid = true;
	        }
	    }
	}
	return valid;
}