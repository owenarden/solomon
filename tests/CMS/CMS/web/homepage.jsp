<%@ page language="java" import="edu.cornell.csuglab.cms.www.*" %>
<%-- CMS welcome/sign-in page, with logos and not much else --%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <link href="/web/cuwebbanner/styles/screen.css" rel="stylesheet" type="text/css">
  <link href="/web/style.css" rel="stylesheet" type="text/css">
  <link href="/web/navbar_style.css" rel="stylesheet" type="text/css">
  <title>
      Cornell University Computer Science Course Management System
  </title>
</head>
<body>

<jsp:include page="cuwebbanner/banner.jsp" />

<div id="navbar_course">
    <span class="navlink_course" id="navlink_course_overview">
        *** CUCS CMS Version 3.3 ***
    </span>
</div>
<div id="course_wrapper">
	<table id="course_wrapper_table" summary="course wrapper" cellpadding="0" cellspacing="0" border="0" width="100%">
	  <tr>
	  	<td>
		<div id="welcome_cms">
			<center>
			Welcome to Course Management System 
			<br>developed by the Department of Computer Science at Cornell University
			<br><br><br>
			Select a login method: <br><br>
			<small><a href="https://<%=request.getServerName()%>/web/auth/?<%=AccessController.P_ACTION%>=<%=AccessController.ACT_OVERLOGIN %>">Sign in</a> 
			using your Cornell NetID and password.<br><br>
			<a href="/web/guest/?<%=AccessController.P_ACTION%>=<%=AccessController.ACT_GUESTLOGIN %>">Visit</a> this site as a guest.<br><br>
			<a href="/web/guest/?<%=AccessController.P_ACTION%>=<%=AccessController.ACT_EXTERNALLOGIN %>">External Login</a> for non-Cornell users.
			</small>
			</center>
		</div>
	    </td>
	  </tr>
	</table>

<center>
<table cellpadding="0" cellspacing="0" border="0" width="200">
<tr>
<td align="center">
<a href="http://www.cs.cornell.edu/Projects/CMS/">About Us</a>  <font color="#b3b3aa">&#8226;</font> 
<a href="http://www.cs.cornell.edu/Projects/CMS/help.html">Help</a>  <font color="#b3b3aa">&#8226;</font> 
<a href="http://www.cs.cornell.edu/Projects/CMS/faqs.html">FAQs</a> 
</td>
</tr>
</table>
</center>

</div>

</body>
</html>
