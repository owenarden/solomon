<%@ page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.www.xml.*, edu.cornell.csuglab.cms.author.*" %>
<% Document displayData = (Document)session.getAttribute(AccessController.A_DISPLAYDATA);
Principal p = (Principal)session.getAttribute(AccessController.A_PRINCIPAL);
Element root = (Element)displayData.getElementsByTagName(XMLBuilder.TAG_ROOT).item(0); 
Element semRoot = (Element)root.getElementsByTagName(XMLBuilder.TAG_SEMESTERS).item(0); 
Element curSem = (Element)semRoot.getElementsByTagName(XMLBuilder.TAG_CURSEMESTER).item(0);
NodeList sems = XMLUtil.getChildrenByTagNameAndAttributeValue(semRoot, XMLBuilder.TAG_SEMESTER, XMLBuilder.A_HIDDEN, "false");
%>
  <h2>
    <a name="n_courses"></a>
    Add/Edit Courses
    <span id="courseshead">
      <a class="hide" href="#" onClick="hide('courses', '(hide)', '(show)'); return false;">(hide)</a>
  	</span>
  </h2>
  <div id="courses" class="showhide">
    <table class="assignment_table" cellpadding="2" cellspacing="0" border="0" width="100%">
    	<tr>
    		<th align="left">Code</th>
    		<th align="left">Name</th>
    		<th align="left">Enrollment</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
     	</tr>
<% Element crs = (Element)root.getElementsByTagName(XMLBuilder.TAG_ALLCOURSES).item(0);
NodeList courseList = crs.getChildNodes();
for(int i = 0; i < courseList.getLength(); i++)
{ 
	Element item = (Element)courseList.item(i); %>
      <tr>
        <td align="left"><%= item.getAttribute(XMLBuilder.A_CODE) %></td>
        <td align="left"><%= item.getAttribute(XMLBuilder.A_COURSENAME) %></td>
        <td align="left"><%= item.getAttribute(XMLBuilder.A_ENROLLMENT) %></td>
        <td align="center"><a href="?<%= AccessController.P_ACTION %>=<%= AccessController.ACT_CMSADMINCOURSEPROPS %>&amp;<%= AccessController.P_COURSEID %>=<%= item.getAttribute(XMLBuilder.A_COURSEID) %>">Course Admin</a></td>
        <td align="center"><a href="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_FINALGRADES + "&amp;" + AccessController.P_COURSEID + "=" + item.getAttribute(XMLBuilder.A_COURSEID) %>">Upload Classlist</a></td>
  	  </tr><%
} %>
    </table>
  	<p><form name="addcourse" action="?<%= AccessController.P_ACTION %>=<%= AccessController.ACT_ADDCOURSE %>" method="post">
  		<b>Create Course:</b>
	  	<div class="admin_formbox" style="width: 70%">
		   	<table border="0">
		  		<tr>
			 			<td align="right" nowrap>Code (e.g. COM S 211):</td>
		   			<td align="left"><input type="text" name="<%= AccessController.P_CODE %>" size="15" maxlength="15"></td>
		      </tr>
		    	<tr>
		      	<td align="right" nowrap>Name (e.g. Intro to Java):</td>
		      	<td align="left"><input type="text" name="<%= AccessController.P_COURSENAME %>" size="60" maxlength="300"></td>
		      </tr>
		   		<tr>
		  			<td></td>
		  			<td align="left"><input type="submit" value="Create"></td>
		  		</tr>
		   	</table>
		  </div>
    </form>
    
    
     <p>
  		<b>Batch Course Creation:</b>
	  	<div id="upload_general_div">
	    	<br>
	    	<form name="batchcreateform" method="post" enctype="multipart/form-data" action="?<%= AccessController.P_ACTION %>=<%= AccessController.ACT_BATCHCREATIONSTART %>" method="post">
			<%
			String curSemName = curSem.getAttribute(XMLBuilder.A_NAME);
			String curSemID = curSem.getAttribute(XMLBuilder.A_ID);
			String curSemSeason = curSemName.split(" ")[0];
			int curSemYear = Integer.parseInt(curSemName.split(" ")[1]);
			String semName="", semID="", semSeason;
			int semYear;
			boolean semIsLater;
			Element semItem;
			%>
			
			<select name="<%=AccessController.P_BATCHCREATESEMESTER%>" id="<%=AccessController.P_BATCHCREATESEMESTER%>">
				<option value="<%=curSemID%>"><%=curSemName%></option>
				<%
				for(int semIndex=0; semIndex<sems.getLength(); semIndex++)
				{
					semIsLater = false;
					semItem = (Element) sems.item(semIndex);
					semID = semItem.getAttribute(XMLBuilder.A_ID);
					semName = semItem.getAttribute(XMLBuilder.A_NAME);
					semSeason = semName.split(" ")[0];
					semYear = Integer.parseInt(semName.split(" ")[1]);
					if(semYear > curSemYear || curSemID.equals(semID))
					{
						semIsLater = true;
					}
					else if(semYear == curSemYear)
					{
						if(curSemSeason.equals("Spring") && (semSeason.equals("Summer") || semSeason.equals("Fall")))
						{
							semIsLater = true;
						}
						else if(curSemSeason.equals("Summer") && semSeason.equals("Fall"))
						{
							semIsLater = true;
						}
					}
					if(semIsLater)
					{
					%>
					<option value="<%=semID%>"><%=semName%></option>
					<%
					}
				}
				%>
			</select>
	  
 			<input type="file" size="30" name="<%= AccessController.P_BATCHCREATECSV %>">
		        &nbsp;<input type="submit" value="Upload">
	      	</form>
		    <br>
		    <a href="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_BATCHCOURSETEMPLATE  %>">Download</a> 
		a template, only xlstatus is a removable column.
  </div>	</p>
  </div>