<%@ page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.www.xml.*, edu.cornell.csuglab.cms.author.*" %>
<% 
Document displayData = (Document)session.getAttribute(AccessController.A_DISPLAYDATA);
Principal p = (Principal)session.getAttribute(AccessController.A_PRINCIPAL);
Element root = (Element)displayData.getElementsByTagName(XMLBuilder.TAG_ROOT).item(0);
String domainID = (String)session.getAttribute(AccessController.P_DOMAINID);
String domainName = "";

Element domRoot = (Element)root.getElementsByTagName(XMLBuilder.TAG_DOMAINS).item(0);
NodeList doms = domRoot.getElementsByTagName(XMLBuilder.TAG_DOMAIN);
for(int i = 0; i < doms.getLength(); i++)
{
	Element item = (Element)doms.item(i);
	if(item.getAttribute(XMLBuilder.A_ID).equals(domainID))	
	{
		domainName = item.getAttribute(XMLBuilder.A_NAME);
		break;
	}
}
%>

<jsp:include page="../header.jsp" />
<script src="CalendarPopup.js" type="text/javascript"></script>
<style type="text/css">
  .course_code_edit {float:right; text-align: right; margin:1em; padding-left: 1em; white-space: nowrap; width: 12em}
</style>
<link href="calstyle.css" rel="stylesheet" type="text/css">
<jsp:include page="header-cmsadmin.jsp" />

<div id="course_wrapper_withnav">
<table id="course_wrapper_table" cellpadding="0" cellspacing="0" border="0" width="90%">
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
  <td valign="top" id="course_page_container">
    <div id="course_page">
		<jsp:include page="../problem-report.jsp"/>
			
			<h2>
			    <a name="n_userlist"></a>
			    List of Users for: 
			    <%if(p.isCMSAdmin())
				{%>
				<select id="domainSelector" onchange="javascript:window.location='?<%= AccessController.P_ACTION %>=<%= AccessController.ACT_CMSUSERS %>&<%=AccessController.P_DOMAINID%>='+this.value;">
				<% domRoot = (Element)root.getElementsByTagName(XMLBuilder.TAG_DOMAINS).item(0);
					doms = domRoot.getElementsByTagName(XMLBuilder.TAG_DOMAIN);
					for(int i = 0; i < doms.getLength(); i++)
					{
						Element item = (Element)doms.item(i);
						if (!item.getAttribute(XMLBuilder.A_ID).equals("1"))
						{%>
						<option value="<%= item.getAttribute(XMLBuilder.A_ID)%>" <%if(domainID.equals(item.getAttribute(XMLBuilder.A_ID))){%>SELECTED<%}%>><%= item.getAttribute(XMLBuilder.A_NAME) %></input>
						<%}
					}
				%>
				</select><br>
				<%
				}
				else
				{%>
					<%= domainName %> <br>
				<%
				}%>
			    <span id="userlisthead">
			      <a class="hide" href="#" rel="userlist" onClick="hide('userlist', '(hide)', '(show)'); return false;">(hide)</a>
			    </span>
			  </h2>
			  <div id="userlist" class="showhide">
			    <table class="assignment_table" cellpadding="2" cellspacing="0" border="0" width="100%">
			      <tr>
			        <th align="left">NetID</th>
			        <th align="left">Name</th>
			        <th align="left">Email</th>
			        <th align="left">IsDomainSubAdmin</th>
			        <th align="left">PWExpired</th>
			        <th align="left">Deactivated</th>
			      </tr>
			<% Element usersRoot = (Element)root.getElementsByTagName(XMLBuilder.TAG_USERS).item(0);
				NodeList users = usersRoot.getElementsByTagName(XMLBuilder.TAG_USER);
				for(int i = 0; i < users.getLength(); i++)
				{
					Element item = (Element)users.item(i);
					%>
				      <tr>
				        <td align="left"><%= item.getAttribute(XMLBuilder.A_NETID) %></td>
				        <td align="left"><%= item.getAttribute(XMLBuilder.A_FIRSTNAME) + " " + item.getAttribute(XMLBuilder.A_LASTNAME) %></td>
				        <td align="left"><%= item.getAttribute(XMLBuilder.A_EMAILADDRESS) %></td>
				        <td align="left"><%= item.getAttribute(XMLBuilder.A_ISDOMAINSUBADMIN) %></td>				        
				        <td align="left"><%= item.getAttribute(XMLBuilder.A_PWEXPIRED) %></td>
				        <td align="left"><%= item.getAttribute(XMLBuilder.A_DEACTIVATED) %></td>
				      </tr>
					<%
				}
				%>
				</table>
			</div><br><br>
			<p>
				<b>Batch User Update:</b><br>
				Domain: <%= domainName %>
				<form name="batchcreateform" method="post" enctype="multipart/form-data" action="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_BATCHUPDATEUSERS%>" method="post">
					<input type="hidden" id="<%=AccessController.P_DOMAINID%>" name="<%=AccessController.P_DOMAINID%>" value="<%=domainID%>">
					<input type="file" size="30" name="<%= AccessController.P_BATCHUSERADDCSVFILE %>">&nbsp;<input type="submit" value="Upload"><br>
				</form>
				<a href="?<%= AccessController.P_ACTION + "=" + AccessController.ACT_BATCHUSERSTEMPLATE %>">Download</a> a template.
			</p>
    </div>
  </td>
  <td id="course_menu_container"><div id="course_menu_top">&nbsp;</div></td>
</tr>
<jsp:include page="../footer.jsp"/>