<%@ page language="java" import="org.w3c.dom.*, edu.cornell.csuglab.cms.www.*, edu.cornell.csuglab.cms.www.xml.*, java.util.*" %><%
Document displayData= (Document) session.getAttribute(AccessController.A_DISPLAYDATA); 
Element root= (Element) displayData.getElementsByTagName(XMLBuilder.TAG_ROOT).item(0);
NodeList rows = XMLUtil.getChildrenByTagName(root, XMLBuilder.TAG_ROW);

%><jsp:include page="../header.jsp"/>
<style type="text/css">
  div#div_left {float: left}
  span#span_right {float: right}
  .note {color: red}
  .showhide {font-weight: normal}
  .confirmtable {border-width: thin; width: 100%}
</style>
<script language="javascript">
	function setEditable(element, boolEditable) {	
		if(boolEditable == true) {
			element.readOnly = false;
			element.style.backgroundColor = "white";
		} else {
			element.readOnly = true;
			element.style.backgroundColor = "#EFEFE5";
		}
	}
	
	function toggleRowEditable(element, row, numcols) 
	{	
		if(element.innerHTML=="Edit") 
		{
			enableRowEditable(row, numcols);
		} 
		else 
		{
			disableRowEditable(row, numcols);
		}
	}
	
	function enableRowEditable(row, numcols)
	{
		element = document.getElementById('editButton_'+row);
		element.innerHTML="Done";
			
		for(i=0;i<numcols-1;i++) 
		{
			inputele = document.getElementById('csvdata['+row+']['+i+']');
			if(inputele != null) 
			{
				inputele.readOnly = false;
				inputele.style.backgroundColor = "white";
				inputele.style.border="1px solid gray";
			}
		}
	}
	
	function disableRowEditable(row, numcols)
	{
		element = document.getElementById('editButton_'+row);
		element.innerHTML="Edit";
			
		for(i=0;i<numcols-1;i++) 
		{
			inputele = document.getElementById('csvdata['+row+']['+i+']');
			if(inputele != null) 
			{
				inputele.readOnly = true;
				inputele.style.backgroundColor = "#EFEFE5";
				inputele.style.border="1px solid #EFEFE5";
			}
		}
	}
	
	function toggleRowRevert(row, numcols) 
	{
		if (confirm("Are you sure you want to revert changes?")) 
		{
			for(i=0;i<numcols;i++) 
			{
					inputele = document.getElementById('csvdata['+row+']['+i+']');
					inputeleOrig = document.getElementById('csvdata['+row+']['+i+']Orig');
					if(inputele != null && inputeleOrig != null) {
						inputele.value = inputeleOrig.value;
					}
				}
		}
	}
	
	function fixCheckedFormatting()
	{
		for(i=0; i<document.elements.length; i++)
		{
			if(document.elements[i].type=="checkbox" && document.elements[i].id != "global_check")
			{
				document.FormName.elements[i].checked=true;
			}
		}
	}
	
	function toggleRowDisabled(element) {
			
		splitRes = element.value.split(',');
		rowidx = splitRes[0];
		numcols = splitRes[1];
		
		if(element.checked == true) 
		{
			for(i=0;i<numcols;i++) 
			{
				inputele = document.getElementById('csvdata['+rowidx+']['+i+']');
				if(inputele != null) 
				{
					inputele.disabled=false;
					if(i == 6)
						inputele.style.color="red";
					else
						inputele.style.color="black";
				}
			}
			document.getElementById('editRevertButtonsDiv_'+rowidx).style.display='';
			disableRowEditable(rowidx, numcols);			
		} 
		else 
		{
			for(i=0; i<numcols; i++) 
			{
				inputele = document.getElementById('csvdata['+rowidx+']['+i+']');
				if(inputele != null) 
				{
					inputele.disabled=true;
					inputele.style.color="gray";
				}
			}
			disableRowEditable(rowidx, numcols);
			document.getElementById('editRevertButtonsDiv_'+rowidx).style.display='none';
		}
	}
	
	function toggleCheckAll(element) {
		elements = document.getElementsByName('CheckedForCreation');
		temp_len = elements.length;
		if(element.checked==true) {
			for(j=temp_len-1; j>=0; j--) {
				elements[j].checked=true;
				toggleRowDisabled(elements[j]);	
			}				
		} else {
			for(j=temp_len-1; j>=0; j--) {
				elements[j].checked=false;
				toggleRowDisabled(elements[j]);
			}	
		}
	}
	
</script>
<jsp:include page="../header-page.jsp"/>
<div id="course_wrapper_withnav">
<form action="?<%= AccessController.P_ACTION %>=<%= AccessController.ACT_BATCHUPDATEUSERSCONFIRMED %>" method="post">
<table id="course_wrapper_table" summary="course wrapper" cellpadding="0" cellspacing="0" border="0" width="100%">
  <tr>
  	<jsp:include page="../navbar.jsp"/>
    <td valign="top" id="course_page_container">
      <div id="course_page">
      	<jsp:include page="../problem-report.jsp"/>
        <h2>Parsed Data:</h2>
		<style type="text/css">
		#course_list_table thead {background-color:lightgray;}
		#course_list_table thead td {color:black; font-weight:bold;}
		</style>
        <table cellpadding="5" cellspacing="1" border="0" rules="rows" id="course_list_table">
		<thead>
		<tr>
		<td><input onclick="toggleCheckAll(this)" type="checkbox" id="global_check" name="global_check" title="Check/Uncheck All" checked /></td>
		<%
        int i;
        Element row = (Element)rows.item(0);
        NodeList errors= row.getElementsByTagName(XMLBuilder.TAG_ERROR);
        NodeList cells= row.getElementsByTagName(XMLBuilder.TAG_ELEMENT);
        Element cell;
        String cellString;
        %>
        <td>Line</td>
		<%
        for (i=0 ; i < cells.getLength(); i++) 
        {  
          cell =(Element)cells.item(i);    
          cellString = cell.getAttribute(XMLBuilder.A_DATA);
		  
		  %><td><%=cellString%></td><%
        }
        %><td width="130">Edit</td></tr></thead><%
           
        for (i=1 ; i != rows.getLength(); i++) { 
          row = (Element)rows.item(i); 
          errors= row.getElementsByTagName(XMLBuilder.TAG_ERROR);
          cells= row.getElementsByTagName(XMLBuilder.TAG_ELEMENT);  
          cell = (Element)cells.item(6);
          int textBoxWidth = 0;
          boolean checkFlag;
          %>
          <tr valign="middle">
          <% if(cell.getAttribute(XMLBuilder.A_DATA).trim().length() == 0){
               checkFlag=true;%>
            <td>
            <input onclick="toggleRowDisabled(this);" type="checkbox" id="CheckedForCreation<%=i%>" name="CheckedForCreation" value="<%=i%>,<%=cells.getLength()%>" checked></input>
            </td>
           <%}
           else{
             checkFlag=false;%>
            <td>
            <input onclick="toggleRowDisabled(this);" type="checkbox" id="CheckedForCreation<%=i%>" name="CheckedForCreation" value="<%=i%>,<%=cells.getLength()%>" ></input>
            </td>
           <%}%>
	    <td><%=i%></td><%

          for (int j= 0; j < cells.getLength(); j++) {
            if(j == 0) {textBoxWidth = 75;}
            else if(j == 1) {textBoxWidth = 75;}
			else if(j == 2) {textBoxWidth = 75;}
			else if(j == 3) {textBoxWidth = 150;}
			else if(j == 4) {textBoxWidth = 75;}
			else if(j == 5) {textBoxWidth = 75;}
			else if(j == 6) {textBoxWidth = 400;}

            cell = (Element)cells.item(j);
		if(!checkFlag && j>=0){%>
            <td>
            	<%if(j==6){%>
            	<p style="color:red;"><%= cell.getAttribute(XMLBuilder.A_DATA) %></p>
            	<%}%>
			  <input type="text" id="csvdata[<%=i%>][<%=j%>]" name="csvdata[<%=i%>][<%=j%>]" value="<%= cell.getAttribute(XMLBuilder.A_DATA) %>" readonly=true style="color:gray;background-color:#EFEFE5; border:1px solid #EFEFE5; width:<%=textBoxWidth%>px;display:<%if(j==6){%>none<%}%>"></input>
			  <input type="hidden" id="csvdata[<%=i%>][<%=j%>]Orig" name="csvdata[<%=i%>][<%=j%>]Orig" value="<%= cell.getAttribute(XMLBuilder.A_DATA) %>"></input>
            </td><%
            }
            else if(j==6){%>
            <td>
              <p style="color:red;"><%= cell.getAttribute(XMLBuilder.A_DATA) %></p>
			  <input type="text" id="csvdata[<%=i%>][<%=j%>]" name="csvdata[<%=i%>][<%=j%>]" value="<%= cell.getAttribute(XMLBuilder.A_DATA) %>" readonly="readonly" style="color:red;background-color:#EFEFE5; border:1px solid #EFEFE5; width:<%=textBoxWidth%>px;display:none"></input>
			  <input type="hidden" id="csvdata[<%=i%>][<%=j%>]Orig" name="csvdata[<%=i%>][<%=j%>]Orig" value="<%= cell.getAttribute(XMLBuilder.A_DATA) %>"></input>
            </td><%
            }
            else if(j>=0){
            %>
            <td>				
			  <input type="text" id="csvdata[<%=i%>][<%=j%>]" name="csvdata[<%=i%>][<%=j%>]" value="<%= cell.getAttribute(XMLBuilder.A_DATA) %>" readonly="readonly" style="color:black;background-color:#EFEFE5; border:1px solid #EFEFE5; width:<%=textBoxWidth%>px;"></input>
			  <input type="hidden" id="csvdata[<%=i%>][<%=j%>]Orig" name="csvdata[<%=i%>][<%=j%>]Orig" value="<%= cell.getAttribute(XMLBuilder.A_DATA) %>"></input>
            </td><%
            }
            else{            
            %>
            <td>				
			  <input type="hidden" id="csvdata[<%=i%>][<%=j%>]" name="csvdata[<%=i%>][<%=j%>]" value="<%= cell.getAttribute(XMLBuilder.A_DATA) %>" readonly="readonly" style="color:black;background-color:#EFEFE5; border:1px solid #EFEFE5; width:<%=textBoxWidth%>px;"></input>
			  <input type="hidden" id="csvdata[<%=i%>][<%=j%>]Orig" name="csvdata[<%=i%>][<%=j%>]Orig" value="<%= cell.getAttribute(XMLBuilder.A_DATA) %>"></input>
            </td><%
            }
          }
		if(!checkFlag){%>
             <td>
		 <div id="editRevertButtonsDiv_<%=i%>" style="display:none">
			<span id="editButton_<%=i%>" onclick="toggleRowEditable(this, <%=i%>,<%=cells.getLength()%>);" style="color:blue; cursor:pointer;" onmouseover="this.style.fontWeight='bold'" onmouseout="this.style.fontWeight='normal'">Edit</span> | 
			<span id="revertButton_<%=i%>" onclick="toggleRowRevert(<%=i%>,<%=cells.getLength()%>);" style="color:blue; cursor:pointer;" onmouseover="this.style.fontWeight='bold'" onmouseout="this.style.fontWeight='normal'">Revert Changes</span>
		 </div>
	    </td><%
            }
            else{            
            %>
          <td>
		 <div id="editRevertButtonsDiv_<%=i%>">
			<span id="editButton_<%=i%>" onclick="toggleRowEditable(this, <%=i%>,<%=cells.getLength()%>);" style="color:blue; cursor:pointer;" onmouseover="this.style.fontWeight='bold'" onmouseout="this.style.fontWeight='normal'">Edit</span> | 
			<span id="revertButton_<%=i%>" onclick="toggleRowRevert(<%=i%>,<%=cells.getLength()%>);" style="color:blue; cursor:pointer;" onmouseover="this.style.fontWeight='bold'" onmouseout="this.style.fontWeight='normal'">Revert Changes</span>
		 </div>
	    </td><%
            }%>

          </tr><%
          
          if (errors.getLength() != 0) {%>
            <tr><td>&nbsp;</td>
            <td colspan="<%= cells.getLength() %>">
            <br>Errors:
            <ul><%
            for (int j= 0; j != errors.getLength(); j++) {
              Element error= (Element)errors.item(j); %>
              <li><%= error.getAttribute(XMLBuilder.A_DATA) %></li><%
            } %>
            </ul></td></tr><%
          } 
        }%>
        </table>
        <input type="hidden" name="TotalNumRows" value="<%=i%>">
        <input type="submit" value="Commit Changes">
		</div>
    </td>
    <td id="course_menu_container"><div id="course_menu_top">&nbsp;</div></td>
  </tr>
  </table>
  </form>
<jsp:include page="../footer.jsp"/>
