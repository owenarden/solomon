import edu.cornell.csuglab.cms.base.RootLocal;
import edu.cornell.csuglab.cms.base.RootLocalHome;
import edu.cornell.csuglab.cms.base.RootUtil;
import edu.cornell.csuglab.cms.base.UserLocal;
import edu.cornell.csuglab.cms.base.UserLocalHome;
import edu.cornell.csuglab.cms.www.AccessController;
import javax.servlet.http.*;
public class Main {
    public static void main(String[] args) throws Exception {
        AccessController ac = new AccessController();
		HttpServletRequest request = new HttpServletRequestWrapper(null);
		HttpServletResponse response = new HttpServletResponseWrapper(null);
        ac.init(ac);
		ac.processRequest(request, response);
        //RootLocalHome home = RootUtil.getLocalHome();
        //RootLocal root = home.create();
        //UserLocalHome userHome = root.userHome();
        //UserLocal user = userHome.findByUserID("foo");
        //String name = user.getUserID();
    }
}
