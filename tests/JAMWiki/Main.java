import org.jamwiki.servlets.*;

public class Main {
    
    public static void main(String[] args) {
        LoginServlet ls = new LoginServlet();
        AdminServlet a5 = new AdminServlet();
        AdminVirtualWikiServlet a6 = new AdminVirtualWikiServlet();
        BlockListServlet a7 = new BlockListServlet();
        BlockServlet a8 = new BlockServlet();
        CategoryServlet a9 = new CategoryServlet();
        ContributionsServlet a10 = new ContributionsServlet();
        DiffServlet a11 = new DiffServlet();
        EditServlet a12 = new EditServlet();
        ExportServlet a13 = new ExportServlet();
        HistoryServlet a14 = new HistoryServlet();
        ImageServlet a15 = new ImageServlet();
        ImportServlet a16 = new ImportServlet();
        ImportTiddlyWikiServlet a17 = new ImportTiddlyWikiServlet();
        ItemsServlet a18 = new ItemsServlet();
        JAMWikiFilter a20 = new JAMWikiFilter();
        JAMWikiListener a21 = new JAMWikiListener();
        JAMWikiLocaleInterceptor a22 = new JAMWikiLocaleInterceptor();
        LinkToServlet a24 = new LinkToServlet();
        LogServlet a25 = new LogServlet();
        LoginServlet a26 = new LoginServlet();
        ManageServlet a27 = new ManageServlet();
        MoveServlet a28 = new MoveServlet();
        PrintableServlet a29 = new PrintableServlet();
        RecentChangesFeedServlet a30 = new RecentChangesFeedServlet();
        RecentChangesServlet a31 = new RecentChangesServlet();
        RegisterServlet a32 = new RegisterServlet();
        RolesServlet a33 = new RolesServlet();
        SearchServlet a34 = new SearchServlet();
        SetupServlet a36 = new SetupServlet();
        SpecialPagesServlet a37 = new SpecialPagesServlet();
        StylesheetServlet a38 = new StylesheetServlet();
        TopicServlet a39 = new TopicServlet();
        TranslationServlet a40 = new TranslationServlet();
        UpgradeServlet a41 = new UpgradeServlet();
        UploadServlet a42 = new UploadServlet();
        ViewSourceServlet a43 = new ViewSourceServlet();
        WatchlistServlet a44 = new WatchlistServlet();
    }
}
