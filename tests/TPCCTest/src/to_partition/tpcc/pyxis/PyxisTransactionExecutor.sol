package tpcc.pyxis;

import java.sql.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.io.IOException;

public class PyxisTransactionExecutor implements tpcc.client.TransactionEngine
{
		private Connection conn;
	
	
	// Stock Level Txn
		private PreparedStatement stockGetDistOrderId;
		private PreparedStatement stockGetCountStock;
	
	// Order Status Txn
		private PreparedStatement ordStatGetNewestOrd;
		private PreparedStatement ordStatGetOrderLines;
	
	 PreparedStatement customerByName;
	

	// Payment Txn
		private PreparedStatement payUpdateWhse;
		private PreparedStatement payGetWhse;
		private PreparedStatement payUpdateDist;
		private PreparedStatement payGetDist;
		private PreparedStatement payGetCust;
		private PreparedStatement payGetCustCdata;
		private PreparedStatement payUpdateCustBalCdata;
		private PreparedStatement payUpdateCustBal;
		private PreparedStatement payInsertHist;


	// Delivery Txn
		private PreparedStatement delivGetOrderId;
		private PreparedStatement delivDeleteNewOrder;
		private PreparedStatement delivGetCustId;
		private PreparedStatement delivUpdateCarrierId;
		private PreparedStatement delivUpdateDeliveryDate;
		private PreparedStatement delivSumOrderAmount;
		private PreparedStatement delivUpdateCustBalDelivCnt;
	

	// NewOrder Txn
		private PreparedStatement stmtGetCustWhse;
		private PreparedStatement stmtGetDist;
		private PreparedStatement stmtInsertNewOrder;
		private PreparedStatement stmtUpdateDist;
		private PreparedStatement stmtInsertOOrder;
		private PreparedStatement stmtGetItem;
		private PreparedStatement stmtGetStock;
		private PreparedStatement stmtUpdateStock;
		private PreparedStatement stmtInsertOrderLine;	
	
	// for formatting time string
	 private SimpleDateFormat dateFormat; 
	
    public PyxisTransactionExecutor(String dbServer) throws java.lang.Exception {
        
         super();
        StringBuilder sb;
         sb = new StringBuilder();
         sb.append("jdbc:mysql://");
         sb.append(dbServer);
         sb.append(":3306/tpcc");
        String s;
         s = sb.toString();
         //System.out.println("db is at: ");
         //System.out.println(s);
         this.conn = DriverManager.getConnection(s, "root", "");
        
         conn.setAutoCommit(false);
        
         this.stockGetDistOrderId = null;
         this.stockGetCountStock = null;
        
         this.ordStatGetNewestOrd = null;
         this.ordStatGetOrderLines = null;
         this.customerByName = null;
        
         this.payUpdateWhse = null;
         this.payGetWhse = null;
         this.payUpdateDist = null;
         this.payGetDist = null;
         this.payGetCust = null;
         this.payGetCustCdata = null;
         this.payUpdateCustBalCdata = null;
         this.payUpdateCustBal = null;
         this.payInsertHist = null;
        
         this.delivGetOrderId = null;
         this.delivDeleteNewOrder = null;
         this.delivGetCustId = null;
         this.delivUpdateCarrierId = null;
         this.delivUpdateDeliveryDate = null;
         this.delivSumOrderAmount = null;
         this.delivUpdateCustBalDelivCnt = null;
        
         this.stmtGetCustWhse = null;
         this.stmtGetDist = null;
         this.stmtInsertNewOrder = null;
         this.stmtUpdateDist = null;
         this.stmtInsertOOrder = null;
         this.stmtGetItem = null;
         this.stmtGetStock = null;
         this.stmtUpdateStock = null;
         this.stmtInsertOrderLine = null;        
        
         this.dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
         ;   
        
    }
			
	
	public int testTransaction () throws SQLException
	{
		Statement stat;
	    ResultSet rs;
	        	
    	 stat = conn.createStatement();
		 rs = stat.executeQuery("select count(*) as cnt from warehouse");
				
		 rs.next();
		int result;
		 result = rs.getInt("cnt");
		
		 return result;
	}
	
	
	public String stockLevelTransaction (int w_id, int d_id, int threshold) throws SQLException
	{
		 ;
		StringBuilder terminalMessage;
		 terminalMessage = new StringBuilder();
		
		//try
		{
			int o_id;
			 o_id = 0;
			int stock_count;
			 stock_count = 0;
			
			if ( stockGetDistOrderId == null) 
			{
					//System.out.println("creating prepared statement");
					stockGetDistOrderId = conn.prepareStatement("SELECT d_next_o_id FROM district WHERE d_w_id = ? AND d_id = ?");
			}
					//else
					//System.out.println("not creating prepared statement");
				
				
				stockGetDistOrderId.setInt(1, w_id);
				stockGetDistOrderId.setInt(2, d_id);
			
			ResultSet rs;
			 rs = stockGetDistOrderId.executeQuery();
			
			
			boolean hasNext;
			 hasNext = rs.next();
			if ( !hasNext)			
			//	throw new RuntimeException("D_W_ID=" + w_id + " D_ID=" + d_id + " not found!");
				return "not found 1";
						
				o_id = rs.getInt("d_next_o_id");
				rs.close();
				rs = null;
			//printMessage("Next Order ID for District = " + o_id);
			
						
			if ( stockGetCountStock == null) 
			{
				String queryString;				
				 queryString = "SELECT COUNT(DISTINCT (s_i_id)) AS stock_count FROM order_line, stock WHERE ol_w_id = ? AND ol_d_id = ? AND ol_o_id < ? AND ol_o_id >= ? - 20 AND s_w_id = ? AND s_i_id = ol_i_id AND s_quantity < ?";
												
				//	stockGetCountStock = conn.prepareStatement("SELECT COUNT(DISTINCT (s_i_id)) AS stock_count"
				//		+ " FROM order_line, stock"
				//		+ " WHERE ol_w_id = ?" + " AND ol_d_id = ?"
				//		+ " AND ol_o_id < ?" + " AND ol_o_id >= ? - 20"
				//		+ " AND s_w_id = ?" + " AND s_i_id = ol_i_id"
				//		+ " AND s_quantity < ?");
				
				 //System.out.println(queryString);
				 stockGetCountStock = conn.prepareStatement(queryString);
			}
			
						
				stockGetCountStock.setInt(1, w_id);
			
				stockGetCountStock.setInt(2, d_id);
				stockGetCountStock.setInt(3, o_id);
				stockGetCountStock.setInt(4, o_id);
				stockGetCountStock.setInt(5, w_id);
				stockGetCountStock.setInt(6, threshold);			
			
				rs = stockGetCountStock.executeQuery();

			 hasNext = rs.next();
			if ( !hasNext)
			//	throw new RuntimeException("OL_W_ID=" + w_id + " OL_D_ID=" + d_id + " OL_O_ID=" + o_id + " (...) not found!");
				return "not found 2";	
																
				stock_count = rs.getInt("stock_count");

				conn.commit();

				rs.close();
				rs = null;

				terminalMessage.append("\n+-------------------------- STOCK-LEVEL --------------------------+");
				terminalMessage.append("\n Warehouse: ");
				terminalMessage.append(w_id);
				terminalMessage.append("\n District:  ");
				terminalMessage.append(d_id);
				terminalMessage.append("\n\n Stock Level Threshold: ");
				terminalMessage.append(threshold);
				terminalMessage.append("\n Low Stock Count:       ");
				terminalMessage.append(stock_count);
				terminalMessage.append("\n+-----------------------------------------------------------------+\n\n");
			//terminalMessage(terminalMessage.toString());											    		    
		}
		
		/*
		catch (SQLException e)
		{
			//terminalMessage = new StringBuilder(e.getErrorCode() + "#" + e.getSQLState() + "#" + e.getMessage());
			// throw e;
				try { conn.rollback(); } catch (SQLException e2) { throw new RuntimeException(e2); }
				terminalMessage = new StringBuilder();			
		}
		*/
	 	
	 	String r;
			r = terminalMessage.toString();
		
			return r;
	}
	

	public String orderStatusTransaction (int w_id, int d_id, int c_id, String c_last, boolean c_by_name) throws SQLException 
	{
		StringBuilder terminalMessage;
			terminalMessage = new StringBuilder();
		//try
		{
			int o_id, o_carrier_id;			
				o_id = -1;
			 o_carrier_id = -1;
			
			Timestamp entdate;
			ArrayList orderLines;
			 orderLines = new ArrayList();

			Customer c;
			if ( c_by_name) 
			{
				// assert c_id <= 0;
				// TODO: This only needs c_balance, c_first, c_middle, c_id
				// only fetch those columns?
					c = getCustomerByName(w_id, d_id, c_last);
				 ;
			} 
			else 
			{
				// assert c_last == null;
					c = getCustomerById(w_id, d_id, c_id);
				 ;
			}

			// find the newest order for the customer
			// retrieve the carrier & order date for the most recent order.

			if ( ordStatGetNewestOrd == null) 
			{
					ordStatGetNewestOrd = conn.prepareStatement("SELECT o_id, o_carrier_id, o_entry_d FROM oorder WHERE o_w_id = ? AND o_d_id = ? AND o_c_id = ? ORDER BY o_id DESC LIMIT 1");
			}
				ordStatGetNewestOrd.setInt(1, w_id);
				ordStatGetNewestOrd.setInt(2, d_id);
				ordStatGetNewestOrd.setInt(3, c.c_id);
			ResultSet rs;
			
			
				rs = ordStatGetNewestOrd.executeQuery();

			boolean hasNext;
			 hasNext = rs.next();
			if ( !hasNext) 
			{
				// throw new RuntimeException("No orders for o_w_id=" + w_id + " o_d_id=" + d_id + " o_c_id=" + c.c_id);
				 return "no orders found";
			}						

				o_id = rs.getInt("o_id");
				o_carrier_id = rs.getInt("o_carrier_id");
				entdate = rs.getTimestamp("o_entry_d");
				rs.close();
				rs = null;

			// retrieve the order lines for the most recent order

			if ( ordStatGetOrderLines == null) 
			{
					ordStatGetOrderLines = conn.prepareStatement("SELECT ol_i_id, ol_supply_w_id, ol_quantity, ol_amount, ol_delivery_d FROM order_line  WHERE ol_o_id = ? AND ol_d_id =? AND ol_w_id = ?");
			}
				ordStatGetOrderLines.setInt(1, o_id);
				ordStatGetOrderLines.setInt(2, d_id);
				ordStatGetOrderLines.setInt(3, w_id);
				rs = ordStatGetOrderLines.executeQuery();

			 hasNext = rs.next();
			while ( hasNext) 
			{
				StringBuilder orderLine;
					orderLine = new StringBuilder();
					orderLine.append("[");
					orderLine.append(rs.getLong("ol_supply_w_id"));
					orderLine.append(" - ");
					orderLine.append(rs.getLong("ol_i_id"));
					orderLine.append(" - ");
					orderLine.append(rs.getLong("ol_quantity"));
					orderLine.append(" - ");
				
				String s;
				double d;
				 d = rs.getDouble("ol_amount");
				 s = formattedDouble(d);
				 orderLine.append(s);
					orderLine.append(" - ");
				
				Object o1;
				 o1 = rs.getTimestamp("ol_delivery_d"); 
				if ( o1 != null)
				{
					Object o2;
					 o2 = rs.getTimestamp("ol_delivery_d");
						orderLine.append(o2);
				}
				else
						orderLine.append("99-99-9999");
					orderLine.append("]");
				Object o3;
				 o3 = orderLine.toString();
					orderLines.add(o3);
				
				 hasNext = rs.next();
			}
			 rs.close();
				rs = null;

			// commit the transaction
				conn.commit();


				terminalMessage.append("\n");
				terminalMessage.append("+-------------------------- ORDER-STATUS -------------------------+\n");
				terminalMessage.append(" Date: ");
			String t;
			 t = getCurrentTime();
				terminalMessage.append(t);
				terminalMessage.append("\n\n Warehouse: ");
				terminalMessage.append(w_id);
				terminalMessage.append("\n District:  ");
				terminalMessage.append(d_id);
				terminalMessage.append("\n\n Customer:  ");
				terminalMessage.append(c.c_id);
				terminalMessage.append("\n   Name:    ");
				terminalMessage.append(c.c_first);
				terminalMessage.append(" ");
				terminalMessage.append(c.c_middle);
				terminalMessage.append(" ");
				terminalMessage.append(c.c_last);
				terminalMessage.append("\n   Balance: ");
				terminalMessage.append(c.c_balance);
				terminalMessage.append("\n\n");
			if ( o_id == -1) 
			{
					terminalMessage.append(" Customer has no orders placed.\n");
			} 
			else 
			{
					terminalMessage.append(" Order-Number: ");
					terminalMessage.append(o_id);
					terminalMessage.append("\n    Entry-Date: ");
					terminalMessage.append(entdate);
					terminalMessage.append("\n    Carrier-Number: ");
					terminalMessage.append(o_carrier_id);
					terminalMessage.append("\n\n");
				
				int i1;
				 i1 = orderLines.size();
				if ( i1 != 0) 
				{
						terminalMessage.append(" [Supply_W - Item_ID - Qty - Amount - Delivery-Date]\n");
					
					//	for (int i = 0; i < orderLines.size(); ++i) {
					
					int i, size;
					 i = 0;
					 size = orderLines.size();
					
					while ( i < size)
					{
						Object o1;
						String orderLine;
						 o1 = orderLines.get(i);
						 orderLine = (String)o1; 
							terminalMessage.append(" ");
							terminalMessage.append(orderLine);
							terminalMessage.append("\n");
						 i = ++i;
					}
				} 
				else 
				{
					//terminalMessage(" This Order has no Order-Lines.\n");
					 terminalMessage.append(" This Order has no Order-Lines.\n");
				}
			}
				terminalMessage.append("+-----------------------------------------------------------------+\n\n");
		}
		/*
		catch (SQLException e)
		{
			//terminalMessage = new StringBuilder(e.getErrorCode() + "#" + e.getSQLState() + "#" + e.getMessage());
			//			throw e;
						try { conn.rollback(); } catch (SQLException e2) { throw new RuntimeException(e2); }
						terminalMessage = new StringBuilder();
		}
		//terminalMessage(terminalMessage.toString());
		*/
		
		String r;
		 r = terminalMessage.toString();
			return r;
	}
	
	
	public String paymentTransaction (int w_id, int c_w_id, float h_amount, int d_id, int c_d_id, int c_id, 
	  							      String c_last, boolean c_by_name) throws SQLException
	{
		StringBuilder terminalMessage;
			terminalMessage = new StringBuilder();
		
		//try
		{
			String w_street_1, w_street_2, w_city, w_state, w_zip, w_name;
			String d_street_1, d_street_2, d_city, d_state, d_zip, d_name;

			if ( payUpdateWhse == null) 
			{
				 payUpdateWhse = conn.prepareStatement("UPDATE warehouse SET w_ytd = w_ytd + ?  WHERE w_id = ? ");
		    }
			 payUpdateWhse.setFloat(1, h_amount);
			 payUpdateWhse.setInt(2, w_id);
		    // MySQL reports deadlocks due to lock upgrades:
		    // t1: read w_id = x; t2: update w_id = x; t1 update w_id = x
			
			int result;
			 result = payUpdateWhse.executeUpdate();
			if ( result == 0)
				// throw new RuntimeException("W_ID=" + w_id + " not found!");
				 return "not found 1";

		    if ( payGetWhse == null) 
			{
				 payGetWhse = conn.prepareStatement("SELECT w_street_1, w_street_2, w_city, w_state, w_zip, w_name FROM warehouse WHERE w_id = ?");
		    }
			 payGetWhse.setInt(1, w_id);
			
			ResultSet rs;
			 rs = payGetWhse.executeQuery();
			
			boolean b1;
			 b1 = !rs.next();
			if ( b1)
				//  throw new RuntimeException("W_ID=" + w_id + " not found!");
				 return "not found 2";
				
			 w_street_1 = rs.getString("w_street_1");
			 w_street_2 = rs.getString("w_street_2");
			 w_city = rs.getString("w_city");
			 w_state = rs.getString("w_state");
			 w_zip = rs.getString("w_zip");
			 w_name = rs.getString("w_name");
				rs.close();
				rs = null;

			if ( payUpdateDist == null) 
			{
				 payUpdateDist = conn.prepareStatement("UPDATE district SET d_ytd = d_ytd + ? WHERE d_w_id = ? AND d_id = ?");
		    }
			 payUpdateDist.setFloat(1, h_amount);
			 payUpdateDist.setInt(2, w_id);
			 payUpdateDist.setInt(3, d_id);
			 result = payUpdateDist.executeUpdate();
			if ( result == 0)
				// throw new RuntimeException("D_ID=" + d_id + " D_W_ID=" + w_id + " not found!");
				 return "not found 3";

			if ( payGetDist == null) 
			{
				 payGetDist = conn.prepareStatement("SELECT d_street_1, d_street_2, d_city, d_state, d_zip, d_name FROM district WHERE d_w_id = ? AND d_id = ?");
		    }
			 payGetDist.setInt(1, w_id);
			 payGetDist.setInt(2, d_id);
			 rs = payGetDist.executeQuery();
			
			boolean b2;
			 b2 = !rs.next();
			if ( b2)
				//  throw new RuntimeException("D_ID=" + d_id + " D_W_ID=" + w_id + " not found!");
				 return "not found 4";
				
			 d_street_1 = rs.getString("d_street_1");
			 d_street_2 = rs.getString("d_street_2");
			 d_city = rs.getString("d_city");
			 d_state = rs.getString("d_state");
			 d_zip = rs.getString("d_zip");
			 d_name = rs.getString("d_name");
			 rs.close();
			 rs = null;

			Customer c;
			if ( c_by_name) 
			{
				//	assert c_id <= 0;
				 c = getCustomerByName(c_w_id, c_d_id, c_last);
				 ;
		    } 
		    else 
		    {
				//	assert c_last == null; 
				 c = getCustomerById(c_w_id, c_d_id, c_id);
				 ;
		    }

			 c.c_balance -= h_amount;
				c.c_ytd_payment += h_amount;
				c.c_payment_cnt += 1;
			
			String c_data;
			 c_data = null;
			
			boolean b3;
			String s3;
			 s3 = c.c_credit;
			 b3 = s3.equals("BC");
			if ( b3) 
			{ // bad credit

				if ( payGetCustCdata == null) 
				{
					 payGetCustCdata = conn.prepareStatement("SELECT c_data FROM customer WHERE c_w_id = ? AND c_d_id = ? AND c_id = ?");
		    	}
				 payGetCustCdata.setInt(1, c_w_id);
				 payGetCustCdata.setInt(2, c_d_id);
				 payGetCustCdata.setInt(3, c.c_id);
				 rs = payGetCustCdata.executeQuery();
			
				boolean b4;
				 b4 = !rs.next();
				if ( b4) 
					//   throw new RuntimeException("C_ID=" + c.c_id + " C_W_ID=" + c_w_id + " C_D_ID=" + c_d_id + " not found!");
					 return "not found 5";
			
				 c_data = rs.getString("c_data");
				 rs.close();
				 rs = null;

				StringBuilder sb;
				 sb = new StringBuilder();
				 sb.append(c.c_id);
				 sb.append(" ");
				 sb.append(c_d_id);
				 sb.append(" ");
				 sb.append(c_w_id);
				 sb.append(" ");
				 sb.append(d_id);
				 sb.append(" ");
				 sb.append(w_id);
				 sb.append(" ");
				 sb.append(h_amount);
				 sb.append(" | ");
				 sb.append(c_data);
				 c_data = sb.toString();
						
				// c_data = c.c_id + " " + c_d_id + " " + c_w_id + " " + d_id + " " + w_id + " " + h_amount + " | " + c_data;

				int i1;
				 i1 = c_data.length();
				if ( i1 > 500) 
					 c_data = c_data.substring(0, 500);

	      		if ( payUpdateCustBalCdata == null) 
	      		{
					 payUpdateCustBalCdata = conn.prepareStatement("UPDATE customer SET c_balance = ?, c_ytd_payment = ?, c_payment_cnt = ?, c_data = ? WHERE c_w_id = ? AND c_d_id = ? AND c_id = ?");
		   	 	}
				 payUpdateCustBalCdata.setFloat(1, c.c_balance);
				 payUpdateCustBalCdata.setFloat(2, c.c_ytd_payment);
				 payUpdateCustBalCdata.setInt(3, c.c_payment_cnt);
					payUpdateCustBalCdata.setString(4, c_data);
					payUpdateCustBalCdata.setInt(5, c_w_id);
				 payUpdateCustBalCdata.setInt(6, c_d_id);
				 payUpdateCustBalCdata.setInt(7, c.c_id);
				 result = payUpdateCustBalCdata.executeUpdate();

		    	if ( result == 0)
					// throw new RuntimeException("Error in PYMNT Txn updating Customer C_ID=" + c.c_id + " C_W_ID=" + c_w_id + " C_D_ID=" + c_d_id);
					 return "error in PYMNT Txn";
		    } 
		    else 
		    { // GoodCredit

				if ( payUpdateCustBal == null) 
				{
						payUpdateCustBal = conn.prepareStatement("UPDATE customer SET c_balance = ?, c_ytd_payment = ?, c_payment_cnt = ? WHERE c_w_id = ? AND c_d_id = ? AND c_id = ?");
		      	}
				 payUpdateCustBal.setFloat(1, c.c_balance);
				 payUpdateCustBal.setFloat(2, c.c_ytd_payment);
					payUpdateCustBal.setFloat(3, c.c_payment_cnt);
				 payUpdateCustBal.setInt(4, c_w_id);
				 payUpdateCustBal.setInt(5, c_d_id);
				 payUpdateCustBal.setInt(6, c.c_id);
				 result = payUpdateCustBal.executeUpdate();

	      		if ( result == 0)
		    		// throw new RuntimeException("C_ID=" + c.c_id + " C_W_ID=" + c_w_id + " C_D_ID=" + c_d_id + " not found!");
					 return "not found 6";
		    }
			
			int i2;
			 i2 = w_name.length();
		    if ( i2 > 10)
				 w_name = w_name.substring(0, 10);
			int i3;
			 i3 = d_name.length();
			if ( i3 > 10)
				 d_name = d_name.substring(0, 10);
			
			StringBuilder sb2;
			 sb2 = new StringBuilder();
			 sb2.append(w_name);
			 sb2.append("    ");
			 sb2.append(d_name);
			
			String h_data;
			 h_data = sb2.toString();

			if ( payInsertHist == null) 
			{
				 payInsertHist = conn.prepareStatement("INSERT INTO history (h_c_d_id, h_c_w_id, h_c_id, h_d_id, h_w_id, h_date, h_amount, h_data) VALUES (?,?,?,?,?,?,?,?)");
		    }
			 payInsertHist.setInt(1, c_d_id);
			 payInsertHist.setInt(2, c_w_id);
			 payInsertHist.setInt(3, c.c_id);
			 payInsertHist.setInt(4, d_id);
			 payInsertHist.setInt(5, w_id);
			
			Timestamp ts;
			 ts = new Timestamp(System.currentTimeMillis());
				payInsertHist.setTimestamp(6, ts);
				payInsertHist.setFloat(7, h_amount);
			 payInsertHist.setString(8, h_data);
			 payInsertHist.executeUpdate();

			 conn.commit();
		    
			 terminalMessage.append("\n+---------------------------- PAYMENT ----------------------------+");
			
			String t;
			 t = getCurrentTime();			
				terminalMessage.append("\n Date: ");
			 terminalMessage.append(t);
				terminalMessage.append("\n\n Warehouse: ");
				terminalMessage.append(w_id);
				terminalMessage.append("\n   Street:  ");
				terminalMessage.append(w_street_1);
				terminalMessage.append("\n   Street:  ");
				terminalMessage.append(w_street_2);
				terminalMessage.append("\n   City:    ");
				terminalMessage.append(w_city);
				terminalMessage.append("   State: ");
				terminalMessage.append(w_state);
				terminalMessage.append("  Zip: ");
				terminalMessage.append(w_zip);
				terminalMessage.append("\n\n District:  ");
				terminalMessage.append(d_id);
				terminalMessage.append("\n   Street:  ");
				terminalMessage.append(d_street_1);
				terminalMessage.append("\n   Street:  ");
				terminalMessage.append(d_street_2);
				terminalMessage.append("\n   City:    ");
				terminalMessage.append(d_city);
				terminalMessage.append("   State: ");
				terminalMessage.append(d_state);
				terminalMessage.append("  Zip: ");
				terminalMessage.append(d_zip);
				terminalMessage.append("\n\n Customer:  ");
				terminalMessage.append(c.c_id);
				terminalMessage.append("\n   Name:    ");
				terminalMessage.append(c.c_first);
				terminalMessage.append(" ");
				terminalMessage.append(c.c_middle);
				terminalMessage.append(" ");
				terminalMessage.append(c.c_last);
				terminalMessage.append("\n   Street:  ");
				terminalMessage.append(c.c_street_1);
				terminalMessage.append("\n   Street:  ");
				terminalMessage.append(c.c_street_2);
				terminalMessage.append("\n   City:    ");
				terminalMessage.append(c.c_city);
				terminalMessage.append("   State: ");
				terminalMessage.append(c.c_state);
				terminalMessage.append("  Zip: ");
				terminalMessage.append(c.c_zip);
				terminalMessage.append("\n   Since:   ");
			
			if ( c.c_since != null) 
			{
				Timestamp t1;
				 t1 = c.c_since;
				String s1;
				 s1 = c.c_since.toString();
				 terminalMessage.append(s1);
			} 
			else 
			{
				 terminalMessage.append("");
			}
			 terminalMessage.append("\n   Credit:  ");
			 terminalMessage.append(c.c_credit);
			 terminalMessage.append("\n   %Disc:   ");
			 terminalMessage.append(c.c_discount);
			 terminalMessage.append("\n   Phone:   ");
			 terminalMessage.append(c.c_phone);
			 terminalMessage.append("\n\n Amount Paid:      ");
			 terminalMessage.append(h_amount);
			 terminalMessage.append("\n Credit Limit:     ");
			 terminalMessage.append(c.c_credit_lim);
			 terminalMessage.append("\n New Cust-Balance: ");
			 terminalMessage.append(c.c_balance);

			boolean b5;
			String s2;
			 s2 = c.c_credit;
			 b5 = s2.equals("BC");
			if ( b5) 
			{
				int i4;
				 i4 = c_data.length();
				if ( i4 > 50) 
				{
					 terminalMessage.append("\n\n Cust-Data: ");
					String s1;
					 s1 = c_data.substring(0, 50);
					 terminalMessage.append(s1);				
					
					int data_chunks;
					boolean b6;
					 b6 = c_data.length() > 200;
					if ( b6)
						 data_chunks = 4;
					else
					{
						int i5;
						 i5 = c_data.length();
						 data_chunks = i5 / 50;
					}										
					// data_chunks = c_data.length() > 200 ? 4 : c_data.length() / 50;
					
					int n;
					boolean b7;
					 n = 1;
					 b7 = n < data_chunks;
					while ( b7)
					{
						// for (int n = 1; n < data_chunks; n++)
						// terminalMessage.append("\n            " + c_data.substring(n * 50, (n + 1) * 50));
						
						 terminalMessage.append("\n            ");
						String s4;
						 s4 = c_data.substring(n * 50, (n + 1) * 50);
						 terminalMessage.append(s4);
						
						 n++;
						 b7 = n < data_chunks;
					}					
		      	} 
		      	else 
		      	{
					 terminalMessage.append("\n\n Cust-Data: ");
					 terminalMessage.append(c_data);
		      	}
		    }
			 terminalMessage.append("\n+-----------------------------------------------------------------+\n\n");
		    
		    //terminalMessage(terminalMessage.toString());
		}
		/*
		catch (SQLException e)
		{
			//terminalMessage = new StringBuilder(e.getErrorCode() + "#" + e.getSQLState() + "#" + e.getMessage());
			//			throw e;
						try { conn.rollback(); } catch (SQLException e2) { throw new RuntimeException(e2); }
						terminalMessage = new StringBuilder();
		}
		*/
		
		String r;
		 r = terminalMessage.toString();
			return r;
	}

	
	public String deliveryTransaction (int w_id, int o_carrier_id) throws SQLException
	{
		StringBuilder terminalMessage;
			terminalMessage = new StringBuilder();

		//try
		{
			int d_id, c_id;
			float ol_total;
			int [] orderIDs;			

				orderIDs = new int[10];
			boolean b1;

			// for (d_id = 1; d_id <= 10; d_id++) {
			 d_id = 1;
			 b1 = d_id <= 10;
			while ( b1)
			{						
				if ( delivGetOrderId == null) 
				{
					 delivGetOrderId = conn.prepareStatement("SELECT no_o_id FROM new_order WHERE no_d_id = ? AND no_w_id = ? ORDER BY no_o_id ASC LIMIT 1");
				}
					delivGetOrderId.setInt(1, d_id);
					delivGetOrderId.setInt(2, w_id);
				
				ResultSet rs;
				 rs = delivGetOrderId.executeQuery();
				
				boolean b2;
				 b2 = !rs.next();
				if ( b2) 
				{
					// This district has no new orders; this can happen but should be rare
						continue;
				}

				int no_o_id;
				 no_o_id = rs.getInt("no_o_id");
					orderIDs[d_id - 1] = no_o_id;
					rs.close();
					rs = null;

				if (  delivDeleteNewOrder == null) 
				{
					 delivDeleteNewOrder = conn.prepareStatement("DELETE FROM new_order WHERE no_o_id = ? AND no_d_id = ? AND no_w_id = ?");
				}
					delivDeleteNewOrder.setInt(1, no_o_id);
					delivDeleteNewOrder.setInt(2, d_id);
					delivDeleteNewOrder.setInt(3, w_id);
				int result;
				 result = delivDeleteNewOrder.executeUpdate();
				
				if ( result != 1) 
				{
					// This code used to run in a loop in an attempt to make this work
					// with MySQL's default weird consistency level. We just always run
					// this as SERIALIZABLE instead. I don't *think* that fixing this one
					// error makes this work with MySQL's default consistency. Careful
					// auditing would be required.
					
					//					throw new RuntimeException("new order w_id=" + w_id + " d_id=" + d_id + " no_o_id=" + no_o_id + " delete failed (not running with SERIALIZABLE isolation?)");
					 return "delivery txn error 1";					
				}

				if ( delivGetCustId == null) 
				{
						delivGetCustId = conn.prepareStatement("SELECT o_c_id FROM oorder WHERE o_id = ? AND o_d_id = ? AND o_w_id = ?");
				}
					delivGetCustId.setInt(1, no_o_id);
					delivGetCustId.setInt(2, d_id);
					delivGetCustId.setInt(3, w_id);
					rs = delivGetCustId.executeQuery();

				boolean b3;
				 b3 = !rs.next();
				if ( b3)
					//	throw new RuntimeException("O_ID=" + no_o_id + " O_D_ID=" + d_id + " O_W_ID=" + w_id + " not found!");
					 return "delivery txn error 2";
				
					c_id = rs.getInt("o_c_id");
					rs.close();
					rs = null;

				if ( delivUpdateCarrierId == null) 
				{
					 delivUpdateCarrierId = conn.prepareStatement("UPDATE oorder SET o_carrier_id = ? WHERE o_id = ? AND o_d_id = ? AND o_w_id = ?");
				}
					delivUpdateCarrierId.setInt(1, o_carrier_id);
					delivUpdateCarrierId.setInt(2, no_o_id);
					delivUpdateCarrierId.setInt(3, d_id);
					delivUpdateCarrierId.setInt(4, w_id);
					result = delivUpdateCarrierId.executeUpdate();

				if ( result != 1)
					//	throw new RuntimeException("O_ID=" + no_o_id + " O_D_ID=" + d_id + " O_W_ID=" + w_id + " not found!");
					 return "delivery txn error 3";

				if ( delivUpdateDeliveryDate == null) 
				{
					 delivUpdateDeliveryDate = conn.prepareStatement("UPDATE order_line SET ol_delivery_d = ? WHERE ol_o_id = ? AND ol_d_id = ? AND ol_w_id = ?");
				}

				Timestamp t1;
				 t1 = new Timestamp(System.currentTimeMillis());
					delivUpdateDeliveryDate.setTimestamp(1, t1);
					delivUpdateDeliveryDate.setInt(2, no_o_id);
					delivUpdateDeliveryDate.setInt(3, d_id);
					delivUpdateDeliveryDate.setInt(4, w_id);
					result = delivUpdateDeliveryDate.executeUpdate();

				if ( result == 0)
					//	throw new RuntimeException("OL_O_ID=" + no_o_id + " OL_D_ID=" + d_id + " OL_W_ID=" + w_id + " not found!");
					 return "delivery txn error 4";

				if ( delivSumOrderAmount == null) 
				{
						delivSumOrderAmount = conn.prepareStatement("SELECT SUM(ol_amount) AS ol_total FROM order_line WHERE ol_o_id = ? AND ol_d_id = ? AND ol_w_id = ?");
				}
					delivSumOrderAmount.setInt(1, no_o_id);
					delivSumOrderAmount.setInt(2, d_id);
					delivSumOrderAmount.setInt(3, w_id);
					rs = delivSumOrderAmount.executeQuery();

				boolean b4;
				 b4 = !rs.next();
				if ( b4)
					//	throw new RuntimeException("OL_O_ID=" + no_o_id + " OL_D_ID=" + d_id + " OL_W_ID=" + w_id + " not found!");
					 return "delivery txn error 5";
				
					ol_total = rs.getFloat("ol_total");
					rs.close();
					rs = null;

				if ( delivUpdateCustBalDelivCnt == null) 
				{
						delivUpdateCustBalDelivCnt = conn.prepareStatement("UPDATE customer SET c_balance = c_balance + ?, c_delivery_cnt = c_delivery_cnt + 1 WHERE c_w_id = ? AND c_d_id = ? AND c_id = ?");
				}
					delivUpdateCustBalDelivCnt.setFloat(1, ol_total);
					delivUpdateCustBalDelivCnt.setInt(2, w_id);
					delivUpdateCustBalDelivCnt.setInt(3, d_id);
					delivUpdateCustBalDelivCnt.setInt(4, c_id);
					result = delivUpdateCustBalDelivCnt.executeUpdate();

				if ( result == 0)
					//	throw new RuntimeException("C_ID=" + c_id + " C_W_ID=" + w_id + " C_D_ID="+ d_id + " not found!");
					 return "delivery txn error 6";

				 d_id++;
				 b1 = d_id <= 10;
			}

			 conn.commit();

				terminalMessage.append("\n+---------------------------- DELIVERY ---------------------------+\n");
				terminalMessage.append(" Date: ");
			String t;
			 t = getCurrentTime();
				terminalMessage.append(t);
				terminalMessage.append("\n\n Warehouse: ");
				terminalMessage.append(w_id);
				terminalMessage.append("\n Carrier:   ");
				terminalMessage.append(o_carrier_id);
				terminalMessage.append("\n\n Delivered Orders\n");
			int skippedDeliveries;
			 skippedDeliveries = 0;
			
			
			//	for (int i = 1; i <= 10; i++) {
			int i2;
			boolean b5;
			 i2 = 1;
			 b5 = i2 <= 10;
			while ( b5)
			{
				boolean b6;
				 b6 = orderIDs[i2 - 1] >= 0; 
				if ( b6) 
				{
						terminalMessage.append("  District ");
					if ( i2 < 10)
							terminalMessage.append(" ");
					else
						 terminalMessage.append("");
					
						terminalMessage.append(i2);
						terminalMessage.append(": Order number ");
						terminalMessage.append(orderIDs[i2 - 1]);
						terminalMessage.append(" was delivered.\n");
				} 
				else 
				{
						terminalMessage.append("  District ");
					if ( i2 < 10)
							terminalMessage.append(" ");
					else
						 terminalMessage.append("");
					
						terminalMessage.append(i2);
						terminalMessage.append(": No orders to be delivered.\n");
						skippedDeliveries++;
				}
				 i2++;
				 b5 = i2<= 10;
			}
				terminalMessage.append("+-----------------------------------------------------------------+");
			 terminalMessage.append(skippedDeliveries);
			 terminalMessage.append("\n\n");

			//terminalMessage(terminalMessage.toString());

			//return skippedDeliveries;
		}
		/*
		catch (SQLException e)
		{
			//terminalMessage = new StringBuilder(e.getErrorCode() + "#" + e.getSQLState() + "#" + e.getMessage());
			//			throw e;
				try { conn.rollback(); } catch (SQLException e2) { throw new RuntimeException(e2); }
			 terminalMessage = new StringBuilder();
		}
		*/

		String r;
		 r = terminalMessage.toString();
		 return r;
	}
	

 	/**
 	 * w_id: warehouse id where this order originates from
 	 * d_id: district id where this order originates from
 	 * o_ol_cnt: number of items in the order
 	 * o_all_local: whether all items ordered are only includes home order lines
 	 * itemIDS, supplierWarehouseIDs, orderQuantities: arrays of length o_ol_cnt 
 	 */
	public String newOrderTransaction(int w_id, int d_id, int c_id, int o_ol_cnt, int o_all_local,
									  int [] itemIDs, int [] supplierWarehouseIDs, int [] orderQuantities) throws SQLException
	{	
		
		StringBuilder terminalMessage;
		 terminalMessage = new StringBuilder();
		
		//try
		{
		    float c_discount, w_tax, d_tax, i_price;
		    int d_next_o_id, o_id, s_quantity;
		    String c_last, c_credit, i_name, i_data, s_data;
		    String s_dist_01, s_dist_02, s_dist_03, s_dist_04, s_dist_05;
		    String s_dist_06, s_dist_07, s_dist_08, s_dist_09, s_dist_10, ol_dist_info;
		    float [] itemPrices;
		    float [] orderLineAmounts;
		    String [] itemNames;
		    int [] stockQuantities;
			
		     d_tax = 0;
		     o_id = -1;
		     c_last = null;
		     c_credit = null;		    
		     ol_dist_info = null;
		     itemPrices = new float[o_ol_cnt];
		     orderLineAmounts = new float[o_ol_cnt];
		     itemNames = new String[o_ol_cnt];
		     stockQuantities = new int[o_ol_cnt];

		    // char {} [] brandGeneric = new char[o_ol_cnt];
		    String [] brandGeneric;		    
		     brandGeneric = new String[o_ol_cnt];
		    
		    int ol_supply_w_id, ol_i_id, ol_quantity;
		    int s_remote_cnt_increment;
		    float ol_amount, total_amount;
		     total_amount = 0;

		    //try 
		    {
		    	if ( stmtGetCustWhse == null) 
		    	{
		    		 stmtGetCustWhse = conn.prepareStatement("SELECT c_discount, c_last, c_credit, w_tax FROM customer, warehouse WHERE w_id = ? AND c_w_id = ? AND c_d_id = ? AND c_id = ?");
		    	}
		    	 stmtGetCustWhse.setInt(1, w_id);
		    	 stmtGetCustWhse.setInt(2, w_id);
		    	 stmtGetCustWhse.setInt(3, d_id);
		    	 stmtGetCustWhse.setInt(4, c_id);
		    	ResultSet rs;		    	
		    		rs = stmtGetCustWhse.executeQuery();		    	
		    	
		    	boolean b1;
		    	 b1 = !rs.next();
		    	if ( b1)
		    		//  throw new RuntimeException("W_ID=" + w_id + " C_D_ID=" + d_id + " C_ID=" + c_id + " not found!");
		    		 return "new order txn error 1";
		    		
		    	 c_discount = rs.getFloat("c_discount");
		    		c_last = rs.getString("c_last");
		    	 c_credit = rs.getString("c_credit");
		    	 w_tax = rs.getFloat("w_tax");
		    		rs.close();
		    	 rs = null;

		    	if ( stmtGetDist == null) 
		    	{
		    		 stmtGetDist = conn.prepareStatement("SELECT d_next_o_id, d_tax FROM district WHERE d_w_id = ? AND d_id = ? FOR UPDATE");
		    	}
		    	 stmtGetDist.setInt(1, w_id);
		    	 stmtGetDist.setInt(2, d_id);
		    	 rs = stmtGetDist.executeQuery();
		    	
		    	boolean b2;
		    	 b2 = !rs.next();
		    	if ( b2) 
		    	{
		    		//  throw new RuntimeException("D_ID=" + d_id + " D_W_ID=" + w_id + " not found!");
		    		 return "new order txn error 2";
		    	}
		    	 d_next_o_id = rs.getInt("d_next_o_id");
		    	 d_tax = rs.getFloat("d_tax");
		    	 rs.close();
		    	 rs = null;
		    	 o_id = d_next_o_id;		    	
		    	
		    	if ( stmtInsertNewOrder == null) 
		    	{
		    		 stmtInsertNewOrder = conn.prepareStatement("INSERT INTO new_order (no_o_id, no_d_id, no_w_id) VALUES ( ?, ?, ?)");
		    	}
		    	 stmtInsertNewOrder.setInt(1, o_id);
		    	 stmtInsertNewOrder.setInt(2, d_id);
		    	 stmtInsertNewOrder.setInt(3, w_id);
		    	 stmtInsertNewOrder.executeUpdate();

		    	if ( stmtUpdateDist == null) 
		    	{
		    		 stmtUpdateDist = conn.prepareStatement("UPDATE district SET d_next_o_id = d_next_o_id + 1 WHERE d_w_id = ? AND d_id = ?");
		    	}
		    	 stmtUpdateDist.setInt(1, w_id);
		    		stmtUpdateDist.setInt(2, d_id);
		    	int result;
		    	 result = stmtUpdateDist.executeUpdate();
		    	if ( result == 0)
		    		//  throw new RuntimeException("Error!! Cannot update next_order_id on district for D_ID="+ d_id + " D_W_ID=" + w_id);
		    		 return "new order txn error 3";

		    	if ( stmtInsertOOrder == null) 
		    	{
		    		 stmtInsertOOrder = conn.prepareStatement("INSERT INTO oorder (o_id, o_d_id, o_w_id, o_c_id, o_entry_d, o_ol_cnt, o_all_local) VALUES (?, ?, ?, ?, ?, ?, ?)");
		    	}
		    	 stmtInsertOOrder.setInt(1, o_id);
		    		stmtInsertOOrder.setInt(2, d_id);
		    	 stmtInsertOOrder.setInt(3, w_id);
		    	 stmtInsertOOrder.setInt(4, c_id);
		    	Timestamp t1;
		    	 t1 = new Timestamp(System.currentTimeMillis());
		    		stmtInsertOOrder.setTimestamp(5, t1);
		                                   
		    	 stmtInsertOOrder.setInt(6, o_ol_cnt);
		    	 stmtInsertOOrder.setInt(7, o_all_local);
		    		stmtInsertOOrder.executeUpdate();


		    	int ol_number;
		    	boolean b3;
		    	 ol_number = 1;
		    	 b3 = ol_number <= o_ol_cnt;
		    	// for (int ol_number = 1; ol_number <= o_ol_cnt; ol_number++) {
		    	while ( b3)
		    	{		    			    		
		    		// array hack
		    		// ol_supply_w_id = supplierWarehouseIDs[ol_number - 1];

		    		int supplierWarehouseID;
		    		 supplierWarehouseID = supplierWarehouseIDs[ol_number - 1];
		    		 ol_supply_w_id = supplierWarehouseID;


		    		// array hack
		    		//  ol_i_id = itemIDs[ol_number - 1];

		    		int itemID;
		    			itemID = itemIDs[ol_number - 1];
		    			ol_i_id = itemID;


		    		// array hack
		    		// int quantity = orderQuantities[ol_number - 1];

		    		int orderQuantity;
		    		 orderQuantity = orderQuantities[ol_number - 1];
		    		int quantity;
		    			quantity = orderQuantity;				

		    		 ol_quantity = quantity;

		    		if ( stmtGetItem == null) 
		    		{
		    			 stmtGetItem = conn.prepareStatement("SELECT i_price, i_name , i_data FROM item WHERE i_id = ?");
		    		}
		    		 stmtGetItem.setInt(1, ol_i_id);
		    		 rs = stmtGetItem.executeQuery();

		    		boolean b4;
		    		 b4 = !rs.next();
		    		if ( b4) 
		    		{
		    			// This is (hopefully) an expected error: this is an expected new order rollback
		    			//		          assert ol_number == o_ol_cnt;
		    			//		          assert ol_i_id == jTPCCConfig.INVALID_ITEM_ID;
		    			//		          throw new UserrollbackException(
		    			// return "new order txn expected rollback";
		    			String s;
		    			 s = rollbackNewOrder(terminalMessage, w_id, d_id, o_id, c_id, c_last, c_credit);
		    			 return s;
		    			
		    		}

		    		 i_price = rs.getFloat("i_price");
		    		 i_name = rs.getString("i_name");
		    		 i_data = rs.getString("i_data");
		    		 rs.close();
		    		 rs = null;

		    		 itemPrices[ol_number - 1] = i_price;
		    		 itemNames[ol_number - 1] = i_name;

		    		if ( stmtGetStock == null) 
		    		{
		    			 stmtGetStock = conn.prepareStatement("SELECT s_quantity, s_data, s_dist_01, s_dist_02, s_dist_03, s_dist_04, s_dist_05, s_dist_06, s_dist_07, s_dist_08, s_dist_09, s_dist_10 FROM stock WHERE s_i_id = ? AND s_w_id = ? FOR UPDATE");
		    		}
		    		
		    		 stmtGetStock.setInt(1, ol_i_id);
		    		 stmtGetStock.setInt(2, ol_supply_w_id);
		    		 rs = stmtGetStock.executeQuery();
		    		
		    		boolean b5;
		    		 b5 = !rs.next();
		    		if ( b5)
		    			// throw new RuntimeException("I_ID=" + ol_i_id + " not found!");
		    			 return "new order error 4";
		    		
		    		 s_quantity = rs.getInt("s_quantity");
		    		 s_data = rs.getString("s_data");
		    		 s_dist_01 = rs.getString("s_dist_01");
		    		 s_dist_02 = rs.getString("s_dist_02");
		    		 s_dist_03 = rs.getString("s_dist_03");
		    		 s_dist_04 = rs.getString("s_dist_04");
					 s_dist_05 = rs.getString("s_dist_05");
						s_dist_06 = rs.getString("s_dist_06");
						s_dist_07 = rs.getString("s_dist_07");
						s_dist_08 = rs.getString("s_dist_08");
						s_dist_09 = rs.getString("s_dist_09");
						s_dist_10 = rs.getString("s_dist_10");
						rs.close();
						rs = null;

					 stockQuantities[ol_number - 1] = s_quantity;

					if ( s_quantity - ol_quantity >= 10) 
						 s_quantity -= ol_quantity;				
					else 					
						 s_quantity += -ol_quantity + 91;
		        
		        	if ( ol_supply_w_id == w_id) 
		        		 s_remote_cnt_increment = 0;
		        	else 
		        		 s_remote_cnt_increment = 1;
		        

		        	if ( stmtUpdateStock == null) 
		        	{
		        		 stmtUpdateStock = conn.prepareStatement("UPDATE stock SET s_quantity = ? , s_ytd = s_ytd + ?, s_remote_cnt = s_remote_cnt + ? WHERE s_i_id = ? AND s_w_id = ?");
		        	}
		        	 stmtUpdateStock.setInt(1, s_quantity);

		        	 stmtUpdateStock.setInt(2, ol_quantity);
		        		stmtUpdateStock.setInt(3, s_remote_cnt_increment);
		        	 stmtUpdateStock.setInt(4, ol_i_id);
		        	 stmtUpdateStock.setInt(5, ol_supply_w_id);
		        	 stmtUpdateStock.addBatch();

		        	 ol_amount = ol_quantity * i_price;
		        	 orderLineAmounts[ol_number - 1] = ol_amount;
		        	 total_amount += ol_amount;
		        	
		        	if ( i_data.indexOf("GENERIC") != -1 && s_data.indexOf("GENERIC") != -1) 
		        	{
		        		// brandGeneric[ol_number - 1] = 'B';
		        		 brandGeneric[ol_number - 1] = "B";
		        	} 
		        	else 
		        	{
		        		// brandGeneric[ol_number - 1] = 'G';
		        		 brandGeneric[ol_number - 1] = "G";
		        	}

		        	if ( d_id == 1) 		        	
		        		 ol_dist_info = s_dist_01;
		        	else if ( d_id == 2)		        
		        		 ol_dist_info = s_dist_02;
					else if ( d_id == 3)		        
						 ol_dist_info = s_dist_03;
					else if ( d_id == 4)				
						 ol_dist_info = s_dist_04;
					else if ( d_id == 5)				
						 ol_dist_info = s_dist_05;
					else if ( d_id == 6)				
						 ol_dist_info = s_dist_06;
					else if ( d_id == 7)					
						 ol_dist_info = s_dist_07;
					else if ( d_id == 8)				
						 ol_dist_info = s_dist_08;
					else if ( d_id == 9)				
						 ol_dist_info = s_dist_09;
					else if ( d_id == 10)									      
						 ol_dist_info = s_dist_10;				

		        	if ( stmtInsertOrderLine == null) 
		        	{
		        		 stmtInsertOrderLine = conn.prepareStatement("INSERT INTO order_line (ol_o_id, ol_d_id, ol_w_id, ol_number, ol_i_id, ol_supply_w_id, ol_quantity, ol_amount, ol_dist_info) VALUES (?,?,?,?,?,?,?,?,?)");
		        	}
		        	 stmtInsertOrderLine.setInt(1, o_id);
					 stmtInsertOrderLine.setInt(2, d_id);
						stmtInsertOrderLine.setInt(3, w_id);
						stmtInsertOrderLine.setInt(4, ol_number);
						stmtInsertOrderLine.setInt(5, ol_i_id);
						stmtInsertOrderLine.setInt(6, ol_supply_w_id);
						stmtInsertOrderLine.setInt(7, ol_quantity);
						stmtInsertOrderLine.setFloat(8, ol_amount);
						stmtInsertOrderLine.setString(9, ol_dist_info);
					
					 stmtInsertOrderLine.addBatch();								
					
			    	 ol_number++;
			    	 b3 = ol_number <= o_ol_cnt;

		    	} // end-for

		    	 stmtInsertOrderLine.executeBatch();
		    	 stmtUpdateStock.executeBatch();
		      
		    	 conn.commit();
		      
		    	 stmtInsertOrderLine.clearBatch();
		    	 stmtUpdateStock.clearBatch();

		    		total_amount *= (1 + w_tax + d_tax) * (1 - c_discount);
		     
		    	 terminalMessage.append("CMT\n+--------------------------- NEW-ORDER ---------------------------+\n");
		    	 terminalMessage.append(" Date: ");
				String t;
				 t = getCurrentTime();
		    	 terminalMessage.append(t);
				 terminalMessage.append("\n\n Warehouse: ");
				 terminalMessage.append(w_id);
					terminalMessage.append("\n   Tax:     ");
				 terminalMessage.append(w_tax);
				 terminalMessage.append("\n District:  ");
				 terminalMessage.append(d_id);
					terminalMessage.append("\n   Tax:     ");
					terminalMessage.append(d_tax);
					terminalMessage.append("\n Order:     ");
					terminalMessage.append(o_id);
					terminalMessage.append("\n   Lines:   ");
					terminalMessage.append(o_ol_cnt);
					terminalMessage.append("\n\n Customer:  ");
					terminalMessage.append(c_id);
					terminalMessage.append("\n   Name:    ");
					terminalMessage.append(c_last);
					terminalMessage.append("\n   Credit:  ");
					terminalMessage.append(c_credit);
					terminalMessage.append("\n   %Disc:   ");
					terminalMessage.append(c_discount);
				 terminalMessage.append("\n\n Order-Line List [Supp_W - Item_ID - Item Name - Qty - Stock - B/G - Price - Amount]\n");

				
				// for (int i = 0; i < o_ol_cnt; i++) {
				int i;
				boolean b6;
				 i = 0;
				 b6 = i < o_ol_cnt;
				while ( b6)
				{
					 terminalMessage.append("                 [");

					int supplierWarehouseID;
					 supplierWarehouseID = supplierWarehouseIDs[i];
					 terminalMessage.append(supplierWarehouseID);

					 terminalMessage.append(" - ");

					int itemID;
					 itemID = itemIDs[i];
					 terminalMessage.append(itemID);


					 terminalMessage.append(" - ");
						terminalMessage.append(itemNames[i]);
						terminalMessage.append(" - ");

					int quantity;
						quantity = orderQuantities[i]; 
						terminalMessage.append(quantity);

					 terminalMessage.append(" - ");
					 terminalMessage.append(stockQuantities[i]);
					 terminalMessage.append(" - ");
					 terminalMessage.append(brandGeneric[i]);
					 terminalMessage.append(" - ");
					
					String s;
					double d2;
					 d2 = itemPrices[i];
					 s = formattedDouble(d2);
					 terminalMessage.append(s);
					 terminalMessage.append(" - ");
					
					double d;
					 d = orderLineAmounts[i];
					 s = formattedDouble(d);				
					 terminalMessage.append(s);
					 terminalMessage.append("]\n");
					
					 i++;
					 b6 = i < o_ol_cnt;
				}
				 terminalMessage.append("\n\n Total Amount: ");
					terminalMessage.append(total_amount);
					terminalMessage.append("\n\n Execution Status: New order placed!\n");
				 terminalMessage.append("+-----------------------------------------------------------------+\n\n");
				//terminalMessage(terminalMessage.toString());

		    } // // ugh :-), this is the end of the try block at the beginning of this
		      
		    //		    catch (UserrollbackException e) {
		    /*
			catch (IOException e) 
			{
				 terminalMessage.append("ABT\n+---- NEW-ORDER Rollback Txn expected to happen for 1% of Txn's -----+");
				 terminalMessage.append("\n Warehouse: ");
				 terminalMessage.append(w_id);
				 terminalMessage.append("\n District:  ");
				 terminalMessage.append(d_id);
				 terminalMessage.append("\n Order:     ");
				 terminalMessage.append(o_id);
				 terminalMessage.append("\n\n Customer:  ");
				 terminalMessage.append(c_id);
				 terminalMessage.append("\n   Name:    ");
				 terminalMessage.append(c_last);
				 terminalMessage.append("\n   Credit:  ");
				 terminalMessage.append(c_credit);
				 terminalMessage.append("\n\n Execution Status: Item number is not valid!\n");
				 terminalMessage.append("+-----------------------------------------------------------------+\n\n");
				//terminalMessage(terminalMessage.toString());				
						      conn.rollback();		      		      
		    } 
		    
		    //finally {
			//		      if (stmtInsertOrderLine != null)
			//		        stmtInsertOrderLine.clearBatch();
			//		      if (stmtUpdateStock != null)
			//		        stmtUpdateStock.clearBatch();
			//		    }
		    */
		    
		    if ( stmtInsertOrderLine != null)
		    	 stmtInsertOrderLine.clearBatch();
		    if ( stmtUpdateStock != null)
		    	 stmtUpdateStock.clearBatch();
		   
		}
		/*
		catch (SQLException e)
		{
			//terminalMessage = new StringBuilder(e.getErrorCode() + "#" + e.getSQLState() + "#" + e.getMessage());
			//			throw e;
						try { conn.rollback(); } catch (SQLException e2) { throw new RuntimeException(e2); }
						terminalMessage = new StringBuilder();
		}
		*/
				
		String r;
			r = terminalMessage.toString();
		
		 return r;
	}	
	

	private String rollbackNewOrder (StringBuilder terminalMessage, int w_id, int d_id, int o_id, int c_id, 
									 String c_last, String c_credit) throws SQLException 
	{
		 terminalMessage.append("ABT\n+---- NEW-ORDER Rollback Txn expected to happen for 1% of Txn's -----+");
		 terminalMessage.append("\n Warehouse: ");
			terminalMessage.append(w_id);
			terminalMessage.append("\n District:  ");
			terminalMessage.append(d_id);
			terminalMessage.append("\n Order:     ");
			terminalMessage.append(o_id);
			terminalMessage.append("\n\n Customer:  ");
			terminalMessage.append(c_id);
			terminalMessage.append("\n   Name:    ");
			terminalMessage.append(c_last);
			terminalMessage.append("\n   Credit:  ");
			terminalMessage.append(c_credit);
			terminalMessage.append("\n\n Execution Status: Item number is not valid!\n");
		 terminalMessage.append("+-----------------------------------------------------------------+\n\n");
		    //terminalMessage(terminalMessage.toString());
		
	    if ( stmtInsertOrderLine != null)
	    	 stmtInsertOrderLine.clearBatch();
	    if ( stmtUpdateStock != null)
	    	 stmtUpdateStock.clearBatch();
		
		 conn.rollback();		
		
		String r;
		 r = terminalMessage.toString();
		 return r;
	}
	
	public void terminate ()
	{
		 ;
	}
	
	
	
	private Customer newCustomerFromResults (ResultSet rs) throws SQLException 
	{
		Customer c;
			c = new Customer();
		// TODO: Use column indices: probably faster?
			c.c_first = rs.getString("c_first");
			c.c_middle = rs.getString("c_middle");
			c.c_street_1 = rs.getString("c_street_1");
			c.c_street_2 = rs.getString("c_street_2");
			c.c_city = rs.getString("c_city");
			c.c_state = rs.getString("c_state");
			c.c_zip = rs.getString("c_zip");
			c.c_phone = rs.getString("c_phone");
			c.c_credit = rs.getString("c_credit");
			c.c_credit_lim = rs.getFloat("c_credit_lim");
			c.c_discount = rs.getFloat("c_discount");
			c.c_balance = rs.getFloat("c_balance");
			c.c_ytd_payment = rs.getFloat("c_ytd_payment");
			c.c_payment_cnt = rs.getInt("c_payment_cnt");
			c.c_since = rs.getTimestamp("c_since");
			return c;
	}	
	
	private Customer getCustomerByName (int c_w_id, int c_d_id, String c_last) throws SQLException 
	{
		ArrayList customers;
		 customers = new ArrayList();
		if ( customerByName == null) 
		{
				customerByName = conn.prepareStatement("SELECT c_first, c_middle, c_id, c_street_1, c_street_2, c_city, c_state, c_zip, c_phone, c_credit, c_credit_lim, c_discount, c_balance, c_ytd_payment, c_payment_cnt, c_since FROM customer WHERE c_w_id = ? AND c_d_id = ? AND c_last = ? ORDER BY c_first");
		}
		
			customerByName.setInt(1, c_w_id);
			customerByName.setInt(2, c_d_id);
			customerByName.setString(3, c_last);
		
		ResultSet rs;
		 rs = customerByName.executeQuery();

		boolean hasNext;
		 hasNext = rs.next();
		while ( hasNext) 
		{
			Customer c;
				c = newCustomerFromResults(rs);
				c.c_id = rs.getInt("c_id");
				c.c_last = c_last;
				customers.add(c);
        hasNext = rs.next();
		}
		 rs.close();

		int i1;
		 i1 = customers.size();
		if ( i1 == 0) 
		{
			// throw new RuntimeException("C_LAST=" + c_last + " C_D_ID=" + c_d_id + " C_W_ID=" + c_w_id + " not found!");			
			 return null;
		}

		// TPC-C 2.5.2.2: Position n / 2 rounded up to the next integer, but that
		// counts starting from 1.
		int index;
		 index = customers.size() / 2;
		
		int i2;
		 i2 = customers.size() % 2;
		if ( i2 == 0) 
		{
			 index -= 1;
		}
		
		Object o1;
		Customer c1;
		 o1 = customers.get(index);
		 c1 = (Customer)o1;
		 return c1; 
	}

	private Customer getCustomerById (int c_w_id, int c_d_id, int c_id) throws SQLException 
	{
		if ( payGetCust == null) 
		{
				payGetCust = conn.prepareStatement("SELECT c_first, c_middle, c_last, c_street_1, c_street_2, c_city, c_state, c_zip, c_phone, c_credit, c_credit_lim, c_discount, c_balance, c_ytd_payment, c_payment_cnt, c_since FROM customer WHERE c_w_id = ? AND c_d_id = ? AND c_id = ?");
		}
			payGetCust.setInt(1, c_w_id);
		 payGetCust.setInt(2, c_d_id);
			payGetCust.setInt(3, c_id);
		ResultSet rs;
				
				
			rs = payGetCust.executeQuery();
		boolean b1;
		 b1 = rs.next();
		if ( !b1) 
		{
			//	throw new RuntimeException("C_ID=" + c_id + " C_D_ID=" + c_d_id + " C_W_ID=" + c_w_id + " not found!");
			 return null;
		}
		
		Customer c;
			c = newCustomerFromResults(rs);
			c.c_id = c_id;
			c.c_last = rs.getString("c_last");
			rs.close();
			return c;
	}	
	
	
	// following 2 functions are copied from jTPCCUtil
	private String getCurrentTime () 
	{
		java.util.Date date;		
		 date = new java.util.Date();
		String r;
		 r = dateFormat.format(date);
		 return r;
	}
	
	private static String formattedDouble (double d) 
	{
		String dS;
		 dS = Double.toString(d);
		
		String s;
		if ( dS.length() > 6)		
			 s = dS.substring(0, 6);
		else
			 s = dS;
		 return s;			
	}
	
	
}
