package tpcc.client;

import java.sql.*;

import tpcc.pyxis.PyxisTransactionExecutor;

public class TestMain 
{
	public static void main (String [] args) throws Exception 
	{	
        String dbServer = "localhost";

		int mode = Integer.parseInt(args[0]);
			
		int [] itemIDs = new int[10];
		int [] rollbackItemIds = new int[10];

		int [] supplierWarehouseIDs = new int[10];
		int [] orderQuantities = new int[10];

		PyxisTransactionExecutor exe = null;
		if (mode == 0)
			exe = new PyxisTransactionExecutor(dbServer);

		int numTransaction = Integer.parseInt(args[1]);
		long startTime = System.currentTimeMillis();
		
		// standard says 10% of new order should be rolled back, change if necessary
		double percentRollback = 0.1;
        int numNormalTxns = (int)(numTransaction * (1 - percentRollback));
        int numRollbackTxns = (int)(numTransaction * percentRollback);
	 	exe.newOrderTransaction(1, 1, 1889, itemIDs.length, 0, itemIDs, supplierWarehouseIDs, orderQuantities);
        exe.newOrderTransaction(1, 1, 1889, rollbackItemIds.length, 0, rollbackItemIds, supplierWarehouseIDs, orderQuantities);
		exe.stockLevelTransaction (1, 1, 0);
		exe.testTransaction();
		exe.orderStatusTransaction (1, 1, 1, "", false); 
		exe.paymentTransaction (1,1, 0.0F, 1, 0, 0,"", false);
		exe.deliveryTransaction (0,0);		
		long endTime = System.currentTimeMillis();	
	}
}
