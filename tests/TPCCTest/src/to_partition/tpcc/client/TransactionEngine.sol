package tpcc.client;

import java.sql.SQLException;

public interface TransactionEngine 
{	
	//public int getNumSkippedDeliveries ();
	
	public String deliveryTransaction (int w_id, int o_carrier_id) throws SQLException;
	public String orderStatusTransaction(int w_id, int d_id, int c_id, String c_last, boolean c_by_name) throws SQLException;
	public String newOrderTransaction (int w_id, int d_id, int c_id, int o_ol_cnt, int o_all_local, int[] itemIDs, 
			   						   int[] supplierWarehouseIDs, int[] orderQuantities) throws SQLException;
	public String stockLevelTransaction (int w_id, int d_id, int threshold) throws SQLException;
	public String paymentTransaction (int w_id, int c_w_id, float h_amount, int d_id, int c_d_id, int c_id, 
			  						  String c_last, boolean c_by_name) throws SQLException;
	
	public void terminate();
}
