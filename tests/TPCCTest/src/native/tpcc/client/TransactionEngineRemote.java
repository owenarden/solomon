package tpcc.client;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TransactionEngineRemote extends Remote
{	
	
	public String deliveryTransaction (int w_id, int o_carrier_id) throws RemoteException;
	public String orderStatusTransaction(int w_id, int d_id, int c_id, String c_last, boolean c_by_name) throws RemoteException;
	public String newOrderTransaction (int w_id, int d_id, int c_id, int o_ol_cnt, int o_all_local, int[] itemIDs, 
			   						   int[] supplierWarehouseIDs, int[] orderQuantities) throws RemoteException;
	public String stockLevelTransaction (int w_id, int d_id, int threshold) throws RemoteException;
	public String paymentTransaction (int w_id, int c_w_id, float h_amount, int d_id, int c_d_id, int c_id, 
			  						  String c_last, boolean c_by_name) throws RemoteException;
	
	public void terminate() throws RemoteException;
}
