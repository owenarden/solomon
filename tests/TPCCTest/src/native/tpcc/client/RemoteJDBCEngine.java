package tpcc.client;

import java.sql.Connection;
import java.sql.SQLException;

public class RemoteJDBCEngine implements TransactionEngine 
{
	private Connection conn;
	private TransactionExecutor executor;
	
	public RemoteJDBCEngine (Connection conn)
	{
		this.conn = conn;
		this.executor = new TransactionExecutor(conn);
	}
	
	@Override
	public String deliveryTransaction(int w_id, int o_carrier_id) throws SQLException 
	{
		String ret = executor.deliveryTransaction(w_id, o_carrier_id);
		//System.out.println(ret);
		conn.commit();
		return ret;		
	}

	@Override
	public String orderStatusTransaction(int w_id, int d_id, int c_id, String c_last, boolean c_by_name) throws SQLException 
	{
		String ret = executor.orderStatusTransaction(w_id, d_id, c_id, c_last, c_by_name);
		//System.out.println(ret);
		conn.commit();
		return ret;
	}

	@Override
	public String newOrderTransaction(int w_id, int d_id, int c_id,
			int o_ol_cnt, int o_all_local, int[] itemIDs,
			int[] supplierWarehouseIDs, int[] orderQuantities) throws SQLException
	{
		String ret = executor.newOrderTransaction(w_id, d_id, c_id, o_ol_cnt, o_all_local, itemIDs, supplierWarehouseIDs, orderQuantities);
		//System.out.println(ret);
		if (ret.startsWith("CMT"))
			conn.commit();
		else
			conn.rollback();
		return ret;
	}

	@Override
	public String stockLevelTransaction(int w_id, int d_id, int threshold) throws SQLException 
	{
		String ret = executor.stockLevelTransaction(w_id, d_id, threshold);
		//System.out.println(ret);
		conn.commit();
		return ret;
	}

	@Override
	public String paymentTransaction(int w_id, int c_w_id, float h_amount,
			int d_id, int c_d_id, int c_id, String c_last, boolean c_by_name) throws SQLException 
	{
		String ret = executor.paymentTransaction(w_id, c_w_id, h_amount, d_id, c_d_id, c_id, c_last, c_by_name);
		//System.out.println(ret);
		conn.commit();
		return ret;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
