package tpcc.client;

/*
 * jTPCCTerminal - Terminal emulator code for jTPCC (transactions)
 *
 * Copyright (C) 2003, Raul Barbosa
 * Copyright (C) 2004-2006, Denis Lussier
 *
 */

import static tpcc.client.jTPCCConfig.DELIVERY;
import static tpcc.client.jTPCCConfig.NEW_ORDER;
import static tpcc.client.jTPCCConfig.ORDER_STATUS;
import static tpcc.client.jTPCCConfig.PAYMENT;
import static tpcc.client.jTPCCConfig.STOCK_LEVEL;
import static tpcc.client.jTPCCConfig.TERMINAL_MESSAGES;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import tpcc.pyxis.PyxisTransactionExecutor;

public class jTPCCTerminal implements Runnable {
  private String terminalName;
  private Connection conn = null;
  private Statement stmt = null;
  private ResultSet rs = null;
  private final int terminalWarehouseID;
  /** Forms a range [lower, upper] (inclusive). */
  private final int terminalDistrictLowerID;
  private final int terminalDistrictUpperID;
  private int paymentWeight, orderStatusWeight, deliveryWeight,
      stockLevelWeight;
  private SimplePrinter terminalOutputArea, errorOutputArea;
  private boolean debugMessages;
  private jTPCCDriver parent;
  private final Random gen = new Random();

  private int transactionCount = 1, numTransactions, numWarehouses,
      newOrderCounter;
  private int result = 0;
  private volatile boolean stopRunningSignal = false;
    
  private enum ExecutionMode { JDBC, RMI, PYXIS };
  private ExecutionMode mode;
  
  private TransactionEngine engine;
  private int terminalNumber;

  private static int numTerms = 0;  
  
  public jTPCCTerminal(String terminalName, int terminalNumber, int terminalWarehouseID,
      int terminalDistrictLowerID, int terminalDistrictUpperID, Connection conn,
      int numTransactions,
      SimplePrinter terminalOutputArea, SimplePrinter errorOutputArea,
      boolean debugMessages, int paymentWeight, int orderStatusWeight,
      int deliveryWeight, int stockLevelWeight, int numWarehouses,
      jTPCCDriver parent) throws SQLException
  {
    this.terminalName = terminalName;
    System.out.println("term number: " + terminalNumber);
    this.terminalNumber = terminalNumber;
    this.conn = conn;
    if (conn != null)
    	this.stmt = conn.createStatement();
    //this.stmt.setMaxRows(200);
    //this.stmt.setFetchSize(100);

    this.terminalWarehouseID = terminalWarehouseID;
    this.terminalDistrictLowerID = terminalDistrictLowerID;
    this.terminalDistrictUpperID = terminalDistrictUpperID;
    assert this.terminalDistrictLowerID >= 1;
    assert this.terminalDistrictUpperID <= jTPCCConfig.configDistPerWhse;
    assert this.terminalDistrictLowerID <= this.terminalDistrictUpperID;
    this.terminalOutputArea = terminalOutputArea;
    this.errorOutputArea = errorOutputArea;
    this.debugMessages = debugMessages;
    this.parent = parent;
    this.numTransactions = numTransactions;
    this.paymentWeight = paymentWeight;
    this.orderStatusWeight = orderStatusWeight;
    this.deliveryWeight = deliveryWeight;
    this.stockLevelWeight = stockLevelWeight;
    this.numWarehouses = numWarehouses;
    this.newOrderCounter = 0;
    
    
    String modeString = System.getProperty("mode");
    if (modeString.equalsIgnoreCase("rmi"))
    {
    	this.mode = ExecutionMode.RMI;
    }
    else if (modeString.equalsIgnoreCase("jdbc"))
    {
    	this.mode = ExecutionMode.JDBC;   
    }
    else if (modeString.equalsIgnoreCase("pyxis"))
    {
    	this.mode = ExecutionMode.PYXIS;    	    	
    }
    else
    	throw new RuntimeException("must specify execution mode");
  
    ++numTerms;
    
    terminalMessage("Terminal \'" + terminalName + "\' has WarehouseID="
                    + terminalWarehouseID + " and DistrictID=["
                    + terminalDistrictLowerID + ", " + terminalDistrictUpperID + "].");
  }

  public void closeConnection()
  {
	  System.out.println("trying to close conn");
	  try 
	  {
		  stmt.close();
		  conn.close(); 
	  } 
	  catch (SQLException e) { e.printStackTrace(); }
  }
  
	public void createEngine() 
	{
		try
		{
			if (mode == ExecutionMode.RMI)
			{
				System.out.println("running rmi mode");
				String rmiServer = System.getProperty("remoteHost");
				assert (rmiServer != null);
				this.engine = new RmiEngine(rmiServer, terminalNumber + "");
	
				// this.engine = new SwiftTransactionExecutor();
			} 
			else if (mode == ExecutionMode.JDBC) 
			{
				System.out.println("running jdbc mode");
				this.engine = new RemoteJDBCEngine(conn);
			} 
			else if (mode == ExecutionMode.PYXIS) 
			{
				System.out.println("running pyxis mode");
							
				String pyxisServer = System.getProperty("remoteHost");
				assert (pyxisServer != null);
				this.engine = new PyxisTransactionExecutor(pyxisServer);		
			}
		}
		catch (Exception e) 
		{
			throw new RuntimeException(e);
		}
	}
  
  public void run() {
			
    executeTransactions(numTransactions);

    System.out.println("closing statement and connection");
    printMessage("Closing statement and connection...");

    try {
      stmt.close();
      if (conn != null)
    	  conn.close();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }

    printMessage("Terminal \'" + terminalName + "\' finished after "
                 + (transactionCount - 1) + " transaction(s).");

    parent.signalTerminalEnded(this, newOrderCounter);

    if (deadlockLocations != null) {
      for (Map.Entry<Integer, Integer> e : deadlockLocations.entrySet()) {
        System.out.println("jTPCCTerminal.java:" + e.getKey() + ": " + e.getValue() + " deadlocks");
      }
    }
    
    System.out.println("num deadlocks: " + numDeadlocks);
  }

  public void stopRunningWhenPossible() {
    stopRunningSignal = true;
    printMessage("Terminal received stop signal!");
    printMessage("Finishing current transaction before exit...");
  }

  public jTPCCConfig.TransactionType chooseTransaction() {
    // Generate an integer in the range [1, 100] (that means inclusive!)
    int randomPercentage = gen.nextInt(100) + 1;

    jTPCCConfig.TransactionType type;
    if (randomPercentage <= paymentWeight) {
      type = jTPCCConfig.TransactionType.PAYMENT;
    } else if (randomPercentage <= paymentWeight + stockLevelWeight) {
      type = jTPCCConfig.TransactionType.STOCK_LEVEL;
    }  else if (randomPercentage <= paymentWeight + stockLevelWeight
        + orderStatusWeight) {
      type = jTPCCConfig.TransactionType.ORDER_STATUS;
    } else if (randomPercentage <= paymentWeight + stockLevelWeight
        + orderStatusWeight + deliveryWeight) {
      type = jTPCCConfig.TransactionType.DELIVERY;
    } else {
      assert paymentWeight + stockLevelWeight + orderStatusWeight + deliveryWeight
          < randomPercentage && randomPercentage <= 100;
      type = jTPCCConfig.TransactionType.NEW_ORDER;
    }

    return type;
  }

  private void executeTransactions(int numTransactions) {
    boolean stopRunning = false;

    int nhot = Integer.getInteger("nhot", 0);

    //
    // Following vars for TPMC throttling.
    //

    long startTime = System.currentTimeMillis();
    int numNewOrders = 0;
    // double lastTargetTpmc = 0;

    if (numTransactions != -1)
      printMessage("Executing " + numTransactions + " transactions...");
    else
      printMessage("Executing for a limited time...");

    for (int i = 0; (i < numTransactions || numTransactions == -1)
                    && !stopRunning; i++) {
      jTPCCConfig.TransactionType type = chooseTransaction();

      if (type == jTPCCConfig.TransactionType.NEW_ORDER) {
        Double targetTpmcRef = jTPCCDriver.targetTpmc.get();
        double targetTpmc = targetTpmcRef == null ? 0 : targetTpmcRef
            .doubleValue();
        // Throttle TPMC?
        if (targetTpmc != 0) {
          // This was a bad idea because the target TPMC is being
          // updated continuously.
          //
          // // Reset our counters if the rate has been changed, since
          // // this should mark a new epoch (otherwise we may suddenly
          // // try to catch up or slow down a lot).
           long t =System.currentTimeMillis();
           if (t-startTime > 10000) {
        	    startTime = t;
           		numNewOrders = 0;
           }
          // lastTargetTpmc = targetTpmc;

          long deadline = startTime + (long) (60000.0 / targetTpmc)
                          * numNewOrders;
          long currTime = System.currentTimeMillis();
          long wait = deadline - currTime;
          System.out.println("targetTpmc " + targetTpmc + " currentTtpmc "
                             + 60e3 * numNewOrders / (currTime - startTime)
                             + " wait " + wait);
          if (wait > 0) {
            try {
              Thread.sleep(wait);
            } catch (InterruptedException e) {
              throw new RuntimeException(e);
            }
          }
          numNewOrders++;
        }

        newOrderCounter++;
      }

      long transactionStart = System.currentTimeMillis();
      int skippedDeliveries = executeTransaction(type.ordinal());
      long transactionEnd = System.currentTimeMillis();

      String skippedMessage = null;
      if (type == jTPCCConfig.TransactionType.DELIVERY) {
        skippedMessage = skippedDeliveries == 0 ?
            "None" : "" + skippedDeliveries + " delivery(ies) skipped.";
      }

      int isNewOrder = type == jTPCCConfig.TransactionType.NEW_ORDER ? 1 : 0;
      parent.signalTerminalEndedTransaction(
          this.terminalName,
          type.toString(),
          transactionEnd - transactionStart,
          skippedMessage,
          isNewOrder);

      if (this.terminalWarehouseID < nhot) {
        nhot = 0;
      }

      if (stopRunningSignal)
        stopRunning = true;
    }
  }

  /** Executes a single TPCC transaction of type transactionType. */
  public int executeTransaction(int transaction) 
  {
	 
    int result = 0;

    try {
      switch (transaction) {
      case NEW_ORDER:
        int districtID = chooseRandomDistrict();
        int customerID = jTPCCUtil.getCustomerID(gen);
  
        int numItems = (int) jTPCCUtil.randomNumber(5, 15, gen);
        int[] itemIDs = new int[numItems];
        int[] supplierWarehouseIDs = new int[numItems];
        int[] orderQuantities = new int[numItems];
        int allLocal = 1;
        for (int i = 0; i < numItems; i++) {
          itemIDs[i] = jTPCCUtil.getItemID(gen);
          if (jTPCCUtil.randomNumber(1, 100, gen) > 1) {
            supplierWarehouseIDs[i] = terminalWarehouseID;
          } else {
            do {
              supplierWarehouseIDs[i] = jTPCCUtil.randomNumber(1, numWarehouses,
                                                               gen);
            } while (supplierWarehouseIDs[i] == terminalWarehouseID
                     && numWarehouses > 1);
            allLocal = 0;
          }
          orderQuantities[i] = jTPCCUtil.randomNumber(1, 10, gen);
        }
  
        // we need to cause 1% of the new orders to be rolled back.
        if (jTPCCUtil.randomNumber(1, 100, gen) == 1)
          itemIDs[numItems - 1] = jTPCCConfig.INVALID_ITEM_ID;
  
        terminalMessage("\nStarting transaction #" + transactionCount
                        + " (New-Order)...");
        while (true) {
          try {
            newOrderTransaction(terminalWarehouseID, districtID, customerID,
                                numItems, allLocal, itemIDs, supplierWarehouseIDs,
                                orderQuantities);
            break;
          } catch (SQLException e) {
            rollbackAndHandleSerializationError(e);
          }
        }
        break;

      case PAYMENT:
        districtID = chooseRandomDistrict();
  
        int x = jTPCCUtil.randomNumber(1, 100, gen);
        int customerDistrictID;
        int customerWarehouseID;
        if (x <= 85) {
          customerDistrictID = districtID;
          customerWarehouseID = terminalWarehouseID;
        } else {
          customerDistrictID = jTPCCUtil.randomNumber(1, jTPCCConfig.configDistPerWhse, gen);
          do {
            customerWarehouseID = jTPCCUtil.randomNumber(1, numWarehouses, gen);
          } while (customerWarehouseID == terminalWarehouseID
                   && numWarehouses > 1);
        }
  
        long y = jTPCCUtil.randomNumber(1, 100, gen);
        boolean customerByName;
        String customerLastName = null;
        customerID = -1;
        if(y <= 60) {
          // 60% lookups by last name
          customerByName = true;
          customerLastName = jTPCCUtil.getNonUniformRandomLastNameForRun(gen);
        } else {
          // 40% lookups by customer ID
          customerByName = false;
          customerID = jTPCCUtil.getCustomerID(gen);
        }
  
        float paymentAmount = (float) (jTPCCUtil.randomNumber(100, 500000, gen) / 100.0);
  
        terminalMessage("\nStarting transaction #" + transactionCount
                        + " (Payment)...");
        while (true) {
          try {
            paymentTransaction(terminalWarehouseID, customerWarehouseID,
                               paymentAmount, districtID, customerDistrictID,
                               customerID, customerLastName, customerByName);
            break;
          } catch (SQLException e) {
            rollbackAndHandleSerializationError(e);
          }
        }
        break;
  
      case STOCK_LEVEL:
        int threshold = jTPCCUtil.randomNumber(10, 20, gen);
  
        terminalMessage("\nStarting transaction #" + transactionCount
                        + " (Stock-Level)...");
        districtID = chooseRandomDistrict();
        
        while (true) {
          try {
            stockLevelTransaction(terminalWarehouseID, districtID, threshold);
            break;
          } catch (SQLException e) {
            rollbackAndHandleSerializationError(e);
          }
        }
        break;
  
      case ORDER_STATUS:
        districtID = chooseRandomDistrict();
  
        y = jTPCCUtil.randomNumber(1, 100, gen);
        customerLastName = null;
        customerID = -1;
        if (y <= 60) {
          customerByName = true;
          customerLastName = jTPCCUtil.getNonUniformRandomLastNameForRun(gen);
        } else {
          customerByName = false;
          customerID = jTPCCUtil.getCustomerID(gen);
        }
  
        terminalMessage("\nStarting transaction #" + transactionCount
                        + " (Order-Status)...");
        while (true) {
          try {
            orderStatusTransaction(terminalWarehouseID, districtID, customerID,
                                   customerLastName, customerByName);
            break;
          } catch (SQLException e) {
            rollbackAndHandleSerializationError(e);
          }
        }
        break;
  
      case DELIVERY:
        int orderCarrierID = jTPCCUtil.randomNumber(1, 10, gen);
  
        terminalMessage("\nStarting transaction #" + transactionCount
                        + " (Delivery)...");
        while (true) {
          try {
            result = deliveryTransaction(terminalWarehouseID, orderCarrierID);
            break;
          } catch (SQLException e) {
        	  System.out.println("caught exception in rollback");
            rollbackAndHandleSerializationError(e);
          }
        }
        break;
  
      default:
        throw new RuntimeException("Bad transaction type = " + transaction);
      }
      transactionCount++;

    } catch (SQLException e) {
      throw new RuntimeException(e);
    }

    return result;
  }

  /** Rolls back the current transaction, then rethrows e if it is not a
   * serialization error. Serialization errors are exceptions caused by
   * deadlock detection, lock wait timeout, or similar.
   *  
   * @param e Exception to check if it is a serialization error.
   * @throws SQLException
   */
  private static int numDeadlocks = 0;
  // Lame deadlock profiling: set this to new HashMap<Integer, Integer>() to enable.
  private final HashMap<Integer, Integer> deadlockLocations = null;
  private void rollbackAndHandleSerializationError(SQLException e) throws SQLException {
    //conn.rollback();

    // Unfortunately, JDBC provides no standardized way to do this, so we
    // resort to this ugly hack.
    boolean isSerialization = false;
    if (e.getErrorCode() == 1213 && e.getSQLState().equals("40001")) {
      isSerialization = true;
      assert e.getMessage().equals("Deadlock found when trying to get lock; try restarting transaction");
      System.out.println(Thread.currentThread().getName() + "retry deadlock");
      ++numDeadlocks;
    } else if (e.getErrorCode() == 1205 && e.getSQLState().equals("41000")) {
      // TODO: This probably shouldn't really happen?
      isSerialization = true;
      assert e.getMessage().equals("Lock wait timeout exceeded; try restarting transaction");
    }

    if (!isSerialization) {
      System.out.println("unknown exception code " + e.getErrorCode() + " state " + e.getSQLState());
      throw e;
    }

    if (deadlockLocations != null) {
      String className = this.getClass().getCanonicalName();
      for (StackTraceElement trace : e.getStackTrace()) {
        if (trace.getClassName().equals(className)) {
          int line = trace.getLineNumber();
          Integer count = deadlockLocations.get(line);
          if (count == null) count = 0;
  
          count += 1;
          deadlockLocations.put(line, count);
          return;
        }
      }
      assert false;
    }
  }

  private int chooseRandomDistrict() {
    return jTPCCUtil.randomNumber(terminalDistrictLowerID, terminalDistrictUpperID, gen);
  }

  private int deliveryTransaction(int w_id, int o_carrier_id) throws SQLException
  {
	  String result = engine.deliveryTransaction(w_id, o_carrier_id);
	  terminalMessage(result);
	  
	  int numSkippedDeliveries = 0;
	  if (result.startsWith("CMT"))
	  {
		  int startIndex = result.lastIndexOf('+');
		  int endIndex = result.indexOf('\n', startIndex);
		  assert(startIndex != -1 && endIndex != -1);
		  numSkippedDeliveries = Integer.parseInt(result.substring(startIndex + 1, endIndex));
	  }
	  
	  //System.out.println("num skipped: " + numSkippedDeliveries);
	  return numSkippedDeliveries; 
  }

  private void orderStatusTransaction(int w_id, int d_id, int c_id,
                                      String c_last, boolean c_by_name) throws SQLException 
  {
	  String result = engine.orderStatusTransaction(w_id, d_id, c_id, c_last, c_by_name);
	  terminalMessage(result);
  }

  private void newOrderTransaction(int w_id, int d_id, int c_id, int o_ol_cnt,
                                   int o_all_local, int[] itemIDs,
                                   int[] supplierWarehouseIDs,
                                   int[] orderQuantities) throws SQLException
  {
	  String result = engine.newOrderTransaction(w_id, d_id, c_id, o_ol_cnt, o_all_local, itemIDs, supplierWarehouseIDs, orderQuantities);
	  terminalMessage(result);
  }

  private void stockLevelTransaction(int w_id, int d_id, int threshold) throws SQLException
  {
	  String result = engine.stockLevelTransaction(w_id, d_id, threshold);
	  terminalMessage(result);
  }

  private void paymentTransaction(int w_id, int c_w_id, float h_amount,
                                  int d_id, int c_d_id, int c_id,
                                  String c_last, boolean c_by_name) throws SQLException
  {
	  String result = engine.paymentTransaction(w_id, c_w_id, h_amount, d_id, c_d_id, c_id, c_last, c_by_name);
	  terminalMessage(result);
  }

  private void error(String type) {
    errorOutputArea.println("[ERROR] TERMINAL=" + terminalName + "  TYPE="
                            + type + "  COUNT=" + transactionCount);
  }

  private void terminalMessage(String message) {
    if (TERMINAL_MESSAGES)
      terminalOutputArea.println(message);
  }

  private void printMessage(String message) {
    if (debugMessages)
      terminalOutputArea.println("[ jTPCC ] " + message);
  }
  
  /*
  private void commitOrRollback (boolean isCommit)
  {
	  try
	  {
		  if (isCommit)
			  conn.commit();
		  else
			  conn.rollback();
	  }
	  catch (SQLException e)
	  {
		  throw new RuntimeException("cannot commit/rollback xact");
	  }
  }
  */
}
