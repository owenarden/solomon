package tpcc.client;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.sql.SQLException;

/**
 * the client part of the rmi engine
 * @author akcheung
 *
 */
public class RmiEngine implements TransactionEngine 
{
	private TransactionEngineRemote server;
	
	// standardize the thrown exception like the other engines
	public RmiEngine (String serverName, String terminalName) throws SQLException
	{
		try
		{
			System.out.println("look up RMI object at: " + serverName + "/RmiServer-" + terminalName);
			this.server = (TransactionEngineRemote)Naming.lookup("//" + serverName + "/RmiServer-" + terminalName);
			System.out.println("rmi success");
		}
		catch (Exception e)
		{ throw new SQLException(e); }		
	}
	
	@Override
	public String deliveryTransaction(int w_id, int o_carrier_id) throws SQLException 
	{
		String r = "";
		try { r = server.deliveryTransaction(w_id, o_carrier_id); }
		catch (RemoteException e) { throw (SQLException)(e.getCause().getCause());  }
		
		//System.out.println(r);
		return r;
	}

	@Override
	public String orderStatusTransaction(int w_id, int d_id, int c_id, String c_last, boolean c_by_name) throws SQLException
	{
		String r = "";
		try { r = server.orderStatusTransaction(w_id, d_id, c_id, c_last, c_by_name); }
		catch (RemoteException e) { throw (SQLException)(e.getCause().getCause());   }
		
		//System.out.println(r);
		return r;
	}

	public static long newOrderTime = 0;
	@Override
	public String newOrderTransaction (int w_id, int d_id, int c_id, int o_ol_cnt, int o_all_local, int[] itemIDs,
									   int[] supplierWarehouseIDs, int[] orderQuantities) throws SQLException
	{
		//long start = System.currentTimeMillis();
		
		String r = "";
		try 
		{
			r = server.newOrderTransaction(w_id, d_id, c_id, o_ol_cnt, o_all_local, itemIDs,
			     						     supplierWarehouseIDs, orderQuantities); 

		}
		catch (RemoteException e) 
		{			
			System.out.println("exception: ");
			e.printStackTrace();
			throw (SQLException)(e.getCause().getCause()); 
		}
		
		//System.out.println(r);
		/*
		long end = System.currentTimeMillis();
		newOrderTime += (end - start);
		System.out.println("rpc time: " + newOrderTime);
		*/
		return r;
	}

	@Override
	public String stockLevelTransaction(int w_id, int d_id, int threshold) throws SQLException
	{
		String r = "";
		try { r = server.stockLevelTransaction(w_id, d_id, threshold); }
		catch (RemoteException e) { throw (SQLException)(e.getCause().getCause());   }
		
		//System.out.println(r);
		return r;	
	}

	@Override
	public String paymentTransaction (int w_id, int c_w_id, float h_amount, int d_id, int c_d_id, 
									  int c_id, String c_last, boolean c_by_name) throws SQLException 
	{
		String r = "";
		try { r = server.paymentTransaction(w_id, c_w_id, h_amount, d_id, c_d_id, c_id, c_last, c_by_name); }
		catch (RemoteException e) { throw (SQLException)(e.getCause().getCause());   }
				
		//System.out.println(r);
		return r;	
	}

	@Override
	public void terminate() 
	{			
	}

}
