package tpcc.client;

import java.sql.SQLException;
import pyxis.runtime.*;

public interface TransactionEngine {
    
    public String deliveryTransaction(int w_id, int o_carrier_id)
          throws SQLException;
    
    public String orderStatusTransaction(int w_id, int d_id, int c_id,
                                         String c_last, boolean c_by_name)
          throws SQLException;
    
    public String newOrderTransaction(int w_id, int d_id, int c_id,
                                      int o_ol_cnt, int o_all_local,
                                      int[] itemIDs, int[] supplierWarehouseIDs,
                                      int[] orderQuantities)
          throws SQLException;
    
    public String stockLevelTransaction(int w_id, int d_id, int threshold)
          throws SQLException;
    
    public String paymentTransaction(int w_id, int c_w_id, float h_amount,
                                     int d_id, int c_d_id, int c_id,
                                     String c_last, boolean c_by_name)
          throws SQLException;
    
    public void terminate();
    
    String jlc$CompilerVersion$jl = "2.4.0";
    long jlc$SourceLastModified$jl = 1323830945000L;
    String jlc$ClassType$jl =
      ("H4sIAAAAAAAAALVXW4hbRRieXDfZTbub9OLSdrvbG1qliQ9a1EVqLFu72+im" +
       "uxXblZJOzplkT3dy\nzunMJJutWhSxtwdBWm/g5UUoSJ9a1AcFhdYrPsi+1B" +
       "cFqYigFfsgFqnoPzPJJjlJy4I1MJM5c/7/\nn///5/sv5+wVFOIMDbkOnStS" +
       "RyTFnEt4MosZJ+Z2ijnfAxs544EN12e/Pja2OoB6p1CvZU8KLCxj\nu2MLUh" +
       "VTKFYipTxhPG2axJxCcZsQc5IwC1PrMBA69hRKcKtoY1FmhE8Q7tCKJEzwsk" +
       "uYOrO+mUEx\nw7G5YGVDOIwL1Jc5iCs4VRYWTWUsLoYzKFywCDX5IXQE+TIo" +
       "VKC4CIQrM3UrUkpiaofcB/JuC9Rk\nBWyQOktwxrJNgQa9HAsWb9wFBMDaVS" +
       "Ji2lk4Kmhj2EAJrRLFdjE1KZhlF4E05JThFIFW3VAoEEVc\nbMzgIskJ1O+l" +
       "y+pXQBVVbpEsAq3wkilJVYZWee6s6bbGw7G/T2b/HPIrnU1iUKl/CJjWepgm" +
       "SIEw\nYhtEM14rJ0+P7iuv8SMExCs8xJomvemDxzM/fzyoaVZ3oBnPHySGyB" +
       "nXt64ZmE//GA1INSKuwy0J\nhRbL1a1ma2+Gqy5gceWCRPkyWX/5ycRn+559" +
       "l/ziR5FRFDYcWi7ZoyhKbHN7bd0F64xlE707Xihw\nIkZRkKqtsKOewR0Fix" +
       "LpjiCsXSym1brqIoS6YPhgJJD+heQEN7CHYZtjQ2oxYhdBXNK1qECPpaad\n" +
       "EknhGWOalAEJLnMOpty5qsVTgnDBU4ZTAkLCUpwZKeEaRsqgFrFFqqPAqlRk" +
       "yazPBz5Y441HCuDd\n6VCTsJxx5vJXT4/sOnHcv4DImgkCDchTkvqUZNspyO" +
       "dT0m9r9bC8MlNG1q/nhvte3MLf96PAFIpa\npVJZ4DwlEJGYUmeWmDmhIBlv" +
       "gr9CHUA2lgf0QiDkKAhS0QIurTC03ovSRmyPwgoD9OaP/PPNb7nZ\n8xJQEg" +
       "DLpXStGlznjNYttnly/9iB4+sDkmg2KG+mqqJyhTzF668dMj/U5ZfyT/1x4a" +
       "3uIS1f8qyq\nC4DDvJwyZ1x7Pnz3pY96PvU3p5fepjw0SYQGa7yh6x5GCOx/" +
       "91r21MtXjj2pFNWaBgSK4DyYDZch\nwLH1bCRQ2C3nqWU0WbKsQzwl+5effm" +
       "XzG5c6GjBwozSgUtixvVdjR/HF/TpYE60XP2KXS/e+/S3Z\n/FDM6ACmqHDc" +
       "LZRUCG2o5z3tUZUe657ufWLwhx1bzzzjVTQMeg7elDNnxCurdwemrS8AfLWc" +
       "1SE/\ntzINN2sMaZ4RKC+2tF3udFXb0ZdljkFMqEGNc1/9a+fvp0L3v+eXIq" +
       "JQegSGUIE8vtZ7eEuuHK4D\nQx7lz6CegsNKmMpX9WLRLaaZM9vYUQHeo9ZL" +
       "wSkRGNtgRGvZRv3Ll31yiut0oKiH1LxBg8kv15vk\ndHsNNj453wkuXtYAY5" +
       "oxPCfjpvrc/MDrn+M3IQdDLuTWYaJSnV/L4u0VIcusEiTcSq0ivLT2nZ/O\n" +
       "X55Y7m8qmxvaKlczjy6dSv1uVyJ03c1OUNQX71p39sjE93mJG8m3RaAARIlc" +
       "pluNlPODMk0r/X3q\nOQEpWlnOD9Hk5O7MSNUgrsx6StZ9Ai0zCYXj2FxTRq" +
       "xz9ilOmcqSupKrF/e03BKB0V27pe7/eEt1\nA0KKLKRMbJ3GFM02V/M9LFBX" +
       "3nEowfZNPSEfH1HaZKATchhUCtmjlXmTzQ3prfalYcRq9vXcIvsi\niizSwT" +
       "498fYGRmFWo+5cYONV/4crN6qgDOYx1xHt7fzaG7uWfk0pH3XVX9p1XfmfU9" +
       "MiXbkXsGOT\n2XHpzUX4cSeMJTU/LrlFfgzoCuLx3iL1NyAyuHCMmYzM4ouw" +
       "IAmjt2bB0v8LCV58Qx51sOgUCXKa\nWKStUC4SLp4rQeezCEP7YcRrhsZvka" +
       "GopS25wUv5XJW1lbCSBd9DxOuNYMWxTKCIt7Vvst/ob/tQ\n058Txvr5A3dc" +
       "cONf6oipt/xh6LsLZUqbqmRzxQy7jBQspVNYVycdLEcF6mnqJKFP0Qul4wua" +
       "5gSo\nKmnk+qTbIZ3qr4DqvySvSZ5iDgAA");
}
