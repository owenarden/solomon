package tpcc.client;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.net.*;
import java.util.*;

public class RmiServer extends UnicastRemoteObject implements TransactionEngineRemote
{
	private static final long serialVersionUID = 1L;
	private TransactionExecutor executor;
	private Connection conn;
	
	public RmiServer (String dbName, String username, String password) throws ClassNotFoundException, SQLException, RemoteException
	{	
		//Class.forName("org.postgresql.Driver");              
        //Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/" + dbName, username, password);
		//Connection conn = DriverManager.getConnection("jdbc:postgresql://128.30.77.80:5432/" + dbName, username, password);
        
		Class.forName("com.mysql.jdbc.Driver");
        
        //this.executor = new TransactionExecutor("jdbc:postgresql://128.30.77.80:5432/" + dbName, username, password);
        
        // hardwired for now
        //this.conn = DriverManager.getConnection("jdbc:mysql://128.30.77.80:3310/tpcc", "root", "");
        this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/tpcc", "root", "");
        conn.setAutoCommit(false);
        this.executor = new TransactionExecutor(conn);
        
        System.out.println("local jdbc connection created");
	}
	
	public String deliveryTransaction (int w_id, int o_carrier_id) throws RemoteException 
	{
		//System.out.println("execute RMI delivery txn");
		try
		{
			String ret = executor.deliveryTransaction(w_id, o_carrier_id);
			conn.commit();
			return ret;
		}
		catch (SQLException e)
		{
			throw new RemoteException("SQLException", e);
		}
	}

	public String orderStatusTransaction (int w_id, int d_id, int c_id, String c_last, boolean c_by_name) throws RemoteException 
	{
		//System.out.println("execute RMI order status txn");
		try
		{
			String ret = executor.orderStatusTransaction(w_id, d_id, c_id, c_last, c_by_name);
			conn.commit();
			return ret;
		}
		catch (SQLException e)
		{
			throw new RemoteException("SQLException", e);
		}
	}

	public static long doWorkTime = 0;
	public String newOrderTransaction (int w_id, int d_id, int c_id, int o_ol_cnt, int o_all_local, int[] itemIDs,
									   int[] supplierWarehouseIDs, int[] orderQuantities) throws RemoteException 
    {
		
		try
		{
			//long start = System.currentTimeMillis();
			
			String ret = executor.newOrderTransaction(w_id, d_id, c_id, o_ol_cnt, o_all_local, itemIDs, supplierWarehouseIDs, orderQuantities);
			if (ret.startsWith("CMT"))
				conn.commit();
			else
				conn.rollback();
			/*
			long end = System.currentTimeMillis();
			doWorkTime += (end - start);
			
			System.out.println("do work time: " + doWorkTime);
			*/
			return ret;
		}
		catch (SQLException e)
		{
			throw new RemoteException("SQLException", e);			
		}
	}

	public String stockLevelTransaction (int w_id, int d_id, int threshold) throws RemoteException 
	{
		//System.out.println("execute RMI stock level txn");
		try
		{
			String ret = executor.stockLevelTransaction(w_id, d_id, threshold);
			conn.commit();
			return ret;
		}
		catch (SQLException e)
		{
			throw new RemoteException("SQLException", e);		
		}
	}

	public String paymentTransaction (int w_id, int c_w_id, float h_amount, int d_id, int c_d_id, int c_id, 
									  String c_last, boolean c_by_name) throws RemoteException 
	{
		//System.out.println("execute RMI payment txn");
		try
		{
			String ret = executor.paymentTransaction(w_id, c_w_id, h_amount, d_id, c_d_id, c_id, c_last, c_by_name);
			conn.commit();
			return ret;
		}
		catch (SQLException e)
		{
			throw new RemoteException("SQLException", e);
		}
		
	}

	public void terminate() throws RemoteException 
	{
		
	}

    public static void main (String args[]) 
    {
        System.out.println("RMI server started");
 
        if (args.length < 1)
        {
        	System.err.println("usage: RmiServer <num instances>");
        	System.exit(1);
        }
        
        int numInstances = Integer.parseInt(args[0]);
        
        // Create and install a security manager
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
            System.out.println("Security manager installed.");
        } else {
            System.out.println("Security manager already exists.");
        }
 
        try { //special exception handler for registry creation
            LocateRegistry.createRegistry(1099); 
            System.out.println("java RMI registry created.");
        } catch (RemoteException e) {
            //do nothing, error means registry already exists
            System.out.println("java RMI registry already exists.");
        }
 
        try {
            //Instantiate RmiServer
            //RmiServer obj = new RmiServer("tpcc", "postgres", "sam15theboss");
 
            // Bind this object instance to the name "RmiServer"
            //Naming.rebind("//salina.csail.mit.edu/RmiServer", obj);
            //Naming.rebind("//128.30.77.25/RmiServer", obj);
            //Naming.rebind("//salina.csail.mit.edu/RmiServer", obj);
            //String termName = "Term-01
            //Naming.rebind("//128.30.77.80/RmiServer", obj);
            
        
        	String hostAddress = getHostAddress();
        	
            assert(hostAddress != null);
            System.out.println("host address: " + hostAddress);
        	
            for (int i = 0; i < numInstances; ++i)
            {            	
            	Naming.rebind("//" + hostAddress + "/RmiServer-" + i, new RmiServer("", "", ""));
            }
            
            System.out.println("PeerServer bound in registry");
        } catch (Exception e) {
            System.err.println("RMI server exception:" + e);
            e.printStackTrace();
        }
    }


	private static String getHostAddress () throws SocketException
	{
    	String hostAddress = null;
    	Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();
        while (e.hasMoreElements())
        {
           NetworkInterface ni = e.nextElement();
        	String interfaceName = ni.getName();
        	if (interfaceName.equals("eth0"))
        	{
        		Enumeration<InetAddress> e2 = ni.getInetAddresses();
        		while (e2.hasMoreElements())
        		{            			    
        			InetAddress ip = e2.nextElement();
        			System.out.println("IP address: "+ ip.toString());
        			if (!ip.isLoopbackAddress())
        				hostAddress = ip.getHostAddress();
        		}
        	}
        }
        
        return hostAddress;
	}
    
}
