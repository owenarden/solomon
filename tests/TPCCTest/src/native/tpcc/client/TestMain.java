package tpcc.client;

import java.sql.*;

import tpcc.pyxis.PyxisTransactionExecutor;

public class TestMain 
{
	
	public static void main (String [] args) throws Exception 
	{	
		if (args.length < 2)
		{
			System.err.println("usage: TestMain <0: pyxis, 1: jdbc> <# txns>");
			System.exit(1);
		}
        String dbServer = System.getProperty("remoteHost");

		int mode = Integer.parseInt(args[0]);
			
		int [] itemIDs = { 111, 23, 2, 123, 24, 57, 75, 125, 55, 19 };
		int [] rollbackItemIds = { 1000, 23, 2, 123, 24, 57, 75, 125, 55, 19 };
		
		int [] supplierWarehouseIDs = { 1, 21, 2, 3, 3, 8, 7, 6, 20, 10 };
		int [] orderQuantities = { 1, 1, 2, 4, 1, 1, 2, 2, 1, 1 };
		
		TransactionEngine exe = null;
		if (mode == 0)
			exe = new PyxisTransactionExecutor(dbServer);
		else if (mode == 1)
		{
            Class.forName(System.getProperty("driver"));
            Connection conn = DriverManager.getConnection(System.getProperty("conn"), "root", "");
            conn.setAutoCommit(false);
            exe = new RemoteJDBCEngine(conn);        
		}
		else
		{
			throw new RuntimeException("unknown executor mode: " + mode);
		}
		int numTransaction = Integer.parseInt(args[1]);
		long startTime = System.currentTimeMillis();
		
		// standard says 10% of new order should be rolled back, change if necessary
		double percentRollback = 0.1;
        int numNormalTxns = (int)(numTransaction * (1 - percentRollback));
        int numRollbackTxns = (int)(numTransaction * percentRollback);
        
		for (int i = 0; i < numNormalTxns; ++i)
		{
		    String r = exe.newOrderTransaction(1, 1, 1889, itemIDs.length, 0, itemIDs, supplierWarehouseIDs, orderQuantities);
		    System.out.println("new order: " + r);
		}
		
		for (int i = 0; i < numRollbackTxns; ++i)
        {
            String r = exe.newOrderTransaction(1, 1, 1889, rollbackItemIds.length, 0, rollbackItemIds, supplierWarehouseIDs, orderQuantities);
            System.out.println("new order: " + r);
        }
		
		long endTime = System.currentTimeMillis();	
	}
	
}
