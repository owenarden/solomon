package tpcw.servlets;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Collections;


public class ConnectionPool 
{
	//private static final int MAX_CONNECTIONS = 10;
	private static List<Connection> connectionPool = new LinkedList<Connection>();
	//private static List<Connection> connectionPool = Collections.synchronizedList(new LinkedList<Connection>());
	
	private static boolean hasInitialized = false;
	
	static
	{
		try { Class.forName("com.mysql.jdbc.Driver"); }				
		catch (ClassNotFoundException e) { throw new RuntimeException(e); }
		try {
		String dbServer = "vise4.csail.mit.edu:5310"; //System.getProperty("dbServer");
		String maxConnections1 = "1000"; //System.getProperty("maxConnections");
		System.out.println("db server: " + dbServer + " # db connections: " + maxConnections1);
		int maxConnections = Integer.parseInt(System.getProperty("maxConnections"));
		System.out.println("db server: " + dbServer + " # db connections: " + maxConnections);
		
		for (int i = 0; i < maxConnections; ++i)		
		{				
			Connection conn = DriverManager.getConnection("jdbc:mysql://" + dbServer + "/tpcw?user=root");
			conn.setAutoCommit(false);
			connectionPool.add(conn);					
		}				
		}
		catch (Exception e) { e.printStackTrace(); }
		System.out.println("initalized");
		hasInitialized = true;
	}
	
	
	
	public static Connection getConnection () throws SQLException
	{
		/*
		Connection toReturn = null;
		
		synchronized(connectionPool)
		{
			if (!hasInitialized)
			{
				try { Class.forName("com.mysql.jdbc.Driver"); }				
				catch (ClassNotFoundException e) { throw new RuntimeException(e); }
				
				String dbServer = System.getProperty("dbServer");
				String maxConnections1 = System.getProperty("maxConnections");
				System.out.println("db server: " + dbServer + " # db connections: " + maxConnections1);
				int maxConnections = Integer.parseInt(System.getProperty("maxConnections"));
				System.out.println("db server: " + dbServer + " # db connections: " + maxConnections);
				
				for (int i = 0; i < maxConnections; ++i)		
				{				
					Connection conn = DriverManager.getConnection("jdbc:mysql://" + dbServer + "/tpcw?user=root");
					conn.setAutoCommit(false);
					conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
					System.out.println("got now");
					connectionPool.add(conn);					
				}				
				
				hasInitialized = true;
			}
			else if (connectionPool.size() == 0)			
				throw new RuntimeException("not enough connections");								
						
			//System.out.println("got a connection");
			toReturn = connectionPool.get(0);
		}		
		return toReturn;	
		*/
		
		Connection toReturn = null;
		synchronized(connectionPool)
		{
			while (connectionPool.size() == 0)
			{
				try { connectionPool.wait(); }
				catch (InterruptedException e) {}
			}
			
			toReturn = connectionPool.remove(0); 
		}
		
		return toReturn;
		
		
		
		/*
		try { Class.forName("com.mysql.jdbc.Driver"); }				
		catch (ClassNotFoundException e) { throw new RuntimeException(e); }
		
		String dbServer = System.getProperty("dbServer");
		String maxConnections1 = System.getProperty("maxConnections");
		System.out.println("db server: " + dbServer + " # db connections: " + maxConnections1);
		int maxConnections = Integer.parseInt(System.getProperty("maxConnections"));
		System.out.println("db server: " + dbServer + " # db connections: " + maxConnections);
		
		Connection conn = DriverManager.getConnection("jdbc:mysql://" + dbServer + "/tpcw?user=root");
		conn.setAutoCommit(false);
		
		return conn;
		*/
		
	}
	
	public static void returnConnection (Connection conn)
	{		
		/*
		synchronized(connectionPool)
		{
			//System.out.println("return a connection");
			connectionPool.add(conn);
		}
		*/
		
		//try { conn.close(); } catch (Exception e) { e.printStackTrace(); }
		
		synchronized(connectionPool)
		{
			connectionPool.add(conn);
			connectionPool.notifyAll();
		}
	}
	
}
