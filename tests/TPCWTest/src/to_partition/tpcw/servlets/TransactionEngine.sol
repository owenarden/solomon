package tpcw.servlets;
import java.sql.SQLException;
import java.util.Vector;
import java.util.List;


public interface TransactionEngine 
{
	public List homeInteractionInteraction (boolean sessionIsNew, String C_ID, String C_FNAME, String C_LNAME, 
											     String SHOPPING_ID, String sessionId) throws SQLException;
		     
	public String newProductsInteraction (String subject, String C_ID, String SHOPPING_ID, String sessionId) throws SQLException;
	
	public String bestSellersInteraction (String subject, String C_ID, String SHOPPING_ID, String sessionId) throws SQLException;
	
	public String shoppingCartInteraction (String C_ID, String SHOPPING_ID, String addFlag, String I_ID, 
										   Vector quantities, Vector ids, String sessionId) throws SQLException;
	
	public String customerRegistrationInteraction (String C_ID, String SHOPPING_ID, String sessionId) throws SQLException;	
	
	public String buyRequestInteraction (String C_ID, String SHOPPING_ID, String RETURNING_FLAG, String UNAME, String PASSWD, 
										 String FNAME, String LNAME, String STREET1, String STREET2, String CITY, 
										 String STATE, String ZIP, String COUNTRY, String PHONE, String EMAIL, String BIRTHDATE,
										 String DATA, String sessionId) throws SQLException;	
	
	public String buyConfirmInteraction (String SHOPPING_IDstr, String C_IDstr, String CC_TYPE, String CC_NUMBERstr, String CC_NAME, 
										 String CC_EXPIRYstr, String SHIPPING, String STREET_1, String STREET_2, String CITY, 
										 String STATE, String ZIP, String COUNTRY, String sessionId) throws SQLException;
	
	public String searchRequestInteraction (String C_ID, String SHOPPING_ID, String sessionId) throws SQLException;
	
	public String executeSearchInteraction (String search_type, String search_string, String C_ID, 
										    String SHOPPING_ID, String sessionId) throws SQLException;
	
	public String orderInquiryInteraction (String C_ID, String SHOPPING_ID, String sessionId) throws SQLException;
	
	public String orderDisplayInteraction (String C_ID, String SHOPPING_ID, String UNAME, String PASSWD, 
										   String sessionId) throws SQLException;
	
	public String adminRequestInteraction (String I_IDstr, String C_ID, String SHOPPING_ID, String sessionId) throws SQLException;
	
	public String adminResponseInteraction (String C_ID, String SHOPPING_ID, String I_ID, String I_NEW_IMAGE, 
										    String I_NEW_THUMBNAIL, String I_NEW_COSTstr, String sessionId) throws SQLException;
	
	public String productDetailInteraction (String I_IDstr, String C_ID, String SHOPPING_ID, String sessionId) throws SQLException;
}
