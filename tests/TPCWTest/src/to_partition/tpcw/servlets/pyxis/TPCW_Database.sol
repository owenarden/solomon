package tpcw.servlets.pyxis;

import java.io.*;
import java.net.URL;
import java.sql.*;
import java.lang.Math.*;
import java.util.*;
import java.sql.Date;
import java.sql.Timestamp;

public class TPCW_Database {
    
     static String[] getName(int c_id, Connection conn) throws SQLException 
    {
        String[] name;
         name = (new String[2]);
        PreparedStatement get_name;
         get_name = conn.prepareStatement("SELECT c_fname,c_lname FROM customer WHERE c_id = ?");
         ;
         get_name.setInt(1, c_id);
         ;
        ResultSet rs;
         rs = get_name.executeQuery();
         ;
        boolean flat$900;
         flat$900 = rs.next();
        if ( flat$900) {
            String flat$901;
             flat$901 = rs.getString("c_fname");
             ;
             name[0] = flat$901;
            String flat$902;
             flat$902 = rs.getString("c_lname");
             ;
             name[1] = flat$902;
        } else {
        	 name[0] = null;
        	 name[1] = null;
        }
         rs.close();
         ;
         get_name.close();
         ;
         return name;
    }
    
     static Vector getNewProducts(String subject, Connection conn) throws SQLException 
    {
        Vector vec;
         vec = new Vector();
         ;
        PreparedStatement statement;
         statement = conn.prepareStatement(("SELECT i_id, i_title, a_fname, a_lname FROM item, author WHERE item.i_a_id = author.a_id AND item.i_subject = ? ORDER BY item.i_pub_date DESC,item.i_title limit 50"));
         ;
         statement.setString(1, subject);
         ;
        ResultSet rs;
         rs = statement.executeQuery();
         ;
        while ( true) 
        {
            boolean loop$9;
             loop$9 = rs.next();
            if ( loop$9) 
            {
                int i_id;
                 i_id = rs.getInt("i_id");
                 ;
                String i_title;
                 i_title = rs.getString("i_title");
                 ;
                String a_fname;
                 a_fname = rs.getString("a_fname");
                 ;
                String a_lname;
                 a_lname = rs.getString("a_lname");
                 ;
                ShortBook flat$903;
                 flat$903 = new ShortBook(i_id, i_title, a_fname, a_lname);
                 ;
                 vec.addElement(flat$903);
                 ;
            } else {
            	 break;
            }
        }
         rs.close();
         ;
         statement.close();
         ;
         return vec;
    }
    
     static Vector getBestSellers(String subject, Connection conn) throws SQLException 
    {
        Vector vec;
         vec = new Vector();
        PreparedStatement statement;
         statement = conn.prepareStatement(("SELECT i_id, i_title, a_fname, a_lname FROM item, author, order_line WHERE item.i_id = order_line.ol_i_id AND item.i_a_id = author.a_id AND order_line.ol_o_id > (SELECT MAX(o_id)-3333 FROM orders) AND item.i_subject = ? GROUP BY i_id, i_title, a_fname, a_lname ORDER BY SUM(ol_qty) DESC limit 50"));
         ;
         statement.setString(1, subject);
         ;
        ResultSet rs;
         rs = statement.executeQuery();
         ;
        while ( true) 
        {
            boolean loop$10;
             loop$10 = rs.next();
             ;
            if ( loop$10) 
            {
                int i_id;
                 i_id = rs.getInt("i_id");
                 ;
                String i_title;
                 i_title = rs.getString("i_title");
                 ;
                String a_fname;
                 a_fname = rs.getString("a_fname");
                 ;
                String a_lname;
                 a_lname = rs.getString("a_lname");
                 ;
                ShortBook flat$904;
                 flat$904 = new ShortBook(i_id, i_title, a_fname, a_lname);
                 ;
                 vec.addElement(flat$904);
                 ;
            } else {
            	 break;
            }
        }
         rs.close();
         ;
         statement.close();
         ;
         return vec;
    }
    
     static Book getBook(int i_id, Connection conn) throws SQLException 
    {
        Book book;
         book = null;
        PreparedStatement statement;
         statement = conn.prepareStatement(("SELECT * FROM item,author WHERE item.i_a_id = author.a_id AND i_id = ?"));
         ;
         statement.setInt(1, i_id);
         ;
        ResultSet rs;
         rs = statement.executeQuery();
         ;
         rs.next();
         book = new Book(rs);
         ;
         rs.close();
         ;
         statement.close();
         ;
         return book;
    }
    
     static Customer getCustomer(String UNAME, Connection conn) throws SQLException 
    {    	
        Customer cust;
         cust = null;
        PreparedStatement statement;
         statement = conn.prepareStatement(("SELECT * FROM customer, address, country WHERE customer.c_addr_id = address.addr_id AND address.addr_co_id = country.co_id AND customer.c_uname = ?"));
         ;
         statement.setString(1, UNAME);
         ;
        ResultSet rs;
         rs = statement.executeQuery();
         ;
        boolean flat$905;
         flat$905 = rs.next();
         ;
        if ( flat$905) {
        	 cust = new Customer(rs);
        	 ;
        } else {
            PrintStream flat$906;
             ;
             rs.close();
             ;
             statement.close();
             ;
            // return null;
             cust = new Customer();
            
        }
         statement.close();
         ;
         return cust;
    }
    /*
     static Vector doSubjectSearch(String search_key, Connection conn) throws SQLException 
    {
        Vector vec;
         vec = new Vector();
        PreparedStatement statement;
         statement = conn.prepareStatement(("SELECT * FROM item, author WHERE item.i_a_id = author.a_id AND item.i_subject = ? ORDER BY item.i_title limit 50"));
         ;
         statement.setString(1, search_key);
         ;
        ResultSet rs;
         rs = statement.executeQuery();
         ;
        while ( true) 
        {
            boolean loop$11;
             loop$11 = rs.next();
             ;
            if ( loop$11) 
            {
                Book flat$907;
                 flat$907 = new Book(rs);
                 ;
                 vec.addElement(flat$907);
                 ;
            } else {
            	 break;
            }
        }
         rs.close();
         ;
         statement.close();
         ;
         return vec;
    }
    
     static Vector doTitleSearch(String search_key, Connection conn) throws SQLException 
    {
        Vector vec;
         vec = new Vector();
         ;
        PreparedStatement statement;
         statement = conn.prepareStatement(("SELECT * FROM item, author WHERE item.i_a_id = author.a_id AND substring(soundex(item.i_title),0,4)=substring(soundex(?),0,4) ORDER BY item.i_title limit 50"));
         ;
        String flat$908;
         flat$908 = search_key + "%";
         statement.setString(1, flat$908);
         ;
        ResultSet rs;
         rs = statement.executeQuery();
         ;
        while ( true) {
            boolean loop$12;
             loop$12 = rs.next();
             ;
            if ( loop$12) {
                Book flat$909;
                 flat$909 = new Book(rs);
                 ;
                 vec.addElement(flat$909);
                 ;
            } else {
            	 break;
            }
        }
         rs.close();
         ;
         statement.close();
         ;
         return vec;
    }
    
     static void doAuthorSearch(String search_key, Connection conn, Book [] result) throws SQLException 
    {
        Vector vec;
         vec = new Vector();
         ;
        
        PreparedStatement statement1;
         statement1 = conn.prepareStatement(("SELECT count(*) AS CNT FROM author, item WHERE substring(soundex(author.a_lname),0,4)=substring(soundex(?),0,4) AND item.i_a_id = author.a_id limit 50"));
         ;
        String flat$9100;
         flat$9100 = search_key + "%";
         statement1.setString(1, flat$9100);
         ;
        ResultSet rs1;
         rs1 = statement1.executeQuery();
         ;
        
        int count;
         count = rs1.getInt("CNT");
         result = new Book[count];
         ;
         statement1.close();
         ;
        
        PreparedStatement statement;
         statement = conn.prepareStatement(("SELECT * FROM author, item WHERE substring(soundex(author.a_lname),0,4)=substring(soundex(?),0,4) AND item.i_a_id = author.a_id ORDER BY item.i_title limit 50"));
         ;
        String flat$910;
         flat$910 = search_key + "%";
         statement.setString(1, flat$910);
         ;
        ResultSet rs;
         rs = statement.executeQuery();
         ;
        
        int i;
         i = 0;
        while ( true) 
        {
            boolean loop$13;
             loop$13 = rs.next();
            if ( loop$13) 
            {
                Book flat$911;
                 flat$911 = new Book(rs);
                 ;
                // vec.addElement(flat$911);
                 result[i] = flat$911;
                 ++i;
                 ;
            } else {
            	 break;
            }
        }
         rs.close();
         ;
         statement.close();
         ;
        // return vec;
        
    }
    */
    
     static void getRelated(int i_id, Vector i_id_vec, Vector i_thumbnail_vec, Connection conn)
          throws SQLException 
    {
        PreparedStatement statement;
         statement = conn.prepareStatement(("SELECT J.i_id,J.i_thumbnail from item I, item J where (I.i_related1 = J.i_id or I.i_related2 = J.i_id or I.i_related3 = J.i_id or I.i_related4 = J.i_id or I.i_related5 = J.i_id) and I.i_id = ?"));
         ;
         statement.setInt(1, i_id);
         ;
        ResultSet rs;
         rs = statement.executeQuery();
         ;
         i_id_vec.removeAllElements();
         ;
         i_thumbnail_vec.removeAllElements();
         ;
        while ( true) 
        {
            boolean loop$14;
             loop$14 = rs.next();
             ;
            if ( loop$14) {
                int flat$912;
                 flat$912 = rs.getInt(1);
                 ;
                Integer flat$913;
                 flat$913 = new Integer(flat$912);
                 ;
                 i_id_vec.addElement(flat$913);
                 ;
                String flat$914;
                 flat$914 = rs.getString(2);
                 ;
                 i_thumbnail_vec.addElement(flat$914);
                 ;
            } else {
            	 break;
            }
        }
         rs.close();
         ;
         statement.close();
    }
    
     static void adminUpdate(int i_id, double cost, String image, String thumbnail, Connection conn)
          throws SQLException 
    {
        PreparedStatement statement;
         statement = conn.prepareStatement(("UPDATE item SET i_cost = ?, i_image = ?, i_thumbnail = ?, i_pub_date = CURRENT_DATE WHERE i_id = ?"));
         ;
         statement.setDouble(1, cost);
         ;
         statement.setString(2, image);
         ;
         statement.setString(3, thumbnail);
         ;
         statement.setInt(4, i_id);
         ;
         statement.executeUpdate();
         ;
         statement.close();
         ;
        PreparedStatement related;
         related = conn.prepareStatement(("SELECT ol_i_id FROM orders, order_line WHERE orders.o_id = order_line.ol_o_id AND NOT (order_line.ol_i_id = ?) AND orders.o_c_id IN (SELECT o_c_id FROM orders, order_line WHERE orders.o_id = order_line.ol_o_id AND orders.o_id > (SELECT MAX(o_id)-10000 FROM orders) AND order_line.ol_i_id = ?) GROUP BY ol_i_id ORDER BY SUM(ol_qty) DESC limit 5"));
         ;
         related.setInt(1, i_id);
         ;
         related.setInt(2, i_id);
         ;
        ResultSet rs;
         rs = related.executeQuery();
         ;
        int[] related_items;
         related_items = (new int[5]);
         ;
        int counter;
         counter = 0;
        int last;
         last = 0;
        while ( true) 
        {
            boolean loop$15;
             loop$15 = rs.next();
             ;
            if ( loop$15) 
            {
            	 last = rs.getInt(1);
            	 ;
            	 related_items[counter] = last;
                {  counter = counter + 1; }
                 ;
            } else {
            	 break;
            }
        }
        {
            int i;
             i = counter;
            boolean loop$16;
             loop$16 = false;
            while ( true) 
            {
                if ( loop$16) {
                    {  i = i + 1; }
                     ;
                }
                 loop$16 = i < 5;
                if ( loop$16) {
                    {  last = last + 1; }
                     ;
                     related_items[i] = last;
                } else {
                	 break;
                }
            }
        }
         rs.close();
         ;
         related.close();
         ;
        {
        	 statement = conn.prepareStatement(("UPDATE item SET i_related1 = ?, i_related2 = ?, i_related3 = ?, i_related4 = ?, i_related5 = ? WHERE i_id = ?"));
        	 ;
        	int flat$915;
             flat$915 = related_items[0];
             statement.setInt(1, flat$915);
             ;
            int flat$916;
             flat$916 = related_items[1];            
             statement.setInt(2, flat$916);
             ;
            int flat$917;
             flat$917 = related_items[2];            
             statement.setInt(3, flat$917);
             ;
            int flat$918;
             flat$918 = related_items[3];
             statement.setInt(4, flat$918);
             ;
            int flat$919;
             flat$919 = related_items[4];
             statement.setInt(5, flat$919);
             ;
             statement.setInt(6, i_id);
             ;
             statement.executeUpdate();
             ;
        }
         statement.close();
         ;
    }
    
     static String GetUserName(int C_ID, Connection conn) throws SQLException 
    {
        String u_name;
         u_name = null;
        PreparedStatement get_user_name;
         get_user_name = conn.prepareStatement("SELECT c_uname FROM customer WHERE c_id = ?");
         ;
         get_user_name.setInt(1, C_ID);
         ;
        ResultSet rs;
         rs = get_user_name.executeQuery();
         ;
        boolean flat$920;
         flat$920 = rs.next();
         ;
        if ( flat$920) 
        { 
        	 u_name = rs.getString("c_uname"); 
        	 ;
        }
         rs.close();
         ;
         get_user_name.close();
         ;
         return u_name;
    }
    
     static String GetPassword(String C_UNAME, Connection conn) throws SQLException 
    {
        String passwd;
         passwd = null;
        PreparedStatement get_passwd;
         get_passwd = conn.prepareStatement("SELECT c_passwd FROM customer WHERE c_uname = ?");
         ;
         get_passwd.setString(1, C_UNAME);
         ;
        ResultSet rs;
         rs = get_passwd.executeQuery();
         ;
         rs.next();
         ;
         passwd = rs.getString("c_passwd");
         ;
         rs.close();
         ;
         get_passwd.close();
         ;
         return passwd;
    }
    
    private static int getRelated1(int I_ID, Connection con) throws SQLException 
    {
        int related1;
         related1 = -1;
        PreparedStatement statement;
         statement = con.prepareStatement("SELECT i_related1 FROM item where i_id = ?");
         ;
         statement.setInt(1, I_ID);
         ;
        ResultSet rs;
         rs = statement.executeQuery();
         ;
        boolean result;
         result = rs.next();
         ;
        boolean flat$921;
         flat$921 = !result;
        if ( flat$921) {
             ;
        }
        else
        {
        	 related1 = rs.getInt(1);
        	 ;
        }
        
         rs.close();
         ;
         statement.close();
         ;
         return related1;
    }
    
     static Order GetMostRecentOrder(String c_uname, Vector order_lines, Connection conn) throws SQLException 
    {
    	 order_lines.removeAllElements();
    	 ;
        int order_id;
        Order order;
        {
            PreparedStatement get_most_recent_order_id;
             get_most_recent_order_id =
              conn.prepareStatement(("SELECT o_id FROM customer, orders WHERE customer.c_id = orders.o_c_id AND c_uname = ? ORDER BY o_date, orders.o_id DESC limit 1"));
             ;
             get_most_recent_order_id.setString(1, c_uname);
             ;
            ResultSet rs;
             rs = get_most_recent_order_id.executeQuery();
             ;
            boolean flat$924;
             flat$924 = rs.next();
             ;
            if ( flat$924) {
            	 order_id = rs.getInt("o_id");
            	 ;
            } else {
            	 rs.close();
            	 ;
            	 get_most_recent_order_id.close();
            	 ;
            	 return null;
            }
             rs.close();
             ;
             get_most_recent_order_id.close();
             ;
        }
        {
            PreparedStatement get_order;
             get_order = conn.prepareStatement(("SELECT orders.*, customer.*, cc_xacts.cx_type, ship.addr_street1 AS ship_addr_street1, ship.addr_street2 AS ship_addr_street2, ship.addr_state AS ship_addr_state, ship.addr_zip AS ship_addr_zip, ship_co.co_name AS ship_co_name, bill.addr_street1 AS bill_addr_street1, bill.addr_street2 AS bill_addr_street2, bill.addr_state AS bill_addr_state, bill.addr_zip AS bill_addr_zip, bill_co.co_name AS bill_co_name FROM customer, orders, cc_xacts, address AS ship, country AS ship_co, address AS bill, country AS bill_co WHERE orders.o_id = ? AND cx_o_id = orders.o_id AND customer.c_id = orders.o_c_id AND orders.o_bill_addr_id = bill.addr_id AND bill.addr_co_id = bill_co.co_id AND orders.o_ship_addr_id = ship.addr_id AND ship.addr_co_id = ship_co.co_id AND orders.o_c_id = customer.c_id"));
             ;
             get_order.setInt(1, order_id);
             ;
            ResultSet rs2;
             rs2 = get_order.executeQuery();
             ;
            boolean flat$925;
             flat$925 = rs2.next();
             ;
            boolean flat$926;
             flat$926 = !flat$925;
            if ( flat$926) {
            	 rs2.close();
            	 ;
            	 return null;
            }
             order = new Order(rs2);
             ;
             rs2.close();
             ;
             get_order.close();
             ;
        }
        {
            PreparedStatement get_order_lines;
             get_order_lines = conn.prepareStatement(("SELECT * FROM order_line, item WHERE ol_o_id = ? AND ol_i_id = i_id"));
             ;
             get_order_lines.setInt(1, order_id);
             ;
            ResultSet rs3;
             rs3 = get_order_lines.executeQuery();
             ;
            while ( true) {
                boolean loop$17;
                 loop$17 = rs3.next();
                 ;
                if ( loop$17) {
                    OrderLine flat$927;
                     flat$927 = new OrderLine(rs3);
                     ;
                     order_lines.addElement(flat$927);
                     ;
                } else {
                	 break;
                }
            }
             rs3.close();
             ;
             get_order_lines.close();
             ;
        }
         return order;
    }
    
     static int createEmptyCart(Connection conn) throws SQLException 
    {
        int SHOPPING_ID;
         SHOPPING_ID = 0;
        PreparedStatement get_next_id;
         get_next_id = conn.prepareStatement("SELECT COUNT(*) FROM shopping_cart");
         ;
        ResultSet rs;
         rs = get_next_id.executeQuery();
         ;
         rs.next();
         ;
         SHOPPING_ID = rs.getInt(1);
         ;
         rs.close();
         ;
        PreparedStatement insert_cart;
         insert_cart = conn.prepareStatement(("INSERT into shopping_cart (sc_id, sc_time) VALUES (?, CURRENT_TIMESTAMP)"));
         ;
         insert_cart.setInt(1, SHOPPING_ID);
         ;
         insert_cart.executeUpdate();
         ;
         get_next_id.close();
         ;
         return SHOPPING_ID;
    }
    
     static Cart doCart(int SHOPPING_ID, Integer I_ID, Vector ids, Vector quantities, Connection conn)
          throws SQLException 
    {
        Cart cart;
         cart = null;
        boolean flat$928;
         flat$928 = I_ID != null;
        if ( flat$928) {
            int flat$929;
             flat$929 = I_ID.intValue();
             ;
             TPCW_Database.addItem(conn, SHOPPING_ID, flat$929);
             ;
        }
         TPCW_Database.refreshCart(conn, SHOPPING_ID, ids, quantities);
         ;
         TPCW_Database.addRandomItemToCartIfNecessary(conn, SHOPPING_ID);
         ;
         TPCW_Database.resetCartTime(conn, SHOPPING_ID);
         ;
         cart = TPCW_Database.getCart(conn, SHOPPING_ID, 0.0);
         ;
         return cart;
    }
    
    private static void addItem(Connection con, int SHOPPING_ID, int I_ID) throws SQLException 
    {
        PreparedStatement find_entry;
         find_entry = con.prepareStatement(("SELECT scl_qty FROM shopping_cart_line WHERE scl_sc_id = ? AND scl_i_id = ?"));
         ;
         find_entry.setInt(1, SHOPPING_ID);
         ;
         find_entry.setInt(2, I_ID);
         ;
        ResultSet rs;
         rs = find_entry.executeQuery();
         ;
        boolean flat$930;
         flat$930 = rs.next();
         ;
        if ( flat$930) {
            int currqty;
             currqty = rs.getInt("scl_qty");
             ;
             currqty = currqty + 1;
            PreparedStatement update_qty;
             update_qty = con.prepareStatement(("UPDATE shopping_cart_line SET scl_qty = ? WHERE scl_sc_id = ? AND scl_i_id = ?"));
             ;
             update_qty.setInt(1, currqty);
             ;
             update_qty.setInt(2, SHOPPING_ID);
             ;
             update_qty.setInt(3, I_ID);
             ;
             update_qty.executeUpdate();
             ;
             update_qty.close();
             ;
        } else {
            PreparedStatement put_line;
             put_line = con.prepareStatement(("INSERT into shopping_cart_line (scl_sc_id, scl_qty, scl_i_id) VALUES (?,?,?)"));
             ;
             put_line.setInt(1, SHOPPING_ID);
             ;
             put_line.setInt(2, 1);
             ;
             put_line.setInt(3, I_ID);
             ;
             put_line.executeUpdate();
             ;
             put_line.close();
             ;
        }
         rs.close();
         ;
         find_entry.close();
         ;
    }
    
    private static void refreshCart(Connection con, int SHOPPING_ID, Vector ids, Vector quantities)
          throws SQLException {
        int i;
        {
        	 i = 0;
            boolean loop$18;
             loop$18 = false;
            while ( true) {
                if ( loop$18) {
                    {  i = i + 1; }
                     ;
                }
                int flat$931;
                 flat$931 = ids.size();
                 loop$18 = i < flat$931;
                if ( loop$18) {
                    Object flat$932;
                     flat$932 = ids.elementAt(i);
                     ;
                    String I_IDstr;
                     I_IDstr = (String) flat$932;
                    Object flat$933;
                     flat$933 = quantities.elementAt(i);
                     ;
                    String QTYstr;
                     QTYstr = (String) flat$933;
                    int I_ID;
                     I_ID = Integer.parseInt(I_IDstr);
                     ;
                    int QTY;
                     QTY = Integer.parseInt(QTYstr);
                     ;
                    boolean flat$934;
                     flat$934 = QTY == 0;
                    if ( flat$934) {
                        PreparedStatement statement;
                         statement =
                          con.prepareStatement(("DELETE FROM shopping_cart_line WHERE scl_sc_id = ? AND scl_i_id = ?"));
                         ;
                         statement.setInt(1, SHOPPING_ID);
                         ;
                         statement.setInt(2, I_ID);
                         ;
                         statement.executeUpdate();
                         ;
                         statement.close();
                         ;
                    }
                    else {
                        PreparedStatement statement;
                         statement =
                          con.prepareStatement(("UPDATE shopping_cart_line SET scl_qty = ? WHERE scl_sc_id = ? AND scl_i_id = ?"));
                         ;
                          statement.setInt(1, QTY);
                         ;
                         statement.setInt(2, SHOPPING_ID);
                         ;
                         statement.setInt(3, I_ID);
                         ;
                         statement.executeUpdate();
                         ;
                         statement.close();
                         ;
                    }
                }
                else {
                	 break;
                }
            }
        }
    }
    
    private static void addRandomItemToCartIfNecessary(Connection con, int SHOPPING_ID) throws SQLException {
        int related_item;
         related_item = 0;
        PreparedStatement get_cart;
         get_cart = con.prepareStatement("SELECT COUNT(*) from shopping_cart_line where scl_sc_id = ?");
         ;
         get_cart.setInt(1, SHOPPING_ID);
         ;
        ResultSet rs;
         rs = get_cart.executeQuery();
         ;
         rs.next();
         ;
        int flat$935;
         flat$935 = rs.getInt(1);
         ;
        boolean flat$936;
         flat$936 = flat$935 == 0;
        if ( flat$936) {
            int rand_id;
             rand_id = TPCW_Util.getRandomI_ID();
             ;
             related_item = TPCW_Database.getRelated1(rand_id, con);
             ;
             TPCW_Database.addItem(con, SHOPPING_ID, related_item);
             ;
        }
         rs.close();
         ;
         get_cart.close();
         ;
    }
    
    private static void resetCartTime(Connection con, int SHOPPING_ID) throws SQLException {
        PreparedStatement statement;
         statement = con.prepareStatement(("UPDATE shopping_cart SET sc_time = CURRENT_TIMESTAMP WHERE sc_id = ?"));
         ;
         statement.setInt(1, SHOPPING_ID);
         ;
         statement.executeUpdate();
         ;
         statement.close();
         ;
    }
    
     static Cart getCart(int SHOPPING_ID, double c_discount, Connection conn) throws SQLException {
        Cart mycart;
         mycart = null;
         mycart = TPCW_Database.getCart(conn, SHOPPING_ID, c_discount);
         ;
         return mycart;
    }
    
    private static Cart getCart(Connection con, int SHOPPING_ID, double c_discount) throws SQLException {
        Cart mycart;
         mycart = null;
        PreparedStatement get_cart;
         get_cart = con.prepareStatement(("SELECT * FROM shopping_cart_line, item WHERE scl_i_id = item.i_id AND scl_sc_id = ?"));
         ;
         get_cart.setInt(1, SHOPPING_ID);
         ;
        ResultSet rs;
         rs = get_cart.executeQuery();
         ;
         mycart = new Cart(rs, c_discount);
         ;
         rs.close();
         ;
         get_cart.close();
         ;
         return mycart;
    }
    
     static void refreshSession(int C_ID, Connection conn) throws SQLException {
        PreparedStatement updateLogin;
         updateLogin = conn.prepareStatement(("UPDATE customer SET c_login = NOW(), c_expiration = (CURRENT_TIMESTAMP + INTERVAL 2 HOUR) WHERE c_id = ?"));
         ;
         updateLogin.setInt(1, C_ID);
         ;
         updateLogin.executeUpdate();
         ;
         updateLogin.close();
         ;
    }
    
     static Customer createNewCustomer(Customer cust, Connection conn) throws SQLException {
        double flat$937;
         flat$937 = Math.random();
         ;
        double flat$938;
         flat$938 = flat$937 * 51;
        int flat$939;
         flat$939 = (int) flat$938;
         cust.c_discount = flat$939;
         cust.c_balance = 0.0;
         cust.c_ytd_pmt = 0.0;
        long flat$940;
         flat$940 = System.currentTimeMillis();
         ;
        Date flat$941;
         flat$941 = new Date(flat$940);
         ;
         cust.c_last_visit = flat$941;
        long flat$942;
         flat$942 = System.currentTimeMillis();
         ;
        Date flat$943;
         flat$943 = new Date(flat$942);
         ;
         cust.c_since = flat$943;
        long flat$944;
         flat$944 = System.currentTimeMillis();
         ;
        Date flat$945;
         flat$945 = new Date(flat$944);
         ;
         cust.c_login = flat$945;
        long flat$946;
         flat$946 = System.currentTimeMillis();
         ;
        long flat$947;
         flat$947 = flat$946 + 7200000;
        Date flat$948;
         flat$948 = new Date(flat$947);
         ;
         cust.c_expiration = flat$948;
        PreparedStatement insert_customer_row;
         insert_customer_row = conn.prepareStatement(("INSERT into customer (c_id, c_uname, c_passwd, c_fname, c_lname, c_addr_id, c_phone, c_email, c_since, c_last_login, c_login, c_expiration, c_discount, c_balance, c_ytd_pmt, c_birthdate, c_data) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"));
         ;
        String flat$949;
         flat$949 = cust.c_fname;
         insert_customer_row.setString(4, flat$949);
         ;
        String flat$950;
         flat$950 = cust.c_lname;
         insert_customer_row.setString(5, flat$950);
         ;
        String flat$951;
         flat$951 = cust.c_phone;
         insert_customer_row.setString(7, flat$951);
         ;
        String flat$952;
         flat$952 = cust.c_email;
         insert_customer_row.setString(8, flat$952);
         ;
        java.util.Date flat$953;
         flat$953 = cust.c_since;
        long flat$954;
         flat$954 = flat$953.getTime();
         ;
        Date flat$955;
         flat$955 = new Date(flat$954);
         insert_customer_row.setDate(9, flat$955);
         ;
        java.util.Date flat$956;
         flat$956 = cust.c_last_visit;
        long flat$957;
         flat$957 = flat$956.getTime();
         ;
        Date flat$958;
         flat$958 = new Date(flat$957);
         insert_customer_row.setDate(10, flat$958);
         ;
        java.util.Date flat$959;
         flat$959 = cust.c_login;
        long flat$960;
         flat$960 = flat$959.getTime();
         ;
        Date flat$961;
         flat$961 = new Date(flat$960);
         insert_customer_row.setDate(11, flat$961);
         ;
        java.util.Date flat$962;
         flat$962 = cust.c_expiration;
        long flat$963;
         flat$963 = flat$962.getTime();
         ;
        Date flat$964;
         flat$964 = new Date(flat$963);
         insert_customer_row.setDate(12, flat$964);
         ;
        double flat$965;
         flat$965 = cust.c_discount;
         insert_customer_row.setDouble(13, flat$965);
         ;
        double flat$966;
         flat$966 = cust.c_balance;
         insert_customer_row.setDouble(14, flat$966);
         ;
        double flat$967;
         flat$967 = cust.c_ytd_pmt;
         insert_customer_row.setDouble(15, flat$967);
         ;
        java.util.Date flat$968;
         flat$968 = cust.c_birthdate;
        long flat$969;
         flat$969 = flat$968.getTime();
         ;
        Date flat$970;
         flat$970 = new Date(flat$969);
         ;
         insert_customer_row.setDate(16, flat$970);
         ;
        String flat$971;
         flat$971 = cust.c_data;
         insert_customer_row.setString(17, flat$971);
         ;
        String flat$972;
        // flat$972 = cust.addr_street1;
         flat$972 = "test";
        String flat$973;
        // flat$973 = cust.addr_street2;
         flat$973 = "test";
        String flat$974;
        // flat$974 = cust.addr_city;
         flat$974 = "test";
        String flat$975;
        // flat$975 = cust.addr_state;
         flat$975 = "test";
        String flat$976;
        // flat$976 = cust.addr_zip;
         flat$976 = "test";
        String flat$977;
        // flat$977 = cust.co_name;
         flat$977 = "test";
        int flat$978;
         flat$978 = TPCW_Database.enterAddress(conn, flat$972, flat$973, flat$974, flat$975, flat$976, flat$977);
         ;
         cust.addr_id = flat$978;
        PreparedStatement get_max_id;
         get_max_id = conn.prepareStatement("SELECT max(c_id) FROM customer");
         ;
        ResultSet rs;
         rs = get_max_id.executeQuery();
         ;
         rs.next();
         ;
        int flat$979;
         flat$979 = rs.getInt(1);
         ;
         cust.c_id = flat$979;
         rs.close();
         ;
        int flat$980;
         flat$980 = cust.c_id;
        int flat$981;
         flat$981 = flat$980 + 1;
         cust.c_id = flat$981;
        int flat$982;
         flat$982 = cust.c_id;
        String flat$983;
         flat$983 = TPCW_Util.DigSyl(flat$982, 0);
         ;
         cust.c_uname = flat$983;
        String flat$984;
         flat$984 = cust.c_uname;
        String flat$985;
         flat$985 = flat$984.toLowerCase();
         ;
         cust.c_passwd = flat$985;
        int flat$986;
         flat$986 = cust.c_id;
         insert_customer_row.setInt(1, flat$986);
         ;
        String flat$987;
         flat$987 = cust.c_uname;
         insert_customer_row.setString(2, flat$987);
         ;
        String flat$988;
         flat$988 = cust.c_passwd;
         insert_customer_row.setString(3, flat$988);
         ;
        int flat$989;
         flat$989 = cust.addr_id;
         insert_customer_row.setInt(6, flat$989);
         ;
         insert_customer_row.executeUpdate();
         ;
         insert_customer_row.close();
         ;
         get_max_id.close();
         ;
         return cust;
    }
    
     static BuyConfirmResult doBuyConfirm(int shopping_id, int customer_id, String cc_type, long cc_number,
                                                String cc_name, Date cc_expiry, String shipping, Connection conn)
          throws SQLException {
        BuyConfirmResult result;
         result = new BuyConfirmResult();
         ;
        double c_discount;
         c_discount = TPCW_Database.getCDiscount(conn, customer_id);
         ;
        Cart flat$990;
         flat$990 = TPCW_Database.getCart(conn, shopping_id, c_discount);
         ;
         result.cart = flat$990;
        int ship_addr_id;
         ship_addr_id = TPCW_Database.getCAddr(conn, customer_id);
         ;
        Cart flat$991;
         flat$991 = result.cart;
        int flat$992;
         flat$992 = TPCW_Database.enterOrder(conn, customer_id, flat$991, ship_addr_id, shipping, c_discount);
         ;
         result.order_id = flat$992;
        int flat$993;
         flat$993 = result.order_id;
        Cart flat$994;
         flat$994 = result.cart;
        double flat$995;
         flat$995 = flat$994.SC_TOTAL;
         TPCW_Database.enterCCXact(conn, flat$993, cc_type, cc_number, cc_name, cc_expiry, flat$995, ship_addr_id);
         ;
         TPCW_Database.clearCart(conn, shopping_id);
         ;
         return result;
    }
    
     static BuyConfirmResult doBuyConfirm(int shopping_id, int customer_id, String cc_type, long cc_number,
                                                String cc_name, Date cc_expiry, String shipping, String street_1,
                                                String street_2, String city, String addressState, String zip, String country,
                                                Connection conn)
          throws SQLException {
        BuyConfirmResult result;
         result = new BuyConfirmResult();
         ;
        double c_discount;
         c_discount = TPCW_Database.getCDiscount(conn, customer_id);
         ;
        Cart flat$996;
         flat$996 = TPCW_Database.getCart(conn, shopping_id, c_discount);
         ;
         result.cart = flat$996;
        int ship_addr_id;
         ship_addr_id = TPCW_Database.enterAddress(conn, street_1, street_2, city, addressState, zip, country);
         ;
        Cart flat$997;
         flat$997 = result.cart;
        int flat$998;
         flat$998 = TPCW_Database.enterOrder(conn, customer_id, flat$997, ship_addr_id, shipping, c_discount);
         ;
         result.order_id = flat$998;
        int flat$999;
         flat$999 = result.order_id;
        Cart flat$1000;
         flat$1000 = result.cart;
        double flat$1001;
         flat$1001 = flat$1000.SC_TOTAL;
         TPCW_Database.enterCCXact(conn, flat$999, cc_type, cc_number, cc_name, cc_expiry, flat$1001, ship_addr_id);
         ;
         TPCW_Database.clearCart(conn, shopping_id);
         ;
         return result;
    }
    
    private static double getCDiscount(Connection con, int c_id) throws SQLException {
        double c_discount;
         c_discount = 0.0;
        PreparedStatement statement;
         statement = con.prepareStatement("SELECT c_discount FROM customer WHERE customer.c_id = ?");
         ;
         statement.setInt(1, c_id);
         ;
        ResultSet rs;
         rs = statement.executeQuery();
         ;
         rs.next();
         ;
         c_discount = rs.getDouble(1);
         ;
         rs.close();
         ;
         statement.close();
         ;
         return c_discount;
    }
    
    private static int getCAddrID(Connection con, int c_id) throws SQLException {
        int c_addr_id;
         c_addr_id = 0;
        PreparedStatement statement;
         statement = con.prepareStatement("SELECT c_addr_id FROM customer WHERE customer.c_id = ?");
         ;
         statement.setInt(1, c_id);
         ;
        ResultSet rs;
         rs = statement.executeQuery();
         ;
         rs.next();
         ;
         c_addr_id = rs.getInt(1);
         ;
         rs.close();
         ;
         statement.close();
         ;
         return c_addr_id;
    }
    
    private static int getCAddr(Connection con, int c_id) throws SQLException {
        int c_addr_id;
         c_addr_id = 0;
        PreparedStatement statement;
         statement = con.prepareStatement("SELECT c_addr_id FROM customer WHERE customer.c_id = ?");
         ;
         statement.setInt(1, c_id);
         ;
        ResultSet rs;
         rs = statement.executeQuery();
         ;
         rs.next();
         ;
         c_addr_id = rs.getInt(1);
         ;
         rs.close();
         ;
         statement.close();
         ;
         return c_addr_id;
    }
    
    private static void enterCCXact(Connection con, int o_id, String cc_type, long cc_number, String cc_name,
                                    Date cc_expiry, double total, int ship_addr_id)
          throws SQLException {
        int flat$1002;
         flat$1002 = cc_type.length();
         ;
        boolean flat$1003;
        String cc_type$0, cc_name$0;
         flat$1003 = flat$1002 > 10;
        if ( flat$1003) 
        {  cc_type$0 = cc_type.substring(0, 10);
           ; }
        int flat$1004;
         flat$1004 = cc_name.length();
         ;
        boolean flat$1005;
         flat$1005 = flat$1004 > 30;
        if ( flat$1005) 
        { cc_name$0 = cc_name.substring(0, 30);
         ;
        }
        else {
            cc_name$0 = cc_name;
        }
        PreparedStatement statement;
         statement = con.prepareStatement(("INSERT into cc_xacts (cx_o_id, cx_type, cx_num, cx_name, cx_expire, cx_xact_amt, cx_xact_date, cx_co_id) VALUES (?, ?, ?, ?, ?, ?, CURRENT_DATE, (SELECT co_id FROM address, country WHERE addr_id = ? AND addr_co_id = co_id))"));
         ;
         statement.setInt(1, o_id);
         ;
         statement.setString(2, cc_type);
         ;
         statement.setLong(3, cc_number);
         ;
         statement.setString(4, cc_name);
         ;
         statement.setDate(5, cc_expiry);
         ;
         statement.setDouble(6, total);
         ;
         statement.setInt(7, ship_addr_id);
         ;
         statement.executeUpdate();
         ;
         statement.close();
         ;
    }
    
     static void clearCart(Connection con, int shopping_id) throws SQLException {
        PreparedStatement statement;
         statement = con.prepareStatement("DELETE FROM shopping_cart_line WHERE scl_sc_id = ?");
         ;
         statement.setInt(1, shopping_id);
         ;
         statement.executeUpdate();
         ;
         statement.close();
         ;
    }
    
    private static int enterAddress(Connection con, String street1, String street2, String city, String addressState,
                                    String zip, String country)
          throws SQLException {
        int addr_id;
         addr_id = 0;
        PreparedStatement get_co_id;
         get_co_id = con.prepareStatement("SELECT co_id FROM country WHERE co_name = ?");
         ;
         get_co_id.setString(1, country);
         ;
        ResultSet rs;
         rs = get_co_id.executeQuery();
         ;
        
        boolean b1;
         b1 = rs.next();
         ;
        int addr_co_id;
        if ( b1)
        {
        	 addr_co_id = rs.getInt("co_id");
        }
        else
        {
        	 addr_co_id = 1;
        }
         ;
         rs.close();
         ;
         get_co_id.close();
         ;
        PreparedStatement match_address;
         match_address = con.prepareStatement(("SELECT addr_id FROM address WHERE addr_street1 = ? AND addr_street2 = ? AND addr_city = ? AND addr_state = ? AND addr_zip = ? AND addr_co_id = ?"));
         ;
         match_address.setString(1, street1);
         ;
         match_address.setString(2, street2);
         ;
         match_address.setString(3, city);
         ;
         match_address.setString(4, addressState);
         ;
         match_address.setString(5, zip);
         ;
         match_address.setInt(6, addr_co_id);
         ;
         rs = match_address.executeQuery();
         ;
        boolean flat$1006;
         flat$1006 = rs.next();
         ;
        boolean flat$1007;
         flat$1007 = !flat$1006;
         ;
        if ( flat$1007) {
            PreparedStatement insert_address_row;
             insert_address_row = con.prepareStatement(("INSERT into address (addr_id, addr_street1, addr_street2, addr_city, addr_state, addr_zip, addr_co_id) VALUES (?, ?, ?, ?, ?, ?, ?)"));
             ;
             insert_address_row.setString(2, street1);
             ;
             insert_address_row.setString(3, street2);
             ;
             insert_address_row.setString(4, city);
             ;
             insert_address_row.setString(5, addressState);
             ;
             insert_address_row.setString(6, zip);
             ;
             insert_address_row.setInt(7, addr_co_id);
             ;
            PreparedStatement get_max_addr_id;
             get_max_addr_id = con.prepareStatement("SELECT max(addr_id) FROM address");
             ;
            ResultSet rs2;
             rs2 = get_max_addr_id.executeQuery();
             ;
             rs2.next();
             ;
            int flat$1008;
             flat$1008 = rs2.getInt(1);
             ;
             addr_id = flat$1008 + 1;
             rs2.close();
             ;
             insert_address_row.setInt(1, addr_id);
             ;
             insert_address_row.executeUpdate();
             ;
             get_max_addr_id.close();
             ;
             insert_address_row.close();
             ;
        } else {
        	 addr_id = rs.getInt("addr_id");
        	 ;
        }
         match_address.close();
         ;
         rs.close();
         ;
         return addr_id;
    }
    
    private static int enterOrder(Connection conn, int customer_id, Cart cart, int ship_addr_id, String shipping,
                                  double c_discount)
          throws SQLException {
        int o_id;
         o_id = 0;
        PreparedStatement insert_row;
          insert_row = conn.prepareStatement(("INSERT into orders (o_id, o_c_id, o_date, o_sub_total, o_tax, o_total, o_ship_type, o_ship_date, o_bill_addr_id, o_ship_addr_id, o_status) VALUES (?, ?, CURRENT_DATE, ?, 8.25, ?, ?, CURRENT_DATE + INTERVAL ? DAY, ?, ?, \'Pending\')"));
         ;
         insert_row.setInt(2, customer_id);
         ;
        double flat$1009;
         flat$1009 = cart.SC_SUB_TOTAL;
         insert_row.setDouble(3, flat$1009);
         ;
        double flat$1010;
         flat$1010 = cart.SC_TOTAL;
         insert_row.setDouble(4, flat$1010);
         ;
         insert_row.setString(5, shipping);
         ;
        int flat$1011;
         flat$1011 = TPCW_Util.getRandom(7);
         ;
         insert_row.setInt(6, flat$1011);
         ;
        int flat$1012;
         flat$1012 = TPCW_Database.getCAddrID(conn, customer_id);
         ;
         insert_row.setInt(7, flat$1012);
         ;
         insert_row.setInt(8, ship_addr_id);
         ;
        PreparedStatement get_max_id;
         get_max_id = conn.prepareStatement("SELECT count(o_id) FROM orders");
         ;
        ResultSet rs;
         rs = get_max_id.executeQuery();
         ;
         rs.next();
         ;
        int flat$1013;
         flat$1013 = rs.getInt(1);
         ;
         o_id = flat$1013 + 1;
         rs.close();
         insert_row.setInt(1, o_id);
         ;
         insert_row.executeUpdate();
         ;
         get_max_id.close();
         ;
         insert_row.close();
         ;
        Vector flat$1014;
         flat$1014 = cart.lines;
		int size = flat$1014.size();
		int i = 0;
        int counter;
         counter = 0;
        while ( true) {
            boolean loop$19;
             loop$19 = i < size;
             ;
            if ( loop$19) {
                Object flat$1015;
                 flat$1015 = flat$1014.elementAt(i++);
                 ;
                CartLine cart_line;
                 cart_line = (CartLine) flat$1015;
                int flat$1016;
                 flat$1016 = cart_line.scl_i_id;
                int flat$1017;
                 flat$1017 = cart_line.scl_qty;
                String flat$1018;
                 flat$1018 = TPCW_Util.getRandomString(20, 100);
                 ;
                 TPCW_Database.addOrderLine(conn, counter, o_id, flat$1016, flat$1017, c_discount, flat$1018);
                 ;
                {  counter = counter + 1; }
                 ;
                int flat$1019;
                 flat$1019 = cart_line.scl_i_id;
                int stock;
                 stock = TPCW_Database.getStock(conn, flat$1019);
                 ;
                int flat$1020;
                 flat$1020 = cart_line.scl_qty;
                int flat$1021;
                 flat$1021 = stock - flat$1020;
                boolean flat$1022;
                 flat$1022 = flat$1021 < 10;
                if ( flat$1022) {
                    int flat$1023;
                     flat$1023 = cart_line.scl_i_id;
                    int flat$1024;
                     flat$1024 = cart_line.scl_qty;
                    int flat$1025;
                     flat$1025 = stock - flat$1024;
                    int flat$1026;
                     flat$1026 = flat$1025 + 21;
                     TPCW_Database.setStock(conn, flat$1023, flat$1026);
                     ;
                } else {
                    int flat$1027;
                     flat$1027 = cart_line.scl_i_id;
                    int flat$1028;
                     flat$1028 = cart_line.scl_qty;
                    int flat$1029;
                     flat$1029 = stock - flat$1028;
                     TPCW_Database.setStock(conn, flat$1027, flat$1029);
                     ;
                }
            } else {
            	 break;
            }
        }
         return o_id;
    }
    
    private static void addOrderLine(Connection con, int ol_id, int ol_o_id, int ol_i_id, int ol_qty,
                                     double ol_discount, String ol_comment)
          throws SQLException {
        int success;
         success = 0;
        PreparedStatement insert_row;
         insert_row = con.prepareStatement(("INSERT into order_line (ol_id, ol_o_id, ol_i_id, ol_qty, ol_discount, ol_comments) VALUES (?, ?, ?, ?, ?, ?)"));
         ;
         insert_row.setInt(1, ol_id);
         ;
         insert_row.setInt(2, ol_o_id);
         ;
         insert_row.setInt(3, ol_i_id);
         ;
         insert_row.setInt(4, ol_qty);
         ;
         insert_row.setDouble(5, ol_discount);
         ;
         insert_row.setString(6, ol_comment);
         ;
         insert_row.executeUpdate();
         ;
         insert_row.close();
         ;
    }
    
    private static int getStock(Connection con, int i_id) throws SQLException {
        int stock;
         stock = 0;
        PreparedStatement get_stock;
         get_stock = con.prepareStatement("SELECT i_stock FROM item WHERE i_id = ?");
         ;
         get_stock.setInt(1, i_id);
         ;
        ResultSet rs;
         rs = get_stock.executeQuery();
         ;
         rs.next();
         ;
         stock = rs.getInt("i_stock");
         ;
         rs.close();
         ;
         get_stock.close();
         ;
         return stock;
    }
    
    private static void setStock(Connection con, int i_id, int new_stock) throws SQLException {
        PreparedStatement update_row;
         update_row = con.prepareStatement("UPDATE item SET i_stock = ? WHERE i_id = ?");
         ;
         update_row.setInt(1, new_stock);
         ;
         update_row.setInt(2, i_id);
         ;
         update_row.executeUpdate();
         ;
         update_row.close();
         ;
    }
    
     TPCW_Database() { 
    	 super(); 
    	 ;
    }
}
