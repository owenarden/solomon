package tpcw.servlets.pyxis;

import java.util.Date;
import java.sql.*;

public class ShortBook 
{
    
     ShortBook(int i_id, String i_title, String a_fname, String a_lname) 
    {
    	 super();
    	 this.i_id = i_id;
    	 this.i_title = i_title;
    	 this.a_fname = a_fname;
    	 this.a_lname = a_lname;
    }
    
     public int i_id;
     public String i_title;
     public String a_fname;
     public String a_lname;
}
