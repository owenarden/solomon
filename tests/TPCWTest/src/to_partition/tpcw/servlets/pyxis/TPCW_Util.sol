package tpcw.servlets.pyxis;

import java.util.*;

public class TPCW_Util {
    
     static int getRandomI_ID() 
    {
        int NUM_ITEMS;
         NUM_ITEMS = 10000;
        Random rand;
         rand = new Random();
        float flat$1030;
         flat$1030 = rand.nextFloat();
        float flat$1031;
         flat$1031 = flat$1030 * NUM_ITEMS;
        double flat$1032;
         flat$1032 = Math.floor(flat$1031);
        Double temp;
         temp = new Double(flat$1032);
        int flat$1033;
         flat$1033 = temp.intValue();
         return flat$1033;
    }
    
     static int getRandom(int i) {
        double flat$1034;
         flat$1034 = Math.random();
        double flat$1035;
         flat$1035 = flat$1034 * i;
        int flat$1036;
         flat$1036 = (int) flat$1035;
        int flat$1037;
         flat$1037 = flat$1036 + 1;
         return flat$1037;
    }
    
     static String getRandomString(int min, int max) {
        String newstring;
         newstring = new String();
        Random rand;
         rand = new Random();
        int i;
        char[] chars;
         chars = (new char[79]);
         chars[0] = 'a';
         chars[1] = 'b';
         chars[2] = 'c';
         chars[3] = 'd';
         chars[4] = 'e';
         chars[5] = 'f';
         chars[6] = 'g';
         chars[7] = 'h';
         chars[8] = 'i';
         chars[9] = 'j';
         chars[10] = 'k';
         chars[11] = 'l';
         chars[12] = 'm';
         chars[13] = 'n';
         chars[14] = 'o';
         chars[15] = 'p';
         chars[16] = 'q';
         chars[17] = 'r';
         chars[18] = 's';
         chars[19] = 't';
         chars[20] = 'u';
         chars[21] = 'v';
         chars[22] = 'w';
         chars[23] = 'x';
         chars[24] = 'y';
         chars[25] = 'z';
         chars[26] = 'A';
         chars[27] = 'B';
         chars[28] = 'C';
         chars[29] = 'D';
         chars[30] = 'E';
         chars[31] = 'F';
         chars[32] = 'G';
         chars[33] = 'H';
         chars[34] = 'I';
         chars[35] = 'J';
         chars[36] = 'K';
         chars[37] = 'L';
         chars[38] = 'M';
         chars[39] = 'N';
         chars[40] = 'O';
         chars[41] = 'P';
         chars[42] = 'Q';
         chars[43] = 'R';
         chars[44] = 'S';
         chars[45] = 'T';
         chars[46] = 'U';
         chars[47] = 'V';
         chars[48] = 'W';
         chars[49] = 'X';
         chars[50] = 'Y';
         chars[51] = 'Z';
         chars[52] = '!';
         chars[53] = '@';
         chars[54] = '#';
         chars[55] = '$';
         chars[56] = '%';
         chars[57] = '^';
         chars[58] = '&';
         chars[59] = '*';
         chars[60] = '(';
         chars[61] = ')';
         chars[62] = '_';
         chars[63] = '-';
         chars[64] = '=';
         chars[65] = '+';
         chars[66] = '{';
         chars[67] = '}';
         chars[68] = '[';
         chars[69] = ']';
         chars[70] = '|';
         chars[71] = ':';
         chars[72] = ';';
         chars[73] = ',';
         chars[74] = '.';
         chars[75] = '?';
         chars[76] = '/';
         chars[77] = '~';
         chars[78] = ' ';
        double flat$1038;
         flat$1038 = rand.nextDouble();
        int flat$1039;
         flat$1039 = max - min;
        int flat$1040;
         flat$1040 = flat$1039 + 1;
        double flat$1041;
         flat$1041 = flat$1038 * flat$1040;
        double flat$1042;
         flat$1042 = Math.floor(flat$1041);
        int strlen;
         strlen = (int) flat$1042;
         strlen = strlen + min;
        {
        	 i = 0;
            boolean loop$20;
             loop$20 = false;
            while ( true) {
                if ( loop$20) {
                    {  i = i + 1; }
                     ;
                }
                 loop$20 = i < strlen;
                if ( loop$20) {
                    double flat$1043;
                     flat$1043 = rand.nextDouble();
                    double flat$1044;
                     flat$1044 = flat$1043 * 79;
                    double flat$1045;
                     flat$1045 = Math.floor(flat$1044);
                    int flat$1046;
                     flat$1046 = (int) flat$1045;
                    char c;
                     c = chars[flat$1046];
                    String flat$1047;
                     flat$1047 = String.valueOf(c);
                     newstring = newstring.concat(flat$1047);
                } else {
                	 break;
                }
            }
        }
         return newstring;
    }
    
     static String DigSyl(int d, int n) {
        String[] digS;
         digS = (new String[10]);
         digS[0] = "BA";
         digS[1] = "OG";
         digS[2] = "AL";
         digS[3] = "RI";
         digS[4] = "RE";
         digS[5] = "SE";
         digS[6] = "AT";
         digS[7] = "UL";
         digS[8] = "IN";
         digS[9] = "NG";
        String s;
         s = "";
        boolean flat$1048;
         flat$1048 = n == 0;
        if ( flat$1048) {
            String flat$1049;
             flat$1049 = TPCW_Util.DigSyl(d);
             ;
             return flat$1049;
        }
        {
            boolean loop$21;
             loop$21 = false;
            while ( true) {
                if ( loop$21) {
                    {  n = n - 1; }
                     ;
                }
                 loop$21 = n > 0;
                if ( loop$21) {
                    int c;
                     c = d % 10;
                    String flat$1050;
                     flat$1050 = digS[c];
                     s = flat$1050 + s;
                     d = d / 10;
                } else {
                	 break;
                }
            }
        }
         return s;
    }
    
     static String DigSyl(int d) {
        String[] digS;
         digS = (new String[10]);
         digS[0] = "BA";
         digS[1] = "OG";
         digS[2] = "AL";
         digS[3] = "RI";
         digS[4] = "RE";
         digS[5] = "SE";
         digS[6] = "AT";
         digS[7] = "UL";
         digS[8] = "IN";
         digS[9] = "NG";
        String s;
         s = "";
        {
            boolean loop$22;
             loop$22 = false;
            while ( true) {
                if ( loop$22) {  d = d / 10; }
                 loop$22 = d != 0;
                if ( loop$22) {
                    int c;
                     c = d % 10;
                    String flat$1051;
                     flat$1051 = digS[c];
                     s = flat$1051 + s;
                } else {
                	 break;
                }
            }
        }
         return s;
    }
    
     TPCW_Util() {  super(); }
}
