package tpcw.servlets.pyxis;

import java.util.Date;
import java.sql.*;

public class Book 
{
    
    Book(ResultSet rs) throws SQLException 
    {
        super();
        int flat$1107;
        flat$1107 = rs.getInt("i_id");
        i_id = flat$1107;
        String flat$1108;
        flat$1108 = rs.getString("i_title");
        i_title = flat$1108;
        java.sql.Date flat$1109;
        flat$1109 = rs.getDate("i_pub_date");
        i_pub_Date = flat$1109;
        String flat$1110;
        flat$1110 = rs.getString("i_publisher");
        i_publisher = flat$1110;
        String flat$1111;
        flat$1111 = rs.getString("i_subject");
        i_subject = flat$1111;
        String flat$1112;
        flat$1112 = rs.getString("i_desc");
        i_desc = flat$1112;
        int flat$1113;
        flat$1113 = rs.getInt("i_related1");
        i_related1 = flat$1113;
        int flat$1114;
        flat$1114 = rs.getInt("i_related2");
        i_related2 = flat$1114;
        int flat$1115;
        flat$1115 = rs.getInt("i_related3");
        i_related3 = flat$1115;
        int flat$1116;
        flat$1116 = rs.getInt("i_related4");
        i_related4 = flat$1116;
        int flat$1117;
        flat$1117 = rs.getInt("i_related5");
        i_related5 = flat$1117;
        String flat$1118;
        flat$1118 = rs.getString("i_thumbnail");
        i_thumbnail = flat$1118;
        String flat$1119;
        flat$1119 = rs.getString("i_image");
        i_image = flat$1119;
        double flat$1120;
        flat$1120 = rs.getDouble("i_srp");
        i_srp = flat$1120;
        double flat$1121;
        flat$1121 = rs.getDouble("i_cost");
        i_cost = flat$1121;
        java.sql.Date flat$1122;
        flat$1122 = rs.getDate("i_avail");
        i_avail = flat$1122;
        String flat$1123;
        flat$1123 = rs.getString("i_isbn");
        i_isbn = flat$1123;
        int flat$1124;
        flat$1124 = rs.getInt("i_page");
        i_page = flat$1124;
        String flat$1125;
        flat$1125 = rs.getString("i_backing");
        i_backing = flat$1125;
        String flat$1126;
        flat$1126 = rs.getString("i_dimensions");
        i_dimensions = flat$1126;
        int flat$1127;
        flat$1127 = rs.getInt("a_id");
        a_id = flat$1127;
        String flat$1128;
        flat$1128 = rs.getString("a_fname");
        a_fname = flat$1128;
        String flat$1129;
        flat$1129 = rs.getString("a_lname");
        a_lname = flat$1129;
    }
    
    public int i_id;
    public String i_title;
    public Date i_pub_Date;
    public String i_publisher;
    public String i_subject;
    public String i_desc;
    public int i_related1;
    public int i_related2;
    public int i_related3;
    public int i_related4;
    public int i_related5;
    public String i_thumbnail;
    public String i_image;
    public double i_srp;
    public double i_cost;
    public Date i_avail;
    public String i_isbn;
    public int i_page;
    public String i_backing;
    public String i_dimensions;
    public int a_id;
    public String a_fname;
    public String a_lname;
}
