package tpcw.servlets.pyxis;

import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

import tpcw.servlets.pyxis.PyxisTransactionExecutor;

public class TestMain
{
	// calls all entry points (i.e., all servlet handlers)
	public static void main (String [] args) throws SQLException
	{
		PyxisTransactionExecutor exe = new PyxisTransactionExecutor();
		exe.homeInteractionInteraction(false, "", "", "", "", "");
		exe.newProductsInteraction("", "", "", "");
		exe.bestSellersInteraction("", "", "", "");
		exe.shoppingCartInteraction("", "", "", "", new Vector(), new Vector(), ""); 
		exe.customerRegistrationInteraction("", "", "");
		exe.buyRequestInteraction("", "", "", "", "",  
				 				  "", "", "", "", "",
				 				  "", "", "", "", "", "", "", ""); 
		exe.buyConfirmInteraction ("", "", "", "", "",
								   "", "", "", "", "",
								   "", "", "", "");
		exe.searchRequestInteraction("", "", "");
		exe.executeSearchInteraction("", "", "", "", "");
		exe.orderInquiryInteraction("", "", "");
		exe.orderDisplayInteraction("", "", "", "", "");
		exe.adminRequestInteraction("", "", "", "");
		exe.adminResponseInteraction("", "", "", "", "", "", ""); 
		exe.productDetailInteraction("", "", "", "");
		
	}
}
