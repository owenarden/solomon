package tpcw.servlets.pyxis;

import java.sql.*;

public class OrderLine 
{
    
     OrderLine(ResultSet rs) throws SQLException 
    {
    	 super();
        int flat$1130;
         flat$1130 = rs.getInt("ol_i_id");
         ol_i_id = flat$1130;
        String flat$1131;
         flat$1131 = rs.getString("i_title");
         i_title = flat$1131;
        String flat$1132;
         flat$1132 = rs.getString("i_publisher");
         i_publisher = flat$1132;
        double flat$1133;
         flat$1133 = rs.getDouble("i_cost");
         i_cost = flat$1133;
        int flat$1134;
         flat$1134 = rs.getInt("ol_qty");
         ol_qty = flat$1134;
        double flat$1135;
         flat$1135 = rs.getDouble("ol_discount");
         ol_discount = flat$1135;
        String flat$1136;
         flat$1136 = rs.getString("ol_comments");
         ol_comments = flat$1136;
    }
    
     public int ol_i_id;
     public String i_title;
     public String i_publisher;
     public double i_cost;
     public int ol_qty;
     public double ol_discount;
     public String ol_comments;
}
