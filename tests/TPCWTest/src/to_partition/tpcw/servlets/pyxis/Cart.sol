package tpcw.servlets.pyxis;

import java.io.*;
import java.util.*;
import java.sql.*;

public class Cart 
{
     public double SC_SUB_TOTAL;
     public double SC_TAX;
     public double SC_SHIP_COST;
     public double SC_TOTAL;
     public Vector lines;
    
     Cart(ResultSet rs, double C_DISCOUNT) throws SQLException 
    {
    	 super();
        int i;
        int total_items;
        Vector flat$1052;
          flat$1052 = new Vector();
          lines = flat$1052;
        while ( true) 
        {
            boolean loop$23;
             loop$23 = rs.next();
            if ( loop$23) 
            {
                String flat$1053;
                 flat$1053 = rs.getString("i_title");
                double flat$1054;
                 flat$1054 = rs.getDouble("i_cost");
                double flat$1055;
                 flat$1055 = rs.getDouble("i_srp");
                String flat$1056;
                 flat$1056 = rs.getString("i_backing");
                int flat$1057;
                 flat$1057 = rs.getInt("scl_qty");
                int flat$1058;
                 flat$1058 = rs.getInt("scl_i_id");
                CartLine line;
                 line = new CartLine(flat$1053, flat$1054, flat$1055, flat$1056, flat$1057, flat$1058);
                 ;
                Vector flat$1059;
                 flat$1059 = lines;
                 flat$1059.addElement(line);
                 ;
            } 
            else 
            {
            	 break;
            }
        }
         SC_SUB_TOTAL = 0;
         total_items = 0;
        {
        	 i = 0;
            boolean loop$24;
             loop$24 = false;
            while ( true) 
            {
                if ( loop$24) 
                {                
                   	 i = i + 1;                  
                     ;
                }
                Vector flat$1060;
                 flat$1060 = lines;
                int flat$1061;
                 flat$1061 = flat$1060.size();
                 loop$24 = i < flat$1061;
                if ( loop$24) 
                {
                    Vector flat$1062;
                     flat$1062 = lines;
                    Object flat$1063;
                     flat$1063 = flat$1062.elementAt(i);
                    CartLine thisline;
                     thisline = (CartLine) flat$1063;
                    double flat$1064;
                     flat$1064 = thisline.scl_cost;
                    int flat$1065;
                     flat$1065 = thisline.scl_qty;
                    double flat$1066;
                     flat$1066 = flat$1064 * flat$1065;
                    double flat$1067;
                     flat$1067 = SC_SUB_TOTAL;
                    double flat$1068;
                     flat$1068 = flat$1067 + flat$1066;
                     SC_SUB_TOTAL = flat$1068;
                    int flat$1069;
                     flat$1069 = thisline.scl_qty;
                     total_items = total_items + flat$1069;
                } 
                else 
                {
                	 break;
                }
            }
        }
        
        double flat$1070;
         flat$1070 = SC_SUB_TOTAL;
        double flat$1071;
         flat$1071 = 100 - C_DISCOUNT;
        double flat$1072;
         flat$1072 = flat$1071 * 0.01;
        double flat$1073;
         flat$1073 = flat$1070 * flat$1072;
         SC_SUB_TOTAL = flat$1073;
        double flat$1074;
         flat$1074 = SC_SUB_TOTAL;
        double flat$1075;
         flat$1075 = flat$1074 * 0.0825;
         SC_TAX = flat$1075;
        double flat$1076;
         flat$1076 = 1.0 * total_items;
        double flat$1077;
         flat$1077 = 3.0 + flat$1076;
         SC_SHIP_COST = flat$1077;
        double flat$1078;
         flat$1078 = SC_SUB_TOTAL;
        double flat$1079;
         flat$1079 = SC_SHIP_COST;
        double flat$1080;
         flat$1080 = flat$1078 + flat$1079;
        double flat$1081;
         flat$1081 = SC_TAX;
        double flat$1082;
         flat$1082 = flat$1080 + flat$1081;
         SC_TOTAL = flat$1082;
    }
}
