import java.sql.*;
public class SalaryCalc {
	public static int getMaxSalary (int deptnoToQuery) throws SQLException
	{
		Connection conn;
		Statement stmt;
		ResultSet rs;
		int maxSalary, salary, deptno;
		
		conn = DriverManager.getConnection("dbURL");
		stmt = conn.createStatement();
		rs = stmt.executeQuery("select deptno, salary from emp");
		maxSalary = -1;
		
		while (rs.next())
		{
			deptno = rs.getInt(0);
			if (deptno == deptnoToQuery)
			{
				salary = rs.getInt(1);
				if (salary > maxSalary)
					maxSalary = salary;
			}
		}
		
		return maxSalary;
	}
	public static void main(String[] args) {
		try {
			getMaxSalary(2);	
		} catch (SQLException e) {
			
		}
	}
}
