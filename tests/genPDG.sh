#!/bin/bash
echo "digraph $1 {" > $1.dot
echo Generating dot file
../bin/solc $1.ifcn -report depends=3 2>&1|sed -n '/^ subgraph /, / }$/p' >> $1.dot
echo "}" >> $1.dot
echo Rendering dot file
dot -Tpdf $1.dot > $1.pdf
